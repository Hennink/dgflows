#!/usr/bin/env bash
set -e

DIR=$HOME/software/FoBiS/

# Get one copy:
rm -rf $DIR
cd $HOME/software/
git clone https://github.com/szaghi/FoBiS.git
cd $DIR
git checkout d9abb5c056be0aa706de8f4a41815a0fdb4adacf
chmod +x release/FoBiS-master/FoBiS.py

# Gain access from every directory:
mkdir -p $HOME/bin
ln -fs $DIR/release/FoBiS-master/FoBiS.py $HOME/bin/FoBiS.py

