#!/usr/bin/env bash
set -e
. ./env.sh

# This will download and install various architectures of METIS.
# [1]  http://glaros.dtc.umn.edu/gkhome/metis/metis/changes
# [2]  http://glaros.dtc.umn.edu/gkhome/metis/metis/download

VERSION=5.1.0 # released on March 30, 2013. See [1].
DIR=$HOME/software/metis-${VERSION}/
DOWNLOADS_DIR=$(readlink -f downloads/)

# Delete all old tars and installs:
rm -rf $HOME/software/metis-${VERSION}*
# Download and extract the METIS release from [2]:
rm -rf $DIR
mkdir -p $DIR
cd $HOME/software/
{
    wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-${VERSION}.tar.gz
} || {
    cp ${DOWNLOADS_DIR}/metis-${VERSION}.tar.gz .
}

tar -xvzf metis-${VERSION}.tar.gz

cd $DIR

# with very expensive run-time assertions:____
ARCHITECTURE=arch-debug
make distclean # just to be sure to start clean
make config cc=$CC assert=1 assert2=1 prefix=$DIR/$ARCHITECTURE
make install

# with more optimized options:____
ARCHITECTURE=arch-opt
make distclean # just to be sure to start clean
make config cc=$CC prefix=$DIR/$ARCHITECTURE
make install

