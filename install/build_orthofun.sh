#!/usr/bin/env bash
set -e
. env.sh

cd orthofun/

(
    mkdir -p devel
    ${FC} ${DGF_FLAGS_DEVEL_F} -c orthofun.f90 -o devel/orthofun.o ${DGF_FLAG_MODDIR}devel/
) &
(
    mkdir -p release
    ${FC} ${DGF_FLAGS_RELEASE_F} -c orthofun.f90 -o release/orthofun.o ${DGF_FLAG_MODDIR}release/
) &

wait || {echo "orthofun build failed"}

