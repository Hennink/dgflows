#!/bin/bash
set -e

DIR=downloads/

mkdir -p $DIR
cd $DIR

wget http://gmsh.info/bin/Linux/gmsh-3.0.5-Linux64.tgz
wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-5.1.0.tar.gz
wget ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-3.10.1.tar.gz

