#!/usr/bin/env bash
# 
# You should have these lines in ~/.bashrc:
# 
#    # MPI Library:
#    export OPENMPI_ROOT=${HOME}/openmpi_64
#    export PATH=${OPENMPI_ROOT}/bin:$PATH
#    export LD_LIBRARY_PATH=${OPENMPI_ROOT}/lib:$LD_LIBRARY_PATH
#
set -e
. env.sh

_=${VALGRIND_ROOT?Error \$VALGRIND_ROOT is not defined}
_=${OPENMPI_ROOT?Error \$OPENMPI_ROOT is not defined}

version=4.0.2
ompi=openmpi-${version}

tmp_dir=${HOME}/delete-this-tmp-openmpi-dir/
rm -rf ${tmp_dir} ${OPENMPI_ROOT}
mkdir -p ${tmp_dir}
cd ${tmp_dir}
wget https://download.open-mpi.org/release/open-mpi/v4.0/${ompi}.tar.gz
tar -xz -f ${ompi}.tar.gz
cd ${ompi}
./configure CC=${CC} CXX=${CXX} FC=${FC}  \
    --prefix=${OPENMPI_ROOT} --enable-mpirun-prefix-by-default  \
    --with-valgrind=${VALGRIND_ROOT} \
    --enable-mpi-cxx  \
    --enable-mpi-fortran  \
    --enable-static
make -j 2 # using more cores gave me errors about not having enough memory to allocate stuff
make install -j
cd
rm -rf ${tmp_dir}
