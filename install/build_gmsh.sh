#!/bin/bash
# This downloads and extracts a gmsh binary
set -e

VERSION=3.0.6

GMSH_VERSION=gmsh-${VERSION}
SOFTWARE_DIR=$HOME/software/
progname=${GMSH_VERSION}-Linux64

downloads_dir=$(readlink -f downloads/)

mkdir -p $SOFTWARE_DIR
cd $SOFTWARE_DIR
rm -rf ${GMSH_VERSION}*  # also gets rid of old .tar files

# Download the Linux release from [1]:
{
    wget http://gmsh.info/bin/Linux/${progname}.tgz
} || {
    cp ${downloads_dir}/${progname}.tgz .
}

# Simply extract the file (nothing to install):
tar -xz -f ${progname}.tgz

# Gain access from every directory:
mkdir -p $HOME/bin
ln -fs ${SOFTWARE_DIR}/${progname}/bin/gmsh $HOME/bin/gmsh
