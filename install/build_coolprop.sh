#!/bin/bash

# This builds the CoolProp library.
# Repo on GitHub: [0]
# Documentation: [1]
# Open source; free license for academia and business
# 
# We followed the instructions laid down in [2], with the exception that we use
# Intel compilers for C/C++ and Fortran, not GNU. 

# We got some inconsequential warnings about the C/C++ code. ( All were
# of the type "warning #858: type qualifier on return type is meaningless".)
# These warnings do not appear when using the GNU compilers.

# [0] https://github.com/CoolProp/CoolProp
# [1] http://www.coolprop.org/
# [2] http://www.coolprop.org/coolprop/wrappers/FORTRAN/index.html

# On HLRS (Hazelhen), you will need to force it to link dynamically, because 
# it does not do so by default, and 'glibc' cannot be linked statically.
# Failure to do so will give a compile-time warning
#     "warning: Using '<func. name>' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
# and a runtime error for some (not all) CoolProp calls:
#     Stack trace terminated abnormally.
#     (...)
#     _pmiu_daemon(SIGCHLD): [NID 04303] [c10-1c1s3n3] [Mon Nov 26 10:26:19 2018] PE RANK 22 exit signal Aborted
#     forrtl: error (76): Abort trap signal
# To fix this, I appended this to ~/.bash_profile:
#     export XTPE_LINK_TYPE=dynamic
#     export CRAYPE_LINK_TYPE=dynamic
set -e
. ./env.sh

SOFTWARE_DIR=$HOME/software/
ARCH=arch-static

for c in $MPICC $MPICXX cmake; do
  command -v $c >/dev/null 2>&1 || { echo >&2 "I require $c it's not available.  Aborting."; exit 1; }
done

mkdir -p $SOFTWARE_DIR
cd $SOFTWARE_DIR
rm -rf CoolProp/
git clone https://github.com/CoolProp/CoolProp.git
cd CoolProp/
git checkout v6.3.0 # We've had several glitches with their regular updates. They probably don't test with the Intel compilers.
git submodule update --init --recursive
mkdir build && cd build
mkdir ${ARCH} && cd ${ARCH}

cmake ../..  \
  -DCOOLPROP_STATIC_LIBRARY=ON \
  -DCOOLPROP_EXTERNC_LIBRARY=ON \
  -DCMAKE_C_COMPILER=$MPICC \
  -DCMAKE_CXX_COMPILER=$MPICXX \
  -DCMAKE_VERBOSE_MAKEFILE=ON
cmake --build .
