#!/bin/bash
#PBS -N PETSc_conftest
#PBS -l nodes=1:ppn=1
#PBS -l walltime=00:25:00
#PBS -q test
  
cd $HOME/software/petsc-3.10.1/

# Launch the parallel job to the allocated compute nodes

aprun -n 1 -N 1 ./conftest-arch-linux2-c-mpi-debug
aprun -n 1 -N 1 ./conftest-arch-linux2-c-mpi-opt
