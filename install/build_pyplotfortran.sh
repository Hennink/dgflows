#!/usr/bin/env bash
set -e
. ./env.sh

DIR=$HOME/software/pyplot-fortran/

rm -rf $DIR
cd $HOME/software/
git clone https://github.com/jacobwilliams/pyplot-fortran.git
cd $DIR
# Uncomment to use our last version:
#   git checkout fd73436047aacc9b453bcea55aea62a5f2ca8274

# The build script that's included requires FoBiS, so we do this manually:
mkdir lib/
$FC ${DGF_FLAGS_DEVEL_F} ${DGF_FLAG_MODDIR}lib/ -c src/pyplot_module.f90 -o lib/pyplot_module.o
