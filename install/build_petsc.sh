#!/usr/bin/env bash

# This will download and install various architectures of PETSc.
# Installation instructions are in [3].
# [1]  https://www.mcs.anl.gov/petsc/documentation/changes/index.html
# [2]  https://www.mcs.anl.gov/petsc/download/index.html
# [3]  https://www.mcs.anl.gov/petsc/documentation/installation.html
set -e
. ./env.sh

VERSION="3.14.3"
PETSC_DIR=$HOME/software/petsc-${VERSION}/
DOWNLOAD_HYPRE=no
DOWNLOADS_DIR=$(readlink -f downloads/)

if [ ! -v MKLROOT ]; then
    echo "environment variable \$$MKLROOT is not set"
    exit 1
fi

for c in $CC $CXX $FC $MPICC $MPICXX $MPIFC; do
  command -v $c >/dev/null 2>&1 || { echo >&2 "I require $c it's not available.  Aborting."; exit 1; }
done

# Make a `petsc` link, so that $PETSC_DIR in the Makefile does not need
# to change when we bump the version number:
rm -rf "${PETSC_DIR%/}"*     # also gets rid of old .tar files
mkdir -p ${PETSC_DIR}
rm -f $PETSC_DIR/../petsc
ln -s $PETSC_DIR $PETSC_DIR/../petsc

# Download and extract the PETSc release from [2]:
cd ${PETSC_DIR}/..
{
    wget "https://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-${VERSION}.tar.gz"
} || {
    cp ${DOWNLOADS_DIR}/petsc-${VERSION}.tar.gz .
}
gunzip -c petsc-${VERSION}.tar.gz | tar -xof -

cd ${PETSC_DIR}

compilers_lapack="--with-cc=${MPICC} --with-cxx=${MPICXX} --with-fc=${MPIFC}  --download-hypre=${DOWNLOAD_HYPRE}  --with-blaslapack-dir=${MKLROOT}  --with-x=0"
flags_devel="    --COPTFLAGS=${DGF_FLAGS_DEVEL_C}                   --CXXOPTFLAGS=${DGF_FLAGS_DEVEL_C}                   --FOPTFLAGS=${DGF_FLAGS_DEVEL_F}"
flags_optim="    --COPTFLAGS=${DGF_FLAGS_RELEASE_C}                 --CXXOPTFLAGS=${DGF_FLAGS_RELEASE_C}                 --FOPTFLAGS=${DGF_FLAGS_RELEASE_F}"
flags_optim_ipo="--COPTFLAGS=${DGF_FLAGS_RELEASE_C} ${DGF_FLAG_IPO} --CXXOPTFLAGS=${DGF_FLAGS_RELEASE_C} ${DGF_FLAG_IPO} --FOPTFLAGS=${DGF_FLAGS_RELEASE_F} ${DGF_FLAG_IPO}"

np=4

(
    arch=arch-devel
    ./configure   PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${arch}  ${compilers_lapack} ${flags_devel}
    make -j ${np} PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${arch}  all
    make PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${arch} test
)

(
    arch=arch-optim
    ./configure   PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${arch}  --with-debugging=no  ${compilers_lapack} ${flags_optim}
    make -j ${np} PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${arch}  all
    make PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${arch} test
)

(
    arch=arch-optim-ipo
    ./configure   PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${arch} --with-debugging=no  --with-shared-libraries=0  ${compilers_lapack} ${flags_optim_ipo}
    make -j ${np} PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${arch}  all
    make PETSC_DIR=${PETSC_DIR} PETSC_ARCH=${arch} test
)

