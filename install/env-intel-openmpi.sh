CC=icc
CXX=icpc
FC=ifort
MPICC=mpicc
MPICXX=mpicxx
MPIFC=mpifort

MKL_CFLAGS="-I${MKLROOT}/include/intel64/lp64/"
MKL_LFLAGS="-L${MKLROOT}/lib/intel64/ -Wl,--start-group -lmkl_lapack95_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 --end-group"
# Intel link advisor: MKL_LFLAGS="${MKLROOT}/lib/intel64/libmkl_blas95_lp64.a ${MKLROOT}/lib/intel64/libmkl_lapack95_lp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_lp64.a ${MKLROOT}/lib/intel64/libmkl_sequential.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm -ldl"

CPP_LIB_LFLAG=" -lstdc++ "
DGF_FLAG_MODDIR="-module "
DGF_FLAG_PP=" -fpp "
DGF_FLAG_IPO="-ipo"
DGF_FLAG_IPO+=" -ipo-jobs4" # `-ipo-jobs<n>` should specifys the number of jobs to be executed simultaneously during the IPO link phase (but seems to have no effect for us)


## General basic flags:
_DGF_FLAGS_BASIC="-g -warn all -traceback"

## Fortran basic flags:
_DGF_FLAGS_BASIC_F=${_DGF_FLAGS_BASIC}
_DGF_FLAGS_BASIC_F+=" -stand f18 -warn stderrors" # Conform to Fortran 2018 standard
_DGF_FLAGS_BASIC_F+=" -warn nounused" # Avoid unnecessary warnings about dummy arguments in overwriting procedures.

## C/C++ basic flags:
_DGF_FLAGS_BASIC_C=${_DGF_FLAGS_BASIC}
_DGF_FLAGS_BASIC_F+=" -std=c11" # Conform to C11 standard
_DGF_FLAGS_BASIC_F+=" -std=c++18" # Conform to C11 standard

## Runtime checks:
_DGF_FLAGS_RUNTIME_CHECKS="-O0 -fpe0 -fp-model strict -fp-speculation=off -inline-level=0 -check all -init=arrays -init=snan -fstack-security-check -fp-stack-check -fstack-protector" # -Winline Wcheck
# _DGF_FLAGS_RUNTIME_CHECKS+=" -fno-builtin" # disable inline expansion of intrinsic functions
_DGF_FLAGS_RUNTIME_CHECKS+=" -fno-defer-pop" # disable optimizations which may result in deferred clearance of the stack arguments
_DGF_FLAGS_RUNTIME_CHECKS+=" -nolib-inline" # disable inline expansion of intrinsic functions
# _DGF_FLAGS_RUNTIME_CHECKS+=" -ansi-alias" # enable use of ANSI aliasing rules optimizations; user asserts that the program adheres to these rules
_DGF_FLAGS_RUNTIME_CHECKS+=" -fstack-protector-all" # enable stack overflow security checks including functions

## Optim flags:
# (doesn't use Profile Guided Optimization (PGO) or aliasing heutistics)
_DGF_FLAGS_OPTIM=""
_DGF_FLAGS_OPTIM+=" -O3 -no-prec-div -fp-model fast=2" # same as `-Ofast`
_DGF_FLAGS_OPTIM+=" -fno-alias" # assume no aliasing in program
_DGF_FLAGS_OPTIM+=" -fno-fnalias" # assume no aliasing within functions, but assume aliasing across calls
_DGF_FLAGS_OPTIM+=" -fdefer-pop" # 
_DGF_FLAGS_OPTIM+=" -fno-protect-parens" # disable a reassociation optimization for REAL and COMPLEX expression evaluations by not honoring parenthesis
_DGF_FLAGS_OPTIM+=" -unroll-aggressive" # enables more aggressive unrolling heuristics
_DGF_FLAGS_OPTIM+=" -finline-functions" # inline any function at the compiler's discretion
_DGF_FLAGS_OPTIM+=" -no-inline-max-per-routine -no-inline-max-per-compile" # no maximum number of inline instances in any function and in the current compilation
_DGF_FLAGS_OPTIM+=" -no-inline-factor" # do not set set inlining upper limits
_DGF_FLAGS_OPTIM+=" -nocheck" # do not check run-time conditions
_DGF_FLAGS_OPTIM+=" -qoverride-limits" # override certain internal compiler limits that are intended to prevent excessive memory usage or compile times for very large, complex compilation units
_DGF_FLAGS_OPTIM+=" -qopt-mem-layout-trans" # enable aggressive memory layout transformations
_DGF_FLAGS_OPTIM+=" -qopt-jump-tables=large" # generate jump tables up to a certain pre-defined size (64K entries)
_DGF_FLAGS_OPTIM+=" -qopt-subscript-in-range" # assumes no overflows in the intermediate computation of the subscripts
_DGF_FLAGS_OPTIM+=" -qopt-matmul" # replace matrix multiplication with calls to intrinsics and threading libraries for improved performance
# _DGF_FLAGS_OPTIM+=" -static-intel" # link Intel provided libraries statically
# _DGF_FLAGS_OPTIM+=" -static-libgcc" # link libgcc statically
# _DGF_FLAGS_OPTIM+=" -static-libstdc++" # link libstdc++ statically
_DGF_FLAGS_OPTIM+=" -fomit-frame-pointer"
# _DGF_FLAGS_OPTIM+=" -guide-opts"

# Full compile flags:
DGF_FLAGS_DEVEL_C="${_DGF_FLAGS_BASIC_C}    ${_DGF_FLAGS_RUNTIME_CHECKS}"
DGF_FLAGS_DEVEL_F="${_DGF_FLAGS_BASIC_F}    ${_DGF_FLAGS_RUNTIME_CHECKS}"
DGF_FLAGS_RELEASE_C="${_DGF_FLAGS_BASIC_C}  ${_DGF_FLAGS_OPTIM}"
DGF_FLAGS_RELEASE_F="${_DGF_FLAGS_BASIC_F}  ${_DGF_FLAGS_OPTIM}"
