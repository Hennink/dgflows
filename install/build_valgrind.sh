#!/usr/bin/env bash
# 
# You should have these lines in ~/.bashrc:
# 
#       # Valgrind:
#       export VALGRIND_LIB="$HOME/valgrind/lib/valgrind"
#       export PATH=$HOME/valgrind/bin:$PATH
# 
set -e
. env.sh

_=${VALGRIND_ROOT?Error \$VALGRIND_ROOT is not defined}

version=3.15.0
progname=valgrind-${version}

tmp_dir=${HOME}/delete-this-tmp-valgrind-dir/

rm -rf ${VALGRIND_ROOT} ${tmp_dir}
mkdir -p ${tmp_dir}
cd ${tmp_dir}
wget https://sourceware.org/pub/valgrind/${progname}.tar.bz2
tar -x -f ${progname}.tar.bz2
cd ${progname}/
# Valgrind's MPI support is incompatible with OpenMPI 4.0.2, because
# Valgrind uses some deprecated MPI-2 macro.
# A solution is to build Valgrind without MPI (as below), and then to rebuild openMPI with 
# Vallgrind support.
./configure \
    --prefix=${VALGRIND_ROOT} \
    CC=${CC} \
    --with-mpicc=
make
make install
cd
rm -rf ${tmp_dir}

