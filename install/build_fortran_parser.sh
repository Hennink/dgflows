#!/usr/bin/env bash

# ======== FortranParser =========
# Repo in GitHub: [0]
# This library parses mathematical expressions: it interprets strings as 
# functions that can be evaluated. 
# 
# The following commands build the lib manually.
#  * I did not manage to follow the cmake build instructions.
#  * The master version is buggy; it leaks memory (see [1]). Use the corrected 
#    version in Aldo's fork instead.
# 
# [0]  https://github.com/jacopo-chevallard/FortranParser
# [1]  https://github.com/jacopo-chevallard/FortranParser/issues/3
set -e
. ./env.sh

FORTRANPARSER_ROOT=$HOME/software/FortParLib/

git_hash=fb1ee788a4758d35378a674864a41396c8b4c908
tmp_dir=${HOME}/delete-this-tmp-fortranparser-dir/

rm -rf ${tmp_dir} ${FORTRANPARSER_ROOT}

mkdir -p ${tmp_dir}
cd ${tmp_dir}
git clone https://github.com/hennink/FortranParser.git
cd FortranParser/
git checkout ${git_hash}

mkdir -p ${FORTRANPARSER_ROOT}/obj/ ${FORTRANPARSER_ROOT}/mod/
for f in FortranParser_parameters.f90 FortranParser.f90
do
    ${FC} ${DGF_FLAGS_DEVEL_F}  -c src/${f}  -o ${FORTRANPARSER_ROOT}/obj/${f%.*}.o  ${DGF_FLAG_MODDIR}${FORTRANPARSER_ROOT}/mod/
done

rm -rf ${tmp_dir}
