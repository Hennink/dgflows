#!/bin/bash
# These are instructions on how to build PETSc on the HLRS (Hazelhen) supercomputer.
# This is not a stand-alone build script. You will have to some stuff manually, see below.
# HYPRE is not installed.
set -e

VERSION=3.10.1              # the latest version, released on Sep 26, 2018. See [1].
PETSC_DIR=$HOME/software/petsc-${VERSION}/
DOWNLOADS_DIR=$HOME/third_party/

# Only install the MPI versions:
MPICC=cc
MPICXX=CC
MPIFC=ftn

for c in $MPICC $MPICXX $MPIFC; do
  command -v $c &> /dev/null || { echo >&2 "I require $c it's not available.  Aborting."; exit 1; }
done

rm -rf ${PETSC_DIR}
mkdir -p ${PETSC_DIR}
cd ${PETSC_DIR}/..
cp ${DOWNLOADS_DIR}/petsc-${VERSION}.tar.gz .
gunzip -c petsc-${VERSION}.tar.gz | tar -xof -

cd $PETSC_DIR

# The automatic HYPRE install is not compatible with the "--with-batch" option.
DOWNLOAD_HYPRE=no
BASIC_FFLAGS="-traceback"
DEBUG_FFLAGS="${BASIC_FFLAGS}  -check all -init=arrays -init=snan -fstack-security-check -fp-stack-check -fstack-protector"
RELEASE_FFLAGS="${BASIC_FFLAGS} -ipo -O3 -no-prec-div -fp-model fast=2 -nocheck -finline-functions -unroll-aggressive"
echo ${DEBUG_FFLAGS}
echo ${RELEASE_FFLAGS}

#      ____with debugging, with MPI:____
arch=arch-linux2-c-mpi-debug
./configure  \
    PETSC_DIR=$PETSC_DIR PETSC_ARCH=$arch  \
    --with-cc=${MPICC} --with-cxx=${MPICXX} --with-fc=${MPIFC} \
    -COPTFLAGS=${DEBUG_FFLAGS} -CXXOPTFLAGS=${DEBUG_FFLAGS} -FOPTFLAGS=${DEBUG_FFLAGS} \
    --with-batch=1 --known-mpi-shared-libraries=0 --known-64-bit-blas-indices=0 \
    --download-hypre=${DOWNLOAD_HYPRE}

#      ____without debugging, with MPI:____
arch=arch-linux2-c-mpi-opt
./configure  \
    PETSC_DIR=$PETSC_DIR PETSC_ARCH=$arch  \
    --with-cc=${MPICC} --with-cxx=${MPICXX} --with-fc=${MPIFC} \
    -COPTFLAGS=${RELEASE_FFLAGS} -CXXOPTFLAGS=${RELEASE_FFLAGS} -FOPTFLAGS=${RELEASE_FFLAGS} \
    --with-batch=1 --known-mpi-shared-libraries=0 --known-64-bit-blas-indices=0 \
    --download-hypre=${DOWNLOAD_HYPRE} \
    --with-debugging=no

# Now submit a job that runs the "conftest-<<ARCH>>" program. See 'hlrs_config_job.sh'. Wait for the job to finish.

# Now go to the PETSc directory and reconfigure:
# cd $PETSC_DIR
# ./reconfigure-arch-linux2-c-mpi-debug.py
# ./reconfigure-arch-linux2-c-mpi-opt.py

# Now build the PETSC libraries.
# To check whether they are working you would need to submit the tests with a job:
# make PETSC_DIR=/zhome/academic/HLRS/ike/ikeahenn/software/petsc-3.10.1/ PETSC_ARCH=arch-linux2-c-mpi-debug all
# # make PETSC_DIR=/zhome/academic/HLRS/ike/ikeahenn/software/petsc-3.10.1/ PETSC_ARCH=arch-linux2-c-mpi-debug check
# 
# make PETSC_DIR=/zhome/academic/HLRS/ike/ikeahenn/software/petsc-3.10.1/ PETSC_ARCH=arch-linux2-c-mpi-opt all
# # make PETSC_DIR=/zhome/academic/HLRS/ike/ikeahenn/software/petsc-3.10.1/ PETSC_ARCH=arch-linux2-c-mpi-opt check
