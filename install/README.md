# Compilation Instructions

## Step 0: Define Basic Macros

Put

```bash
export PATH=$HOME/bin:$PATH
```

somewhere in `~/.bash_profile`.
Every time you change this file,
you'll either need to log out and log in, or run `. ~/.bash_profile`.

## Step 1: Get C, C++, and Fortran compilers, and link the Intel MKL library

*
    **`hpc{11,20,21,22,23}.tudelft.net`**
    have the Intel compiler suite.
    The module system does not properly source the scripts with environment variables,
    so we do this ourselves.
    Use `module list` to verify that no modules are loaded,
    and add these lines to `~/.bash_profile`:

    ```bash
    # Intel compilers and MKL lib:
    INTEL_COMPILERS_AND_LIBS=/opt/ud/intel_xe_2019u2/compilers_and_libraries_2019/linux/
    . ${INTEL_COMPILERS_AND_LIBS}/bin/compilervars.sh intel64
    . ${INTEL_COMPILERS_AND_LIBS}/mkl/bin/mklvars.sh intel64
    ```

    where `INTEL_COMPILERS_AND_LIBS` can be adjusted to the newest version.
    (On `hpc{11,20,21,22,23}.tudelft.net`
    you can also find it by loading the newest `intel/20XX` module,
    typing `which ifort`, and then unloading the module.)

*
    **`reynolds.tudelft.nl`**
    also has the Intel compiler suite, but no module system.
    Source the `compilervars.sh` and `mklvars.sh` scripts,
    just like on `hpc{11,20,21,22,23}.tudelft.net`.
    The compiler root directory is probably

    ```bash
    INTEL_COMPILERS_AND_LIBS=/opt/intel2016/compilers_and_libraries_2018.1.163/linux/
    ```

*
    **`cartesius.surfsara.nl`**

    Using the new modules (not 'pre2019'),
    put this in `~/bash_profile`:

    ```bash
    module load 2019
    module load imkl
    ```

*
    **On your own computer,
    using the GNU compiler suite.**

    1.
        First let `gcc`/`g++`/`gfortran` point to the newest versions of the compilers:

        ```bash
        _GNU_VERSION=9

        _CC=gcc-${_GNU_VERSION}
        _CXX=g++-${_GNU_VERSION}
        _FC=gfortran-${_GNU_VERSION}

        sudo add-apt-repository ppa:ubuntu-toolchain-r/test
        sudo apt update
        sudo apt install ${_CC}
        sudo apt install ${_CXX}
        sudo apt install ${_FC}

        # Now make sure that `gcc --version`, `g++ --version`, and `gfortran --version`
        # are the same as what you'll use to compile DGF
        # (so not the outdated compilers provided by the OS):
        sudo update-alternatives --install /usr/bin/gcc         gcc         /usr/bin/${_CC}  60
        sudo update-alternatives --install /usr/bin/g++         g++         /usr/bin/${_CXX} 60
        sudo update-alternatives --install /usr/bin/gfortran    gfortran    /usr/bin/${_FC}  60
        yes '' | sudo update-alternatives --force --all

        unset _GNU_VERSION _CC _CXX _FC
        ```

        The last steps are necessary because the build scripts for MKL and Valgrind
        will call `gcc`, not `gcc-9`.

    1.
        [Download](https://software.intel.com/en-us/performance-libraries) the MKL library,
        and extract:

        ```bash
        tar -xz -f l_mkl_2020.1.217.tgz
        cd l_mkl_2020.1.217/
        ```

    1.
        Install with either `sudo ./install.sh` or `sudo ./install_GUI.sh`
        with the install directory `/opt/intel/`.

        (I ignored warnings about not officially supporting Linux Mint.
        They claim to support Ubuntu, which is pretty much the same thing.)

    1.
        Source the MKL script by adding these lines to `~/.bash_profile`:

        ```bash
        # MKL Environment Variables:
        INTEL_COMPILERS_AND_LIBS=/opt/intel/compilers_and_libraries_2020/linux
        . $INTEL_COMPILERS_AND_LIBS/mkl/bin/mklvars.sh intel64
        ```

    1. Build the interfaces for gfortran:

        ```bash
        _FC=gfortran
        _MKL_F95ROOT=${HOME}/mkl_gnu_f95root
        _MKL_LIB=libintel64 # can be libia32 or libintel64
        _MKL_INTERFACE=lp64 # can be lp64 or ilp64 for intel64. Default value is lp64.

        rm -rf ${_MKL_F95ROOT}

        cd ${MKLROOT}/interfaces/lapack95
        make -j ${_MKL_LIB} FC=${_FC} interface=${_MKL_INTERFACE} INSTALL_DIR=${_MKL_F95ROOT}

        cd ${MKLROOT}/interfaces/blas95
        make -j ${_MKL_LIB} FC=${_FC} interface=${_MKL_INTERFACE} INSTALL_DIR=${_MKL_F95ROOT}

        unset _FC _MKL_F95ROOT _MKL_LIB _MKL_INTERFACE
        ```

## Step 2 Tell `DGFlows` about the compilers

Add the file `install/env.sh` that lists the right compilers
and some extra stuff that is particular to you system.
There are examples for several clusters in the Git repo.
If you are on one of these clusters,
then you can just point to the right pre-existing `env.sh` file.
For example, TUD's HPC clusters use OpenMPI, so do

```bash
cd install/
ln -sf env-intel-openmpi.sh env.sh
```

On Cartesius you'll use the Intel implementation of the MPI library,
so use

```bash
cd install/
ln -sf env-intel-mpich.sh env.sh
```

If you're using GNU compilers with OpenMPI, then

```bash
cd install/
ln -sf env_gnu.sh env.sh
```

## Step 3: Build Valgrind

You will build a newer version of Valgrind
for debugging memory-related issues,
because pre-installed versions on the TUD clusters do not understand modern processor commands.
Add these lines to `~/.bash_profile`:

```bash
# Valgrind:
export VALGRIND_ROOT=$HOME/valgrind
export PATH=${VALGRIND_ROOT}/bin:$PATH
export CPATH=${VALGRIND_ROOT}/include/:$CPATH
# export VALGRIND_LIB="${VALGRIND_ROOT}/lib/valgrind" # probably not needed
```

(Adjusting `CPATH` in this way will allow PETSc to interface with Valgrind through the C/C++ API.)

Then install with

```bash
cd install/
./build_valgrind.sh
```

## Step 4: Get MPI wrappers for the compilers

You need an MPI3-complient implementation of the MPI library,
because we use the `mpi_f08` Fortran interfaces.

*
    On `hpc{11,20,21,22,23}.tudelft.net`,
    the default version of OpenMPI is ancient,
    and not MPI3-complient.
    Therefore build your own OpenMPI library by putting

    ```bash
    # MPI Library:
    export OPENMPI_ROOT=${HOME}/openmpi_64
    export PATH=${OPENMPI_ROOT}/bin:$PATH
    export LD_LIBRARY_PATH=${OPENMPI_ROOT}/lib:$LD_LIBRARY_PATH
    ```

    in `~/.bash_profile`,
    and run

    ```bash
    cd install/
    ./build_openmpi.sh
    ```

*
    On `reynolds.tudelft.nl`
    you build your own OpenMPI library,
    as on `hpc{11,20,21,22,23}.tudelft.net`.
    I never got the pre-installed version to work.

*
    On `cartesius.surfsara.nl`
    there is no need to do anything,
    because the `imkl` module already loads the parallel Intel compilers in `impi`.

## Step 5: Build the External Libraries

First make sure you have `cmake` installed,
because it's used by the build scripts of METIS and CoolProp.
On `cartesius.surfsara.nl` you can do

```bash
module load CMake/3.12.1-GCCcore-7.3.0
```

I ignored the complaints about not being able to load (older) versions of the compilers.

Then run all the build scripts (can take a long time):

```bash
cd install/
nohup sh -c '
    set -e
    ./build_petsc.sh
    ./build_coolprop.sh

    ./build_metis.sh &
    ./build_orthofun.sh &
    ./build_gmsh.sh &

    ./build_fobis.sh
    ./build_fortran_parser.sh
    ./build_pyplotfortran.sh
' &> BUILD_ALL &
```

Some external build processes
(`build_pyplotfortran.sh`, `build_fortran_parser.sh`)
will complain a bit about not being able to run the tests,
which is safely ignored.

## Step 6: Compile DGFlows

```bash
./list_deps.sh
make -j mode=0 ipo=0 nocheck=0 &
make -j mode=1 ipo=0 nocheck=0 &
make -j mode=1 ipo=1 nocheck=0 &
make -j mode=1 ipo=1 nocheck=1 &
```

The binaries are in `build/<arch>/bin/`.

## Miscellaneous Notes on Compilation

### Finding the right link and compile flags for MKL

If you're lucky then `pkg-config` will know about MKL,
so you can browse through the output of `pkg-config --list-all`
to find the MKL lib you want to link (e.g., `mkl-static-lp64-seq`).
If it functions properly, then you can add
`pkg-config --libs mkl-static-lp64-seq` to the link flags, and
`pkg-config --cflags mkl-static-lp64-seq` to the compile flags.

Otherwise, use the
[Intel link advisor](https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor/).
If you're using the GNU compilers,
then the `F95ROOT` variable should be the install directory (`_MKL_F95ROOT` in these notes).

### Installing without 'wget'

Some clusters have no internet access,
and somehow it's not easy to rout `wget` through a proxy.
This was the case on the HazelHen HLRS cluster in Stuttgart.
(Possibly it does not support SOCKS4/SOCKS5 from ssh tunneling?).
Assuming you can still use `git clone`, you can provide the files yourself:

1. Before running the build scripts above, run

    ```bash
    ./download_all.sh
    ```

    on a machine with internet access.

1. Copy the `downloads/` directory to `dgflows/install/downloads/` on the machine without wget.

1. Then run the build scripts.

## Profiling with Intel

We profiled the code on the `cartesius.surfsara.nl` cluster
with the Intel2019 compilers.
See the [Intel website](https://www.nas.nasa.gov/hecc/support/kb/Finding-Hotspots-in-Your-Code-with-the-Intel-Vtune-Command-Line-Interface_506.html)
and also a [NASA website](https://www.nas.nasa.gov/hecc/support/kb/Finding-Hotspots-in-Your-Code-with-the-Intel-Vtune-Command-Line-Interface_506.html).

1.
    Compile the code with the `-g` flag.
    (You should really always do this.)

1.
    Submit a batch script with `sbatch` as usual.
    Within script you call the MPI program with

    ```bash
    module load VTune/2019_update6
    srun amplxe-cl -collect hotspots -result-dir ./r000hs ./dgflows
    ```

    Each node will create a directory of the form `r000hs.tcn`*X*`.bullx`,
    where *X* is some number.
    Use the GUI to analyse the raw results:

    ```bash
    amplxe-gui r000hs.tcn552.bullx/ 2>/dev/null &
    ```

    While analyzing the results, VTune will complain that
    *'The current result was collected on a another host.
    For proper symbol resolution, please specify search directories for the binaries of interest
    using the `-search-dir` option or *"Binary/Symbol Search"* dialog in GUI.'*

    Resolve this by going to *Analysis Configuration* --> *Search Sources Binaries*,
    and adding some paths.
    The most important is

    ```bash
    /path/to/my/dgflows
    ```

    Include other stuff for more details on PETSc and such:

    ```bash
    /home/ahennink/software/petsc-3.10.4/arch-linux2-c-mpi-opt/lib
    /usr/lib64
    /sw/arch/RedHatEnterpriseServer7/EB_production/2019/software/imkl/2019.1.144-iimpi-2019a/mkl/lib/intel64/
    /sw/arch/RedHatEnterpriseServer7/EB_production/2019/software/impi/2018.4.274-iccifort-2019.1.144-GCC-8.2.0-2.31.1/intel64/lib
    ```

    I found the last two directories with
    `${MKLROOT}/2019.1.144-iimpi-2019a/mkl` and
    `${I_MPI_ROOT}/intel64/lib`.
    Then click *re-resolve*.

## Debugging with Intel

Intel has a special fork of `gdb` for their compilers,
called `gdb-ia`.
The use is identical.
You have to source the `compilervars.sh` script for this
(see above); the module system on the TUD clusters does not do this.

See [here](https://software.intel.com/en-us/psxe-get-started-with-debugging-linux).
