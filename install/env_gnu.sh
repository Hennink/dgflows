CC=gcc
CXX=g++
FC=gfortran
MPICC=mpicc
MPICXX=mpicxx
MPIFC=mpifort

_MKL_F95ROOT=${HOME}/mkl_gnu_f95root
MKL_CFLAGS="-I${_MKL_F95ROOT}/include/intel64/lp64 -m64 -I${MKLROOT}/include"
MKL_LFLAGS="${_MKL_F95ROOT}/lib/intel64/libmkl_blas95_lp64.a ${_MKL_F95ROOT}/lib/intel64/libmkl_lapack95_lp64.a -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_gf_lp64.a ${MKLROOT}/lib/intel64/libmkl_sequential.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm -ldl"

CPP_LIB_LFLAG=" -lstdc++ -ldl -lgfortran -lm " # -L/usr/lib/gcc/x86_64-linux-gnu/9/ 
DGF_FLAG_MODDIR="-J"
DGF_FLAG_PP=" -cpp "
DGF_FLAG_IPO="-flto"

## General basic flags:
_DGF_FLAGS_BASIC=" -I." # Unlike `ifort`, `gfortran` does not include files in the currect directory by default.
_DGF_FLAGS_BASIC+=" -g  -fbacktrace  -std=f2018 -pedantic "
_DGF_FLAGS_BASIC+=" -Werror"
_DGF_FLAGS_BASIC+=" -Wall -Wextra"
_DGF_FLAGS_BASIC+=" -Werror=shadow -Werror=intrinsic-shadow" # warn whenever a local variable shadows another local variable, parameter or global variable or whenever a built-in function is shadowed.
_DGF_FLAGS_BASIC+=" -Wuninitialized"
_DGF_FLAGS_BASIC+=" -Waggregate-return" # warn if any functions that return structures or unions are defined or called.
_DGF_FLAGS_BASIC+=" -Warray-bounds" # 
# These are possibly verbose:
_DGF_FLAGS_BASIC+=" -Wunreachable-code" # warn if the compiler detects that code will never be executed
_DGF_FLAGS_BASIC+=" -Wconversion" # warn for implicit conversions that may alter a value
_DGF_FLAGS_BASIC+=" -Wstrict-overflow=5" # warns about cases where the compiler optimizes based on the assumption that signed overflow does not occur. (The value 5 may be too strict, see the manual page.)
# " -fmudflap" is no longer supported. Adds runtime checks to all risky pointer operations to catch UB. This effectively immunizes your program again buffer overflows and helps to catch all kinds of dangling pointers

_DGF_FLAGS_BASIC+=" -Wno-maybe-uninitialized" # too many false positives for automatically allocated chars
_DGF_FLAGS_BASIC+=" -Wno-aggregate-return" # Why would returning a derived type be bad?
_DGF_FLAGS_BASIC+=" -Wno-unused-dummy-argument -Wno-error=return-type" # needed for polymorphism
# The following would ideally not be necessary if we were never sloppy:
_DGF_FLAGS_BASIC+=" -Wno-error=compare-reals" # done very occasionally
_DGF_FLAGS_BASIC+=" -Wno-error=unused-function -Wno-error=unused-parameter"


## Fortran basic flags:
_DGF_FLAGS_BASIC_F=${_DGF_FLAGS_BASIC}
_DGF_FLAGS_BASIC_F+=" -Waliasing" # Warn about possible aliasing of dummy arguments. Specifically, it warns if the same actual argument is associated with a dummy argument with INTENT(IN) and a dummy argument with INTENT(OUT) in a call with an explicit interface.
_DGF_FLAGS_BASIC_F+=" -Wampersand" # Warn about missing ampersand in continued character constants
_DGF_FLAGS_BASIC_F+=" -Wc-binding-type" # Warn if the a variable might not be C interoperable. Implied by -Wall
_DGF_FLAGS_BASIC_F+=" -Wcharacter-truncation" # Warn when a character assignment will truncate the assigned string. 
_DGF_FLAGS_BASIC_F+=" -Wconversion" # Implied by -Wall
_DGF_FLAGS_BASIC_F+=" -Wdo-subscript" # implied by -Wextra
_DGF_FLAGS_BASIC_F+=" -Wfunction-elimination" # Warn if any calls to impure functions are eliminated by the optimizations enabled by the -ffrontend-optimize option. This option is implied by -Wextra. 
_DGF_FLAGS_BASIC_F+=" -Wimplicit-interface" # Warn if a procedure is called without an explicit interface. Note this only checks that an explicit interface is present. It does not check that the declared interfaces are consistent across program units. 
_DGF_FLAGS_BASIC_F+=" -Wimplicit-procedure" # Warn if a procedure is called that has neither an explicit interface nor has been declared as EXTERNAL. 
_DGF_FLAGS_BASIC_F+=" -Wintrinsic-shadow" # implied by -Wall. 
_DGF_FLAGS_BASIC_F+=" -Wintrinsics-std" # Warn if gfortran finds a procedure named like an intrinsic not available in the currently selected standard (with -std) and treats it as EXTERNAL procedure because of this.
_DGF_FLAGS_BASIC_F+=" -Wline-truncation" # 
_DGF_FLAGS_BASIC_F+=" -Wno-tabs" #  tabs are not members of the Fortran Character Set
_DGF_FLAGS_BASIC_F+=" -Wreal-q-constant" # Produce a warning if a real-literal-constant contains a q exponent-letter. 
_DGF_FLAGS_BASIC_F+=" -Wsurprising" # Produce a warning when “suspicious” code constructs are encountered. 
_DGF_FLAGS_BASIC_F+=" -Wunderflow" # Produce a warning when numerical constant expressions yield an UNDERFLOW during compilation
_DGF_FLAGS_BASIC_F+=" -Wunused-parameter" # Contrary to gcc’s meaning of -Wunused-parameter, gfortran’s implementation of this option does not warn about unused dummy arguments, but about unused PARAMETER values.
_DGF_FLAGS_BASIC_F+=" -Wfrontend-loop-interchange" # Warn when using -ffrontend-loop-interchange for performing loop interchanges. 
_DGF_FLAGS_BASIC_F+=" -Wtarget-lifetime" # Warn if the pointer in a pointer assignment might be longer than the its target. This option is implied by -Wall. 
_DGF_FLAGS_BASIC_F+=" " # 
#  -Wuse-without-only
# -Wno-align-commons
# -Wrealloc-lhs  -Wrealloc-lhs-all
# -pedantic-errors

## C/C++ basic flags:
_DGF_FLAGS_BASIC_C=${_DGF_FLAGS_BASIC}
_DGF_FLAGS_BASIC_C+=" -Wundef" # warn if an uninitialized identifier is evaluated in an #if directive
_DGF_FLAGS_BASIC_C+=" -Winit-self" # not included in -Wuninitialized
_DGF_FLAGS_BASIC_C+=" -Werror-implicit-function-declaration" # Give a error whenever a function is used before being declared
_DGF_FLAGS_BASIC_C+=" -Wfloat-equal" # useful because usually testing floating-point numbers for equality is bad
_DGF_FLAGS_BASIC_C+=" -Wpointer-arith" # warn if anything depends upon the size of a function or of void
_DGF_FLAGS_BASIC_C+=" -Wmissing-prototypes"
_DGF_FLAGS_BASIC_C+=" -Wstrict-prototypes" # warn if a function is declared or defined without specifying the argument types
_DGF_FLAGS_BASIC_C+=" -Wwrite-strings" # give string constants the type const char[length] so that copying the address of one into a non-const char * pointer will get a warning.
_DGF_FLAGS_BASIC_C+=" -Wcast-align" # warn whenever a pointer is cast such that the required alignment of the target is increased. For example, warn if a char * is cast to an int * on machines where integers can only be accessed at two- or four-byte boundaries.
_DGF_FLAGS_BASIC_C+=" -Wswitch-default" #  warn whenever a switch statement does not have a default case
_DGF_FLAGS_BASIC_C+=" -Wswitch-enum" # warn whenever a switch statement has an index of enumerated type and lacks a case for one or more of the named codes of that enumeration
_DGF_FLAGS_BASIC_C+=" -Wformat=2" # Extra format checks on printf/scanf functions
_DGF_FLAGS_BASIC_C+=" -Wcast-qual" # warn whenever a pointer is cast to remove a type qualifier from the target type

## Runtime checks:
_DGF_FLAGS_RUNTIME_CHECKS="-O0"
_DGF_FLAGS_RUNTIME_CHECKS+=" -ftrapv" # abort on signed integer overflow (formally "undefined behaviour" in C)
_DGF_FLAGS_RUNTIME_CHECKS+=" -fsanitize-address-use-after-scope" # 
_DGF_FLAGS_RUNTIME_CHECKS+=" -ffpe-trap=invalid,zero,overflow" # Floating Point Exceptions. Adding 'inexact' is likely uninteresting.
_DGF_FLAGS_RUNTIME_CHECKS+=" -fcheck=all"
_DGF_FLAGS_RUNTIME_CHECKS+=" -finit-real=snan  -finit-integer=9999 -finit-character=36  -finit-derived" # 36 is "$" in the ASCII char set

## Optim flags:
_DGF_FLAGS_OPTIM="-O2"
# _DGF_FLAGS_OPTIM+=" -fexternal-blas" # ERRORS with "f951: internal compiler error: in gfc_enforce_clean_symbol_state, at fortran/symbol.c:4273"
_DGF_FLAGS_OPTIM+=" -march=native" # optimizes for current hardware. Probably not good for clusters with job schedulers.
_DGF_FLAGS_OPTIM+=" -faggressive-function-elimination -Wno-error=function-elimination" # not evaluating impure functions, or not repeating call with same args, etc
_DGF_FLAGS_OPTIM+=" -fno-protect-parens" # 
# Turn off some pedantry that only appears with optimized code:
_DGF_FLAGS_OPTIM+=" -Wno-strict-overflow"
_DGF_FLAGS_OPTIM+=" -Wno-array-bounds -Wno-uninitialized" # too many false positives with allocatable chars

# Full compile flags:
DGF_FLAGS_DEVEL_C="${_DGF_FLAGS_BASIC_C}    ${_DGF_FLAGS_RUNTIME_CHECKS}"
DGF_FLAGS_DEVEL_F="${_DGF_FLAGS_BASIC_F}    ${_DGF_FLAGS_RUNTIME_CHECKS}"
DGF_FLAGS_RELEASE_C="${_DGF_FLAGS_BASIC_C}  ${_DGF_FLAGS_OPTIM}"
DGF_FLAGS_RELEASE_F="${_DGF_FLAGS_BASIC_F}  ${_DGF_FLAGS_OPTIM}"
