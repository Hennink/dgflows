#!/usr/bin/env bash
set -e

mode=1
ipo=0
nocheck=0
max_np=5
prefix=OUT
# prefix=$(git rev-parse --short HEAD)

ncores=$(nproc --all) # not POSIX, but portable enough.  https://stackoverflow.com/q/6481005
np=$(( $max_np > $ncores ? $ncores : $max_np ))

petsc_opts=()
for q in m p h H rhop WD; do
    petsc_opts+=("-${q}_ksp_rtol" '1.0e-11')
    petsc_opts+=("-${q}_ksp_atol" '1.0e-30')
    petsc_opts+=("-${q}_ksp_max_it" '500')
    petsc_opts+=("-${q}_ksp_norm_type" 'unpreconditioned') # to compare codes with different preconditioners
done

REF_TESTS=(
    input_files/mansols/p3_3D/
    input_files/mansols/tg_vortex_2D/
    input_files/outflow_symmetry_bc_2d_pipe/
    input_files/buoyancy_driven_cavity/
    input_files/backward_facing_step/
    input_files/liddriven_cavity/
    input_files/CHT_with_flow/
    input_files/periodic/
    input_files/pipe/
    input_files/plane_channel/
    input_files/wall_distance/
);

REF_FS_TESTS=(
    input_files/buoyancy_driven_cavity_fs/
);

arch=mode${mode}ipo${ipo}nocheck${nocheck}/
bin=build/${arch}/bin/dgflows
make -j hardcode_dimen=0 mode=${mode} ipo=${ipo} nocheck=${nocheck}

my_dir=$(pwd)
for case_dir in "${REF_TESTS[@]}";
do
    echo ${case_dir}
    cp $bin $case_dir
    cd $case_dir
    mpiexec --bind-to none -n $np \
            ./dgflows "${petsc_opts[@]}" \
            &> ${my_dir}/${prefix}_$(basename ${case_dir})
    cat fort.11 *.pos > ALL.msh
    cd $my_dir
done

# If you want to run cases with coupled m-p (fieldsplit), see following example for the run-time options:
# Also, set use_approx_schur_KSP = T in the INPUT file.
# Use -pc_fieldsplit_schur_precondition selfp to use the SIMPLE-type approx Schur complement, as explained by Klaij and Vuik
for case_dir in "${REF_FS_TESTS[@]}";
do
    echo ${case_dir}
    cp $bin $case_dir
    cd $case_dir
    capped_n=$(( $np >= 2 ? $np : 2))
    mpiexec --bind-to none -n ${np} \
            ./dgflows "${petsc_opts[@]}" -pc_fieldsplit_type schur -pc_fieldsplit_schur_fact_type full -fieldsplit_m_ksp_rtol 1e-2 -fieldsplit_p_ksp_rtol 1e-2 \
            &> ${my_dir}/${prefix}_$(basename ${case_dir})
    cat fort.11 *.pos > ALL.msh
    cd $my_dir
done

echo "All tests completed successfully"
