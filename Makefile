SHELL := /bin/bash

mode = 0
ipo = 0
nocheck = 0
hardcode_dimen = $(nocheck)

SRC_DIR = src/
TOP_DIR = build/
ARCH = mode${mode}ipo${ipo}nocheck${nocheck}/
BUILD_DIR = $(TOP_DIR)/$(ARCH)
OBJS_DIR = $(BUILD_DIR)obj/
MODS_DIR = $(BUILD_DIR)mod/
EXE = $(BUILD_DIR)bin/dgflows

all : $(EXE)


######  Build the documentation:  ######
readme=install/README
docs=$(shell echo $(readme).{pdf,html,txt})
$(docs): $(readme).md
	@if command -v pandoc &>/dev/null ; then pandoc -V geometry:landscape -V linkcolor:blue $< -o $@ ; echo '$@' ; fi
all: $(docs)


######  Compile and link the DGF modules:  ######
f90_files=$(wildcard $(SRC_DIR)/*.f90)
OBJ_NAMES=$(patsubst %.f90,%.o,$(notdir $(f90_files)))
OBJS = $(addprefix $(OBJS_DIR), $(OBJ_NAMES))

$(EXE): $(OBJ_NAMES)
	@mkdir -p $(dir $@) \
    && $(MY_FC) -o $@ $(OBJS) $(EXTERNAL_OBJECTS) $(LFLAGS) \
        && echo "executable: $@"

$(OBJ_NAMES):
	@mkdir -p $(OBJS_DIR) $(MODS_DIR) \
    && $(MY_FC) $(FFLAGS) -c $(SRC_DIR)$(@:.o=.f90) -o $(OBJS_DIR)$@ ${DGF_FLAG_MODDIR}$(MODS_DIR)  \
        &&  echo "$(OBJS_DIR)$@"

# An external script builds a list of Fortran module dependencies. This should
# technically be redone after every change in USE statements, but that is slow
# and often not needed.
# 
# Do not include the full $(OBJS_DIR) path in the dependency list, because it
# changes with every build.
VPATH = $(OBJS_DIR)
include $(SRC_DIR)/dependencies
$(SRC_DIR)/dependencies: list_deps.sh
	./list_deps.sh



######  Get compiler commands and standard flags:  ######
# Crude parsing before including Bash script with environment variables:
# (Taken from https://blog.153.io/2016/04/18/source-a-shell-script-in-make/ )
# Not very robust, but works for simple variable listing.
include install/env.mk
install/env.mk: install/env.sh
	sed 's/"//g' < $< > $@

MY_FC = ${MPIFC}

# main mode:
ifeq ($(mode), 0)
    FFLAGS = $(DGF_FLAGS_DEVEL_F)
else ifeq ($(mode), 1)
    FFLAGS = $(DGF_FLAGS_RELEASE_F)
else ifeq ($(mode), 2)
    # Adds a profile flag:
    FFLAGS = -p $(DGF_FLAGS_RELEASE_F)
else
    $(error bad mode)
endif

# inter-procedural optimization:
ifeq ($(ipo), 0)
else ifeq ($(ipo), 1)
    ifeq (${FC}, ifort)
        OBJS_WITH_IPO = array_opers_mod.o assertions.o avg_over_elem_groups_mod.o binary_tree.o bnd_t_periodic_mod.o boundaries.o boundary_condition_types.o boundary_types.o boundary_types__fixed_velocity.o boundary_types__noslip_fixedQ_wall.o boundary_types__noslip_fixedT_wall.o boundary_types__noslip_htc_wall.o boundary_types__noslip_internal_wall.o boundary_types__noslip_isolated_wall.o boundary_types__outflow.o boundary_types__solid_fixedQ.o boundary_types__solid_fixedT.o boundary_types__solid_htc.o boundary_types__solid_isolated.o boundary_types_support_mod.o boundary_types__symmetry.o boundary_types__wall_func_adiabatic.o boundary_types__wall_func_fixedQ.o boundary_types__wall_func_fixedT.o characteristic_len_mod.o code_const.o convection.o coolprop_mod.o coolprop_with_fixed_qnty_mod.o csr_format.o curl_mod.o density_mod.o density_weighted_time_mod.o div_cont_penalties_and_err.o divergence_mod.o dof_handler.o dyad_axpy_mod.o dyad_mod.o dyad_of_scalfuncs_mod.o dyad_of_vecfuncs_mod.o dyad_of_vec_scal_funcs_mod.o dyad_product_mod.o dyad_ratio_mod.o dyn_smag_visc_mod.o eqv_elem_groups_mod.o exceptions.o f90_kind.o face_integrals_mod.o fem.o fick_sip_mod.o fieldsplit_mod.o first_order_reaction_mod.o fortran_parser_interface_mod.o fortran_parser_mod.o func_link_mod.o func_operations_mod.o func_ptr2monad_mod.o func_set_expansion_mod.o func_set_mod.o functionals.o functions_lib.o galerkin_projection.o gmsh_interface.o gsip.o gsip_viscous_stress_mod.o initial_condition.o interface_continuity_mod.o io_basics.o iter_manager_mod.o kahan_sum_mod.o lapack_interface_mod.o les_const_eddy_visc_mod.o les_eddy_viscosity_mod.o les_heat_flux_mod.o local_elem.o local_global_mappings.o local_integrals.o local_elem__orthofun.o local_elem__old__quadrature.o local_linsys_mod.o main.o mansol_mod.o mass_flux_solver_mod.o mass_mtx.o math_const.o matprop__init.o matprop__lib_boussinesq.o matprop__lib_cp.o matprop__lib_linear.o matprop_lib_mansol.o matprop__lib_metal.o matprop__lib_MSFR_salt.o matprop_local2global_mod.o matprop_mod.o matprop_scalfunc_mod.o mesh_objects__active_subfaces.o mesh_objects.o mesh_partitioning.o metis_interfaces.o mesh_type.o monad_dyad_lib_mod.o monad_mod.o mpi_wrappers_mod.o mtx_block_mod.o monad_of_scalfunc_mod.o numerical_solutions.o pde_mod.o pde_term_mod.o penalty_param_mod.o pnt_set_mod.o pprint_mod.o qnty_handler_mod.o pressure.o qnty_solver_mod.o rel_grad_of_scalfunc_mod.o scalfunc_from_proc_mod.o scalfunc_gcors_mod.o scal_func_masked_mod.o scal_func_mod.o support.o time_der_mod.o vec_func_global_coord_mod.o vec_func_mod.o vec_func_scal_compon_mod.o
        # known_not_to_work_with_ipo = les_dyn_turb_pr_mod.o lincom_rank1_mod.o lincom_rank2_mod.o local_elem__elem_face_coupling.o local_elem__old.o mesh_refinement.o multiphysics_mod.o petsc_basic_types_mod.o petsc_is_mod.o petsc_ksp_mod.o petsc_mat_mod.o petsc_matnullspace_mod.o petsc_mod.o petsc_pc_mod.o petsc_solver_mod.o petsc_vec_mod.o phys_time_mod.o pressure_corr.o rans_mod.o rans_mod__k_eps.o rans_mod__k.o rans_mod__k_log_w.o rans_mod__k_omega.o rans_mod__log_k_log_eps.o rans_mod__prandtl.o rans_mod__SA.o rans_wall_functions_mod.o rans_y_plus_mod.o quantities_mod.o solution_refinement.o solvec_mod.o solvec_as_scalfunc_mod.o solvec_as_vecfunc_mod.o sources_mod.o source_pde_term_mod.o time_step_mod.o unidir_lincom_rank0_mod.o vec_block_mod.o vol_averages_mod.o
        # not_tested_because_not_performance_critical = read_gmsh_mesh_mod.o read_funcs_mod.o run_data.o sort.o spec_enthalpy_solver_mod.o spec_h_implicit_time_mod.o spline_mod.o stat_enthalpy_solver_mod.o stokes_solver_mod.o string_manipulations.o string_mod.o system_commands.o timer_typedef.o turb_quantities.o tvd_mod.o wall_dist_mod.o wall_dist_solver_mod.o vol_enthalpy_solver_mod.o
        $(OBJS_WITH_IPO): FFLAGS += ${DGF_FLAG_IPO}
    else ifeq (${FC}, gfortran)
        FFLAGS += ${DGF_FLAG_IPO}
        # GNU problems with IPO:
        local_elem__old.o local_elem__elem_face_coupling.o: FFLAGS := $(filter-out -flto,$(FFLAGS)) # "internal compiler error: in write_symbol, at lto-streamer-out.c:2657"
        sources_mod.o run_data.o:   FFLAGS := $(filter-out -flto,$(FFLAGS)) # "error: type of '_gfortran_pack_char' does not match original declaration [-Werror=lto-type-mismatch]"
    else
        $(error unknown FC)
    endif
else 
    $(error bad ipo)
endif

# auto-generated code:
support.o: utils/trace_powers.f90
utils/trace_powers.f90: utils/codegen_trace_powers.py
	./$< > $@

# assertions:
assertions.o: FFLAGS += ${DGF_FLAG_PP}
assertions.o: FFLAGS += -DASSERTIONS  # Comment out this line to disable all assertions.
ifeq ($(nocheck), 0)
    assertions.o: FFLAGS += -DINTERNAL_BOUNDS_CHECKS
else ifeq ($(nocheck), 1)
    # https://software.intel.com/content/www/us/en/develop/articles/intel-math-kernel-library-improved-small-matrix-performance-using-just-in-time-jit-code.html
    FFLAGS += -DMKL_DIRECT_CALL_SEQ_JIT
else
    $(error bad nocheck)
endif

ifeq ($(hardcode_dimen), 0)
else ifeq ($(hardcode_dimen), 1)
    FFLAGS += -DHARDCODE_DIMEN
else 
    $(error bad hardcode_dimen)
endif


######  Special cases where FFLAGS need to be adjusted:  ######
exceptions.o func_link_mod.o func_set_mod.o run_data.o: FFLAGS += ${DGF_FLAG_PP}

ifeq (${FC}, ifort)
    vol_averages_mod.o: FFLAGS += -warn nostderrors # Allow for non-standard preprocessor directives
    main.o: FFLAGS += -warn nostderrors # bug in the compiler about `compiler_version` and `compiler_options` not being in F2018
endif

# Add macro's for program version info:
# https://stackoverflow.com/a/44038455/5930169
VERSION_FLAGS =                                                      \
    -DGIT_HASH="\"$(shell git rev-parse HEAD)\""                     \
    -DGIT_BRANCH="\"$(shell git branch | sed -n 's/^[*][ ]//p')\""   \
    -DCOMPILE_TIME="\"$(shell date -u +'%Y-%m-%d %H:%M:%S UTC')\""
main.o: FFLAGS += ${DGF_FLAG_PP} $(VERSION_FLAGS)

######  External libraries: adjust the flags and build external object list:  ######
LFLAGS = $(FFLAGS)
EXTERNAL_OBJECTS = 

ORTHOFUN_DIR = install/orthofun/
ifeq ($(mode), 0)
    ORTHOFUN_ARCH = devel/
else ifeq ($(mode), 1)
    ORTHOFUN_ARCH = release/
else ifeq ($(mode), 2)
    ORTHOFUN_ARCH = release/
else
    $(error bad mode)
endif
avg_over_elem_groups_mod.o local_elem__orthofun.o: FFLAGS += -I$(ORTHOFUN_DIR)/$(ORTHOFUN_ARCH)
EXTERNAL_OBJECTS += $(ORTHOFUN_DIR)/$(ORTHOFUN_ARCH)/*.o

# Link the C++ standard library with CoolProp:
# http://www.coolprop.org/coolprop/wrappers/FORTRAN/index.html
COOLPROP_DIR = $$HOME/software/CoolProp/build/arch-static/
EXTERNAL_OBJECTS += $(COOLPROP_DIR)/libCoolProp.a 
LFLAGS += ${CPP_LIB_LFLAG}

FORTRANPARSER_ROOT = $$HOME/software/FortParLib/
EXTERNAL_OBJECTS += $(FORTRANPARSER_ROOT)/obj/*.o
fortran_parser_interface_mod.o: FFLAGS += -I$(FORTRANPARSER_ROOT)/mod/

PYPLOT_FORTRAN_LIB = $$HOME/software/pyplot-fortran/lib/
EXTERNAL_OBJECTS += $(PYPLOT_FORTRAN_LIB)/*.o
spline_mod.o: FFLAGS += -I$(PYPLOT_FORTRAN_LIB)

PETSC_DIR = ${HOME}/software/petsc/
ifeq ($(mode), 0)
    PETSC_ARCH = arch-devel
else ifeq ($(ipo), 0)
    PETSC_ARCH = arch-optim
else
    PETSC_ARCH = arch-optim-ipo
endif
PETSC_PC_FILE=$(PETSC_DIR)/$(PETSC_ARCH)/lib/pkgconfig/PETSc.pc
petsc%.o: FFLAGS += $(shell pkg-config --cflags ${PETSC_PC_FILE})
petsc%.o: FFLAGS += ${DGF_FLAG_PP} # PETSc pkg-config does not specify the preprocessor flag.
ifeq (${FC}, ifort)
    petsc%.o: FFLAGS += -warn nostderrors # because PETSc uses obsoleted `CHARACTER*` declarations
endif
LFLAGS += $(shell pkg-config --libs ${PETSC_PC_FILE})
LFLAGS += -Wl,-rpath,${PETSC_DIR}/${PETSC_ARCH}/lib/  # pkg-config does not manage the rpath, so pass it to the linker manually

METIS_DIR=$$HOME/software/metis-5.1.0/
METIS_ARCH=arch-debug
LFLAGS += -L${METIS_DIR}/${METIS_ARCH}/lib/ -lmetis

# dgf_mkl_obj = avg_over_elem_groups_mod.o lapack_interface_mod.o mesh_objects.o
# $(dgf_mkl_obj): FFLAGS += ${MKL_CFLAGS}
FFLAGS += ${MKL_CFLAGS}
LFLAGS += ${MKL_LFLAGS}
