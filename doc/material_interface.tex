\documentclass{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{stmaryrd}

\newcommand{\jump}[1]{\left\llbracket #1 \right\rrbracket}
\newcommand{\avg}[1]{\left\{ #1 \right\}}

\newcommand{\MatIF}{\mathcal{F}^\text{m}_\ell}
\newcommand{\DiffConst}{\tilde{\alpha}}

\title{Enthalpy discretization at material interface}
\author{NERA people}
\date{}


\begin{document} 
\maketitle

\section{Problem:}
The governing equation is
\begin{align*}
     \nabla \cdot (- \lambda \nabla T) = f
     &&& \text{on $\Omega$,}
\end{align*}
so $T$ is continuous everywhere. We want to solve for the volumetric enthalpy $H$ (J/m$^3$). Let $\{\Omega_i\}$ be the regions with various material properties, so $\cup_i\Omega_i=\Omega$. Now the pde's are
\begin{align*}
  \nabla \cdot \mathbf{q}(H)
  & = \mathbf{f}
  && \text{on $\Omega_i \ \forall i$,}
  \\
  \mathbf{q}(H)
  & \coloneqq
  - \frac
    {\lambda}
    {\partial H / \partial T}
  \nabla H
  \ \text{,}
\end{align*}
$H$ is discontinuous over faces where the material
properties change. We provide boundary conditions by enforcing continuity of the heat flux at the interfaces between the materials. There is always a mesh face at an element interface, so we simply require that the heat flux be continuous across all faces in the mesh:
\begin{align} \label{eq:continuous_heat_flux}
  \jump{\mathbf{q}(H)} = \mathbf{0}
  & & & \text{for all faces.}
  \\
  \label{eq:continuous_temperature}
  \jump{T} = 0
  & & & \text{for all faces.}
\end{align}
What is the correct discretization?

\section{A solution: SIP for $H$}
\subsection{Enforcing a continuous heat flux in the weak form}
We adjust the SIP form by rederiving it for all faces, including material
interfaces. Let $\MatIF$ be the set of all material interfaces. Now we follow
chapter 4 of Di Pietro's book, up to and including page 123. The
weak form is based on  
\begin{align} \label{eq:weak_form}
  a(H,w) = \int_\Omega f H
  \ \text{.}
\end{align}
A consistent discretization is
\begin{align} \label{eq:bilinear_operator_consistent}
  a^1(v,w) =
  - \sum_{E\in\mathcal{E}_\ell}
  \int_E \mathbf{q}(v) \cdot \nabla w
  + \sum_{F\in\mathcal{F}_\ell}  
  \int_F \mathbf{n}_F \cdot \avg{\mathbf{q}(v)} \jump{w}
  \ \text{,}
\end{align}
where $\mathcal{E}_\ell$ is the mesh (i.e.: the set of all elements) and
$\mathcal{F}_\ell$ is the set of all faces. We need equation
\ref{eq:continuous_heat_flux} to prove that equation
\ref{eq:bilinear_operator_consistent} satisfies \ref{eq:weak_form}. For the
other faces in the mesh, we now derive the full SIP form with symmetry and
penalty terms. These all contain a factor $\jump{H}$, so they don't hurt
continuity. For material interfaces, we cannot do this, so we get
\begin{align*}
  a^\text{SIP}(v,w)
  & =
  - \sum_{E\in\mathcal{E}_\ell}
  \int_E \mathbf{q}(v) \cdot \nabla w
  + \sum_{F\in\mathcal{F}_\ell}  
  \int_F \mathbf{n}_F \cdot \avg{\mathbf{q}(v)} \jump{w}
  \\
  & + \text{[usual symmetry/penalty for faces that are not in $\MatIF$]}
  \ \text{.}
\end{align*}

Note that the linear system is no longer symmetrical if the contributions of the
faces in $\MatIF$ are taken implicitly.

For material interfaces, we can still use equation \ref{eq:continuous_heat_flux}
to add terms that contain $\jump{\mathbf{q}(v)}$ to $a^\text{SIP}(v,w)$
without affecting consistency. Examples of terms that we could add to $a^\text{SIP}(v,w)$ are
\begin{align*}
  \sum_{F\in\MatIF} \int_F
  \mathbf{n}_F \cdot \jump{\mathbf{q}(v)} \avg{w}
\end{align*}
and 
\begin{align*}
  \sum_{F\in\MatIF} \int_F
  \eta
  \frac
    {\ell_F}
    {\DiffConst^2}
  \jump{\mathbf{q}(v)} \cdot \jump{\mathbf{q}(w)}
  \ \text{,}
\end{align*}
where $\DiffConst \coloneqq \dfrac{\lambda}{\partial H / \partial T}$ is the
diffusion constant. This last term could be interesting, because it penalizes
the jump in the heat flux. The scaling in the penalty factor is based on a dimensionless analysis, such that $\eta$ is dimensionless.

\subsection{Enforcing a continuous temperature}
The linear system that arrises from the SIP form in the previous section is still singular, because we have not yet used the fact that $\jump{T} = 0$, which is necessary to obtain a solution. If the material properties are continuous, $\jump{H} = 0$, which is also what we normally have, so the usual SIP form works fine. On material interfaces, $\jump{H} \neq 0$ in general.

We need to rewrite equation \ref{eq:continuous_temperature} in terms of $H$. This is complicated by the fact that there is generally no bijection between $T$ and $H$. We will instead linearize the dependency between $h$ and $T$. That is, if $(\cdot)^n$ is evaluated at the $n^\text{th}$ time step, then
\begin{align} \label{eq:linearize_h_in_T}
    h^{n+1} - h^n
    \approx
    c_p
    \left( T^{n+1} - T^n \right)
    \ \text{,} 
\end{align}
where the heat capacity $c_p \coloneqq \partial h / \partial T$ is taken explicitly somehow. Thus, for all $F\in\MatIF$,
\begin{align}
  \nonumber
  \jump{T^{n+1}}
  & = 0
  \\
  \label{eq:jump_in_H}
  \Rightarrow
  \jump{
      \frac{1}{\rho^{n+1} c_p}
      H^{n+1}
  }
  & = j^n
  \ \text{,}
\end{align}
where
\begin{align*}
    j^n
    = \jump{\frac{h^n}{c_p}}
    - \jump{T^n}
\end{align*}
is a known jump.

\emph{Remarks:}
\begin{enumerate}
    \item We're lucky with equation \ref{eq:jump_in_H}: we already have $\rho^{n+1}$ when we solve for $H^{n+1}$.
    \item We can take $c_p$ explicitly in two ways:
    \begin{itemize}
        \item $c_p = \left(c_p\right)^n$, which makes equations \ref{eq:linearize_h_in_T} and \ref{eq:jump_in_H} $\mathcal{O}\left(\delta t\right)$ accurate;
        \item $c_p = - \frac 1 2 \left(c_p\right)^{n-1} + \frac 3 2 \left(c_p\right)^n$, which is $\mathcal{O}\left(\delta t^2\right)$ accurate, but requires two time steps for $H$ to be stored. (We already need two time steps for $\rho$ anyway.)
    \end{itemize}
    \item In practice, $c_p$ often doesn't vary much with $T$, which could mean that equations \ref{eq:linearize_h_in_T} and (by extension) \ref{eq:jump_in_H} could be very accurate in practice, regardless of the formal order of the time scheme.
    
    Even for fluids where it's feasible, a linearization that fixes $\partial H / \partial T$ is likely less good. 
\end{enumerate}

Now the question is how to enforce equation \ref{eq:jump_in_H}. There are at least two possibilities.
\begin{enumerate}
    \item View equation \ref{eq:jump_in_H} as a sort of complementary pde. We add terms to the SIP bilinear and linear operators. There are two possibilities:
    \begin{align*}
        a^\text{SIP}(v,w)
        & \ \texttt{+=} \ 
        \sum_{F\in\MatIF} \int_F 
        \jump{
            \frac{1}{\rho^{n+1} c_p}
            v
        }
        \jump{w}
        \\
        b^\text{SIP}(w)
        & \ \texttt{+=} \ 
        \sum_{F\in\MatIF} \int_F
        j^n \jump{w}
    \end{align*}
    and 
    \begin{align*}
        a^\text{SIP}(v,w)
        & \ \texttt{+=} \ 
        \sum_{F\in\MatIF} \int_F 
        \jump{
            \frac{1}{\rho^{n+1} c_p}
            v
        } \avg{w}
        \\
        b^\text{SIP}(w)
        & \ \texttt{+=} \ 
        \sum_{F\in\MatIF} \int_F
        j^n \avg{w}
    \end{align*}
    Subsituting $v \leftarrow \jump{H^{n+1}}$, it seems that both are consistent, which does not mean that they yield the same numerical solution. 
    
    It might also be necessary to penalize the jump of the temperature at the material interface, which would be done with a $\int_F \jump{T}\jump{w}$ term. Perhaps we'll merge the penalty and consistency terms into
    \begin{align*}
        a^\text{SIP}(v,w)
        & \ \texttt{+=} \ 
        \sum_{F\in\MatIF} \int_F
        \eta_F 
        \jump{
            \frac{1}{\rho^{n+1} c_p}
            v
        }
        \jump{w}
        \\
        b^\text{SIP}(w)
        & \ \texttt{+=} \ 
        \sum_{F\in\MatIF} \int_F
        \eta_F
        j^n \jump{w}
    \end{align*}
    for some $\eta_F>0$.
    
    \item
    View equation \ref{eq:jump_in_H} as a constraint on the solution space. Enforce it in a `hard' way. That is, restrict the solution space to functions that adhere to equation \ref{eq:jump_in_H}. Suppose there are $M$ basis functions in both neighbors of $F\in\MatIF$. Two approaches:
    \begin{enumerate}
        \item
        Enforce equation \ref{eq:jump_in_H} at $M$ distinct points in $F$.
        
        \item
        Find a set $\{\phi_i\}$ of $M$ basis functions on the face $F$. Let
        \begin{align*}
            \int_F
            \jump{
                \frac{1}{\rho^{n+1} c_p}
            H^{n+1}
            }
            \phi_i
            & = \int_F j^n \phi_i
            &
            \text{for $i = 1 \ldots M$.}
        \end{align*}
        This looks very neat. If we cannot get \ref{eq:jump_in_H} everywhere on the face, then at least we'll get its first moments.
    \end{enumerate}
\end{enumerate}
All these options provide new equations can be added to the linear system to make it non-singular.


\section{Discretization in $T$}
We could also have discretized the system in terms of the temperature $T$. This would of course have been consistent and perfectly respectable (many authors do it). The discretization in terms of $T$ would have a bilinear form $b(\alpha,\beta)$. We would find an affine relation $T=f(H)$ and substitute $\alpha=f(u)$.

One problem is that the locally conservative DG property is lost on $T$. Another issue is that the numerical solution $u$ and the test function $\alpha$ are not in the same space (one is enthalpy, the other temperature). This means that, even if there is no material interface,
\begin{itemize}
    \item it is by definition not a Galerkin method;
    \item the linear system would be asymmetrical;
    \item it is not adjoint consistent.
\end{itemize}

\end{document}
