\documentclass[{a4paper,11pt}]{article}

\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}


%% IN ORDER TO WRITE IN Courier
\usepackage{courier}

%% TO SET PAGE MARGINS
\usepackage{geometry}
\usepackage{calc} %necessary to change units of measurement
\geometry{top=3.0cm,bottom=3.5cm,
	headheight=15pt,headsep=1.2cm-15pt\relax,
	footskip=1.4cm,left=2.8cm,right=2.8cm}

%% TO CHANGE TITLE'S STYLE
\usepackage{titlesec}

%% TO CUSTOMIZE THE INDENTATION
\usepackage{enumitem}

%% TO WRITE ALGORITHMS
\usepackage[boxed,linesnumbered,lined,commentsnumbered]{algorithm2e}

%% TO USE COLORS
\usepackage{color}

%% TO INSERT IMAGES
\usepackage{graphicx}
\usepackage{subfigure}  %to use subfigures (more than one figure with a single caption)

%% TO CUSTOMIZE CAPTIONS
\usepackage[font={footnotesize, md}, labelfont=md]{caption}
\captionsetup[table]{skip=5pt, font={md}} %in this way caption for tables is different

%% TO AUGMENT THE SPACE BETWEEN LINES: \setstretch{#} to set interline, \renewcommand{\arraystretch}{#} to set height of raws in a table
\usepackage{setspace}

%% TO INSERT AND CUSTOMIZE TABLES
\usepackage{bigstrut}
\usepackage{rotating}
\usepackage{multirow}
\usepackage{booktabs}
\usepackage{tabularx} %to have tables with adapting column width

%% TO CUSTOMIZE HEADERS AND FOOTERS
\usepackage{fancyhdr}

%% TO INSERT AND CUSTOMIZE MATHS FORMULAS
\usepackage{mathtools}
\usepackage{amsthm} %to insert theorems
\usepackage{amssymb}
\usepackage{textcomp}
\everymath{\displaystyle} %in this way all formulas are "nice"
\usepackage[makeroom]{cancel}
\usepackage{mathabx}
\usepackage[overload]{empheq} %to draw a box around formulas
\newcommand*{\widebox}[2][0.5em]{\fbox{\hspace{#1}$\displaystyle #2$\hspace{#1}}}
\usepackage{stmaryrd} % for nice double brackets

%% TO TYPESET TODO LISTS
\usepackage{todonotes}

%% TO TYPESET CHEMICAL FORMULAS 
\usepackage[version=4]{mhchem}

%% TO CHANGE THE BIB STYLE
\usepackage[round]{natbib}

%% TO CREATE HYPERLINKS AND BOOKMARKS
\usepackage{hyperref}
\usepackage{bookmark} %better to specify, it creates better results than hyperref

%\makeatletter
%\renewcommand\@seccntformat[1]{} %this command is used to suppress the numbers from section,
%\makeatother                     %with the possibility to have still bookmarks

\hypersetup{pdfborder={0 0 0},
	colorlinks=true,
	linkcolor=blue,    % color for refs \cref to fig, eq, tab.....
	citecolor=green,   % color for refs \cite to bibliography
	urlcolor=blue}
\urlstyle{same}


\title{Wall-functions approach for solid-wall BCs}
\author{Marco Tiberga}
\date{\today}

\begin{document}
	\maketitle
	
	In order to apply at walls the usual no-slip boundary condition for the velocity, plus Dirichlet BCs for the turbulence quantities (homogeneous or not, depending on the quantity considered) one should have an extremely fine mesh near the wall, due to the large gradients in the boundary layer, leading to a stiff and computationally expensive problem. Moreover, some modifications must be introduced in the $k-\epsilon$ model, in order to damp correctly the eddy viscosity (adopting a so called \textit{low-Re turbulence model}). 
	
	A popular approach to tackle this problem consists in avoiding solving the Navier-Stokes equations and the two for turbulence quantities in the region closest to the wall, basically removing a layer of width $\delta$ from the computational domain. 
	
	At $y=\delta$ ($y$ indicates the distance from the wall in the normal direction), boundary conditions based on experimental observations are applied and this constitutes the \textit{wall-functions} approach.
	
	\section{Boundary conditions for the momentum equation}
	Based on a quite extensive literature review on the subject, it can be said that, unfortunately, there is not a unique way in which wall-functions can be implemented in FEM codes. The main references for the present work are \citet{Hachem2009}, \citet{Ignat1998} and \citet{Kuzmin2007}, which constitute an extension of the approach originally proposed in \citet{Launder1974}.
	
	\bigskip \noindent
	In general, for the problem to be well-posed, it is necessary to specify one boundary condition for the normal direction and one/two (depending whether the domain is 2D/3D) for the tangential direction(s):
	
	\begin{description}
		\item[normal direction:] No-penetration (also called \textit{free-slip}) condition:
		\begin{equation}
			\mathbf{n}\cdot \mathbf{u} = \mathbf{0}
			\label{eq:normal_bc}
		\end{equation}
		where $\mathbf{u}$ is the velocity vector and $\mathbf{n}$ is the unit normal vector to the wall.
		
		\item[tangential direction(s):] Specified shear-stress:
		\begin{equation}
			\boldsymbol{\sigma}\cdot \mathbf{n}	- \mathbf{n}(\mathbf{n}\cdot \boldsymbol{\sigma}\cdot \mathbf{n}) = -\rho u_k u_{\tau} \frac{\mathbf{u}}{\left| \mathbf{u}\right| } = -\rho u_k \frac{\mathbf{u}}{u^+}
			\label{eq:tangential_bc}
		\end{equation}
		 where: $\boldsymbol{\sigma}$ is the stress tensor, $\rho$ is the fluid density at the wall, $ u_{\tau} = \left| \mathbf{u}\right|/u^+$ is the \textit{wall friction velocity}, while $u_k$ is a second velocity scale\footnote{Formally, the friction velocity is defined from the shear stress at the wall, $\tau_w$: $ u_{\tau} = \sqrt{\frac{\tau_w}{\rho}}$. This means condition \eqref{eq:tangential_bc} should be: \begin{equation*}
		 	\boldsymbol{\sigma}\cdot \mathbf{n}	- \mathbf{n}(\mathbf{n}\cdot \boldsymbol{\sigma}\cdot \mathbf{n}) = -\rho u_{\tau}^2 \frac{\mathbf{u}}{\left| \mathbf{u}\right| }
		 	\end{equation*}
		 Moreover, $ u_{\tau}$ should be the only velocity scale used to define normal distances in wall-units, boundary conditions for turbulent quantities and for heat flux. However, this leads to null wall-distances, turbulent kinetic energy and heat flux at stagnation or reattachment points, where the shear stress vanishes, in contradiction with experimental observations. To prevent this, it is necessary to introduce the second velocity scale $u_k$ \citep[see][for more details]{Ignat1998}.
		 } defined from the turbulent kinetic energy at the wall, $k$:
		\begin{equation}
			u_k = C_{\mu}^{1/4} \ k^{1/2} \ \ \text{or} \ \  	u_k = {\beta^*}^{1/4} \ k^{1/2} \qquad \left( C_{\mu} = \beta^* = 0.09 \right)  
		\end{equation}
		depending whether the $k-\epsilon$ or the $k-\omega$ model is used, respectively.
		Finally, $u^+$ is the velocity measured in the wall-units. From experiments, it was observed that its profile is universal for all wall-bounded flows: linear in the viscous sub-layer and logarithmic in the log-layer. Therefore, this variable is computed as:
		\begin{equation}
			u^+ = \begin{cases}
			    	y^+  \quad  &\text{for} \quad y^+<y_c^+ \\[5pt]
			    	\frac{1}{\kappa} \ln\left( Ey^+\right)   \quad & \text{for} \quad y^+>y_c^+
				  \end{cases}
		\label{eq:u_plus_profiles}
		\end{equation}
		where $y^+ = \frac{\rho u_k \delta }{\mu}$ is the normal distance measured in wall-units and $y_c^+ \approx 11.06$ is the value at which the linear and log profiles would match if there were not the buffer layer in between. $\mu$ is the fluid dynamic viscosity at the wall, $E \approx 9.0$ is a constant that is usually given for smooth walls but can be corrected to take into account the wall roughness (see next section), while $ \kappa $ is the von K\'arm\'an constant ($ \kappa \approx 0.41 $).
	\end{description}
	
	\subsubsection{Treatment of rough walls}
	\todo{Write how eq.\eqref{eq:u_plus_profiles} changes with rough walls}
	
	\smallskip
	
	\section{Boundary conditions for the turbulence equations}
	\begin{description}
		\item[turbulent kinetic energy, $k \ $:] an homogeneous Neumann bc is applied:
		\begin{equation}
			\mathbf{n} \cdot \nabla k = 0
		\end{equation}
		\item[turbulent dissipation, $\epsilon \ $:] a Dirichlet bc is applied:
		\begin{equation}
			\epsilon = \frac{u_k^3}{\kappa \delta}
		\end{equation}		
		\item[turbulent spec. dissipation, $\omega \ $:] a Dirichlet bc is applied \citep{Wilcox1993}:
		\begin{equation}
			\omega = \frac{u_k}{\sqrt{\beta^*}\kappa \delta}
		\end{equation}				
	\end{description}
	
	\bigskip
	
    \section{Implementation issues}
    Having described the theory related to wall-functions, it is necessary to do some reflections about their actual implementation in \texttt{DG-Flow}:
    \begin{enumerate}
    	\setlength{\itemsep}{0pt}
		\item the momentum equation is solved freezing the values of the turbulent kinetic energy $k$, therefore $u_k$ can be evaluated explicitly
		\item in order to easily evaluate $y^+$ and thus $u^+$ everywhere, one has to know the distance from the wall $\delta$ at which the BCs are imposed: its evaluation is the topic of section \ref{sec:distance_delta}. 
		\item boundary conditions for the turbulent quantities are trivial to implement and we already have in the code everything that is necessary
		\item the BCs for the momentum equation require instead more attention, being a mixture of Robin and Dirichlet BCs: they are the subject of section \ref{sec:implementation_momentum_bcs}.
	\end{enumerate}

	\subsection{Evaluation of the wall distance $\delta$}
	 \label{sec:distance_delta}
	 \citet{Hachem2009} chose to remove a layer of width $\delta$ (imposed) from the computational domain: conceptually, this is correct, since the boundary conditions must be imposed at a distance $\delta$ from the real wall. However, this approach is not satisfying, since it is too user-dependent: what is a suitable value for $\delta$ in complex domains, where the solution neither is known nor can be guessed?
	 
	 For this reason, following what is usually done for Finite Volumes, it was decided to correlate $\delta$ to the mesh size near the wall. In fact, this approach is also adopted in other FEM-based codes, like $\text{\texttt{COMSOL}}^{\text{\textcopyright}}$ and $\text{\texttt{ANSYS-CFX}}^{\text{\textcopyright}}$. There is no distinction between the real and the computational domain, which means the mesh points located at the wall are treated as if they were shifted in the normal direction by the distance $\delta$: since this distance is usually very small, compared to the domain size, it is reasonable to neglect it and the error will be small \citep{Kuzmin2007}. The following Figure\footnote{Taken from \url{https://www.comsol.com/blogs/which-turbulence-model-should-choose-cfd-application/}} illustrates the concept.
	 
	 \begin{figure}[h]
	 	\centering
	 	\includegraphics[width=0.7\linewidth]{aux_files/wall_functions_approach_near_wall}
	 \end{figure}
	 
	 In \texttt{DG-Flow}, $\delta$ will be evaluated as the distance in the normal direction between the boundary-cell centre and the boundary-face where the wall-function is applied. This implies that corner boundary-cells, adjacent to more than one wall, are characterized by more than one value $\delta$. 
	 
	 Finally, it is has to be mentioned that, with this approach, $\delta$ varies with mesh refinement near the wall, but can also vary along the wall boundary itself, in case of local mesh refinement.
	 
	 \todo{Explain and illustrate how $\delta$ is evaluated in practice}
	 	
	\subsection{Implementation of momentum BCs}
	\label{sec:implementation_momentum_bcs}
	Firstly, consider the case in which wall boundaries are parallel to the cartesian axes.
	
	The free-slip condition \eqref{eq:normal_bc} is of homogeneous Dirichlet type and therefore it is trivial to impose. The shear-stress condition \eqref{eq:tangential_bc} is instead a Robin bc, since it relates the velocity gradients to the value of the velocity itself \citep{Silliman1980}. This is easy to prove if a 2D domain is considered:
	\begin{enumerate}
		\item re-write condition \eqref{eq:tangential_bc}, introducing the tangential unit vector to the wall, $\mathbf{t}$:
		\begin{equation}
			\mathbf{t} \cdot \boldsymbol{\sigma} \cdot \mathbf{n}
		\end{equation}
		\item since the stress tensor, in components (adopting summation convention over repeated indexes), can be written as:
			\begin{equation}
				\sigma_{ij} = -p\delta_{ij} + \mu \frac{\partial u_i}{\partial x_j} + \mu \frac{\partial u_j}{\partial x_i} - \frac{2}{3}\mu \frac{\partial u_k}{\partial x_k}\delta_{ij}
			\end{equation}
			where $p$ is the pressure and $\delta_{ij}$ is the Kronecker delta, it follows that:
			\begin{equation}
			\sigma_{ij} n_j = -p n_i + \mu \frac{\partial u_i}{\partial x_j}n_j + \mu \frac{\partial u_j}{\partial x_i}n_j - \frac{2}{3}\mu \frac{\partial u_k}{\partial x_k}n_i
			\end{equation}	
			which, going back to tensor-notation, is equal to:
			\begin{equation}
			\boldsymbol{\sigma} \cdot \mathbf{n} = -p\mathbf{n} + \mu \left(\mathbf{n} \cdot \nabla \right) \mathbf{u} + \mu \nabla(\mathbf{n} \cdot \mathbf{u}) - \frac{2}{3}\mu \left( \nabla \cdot \mathbf{u}\right) \mathbf{n}
			\end{equation}	
		\item by taking the tensor contraction with the tangential unit vector, it follows that:
			\begin{equation}
				\mathbf{t} \cdot \boldsymbol{\sigma} \cdot \mathbf{n} = -p\left( \mathbf{t} \cdot \mathbf{n}\right)  + \mu \left(\mathbf{n} \cdot \nabla \right) \left( \mathbf{t} \cdot \mathbf{u}\right)  + \mu \left( \mathbf{t} \cdot \nabla\right) (\mathbf{n} \cdot \mathbf{u}) - \frac{2}{3}\mu \left( \nabla \cdot \mathbf{u}\right) \left( \mathbf{t} \cdot \mathbf{n}\right) 
			\end{equation}	
		The first and last terms obviously drop out, since $\left( \mathbf{t} \perp \mathbf{n}\right) $, together with the third term (due to the no-penetration condition \eqref{eq:normal_bc}). Therefore, what is left is $ \mu \left(\mathbf{n} \cdot \nabla \right) \left( \mathbf{t} \cdot \mathbf{u}\right) $, which is the normal gradient of the tangential velocity, which depends on the value of the tangential velocity itself, from condition \eqref{eq:tangential_bc}. This condition is clearly Robin-type.
	\end{enumerate}
	
	\noindent
	Next sections will focus on how the Robin condition (together with the free-slip one) can be imposed in the NS weak formulation solved by \texttt{DG-Flow}.
	 	
	\subsubsection{Implementation of momentum BCs in the SIP part of the weak form}
	\citet[p. 127]{DiPietro2012} describes how to modify the SIP weak formulation when a Robin bc is imposed.
	Using more or less the same notation adopted in the document for viscous terms, it is possible to write that: 
	
	\begin{itemize}
	\item if the problem to be solved on the continuous domain $\Omega$ is
		\begin{gather}
		\begin{aligned}
		-\nabla \cdot\left(\mu\nabla\mathbf{u}\right) = \mathbf{f} & \qquad \text{on $\Omega$} \\
		\gamma \mathbf{u} + \left( \mathbf{n}\cdot \nabla\right) \mathbf{u} = \mathbf{g}_R  & \qquad \text{on $(\partial\Omega)_R \equiv \partial\Omega$}
		\end{aligned}
		\label{eq:problem}
		\end{gather}
		where $ \mathbf{f} $ is the body-force function, $\gamma$ is a positive constant and $\mathbf{g}_R  $ is another known vector-valued function
	\item then the SIP continuous weak form is
		\begin{equation}
		\text{Find $\mathbf{u}\in V_h$, such that }
		 \quad A^\mathrm{R}(\mathbf{u},\mathbf{v}) = l^\mathrm{R}(\mathbf{v})
		 \qquad \text{for all $\mathbf{v}\in V_h$}
		 \label{eq:SIP_Robin}
		\end{equation}
		where $V_h$ is the DG space and
		\begin{gather}
		\begin{aligned}
		A^\mathrm{R}(\mathbf{u},\mathbf{v})	&
		= \sum_{T\in\mathcal{T}_h} \int_{T} 
			\mu(\nabla\mathbf{u})\colon(\nabla\mathbf{v}) + \sum_{F\in\mathcal{F}^\text{i}_h} \int_{F} 
			\eta\ \llbracket\mathbf{u}\rrbracket \cdot \llbracket\mathbf{v}\rrbracket \\&
		- \sum_{F\in\mathcal{F}^\text{i}_h} \int_{F}
			\left( \textbf{n}_F \cdot \left\{\mu \nabla \mathbf{u}\right\} \cdot \llbracket\mathbf{v}\rrbracket + \textbf{n}_F \cdot \left\{\mu \nabla \mathbf{v}\right\} \cdot \llbracket\mathbf{u}\rrbracket\right) 
		+ \sum_{F\in\mathcal{F}^R_h} \int_F 
		    \gamma \mathbf{u}\cdot\mathbf{v}
		\end{aligned}
		\label{eq:bilinear_vel}
		\end{gather}
		and
		\begin{equation}
		l^\mathrm{R}(\mathbf{v}) = 
		  \sum_{T\in\mathcal{T}_h} \int_{T}
			 \mathbf{f}\cdot\mathbf{v} 
		+ \sum_{F\in\mathcal{F}^R_h} \int_F 
		     \mathbf{g}_R\cdot\mathbf{v}
		\label{eq:rhs}
		\end{equation}
	\end{itemize}
	Here, $\mathcal{T}_h $ is the mesh and $\mathcal{F}_h $ is the set of all element faces, with subsets: $\mathcal{F}^\text{i}_h $ for interior faces, $ \mathcal{F}^R_h$ for Robin faces. $\eta$ is the penalty parameter, depending (also) on the diffusion coefficient and the face size. Finally, the \textit{average} and the \textit{jump} operators are respectively defined as:
	\begin{equation*}
	\{ \cdot \}_F  \equiv \frac{1}{2} \left( \left. \cdot \right|_{T_1} + \left. \cdot \right|_{T_2} \right)
	\qquad \text{ and } \qquad
	\llbracket \cdot \rrbracket_F \equiv \left. \cdot \right|_{T_1} - \left. \cdot \right|_{T_2} 
	\end{equation*}	
	and they act component-wise. On boundary faces, it is simply $\{ \cdot \}_F  \equiv  \left. \cdot \right|_{T_1} $ and $\llbracket \cdot \rrbracket_F \equiv \left. \cdot \right|_{T_1}  $.
	
	\smallskip	
	Note that problem \eqref{eq:problem} is vectorial (i.e. there is one equation for each $\mathbf{u}$-component), while the weak formulation \eqref{eq:SIP_Robin} describes a scalar equation. The key to understand the connection is the fact that the weak formulation must be valid for \textit{all} vectorial test functions $\mathbf{v}$: among all the possibilities, there will be those test functions with a non-zero entry only in their first component, other with a non-zero entry only in their second component and so on. This means that equation \eqref{eq:SIP_Robin} is just a short-cut notation for a set of vector equations with $d$ components (where $d$ is the dimensionality of the problem \citep[see][p. 425, for more details]{Gresho1998}.
	
	\bigskip \medskip \noindent
	The SIP weak form \eqref{eq:SIP_Robin} is not enough to impose the wall-functions bc and must be further modified, based upon these considerations:
	\begin{enumerate}
		\item the shear-stress condition \eqref{eq:tangential_bc} is a homogeneous Robin bc
		\item the same condition acts only in the tangential direction
		\item the Navier-Stokes equations are not pure elliptic as problem \eqref{eq:problem}: how the convective flux and the pressure gradient discretization are influenced by the boundary conditions \eqref{eq:normal_bc} and \eqref{eq:tangential_bc}?
	\end{enumerate}
	Solution to Problem 1 is trivial: the integral containing $\mathbf{g}_R$ in equation \eqref{eq:rhs} drops out.
	Solution to Problem 3 will be the topic of sections \ref{sec:pressure_grad} and \ref{sec:convection}, while problem 2 will be tackled in the remainder of this section.	
	
	\bigskip \medskip \noindent
	A good solution for Problem 2 is given by \citet{Cliffe2010}, who solve the 2D incompressible Navier-Stokes equations with the following boundary conditions:
	\begin{gather}
	\begin{aligned}
	\mathbf{u} = \mathbf{g}_D \qquad & \text{on $ \ (\partial\Omega)_D$} \\
	\mu \left( \mathbf{n}\cdot \nabla\right)  \mathbf{u} -p\mathbf{n} = 0 \qquad & \text{on $ \ (\partial\Omega)_N$} \\	
	\mu \left( \mathbf{n}\cdot \nabla\right)  u_x -pn_x  = 0 \qquad \text{and} \qquad u_y = 0 \qquad & \text{on $ \ (\partial\Omega)_M$}
	\end{aligned}
	\end{gather}
	where $(\partial\Omega)_M$ is a symmetry axis \underline{parallel to the $x$-axis}.
	They basically write the bilinear form  \eqref{eq:bilinear_vel} as:
	\begin{gather}
	\begin{aligned}
	A^\mathrm{M}(\mathbf{u},\mathbf{v})	&
	= \sum_{T\in\mathcal{T}_h} \int_{T} 
		\mu (\nabla\mathbf{u})\colon(\nabla\mathbf{v})
	+ \sum_{F\in\mathcal{F}^\text{i}_h\cup\mathcal{F}^\text{D}_h} \int_{F}
		\eta \ \llbracket\mathbf{u}\rrbracket \cdot \llbracket\mathbf{v}\rrbracket
	+ \sum_{F\in\mathcal{F}^\text{M}_h} \int_{F} 
		\eta \ \llbracket u_y \rrbracket \llbracket v_y \rrbracket\\&
	- \sum_{F\in\mathcal{F}^\text{i}_h\cup\mathcal{F}^\text{D}_h} \int_{F}
		\left( \textbf{n}_F \cdot \left\{\mu \nabla \mathbf{u}\right\} \cdot \llbracket\mathbf{v}\rrbracket + \textbf{n}_F \cdot \left\{\mu \nabla \mathbf{v}\right\} \cdot \llbracket\mathbf{u}\rrbracket\right) \\ &
	- \sum_{F\in\mathcal{F}^\text{M}_h} \int_{F}
		\left( \textbf{n}_F \cdot \left\{\mu \nabla u_y\right\} \llbracket v_y\rrbracket + \textbf{n}_F \cdot \left\{\mu \nabla v_y\right\} \llbracket u_y\rrbracket\right)
	\end{aligned}
	\label{eq:bilinear_vel_modified}
	\end{gather}
	where $\mathcal{F}^\text{D}_h $ is the subset of Dirichlet faces and $ \mathcal{F}^M_h$ of ``symmetry'' faces.
	
	This shows how the SIP weak form is modified taking into account only those velocity components not affected by a natural boundary conditions (i.e. the normal component, along $y$-axis). Note, in fact, that, since the integrals on the set  $\mathcal{F}^M_h$ contain the $y$-component of the test function $\mathbf{v}$, they should be taken into account only when solving the $y$-momentum equation.
	
	\bigskip \bigskip \bigskip \noindent
	The natural extension of \eqref{eq:bilinear_vel_modified}, when the wall-function BCs \eqref{eq:normal_bc} and \eqref{eq:tangential_bc} are applied on $(\partial\Omega)_M$, should be (already simplifying jumps and averages expressions on boundary faces):
	\begin{gather}
	\begin{aligned}
	A^\mathrm{M}(\mathbf{u},\mathbf{v})	&
	= \sum_{T\in\mathcal{T}_h} \int_{T} 
	\mu (\nabla\mathbf{u})\colon(\nabla\mathbf{v})
	+ \sum_{F\in\mathcal{F}^\text{i}_h\cup\mathcal{F}^\text{D}_h} \int_{F}
	\eta \ \llbracket\mathbf{u}\rrbracket \cdot \llbracket\mathbf{v}\rrbracket
	+ \sum_{F\in\mathcal{F}^\text{M}_h} \int_{F} 
	\eta \ u_y v_y\\&
	- \sum_{F\in\mathcal{F}^\text{i}_h\cup\mathcal{F}^\text{D}_h} \int_{F}
	\left( \textbf{n}_F \cdot \left\{\mu \nabla \mathbf{u}\right\} \cdot \llbracket\mathbf{v}\rrbracket + \textbf{n}_F \cdot \left\{\mu \nabla \mathbf{v}\right\} \cdot \llbracket\mathbf{u}\rrbracket\right) \\ &
	- \sum_{F\in\mathcal{F}^\text{M}_h} \int_{F}
	\left( \mu \left(  \textbf{n}_F \cdot \nabla u_y\right) v_y + \mu \left( \textbf{n}_F \cdot \nabla v_y \right)  u_y  \right) 
	+ \sum_{F\in\mathcal{F}^M_h} \int_F 
	  \gamma u_x v_x
	\end{aligned}
	\label{eq:bilinear_vel_modified_robin}
	\end{gather}
	where $\gamma = \frac{\rho u_k}{u^+}$. The last integral, to be considered only when solving the $x$-momentum equation, derives indeed from the imposition of the tangential bc \eqref{eq:tangential_bc}.
	
	
	\subsubsection{Implementation of momentum BCs in the discretization of the pressure gradient}
	\label{sec:pressure_grad}
	This section focuses on the first part of Problem 3 outlined previously: how the discretization of the pressure gradient in the NS equations is influenced by the Robin bc \eqref{eq:tangential_bc}? 
	
	Once more, \citet{Cliffe2010} provide the solution: they write the bilinear form associated to the divergence/gradient operator as
	\begin{equation}
		B^\mathrm{M}(\mathbf{v},q) = 
		- \sum_{T\in\mathcal{T}_h} \int_{T} 
			q \nabla \cdot \mathbf{v}
		+ \sum_{F\in\mathcal{F}^\text{i}_h\cup\mathcal{F}^\text{D}_h} \int_{F}
			\left\{ q \right\} \llbracket\mathbf{v}\rrbracket \cdot \textbf{n}_F
		+ \sum_{F\in\mathcal{F}^\text{M}_h} \int_{F} 
			\left\{ q \right\} v_y{n}_{F,y}
	\label{eq:bilinear_form_grad}
	\end{equation}		
	Although their boundary condition on $(\partial\Omega)_M$  is homogeneous Neumann in the $x$-direction, while condition \eqref{eq:tangential_bc} is homogeneous Robin, there is no reason why the bilinear form $ B^\mathrm{M}(\mathbf{v},q) $ should change. One could say that the discretization of the gradient operator requires boundary conditions only in the normal direction (being a first order operator), but also that the pressure term is not affected by natural boundary conditions, that in the Navier-Stokes equations are expressed directly in terms of the stress tensor $\boldsymbol{\sigma}$. 
	
	
	\subsubsection{Implementation of momentum BCs in the convection term of the weak form}
	\label{sec:convection}
	The convection term is affected only by the no-penetration condition \eqref{eq:normal_bc} (the normal component of the velocity is the only one having incoming characteristics at the boundary). 
	Strictly speaking, nothing changes in \texttt{Dg-Flow}, except the choice of the suitable boundary function $\mathbf{u}_{\partial \Omega}$ to be used for the Lax-Friedrichs flux. 
	This is provided by \citet{Hartmann2008b}:
	\begin{itemize}
		\item at p.80, for slip wall boundary conditions he takes: $\mathbf{u}_{\partial \Omega} = \mathbf{u}^+ - 2\mathbf{n}\left( \mathbf{u}^+ \cdot \mathbf{n}\right) $, which ensures a vanishing average normal velocity at the boundary $\frac{1}{2}\left( \mathbf{u}^+ + \mathbf{u}_{\partial \Omega}\right)\cdot \mathbf{n} =0$
		\item instead, at p.85 he takes $\mathbf{u}_{\partial \Omega} = \mathbf{u}^+ - \mathbf{n}\left( \mathbf{u}^+ \cdot \mathbf{n}\right) $, which ensures that $\mathbf{u}_{\partial \Omega} \cdot \mathbf{n} = 0$
	\end{itemize}
	The second choice seems to be the most reasonable one, because it is the straightforward extension of the choices usually done for Dirichlet and Neumann BCs. Moreover, the author claims this choice leads to an adjoint consistent discretization. Finally, it is also in agreement to what \citet{Cliffe2010} do. 
	
	\subsubsection{Implementation of momentum BCs in general domains}
	Still one problem has to be solved: conditions \eqref{eq:normal_bc} and \eqref{eq:tangential_bc} refer to the normal and tangential directions, which means their imposition is not straightforward when the boundary is not aligned with the cartesian axes, as it was $(\partial\Omega)_M$  in the previous section.
	
    \citet{Engelman1982} and \citet{Gresho1998} tackled the problem in the framework of standard FEM codes, which use nodal basis functions. They proposed to locally (i.e. at boundary-node $i$) rotate the momentum equations and the solution vector set, locally solving for $(u_{n}, u_{\tau})^T$ instead of $(u_x, u_y)^T$. Unfortunately, this approach cannot be adopted in \texttt{DG-Flow}, since the basis functions chosen are not nodal, which means the velocity solution vector does not contain the nodal values of the unknown.
    
    However, the solution should be a simple generalization of the bilinear forms \eqref{eq:bilinear_vel_modified_robin} and \eqref {eq:bilinear_form_grad}, which become:

    \begin{gather}
    \begin{aligned}
    A^\mathrm{M}(\mathbf{u},\mathbf{v})	&
    = \sum_{T\in\mathcal{T}_h} \int_{T} 
    \mu (\nabla\mathbf{u})\colon(\nabla\mathbf{v})
    + \sum_{F\in\mathcal{F}^\text{i}_h\cup\mathcal{F}^\text{D}_h} \int_{F}
    \eta \ \llbracket\mathbf{u}\rrbracket \cdot \llbracket\mathbf{v}\rrbracket
    + \sum_{F\in\mathcal{F}^\text{M}_h} \int_{F} 
    \eta \ u_n v_n \\ &
    - \sum_{F\in\mathcal{F}^\text{i}_h\cup\mathcal{F}^\text{D}_h} \int_{F}
    \left( \textbf{n}_F \cdot \left\{\mu \nabla \mathbf{u}\right\} \cdot \llbracket\mathbf{v}\rrbracket + \textbf{n}_F \cdot \left\{\mu \nabla \mathbf{v}\right\} \cdot \llbracket\mathbf{u}\rrbracket\right) \\ &
	- \sum_{F\in\mathcal{F}^\text{M}_h} \int_{F}
	\left( \mu \left(  \textbf{n}_F \cdot \nabla u_n\right) v_n + \mu \left( \textbf{n}_F \cdot \nabla v_n \right)  u_n  \right) \\ &
    + \sum_{F\in\mathcal{F}^M_h} \int_F 
    \gamma \left( \mathbf{u}-u_n\mathbf{n}\right) \cdot\left( \mathbf{v}-v_n\mathbf{n}\right) 
    \end{aligned}
    \label{eq:bilinear_vel_modified_robin_general_domain}
    \end{gather}
    and
 	\begin{equation}
	 B^\mathrm{M}(\mathbf{v},q) = 
	 - \sum_{T\in\mathcal{T}_h} \int_{T} 
	 q \nabla \cdot \mathbf{v}
	 + \sum_{F\in\mathcal{F}^\text{i}_h\cup\mathcal{F}^\text{D}_h} \int_{F}
	 \left\{ q \right\} \llbracket\mathbf{v}\rrbracket \cdot \textbf{n}_F
	 + \sum_{F\in\mathcal{F}^\text{M}_h} \int_{F} 
	 \left\{ q \right\} v_n
	 \label{eq:bilinear_form_grad_general_domain}
	 \end{equation}	   
    where $u_n$ and $\left( \mathbf{u}-u_n\mathbf{n}\right) $ are the normal and tangential velocity components, while $v_n$ and $\left( \mathbf{v}-v_n\mathbf{n}\right) $ are the normal and tangential components of the test function.
    
	\smallskip 
    In order to implement the bilinear forms \eqref{eq:bilinear_vel_modified_robin_general_domain} and \eqref{eq:bilinear_form_grad_general_domain} in \texttt{DG-Flow} it is necessary to write explicitly the normal and tangential velocity and test function components in terms of their cartesian components, according to the transformation:
    
    \begin{equation}
	    \begin{bmatrix}
	    u_n \\
	    u_{t_1} \\
	    u_{t_2}
	    \end{bmatrix} = 
	    \begin{bmatrix}
	    n_x & n_y & n_z\\
	    t_{1,x} & t_{1,y} & t_{1,z}\\
	    t_{2,x} & t_{2,y} & t_{2,z}
	    \end{bmatrix}
	    \begin{bmatrix}
		u_x \\
		u_{y} \\
		u_{z}
		\end{bmatrix}	    
    	\label{eq:rotation_transf_3D}
    \end{equation}
    where $\mathbf{t}_1$ and $\mathbf{t}_2$ are two linear independent unit vectors lying on the tangential plane orthogonal to $\mathbf{n}$. In 2D it is easier, since the tangential vector is unique:
     \begin{equation}
	 \begin{bmatrix}
	 u_n \\
	 u_{t}
	 \end{bmatrix} = 
	 \begin{bmatrix}
	 n_x & n_y\\
	 t_{x} & t_{y}
	 \end{bmatrix}
	 \begin{bmatrix}
	 u_x \\
	 u_{y}
	 \end{bmatrix}	    
	 \label{eq:rotation_transf_2D}
	 \end{equation}   
	How to find the tangential vectors is the topic of section \ref{sec:tang_and_normal_vectors}
	
	\bigskip
	For the sake of simplicity, let's start with the 2D case. In the following, the integrals along the set of faces $\mathcal{F}^\text{M}_h$ in equations \eqref{eq:bilinear_vel_modified_robin_general_domain} and \eqref{eq:bilinear_form_grad_general_domain} will be rewritten in terms of cartesian components:
	\begin{itemize}
		\item $A^\mathrm{M}(\mathbf{u},\mathbf{v})$, penalty integral 
			\begin{multline}
				\int_{F} \eta \ u_n v_n = \int_{F} \eta \left(u_x n_x +u_y n_y\right) \left(v_x n_x +v_y n_y\right) = \\[5pt]
				= \int_{F} \eta \ n_x^2 \ u_x v_x +  \int_{F} \eta \ n_y n_x \ u_y v_x + \int_{F} \eta \ n_x n_y \ u_x v_y + \int_{F} \eta \ n_y^2 \ u_y v_y			
				\label{penalty_int_general_geom_2D}	
			\end{multline}
	
		\item $A^\mathrm{M}(\mathbf{u},\mathbf{v})$, consistency integral
			\begin{multline}
			-\int_{F} \mu \left(  \textbf{n}_F \cdot \nabla u_n\right) v_n = \\[5pt]
			= -\int_{F} \left[\mu \left(  \textbf{n}_F \cdot \nabla u_x\right)  n_x +\mu \left(  \textbf{n}_F \cdot \nabla u_y \right)n_y\right] \left(v_x n_x +v_y n_y\right)= \\[5pt]
			= -\int_{F} \ n_x^2 \mu \left(  \textbf{n}_F \cdot \nabla u_x\right) v_x -  \int_{F} \ n_y n_x \mu \left(  \textbf{n}_F \cdot \nabla u_y\right) v_x +\\[5pt] -\int_{F} \ n_x n_y \mu \left(  \textbf{n}_F \cdot \nabla u_x\right) v_y - \int_{F} \ n_y^2 \mu \left(  \textbf{n}_F \cdot \nabla u_y\right) v_y	
			\label{consistency_int_general_geom_2D}			
			\end{multline}		
			
		\item $A^\mathrm{M}(\mathbf{u},\mathbf{v})$, symmetry integral
			\begin{multline}
			-\int_{F}\mu \left( \textbf{n}_F \cdot \nabla v_n \right)  u_n = \\[5pt]
			= -\int_{F} \left[\mu \left(  \textbf{n}_F \cdot \nabla v_x\right)  n_x +\mu \left(  \textbf{n}_F \cdot \nabla v_y \right)n_y\right] \left(u_x n_x +u_y n_y\right)= \\[5pt]
			= -\int_{F} \ n_x^2 \mu \left(  \textbf{n}_F \cdot \nabla v_x\right) u_x  -\int_{F} \ n_x n_y \mu \left(  \textbf{n}_F \cdot \nabla v_x\right) u_y +\\[5pt] \int_{F} \ n_y n_x \mu \left(  \textbf{n}_F \cdot \nabla v_y\right) u_x - \int_{F} \ n_y^2 \mu \left(  \textbf{n}_F \cdot \nabla v_y\right) u_y			
			\label{symmetry_int_general_geom_2D}	
			\end{multline}	
			
		\item $A^\mathrm{M}(\mathbf{u},\mathbf{v})$, ``Robin'' integral
		\begin{multline}
		\int_F \gamma \left( \mathbf{u}-u_n\mathbf{n}\right) \cdot\left( \mathbf{v}-v_n\mathbf{n}\right) = \int_F \gamma u_t v_t = \int_{F} 	\gamma \left(u_x t_x +u_y t_y\right) \left(v_x t_x +v_y t_y\right) = \\[5pt]
		= \int_{F} \gamma \ t_x^2 \ u_x v_x +  \int_{F} \gamma \ t_y t_x \ u_y v_x + \int_{F} \gamma \ t_x t_y \ u_x v_y + \int_{F} \gamma \ t_y^2 \ u_y v_y		
		\label{robin_int_general_geom_2D}	
		\end{multline}	

		\item $B^\mathrm{M}(\mathbf{v},q)$, face integral
			\begin{multline}
			\sum_{F\in\mathcal{F}^\text{M}_h} \int_{F} 
			\left\{ q \right\} v_n = \sum_{F\in\mathcal{F}^\text{M}_h} \int_{F} 
			\left\{ q \right\} \left(v_x n_x +v_y n_y\right) = \\[5pt] = \sum_{F\in\mathcal{F}^\text{M}_h} \int_{F} 
			\left\{ q \right\} v_x n_x  + \sum_{F\in\mathcal{F}^\text{M}_h} \int_{F} 
			\left\{ q \right\} v_y n_y
			\label{face_int_B_general_geom_2D}
			\end{multline}	
			It seems that the integral $\int_{F}
			\left\{ q \right\} \llbracket\mathbf{v}\rrbracket \cdot \textbf{n}_F $ in \eqref{eq:bilinear_form_grad_general_domain} can be simply extended to the set of faces $\mathcal{F}^\text{M}_h$.	
	\end{itemize} 
	These equations show how the momentum equations become coupled now, via the wall function boundary condition: those integrals containing $v_x$ must be taken into account while solving the $x$-momentum equation, while the remaining when solving the $y$-momentum equation. However, the procedure followed to derive each integral seem to be exact, since in the simple case when $(\partial\Omega)_M$ is parallel to one of the cartesian axes, the integrals in equations \eqref{eq:bilinear_vel_modified_robin} and \eqref{eq:bilinear_form_grad} are obtained again.
	
	\textcolor{red}{IS ALL OF THIS CORRECT? ANY PROBLEM IN THE SOLUTION OF THE LINEAR SYSTEM, DETERMINED BY THE COUPLING? COUPLING TERM TREATED EXPLICITLY (GAUSS-SIDEL ITERATION)?}
	
	\bigskip
	For the sake of completeness, let's consider now the 3D case. Actually, the generalization of integrals \eqref{penalty_int_general_geom_2D},\eqref{consistency_int_general_geom_2D}, \eqref{symmetry_int_general_geom_2D} and \eqref{face_int_B_general_geom_2D} is straightforward. The ``Robin'' integral requires instead a little bit of more effort, since the tangential components of the velocity are two:
	\begin{itemize}
		\item $A^\mathrm{M}(\mathbf{u},\mathbf{v})$, ``Robin'' integral
		\begin{equation}
		\begin{gathered}
		\int_F \gamma \left( \mathbf{u}-u_n\mathbf{n}\right) \cdot\left( \mathbf{v}-v_n\mathbf{n}\right) = \int_F \gamma \left(u_{t,1}\mathbf{t}_1 + u_{t,2}\mathbf{t}_2\right) \cdot \left(v_{t,1}\mathbf{t}_1 + v_{t,2}\mathbf{t}_2\right)  = \\[5pt] = \int_F \gamma u_{t,1} v_{t,1} + \int_F \gamma  u_{t,2} v_{t,2}   = \\[5pt] = \int_{F} 	\gamma \left(u_x t_{1,x} +u_y t_{1,y} +u_z t_{1,z}\right) \left(v_x t_{1,x} +v_y t_{1,y} +v_z t_{1,z}\right)  + \\[5pt] + \int_{F} 	\gamma \left(u_x t_{2,x} +u_y t_{2,y} +u_z t_{2,z}\right) \left(v_x t_{2,x} +v_y t_{2,y} +v_z t_{2,z}\right) = \\[5pt]
		= \int_{F} \gamma \ t_{1,x}^2 \ u_x v_x +  \int_{F} \gamma \ t_{1,y} t_{1,x} \ u_y v_x +  \int_{F} \gamma \ t_{1,z} t_{1,x} \ u_z v_x + \\[5pt]
		+ \int_{F} \gamma \ t_{1,x}t_{1,y} \ u_x v_y + \int_{F} \gamma \ t_{1,y}^2 \ u_y v_y	+ \int_{F} \gamma \ t_{1,z} t_{1,y} \ u_z v_y + \\[5pt]	
		+ \int_{F} \gamma \ t_{1,x}t_{1,z} \ u_x v_z + \int_{F} \gamma \ t_{1,y}t_{1,z} \ u_y v_z	+ \int_{F} \gamma \ t_{1,z}^2 \ u_z v_z + \\[5pt]
		+\int_{F} \gamma \ t_{2,x}^2 \ u_x v_x +  \int_{F} \gamma \ t_{2,y} t_{2,x} \ u_y v_x +  \int_{F} \gamma \ t_{2,z} t_{2,x} \ u_z v_x + \\[5pt]
		+ \int_{F} \gamma \ t_{2,x}t_{2,y} \ u_x v_y + \int_{F} \gamma \ t_{2,y}^2 \ u_y v_y	+ \int_{F} \gamma \ t_{2,z} t_{2,y} \ u_z v_y + \\[5pt]	
		+ \int_{F} \gamma \ t_{2,x}t_{2,z} \ u_x v_z + \int_{F} \gamma \ t_{2,y}t_{2,z} \ u_y v_z	+ \int_{F} \gamma \ t_{2,z}^2 \ u_z v_z	
		\end{gathered}	
		\label{robin_int_general_geom_3D}	
		\end{equation}
	\end{itemize} 
	Eighteen integrals arise, six of which must be considered in each momentum equation. However, seems to be reasonable, because for a 2D problem, with tangential axis parallel to a cartesian one, this integral simplifies into the one considered in equation \eqref{eq:bilinear_vel_modified_robin}.
	\textcolor{red}{IS THIS CORRECT?}
	
	
	\subsubsection{Evaluation of normal and tangential vectors on each face}
	\label{sec:tang_and_normal_vectors}
	Normal unit vectors on each face are already evaluated in \texttt{DG-Flow}, so it remains to describe how to find the tangential unit vector(s).
	
	\smallskip
	If the geometry is 2D, the single tangential unit vector $\mathbf{t}$ is simply found by:
	\begin{equation}
		\mathbf{t} = \mathbf{n} \times \mathbf{e}_z 
	\end{equation}
	where the positive tangent direction is chosen to be clockwise. In this way, the coordinate system remains right-handed \citep{Engelman1982}. 
	
	\smallskip
	In the 3D case, given the normal vector $\mathbf{n}$ it exists only a unique tangent \textit{plane}, with an infinite number of possibilities for the pair $(\mathbf{t}_1,\mathbf{t}_2)$. A possible procedure to find such a pair can be found in \citet{Engelman1982}:
	\begin{enumerate}
		\item compute: $\max\left(\left| \mathbf{n} \times \mathbf{e}_x \right|, \left| \mathbf{n} \times \mathbf{e}_y \right|,\left| \mathbf{n} \times \mathbf{e}_z \right| \right) $
		\item choose the unit vector associated with the maximum to evaluate $\mathbf{t}_1$, say $\mathbf{e}_z$, via:
		\begin{equation}
			\mathbf{t}_1 = \frac{\mathbf{n} \times \mathbf{e}_z}{\left| \mathbf{n} \times \mathbf{e}_z \right|} = \left( \frac{n_y}{n^*}, \frac{-n_x}{n^*}, 0\right)^T, \qquad \text{where} \quad n^* = \sqrt{n_x^2 + n_y^2} 
		\end{equation}
		\item complete the right-handed triad with:
		\begin{equation}
			\mathbf{t}_2 = \mathbf{n} \times \mathbf{t}_1 = \left( \frac{n_x n_z}{n^*}, \frac{n_y n_z}{n^*}, -n^*\right)^T
		\end{equation}
	\end{enumerate}
    
    
	\section{Wall functions for the energy equation}
	\todo{Write wall-functions used for the energy eq.}
	
	\section{Boundary conditions expressed in term of mass flux}
	When a solution will be found for writing the viscous term in the NS equations in terms of the mass-flux, the extension of this solution in order to correctly imposing the wall-functions boundary conditions will be straightforward. These BCs do not introduce any specific problem.
	
	
	\listoftodos
	%% BIBLIOGRAPHY
	\bibliography{aux_files/wall_functions_bib}
	\bibliographystyle{plainnat}
\end{document}  