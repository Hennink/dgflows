\documentclass{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{stmaryrd}

\newcommand{\jump}[1]{\left\llbracket #1 \right\rrbracket}
\newcommand{\avg}[1]{\left\{ #1 \right\}}

\newcommand{\DoNotBC}{\Gamma_\text{GN}}
\newcommand{\DoNotFaces}{\mathcal{F}^h_\text{GN}}

\newcommand{\AdjDoNotBC}{\Gamma_\text{AdjTrac}}
\newcommand{\AdjDoNotFaces}{\mathcal{F}^h_\text{AdjTrac}}

\title{Outflow boundary conditions}
\author{NERA people}
\date{}


\begin{document} 
\maketitle

\section{Problem:}
Governing equation:
\begin{align*}
    - \partial_k \sigma_{ki}(\mathbf{u})
    & =
    g_i
    \\
    \sigma_{ij}(\mathbf{v})
    & \coloneqq
    - p \delta_{ij} + \tau_{ij}(\mathbf{v})
    \\
    \tau_{ij}(\mathbf{v})
    & \coloneqq
    \mu \left(
        \partial_j v_i
        + \partial_i v_j
        - \frac 2 3 \delta_{ij} \partial_k v_k
    \right)
\end{align*}
We discuss the discretization of two outflow boundary conditions:
\begin{align}
    \text{Generalized Neumann:}
    && 
    - p n_i + \tau_{ik}(\mathbf{u}) \ n_k
    & = F_i
    &&
    \text{on $\DoNotBC$}
    \label{eq:DoNot_bc_definition}
    \\
    \text{Adjusted Traction:}
    && 
    - p n_i
    +
    \mu \ n_k \partial_k u_i
    & = f_i
    &&
    \text{on $\AdjDoNotBC$.}
    \label{eq:AdjTrac_bc_definition}
\end{align}
In the weak form we need $n_k \sigma_{ki}(\mathbf{u})$, so we rewrite the boundary conditions as
\begin{equation} \label{eq:bc_rewritten}
\begin{aligned}
    \text{Generalized Neumann:}
    && n_k \sigma_{ki}(\mathbf{u})
    & = F_i
    &&
    \text{on $\DoNotBC$}
    \ \text{,}
    \\
    \text{Adjusted Traction:}
    && n_k \sigma_{ki}(\mathbf{u})
    & =
    f_i
    + n_k R_{ki}(\mathbf{u})
    &&
    \text{on $\AdjDoNotBC$,}
\end{aligned}
\end{equation}
where 
\begin{align*}
    R_{ij}(\mathbf{v})
    \coloneqq &
    \tau_{ij}(\mathbf{v}) - \mu \partial_i v_j
    \\
    = &
    \sigma_{ij}(\mathbf{v}) + p \delta_{ij} - \mu \partial_i v_j
    \\
    = &
    \mu \left(
        \partial_j v_i
        - \frac 2 3
        \delta_{ij} \partial_k v_k
    \right)
    \ \text{.}
\end{align*}
Equation \ref{eq:DoNot_bc_definition} is usually referred to as the `traction' boundary condition. The current name is based on the notion that it would be equivalent to a Neumann boundary condition if we'd have had a standard Fick stress (that is, $\sigma_{ij}(\mathbf{u}) \propto \partial_i u_j$).\footnote{
    Note though that the Fick stress and the viscous tress are unfortunately defined with opposite signs; $\sigma_{ij} n_j$ points in the direction of \emph{increasing} $i$-momentum, hence the minus sign in the pde.}
Equation \ref{eq:AdjTrac_bc_definition} is also sometimes referred to as a traction boundary condition for incompressible flows. Both of these boundary conditions are suitable for incompressible (and probably compressible) flows. Useful references include the Gresho book and the Volker book.

\section{SIP}
We add terms to the standard SIP form to make it consistent with the outflow boundary conditions above. Let $\DoNotFaces$ (resp. $\AdjDoNotFaces$) denote the faces that make up $\DoNotBC$ (resp. $\AdjDoNotBC$). Let $a^*(\mathbf{w},\mathbf{v})$ and $b^*(\mathbf{w})$ be the usual SIP operators, without outflow faces. On a mesh $\mathcal{T}^h$ with faces $\mathcal{F}^h$,
\begin{align*}
    a^*(\mathbf{w},\mathbf{v})
    & = \sum_{T\in\mathcal{T}^h} \int_T
    \sigma_{ij}(\mathbf{w}) \ \partial_i v_j
    + \text{[terms for all faces except outflow faces]}
    \\
    b^*(\mathbf{w})
    & = \sum_{T\in\mathcal{T}^h} \int_T
    g_i w_i
    + \text{[terms for all faces except outflow faces]}
\end{align*}
Now
\begin{itemize}
    \item integrate $a^*$ by parts,
    \item collect the face terms, rewriting them as jumps,
    \item use the fact that $\jump{\sigma(\mathbf{u})}=0$ implies $\jump{\sigma_{ik}(\mathbf{u}) v_k} = \avg{\sigma_{ik}(\mathbf{u})} \jump{v_k}$,
\end{itemize}
to find
\begin{align*}
    a^*(\mathbf{u},\mathbf{v})
    - b^*(\mathbf{u})
    & = \sum_{F \in \DoNotFaces \cup \AdjDoNotFaces} \int_F
    n_k \sigma_{kl}(\mathbf{u}) \ v_l
    \ \text{.}
\end{align*}
Using equation \ref{eq:bc_rewritten},
\begin{align*}
    \sum_{F \in \DoNotFaces \cup \AdjDoNotFaces} \int_F
    n_k \sigma_{kl}(\mathbf{u}) v_l
    = & \quad
    \sum_{F \in \DoNotFaces} \int_F
    \mathbf{F} \cdot \mathbf{v}
    + \sum_{F \in \AdjDoNotFaces} \int_F
    \mathbf{f} \cdot \mathbf{v}
    \\
    & + \sum_{F \in \AdjDoNotFaces} \int_F
    n_k R_{kl}(\mathbf{u}) \ v_l
    \ \text{,}
\end{align*}
and thus we are prompted to modify the SIP operators:
\begin{align*}
    a(\mathbf{w},\mathbf{v})
    & = a^*(\mathbf{w},\mathbf{v})
    - \sum_{F \in \AdjDoNotFaces} \int_F
    n_k R_{kl}(\mathbf{u}) \ v_l
    \\
    b(\mathbf{w})
    & = b^*(\mathbf{w})
    + \sum_{F \in \DoNotFaces} \int_F
    \mathbf{F} \cdot \mathbf{w}
    + \sum_{F \in \AdjDoNotFaces} \int_F
    \mathbf{f} \cdot \mathbf{w}
\end{align*}


\section{Outflow for mass flux?}
Does the mass flux version 
\begin{align*}
    \text{on $\Gamma_\text{out\_3}$:}
    &&
    - p n_i
    +
    \mu n_k \partial_k m_i
    & = \tilde{f}_i
\end{align*}
make sense?

\end{document}
