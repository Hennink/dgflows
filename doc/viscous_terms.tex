\documentclass[oneside,12pt]{book}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{empheq}
\usepackage{mathtools}
\usepackage{stmaryrd} % for nice double brackets
\usepackage{tabularx}
\usepackage{todonotes}

\usepackage{listings}
\lstset{language={[08]Fortran},frame=single}

\begin{document}
%
\title{Viscous terms in DG-FLOW}
\author{NERA people}
\maketitle
% \tableofcontents

\chapter{Notation}
\begin{tabularx}{\textwidth}{l X}
    \hline
    \hline
    \textbf{symbol} & \textbf{meaning} \\
    \hline
    $\mathbf{\Upsilon}$ & full state vector \\
    $\mathbf{u}$ & velocity = (total momentum)/mass \\
    $\mathbf{m} = \rho \mathbf{u}$ & mass flux = (total momentum)/volume \\
    $\rho$ & density = (total mass)/volume \\
    $\mu$ & dynamic viscosity \\
    $\nu = \mu / \rho$ & kinematic viscosity \\
    $k$ & thermal conductivity \\
    $p$ & pressure \\
    $\mathbf{f}$ & external body force \\
    $\mathbf{d}\coloneqq(1/\rho)\nabla\rho$ & relative spatial density gradient \\
    \hline
    $\mathcal{T}_h$ & mesh = set of all elements \\
    $\mathcal{F}_h$ & set of all faces \\
    $\mathcal{F}^?_h$ & subset of faces:
        \begin{itemize}
            \item interior faces: $?=i$
            \item boundary faces: $?=X$ for on boundary type $X$ (\{$D$:Dirichlet, $N$:Neumann, \ldots\})
        \end{itemize} \\
    $\mathbf{n}_\text{T}$ & outward normal of element $T$ \\
    $\mathbf{n}_\text{F}$ & normal at face $F$ (if $F\in\mathcal{F}_h^i$ it points from $T_1$ to $T_2$) \\
    $\llbracket \cdot \rrbracket_F$ & 
        jump operator: $\left. \cdot \right|_{T_1} - \left. \cdot \right|_{T_2}$
        (acts component-wise) (equals trace on boundary faces) \\
    $\{ \cdot \}_F$ & 
        average operator: $\frac{1}{2} \left( \left. \cdot \right|_{T_1} + \left. \cdot \right|_{T_2} \right)$
        (acts component-wise) (equals trace on boundary faces) \\
    $\eta_F$ & penalty parameter in SIP (excluding diff. coeff. and face size) \\
    $p_F$ & total penalty parameter (including everything) \\
    \hline
    $\mathbf{a}\cdot\mathbf{b}$ & $a_i b_i$ \\
    $A\colon B$ & $A_{ij} B_{ij}$ \\
    \hline
    $\mathbf{c}^\phi$ & solution vector of quantity $\phi$ \\
    \hline
    \hline
\end{tabularx}


\chapter{Discretization of stress tensor}
We solve for $\textbf{m}$ in the equations
\begin{align} \label{eq:NS}
    \textbf{m}_t
    + \nabla \cdot (\boldsymbol{\beta} \ \textbf{m})
    = \nabla \cdot \tau + \textbf{f} - \nabla p 
\end{align}
on a domain $\Omega$. We `freeze' all quantities except $\mathbf{m}$, so that the convective field $\boldsymbol{\beta} = \mathbf{u}$ can always be computed explicitly, based on a previous time step. The deviatoric stress tensor is usually written in terms of $\textbf{u}$:
\begin{align*}
    \tau = \mu \left( 
        \nabla \mathbf{u}
        + \left( \nabla \mathbf{u} \right)^\intercal
        - \frac 2 3 \left(\nabla \cdot \mathbf{u}\right) I
    \right)
    \ \text{.}
\end{align*}
The problem is that we want a DG discretization of this in terms of $\mathbf{m}$.

\begin{itemize}
    \item Section \ref{sect:discretize_velocity_stress_tensor} looks at the discretization in the velocity. This is interesting, as the results may carry over to other methods, and we might need this if nothing else works.
    \item Section \ref{sect:GSIP} is a generalization of section \ref{sect:discretize_velocity_stress_tensor}, and probaly the way to go.
    \item Section \ref{sect:full_state_vector} starts from the discretizations of Hartman/Houston/Van der Vegt, who use a full state vector, and then translate those results to out pressure correction scheme. This could well turn out to be equivalent to section \ref{sect:GSIP}.
    \item In section \ref{sect:pure_approach}, we manipulate the the governing equations as they are, before we think of discretizations. This is a very `pure' way of doing it, and it nicely separates some physical effects in a mathematical way.
\end{itemize}

\section{Generalized SIP} \label{sect:GSIP}
In this section we generalize the SIP method to equations of the form 
\begin{align} \label{eq:stokes_equation}
    - \nabla \cdot \tau & = \mathbf{f} && \text{on }\Omega
    \ \text{,}
\end{align}
where, in general, $\tau = \tau(\mathbf{q},\nabla\mathbf{q})$, but we will write $\tau = \tau(\mathbf{q})$ for brevity. We want to solve equation \ref{eq:stokes_equation} for $\mathbf{q}$ with the following boundary conditions:\label{misc:GSIP_bc_list}
\begin{itemize}
    \item Dirichlet: $\mathbf{q} = \mathbf{g}_D$ on $\partial\Omega^\text{D}$;
    \item generalized Robin: $\gamma_\text{GR}\ \mathbf{q} + \mathbf{n}\cdot\tau = \mathbf{g}_\text{GR}$ on $\partial\Omega^\text{GR}$.
\end{itemize}
The weak form corresponding to equation \ref{eq:stokes_equation} is 
\begin{align} \label{eq:stoked_continuous_weak_form}
    \text{\emph{Find $\mathbf{q}\in V$, such that $b(\mathbf{q},\mathbf{v}) = l(\mathbf{v})$ for all $\mathbf{v}\in V$, }}
\end{align}
where $V$ is some continuous space like $[H^1(\Omega)]^d$ and 
\begin{align*}
    b(\mathbf{w},\mathbf{v}) & = 
    \int_\Omega \tau(\mathbf{w}) \colon \nabla \mathbf{v}
    + \int_{\partial\Omega^\text{GR}} \gamma_\text{GR} \mathbf{w}\cdot\mathbf{v}
    \\
    l(\mathbf{v}) & =
    \int_\Omega \mathbf{f}\cdot\mathbf{v}
    + \int_{\partial\Omega^\text{GR}} \mathbf{g}_\text{GR}\cdot\mathbf{v}
    - \int_{\partial\Omega^\text{D}} \mathbf{n}\cdot\tau(\mathbf{g}_\text{D})\cdot\mathbf{v}
\end{align*}

We discretize equation \ref{eq:stokes_equation} as
\begin{align*}
    \text{\emph{Find $\mathbf{q}_h\in V_h$, such that $b^\text{GSIP}(\mathbf{q}_h,\mathbf{v}) = l^\text{GSIP}(\mathbf{v})$ for all $\mathbf{v}\in V_h$}}
    \ \text{,}
\end{align*}
where
\begin{empheq}[box=\fbox]{align} \label{eq:GSIP_bilinear_operator}
    b^\text{GSIP}(\mathbf{w},\mathbf{v}) & = \quad
    \sum_{T\in\mathcal{T}_h} \int_{T} \tau(\mathbf{w}) \colon \nabla\mathbf{v}
    + \sum_{F\in\mathcal{F}^\text{i}_h\cup\mathcal{F}^\text{D}_h} \int_{F}
    p_f \llbracket\mathbf{w}\rrbracket \cdot \llbracket\mathbf{v}\rrbracket
    \\ \nonumber
    & \quad - \sum_{F\in\mathcal{F}^\text{i}_h\cup\mathcal{F}^\text{D}_h} \int_{F}
    \mathbf{n}_F \cdot \left(
          \{\tau(\mathbf{w})\}\cdot\llbracket\mathbf{v}\rrbracket
        + \{\tau(\mathbf{v})\}\cdot\llbracket\mathbf{w}\rrbracket
    \right)
    \\ \nonumber
    & \quad + \sum_{F\in\mathcal{F}^\text{GR}_h} \int_{F}
    \gamma_\text{GR} \mathbf{w}\cdot\mathbf{v}
\end{empheq}
is a generalized SIP bilinear operator. Here $p_F$ is a total penalty function on face $F$. The analysis in the following section will demonstrate that this bilinear form ensures consistency over the internal faces. A proper choice of $l^\text{GSIP}(\mathbf{v})$ ensures a consistent treatment of the boundary conditions.

\subsection{Enforcing consistency with $l^\text{GSIP}$} \label{sect:GSIPG_consistency}
We derive $l^\text{GSIP}(\mathbf{v})$ by enforcing consistency of the discretization, which is to say that 
\begin{align} \label{eq:stokes_consistency}
    b^\text{GSIP}(\mathbf{q},\mathbf{v}) = l^\text{GSIP}(\mathbf{v}) && \text{for all $\mathbf{v}\in V$.}
\end{align}
We therefore investigate $b^\text{GSIP}(\mathbf{q},\mathbf{v})$. Since both $\mathbf{q}$ and $\tau(\mathbf{q})$ are assumed continuous, we have $\{\mathbf{q}\}_F = \mathbf{q}$, $\{\tau(\mathbf{q})\}_F = \tau(\mathbf{q})$, $\llbracket\mathbf{q}\rrbracket_F = \mathbf{0}$ and $\llbracket\tau(\mathbf{q})\rrbracket_F = 0$ for all internal faces $F\in\mathcal{F}_h^i$, so that
\begin{align*}
    b^\text{GSIP}(\mathbf{q},\mathbf{v}) & =
    \sum_{T\in\mathcal{T}_h} \int_{T} \tau(\mathbf{q}) \colon \nabla\mathbf{v}
    - \sum_{F\in\mathcal{F}^\text{i}_h\cup\mathcal{F}^\text{D}_h} \int_{F}
    \mathbf{n}_F \cdot \tau(\mathbf{q})\cdot\llbracket\mathbf{v}\rrbracket \\
    & \quad + \sum_{F\in\mathcal{F}^\text{D}_h} 
    \left(
        p_F\ \mathbf{g}_\text{D} \cdot \mathbf{v}
        - \mathbf{n}_F \cdot \tau(\mathbf{v}) \cdot \mathbf{g}_\text{D}
    \right)
    + \sum_{F\in\mathcal{F}^\text{GR}_h} \int_{F}
    \gamma_\text{GR} \mathbf{q}\cdot\mathbf{v}
    \ \text{.}
\end{align*}
Integrate the volumetric term by parts and use the fact that
\begin{align} \label{eq:elemsum2facesum}
    \sum_{T\in\mathcal{T}_h} \int_{\partial T} \mathbf{n}_T \cdot \tau(\mathbf{q}) \cdot \mathbf{v}
    & =
    \sum_{F\in\mathcal{F}^\text{i}_h\cup\mathcal{F}^\text{D}_h} \int_{F}
    \mathbf{n}_F \cdot \tau(\mathbf{q})\cdot\llbracket\mathbf{v}\rrbracket
    \\ \nonumber & \quad 
    + \sum_{F\in\mathcal{F}^\text{GR}_h} \int_{F}
    \left(
        \mathbf{g}_\text{GR} \cdot \mathbf{v}
        - \gamma_\text{GR} \mathbf{q}\cdot\mathbf{v}
    \right)
\end{align}
to find
\begin{align} \label{eq:GSIP:fully_simplified_exact_b}
    b^\text{GSIP}(\mathbf{q},\mathbf{v}) =
    & - \sum_{T\in\mathcal{T}_h} \int_{T}
    \left(\nabla\cdot\tau(\mathbf{q})\right) \cdot \mathbf{v}
    + \sum_{F\in\mathcal{F}^\text{D}_h} 
    \left(
        p_F\ \mathbf{g}_\text{D} \cdot \mathbf{v}
        - \mathbf{n}_F \cdot \tau(\mathbf{v}) \cdot \mathbf{g}_\text{D}
    \right)
    \\ & \nonumber
    + \sum_{F\in\mathcal{F}^\text{GR}_h} \int_{F}
    \mathbf{g}_\text{GR} \cdot \mathbf{v}
    \ \text{.}
\end{align}
In order for the consistency requirement \ref{eq:stokes_consistency} to conform with the strong form \ref{eq:stokes_equation}, we must set
\begin{empheq}[box=\fbox]{align}  \label{eq:GSIP_linear_operator}
    l^\text{GSIP}(\mathbf{v}) & =
    \quad \sum_{T\in\mathcal{T}_h} \int_{T} \mathbf{f} \cdot \mathbf{v}
    + \sum_{F\in\mathcal{F}^\text{D}_h} 
    \left(
        p_F\ \mathbf{g}_\text{D} \cdot \mathbf{v}
        - \mathbf{n}_F \cdot \tau(\mathbf{v}) \cdot \mathbf{g}_\text{D}
    \right) \\
    \nonumber
    & \quad +
    \sum_{F\in\mathcal{F}^\text{GR}_h} \int_{F}
    \mathbf{g}_\text{GR} \cdot \mathbf{v}
    \ \text{.}
\end{empheq}

\subsection{Expressions for $\tau$}
The governing equation for the quantity $\mathbf{q}$ determines $\tau$. If $\mathbf{q}$ is the enthalpy, then $\tau \propto \nabla\mathbf{q}$ and the GSIP discretization reduces to a standard SIP. For the the momentum equation, we have two forms for the viscous stress tensor. If $\mathbf{q}$ is the velocity, then
\begin{align}
    \label{eq:stress_tensor_velocity}
    \tau \leftarrow \tau^u(\mathbf{u}) \coloneqq \mu \left( 
        \nabla \mathbf{u}
        + \left( \nabla \mathbf{u} \right)^\intercal
        - \frac 2 3 \left(\nabla \cdot \mathbf{u}\right) I
    \right)
    \ \text{.}
\end{align}
If $\mathbf{q}$ is the mass flux, then
\begin{align}
    \tau \leftarrow \tau^m(\mathbf{m})
    & = \label{eq:stress_tensor_mass_flux} 
    \nu \left( 
        \hat{\delta} \mathbf{m}
        + \left( \hat{\delta} \mathbf{m} \right)^\intercal
        - \frac 2 3 \left(\hat{\delta} \cdot \mathbf{m}\right) I
    \right)
    \\ & = \nonumber
    \frac 1 \rho \tau^u(\mathbf{m}) + 
    \nu \left(
        \mathbf{d}\ \mathbf{m}
        + \mathbf{m}\ \mathbf{d}
        - \frac 2 3 (\mathbf{d}\cdot\mathbf{m}) I
    \right)
    \\ & = \nonumber
    \nu \left(
        \nabla \mathbf{m}
        + \left( \nabla \mathbf{m} \right)^\intercal
        - \frac 2 3 \left(\nabla \cdot \mathbf{m}\right) I
        + \mathbf{d}\ \mathbf{m}
        + \mathbf{m}\ \mathbf{d}
        - \frac 2 3 (\mathbf{d}\cdot\mathbf{m}) I
    \right)
    \ \text{.}
\end{align}
Here
\begin{align*}
     \mathbf{d}\coloneqq(1/\rho)\nabla\rho
     && \text{and} &&
     \hat{\delta}\coloneqq\mathbf{d}+\nabla
\end{align*}
are `frozen' when we solve for $\mathbf{m}$. A difference between equations \ref{eq:stress_tensor_velocity} and \ref{eq:stress_tensor_mass_flux} is that $\nabla$ is a linear operator, whereas $\hat{\delta}$ is affine. In terms of the velocity, we have $\tau = \tau(\nabla\mathbf{u})$, so that the viscous momentum transfer is due to a velocity gradient. The absolute value of the velocity is irrelevant. Written in terms of the mass flux, we have $\tau = \tau(\mathbf{m},\nabla\mathbf{m})$. If there is a non-zero density gradient, then there can be viscous momentum transfer even when $\partial_i m_j = 0$. In a velociity formulation, the momentum transfer is always perpendicular to the $\mathbf{u}$-isolines, but the equivalent statement is not valid for the mass flux formulation. Somewhat surprisingly, the previous analysis shows that these differences don't really effect the discretization.

\subsection{Adjoints}
We investigate the adjoints of the linear operators in the original problem
\begin{align*}
    L(\mathbf{q}) = \mathbf{f} && \text{on $\Omega$,} \\
    B(\mathbf{q}) = \mathbf{g} && \text{on $\partial\Omega$.}
\end{align*}
where $L(\mathbf{q})=-\nabla\cdot\tau(\mathbf{q})$ and $B$ is the linear boundary operator with
\begin{align} \label{eq:linear_bdn_operator}
    B(\mathbf{q}) = 
    \begin{cases}
        \mathbf{q} & \text{ on $\partial\Omega^\text{D}$} \\
        \gamma_\text{GR}\mathbf{q} + \mathbf{n}\cdot\tau(\mathbf{q}) & \text{ on $\partial\Omega^\text{GR}$}
    \end{cases}
    && \text{and} && 
    \mathbf{g} = 
    \begin{cases}
        \mathbf{g}_\text{D} & \text{ on $\partial\Omega^\text{D}$} \\
        \mathbf{g}_\text{GR} & \text{ on $\partial\Omega^\text{GR}$}
    \end{cases}
\end{align}
The inner product is defined as
$    <\mathbf{x},\mathbf{y}>_\Omega \coloneqq
    \int_\Omega \mathbf{x}\cdot\mathbf{y}
$. If both $\mathbf{x}$ and $\mathbf{y}$ satisfy the inhomogeneous boundary conditions above, then 
\begin{align*}
    <\mathbf{x},L(\mathbf{y})>_\Omega
    - <L(\mathbf{x}),\mathbf{y}>_\Omega
    = & \quad \int_\Omega
    \left(
        \tau(\mathbf{y})\colon\nabla\mathbf{x}
        - \tau(\mathbf{x})\colon\nabla\mathbf{y}
    \right)
    \\ & - \int_{\partial\Omega} \mathbf{n} \cdot
    \left(
        \tau(\mathbf{y})\cdot\mathbf{x}
        - \tau(\mathbf{x})\cdot\mathbf{y}
    \right)
    \\ = & \quad \int_\Omega
    \left(
        \tau(\mathbf{y})\colon\nabla\mathbf{x}
        - \tau(\mathbf{x})\colon\nabla\mathbf{y}
    \right)
    \\ & - \int_{\partial\Omega^\text{GR}}
    \mathbf{g}_\text{GR} \cdot (\mathbf{x} - \mathbf{y})
    \ \text{.}
\end{align*}
Thus $L$ is is probably self-adjoint if $\tau(\mathbf{y})\colon\nabla\mathbf{x} = \tau(\mathbf{x})\colon\nabla\mathbf{y}$. This is true for the standard case $\tau\propto\nabla\mathbf{q}$. \todo[inline]{Is the above equation correct? The lhs is linear in both x and y, whereas the rhs is not. To see this, just plug in $\mathbf{x}=\mathbf{0}$ or $\mathbf{y}=\mathbf{0}$. Is the inner product supposed to use homogeneous boundary conditions?}

Consider the specific linear operators $L^u$ and $L^m$ corresponding to equations \ref{eq:stress_tensor_velocity} and \ref{eq:stress_tensor_mass_flux} respectively. A direct computation shows that $\tau^v(\mathbf{y})\colon\nabla\mathbf{x} = \tau^v(\mathbf{x})\colon\nabla\mathbf{y}$ holds true for the velocity stress tensor $\tau^u$ of equation \ref{eq:stress_tensor_velocity}. Now consider $L^m$. The difference in the stress tensors is
\begin{align*}
    e(\mathbf{q}) \coloneqq \tau^m(\mathbf{q}) - \tau^u(\mathbf{q})
    = \nu
    \left(
        \mathbf{d}\mathbf{q}
        + \mathbf{q}\mathbf{d}
        - \frac{2}{3}(\mathbf{d}\cdot\mathbf{q}) I
    \right)
\end{align*}
and difference in linear operators is 
\begin{align*}
    L^e(\mathbf{q}) & \coloneqq L^m(\mathbf{q}) - L^u(\mathbf{q}) =
    - \nabla\cdot e(\mathbf{q})
    \\ & = 
    - \nabla \cdot \left( \nu \left(
        \mathbf{d}\mathbf{q}
        + \mathbf{q}\mathbf{d}
        - \frac{2}{3}(\mathbf{d}\cdot\mathbf{q}) I
    \right) \right)
    \ \text{.}
\end{align*}
We have
\begin{align*}
    e(\mathbf{x}) \colon \nabla\mathbf{y}
    - e(\mathbf{y}) \colon \nabla\mathbf{x}
    = - \frac 2 3 \nu \mathbf{d} \cdot
    \left(
        (\nabla\cdot\mathbf{y}) \mathbf{x}
        - (\nabla\cdot\mathbf{x}) \mathbf{y}
    \right)
    \neq 0
    \ \text{,}
\end{align*}
so it seems that $L^e$, and by extension $L^m$, is not self-adjoint due to the $((2/3)(\mathbf{d}\cdot\mathbf{m})I)$-term in equation \ref{eq:stress_tensor_mass_flux}. 

\subsection{Other boundary conditions}
We could implement some other boundary conditions, should we need them. With the possible exception of the homogeneous Neumann case, the following boundary conditions would all add extra terms to equation \ref{eq:elemsum2facesum} in the consistency analysis of section \ref{sect:GSIPG_consistency}. In order to preserve consistency, we should add similar boundary terms to $b^\text{GSIP}(\mathbf{w},\mathbf{v})$, to ensure that everything cancels out again in equation \ref{eq:GSIP:fully_simplified_exact_b}.
\begin{itemize}
    \item homogeneous Neumann: $\mathbf{n} \cdot \nabla \mathbf{q} = \mathbf{0}$ on $\partial\Omega^\text{HN}$.
    
    This can perhaps be dealt with easily, as it might be consistent to treat it as a Dirichlet boundary condition with $\mathbf{g}_\text{D} = \mathbf{q}^\text{internal}$. An inspection of equations \ref{eq:GSIP_bilinear_operator} and \ref{eq:GSIP_linear_operator} reveals that such a boundary condition would have equal contribution on both sides of the linear system. We then wouldn't have to change anything.
    \item inhomogeneous Neumann: $\mathbf{n} \cdot \nabla \mathbf{q} = \mathbf{g}_\text{IN}$ on $\partial\Omega^\text{IN}$.
    
    \item Robin for the mass flux: $\mathbf{n} \cdot \nabla \mathbf{m} + \gamma_\text{R}\mathbf{m} = \mathbf{g}_\text{R}$
    
    This includes reflection in the velocity (i.e.: $\mathbf{n}\cdot\nabla\mathbf{u}=\mathbf{0}$), in which case $\gamma_\text{R} = \mathbf{n}\cdot\mathbf{d}$ and $\mathbf{g}_\text{R}=\mathbf{0}$.
    
    \item vanishing normal component:
    $\mathbf{n}\cdot\mathbf{q} = 0$, for example for a slip wall.
    
    This should be accompanied with a boundary condition for the component perpendicular to $\mathbf{n}$, possibly just a fixed stress or one of those turbulent thingies.
\end{itemize}. 


\chapter{Implementation}
\section{Stencils}
\subsection{Assembly using stencils}
An arbitrary quantity that is linear in $\phi$ is also linear in its solution vector $\mathbf{c}^\phi$. This is because $\phi = \boldsymbol{\gamma}^\phi\cdot\mathbf{c}^\phi$, where $\boldsymbol{\gamma}^\phi$ are the DG basis functions. More generally, any \emph{unknown} quantity $X$ that's linear in $\phi$ can be represented by a \emph{known} $\boldsymbol{\gamma}^X$, because $X$ can be computed as $X = \boldsymbol{\gamma}^X\cdot\mathbf{c}^\phi$. Examples are $\llbracket\phi\rrbracket = \boldsymbol{\gamma}^{\llbracket\phi\rrbracket}\cdot\mathbf{c}^\phi$ and $\mathbf{n}\cdot\nabla\phi = \boldsymbol{\gamma}^{\mathbf{n}\cdot\nabla\phi}\cdot\mathbf{c}^\phi$, but also the more complex full stress tensor $\tau_{ij} = \boldsymbol{\gamma}^{\tau_{ij}}\cdot\mathbf{c}^\mathbf{m}$, with $\tau_{ij} = \tau_{ij}(\mathbf{m},\nabla\mathbf{m})$ given by equation \ref{eq:stress_tensor_mass_flux}.

Now consider what this means for the assembly of a linear system. The penalty integrals in the SIP bilinear operator are of the form
\begin{align*}
    \int_F p\ \llbracket v \rrbracket \ \llbracket w \rrbracket
    = \int_F
        \left(
             p \ 
            \boldsymbol{\gamma}^{\llbracket v \rrbracket} 
            \boldsymbol{\gamma}^{\llbracket w \rrbracket}
        \right) \colon \left(
            \mathbf{c}^v
            \mathbf{c}^w
        \right)
        \ \text{.}
\end{align*}
The makes plain the the link between the continuous form (on the lhs) and the what we do for the assembly ($p\ \boldsymbol{\gamma}^{\llbracket v \rrbracket}\ \boldsymbol{\gamma}^{\llbracket w \rrbracket}$). All contributions in the SIP bilinear operator can be written this way, otherwise it wouldn't be bilinear. The linear operator is analogous, for example the Dirichlet term:
\begin{align*}
    \int_F g_D \mu\ \mathbf{n} \cdot \nabla v 
    = \int_F
        \left(
            g_D \mu \ 
            \boldsymbol{\gamma}^{\mathbf{n}\cdot\nabla v}
        \right) \cdot \left(
            \mathbf{c}^v
        \right)
        \ \text{.}
\end{align*}
Convective terms are trilinear, as is the SIP form for enthalpy, where the diffusion coefficient is temperature-dependent. For example,
\begin{align*}
    \int_F (\mathbf{n}\cdot \mathbf{u}) v w
    = \int_F
        \left(
            \gamma_i^{\mathbf{n}\cdot\mathbf{u}}
            \gamma_k^v
            \gamma_j^w
        \right) \left(
            c_i^\mathbf{u}
            c_j^v
            c_k^w
        \right)
    \ \text{.}
\end{align*}
This would need to be contracted once with a solution vector before inserting it into the matrix, thus taking one variable explicitly.

\subsection{Code outline}
These stencils can be implemented as follows. Listing \ref{listing:stencil_t} is pseudo-code for a quantity $X$ that can be represented as
\begin{align} \label{eq:affine_in_solvec}
    X = X(\phi) = \boldsymbol{\gamma}^X \cdot \mathbf{c}^\phi + \beta^X
    \ \text{.}
\end{align}
Though it's not strictly necessary, section \ref{sect:stencil_benefits} will show that there are some benefits in allowing $X$ to be affine in $\mathbf{c}^\phi$. That is, there is an offset $\beta$. For efficiency we store the values for $\boldsymbol{\gamma}^X$ and $\beta$ at more than one point in {\tt stencil\_t}. For the assembly we need a routine like what's shown in \ref{listing:assembly_of_integral}.
\begin{lstlisting}[float,
                   mathescape,
                   caption=implementation of equation \ref{eq:affine_in_solvec},
                   label=listing:stencil_t]
type :: stencil_t
    ! $d$ = # d.o.f. in element
    ! $N$ = # points at which the quantity is represented
    ! 
    integer :: degrees_of_freedom($d$) ! or elem_no
    real :: $\gamma$($d$,$N$)
    real :: $\beta$($N$)
contains
    procedure :: eval
    ...
end type stencil_t
...
function eval(this,$\mathbf{c}^\phi$) result(values)
    class(stencil_t), intent(in) :: this
    real, intent(in) :: $\mathbf{c}^\phi$(:)
    real, intent(out) :: values(this%$N$)
    
    c_local = $\mathbf{c}^\phi$(this%degrees_of_freedom)
    values = matmul(c_local,this%$\gamma$) + this%$\beta$
end function eval
\end{lstlisting}
\begin{lstlisting}[float,
                   label=listing:assembly_of_integral,
                   caption=suggested assembly of $\int XYZ$,
                   mathescape]
subroutine assemble_integral(elem_matrix,
                             quad_points,quad_weights,
                             X_stl,Y_stl,Z_stl)
    type(...), intent(inout) :: elem_matrix
    real, intent(in) :: quad_points(:,:), quad_weights(:)
    type(stencil_t), intent(in) :: X_stl, Y_stl, Z_stl
    
    ! To add $\int XYZ$ to elem_matrix implicitly:
    !  - If $X$ and $Y$ both depend on $\mathbf{c}^\phi$, then take $Z$ 
    !    explicitly.
    !  - Otherwise, take $Z$ implicitly.
    ...
end subroutine assemble_integral
\end{lstlisting}

\subsection{Benefits} \label{sect:stencil_benefits}
\begin{enumerate}
    \item We can implement the SIP routine generically for both the enthalpy and the mass flux. The only difference between the SIP forms is in $\tau$, so we only need to adjust $\boldsymbol{\gamma}^\tau$ and possibly $\beta^\tau$.
    \item Several boundary conditions become trivial (now bc.'s form most code)
    \begin{description}
        \item[Dirichlet:] The routine that returns $\llbracket\phi\rrbracket$ or $\{\mathbf{n}\cdot\nabla\phi\}$ or something can just put the Dirichlet value in $\beta^\phi$. It can then be processed like an internal face.
        \item[Homogeneous Neumann] can always be seen as a Dirichlet bc. with $\phi_\text{internal}=\phi_\text{external}$. The contribution for $\phi_\text{internal}$ is then put in $\boldsymbol{\gamma}^\phi$. The face is subsequently processed as an internal face.
    \end{description}
    \item There would be an easy path to compare DG to ccG and/or FV/FD schemes. ccG has the same weak form, just different definitions of $\boldsymbol{\gamma}^\phi$ and $\beta^\phi$. A direct comparison would be easy to do, and worth at least one publication.
    \item The routine in listing \ref{listing:assembly_of_integral} handles automatically what's implicit or explicit, so {\tt calc\_u}, {\tt calc\_rho}, \ldots do not know about it. This is highly convenient for an accurate fully implicit treatment of boundary conditions.
\end{enumerate}


\section{Direct assembly}
The GSIP terms (Eqs. \ref{eq:GSIP_bilinear_operator} and \ref{eq:GSIP_linear_operator}) with the mass-flux tensor (Eq. \ref{eq:stress_tensor_mass_flux}) can also be assembled directly.
We list some useful results.
For a single point in a quadrature set,
we have
\begin{itemize}
  \item the basis functions: $\phi_m$;
  \item the solution coefficients for the mass flux in the $i^\text{th}$ direction: $\mathbf{c}^i$;
  \item the relative density gradient: $\mathbf{d}$;
  \item $D_{ni} \coloneqq \nu\left(\partial_i\phi_n-d_i\phi_n\right)$, where $\nu$ is the kinematic viscosity;
\end{itemize}
Let
\begin{align}
\begin{aligned}
    \tau_{ij}(\mathbf{w})
    & =
    \nu
    \left(
        \partial_i w_j
        + \partial_j w_i
        - \frac 2 3 \delta_{ij} \partial_k w_k
        - d_i w_j
        - w_i d_j
        + \frac 2 3 \delta_{ij} d_k w_k
    \right)
    \\
    & = 
    \nu
    \left(
        G_i(w_j)
        +
        G_j(w_i)
        - \frac 2 3
        \delta_{ij}
        G_k(w_k)
    \right)
    \ \text{,}
\end{aligned}
\end{align}
where $G_i(x) \coloneqq \partial_i x - d_i x$.
For the numerical mass flux we have $m_i = \phi_n c_n^i$,
and so
\begin{align}
\begin{aligned}
    \tau_{ij}(\mathbf{m})
    & = 
    \nu
    \left(
        G_i(\phi_n) c_n^j
        +
        G_j(\phi_n) c_n^i
        - \frac 2 3
        \delta_{ij}
        G_k(\phi_n) c_n^k
    \right)
    \\
    & =
    D_{ni} c_n^j
    +
    D_{nj} c_n^i
    -
    \frac 2 3 \delta_{ij} D_{nk} c_n^k
    \ \text{.}
\end{aligned}
\end{align}
For an arbitrary test function $\mathbf{v}$ and a normal vector $\mathbf{n}$, we have
\begin{itemize}
  \item consistency entries, of the form 
\begin{equation*}
\begin{aligned}
  v_i
  \tau_{ij}(\mathbf{m})
  \ n_j
  \\
  & = 
  \nu 
  v_i 
  \left(
    n_j
    G_j(\phi_n) c_n^i
    +
    n_j
    G_i(\phi_n) c_n^j
    -
    \frac 2 3
    n_i
    G_k(\phi_n) c_n^k
  \right)
  \\
  & = 
  \nu 
  v_i 
  \left(
    n_j
    G_j(\phi_n) c_n^i
    +
    \left(
      n_j
      G_i(\phi_n)
      -
      \frac 2 3
      n_i
      G_j(\phi_n)
    \right)
    c_n^j
  \right)
  \ \text{;}
\end{aligned}
\end{equation*}
  \item symmetry entries, of the form
\begin{equation*}
  \tau_{ij}(\mathbf{v}) \
  n_i
  m_j
  = 
    \nu
    n_i
    \left(
      G_i(v_j)
      \phi_n c_n^j
      +
      G_j(v_i)
      \phi_n c_n^j
      - \frac 2 3
      G_k(v_k)
      \phi_n c_n^i
    \right)
  \ \text{.}
\end{equation*}
\end{itemize}




\end{document}


