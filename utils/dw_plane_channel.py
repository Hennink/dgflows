#!/usr/bin/env python3

import numpy as np
import dgf_utils

# Input:
rel_roughness = 0.0
channel_halfwidth = 1.0
cfl = 0.25
density = 1.0
friction_reynolds = 178.13
u_tau = 1.0
polorder_m = 3
elem_height = 0.131354

# Exact relations:
hydr_diam = 4 * channel_halfwidth
wall_stress = density * u_tau**2
pdrop = 4 * wall_stress / hydr_diam
kin_viscosity = u_tau * channel_halfwidth / friction_reynolds
dyn_viscosity = kin_viscosity * density
turnover_time = channel_halfwidth / u_tau
charac_wall_len = kin_viscosity / u_tau        # = channel_halfwidth / friction_reynolds
print('friction Re number, based on channel half-width =',friction_reynolds)
print('pdrop =',pdrop)
print('density      ',density)
print('dyn_viscosity',dyn_viscosity)
print('turn-over time = u_tau / delta =',turnover_time)
print('charac_wall_len =',charac_wall_len)


# From friction factors:
print('\n=== From an estimate of the darcy friction factor: ===')
reynolds_bulk = dgf_utils.reynolds_from_pdrop({
    'hydr_diam':      hydr_diam,
    'density':        density,
    'pdrop':          pdrop,
    'kin_viscosity':  kin_viscosity,
    'rel_roughness':  rel_roughness,
})
u_bulk = reynolds_bulk * kin_viscosity / hydr_diam
darcy_friction_factor = pdrop * (2 / density) * hydr_diam / u_bulk**2
flowtime_one_diam = hydr_diam / u_bulk
print('u_bulk = ', u_bulk)
print('reynolds_bulk = ', reynolds_bulk)


# Kolmogorov scales:
print('\n=== From Kolmogorov estimates: ===')
engy_dissipation = u_bulk**3 / channel_halfwidth        # d/dt (energy / mass)
kolmogorov_len = (kin_viscosity**3 / engy_dissipation)**(1/4.0)
print('kolmogorov_len = {:0.2e} = {:0.2f} * charac_wall_len'.format(kolmogorov_len,kolmogorov_len/charac_wall_len))


# about the mesh:
print('\n=== From mesh choices: ===')
eff_len = elem_height / polorder_m**2
dt = cfl * eff_len / u_bulk
print('eff_len / kolmogorov_len = {:0.1f}'.format(eff_len / kolmogorov_len))
print('cfl',cfl)
print('dt = ', dt)
print('steps per turn-over time      ',turnover_time / dt)
print('steps per diam. of flow length',flowtime_one_diam / dt)

