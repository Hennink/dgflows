#!/usr/bin/env bash
# 
# https://stackoverflow.com/questions/31971836/having-parameter-constant-variable-with-nan-value-in-fortran
# 
. ../install/env.sh

tmp_file='delete-this-temp-nanreal-file.f90'

cat > $tmp_file << XXX
    use, intrinsic :: iso_fortran_env
    real(real32) :: x32
    real(real64) :: x64
    ! real(real128) :: x128

    integer(int32) :: i32
    integer(int64) :: i64
    ! integer(int128) :: i128

    x32 = 0
    x32 = 0/x32
    x64 = 0
    x64 = 0/x64
    ! x128 = 0
    ! x128 = 0/x128

    print '(a,i0,a)', 'real(real32), parameter :: NAN32 = transfer(', transfer(x32, i32), '_int32, 1.0_real32)'
    print '(a,i0,a)', 'real(real64), parameter :: NAN64 = transfer(', transfer(x64, i64), '_int64, 1.0_real64)'
    ! print '(a,i0,a)', 'real(real128), parameter :: NAN128 = transfer(', transfer(x128, i128), '_int128, 1.0_real128)'
    end
XXX

$FC $tmp_file
./a.out
rm $tmp_file ./a.out
