#!/usr/bin/env python3

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.ticker import MaxNLocator
import numpy


def read_face_integral_files(files):
    """Returns a dict with headers and values in a list of face_plottable files.
    Headers need not be the same in all files.
    """
    all_name_lists = [colnames_in_file(f) for f in files]
    all_names = set(name for nl in all_name_lists for name in nl)
    data = {name:[] for name in all_names}
    for f, names in zip(files,all_name_lists):
        mat = numpy.loadtxt(f,skiprows=1)
        if mat.size == 0:
            print("skipping empty file " + f)
            continue
        
        for name in all_names:
            if name in names:
                col = names.index(name)
                data[name].append(mat[:,col])
            else:
                nvals = mat.shape[0]
                nan = numpy.array([numpy.nan for _ in range(nvals)])
                data[name].append(nan)
    
    return {
        name: numpy.concatenate(lists)
        for name, lists in sorted(data.items())
    }

def colnames_in_file(file):
    with open(file,'r') as f:
        first_line = f.readline().rstrip('\n')
    
    return first_line.split(',')



if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('word',help="Only plot things with a name that includes 'word'. Use an empty string to plot everything.")
    parser.add_argument('files',nargs='+',help="'face_integrals' files, as outputted by DG Flows.")
    parser.add_argument('--fmt',default=['pdf'],nargs='+',help="Output formats.")
    args = parser.parse_args()

    data = read_face_integral_files(args.files)
    times = data.pop('time')

    fig = Figure()
    canvas = FigureCanvas(fig)
    ax = fig.add_subplot(1,1,1)
    
    # Add some hard-coded theoretical averages:
    vol_src = {
       'stress':    1.0,
       'heat':      17.55,
    }
    halfwidth = 1.0
    plane_area = (2*numpy.pi * 5*numpy.pi) * halfwidth**2
    volume = 2 * halfwidth * plane_area
    for name, s in vol_src.items():
        if name in args.word:   # hinges on the idea that you actually write 'stress' or 'heat' on the command line.
            ax.axhline(-s * volume / 2,color='black',label='theoretical avg.')
    
    # Plot the quantities that contain `args.word`:
    for name, vals in data.items():
        if args.word in name:
            line = ax.plot(times,vals,label=name)
            ax.axhline(numpy.average(vals),ls='--',color=line[0].get_color(),label='avg.')
    
    ax.legend(loc='best')
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.minorticks_on()
    ax.grid(which='major',color='k',linewidth=0.5)
    ax.grid(which='minor',color='k',linewidth=0.2,alpha=0.5)
    for fmt in args.fmt:
        canvas.print_figure('face_integrals.'+fmt,bbox_inches='tight')

