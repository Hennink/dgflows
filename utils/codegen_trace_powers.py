#!/usr/bin/env python3
"""This outputs Fortran functions for the trace of the power of symmetric matrices.
"""

from sympy import zeros, Symbol, sympify, fcode

max_d = 3

A = zeros(max_d)
for j in range(max_d):
    for i in range(j,max_d):
        A[i,j] = Symbol(f"A({i+1},{j+1})", real=True)
        A[j,i] = A[i,j]

funcname = "trace_power_symm"
print(f"""pure real(real64) function {funcname}(A, p) result(trace)
! Return `trace(A^p)`, where the power refers to matrix multiplication.
! `A` is assumed symmetric; its strictly upper triangular part is not referenced.
use, intrinsic :: iso_fortran_env
real(real64), intent(in) :: A(:,:)
integer, intent(in) :: p
if (p < 0) then;  error stop "{funcname}: expected non-negative `p`" """)
for p in range(0,4):
    for d in range(0,max_d+1):
        t = (A[:d,:d]**p).trace()
        t = sympify(t) # fixes a glitch for zero-sized matrices, where sympy returns a regular int
        symbols_to_declare, not_supported_functions, code_text = fcode(t.simplify(), assign_to="trace", human=False, source_format="free", standard=2008)
        assert len(symbols_to_declare) == 0 and len(not_supported_functions) == 0
        print(f"elseif (size(A,1) == {d} .and. p == {p}) then\n" + code_text)
print(f"""else;  error stop "{funcname}: combination of p and matrix size not implemented"
end if
end function
""")
