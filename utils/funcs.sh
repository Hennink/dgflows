#!/usr/bin/env bash

function avg_comp_time {
    if [[ $# -ne 1 ]]; then
        # https://stackoverflow.com/a/18568726/5930169
        echo "Illegal number of parameters"
        return 2
    fi
    
    < $1 grep 'comp\. time this step = ' | tail -n +6 | awk '{s+=$6; ss+=$6^2} END {m=s/NR; print m" +- "sqrt((ss - NR*m^2) / (NR - 1))}'
}
export -f avg_comp_time
