#!/usr/bin/env python3

from sympy import Symbol, tanh, Eq, nsolve, solve
import numpy

wall_dist = 1 / 395.0
dy_wall = 2 * wall_dist # first elem ends at y+ = 2
dy_bulk_vonkarman = 19 * wall_dist # in lecture slides (second-order scheme)
dy_bulk = (35/4.0)**(1/3.0) * dy_bulk_vonkarman # assuming same lengths for x,z; for us 2nd-order is 4 DOFs
# dy_bulk = 35**(1/3.0) * 5 * 5 * 1.27e-03 # 35 DOFs; LES=8*DNS; actual Kolmogorov length is 5(?) times larger than a priori estimate

gamma = Symbol('gamma', positive=True)
N = Symbol('N', positive=True) # cannot expect an integer solution

def y(i):
    return 1 - tanh(gamma*(1-2*i/N)) / tanh(gamma)

print('Initial guess:')
x = Symbol('x', positive=True)
approx_dy_wall = y(x).diff(x).subs(x,0)
approx_dy_bulk = y(x).diff(x).subs(x,N/2)
approx_ratio = (approx_dy_bulk / approx_dy_wall).simplify()
init_gamma = solve(Eq(dy_bulk/dy_wall, approx_ratio), gamma)[0]
init_N = solve(Eq(dy_bulk, approx_dy_bulk.subs(gamma,init_gamma)), N)[0]
print(f'   N = {init_N}')
print(f'   gamma = {init_gamma}')

print('Solution over reals:')
exact_dy_wall = y(1) - y(0)
exact_dy_bulk = y((N+1)/2) - y((N-1)/2) # I know, not really exact for even N... but very close
num_gamma, num_N = nsolve(
    [Eq(dy_wall, exact_dy_wall), Eq(dy_bulk, exact_dy_bulk)],
    [gamma, N], [init_gamma, init_N]
)
print(f'   N = {num_N}')
print(f'   gamma = {num_gamma}')

print('Approximate solution for integer N:')
actual_N = round(num_N)
actual_gamma = nsolve(Eq(dy_wall, exact_dy_wall.subs(N,actual_N)), gamma, num_gamma)
print(f'   N = {actual_N}')
print(f'   gamma = {actual_gamma}')

actual_y = numpy.array([y(j).subs({N:actual_N, gamma:actual_gamma}) for j in range(0,actual_N+1)])
widths = actual_y[1:] - actual_y[:-1]
print('desired min/max widths: %.14f, %.5f = %.5f w.u.' % (dy_wall, dy_bulk, dy_bulk / wall_dist))
print('actual  min/max widths: %.14f, %.5f = %.5f w.u.' % (min(widths), max(widths), max(widths) / wall_dist))
