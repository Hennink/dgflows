#!/usr/bin/env python3

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy
from os import path


def estimate_integral(x,y):
    mask = [not numpy.isnan(yi) for yi in y]
    cont_x = x[mask]
    cont_y = y[mask]
    dx = cont_x[1:] - cont_x[:-1]
    y_avg = (cont_y[1:] + cont_y[:-1]) / 2
    return numpy.dot(dx,y_avg)


## KMM reference data:
def read_kmm_data(kmm_files):
    dicts = (read_kmm_file(f) for f in kmm_files)
    merged_dicts = next(dicts)
    y_vals = merged_dicts['y']
    for d in dicts:
        numpy.testing.assert_allclose(y_vals,d['y'],err_msg='KMM y values not the same.')
        merged_dicts.update(d)
    
    return merged_dicts

def read_kmm_file(filepath):
    _, extension = str(filepath).rsplit('.',1)
    expected_names = {
        'means':        'y y+ Umean dUmean/dy Wmean dWmean/dy Pmean',
        'reystress':    'y y+ R_uu R_vv R_ww R_uv R_uw R_vw',
    }[extension].split()

    with open(filepath,'r') as f:
        for i, line in enumerate(f):
            if i == 23:
                kmm_names = line[1:].split()
                assert all(x == y for x,y in zip(expected_names,kmm_names)), 'not the names I expected: line=' + line
                break
    mat = numpy.loadtxt(filepath)
    
    return dict(zip(expected_names,mat.T))

dgf2kmm = {
    'u1':   'Umean',

    'fluct_u1u1':   'R_uu',
    'fluct_u2u2':   'R_vv',
    'fluct_u3u3':   'R_ww',
    'fluct_u2u1':   'R_uv',
    'fluct_u3u1':   'R_uw',
    'fluct_u3u2':   'R_vw',
}


## Pecnik reference data:
def read_pecnik_datafile(datafile):
    """Return a dict with labels and columns from a file that is in the format
    of the `DNSDataVarProp` repo.
    """
    DNS = numpy.loadtxt(datafile,skiprows=88)
    
    column_names = []
    with open(datafile) as f:
        for i in range(1,87): 
            content = f.readline()
            if i>54: 
                column_names.append(content.split(',')[0].split('...')[1].strip())
    assert DNS.shape[1] == len(column_names), 'bad number of names'
    assert column_names[0] == 'y', 'unexpected first column name'
    assert column_names[-1] == 'vdif', 'unexpected last column name'

    return {n:val for n, val in zip(column_names,DNS.T)}

dgf2pecnik = {
    'u1':       '<u+>',
    'rho':      '<rho>',
    'mu':       '<mu>',
    'k':        '<lambda>',

    # Either of these is going to be fine:
    'rescaled_T':        '<T>',
    'T':                 '<T>',
    
    'favre_T':  r'{T}',
    
    'refluct_TT':       "<T'2>",
    'fluct_TT':         '<rho>{T"T"}',
    'fluct_Tu1':        '<rho>{u"T"}',
    'fluct_Tu2':        '<rho>{v"T"}',

    'fluct_u1u1':       "<u'2>",
    'fluct_u2u2':       "<v'2>",
    'fluct_u3u3':       "<w'2>",

    'turb_stress11':    '<rho>{u"u"}',
    'turb_stress22':    '<rho>{v"v"}',
    'turb_stress33':    '<rho>{w"w"}',
    'turb_stress21':    '<rho>{u"v"}',

    'dissipation':      'eps',
}


## DGF data:
def read_many_group_plottables_files(group_plottables_files):
    outs = [read_group_plottables_file(f) for f in group_plottables_files]
    headers = [out[0] for out in outs]
    times = [out[1] for out in outs]
    coords = [out[2][:,0] for out in outs]
    values = [out[2][:,1:] for out in outs]
    
    coord = coords[0]
    for other in coords[1:]:
        numpy.testing.assert_allclose(other,coord,err_msg='not all coords are the same')

    header = headers[0]
    for other in headers[1:]:
        assert other == header, "not all 'group_plottables' files have the same header"

    names = header.split(',')[1:]
    avg_values = sum(t*v for t,v in zip(times,values)) / sum(times)
    vals_dict = dict(zip(names,avg_values.T))
    enrich_avg_set(vals_dict)
    
    return coord, vals_dict

def read_group_plottables_file(file):
    """Returns (header,avg_time,matrix)
    """
    with open(file,'r') as f:
        header = f.readline().rstrip('\n')
        time_str = f.readline().rstrip('\n')
        avg_time = float(time_str)
    mat = numpy.loadtxt(file,skiprows=2)
    
    return header, avg_time, mat


def enrich_avg_set(avg):
    """Adds derived quantities to a given dict 'avg', which contains Reynolds averages.
    """
    include_transposed_stress = True
    include_div_stress = True

    avg['fluct_u1u1'] = avg['u1*u1'] - avg['u1'] * avg['u1']
    avg['fluct_u2u1'] = avg['u2*u1'] - avg['u2'] * avg['u1'] # = <u2' u1'>_re
    avg['fluct_u3u1'] = avg['u3*u1'] - avg['u3'] * avg['u1']
    avg['fluct_u2u2'] = avg['u2*u2'] - avg['u2'] * avg['u2']
    avg['fluct_u3u2'] = avg['u3*u2'] - avg['u3'] * avg['u2']
    avg['fluct_u3u3'] = avg['u3*u3'] - avg['u3'] * avg['u3']
    
    avg['rms_fluct_u1'] = numpy.sqrt(avg['fluct_u1u1'])
    avg['rms_fluct_u2'] = numpy.sqrt(avg['fluct_u2u2'])
    avg['rms_fluct_u3'] = numpy.sqrt(avg['fluct_u3u3'])

    avg['favre_u1'] = avg['rho*u1'] / avg['rho']
    avg['favre_u2'] = avg['rho*u2'] / avg['rho']
    avg['favre_u3'] = avg['rho*u3'] / avg['rho']

    avg['rescaled_T'] = 1 + (avg['T'] - 1) * 150.0 # / 178.13
    avg['favre_T'] = avg['rho*T'] / avg['rho']
    avg['refluct_TT'] = avg['T*T'] - avg['T']**2 # = <T' T'>_re
    avg['fluct_TT'] = avg['rho*T*T'] - avg['rho*T']**2 / avg['rho'] # = <rho>_re * {T" T"}_fa
    avg['fluct_Tu1'] = avg['rho*T*u1'] - avg['rho*u1'] * avg['rho*T'] / avg['rho'] # = <rho>_re * {T" u1"}_fa
    avg['fluct_Tu2'] = avg['rho*T*u2'] - avg['rho*u2'] * avg['rho*T'] / avg['rho']
    avg['fluct_Tu3'] = avg['rho*T*u3'] - avg['rho*u3'] * avg['rho*T'] / avg['rho']

    avg['favre_d1u1'] = avg['rho*d1u1'] / avg['rho']
    avg['favre_d1u2'] = avg['rho*d1u2'] / avg['rho']
    avg['favre_d1u3'] = avg['rho*d1u3'] / avg['rho']
    avg['favre_d2u1'] = avg['rho*d2u1'] / avg['rho']
    avg['favre_d2u2'] = avg['rho*d2u2'] / avg['rho']
    avg['favre_d2u3'] = avg['rho*d2u3'] / avg['rho']
    avg['favre_d3u1'] = avg['rho*d3u1'] / avg['rho']
    avg['favre_d3u2'] = avg['rho*d3u2'] / avg['rho']
    avg['favre_d3u3'] = avg['rho*d3u3'] / avg['rho']

    avg['turb_stress11'] = avg['rho*u1*u1'] - avg['rho*u1'] * avg['rho*u1'] / avg['rho']
    avg['turb_stress21'] = avg['rho*u2*u1'] - avg['rho*u2'] * avg['rho*u1'] / avg['rho'] # = <rho>_re * {u2" u1"}_fa
    avg['turb_stress31'] = avg['rho*u3*u1'] - avg['rho*u3'] * avg['rho*u1'] / avg['rho']
    avg['turb_stress22'] = avg['rho*u2*u2'] - avg['rho*u2'] * avg['rho*u2'] / avg['rho']
    avg['turb_stress32'] = avg['rho*u3*u2'] - avg['rho*u3'] * avg['rho*u2'] / avg['rho']
    avg['turb_stress33'] = avg['rho*u3*u3'] - avg['rho*u3'] * avg['rho*u3'] / avg['rho']

    # Build the stress tensor.
    # Take care to make a deep copy of the array, in order not to overwrite values.
    for i in range(1,4):
        for j in range(1,4):
            tau_name = 'tau{}{}'.format(i,j)
            avg[tau_name] = numpy.copy(avg['mu*d{}u{}'.format(i,j)])
            if include_transposed_stress:
                avg[tau_name] += avg['mu*d{}u{}'.format(j,i)]
    if include_div_stress:
        trace = sum(avg['tau{}{}'.format(i,i)] for i in range(1,4))
        for i in range(1,4):
            avg['tau{}{}'.format(i,i)] += -1/3.0 * trace

    reavg_tau_x_du = sum(
        avg['tau{}{}'.format(i,j)] * avg['d{}u{}'.format(i,j)]
        for i in range(1,4)  for j in range(1,4)
    )
    avg['dissipation'] = - (avg['tau:du'] - reavg_tau_x_du) # = <tau_ij' du_ji'>_re



## Old-style DGF directories:
def read_old_grp_plottables_files(dirs):
    names = 'u_1 u_2 u_3 uu_11 uu_21 uu_31 uu_22 uu_32 uu_33'.split()
    include_sfs = True
    include_engy = False
    if include_sfs:
        names.append('nu_sfs')
    if include_engy:
        names.extend('h T rho'.split())
        if include_sfs:
            names.append('kcp_sfs')
    
    coord_files = (path.join(d,'group_plottables_coords') for d in dirs)
    coord_mats = (numpy.genfromtxt(f) for f in coord_files)
    coords = next(coord_mats)
    for other in coord_mats:
        numpy.testing.assert_allclose(coords,other,err_msg='Not all old coord files are the same.')
    
    vals_files  = (path.join(d,'group_plottables_vals') for d in dirs)
    nsteps_in_cases = [count_lines_that_start_with(path.join(d,'OUT')," TIME STEP ") for d in dirs]
    avg_mat = sum(ns * numpy.genfromtxt(f) for ns,f in zip(nsteps_in_cases,vals_files)) / sum(nsteps_in_cases)
    assert avg_mat.shape[1] == len(names), 'wrong number of columns old grp. plottable files'
    assert avg_mat.shape[0] == coords.shape[0], 'inconsistent number of points in old grp. plottable files'
    
    my_data = dict(zip(names,avg_mat.T))
    y_halfwidth = 0.001 / 2
    pdrop = 583.9428017122549 # later got adjusted to roughly 597.856482638769
    density = 178.64724691859874
    dyn_viscosity = 1.995077405239653e-05
    kin_viscosity = dyn_viscosity / density
    wall_stress = pdrop * y_halfwidth
    wall_shear_velocity = (wall_stress / density)**0.5
    for n,v in my_data.items():
        if n.startswith('u_') or n.startswith('uu_'):
            v /= wall_shear_velocity
    
    my_data['y'] = 1 - abs(coords) / max(abs(coords))
    return my_data

def count_lines_that_start_with(filepath,word):
    with open(filepath,'r') as f:
        total = sum(line.startswith(word) for line in f)
    return total


def autoscale_y(ax,margin=0.1):
    """Rescales the y-axis based on the data that are visible given the current xlim of the axis.
    ax      -- a matplotlib axes object
    margin  -- the fraction of the total height of the y-data to pad the upper and lower ylims

    Based on https://stackoverflow.com/a/35094823/5930169
    """
    def get_bottom_top(line):
        xd = line.get_xdata()
        yd = line.get_ydata()
        if len(yd) == 2:
            # This is probably an hline or a vline.
            return numpy.inf, -numpy.inf
        
        lo, hi = ax.get_xlim()
        # Handle NaNs in data:
        y_displayed = [y for x,y in zip(xd,yd) if lo < x < hi and not numpy.isnan(y)]
        h = max(y_displayed) - min(y_displayed)
        bot = min(y_displayed) - margin * h
        top = max(y_displayed) + margin * h
        return bot, top

    bots_tops = list(zip(*(get_bottom_top(line) for line in ax.get_lines())))
    bot = min(bots_tops[0])
    top = max(bots_tops[1])

    ax.set_ylim(bot,top)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('name',type=str,help='name of the quantity you want to plot')
    parser.add_argument('--domain',nargs=2,type=float,default=None,help="domain limits in plot")
    parser.add_argument('--dgf',nargs='+',default=None,help="'group_plottables' files, as outputted by DG Flows.")
    parser.add_argument('--old-dgf-dirs',nargs='+',default=None,help="directories with old-style DGF data")
    parser.add_argument('--kmm',nargs='+',default=None,help="'KMM' files, as provided with their paper")
    parser.add_argument('--pecnik',nargs='?',default=None,help="Pecnik data file, as found in https://github.com/Fluid-Dynamics-Of-Energy-Systems-Team/DNSDataVarProp")
    parser.add_argument('--extract-from-pecnik',default=None,help="Read the Pecnik data file, and output `name` in a form that can be read by DGF. `name` can be `|u|`.")

    parser.add_argument('--logx',dest='logx',action='store_true')
    parser.add_argument('--no-logx',dest='logx',action='store_false')
    parser.set_defaults(logx=False)

    args = parser.parse_args()

    if args.extract_from_pecnik is not None:
        data = read_pecnik_datafile(args.extract_from_pecnik)
        if args.name == '|u|':
            avg_u1_squared = data[dgf2pecnik['fluct_u1u1']] + data[dgf2pecnik['u1']]**2
            avg_u2_squared = data[dgf2pecnik['fluct_u2u2']]
            avg_u3_squared = data[dgf2pecnik['fluct_u3u3']]
            vals = numpy.sqrt(avg_u1_squared + avg_u2_squared + avg_u3_squared)
        else:
            vals = data[dgf2pecnik[args.name]]
        
        print(len(vals) + 1)
        print(0.0, 0.0)
        for y, v in zip(data['y'], vals):
            print(y, v)
        
        raise SystemExit(0)

    # Plot everything:
    fig = Figure()
    canvas = FigureCanvas(fig)
    ax = fig.add_subplot(1,1,1)

    if args.dgf is not None:
        coord, all_vals = read_many_group_plottables_files(group_plottables_files=args.dgf)
        vals = all_vals[args.name]
        assert any(not numpy.isnan(v) for v in vals), args.name + ' is all NaN in DGF files'
        ax.plot(abs(coord),vals,label=args.name)
        print('DGF average:',estimate_integral(coord,vals))
        for c, v in zip(coord,vals):
            if numpy.isnan(v) and c >= 0:
                ax.axvline(c,color='black',lw=0.5)

    if args.kmm is not None:
        kmm_name = dgf2kmm[args.name]
        data = read_kmm_data(kmm_files=args.kmm)
        x, y = data['y'], data[kmm_name]
        ax.plot(x,y,label='KMM')
        print('KMM average:', estimate_integral(x,y))

    if args.pecnik is not None:
        pecnik_name = dgf2pecnik[args.name]
        data = read_pecnik_datafile(datafile=args.pecnik)
        x, y = data['y'], data[pecnik_name]
        ax.plot(x,y,label='Pecnik')
        print('Pecnik average:', estimate_integral(x,y))
    
    if args.old_dgf_dirs is not None:
        old_name = 'u_3'
        data = read_old_grp_plottables_files(args.old_dgf_dirs)
        x, y = data['y'], data[old_name]
        ax.plot(x,y,label='old-style DGF')
        print('old-style DGF:', estimate_integral(x,y))

    if args.logx:
        xmin, xmax = ax.get_xlim()
        xmin = max(xmin, 1.0e-3)
        ax.set_xlim(xmin, xmax)
        autoscale_y(ax)
        ax.set_xscale('log')

    if args.domain is not None:
        ax.set_xlim(args.domain)
        autoscale_y(ax)
    
    ax.legend(loc='best')
    canvas.print_figure('group_avg.pdf',bbox_inches='tight')

