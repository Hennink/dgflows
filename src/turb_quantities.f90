module turb_quantities
  use qnty_solver_mod, only: qnty_solver_t
  use exceptions, only: error
  implicit none
  
  private
  public :: turb_solver_t

  type, extends(qnty_solver_t) :: turb_solver_t
     character(6) :: equation
  contains
    procedure, non_overridable :: collect_pde_varfrozconst
  end type turb_solver_t
  
contains
  subroutine collect_pde_varfrozconst(this, variable_terms, frozen_terms, constant_terms)
    use assertions, only: assert
    use convection, only: conv_t
    use fick_sip_mod, only: sip_ficks_law_t
    use first_order_reaction_mod, only: first_order_reaction_t
    use numerical_solutions, only: implicit_convection_for_qnty, temporal_order_for_qnty, &
                                   find_if_add_conv_corr_for_qnty
    use pde_mod, only: pde_t
    use rans_mod, only: rans_model
    use run_data, only: constant_density, dt, rans_model_description
    use scal_func_mod, only: scal_func_type
    use source_pde_term_mod, only: source_t
    use time_der_mod, only: backward_fd
    
    class(turb_solver_t), intent(in) :: this
    type(pde_t), intent(out) :: variable_terms, frozen_terms, constant_terms
    
    type(conv_t) :: conv
    type(sip_ficks_law_t) :: sip
    integer :: order
    type(first_order_reaction_t) :: turb_react
    type(source_t) :: turb_src
    
    ! Time derivative:
    order = temporal_order_for_qnty(this%qnty)
    call frozen_terms%add_term(backward_fd(dt,order))
    
    ! Convection term: always noniter, because the convective field is fixed
    call find_if_add_conv_corr_for_qnty(this%qnty,conv%add_vol_div_corr,conv%add_face_jump_corr)
    conv%explicit = .not. implicit_convection_for_qnty(this%qnty)
    call frozen_terms%add_term(conv)
    
    if (rans_model_description == 'SA') then
      ! Diffusion term: iter for SA, because nu_t changes
      call assert(this%equation == '1st_eq', 'Unknown RANS equation: ' // this%equation)
      sip%diff_param = rans_model%diff_coeff_1st_eq
      sip%add_mass_flux_term = .not. constant_density
      sip%explicit = .false.
      call variable_terms%add_term(sip)
    else
      ! Diffusion term: always noniter, because mu_t is fixed
      if (this%equation == '1st_eq') then
        sip%diff_param = rans_model%diff_coeff_1st_eq
      elseif (this%equation == '2nd_eq') then
        sip%diff_param = rans_model%diff_coeff_2nd_eq
      else
        call error('Unknown RANS equation: '//this%equation)
      endif
      sip%add_mass_flux_term = .not. constant_density
      sip%explicit = .false.
      call frozen_terms%add_term(sip)
    endif

    ! Reaction and source terms: iter because they depend on the values of the turb quantities
    if (this%equation == '1st_eq') then
      if (rans_model_description /= 'log-k-log-eps') allocate(turb_react%rate,source=rans_model%react_coeff_1st_eq)
      allocate(turb_src%src_all_dirs,source=rans_model%source_1st_eq)
    elseif (this%equation == '2nd_eq') then
      if (rans_model_description /= 'k-log-w' .and. rans_model_description /= 'log-k-log-eps') then
        allocate(turb_react%rate,source=rans_model%react_coeff_2nd_eq)
      endif
      allocate(turb_src%src_all_dirs,source=rans_model%source_2nd_eq)
    else
      call error('Unknown RANS equation: '//this%equation)
    endif
    if (rans_model_description /= 'log-k-log-eps' .and. &
        (rans_model_description /= 'k-log-w' .or. this%equation == '1st_eq')) call variable_terms%add_term(turb_react)
    call variable_terms%add_term(turb_src)
  end subroutine
end module turb_quantities
