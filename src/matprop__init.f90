submodule(matprop_mod) matprop__init
  use exceptions, only: error
  implicit none
  
contains
  module subroutine initialize()
    use func_set_expansion_mod, only: func_set_expansion_t
    use io_basics, only: open_input_file, read_line_from_input_file
    use mesh_objects, only: mat_id_is_fluid_id
    use run_data, only: file_path_matprop_specs
    use string_manipulations, only: char2int, int2char, separate_first_word
    implicit none
    
    integer :: unit_number
    logical :: EOF
    character(:), allocatable :: input_line
    character(:), allocatable :: id_char, type_of_matprop_spec
    type(func_set_expansion_t) :: matprops_exp
    integer :: mat_id
    
    call open_input_file(file_path_matprop_specs,unit_number)
      do
        call read_line_from_input_file(unit_number,input_line,EOF)
        if (EOF) exit
        if (input_line == '') cycle
        call separate_first_word(input_line,id_char,type_of_matprop_spec,':')

        mat_id = char2int(id_char)
        call get_matprops_from_file(unit_number,type_of_matprop_spec,matprops_exp)
        call add_material(mat_id,matprops_exp)
      enddo
    close(unit_number)
  end subroutine initialize
  
  subroutine get_matprops_from_file(unit_number,type_of_matprop_spec,matprop_set)
    use func_set_expansion_mod, only: func_set_expansion_t
    use io_basics, only: read_line_from_input_file
    
    integer, intent(in) :: unit_number
    character(*), intent(in) :: type_of_matprop_spec
    type(func_set_expansion_t), intent(out) :: matprop_set
    
    character(:), allocatable :: last_line
    
    select case(type_of_matprop_spec)
      case('given_list')
        call initialize_matprops_from_given_list(unit_number,matprop_set)
      case('library')
        call initialize_matprops_from_library(unit_number,matprop_set)
      case default
        call error("can't interpret type of matprop spec; type_of_matprop_spec=" // type_of_matprop_spec)
    end select
    
    call read_line_from_input_file(unit_number,last_line)
    call assert(last_line == '', "Matprop specifications should end with a blank line, but I got last_line=" // last_line)
  end subroutine get_matprops_from_file
  
  subroutine initialize_matprops_from_given_list(unit_number,matprop_set)
    use func_operations_mod, only: operator(*), operator(/)
    use func_set_mod, only: func_set_t
    use func_set_expansion_mod, only: func_set_expansion_t
    use read_funcs_mod, only: read_block_as_func_specs
    use scal_func_mod, only: scal_func_type
    
    integer, intent(in) :: unit_number
    type(func_set_expansion_t), intent(out) :: matprop_set
    
    type(func_set_t) :: matprops
    
    call read_block_as_func_specs(unit_number,matprops)
    if (.not. matprops%has('kinematic_viscosity') &
        .and. matprops%has_both('dynamic_viscosity','density')) then
      call matprops%add_func('kinematic_viscosity',matprops%func_ptr('dynamic_viscosity') / matprops%func_ptr('density'))
    endif
    if (.not. matprops%has('dynamic_viscosity') &
        .and. matprops%has_both('kinematic_viscosity','density')) then
      call matprops%add_func('dynamic_viscosity',matprops%func_ptr('kinematic_viscosity') * matprops%func_ptr('density'))
    endif
    
    if (.not. matprops%has('k_over_cp') &
        .and. matprops%has_both('thermal_conductivity','spec_heat_capacity')) then
      call matprops%add_func('k_over_cp',matprops%func_ptr('thermal_conductivity') / matprops%func_ptr('spec_heat_capacity'))
    endif
    
    call matprop_set%add_known_functions(matprops)
    call matprop_set%finalize()
  end subroutine initialize_matprops_from_given_list
  
  subroutine initialize_matprops_from_library(unit_number,matprop_set)
    use func_set_expansion_mod, only: func_set_expansion_t
    use io_basics, only: read_line_from_input_file
    use matprop_lib_patel_channel, only: get_patel_channel_matprop
    use matprop_lib_mansol
    
    integer, intent(in) :: unit_number
    type(func_set_expansion_t), intent(out) :: matprop_set
    
    character(:), allocatable :: library_name
    
    call read_line_from_input_file(unit_number,library_name)
    select case(adjustl(library_name))
      case('linear')
        call get_linear_matprop(unit_number,matprop_set)
      case('boussinesq')
        call get_boussinesq_matprop(matprop_set)
      case('metal')
        call get_metal_matprop(unit_number,matprop_set)
      case('MSFR_salt')
        call get_MSFR_salt_matprop(matprop_set)
      case('CoolProp')
        call get_cp_matprop(unit_number,matprop_set)
      case('patel_channel')
        call get_patel_channel_matprop(unit_number,matprop_set)
      case('mansol')
        call get_mansol_matprop(unit_number,matprop_set)
      case default
        error stop 'ERROR: unknown matprop library'
    end select
  end subroutine initialize_matprops_from_library
end submodule matprop__init

