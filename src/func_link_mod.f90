module func_link_mod
  use func_set_mod, only: func_set_t
  use monad_mod, only: monad_t
  use monad_of_scalfunc_mod, only: monad_of_scalfunc
  use scal_func_mod, only: scal_func_type
  implicit none
  
  private
  public :: func_link_t
  
  type :: func_link_t
    ! represents a way to map from one scal. func. to another via a monad
    class(monad_t), allocatable :: monad
    character(:), allocatable :: from, to
  contains
    generic :: init => init_with_func, init_with_monad
    procedure, non_overridable, private :: init_with_func, init_with_monad
    procedure, non_overridable :: map_if_possible
    procedure, non_overridable :: expand_if_possible
    procedure, non_overridable :: repr
    
#ifdef __INTEL_COMPILER
  ! ifort17 had a bug where it failed to free the allocatable components sometimes.
  ! gfortran9 had runtime error with this final procedure.
    final :: destroy_func_link
#endif
  end type func_link_t
  
contains
  subroutine init_with_func(this,from,to,real2real)
    use func_ptr2monad_mod, only: func_ptr2monad, real2real_i
    
    class(func_link_t), intent(out) :: this
    character(*), intent(in) :: from, to
    procedure(real2real_i) :: real2real
    
    allocate(this%from,source=from)
    allocate(this%to,source=to)
    call func_ptr2monad(real2real,this%monad)
  end subroutine init_with_func
  
  subroutine init_with_monad(this,from,to,monad)
    class(func_link_t), intent(out) :: this
    character(*), intent(in) :: from, to
    class(monad_t), intent(in) :: monad
    
    allocate(this%from,source=from)
    allocate(this%to,source=to)
    allocate(this%monad,source=monad)
  end subroutine init_with_monad
  
  subroutine map_if_possible(this,from_set,to_set)
    ! Don't call when 'from_set' and 'to_set' are the same. Perhaps such
    ! aliasing can be supported by providing 'from_set' with a VALUE attribute.
    class(func_link_t), intent(in) :: this
    class(func_set_t), intent(in) :: from_set
    class(func_set_t), intent(inout) :: to_set
    
    class(scal_func_type), allocatable :: from_func

    if (from_set%has(this%from) .and. .not. to_set%has(this%to)) then
      call from_set%get_func(this%from,from_func)
      call to_set%add_func(this%to,monad_of_scalfunc(this%monad,from_func))
    endif
  end subroutine map_if_possible
  
  subroutine expand_if_possible(this,func_set)
    class(func_link_t), intent(in) :: this
    class(func_set_t), intent(inout) :: func_set

    class(scal_func_type), allocatable :: from_func

    if (func_set%has(this%from) .and. .not. func_set%has(this%to)) then
      call func_set%get_func(this%from,from_func)
      call func_set%add_func(this%to,monad_of_scalfunc(this%monad,from_func))
    endif
  end subroutine expand_if_possible

  pure function repr(this)
    class(func_link_t), intent(in) :: this
    character(:), allocatable :: repr

    allocate(repr,source='func_link(' // this%from // ' --> ' // this%to // ')')
  end function repr
  
  
#ifdef __INTEL_COMPILER
  ! ifort17 had a bug where it failed to free the allocatable components sometimes.
  ! gfortran9 had an internal error at compile-time with this final procedures.
  impure elemental subroutine destroy_func_link(this)
    type(func_link_t), intent(inout) :: this
    
    if (allocated(this%monad)) deallocate(this%monad)
    if (allocated(this%from)) deallocate(this%from)
    if (allocated(this%to)) deallocate(this%to)
  end subroutine destroy_func_link
#endif
end module func_link_mod
