module local_global_mappings
  use assertions, only: assert, assert_eq
  use f90_kind, only: dp
  use mesh_objects, only: elem_list, elem_id, elem_ptr, &
                          face_list, face_ptr
  implicit none
  
  private
  public :: lcors2pnt_set, gcors2pnt_set
  public :: global_dg_functions
  public :: elem_global_to_local_coord
  public :: elem_lcor2gcor
  public :: face_lcor2ngbr_lcor
  public :: face_lcor2gcor
  public :: global_to_local_coords
  
contains
  subroutine lcors2pnt_set(elem,func_order,lcors,pnt_set,det_jac)
    use pnt_set_mod, only: pnt_set_t

    type(elem_list), intent(in) :: elem
    real(dp), intent(in) :: lcors(:,:)
    integer, intent(in) :: func_order
    type(pnt_set_t), intent(out) :: pnt_set
    real(dp), allocatable, optional, intent(out) :: det_jac(:)

    integer :: p

    call assert(size(lcors,1) == elem%dimen())

    pnt_set%lcors = lcors
    if (present(det_jac)) then
      call global_dg_functions(elem,func_order,lcors,pnt_set%dg_funcs,pnt_set%dg_transp_gders,det_jac)
    else
      call global_dg_functions(elem,func_order,lcors,pnt_set%dg_funcs,pnt_set%dg_transp_gders)
    endif
    allocate(pnt_set%gcors,mold=lcors)
    do p = 1, size(lcors,2)
      call elem_lcor2gcor(elem,lcors(:,p),pnt_set%gcors(:,p))
    enddo
  end subroutine lcors2pnt_set
  
  subroutine gcors2pnt_set(elem,func_order,gcors,pnt_set,det_jac)
    use pnt_set_mod, only: pnt_set_t

    type(elem_list), intent(in) :: elem
    real(dp), intent(in) :: gcors(:,:)
    integer, intent(in) :: func_order
    type(pnt_set_t), intent(out) :: pnt_set
    real(dp), allocatable, optional, intent(out) :: det_jac(:)

    integer :: p

    call assert(size(gcors,1) == elem%dimen())

    pnt_set%gcors = gcors
    allocate(pnt_set%lcors,mold=gcors)
    do p = 1, size(gcors,2)
      call elem_global_to_local_coord(elem,elem%dimen(),gcors(:,p),pnt_set%lcors(:,p))
    enddo
    if (present(det_jac)) then
      call global_dg_functions(elem,func_order,pnt_set%lcors,pnt_set%dg_funcs,pnt_set%dg_transp_gders,det_jac)
    else
      call global_dg_functions(elem,func_order,pnt_set%lcors,pnt_set%dg_funcs,pnt_set%dg_transp_gders)
    endif
  end subroutine gcors2pnt_set

  subroutine elem_lcor2gcor(elem,lcor,gcor)
    ! This routine finds the global coordinates (gcor) for a given local
    ! coordinate (lcor) on element (elem).
    !
    ! The coordinate x can be written using nodal basis functions h(lcor) as:
    ! x(lcor) = sum_v h_v(lcor)*x_v.
    use local_elem, only: nodal_functions_single_point
    
    type(elem_list), intent(in) :: elem
    real(dp), dimension(:), intent(in) :: lcor
    real(dp), dimension(:), intent(out) :: gcor
    
    integer :: no_vertices
    real(dp), allocatable, dimension(:,:) :: vertices
    real(dp), allocatable, dimension(:) :: fun_values
    
    call assert(size(lcor) == size(gcor))

    call elem%glob_coord_vertices(vertices)
    no_vertices = size(vertices,2)
    allocate(fun_values(no_vertices))
    call nodal_functions_single_point(elem%eltype,lcor,fun_values)
    gcor = matmul(vertices,fun_values)
  end subroutine elem_lcor2gcor
  
  subroutine global_to_local_coords(mesh,only_fluid_mesh,no_points,global_coords,lcors,elem_array,guess_elem_list)
    use mesh_type, only: fem_mesh
    
    type(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid_mesh
    integer, intent(in) :: no_points
    real(dp), dimension(mesh%dimen,no_points), intent(in) :: global_coords
    real(dp), allocatable, dimension(:,:), intent(out) :: lcors
    type(elem_ptr), allocatable, dimension(:), intent(out) :: elem_array
    type(elem_id), target, optional, intent(in) :: guess_elem_list(:)
    
    integer :: point_no
    type(elem_ptr) :: elem_guess
    
    allocate(elem_array(no_points))
    allocate(lcors(mesh%dimen,no_points))
    
    do point_no = 1, no_points
      ! for each point, find the corresponding local coordinates and element.
      ! In order to reduce the computational cost a guess is made regarding the element,
      ! taking the element associated to the previous point.
      ! Skip this if a guess_elem_list is passed in input, because the computational cost
      ! is already reduced by looping over the elements in this list, rather than in the entire mesh.
      if ((point_no > 1) .and. (.not. present(guess_elem_list))) then
        if (associated(elem_guess%point)) nullify(elem_guess%point)
        elem_guess%point => elem_array(point_no-1)%point
      endif
      
      call find_local_coord_and_elem(mesh,only_fluid_mesh,elem_guess,global_coords(:,point_no), &
                                     lcors(:,point_no),elem_array(point_no),guess_elem_list)
      
      if (.not.(associated(elem_array(point_no)%point))) then
        error stop 'global_to_local_coords: conversion failed, no element found for at least one point.'
      else
        if (only_fluid_mesh .and. (.not. elem_array(point_no)%point%is_fluid())) &
          error stop 'global_to_local_coords: at least one element is outside the selected fluid mesh.'
      endif
    enddo
  end subroutine global_to_local_coords
  
  subroutine global_dg_functions(elem,order,lcors,func,transp_gder,det_jac)
    use lapack_interface_mod, only: solve_linsys_keep_matrix
    use local_elem, only: local_dg_functions
    use run_data, only: assume_affine_elems
    use support, only: determinant
    
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: order
    real(dp), dimension(:,:), intent(in) :: lcors
    real(dp), allocatable, optional, intent(out) :: func(:,:), transp_gder(:,:,:), det_jac(:)
    
    integer :: p, np
    real(dp), allocatable :: jacs(:,:,:), gder(:,:,:)
    
    call assert(size(lcors,1) == elem%dimen())

    np = size(lcors,2)
    call jacobians(elem,lcors,jacs)
    if (present(func) .and. present(transp_gder)) then
      call local_dg_functions(elem%eltype,order,lcors,func,gder)
    elseif (present(func)) then
      call local_dg_functions(elem%eltype,order,lcors,func)
    elseif (present(transp_gder)) then
      call local_dg_functions(elem%eltype,order,lcors,local_deriv=gder)
    endif
    if (present(det_jac)) allocate(det_jac(np))
    if (present(transp_gder)) allocate(transp_gder(size(gder,2),size(gder,1),np))
    do p = 1, np
      if (present(transp_gder)) then
        call solve_linsys_keep_matrix(jacs(:,:,p),gder(:,:,p))
        transp_gder(:,:,p) = transpose(gder(:,:,p))
      endif
      if (present(det_jac)) det_jac(p) = determinant(jacs(:,:,p))
    enddo
    if (present(det_jac) .and. assume_affine_elems) &
        det_jac = assert_eq(det_jac,'assume_affine_elems should have implied constant Jacobians.')
  end subroutine global_dg_functions
  
  subroutine jacobians(elem,lcors,jacs)
    ! jacobian := derivative of global_coord w.r.t. local_coord
    use local_elem, only: nodal_functions
    
    type(elem_list), intent(in) :: elem
    real(dp), dimension(:,:), intent(in) :: lcors
    real(dp), allocatable, dimension(:,:,:), intent(out) :: jacs
    
    integer :: dimen
    real(dp), allocatable, dimension(:,:) :: vertices
    integer :: p, np
    real(dp), allocatable, dimension(:,:,:) :: loc_deriv_nodal_func
    
    dimen = assert_eq(size(lcors,1),elem%dimen())
    np = size(lcors,2)
    call elem%glob_coord_vertices(vertices)
    call nodal_functions(elem%eltype,lcors,local_deriv=loc_deriv_nodal_func)
    allocate(jacs(dimen,dimen,np))
    do p = 1, np
      jacs(:,:,p) = matmul(loc_deriv_nodal_func(:,:,p),transpose(vertices))
    enddo
  end subroutine jacobians
  
  recursive subroutine find_local_coord_and_elem(mesh,only_fluid_mesh,elem_guess,global_coord,local_coord,element,guess_elem_list)
    use mesh_type, only: fem_mesh
    
    type(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid_mesh
    type(elem_ptr), intent(inout) :: elem_guess
    real(dp), dimension(mesh%dimen), intent(in) :: global_coord
    real(dp), dimension(mesh%dimen), intent(out) :: local_coord
    type(elem_ptr), intent(out) :: element
    type(elem_id), target, optional, intent(in) :: guess_elem_list(:)
    
    integer :: elem_no
    integer :: i
    type(elem_list), pointer :: elem, neighbor
    type(face_list), pointer :: sub_face
    type(elem_ptr), allocatable, dimension(:) :: active_neighbors
    type(face_ptr), allocatable, dimension(:) :: active_subfaces
    type(elem_id) :: id
    type(elem_id), pointer, dimension(:) :: active_elem_list
    integer :: no_active_elem
    logical :: conversion_failed
    
    
    if (.not. associated(elem_guess%point)) then
      ! No guess: we need to loop over all the elements (of the right portion of mesh), or
      ! of the guess_elem_list passed in input.
      if (present(guess_elem_list)) then
        no_active_elem = size(guess_elem_list)
        active_elem_list => guess_elem_list
      else
        no_active_elem = mesh%get_no_active_elem(only_fluid_mesh)
        call mesh%get_active_elem_list_ptr(only_fluid_mesh,active_elem_list)
      endif
      do elem_no = 1, no_active_elem
        id = active_elem_list(elem_no)
        elem => mesh%get_elem_ptr(id)
        
        call elem_global_to_local_coord(elem,mesh%dimen,global_coord,local_coord,conversion_failed)
        if (.not. conversion_failed) then 
          element%point => elem
          return
        endif
      enddo
    else
      ! A guess is available for the element. In this case:
      ! 1) search if the point is on the guessed element
      ! 2) if fails, search among the neighbors
      ! 3) if fails, points are more or less randomly scattered, 
      !    probably faster to loop over all the elements
      !
      ! THERE IS PROBABLY ROOM FOR IMPROVEMENTS
      
      ! First step
      elem => elem_guess%point
      call elem_global_to_local_coord(elem,mesh%dimen,global_coord,local_coord,conversion_failed)
      if (.not. conversion_failed) then 
        element%point => elem
        return
      else
        ! Second step: loop over the neighbors
        call elem%active_subfaces(active_subfaces,active_neighbors)
        do i = 1, size(active_subfaces)
          sub_face => active_subfaces(i)%point
          if (.not. sub_face%is_boundary(only_fluid_mesh)) then
            neighbor => active_neighbors(i)%point
            call elem_global_to_local_coord(neighbor,mesh%dimen,global_coord,local_coord,conversion_failed)
            if (.not. conversion_failed) then 
              element%point => neighbor
              return
            endif
          endif
        enddo
        
        ! Third step: if the code reaches this section, it means that the point is not 
        ! located in the guess element nor in its neighbors. Therefore it's easier to 
        ! start from the beginning and loop over all the elements.
        elem_guess%point => null()
        call find_local_coord_and_elem(mesh,only_fluid_mesh,elem_guess,global_coord,local_coord,element)
      endif
    endif
  end subroutine find_local_coord_and_elem
  
  subroutine elem_global_to_local_coord(elem,dimen,gcor,lcor,conversion_failed)
    ! Uses Newton iteration to find the global coord. corresponding to a local coord.
    !
    ! Use the 'extra_steps' parameter for extra accuracy after convergence.
    ! 
    ! The coordinate x can be written using nodal basis functions h(lcor) as:
    !     x(lcor) = sum_v h_v(lcor)*x_v.
    ! For the Newton method we write the problem as:
    !     error(lcor)  =  x - sum_v h_v(lcor)*x_v  =  0
    ! which leads to iteration of the form
    !     (grad error) . delta_lcor = -error(lcor^k)
    !     lcor^k+1 = lcor^k + delta_lcor
    use exceptions, only: error
    use lapack_interface_mod, only: solve_linsys_lose_matrix
    use local_elem, only: coord_in_domain, nodal_functions_single_point
    use math_const, only: SMALL
    use pprint_mod, only: pprint
    
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: dimen
    real(dp), dimension(dimen), intent(in) :: gcor
    real(dp), dimension(dimen), intent(out) :: lcor
    logical, optional, intent(out) :: conversion_failed
    
    integer, parameter :: maxiter = 10, extra_steps = 1
    integer :: iter
    integer :: no_vertices
    real(dp), dimension(dimen) :: charac_glob_lens, err
    logical, dimension(dimen) :: small_errs, small_steps
    real(dp), allocatable :: vertices(:,:)
    real(dp), allocatable :: fun_values(:), fun_derivs_local(:,:)
    real(dp) :: Jac(dimen,dimen)
    logical :: conv_history(1-extra_steps:maxiter+extra_steps), converged, in_elem
    
    call elem%glob_coord_vertices(vertices)
    no_vertices = size(vertices,2)
    allocate(fun_values(no_vertices))
    allocate(fun_derivs_local(dimen,no_vertices))
    charac_glob_lens = maxval(vertices,dim=2) - minval(vertices,dim=2)
    if (extra_steps > 0) conv_history(:0) = .false.
    
    ! Newton iteration
    lcor = 0.0_dp ! initial guess could be improved
    do iter = 1, maxiter + extra_steps
      call nodal_functions_single_point(elem%eltype,lcor,fun_values,fun_derivs_local)
      err = gcor - matmul(vertices,fun_values)
      small_errs = err < SMALL * charac_glob_lens
      Jac = matmul(vertices,transpose(fun_derivs_local))
      call solve_linsys_lose_matrix(Jac,err)
      small_steps = err < SMALL
      lcor = lcor + err
      conv_history(iter) = all(small_steps .and. small_errs)
      converged = all(conv_history(iter-extra_steps:iter))
      if (converged) exit
    enddo
    if (present(conversion_failed)) then
      in_elem = coord_in_domain(elem%eltype,lcor)
      conversion_failed = .not. (converged .and. in_elem)
    elseif (.not. converged) then
      call pprint('gcors vertices:',vertices)
      print *, conv_history
      call error('failed to map gcor to lcor')
    endif
  end subroutine elem_global_to_local_coord
  
  subroutine face_lcor2gcor(face,lcor,gcor)
    use local_elem, only: convert_face_to_vol_local_coord
    use run_data, only: mesh_dimen
    
    type(face_list), intent(in) :: face
    real(dp), intent(in) :: lcor(:)
    real(dp), intent(out) :: gcor(:)

    integer :: dimen
    real(dp) :: lcor_E1(size(gcor))
    
    dimen = assert_eq(size(lcor)+1,size(gcor),mesh_dimen)
    
    associate( E1 => face%elem_neighbors(1)%point, &
               face_idx_in_E1 => face%neighbor_loc_face_no(1) )
      call convert_face_to_vol_local_coord(E1%eltype,face_idx_in_E1,dimen,lcor,lcor_E1)
      call elem_lcor2gcor(E1,lcor_E1,gcor)
    end associate
  end subroutine face_lcor2gcor
  
  subroutine face_lcor2ngbr_lcor(face,lcor_face,lcor_E1,lcor_E2)
    use run_data, only: mesh_dimen
    use local_elem, only: convert_face_to_vol_local_coord
    use mesh_type, only: periodicity_shift
    
    type(face_list), intent(in) :: face
    real(dp), intent(in) :: lcor_face(:,:)
    real(dp), dimension(:,:), intent(out) :: lcor_E1
    real(dp), dimension(:,:), optional, intent(out) :: lcor_E2
    
    integer :: p, np
    integer :: dimen
    real(dp), dimension(mesh_dimen) :: gcor_E1, gcor_E2, shift, shift_gcor_1_to_2
    
    dimen = assert_eq(size(lcor_face,1) + 1,size(lcor_E1,1),mesh_dimen)
    np = assert_eq(size(lcor_face,2),size(lcor_E1,2))
    if (present(lcor_E2)) call assert(all(shape(lcor_E1) == shape(lcor_E2)))

    associate(E1 => face%elem_neighbors(1)%point)
      do p = 1, np
        call convert_face_to_vol_local_coord(E1%eltype,face%neighbor_loc_face_no(1),dimen,lcor_face(:,p),lcor_E1(:,p))
      enddo
    end associate
    
    if (present(lcor_E2)) then
      associate( E1 => face%elem_neighbors(1)%point, &
                 E2 => face%elem_neighbors(2)%point )
        if (face%is_periodic) then
          shift = periodicity_shift(:,face%periodic_id)
          shift_gcor_1_to_2 = merge(shift,-shift,dot_product(shift,E2%com()-E1%com()) > 0.0_dp)
        else
          shift_gcor_1_to_2 = 0.0_dp
        endif

        do p = 1, np
          call elem_lcor2gcor(E1,lcor_E1(:,p),gcor_E1)
          gcor_E2 = gcor_E1 + shift_gcor_1_to_2
          call elem_global_to_local_coord(E2,dimen,gcor_E2,lcor_E2(:,p))
        enddo
      end associate
    endif
  end subroutine face_lcor2ngbr_lcor
end module local_global_mappings

