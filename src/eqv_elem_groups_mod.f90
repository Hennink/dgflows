module eqv_elem_groups_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use mpi_wrappers_mod, only: bcast_wrap
  use mesh_objects, only: elem_id, elem_list, elem_ptr
  use mesh_type, only: fem_mesh
  use petsc_mod, only: petsc_proc_id
  use run_data, only: mesh_dimen
  use fortran_parser_interface_mod, only: add_eqparser, evaluate
  implicit none

  private
  public :: elem_groups_t

  type elem_groups_t
    private
    integer, allocatable :: eqparser_ids(:)
    real(dp), allocatable :: uniq_idvecs(:,:)
  contains
    procedure, non_overridable :: init => init_elem_groups
    procedure, non_overridable :: group_of_elem, ngroups
    procedure, non_overridable :: size_id_vec, id_vec, idvec_at_gcor
    procedure, non_overridable :: write_groups_to_gmsh, &
                                  write_vals_on_groups_to_gmsh
  end type elem_groups_t
    
  logical, parameter :: ONLY_FLUID = .true.
  
contains
  subroutine init_elem_groups(this,mesh,id_vec_spec)
    ! Initializes the group identifyer, and initializes 'uniq_idvecs' as a list
    ! of all unique group-identifying vectors of the elements in all processes.
    use string_manipulations, only: split
    use string_mod, only: as_chars
    
    class(elem_groups_t), intent(out) :: this
    type(fem_mesh), intent(in) :: mesh
    character(*), intent(in) :: id_vec_spec

    character(*), parameter :: avg_xyz(3) = 'avg_' // ['x','y','z']
    character(:), allocatable :: components_specs(:)
    integer :: size_id_vec, i

    integer :: elem_no
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    type(elem_id), pointer :: active_elem_list(:)
    integer :: ngroups

    call assert(id_vec_spec /= '','Are all elements really the same?')
    
    components_specs = as_chars(split(id_vec_spec,','))
    size_id_vec = size(components_specs)
    allocate(this%eqparser_ids(size_id_vec))
    do i = 1, size_id_vec
      call add_eqparser(components_specs(i),avg_xyz(:mesh_dimen),this%eqparser_ids(i))
    enddo

    ! A single process can do all the work, because all processes have the
    ! entire mesh.
    ! Parallelizing the work would require non-trivial communication.
    if (petsc_proc_id() == 0) then
      allocate(this%uniq_idvecs(this%size_id_vec(),0))
      call mesh%get_active_elem_list_ptr(ONLY_FLUID,active_elem_list)
      do elem_no = 1, size(active_elem_list)
        id = active_elem_list(elem_no)
        elem => mesh%get_elem_ptr(id)
        call insert_uniq_col(this%uniq_idvecs,this%id_vec(elem))
      enddo
      ngroups = size(this%uniq_idvecs,2)
    endif

    call bcast_wrap(ngroups,0)
    if (.not. petsc_proc_id() == 0) allocate(this%uniq_idvecs(this%size_id_vec(),ngroups))
    call bcast_wrap(this%uniq_idvecs,0)
  end subroutine init_elem_groups

  pure integer function size_id_vec(this)
    class(elem_groups_t), intent(in) :: this

    size_id_vec = size(this%eqparser_ids)
  end function size_id_vec

  function idvec_at_gcor(this,gcor) result(id_vec)
    class(elem_groups_t), intent(in) :: this
    real(dp), intent(in) :: gcor(:)
    real(dp) :: id_vec(this%size_id_vec())

    integer :: i

    call assert(size(gcor) == mesh_dimen, BOUNDS_CHECK)

    do i = 1, this%size_id_vec()
      id_vec(i) = evaluate(this%eqparser_ids(i),gcor)
    enddo
  end function

  function id_vec(this,elem)
    ! Returns a vector that uniquely identifies the group to which 'elem'
    ! belongs. That is, there is a one-to-one correspondence between the group
    ! and this vector.
    class(elem_groups_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    real(dp) :: id_vec(this%size_id_vec())
    
    id_vec = this%idvec_at_gcor(elem%avg_vtx())
  end function id_vec

  integer function group_of_elem(this,elem) result(col_idx)
    class(elem_groups_t), intent(in) :: this
    type(elem_list), intent(in) :: elem

    logical :: found

    call bisect_col_ordered_mtx(this%uniq_idvecs,this%id_vec(elem),col_idx,found)

    call assert(found,"group_of_elem: I do not recognize the ID vector of this elem.",BOUNDS_CHECK)
  end function

  pure integer function ngroups(this)
    class(elem_groups_t), intent(in) :: this

    ngroups = size(this%uniq_idvecs,2)
  end function ngroups

  subroutine write_groups_to_gmsh(this,name,mesh)
    class(elem_groups_t), intent(in) :: this
    character(*), intent(in) :: name
    type(fem_mesh), intent(in) :: mesh
    
    real(dp) :: range(this%ngroups())
    integer :: i
    
    range = [( i, i = 1, size(range) )]
    call this%write_vals_on_groups_to_gmsh(name,mesh,range)
  end subroutine write_groups_to_gmsh

  subroutine write_vals_on_groups_to_gmsh(this,name,mesh,values)
    use gmsh_interface, only: write_gmsh_elementwise_sol
    use mesh_partitioning, only: my_first_last_elems

    class(elem_groups_t), intent(in) :: this
    character(*), intent(in) :: name
    type(fem_mesh), intent(in) :: mesh
    real(dp), intent(in) :: values(:)

    integer :: elem_no, first_elem, last_elem
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    integer :: group
    real(dp), allocatable :: my_elementwise_values(:)

    call assert(size(values) == this%ngroups(),'write_vals_on_groups_to_gmsh: bad size(values)')

    call my_first_last_elems(ONLY_FLUID,first_elem,last_elem)
    allocate(my_elementwise_values(first_elem:last_elem))
    call mesh%get_active_elem_list_ptr(ONLY_FLUID,active_elem_list)
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      group = this%group_of_elem(elem)
      my_elementwise_values(elem_no) = values(group)
    enddo
    call write_gmsh_elementwise_sol(name,my_elementwise_values,mesh,ONLY_FLUID,name//'.pos')
  end subroutine write_vals_on_groups_to_gmsh


  subroutine insert_uniq_col(matrix,col)
    ! Check whether `col` is in the column-sorted matrix. If not, this inserts
    ! `col` at the appropriate location.
    real(dp), allocatable, intent(inout) :: matrix(:,:)
    real(dp), intent(in) :: col(:)

    integer :: loc
    logical :: found
    real(dp), allocatable :: temp(:,:)

    call bisect_col_ordered_mtx(matrix,col,loc,found)

    if (.not. found) then
      allocate(temp(size(matrix,1),1+size(matrix,2)))
      temp(:,:loc-1) = matrix(:,:loc-1)
      temp(:,loc) = col
      temp(:,loc+1:) = matrix(:,loc:)
      call move_alloc(temp,matrix)
    endif
  end subroutine insert_uniq_col

  subroutine bisect_col_ordered_mtx(matrix,col,loc,found)
    ! Find a column in a column-sorted `matrix`, using `compare_vecs` for
    ! the inequalities:
    ! 
    !     matrix(:,loc-1) < col <= matrix(:,loc) ,
    ! 
    ! where `matrix(:,loc)` should be interpreted as `-oo` for `loc == 0`,
    ! and `+oo` for `loc = 1 + size(matrix,2)`.
    ! 
    ! Return `1` if `size(matrix,2) == 0`.
    real(dp), intent(in) :: matrix(:,:), col(:)
    integer, intent(out) :: loc
    logical, intent(out) :: found

    integer :: up, low, mid

    low = 0
    up = 1 + size(matrix,2)
    do
      if (up - low <= 1) exit

      mid = (low + up) / 2
      if (compare_vecs(col,matrix(:,mid)) <= 0) then
        up = mid
      else
        low = mid
      endif
    enddo
    loc = up
    if (loc > size(matrix,2)) then
      found = .false.
    else
      found = compare_vecs(col,matrix(:,loc)) == 0
    endif
  end subroutine

  integer function compare_vecs(vec1,vec2)
    use run_data, only: eqv_elem_group_abstol
    use support, only: iseq

    real(dp), intent(in) :: vec1(:), vec2(:)

    integer :: i

    do i = 1, assert_eq(size(vec1),size(vec2),BOUNDS_CHECK)
      if (abs(vec1(i) - vec2(i)) > eqv_elem_group_abstol) then
        compare_vecs = merge(1,-1,vec1(i) > vec2(i))
        return
      endif
    enddo
    compare_vecs = 0
  end function compare_vecs
end module eqv_elem_groups_mod
