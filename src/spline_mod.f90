module spline_mod
  ! cubic splines, with implementation based on [NumRec77] and [NumRec90].
  ! 
  ! [NumRec77] William H. Press, Brian P. Flannery, Saul A. Teukolsky, 
  !            William T. Vetterling, "Numerical Recipes: the Art of Scientific
  !            Computing", 1987, Cambridge University Press
  ! [NumRec90] William H. Press, Saul A. Teukolsky, William T. Vetterling, 
  !            Brian P. Flannery, "Numerical Recipes in Fortran 90: the Art of
  !            Parallel Scientific Computing", 2nd edition, 1996, Cambridge 
  !            University Press
  !  
  use assertions, only: assert, assert_eq, assert_normal
  use f90_kind, only: dp
  use monad_mod, only: monad_t
  implicit none
  
  private
  public :: cubic_spline_t
  
  type, extends(monad_t) :: cubic_spline_t
    private
    logical, public :: saturate_ends = .true.
    real(dp), allocatable, public :: xa(:), ya(:)
    real(dp), allocatable :: y2a(:)
    integer :: old_locate = 1
    
  contains
    private
    procedure, public :: value, deriv
    procedure, public :: from_table
    procedure, public :: plot
    procedure :: splint, locate
  end type cubic_spline_t
  
contains
  impure elemental real(dp) function value(this,arg)
    class(cubic_spline_t), intent(in) :: this
    real(dp), intent(in) :: arg
    
    if (arg <= this%xa(1)) then
      call assert(this%saturate_ends)
      value = this%ya(1)
    elseif (arg >= this%xa(size(this%xa))) then
      call assert(this%saturate_ends)
      value = this%ya(size(this%ya))
    else
      value = this%splint(arg,deriv=.false.)
    endif
  end function value
  
  real(dp) function deriv(this,arg)
    class(cubic_spline_t), intent(in) :: this
    real(dp), intent(in) :: arg

    if (arg <= this%xa(1)) then
      call assert(this%saturate_ends)
      deriv = 0.0_dp
    elseif (arg >= this%xa(size(this%xa))) then
      call assert(this%saturate_ends)
      deriv = 0.0_dp
    else
      deriv = this%splint(arg,deriv=.true.)
    endif
  end function deriv

  subroutine from_table(this,xa,ya,yp1,ypn)
    ! Given
    !   * the arrays xa and ya, which tabulate a function (with the xa's in
    !     strictly increasing order),
    !   * yp1, ypn, which are the derivatives at the first and last values in xa,
    ! this routine initializes a cubic spline.
    class(cubic_spline_t), intent(out) :: this
    real(dp), intent(in) :: xa(:), ya(:), yp1, ypn
    
    call assert_normal(xa)
    call assert_normal(ya)
    call assert_normal(yp1)
    call assert_normal(ypn)
    call assert(size(xa) == size(ya),size(xa) >= 2)

    allocate(this%xa,source=xa)
    allocate(this%ya,source=ya)
    allocate(this%y2a,mold=ya)
    call spline(this%xa,this%ya,yp1,ypn,this%y2a)
  end subroutine from_table
  
  subroutine plot(this,xlabel,ylabel)
    ! Writes a Python-generated plot of the spline to a file.
    ! Does nothing when called by a non-master process.
    !
    ! https://github.com/jacobwilliams/pyplot-fortran
    use petsc_mod, only: I_am_master_process
    use pyplot_module, only: pyplot

    class(cubic_spline_t), intent(in) :: this
    character(*), intent(in) :: xlabel, ylabel

    type(pyplot) :: plt
    integer :: istat
    character(:), allocatable :: filepath

    if (.not. i_am_master_process()) return

    call plt%initialize(use_oo_api=.true.,grid=.true., &
                        xlabel=xlabel,ylabel=ylabel,   &
                        title='cubic spline',legend=.false.)
    call plt%add_plot(this%xa,this%ya,label='',linestyle='-o',linewidth=1,istat=istat)
    call assert(istat==0,"problem in pyplot's 'add_plot'")
    filepath = xlabel//'-'//ylabel//'.png'
    call plt%savefig(filepath,istat=istat)
    call assert(istat==0,"problem in pyplot's 'savefig'")
    write(*,'(a)') 'wrote spline figure to ' // filepath
  end subroutine plot
  
  real(dp) function splint(this,x,deriv)
    ! Based on the 'splint' routine in [NumRec90, page 1045]. See also 
    ! [NumRec77, pages 86-89].
    class(cubic_spline_t), intent(in) :: this
    real(dp), intent(in) :: x
    logical, intent(in) :: deriv
    
    integer :: i
    real(dp) :: a, b, h
    
    i = this%locate(x)
    h = this%xa(i+1) - this%xa(i)
    a = (this%xa(i+1) - x) / h
    b = 1 - a
    if (deriv) then
      splint = (this%ya(i+1) - this%ya(i)) / h                    &
                 + (   (b**2/2 - 1.0_dp/6.0_dp) * this%y2a(i+1)   &
                     - (a**2/2 - 1.0_dp/6.0_dp) * this%y2a(i) ) * h
    else
      splint =   a * this%ya(i) + b * this%ya(i+1)  &
                 + (   (a**3-a) * this%y2a(i)       &
                     + (b**3-b) * this%y2a(i+1) ) * h**2/6
    endif
  end function splint
  
  integer function locate(this,x)
    ! Given a value x, such that
    !     this%xa(1) < x < this%xa(N),
    ! returns a value j, such that
    !     this%xx(j) <= x <= this%xx(j + 1),
    ! assuming that this%xa is strictly monotonically increasing.
    class(cubic_spline_t), intent(in) :: this
    real(dp), intent(in) :: x
    
    integer :: low, up, midpoint
    
    low = 1
    up = size(this%xa)

    ! Bisect between upper and lower bounds:
    do
      if (up - low <= 1) exit
      midpoint = (up + low) / 2
      if (this%xa(midpoint) <= x) then
        low = midpoint
      else
        up = midpoint
      end if
    end do
    locate = low
    ! this%old_locate = locate

    call assert(this%xa(locate) <= x .and. x <= this%xa(locate+1),"locate: bad output")
  end function locate
  
  subroutine spline(x,y,yp1,ypn,y2)
    ! Given 
    !   * arrays x and y of length N containing a tabulated function, i.e.,
    !       yi = f(xi), with x1 < x2 < .. . < xN, 
    !   * values yp1 and ypn for the first derivative of the interpolating 
    !     function at points 1 and N, respectively,
    ! this routine returns an array y2 of length N that contains the second 
    ! derivatives of the interpolating function at the tabulated points xi.
    ! 
    ! If yp1 and/or ypn are equal to 1e30 or larger, the routine is signaled
    ! to set the corresponding boundary condition for a natural spline, with 
    ! zero second derivative on that boundary.
    ! 
    ! Adapted from [NumRec90, page 1044]. See also [NumRec77, page 88].
    ! 
    real(dp), dimension(:), intent(in) :: x,y
    real(dp), intent(in) :: yp1, ypn
    real(dp), dimension(:), intent(out) :: y2
    
    integer :: n
    real(dp), dimension(size(x)) :: a, b, c, r
    
    n = assert_eq(size(x),size(y),size(y2))
    call assert(all(x(2:)-x(:n-1) > 0.0_dp),"x-values in spline should increase monotonically")
    
    c(1:n-1) = x(2:n) - x(1:n-1)
    r(1:n-1) = 6.0_dp * ((y(2:n)-y(1:n-1)) / c(1:n-1))
    r(2:n-1) = r(2:n-1) - r(1:n-2)
    a(2:n-1) = c(1:n-2)
    b(2:n-1) = 2.0_dp * (c(2:n-1) + a(2:n-1))
    b(1) = 1.0_dp
    b(n) = 1.0_dp
    if (yp1 >= 1.0e30_dp) then
      r(1) = 0.0_dp
      c(1) = 0.0_dp
    else
      r(1)=(3.0_dp/(x(2)-x(1))) * ((y(2)-y(1)) / (x(2)-x(1)) - yp1)
      c(1) = 0.5_dp
    endif
    
    if (ypn >= 1.0e30_dp) then
      r(n) = 0.0_dp
      a(n) = 0.0_dp
    else
      r(n) = (-3.0_dp/(x(n)-x(n-1))) * ((y(n)-y(n-1)) / (x(n)-x(n-1)) - ypn)
      a(n) = 0.5_dp
    endif
    call tridag(a(2:n),b(1:n),c(1:n-1),r(1:n),y2(1:n))
  end subroutine spline
  
  subroutine tridag(a,b,c,rhs,u)
    ! Solves for a vector u of size N from a tridiagonal linear system. Input 
    ! vectors b (diagonal elements) and rhs have size N, while a and c 
    ! (off-diagonal elements) are size N-1.
    ! 
    ! Adapted from the 'tridag_ser' routine in [NumRec90, page 1018]. For a 
    ! justification for the lack of pivoting, see [NumRec77, page 40].
    use math_const, only: SMALL
    
    real(dp), dimension(:), intent(in) :: a, b, c, rhs
    real(dp), dimension(:), intent(out) :: u
    
    real(dp), dimension(size(b)) :: workspace
    integer :: n, j
    real(dp) :: bet
    
    n = assert_eq(size(a)+1,size(b),size(c)+1,size(rhs),size(u))
    bet = b(1)
    call assert(abs(bet) >= SMALL,'rewrite equations as a set of order N-1, with u2 trivially eliminated')
    u(1) = rhs(1) / bet
    do j = 2, n
      workspace(j) = c(j-1) / bet
      bet = b(j) - a(j-1) * workspace(j)
      call assert(abs(bet) >= SMALL)
      u(j) = (rhs(j) - a(j-1) * u(j-1)) / bet
    end do
    do j = n-1, 1, -1 ! backsubstitution
      u(j) = u(j) - workspace(j+1) * u(j+1)
    end do
  end subroutine tridag
end module spline_mod

