module continuity_penal_mod
  use assertions, only: assert, BOUNDS_CHECK
  use exceptions, only: error
  use boundary_condition_types
  use characteristic_len_mod, only: isotropic_charact_len
  use f90_kind, only: dp
  use lincom_rank1_mod, only: lincom_rank1_t, lincom_for_n_dot_rank1
  use lincom_rank2_mod, only: lincom_for_div
  use local_linsys_mod, only: local_linsys_t
  use mesh_objects, only: elem_list, face_list
  use numerical_solutions, only: velocity
  use pde_term_mod, only: pde_term_t
  use run_data, only: dt, div_m_pen_pointwise, div_m_pen_safety_factor, m_penalties_in, m_p_strategy, mesh_dimen, cont_m_pen_mode
  use solvec_mod, only: solvec_t
  use scal_func_mod, only: scal_func_type
  implicit none
  
  private
  public :: continuity_penal_t
  public :: continuity_penalty_at_qps
  public :: preset_norm_u

  class(scal_func_type), allocatable :: preset_norm_u ! square of velocity. Used instead of the instantaneous numerical velocity.

  type, extends(pde_term_t) :: continuity_penal_t
  contains
    procedure, non_overridable :: ngbr_coupling_depth
    procedure, non_overridable :: process_elem
    procedure, non_overridable :: process_face
  end type

  logical, parameter :: ONLY_FLUID = .true.

contains
  pure integer function ngbr_coupling_depth(this)
    class(continuity_penal_t), intent(in) :: this

    ngbr_coupling_depth = 1
  end function

  subroutine process_elem(this,elem,qnty,local_linsys)
    ! divergence penalty: \int "penalty" div(u) div(v)
    ! 
    class(continuity_penal_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys

    type(lincom_rank1_t) :: div_func
    real(dp), allocatable :: quot_x_penalty(:)

    call assert(qnty%name == 'mass_flux', 'continuity penal. is for m', BOUNDS_CHECK)

    ! Checking whether the 'safety factor' is zero is a hack; a flag would be better.
    if (div_m_pen_safety_factor == 0.0_dp) return

    div_func = lincom_for_div(elem,qnty%handler)
    quot_x_penalty = elem%quot * div_penalty_at_qps(elem,dt)
    call local_linsys%add_int_dotprod_to_lhs(div_func,div_func,quot_x_penalty,1.0_dp,explicit=.false.)
    deallocate(quot_x_penalty)
  end subroutine

  subroutine process_face(this,face,Ein_is_E1,qnty,local_linsys)
    ! continuity penalty: \int_F  "penalty" ([u] \cdot n)([v] \cdot n)
    ! 
    use boundaries, only: get_bc

    class(continuity_penal_t), intent(in) :: this
    type(face_list), intent(in) :: face
    logical, intent(in) :: Ein_is_E1
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys

    logical :: boundary_face
    class(bc_type), allocatable :: bc
    integer :: side_Ein, side_Eout
    integer :: qp
    type(lincom_rank1_t) :: func_Ein, func_Eout, n_dot_func_Ein, n_dot_func_Eout
    real(dp), allocatable :: quot_x_penalty(:)
    real(dp), allocatable :: inhom_at_qps(:,:), n_dot_inhom_at_qps(:,:)
    real(dp) :: n_sgn

    call assert(qnty%name == 'mass_flux', 'continuity penal. is for m', BOUNDS_CHECK)

    boundary_face = face%is_boundary(only_fluid_mesh=.true.)
    if (boundary_face) then
      call get_bc(face%bc_id,'mass_flux',bc)

      ! Skip boundary faces that do not have an outgoing edge term:
      select type(bc)
        class is (dirichlet_bc_type)
        type is (slip_bc_type)
        type is (hom_neumann_bc_type);        return
        type is (inhom_neumann_bc_type);      return
        type is (generalized_neumann_bc_t);   return
        type is (adjusted_traction_bc_t);     return
        type is (robin_bc_type);              return
        class default;                        call error('continuity_penal: unknown bc.')
      end select
    endif
    
    if (Ein_is_E1) then
      side_Ein = 1; side_Eout = 2
    else
      side_Ein = 2; side_Eout = 1
    endif

    call func_Ein%set_as_func(face,side_Ein,qnty%handler)
    n_dot_func_Ein = lincom_for_n_dot_rank1(face,side_Ein,func_Ein)
    quot_x_penalty = face%quot * continuity_penalty_at_qps(face,dt)
    call local_linsys%add_int_dotprod_to_lhs(n_dot_func_Ein,n_dot_func_Ein,quot_x_penalty,1.0_dp,explicit=.false.)
    if (boundary_face) then
      select type(bc)
        class is (dirichlet_bc_type)
          allocate(inhom_at_qps(qnty%handler%no_dirs,face%nqp))
          call bc%inhom_part%values_at_qps(face,side_Ein,time=1,values=inhom_at_qps)
          n_sgn = merge(1.0_dp,-1.0_dp,side_Ein == 1)
          allocate(n_dot_inhom_at_qps(1,face%nqp))
          do qp = 1, face%nqp
            n_dot_inhom_at_qps(1,qp) = n_sgn * dot_product(face%normal(:,qp),inhom_at_qps(:,qp))
          enddo
          call local_linsys%add_int_dotprod_to_rhs(n_dot_func_Ein,n_dot_inhom_at_qps,quot_x_penalty,1.0_dp)
          deallocate(inhom_at_qps,n_dot_inhom_at_qps)
        type is (slip_bc_type)
          ! Do nothing, because the normal component of the velocity is set to 0 at the boundary!
      end select
    else
      call func_Eout%set_as_func(face,side_Eout,qnty%handler)
      n_dot_func_Eout = lincom_for_n_dot_rank1(face,side_Eout,func_Eout)
      call local_linsys%add_int_dotprod_to_lhs(n_dot_func_Ein,n_dot_func_Eout,quot_x_penalty,-1.0_dp,explicit=.false.)
    endif
  end subroutine

  function div_penalty_at_qps(elem,dt)
    type(elem_list), intent(in) :: elem
    real(dp), intent(in) :: dt
    real(dp) :: div_penalty_at_qps(elem%nqp)
    
    integer, parameter :: TIME = 0
    real(dp) :: avg_u(mesh_dimen), length_scale
    real(dp) :: u_at_qps(mesh_dimen,elem%nqp), norm_u_at_qps(elem%nqp)
    real(dp) :: factor
    
    length_scale = isotropic_charact_len(elem,'mass_flux','vol','linear')
    factor = div_m_pen_safety_factor * length_scale
    if (m_penalties_in == 'm-postproc' .and. m_p_strategy == 'pressure-correction') factor = factor * dt

    if (allocated(preset_norm_u)) then
      ! Work around a gfortran-10 bug that would results from using the simpler
      !     `preset_norm_u%values_at_qps(elem, TIME)`.
      call preset_norm_u%get_values(elem, elem%quad_points, TIME, norm_u_at_qps)
      div_penalty_at_qps = factor * norm_u_at_qps

    else
      call velocity%values_at_qps(elem,time,u_at_qps)
      if (div_m_pen_pointwise) then
        div_penalty_at_qps = factor * norm2(u_at_qps,dim=1)
      else
        avg_u = matmul(u_at_qps,elem%quot) / elem%volume
        div_penalty_at_qps = factor * norm2(avg_u)
      endif
    endif
  end function

  function continuity_penalty_at_qps(face,dt) result(penal)
    use penalty_param_mod, only: maxval_of_sides_at_qps

    type(face_list), intent(in) :: face
    real(dp), intent(in) :: dt
    real(dp) :: penal(face%nqp)
    
    integer, parameter :: TIME = 0
    type(elem_list), pointer :: elem
    real(dp) :: avg_velocity(mesh_dimen)
    real(dp) :: avg_u_at_sides(size(face%elem_neighbors))
    real(dp) :: face_avg(mesh_dimen,face%nqp)
    logical :: fluid_sides(size(face%elem_neighbors))
    integer :: side
    
    if (allocated(preset_norm_u)) then
      penal = maxval_of_sides_at_qps(preset_norm_u, face, ONLY_FLUID, TIME) ! bit wasteful: `preset_norm_u` is normally continuous
    
    else
      select case(cont_m_pen_mode)
        case('avg_on_neighbors')
          ! 2-norm of the average of the volumetric averages at the neighbors:
          do side = 1, size(face%elem_neighbors)
            elem => face%elem_neighbors(side)%point
            fluid_sides(side) = elem%is_fluid()
            if (fluid_sides(side)) then
              call velocity%eval_avg_value(elem,TIME,avg_velocity)
              avg_u_at_sides(side) = norm2(avg_velocity)
            endif
          enddo
          penal = sum(avg_u_at_sides,mask=fluid_sides) / count(fluid_sides)
        
        case('pointwise_on_face')
          ! 2-norm of the point-wise face average:
          call velocity%avg_at_qps(face,.true.,TIME,face_avg)
          penal = norm2(face_avg,dim=1)
        
        case default
          call error('unknown cont_m_pen_mode=' // cont_m_pen_mode)
      end select
    endif

    if (m_penalties_in == 'm-postproc' .and. m_p_strategy == 'pressure-correction') then
      penal = penal * dt
    endif
  end function
end module
