module petsc_mod
#include "petsc/finclude/petscsys.h"
  use petscsys, only: PETSC_NULL_CHARACTER

  use f90_kind, only: dp
  use assertions, only: assert, assert_eq
  implicit none
  
  private
  public :: MASTER_ID
  public :: I_am_master_process, no_petsc_procs, petsc_proc_id

  public :: init_petsc, stop_petsc
  public :: petsc_initialized
  public :: init_petsc_logstages, switch_logstage

  public :: idx_fort2petsc, idx_petsc2fort
  public :: petsc_mpi_barrier
  public :: petsc_mpi_comm
  public :: petsc_options_set_value, set_petsc_options_from_input_list

  public :: debug_print
  
  logical, save, protected :: petsc_initialized = .false.
  MPI_Comm, allocatable, save, protected :: petsc_mpi_comm

  integer, parameter :: MASTER_ID = 0

  PetscLogStage, allocatable :: logstages(:)
  character(:), allocatable :: logstage_names(:)

contains
  subroutine init_petsc()
    use petscsys, only: PetscInitialize, PETSC_COMM_WORLD
    
    PetscErrorCode :: ierr
    
    call assert(.not. petsc_initialized, 'init_petsc: module already initialized')
    
    call PetscInitialize(PETSC_NULL_CHARACTER,ierr)
    call assert(ierr == 0,'ierr /= 0 in PetscInitialize')
    
    allocate(petsc_mpi_comm,source=PETSC_COMM_WORLD)
    
    petsc_initialized = .true.
  end subroutine init_petsc
  
  subroutine stop_petsc()
    use petscsys, only: PetscFinalize

    PetscErrorCode :: ierr
    
    call assert(petsc_initialized, 'stop_petsc: uninitialized module')
    
    call logstage_pop()

    call PetscFinalize(ierr)
    call assert(ierr == 0,'ierr /= 0 in PetscFinalize')

    petsc_initialized = .false.
  end subroutine stop_petsc
  
  elemental subroutine idx_fort2petsc(fort_idx,petsc_idx)
    integer, intent(in) :: fort_idx
    integer, intent(out) :: petsc_idx
    
    petsc_idx = fort_idx - 1
  end subroutine idx_fort2petsc
  
  elemental subroutine idx_petsc2fort(petsc_idx,fort_idx)
    integer, intent(in) :: petsc_idx
    integer, intent(out) :: fort_idx
    
    fort_idx = petsc_idx + 1
  end subroutine idx_petsc2fort
  
  integer function no_petsc_procs()
    use petscsys, only: MPI_Comm_size

    PetscErrorCode :: ierr
    
    call MPI_Comm_size(petsc_mpi_comm,no_petsc_procs,ierr)
    call assert(ierr == 0,'ierr /= 0 in MPI_Comm_size')
  end function no_petsc_procs

  logical function I_am_master_process()
    I_am_master_process = petsc_proc_id() == MASTER_ID
  end function I_am_master_process

  integer function petsc_proc_id()
    use petscsys, only: MPI_Comm_rank
    
    PetscErrorCode :: ierr
    
    call MPI_Comm_rank(petsc_mpi_comm,petsc_proc_id,ierr)
    call assert(ierr == 0,'ierr /= 0 in MPI_Comm_rank')
  end function
  
  subroutine petsc_mpi_barrier()
    ! This routine calls MPI_Barrier with the communicator of the PETSc Object obj.
    !
    ! You may pass PETSC_NULL_VEC or any other PETSc null object, 
    ! to indicate the barrier should be across MPI_COMM_WORLD. 
    use petscsys, only: PetscBarrier, PETSC_NULL_OPTIONS
    
    PetscErrorCode :: ierr
    
    call PetscBarrier(PETSC_NULL_OPTIONS,ierr)
    call assert(ierr == 0,'ierr /= 0 in PetscBarrier')
  end subroutine petsc_mpi_barrier
  
  subroutine petsc_options_set_value(option,value)
    use petscsys, only: PetscOptionsSetValue, PETSC_NULL_OPTIONS
    
    character(*), intent(in) :: option
    character(*), optional, intent(in) :: value
    
    character(:), allocatable :: prev_val
    PetscErrorCode :: ierr
    
    call petsc_options_get_string(option, prev_val)
    if (allocated(prev_val)) then
      if (I_am_master_process()) then
        write(*,'(4(/,a),/)') '*** Do not set PETSc option at runtime, because it already had a value', &
                              '***     option=' // option, &
                              '*** prev_value=' // prev_val, &
                              '***      value=' // value
      endif
      return
    endif

    if (present(value)) then
      call PetscOptionsSetValue(PETSC_NULL_OPTIONS,option,value,ierr)
    else
      call PetscOptionsSetValue(PETSC_NULL_OPTIONS,option,PETSC_NULL_CHARACTER,ierr)
    endif
    call assert(ierr == 0,'ierr /= 0 in PetscOptionsSetValue')
  end subroutine

  subroutine petsc_options_get_string(name, string)
    use petscsys, only: PetscOptionsGetString, PETSC_NULL_OPTIONS
    
    use petsc_basic_types_mod, only: petsc2fort
    use, intrinsic :: iso_c_binding, only: c_char

    character(*), intent(in) :: name
    character(:), allocatable, intent(out) :: string

    PetscErrorCode :: ierr
    PetscBool :: set
    character(20, kind=c_char) :: petsc_string

    call PetscOptionsGetString(PETSC_NULL_OPTIONS, PETSC_NULL_CHARACTER, name, petsc_string, set, ierr)
    call assert(ierr == 0,'ierr /= 0 in PetscOptionsGetString')

    if (petsc2fort(set)) string = trim(petsc_string)
  end subroutine

  subroutine set_petsc_options_from_input_list(opts_list)
    ! opts_list = ['opt1', 'opt2 val2', 'opt3', ...]
    ! 
    use string_manipulations, only: separate_first_word
    
    character(*), intent(in) :: opts_list(:)

    integer :: i
    character(:), allocatable :: opt, val

    do i = 1, size(opts_list)
      call separate_first_word(opts_list(i), opt, val, ' ')
      if (val == '') then
        call petsc_options_set_value(opt)
      else
        call petsc_options_set_value(opt, val)
      endif
    enddo
  end subroutine
  
  subroutine init_petsc_logstages(names)
    ! Set the names of the log stages, and start the first log stage in the list.
    ! 
    use petscsys, only: PetscLogStageRegister

    character(*), intent(in) :: names(:)

    integer :: i, n
    PetscErrorCode :: ierr

    n = size(names)
    call assert(n > 0, "I expected at least one log stage.")
    
    logstage_names = names
    allocate(logstages(n))

    do i = 1, n
      call PetscLogStageRegister(trim(names(i)), logstages(i), ierr)
      call assert(ierr == 0, "ierr /= 0 in PetscLogStageRegister")
    enddo

    call logstage_push(1)
  end subroutine

  subroutine switch_logstage(new_stage)
    character(*), intent(in) :: new_stage

    integer :: idx
    
    idx = findloc(logstage_names, new_stage, dim=1)
    call assert(idx /= 0, "can not find this logstage: name=" // new_stage)

    call logstage_pop()
    call logstage_push(idx)
  end subroutine

  subroutine logstage_push(idx)
    ! Push a 'log stage' to the stack. Needs to be popped when this stage is completed.
    use petsc, only: PetscLogStagePush

    integer, intent(in) :: idx

    PetscErrorCode :: ierr
    
    call assert(lbound(logstages,1) <= idx .and. idx <= ubound(logstages,1), "logstage_push: bad idx")

    call PetscLogStagePush(logstages(idx), ierr)
    call assert(ierr == 0, "ierr /= 0 in PetscLogStagePush")
  end subroutine

  subroutine logstage_pop()
    use petsc, only: PetscLogStagePop

    PetscErrorCode :: ierr

    call PetscLogStagePop(ierr)
    call assert(ierr == 0, "ierr /= 0 in PetscLogStagePop")
  end subroutine

  subroutine debug_print(msg)
    ! Debug print, not to be used in production code. Must be called on all 
    ! processes. 
    ! Flushes the I/O buffer and calls an MPI barrier.
    use, intrinsic :: iso_fortran_env, only: OUTPUT_UNIT, ERROR_UNIT

    character(*), intent(in) :: msg

    call petsc_mpi_barrier()
    flush(ERROR_UNIT)
    flush(OUTPUT_UNIT)
    call petsc_mpi_barrier()
    write(OUTPUT_UNIT,'(i4,a)') petsc_proc_id(), ': ' // msg
    flush(ERROR_UNIT)
    flush(OUTPUT_UNIT)
  end subroutine
end module petsc_mod
