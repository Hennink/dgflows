module m_solver_mod
  use assertions, only: assert
  use exceptions, only: warning
  use f90_kind, only: dp
  use numerical_solutions, only: matprop_set
  use pde_mod, only: pde_t
  use petsc_mat_mod, only: petsc_mat_type
  use petsc_vec_mod, only: petsc_vec_type
  use qnty_solver_mod, only: qnty_solver_t
  use run_data, only: dt, use_rans_model, eval_grad_rho_k_term_in_momentum_eq, m_p_strategy, rans_model_description, &
      add_m_penalties, m_penalties_in, m_continuity_penal_preset_norm_u_func, m_continuity_penal_is_time_independent
  use support, only: false
  implicit none
  
  private
  public :: new_m_solver
  public :: include_m_convection
  public :: eff_kinematic_viscosity
  
  type, extends(qnty_solver_t) :: m_solver_t
  contains
    procedure, non_overridable :: collect_pde_varfrozconst
    procedure, non_overridable :: postprocess_linsys
  end type
  
  logical :: include_m_convection = .true. ! Set to .false. for Stokes solver. Can be toggled on/off.
                                           ! Global because `eff_kinematic_viscosity` needs access.

  type(petsc_mat_type) :: M_pen_mtx
  type(petsc_vec_type) :: M_pen_rhs
  logical :: M_pen_linsys_initialized = .false.
  
contains
  type(m_solver_t) function new_m_solver()
    new_m_solver%qnty = 'mass_flux'

    new_m_solver%postprocessing_linsys_changes_lhs = add_m_penalties > 0 .and. m_penalties_in == 'momentum' &
                                                    .and. .not. m_continuity_penal_is_time_independent
  end function

  subroutine postprocess_linsys(this,mesh,lhs,rhs)
    use div_cont_penalties_and_err, only: assemble_m_pen_linsys
    use mesh_type, only: fem_mesh
    use numerical_solutions, only: get_solvec
    use petsc_mat_mod, only: same_nonzero_pattern_opt
    use phys_time_mod, only: nold_times
    use pressure, only: gradient_oper
    use solvec_mod, only: solvec_t
    
    class(m_solver_t), intent(in) :: this
    type(fem_mesh), intent(in) :: mesh
    type(petsc_mat_type), intent(inout) :: lhs
    type(petsc_vec_type), intent(inout) :: rhs
    
    type(solvec_t), pointer :: P, M, rho_k
    type(petsc_vec_type) :: grad_pressure, P_petsc
    
    if (m_p_strategy == 'pressure-correction') then
      ! Pressure gradient term: rhs = rhs - div_operator^T * P
      ! When running in RANS mode, there's an extra term equal to -2/3 grad(rho*k),
      ! which can be either included in the pressure (i.e., P = P + 2/3*rho*k), or
      ! evaluated separately (only when the pressure must be computed exactly)
      P => get_solvec('pressure')
      call rhs%duplicate(grad_pressure)
      call gradient_oper%init_compatible_vecs(right_vec=P_petsc)
      call P%petsc_coeffs%copy(P_petsc)
      if (use_rans_model .and. &
          rans_model_description /= 'SA' .and. &
          rans_model_description /= 'log-k-log-eps' .and. &
          eval_grad_rho_k_term_in_momentum_eq) then
        rho_k => get_solvec('vol_turb1')
        call P_petsc%axpy(2.0_dp/3.0_dp,rho_k%petsc_coeffs)
      endif
      call gradient_oper%mult(P_petsc,grad_pressure)
      call rhs%subtract_vec(grad_pressure)
    endif
    
    ! Add divergence and continuity penalties, if necessary; see [1].
    !
    ! [1] Fehn N. et al., "Robust and efficient dG methods for under-resolved turbulent incompressible flows",
    !     Journal of Comp. Phys. 372 (2018), pp. 667-693
    if (add_m_penalties > 0 .and. m_penalties_in == 'momentum' .and. .not. m_continuity_penal_is_time_independent) then
      if (mod(nold_times,add_m_penalties)==0 .or. (.not. M_pen_linsys_initialized)) then
        M => get_solvec('mass_flux')
        call assemble_m_pen_linsys(mesh,dt,M,M_pen_linsys_initialized,M_pen_mtx,M_pen_rhs)
      endif
      call lhs%axpy(1.0_dp,M_pen_mtx,same_nonzero_pattern_opt)
      call rhs%add_vec(M_pen_rhs)
    endif
  end subroutine
  
  subroutine collect_pde_varfrozconst(this, variable_terms, frozen_terms, constant_terms)
    use boundaries, only: bc_are_time_independent
    use continuity_penal_mod, only: continuity_penal_t
    use convection, only: conv_t
    use gsip_viscous_stress_mod, only: gsip_viscous_stress_t
    use numerical_solutions, only: temporal_order_for_qnty, implicit_convection_for_qnty, find_if_add_conv_corr_for_qnty
    use phys_time_mod, only: BDF_weights
    use time_der_mod, only: impl_time_deriv, expl_time_deriv
    use run_data, only: constant_density, constant_kinematic_viscosity, dt
    use sources_mod, only: add_tot_vol_force_to_pde, force_is_constant
    
    class(m_solver_t), intent(in) :: this
    type(pde_t), intent(out) :: variable_terms, frozen_terms, constant_terms
    
    integer :: order
    real(dp), allocatable :: weights(:)
    type(gsip_viscous_stress_t) :: sip
    type(conv_t) :: conv
    type(continuity_penal_t) :: continuity_penal
    
    order = temporal_order_for_qnty(this%qnty)
    weights = BDF_weights(order,dt)
    call constant_terms%add_term(impl_time_deriv(weights(1)))
    call frozen_terms%add_term(expl_time_deriv(weights))
    
    call eff_kinematic_viscosity(sip%diff_param)
    sip%explicit = .false.
    sip%add_mass_flux_term = .not. constant_density
    if (constant_density .and. constant_kinematic_viscosity .and. bc_are_time_independent) then
      call constant_terms%add_term(sip)
    else
      call frozen_terms%add_term(sip)
    endif
    
    if (force_is_constant) then
      call add_tot_vol_force_to_pde(constant_terms)
    else
      call add_tot_vol_force_to_pde(frozen_terms)
    endif

    if (include_m_convection) then
      call find_if_add_conv_corr_for_qnty('mass_flux', conv%add_vol_div_corr, conv%add_face_jump_corr)
      conv%explicit = .not. implicit_convection_for_qnty('mass_flux')
      call variable_terms%add_term(conv)
    endif

    if (add_m_penalties > 0 .and. m_penalties_in == 'momentum' .and. m_continuity_penal_is_time_independent) then
      call assert(m_continuity_penal_preset_norm_u_func /= '', 'continuity penal is not constant unless you set `|u|')

      call constant_terms%add_term(continuity_penal)
    endif
  end subroutine
  
  subroutine eff_kinematic_viscosity(nu_effective)
    use les_eddy_viscosity_mod, only: nu_sfs => eddy_kin_viscosity
    use func_operations_mod
    use petsc_mod, only: I_am_master_process
    use rans_mod, only: rans_model
    use run_data, only: les_specs_file, use_rans_model, clip_viscosity
    use scal_func_mod, only: scal_func_type
    
    class(scal_func_type), allocatable, intent(out) :: nu_effective
    
    class(scal_func_type), allocatable :: nu_molec, nu_clipped

    call assert(.not. (les_specs_file /= '' .and. use_rans_model),"Don't mix LES and RANS.")
    
    call matprop_set%get_func('kinematic_viscosity',nu_molec)
    if (.not. include_m_convection) then
      if ((les_specs_file /= '' .or. use_rans_model) .and. I_am_master_process()) call warning('Skipping LES/RANS for Stokes flow.')
      call move_alloc(nu_molec, nu_effective)
    elseif (les_specs_file /= '') then
      call assert(allocated(nu_sfs), "attempting to use `eddy_kin_viscosity`, but it's not yet allocated")
      nu_effective = nu_molec + nu_sfs
    elseif (use_rans_model) then
      nu_effective = nu_molec + rans_model%kinem_visc
    else
      call move_alloc(nu_molec, nu_effective)
    endif

    if (clip_viscosity) then
      nu_clipped = max_of(nu_effective,0.0_dp)
      call move_alloc(nu_clipped,nu_effective)
    endif
  end subroutine
end module m_solver_mod

