module vec_block_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind,  only: dp
  implicit none
  
  private
  public :: vec_block_t
  
  type :: vec_block_t
    private
    integer :: gr
    integer, public :: elem
    real(dp), pointer, contiguous, public :: vec(:,:) => null()
    real(dp), allocatable :: unidir_block(:)
    
  contains
    procedure, non_overridable :: init
    procedure, non_overridable :: add_inprod_all_dirs, &
                                  add_inprod_to_dir, &
                                  add_int_dotprod
    procedure, non_overridable :: add_to_glob_vec
    procedure, non_overridable, private :: process_unidir_block
    final :: finalize_vec_block
  end type vec_block_t
  
contains
  subroutine init(this,elem,handler)
    use dof_handler, only: dof_handler_type
    
    class(vec_block_t), intent(out) :: this
    integer, intent(in) :: elem
    type(dof_handler_type), intent(in) :: handler
    
    integer :: no_dirs, no_nod
    
    if (handler%no_grps /= 1) error stop "don't know to which groups I belong"
    this%gr = 1
    this%elem = elem
    
    no_dirs = handler%no_dirs
    no_nod = handler%no_nod_elem(elem)
    allocate(this%vec(no_nod,no_dirs))
    this%vec = 0.0_dp
    allocate(this%unidir_block(no_nod))
    this%unidir_block = 0.0_dp
  end subroutine init
  
  subroutine add_inprod_all_dirs(this,scal1,scal2,weights)
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t

    class(vec_block_t), intent(inout) :: this
    type(unidir_lincom_rank0_t), intent(in) :: scal1
    real(dp), intent(in) :: scal2(:), weights(:)

    integer :: p, np

    call assert(this%elem == scal1%elem,BOUNDS_CHECK)
    call assert(size(this%unidir_block) == scal1%nnod(),BOUNDS_CHECK)
    np = assert_eq(size(scal2),size(weights),scal1%np(),BOUNDS_CHECK)
    
    do p = 1, np
      this%unidir_block = this%unidir_block + scal1%stencil(:,p) * scal2(p) * weights(p)
    enddo
  end subroutine add_inprod_all_dirs

  subroutine add_inprod_to_dir(this,scal1,scal2,dir,quad_weights)
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t

    class(vec_block_t), intent(inout) :: this
    type(unidir_lincom_rank0_t), intent(in) :: scal1
    real(dp), intent(in) :: scal2(:)
    integer, intent(in) :: dir
    real(dp), intent(in) :: quad_weights(:)

    integer :: nnod, p, np

    call assert(this%elem == scal1%elem,BOUNDS_CHECK)
    nnod = assert_eq(size(this%vec,1),scal1%nnod(),BOUNDS_CHECK)
    np = assert_eq(size(scal2),size(quad_weights),scal1%np(),BOUNDS_CHECK)

    do p = 1, np
      this%vec(:,dir) = this%vec(:,dir) + scal1%stencil(:,p) * scal2(p) * quad_weights(p)
    enddo
  end subroutine add_inprod_to_dir

  subroutine add_int_dotprod(this,lincom,vecfunc_values,quad_weights,alpha)
    use array_opers_mod, only: gemv_classic
    use lincom_rank1_mod, only: lincom_rank1_t
    use vec_func_mod, only: vec_func_type
    
    class(vec_block_t), intent(inout) :: this
    type(lincom_rank1_t), intent(in) :: lincom
    real(dp), intent(in) :: vecfunc_values(:,:)
    real(dp), intent(in) :: quad_weights(:)
    real(dp), intent(in) :: alpha
    
    logical, parameter :: use_blas = .true.
    integer :: p, np, dir, no_dirs
    
    no_dirs = assert_eq(size(vecfunc_values,1),lincom%no_dirs(),BOUNDS_CHECK)
    np = assert_eq(size(quad_weights),size(vecfunc_values,2),lincom%np(),BOUNDS_CHECK)

    if (use_blas) then
      block
        real(dp) :: w_x_vecvals(size(vecfunc_values,1), size(vecfunc_values,2))
        do p = 1, np
          w_x_vecvals(:,p) = vecfunc_values(:,p) * quad_weights(p) * alpha
        enddo
        call gemv_classic(TRANS=.false., M=size(this%vec), N=np*no_dirs, &
            ALPHA=1.0_dp, A=lincom%stencil, LDA=size(this%vec), X=w_x_vecvals, INCX=1, &
            BETA=1.0_dp,  Y=this%vec, INCY=1)
      end block

    else
      do p = 1, np
        do dir = 1, no_dirs
          this%vec = this%vec + lincom%stencil(:,:,dir,p) * vecfunc_values(dir,p) * quad_weights(p) * alpha
        enddo
      enddo
    endif
  end subroutine
  
  subroutine add_to_glob_vec(this,petsc_vec,handler)
    use dof_handler, only: dof_handler_type, dof
    use petsc_vec_mod, only: petsc_vec_type
    
    class(vec_block_t), intent(inout) :: this
    type(petsc_vec_type), intent(inout) :: petsc_vec
    type(dof_handler_type), intent(in) :: handler

    real(dp), contiguous, pointer :: flat_vec(:)
    integer, allocatable :: rows(:)

    call this%process_unidir_block()
    
    flat_vec(1:size(this%vec)) => this%vec
    if (petsc_vec%assemble_elemvec_as_block) then
      call petsc_vec%add_values_blocked(this%elem, flat_vec)
    else
      rows = handler%flat_dof(this%gr, this%elem)
      call petsc_vec%add(rows, flat_vec)
    endif
  end subroutine
  
  subroutine process_unidir_block(this)
    class(vec_block_t), intent(inout) :: this

    integer :: dir

    do dir = 1, size(this%vec,2)
      this%vec(:,dir) = this%vec(:,dir) + this%unidir_block
    enddo
    this%unidir_block = 0.0_dp
  end subroutine process_unidir_block

  elemental subroutine finalize_vec_block(this)
    type(vec_block_t), intent(inout) :: this

    if (associated(this%vec)) deallocate(this%vec)
  end subroutine
end module vec_block_mod
