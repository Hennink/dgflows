module qnty_handler_mod
  ! This module stores the solvecs, but does not manipulate or evaluate them.
  use code_const, only: UNINITIALIZED_INT
  use f90_kind, only: dp
  use mesh_type, only: fem_mesh, ngbrs_removed_from_bnd
  use solvec_mod, only: solvec_t, deepcopy
  use string_mod, only: string_t
  use timer_typedef, only: timer_type
  implicit none
  
  private
  public :: quantities, findloc_qnty
  public :: max_sol_order
  public :: change_solspace

  type :: qnty_handler_t
    ! Functions will need a stable pointer to the solvec, so it cannot be allocatable.
    type(solvec_t) :: solvec
    
    integer :: temporal_order = -1
    logical :: implicit_convection
    integer :: conv_num_flux = UNINITIALIZED_INT
    logical :: add_conv_vol_corr, add_conv_face_corr
    logical :: tvd_limit

    logical :: symm

  contains
    private
    procedure, public :: init
  end type qnty_handler_t
  
  type(qnty_handler_t), allocatable, dimension(:), target :: quantities
  
contains
  subroutine change_solspace(qnty_handler,mesh,new_orders_near_bnd)
    use galerkin_projection, only: project_onto_different_orders

    type(qnty_handler_t), intent(inout) :: qnty_handler
    type(fem_mesh), intent(in) :: mesh
    integer, intent(in) :: new_orders_near_bnd(:)

    type(solvec_t) :: new_solvec
    integer, allocatable :: new_orders(:)
    logical :: only_fluid

    only_fluid = qnty_handler%solvec%handler%only_fluid_mesh
    new_orders = polorders(mesh,only_fluid,new_orders_near_bnd)
    call project_onto_different_orders(mesh,qnty_handler%solvec,new_orders,new_solvec)
    call deepcopy(new_solvec,qnty_handler%solvec)
  end subroutine
  
  integer function max_sol_order(elem)
    use mesh_objects, only: elem_list
    
    type(elem_list), intent(in) :: elem
    integer :: elem_no, i
    
    max_sol_order = 0
    do i = 1, size(quantities)
      associate(handler => quantities(i)%solvec%handler)
        if (handler%only_fluid_mesh .and. .not. elem%is_fluid()) cycle
        elem_no = elem%get_active_elem_no(handler%only_fluid_mesh)
        max_sol_order = max(max_sol_order,handler%order(elem_no))
      end associate
    enddo
  end function
  
  integer pure function findloc_qnty(qnty_name) result(idx)
    character(*), intent(in) :: qnty_name

    do idx = 1, size(quantities)
      if (quantities(idx)%solvec%name == qnty_name) return
    enddo
    idx = 0
  end function findloc_qnty

  subroutine init(this,name,unit_no,mesh)
    ! Possible PETSc options for HYPRE:
    !     pc_hypre_type boomeramg
    !     pc_hypre_boomeramg_max_iter 1
    !     pc_hypre_boomeramg_P_max 5
    !     pc_hypre_boomeramg_agg_nl 2
    !     pc_hypre_boomeramg_strong_threshold 0.5 (or 0.25 in 2D)
    !     pc_hypre_boomeramg_coarsen_type HMIS
    ! 
    use, intrinsic :: iso_fortran_env, only: ERROR_UNIT
    use assertions, only: assert, assert_eq
    use boundaries, only: absolute_pressure_is_fixed
    use code_const, only: LAX_FRIEDRICHS, UPWIND_CONSERVATIVE, UNINITIALIZED_CHAR, UNINITIALIZED_INT
    use exceptions, only: warning
    use petsc_mod, only: I_am_master_process, no_petsc_procs, set_petsc_options_from_input_list
    use quantities_mod, only: no_dirs, fluid_name, name2sym
    
    class(qnty_handler_t), intent(out) :: this
    type(string_t), intent(in) :: name
    integer, intent(in) :: unit_no
    type(fem_mesh), intent(in) :: mesh
    
    character(100), allocatable :: petsc_options(:) ! without preceding hyphen or quantity prefix
    integer :: temporal_order
    integer :: init_order(100)
    logical :: implicit_convection, symm, &
               add_conv_vol_corr, add_conv_face_corr, tvd_limit
    character(200) :: conv_num_flux
    namelist /unknown_specs/ petsc_options, &
                             temporal_order, init_order, &
                             implicit_convection, &
                             symm, &
                             conv_num_flux, &
                             add_conv_vol_corr, add_conv_face_corr, &
                             tvd_limit
    integer :: ndirs
    integer :: ios
    character(200) :: iomsg
    logical :: only_fluid
    character(:), allocatable :: prefix, sub_pc_type
    
    if (I_am_master_process()) write(*,"('initializing ',a,' ...')") name%get()
    
    ! Set default values:
    petsc_options = spread(UNINITIALIZED_CHAR, dim=1, ncopies=100)
    temporal_order = 1
    init_order = UNINITIALIZED_INT
    implicit_convection = .true.
    symm = name%get() == 'pressure'
    conv_num_flux = UNINITIALIZED_CHAR
    add_conv_vol_corr = .false.
    add_conv_face_corr = .false.
    tvd_limit = .false.

    ! Overwrite with input values:
    read(unit_no, unknown_specs, pad='no', iostat=ios, iomsg=iomsg)
    if (ios /= 0) then
      write(ERROR_UNIT,'(a)') 'INPUT ERROR in specs for the unknowns ... '
      write(ERROR_UNIT,'(a)') iomsg
      error stop
    endif
    
    call assert(init_order(1) /= UNINITIALIZED_INT, "Please set `init_order` for " // name%get())
    if (name%get() == 'pressure') call assert(symm, "Why is the pressure matrix not symmetric?")
    if (name%get() /= 'pressure' .and. implicit_convection) call assert(.not. symm, "implicit convection will give an asymm matrix")
    
    ndirs = no_dirs(name%get())
    
    this%temporal_order = temporal_order
    this%implicit_convection = implicit_convection
    this%symm = symm
    select case(trim(conv_num_flux))
      case('lax_friedrichs');       this%conv_num_flux = lax_friedrichs
      case('upwind_conservative');  this%conv_num_flux = upwind_conservative
      case(UNINITIALIZED_CHAR);     this%conv_num_flux = UNINITIALIZED_INT
      case default;                 error stop 'INPUT ERROR in specs for the unknowns: unknown num flux for convection.'
    end select
    this%add_conv_vol_corr = add_conv_vol_corr
    this%add_conv_face_corr = add_conv_vol_corr
    this%tvd_limit = tvd_limit

    ! Sanity checks:
    if (this%add_conv_vol_corr .or. this%add_conv_face_corr) then
      ! These terms are derived from an upwind flux
      call assert(this%conv_num_flux == upwind_conservative,'Corrective terms in convection must be combined with the upwind flux.')
    endif
    if (name%get() == 'pressure') call assert(this%temporal_order <= 1, &
        'temporal_order > 1 is only conditionally stable for the pressure.' // new_line('a') // ' &
        &[1] Guermond J.L. et al., "An overview of projection methods for incompressible flows", &
        &Comput. Methods Appl. Mech. Engrg. 195 (pp. 6011-6045)')
    
    ! intialize qnty solvec
    only_fluid = fluid_name(name%get())
    call this%solvec%init_to_zero(                                          &
        name%get(),mesh,                                                    &
        ncoeffs=no_time_steps(name,temporal_order),                         &
        no_dirs=ndirs,                                        &
        orders=polorders(mesh,only_fluid,pack(init_order,init_order >= 0)), &
        only_fluid_mesh=only_fluid                                          &
    )
    
    if (all(petsc_options == UNINITIALIZED_CHAR)) then
      deallocate(petsc_options)
      ! The expensive ILU/ICC preconditioners are probably only worthwhile when
      ! we solve for the same matrix more than once, but that is hard to check
      ! here, so we make a guess:
      if (implicit_convection .and. name%get() /= 'pressure') then
        sub_pc_type = 'sor'
      else
        sub_pc_type = merge('icc', 'ilu', symm)
      endif
      petsc_options = [ character(len(petsc_options)) :: &
          'ksp_type ' // merge('cg   ', 'gmres', symm), & ! pipelined versions are better when there are many nodes
          'pc_type bjacobi', & ! `asm` is usually not better for small problems, even for the pressure
          'sub_ksp_type preonly', &
          'sub_pc_type ' // sub_pc_type &
      ]
    endif
    prefix = '-' // name2sym(name%get()) // '_'
    call set_petsc_options_from_input_list(prefix // pack(petsc_options, petsc_options /= UNINITIALIZED_CHAR))
  end subroutine init
  
  integer function no_time_steps(qnty_name,temporal_order)
    type(string_t), intent(in) :: qnty_name
    integer, intent(in) :: temporal_order
    
    select case(qnty_name%get())
      case('mass_flux','pressure','vol_enthalpy','spec_enthalpy','temperature','vol_turb1','vol_turb2')
        no_time_steps = temporal_order + 1
      case('density_predictor')
        no_time_steps = 1
      case('wall_dist')
        no_time_steps = 2
      case default
        error stop 'unknown qnty_name'
    end select
  end function no_time_steps
  
  function polorders(mesh,only_fluid,orders_near_bnd)
    ! Returns an array of the polynomial polorders in each element, as 
    ! indicated by `orders_near_bnd`.
    ! The last entry in `orders_near_bnd` is used for elements that are far from the wall.
    ! 
    ! Operates on the full mesh for now, not just my elements.
    class(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid
    integer, intent(in) :: orders_near_bnd(:)
    integer, allocatable :: polorders(:)

    integer, allocatable :: ngbr_dists(:), indices(:)
    
    call ngbrs_removed_from_bnd(mesh,only_fluid,ngbr_dists)
    indices = min(1 + ngbr_dists, size(orders_near_bnd))
    polorders = orders_near_bnd(indices)
  end function polorders
end module qnty_handler_mod
