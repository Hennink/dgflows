module face_integrals_mod
  use assertions, only: assert, assert_eq
  use exceptions, only: error
  use f90_kind, only: dp
  use io_basics, only: my_temp_proc_name, nints_fmt, nreals_fmt
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only: vec_func_type
  use mesh_objects, only: elem_id, elem_list, elem_ptr, face_list, face_ptr, side_of_internal_nghb_of_bnd_face
  use mesh_type, only: fem_mesh
  use mpi_wrappers_mod, only: reduce_wrap
  use numerical_solutions, only: matprop_set, num_scalfunc_set, mass_flux, velocity
  use petsc_mod, only: i_am_master_process, petsc_proc_id, MASTER_ID
  use run_data, only: face_integrals, mesh_dimen
  implicit none
  
  private
  public :: list_faces, list_bc_id_faces
  public :: my_integral, unity_at_qps, n_dot_u_at_qps
  public :: write_bnd_areas
  public :: continuity_error
  public :: init_face_integrals, write_my_face_integrals, merge_integrals
  
  integer, parameter :: TIME = 2 ! time at which all functions are evaluated
  
  interface
    function func_at_qps_i(face) result(vals)
      use f90_kind, only: dp
      use mesh_objects, only: face_list
      implicit none
      type(face_list), intent(in) :: face
      real(dp) :: vals(face%nqp)
    end function func_at_qps_i
  end interface
  
  type faces_integrand_pair_t
    type(face_ptr), allocatable :: my_faces(:)
    procedure(func_at_qps_i), nopass, pointer :: integrand
  contains
    procedure :: init => init_faces_integrand_pair
  end type faces_integrand_pair_t
  type(faces_integrand_pair_t), allocatable :: integrals(:)
  
  integer :: my_integrals_unit, master_times_unit
  character(*), parameter :: FACE_INTEGRALS_FILE = 'face_integrals', &
                             TIMES_FILE = 'face_integrals_times'
  integer :: no_integrals_written = 0
  
contains
  real(dp) function continuity_error(mesh)
    ! Defined as (see [1], p.685)
    ! E_c = (\int_Fi |[u] \dot n|)/ (\int_Fi |{u} \dot n|)
    !
    ! [1] Fehn N. et al., "Robust and efficient dG methods for under-resolved turbulent incompressible flows",
    !     Journal of Comp. Phys. 372 (2018), pp. 667-693
    use, intrinsic :: ieee_arithmetic, only: ieee_value, IEEE_SIGNALING_NAN
    
    type(fem_mesh), intent(in) :: mesh
    
    type(face_ptr), allocatable :: my_faces(:)
    real(dp) :: my_num_denom(2), num_denom(2)
    real(dp), parameter :: SMALL = epsilon(1.0_dp)
    
    call list_internal_faces(mesh,.true.,my_faces)
    my_num_denom(1) = my_integral(my_faces,abs_jump_u_dot_n_at_qps)
    my_num_denom(2) = my_integral(my_faces,abs_average_u_dot_n_at_qps)
    
    call reduce_wrap(my_num_denom,'sum',MASTER_ID,num_denom)
    if (.not. i_am_master_process()) then
      continuity_error = ieee_value(1.0_dp,IEEE_SIGNALING_NAN)
    elseif (all(num_denom > SMALL)) then
      continuity_error = num_denom(1) / num_denom(2)
    else
      continuity_error = 0.0_dp
    endif
  end function
  
  
  subroutine write_bnd_areas(mesh)
    use boundaries, only: all_specified_bnd_ids
    
    type(fem_mesh), intent(in) :: mesh
    
    integer, allocatable :: bc_ids(:)
    integer :: unit
    integer :: iostat
    character(200) :: iomsg
    integer :: i, nid
    type(face_ptr), allocatable :: my_faces(:)
    real(dp), allocatable :: areas(:), my_areas(:)
    
    bc_ids = all_specified_bnd_ids()
    nid = size(bc_ids)
    
    allocate(areas(nid), my_areas(nid))
    do i = 1, nid
      call list_bc_id_faces(mesh,.false.,bc_ids(i),my_faces)
      my_areas(i) = my_integral(my_faces,unity_at_qps)
    enddo
    call reduce_wrap(my_areas,'sum',0,areas)
    
    if (petsc_proc_id() == 0) then
      open(file='bnd_areas', newunit=unit, &
           status='replace', pad='no', position='rewind', action='write', form='formatted', &
           iostat=iostat, iomsg=iomsg)
      call assert(iostat==0,iomsg)
        write(unit,nints_fmt(nid)) bc_ids
        write(unit,nreals_fmt(nid)) areas
      close(unit=unit,iostat=iostat,iomsg=iomsg)
      call assert(iostat==0,iomsg)
    endif
  end subroutine write_bnd_areas
  
  
  subroutine merge_integrals()
    ! Reads my integrals files, sums them,
    ! lets the master write them to a file with the corresponding times,
    ! and deletes the temporary files.
    integer :: iostat
    character(200) :: iomsg
    integer :: i
    real(dp), allocatable :: my_ints(:,:), ints(:,:), times(:)
    integer :: unit
    
    if (size(integrals) == 0) return
    
    ! Read my own integrals:
    allocate(my_ints(size(integrals),no_integrals_written))
    rewind(my_integrals_unit,iostat=iostat,iomsg=iomsg)
    call assert(iostat==0,iomsg)
      do i = 1, no_integrals_written
        read(my_integrals_unit,nreals_fmt(size(integrals))) my_ints(:,i)
      enddo
    close(my_integrals_unit,status='delete',iostat=iostat,iomsg=iomsg)
    call assert(iostat==0,iomsg)
    
    ! Pass my integrals to the master:
    allocate(ints,mold=my_ints)
    call reduce_wrap(my_ints,'sum',MASTER_ID,ints)

    ! If I am the master process, write the times and the integrals to a single file:
    if (I_am_master_process()) then
      ! Read the times:
      allocate(times(no_integrals_written))
      rewind(master_times_unit,iostat=iostat,iomsg=iomsg)
      call assert(iostat==0,iomsg)
        do i = 1, no_integrals_written
          read(master_times_unit,nreals_fmt(1)) times(i)
        enddo
      close(master_times_unit,status='delete',iostat=iostat,iomsg=iomsg)
      call assert(iostat==0,iomsg)
      
      ! Write times and integrals:
      open(file=FACE_INTEGRALS_FILE, newunit=unit, &
           status='replace', pad='no', position='rewind', action='write', form='formatted', &
           iostat=iostat, iomsg=iomsg)
      call assert(iostat==0,iomsg)
        write(unit,'(a)', advance='no') 'time'
        do i = 1, size(face_integrals)
          write(unit,'(2a)',advance='no') ',', trim(face_integrals(i))
        enddo
        write(unit,'(a)') ''
        do i = 1, no_integrals_written
          write(unit,nreals_fmt(1+size(integrals))) times(i), ints(:,i)
        enddo
      close(unit,iostat=iostat,iomsg=iomsg)
      call assert(iostat==0,iomsg)
    endif
  end subroutine
  
  subroutine write_my_face_integrals()
    ! Writes my integrals to my output file,
    ! and writes the time if I am the master process.
    use phys_time_mod, only: old_times
    
    integer :: i
    real(dp) :: my_ints(size(integrals))
    integer :: iostat
    character(200) :: iomsg
    
    if (size(integrals) == 0) return
    
    do i = 1, size(integrals)
      my_ints(i) = my_integral(integrals(i)%my_faces,integrals(i)%integrand)
    enddo
    write(my_integrals_unit,nreals_fmt(size(integrals))) my_ints
    no_integrals_written = no_integrals_written + 1
    if (i_am_master_process()) then
      write(master_times_unit,nreals_fmt(1),iostat=iostat,iomsg=iomsg) old_times(1)
      call assert(iostat==0,iomsg)
    endif
  end subroutine
  
  subroutine init_face_integrals(mesh)
    type(fem_mesh), intent(in) :: mesh
    
    integer :: i, ni
    integer :: iostat
    character(200) :: iomsg
    
    ni = size(face_integrals)
    allocate(integrals(ni))
    do i = 1, ni
      call integrals(i)%init(mesh,face_integrals(i))
    enddo
    if (ni > 0) then
      open(newunit=my_integrals_unit, file=my_temp_proc_name(FACE_INTEGRALS_FILE), &
           status='replace', pad='no', position='rewind', action='readwrite', form='formatted', &
           iostat=iostat, iomsg=iomsg)
      call assert(iostat==0,iomsg)
      if (i_am_master_process()) then
        open(newunit=master_times_unit, file=TIMES_FILE, &
           status='replace', pad='no', position='rewind', action='readwrite', form='formatted', &
           iostat=iostat, iomsg=iomsg)
        call assert(iostat==0,iomsg)
      endif
    endif
  end subroutine init_face_integrals
  
  subroutine init_faces_integrand_pair(this,mesh,description)
    use string_manipulations, only: char2int, separate_first_word
    
    class(faces_integrand_pair_t), intent(out) :: this
    type(fem_mesh), intent(in) :: mesh
    character(*), intent(in) :: description
    
    character(:), allocatable :: face_selector, integrand_str, ftype, id_str
    logical :: only_fluid
    
    call separate_first_word(description,face_selector,integrand_str,':')
    call separate_first_word(face_selector,ftype,id_str,'=')
    
    select case(integrand_str)
      case('fourier_heat_flux')
        this%integrand => fourier_heat_flux_at_qps
        only_fluid = .false.
      case('mass_flow')
        this%integrand => n_dot_m_at_qps
        only_fluid = .true.
      case('wall_stress_x')
        this%integrand => wall_stress_x_at_qps
        only_fluid = .true.
      case('wall_stress_z')
        this%integrand => wall_stress_z_at_qps
        only_fluid = .true.
      case default
        call error('unknown functional: integrand_str=' // integrand_str)
    end select

    select case(ftype)
      case('bc_id');     call list_bc_id_faces(mesh,only_fluid,char2int(id_str),this%my_faces)
      case default;      call error('cannot interpret face selector: description=' // description)
    end select
  end subroutine init_faces_integrand_pair
  
  real(dp) function my_integral(my_faces,integrand)
    type(face_ptr), intent(in) :: my_faces(:)
    procedure(func_at_qps_i) :: integrand
    
    integer :: f
    type(face_list), pointer :: face
    
    my_integral = 0.0_dp
    do f = 1, size(my_faces)
      face => my_faces(f)%point
      my_integral = my_integral + dot_product(face%quot,integrand(face))
    enddo
  end function my_integral
  
  function fourier_heat_flux_at_qps(face) result(fhf)
    ! Given a boundary face, this returns
    !     k n .dot. gder(T)  =  (k/c_p) n .dot. gder(h)
    ! at the quadrature points, where 'n' the outward normal.
    type(face_list), intent(in) :: face
    real(dp) :: fhf(face%nqp)
    
    integer :: side
    class(scal_func_type), allocatable :: h_func, k_over_cp_func
    real(dp) :: gder_h(mesh_dimen,face%nqp), k_over_cp(face%nqp)
    integer :: p
    
    call assert(face%is_boundary(.true.),'routine implemented only for boundary faces')
    
    side = side_of_internal_nghb_of_bnd_face(face,only_fluid_mesh=.true.)
    call matprop_set%get_func('k_over_cp',k_over_cp_func)
    call num_scalfunc_set%get_func('spec_enthalpy',h_func)
    k_over_cp = k_over_cp_func%values_at_qps(face,side,TIME)
    gder_h = h_func%gders_at_face_qps(face,side,TIME)
    do p = 1, face%nqp
      fhf(p) = k_over_cp(p) * dot_product(face%normal(:,p),gder_h(:,p))
    enddo
    if (side == 2) fhf = - fhf
  end function fourier_heat_flux_at_qps
  
  function n_dot_u_at_qps(face) result(n_dot_u)
    ! Given a boundary or periodic fluid face, this returns
    !     n .dot. u
    ! at the quadrature points. If 'face's is periodic, then 'u' is evaluated
    ! at the secondary side, and 'n' is the normal in the direction of the
    ! 'periodicity_shift'.
    use mesh_type, only: periodicity_shift
    
    type(face_list), intent(in) :: face
    real(dp) :: n_dot_u(face%nqp)

    real(dp) :: E1_to_E2(3)
    integer :: side
    class(scal_func_type), pointer :: rho
    
    call assert(face%is_boundary(.true.) .or. face%is_periodic,'routine implemented only for boundary or periodic faces')
    
    if (face%is_periodic) then
      E1_to_E2 = face%elem_neighbors(2)%point%com() - face%elem_neighbors(1)%point%com()
      side = merge(2,1,dot_product(E1_to_E2,periodicity_shift(:,face%periodic_id)) > 0)
    else
      side = side_of_internal_nghb_of_bnd_face(face,only_fluid_mesh=.true.)
    endif
    rho => matprop_set%func_ptr('density')
    n_dot_u = n_dot_m_at_qps(face) / rho%values_at_qps(face,side,TIME)
  end function n_dot_u_at_qps

  function n_dot_m_at_qps(face) result(n_dot_m)
    ! Given a boundary or periodic fluid face, this returns
    !     n .dot. m
    ! at the quadrature points. If 'face's is periodic, then 'm' is evaluated
    ! at the secondary side, and 'n' is the normal in the direction of the
    ! 'periodicity_shift'.
    use mesh_type, only: periodicity_shift
    
    type(face_list), intent(in) :: face
    real(dp) :: n_dot_m(face%nqp)
    
    real(dp) :: E1_to_E2(3)
    integer :: side
    logical :: reverse_normal
    real(dp) :: m(mesh_dimen,face%nqp)
    integer :: p
    
    call assert(face%is_boundary(.true.) .or. face%is_periodic,'routine implemented only for boundary or periodic faces')
    
    if (face%is_periodic) then
      E1_to_E2 = face%elem_neighbors(2)%point%com() - face%elem_neighbors(1)%point%com()
      side = merge(2,1,dot_product(E1_to_E2,periodicity_shift(:,face%periodic_id)) > 0)
      reverse_normal = dot_product(E1_to_E2,face%avg_unit_normal()) < 0
    else
      side = side_of_internal_nghb_of_bnd_face(face,only_fluid_mesh=.true.)
      reverse_normal = side == 2
    endif
    call mass_flux%values_at_qps(face,side,TIME,m)
    do p = 1, face%nqp
      n_dot_m(p) = dot_product(face%normal(:,p),m(:,p))
    enddo
    if (reverse_normal) n_dot_m = - n_dot_m
  end function n_dot_m_at_qps
  
  function wall_stress_x_at_qps(face) result(ws_z)
    ! Returns the z-component of the wall stress.
    type(face_list), intent(in) :: face
    real(dp) :: ws_z(face%nqp)
    
    real(dp), allocatable:: n_dot_stress(:,:)

    n_dot_stress = wall_stress_at_qps(face)
    ws_z = n_dot_stress(1,:)
  end function
  
  function wall_stress_z_at_qps(face) result(ws_z)
    ! Returns the z-component of the wall stress.
    type(face_list), intent(in) :: face
    real(dp) :: ws_z(face%nqp)
    
    real(dp), allocatable:: n_dot_stress(:,:)

    n_dot_stress = wall_stress_at_qps(face)
    ws_z = n_dot_stress(3,:)
  end function
  
  function wall_stress_at_qps(face) result(n_dot_stress)
    ! Given a boundary face of the fluid,
    !     n .dot. tau,
    ! at the quadrature points, where tau is the full stress tensor.
    ! 
    ! It's probably not really necessary to take the full stress tensor.
    ! *  The transposed term vanishes for straight boundaries (and probably 
    !    also for smooth curved boundaries.).
    ! *  The bulk viscosity term should vanish, because the compressibility 
    !    vanished at the wall (though possibly the numerical velocity does not).
    use run_data, only: div_free_velocity
    use support, only: axpy_own_trace_x_I
    
    type(face_list), intent(in) :: face
    real(dp) :: n_dot_stress(mesh_dimen,face%nqp)
    
    integer :: side
    class(scal_func_type), allocatable :: mu_func
    real(dp) :: mu(face%nqp), gder_u(mesh_dimen,mesh_dimen,face%nqp)
    real(dp) :: tensor(mesh_dimen,mesh_dimen)
    integer :: p
    
    call assert(face%is_boundary(.true.),'routine implemented only for boundary faces')
    
    side = side_of_internal_nghb_of_bnd_face(face,only_fluid_mesh=.true.)
    call matprop_set%get_func('dynamic_viscosity', mu_func)
    mu = mu_func%values_at_qps(face,side,TIME)
    call velocity%gders_at_qps(face,side,TIME,gder_u)
    do p = 1, face%nqp
      tensor = gder_u(:,:,p) + transpose(gder_u(:,:,p))
      if (.not.div_free_velocity) call axpy_own_trace_x_I(tensor,-1.0_dp/3.0_dp)
      n_dot_stress(:,p) = mu(p) * matmul(face%normal(:,p),tensor)
    enddo
    if (side == 2) n_dot_stress = - n_dot_stress
  end function
  
  function abs_jump_u_dot_n_at_qps(face) result(abs_jump_u_dot_n)
    ! Given an internal face of the fluid, this returns
    !     abs([u] .dot. n),
    ! at the quadrature points, where u is the velocity and [.] the jump operator.
    type(face_list), intent(in) :: face
    real(dp) :: abs_jump_u_dot_n(face%nqp)
    
    real(dp), dimension(mesh_dimen,face%nqp) :: jump_u
    integer :: p
    
    call assert(.not. face%is_boundary(.true.))
    
    call velocity%jump_at_qps(face,.true.,TIME,jump_u)
    do p = 1, face%nqp
      abs_jump_u_dot_n(p) = abs(dot_product(face%normal(:,p),jump_u(:,p)))
    enddo
  end function abs_jump_u_dot_n_at_qps
  
  function abs_average_u_dot_n_at_qps(face) result(abs_average_u_dot_n)
    ! Given an internal face of the fluid, this returns
    !     abs({u} .dot. n),
    ! at the quadrature points, where u is the velocity and {.} the average operator.
    type(face_list), intent(in) :: face
    real(dp) :: abs_average_u_dot_n(face%nqp)
    
    real(dp), dimension(mesh_dimen,face%nqp) :: average_u
    integer :: p
    
    call assert(.not. face%is_boundary(.true.))
    
    call velocity%avg_at_qps(face,.true.,TIME,average_u)
    do p = 1, face%nqp
      abs_average_u_dot_n(p) = abs(dot_product(face%normal(:,p),average_u(:,p)))
    enddo
  end function abs_average_u_dot_n_at_qps
  
  function unity_at_qps(face) result(one)
    type(face_list), intent(in) :: face
    real(dp) :: one(face%nqp)
    
    one = 1.0_dp
  end function unity_at_qps
  
  subroutine list_bc_id_faces(mesh,only_fluid,bc_id,faces)
    ! Return a list of all faces with 'bc_id'.
    type(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid
    integer, intent(in) :: bc_id
    type(face_ptr), allocatable, intent(out) :: faces(:)
    
    call list_faces(mesh,only_fluid,face_has_id,faces)
  contains
    logical function face_has_id(face)
      type(face_list), intent(in) :: face
      
      face_has_id = face%bc_id == bc_id
    end function face_has_id
  end subroutine list_bc_id_faces
  
  subroutine list_internal_faces(mesh,only_fluid,faces)
    type(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid
    type(face_ptr), allocatable, intent(out) :: faces(:)
    
    call list_faces(mesh,only_fluid,face_is_internal,faces)
  contains
    logical function face_is_internal(face)
      type(face_list), intent(in) :: face
      
      face_is_internal = .not. face%is_boundary(only_fluid)
    end function face_is_internal
  end subroutine list_internal_faces
  
  subroutine list_faces(mesh,only_fluid,filter,faces)
    ! In lieu of an iterator, return a list of all faces in this process for which filter(face).
    use mesh_partitioning, only: my_first_last_elems
    
    interface
      logical function filter_i(face)
        use mesh_objects, only: face_list
        implicit none
        type(face_list), intent(in) :: face
      end function filter_i
    end interface
    
    type(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid
    procedure(filter_i) :: filter
    type(face_ptr), allocatable, intent(out) :: faces(:)
    
    type(elem_id), pointer :: active_elem_list(:)
    integer :: elem_no, first_elem, last_elem
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    integer :: no_faces
    type(elem_ptr), allocatable, dimension(:) :: dummy
    type(face_ptr), allocatable, dimension(:) :: active_subfaces
    integer :: i
    type(face_list), pointer :: sub_face
    type(face_ptr), allocatable :: temp(:)
    
    allocate(faces(0))
    no_faces = 0
    call mesh%get_active_elem_list_ptr(only_fluid,active_elem_list)
    call my_first_last_elems(only_fluid,first_elem,last_elem)
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      call elem%active_subfaces(active_subfaces,dummy)
      do i = 1, size(active_subfaces)
        sub_face => active_subfaces(i)%point
        if (.not. sub_face%is_boundary(only_fluid)) then
          if (associated(elem,sub_face%elem_neighbors(2)%point)) then
            ! Is E2 internal to this process?
            ! yes:  skip face, so as not to count it twice;
            ! no:   the face is counted in another process.
            cycle
          endif
        endif
        if (filter(sub_face)) then
          no_faces = no_faces + 1
          if (no_faces > size(faces)) then
            allocate(temp(2*no_faces))
            temp(:no_faces-1) = faces
            call move_alloc(temp,faces)
          endif
          faces(no_faces)%point => sub_face
        endif
      enddo
    enddo
    allocate(temp,source=faces(:no_faces))
    call move_alloc(temp,faces)
  end subroutine list_faces
end module face_integrals_mod

