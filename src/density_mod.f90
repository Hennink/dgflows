module density_mod
  use qnty_solver_mod, only: qnty_solver_t
  use support, only: false
  implicit none
  
  private
  public :: density_solver_t

  type, extends(qnty_solver_t) :: density_solver_t
    private
  contains
    procedure, non_overridable :: collect_pde_varfrozconst
  end type
  
contains
  subroutine collect_pde_varfrozconst(this, variable_terms, frozen_terms, constant_terms)
    use convection, only: conv_t
    use density_weighted_time_mod, only: expl_time
    use f90_kind,  only: dp
    use numerical_solutions, only: implicit_convection_for_qnty, find_if_add_conv_corr_for_qnty
    use pde_mod, only: pde_t
    use run_data, only: dt
    use h_solver_mod, only: temporal_weights
    use time_der_mod, only: impl_time_deriv

    class(density_solver_t), intent(in) :: this
    type(pde_t), intent(out) :: variable_terms, frozen_terms, constant_terms

    real(dp), allocatable :: h_weights(:)
    type(conv_t) :: conv

    h_weights = temporal_weights(dt)
    call constant_terms%add_term(impl_time_deriv(h_weights(1)))
    call frozen_terms%add_term(expl_time('rho',h_weights(2:)))

    call find_if_add_conv_corr_for_qnty('density_predictor',conv%add_vol_div_corr,conv%add_face_jump_corr)
    conv%explicit = .not. implicit_convection_for_qnty('density_predictor')
    call variable_terms%add_term(conv)
  end subroutine
end module density_mod
