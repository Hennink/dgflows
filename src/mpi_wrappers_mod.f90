module mpi_wrappers_mod
  use, intrinsic :: iso_fortran_env, only: REAL32, REAL64, REAL128
  use mpi_f08
  use assertions, only: assert, assert_eq
  use f90_kind, only: dp
  use petsc_mod, only: petsc_mpi_comm
  implicit none
  
  private
  public :: bcast_wrap, &
            reduce_wrap, allreduce_wrap, &
            wtime

  ! The mpi_f08 interface specifies `type(*):: buffer(..)`, but this does not
  ! work with the 'select type' construct, and so we are reduced to these
  ! interface blocks.

  interface reduce_wrap
    module procedure reduce_wrap__real32
    module procedure reduce_wrap__real64
    module procedure reduce_wrap__real128
  end interface

  interface allreduce_wrap
    module procedure allreduce_wrap__real32
    module procedure allreduce_wrap__real64
    module procedure allreduce_wrap__real128
  end interface

  interface bcast_wrap
    ! https://www.open-mpi.org/doc/v4.0/man3/mpi_bcast.3.php
    module procedure bcast_wrap__integer
    module procedure bcast_wrap__dp
  end interface

contains
  double precision function wtime()
    wtime = mpi_wtime()
  end function


  subroutine reduce_wrap__real32(my_vals,collective_oper,root,vals)
    real(REAL32), contiguous, intent(inout) :: my_vals(..)
    character(*), intent(in) :: collective_oper
    integer, intent(in) :: root
    real(REAL32), contiguous, intent(out) :: vals(..)

    call reduce_wrap__typed(my_vals,collective_oper,root,vals,mpi_real)
  end subroutine

  subroutine reduce_wrap__real64(my_vals,collective_oper,root,vals)
    real(REAL64), contiguous, intent(inout) :: my_vals(..)
    character(*), intent(in) :: collective_oper
    integer, intent(in) :: root
    real(REAL64), contiguous, intent(out) :: vals(..)

    call reduce_wrap__typed(my_vals,collective_oper,root,vals,mpi_double_precision)
  end subroutine

  subroutine reduce_wrap__real128(my_vals,collective_oper,root,vals)
    real(REAL128), contiguous, intent(inout) :: my_vals(..)
    character(*), intent(in) :: collective_oper
    integer, intent(in) :: root
    real(REAL128), contiguous, intent(out) :: vals(..)

    call reduce_wrap__typed(my_vals,collective_oper,root,vals,mpi_real16)
  end subroutine

  subroutine reduce_wrap__typed(my_vals,collective_oper,root,vals,datatype)
    ! 'root' is the address of the recieve buffer.
    ! 
    ! https://www.open-mpi.org/doc/v4.0/man3/MPI_Reduce.3.php
    type(*), contiguous, intent(inout) :: my_vals(..)
    character(*), intent(in) :: collective_oper
    integer, intent(in) :: root
    type(*), contiguous :: vals(..)
    type(MPI_datatype), intent(in) :: datatype

    integer :: count
    type(mpi_op) :: mpi_collective_oper
    integer :: ierr
    
    count = assert_eq(size(my_vals),size(vals))
    if (count == 0) return ! do not call mpi_reduce with count=0 (see https://github.com/open-mpi/ompi/issues/4135)
    
    mpi_collective_oper = str2mpi_collective_oper(collective_oper)
    call mpi_reduce(my_vals,vals,count,datatype,mpi_collective_oper,root,comm(),ierr)
    call assert(ierr==0,'reduce_wrap: nonzero exit status from mpi_reduce')
  end subroutine


  subroutine allreduce_wrap__real32(my_vals,collective_oper,vals)
    real(REAL32), contiguous, intent(inout) :: my_vals(..)
    character(*), intent(in) :: collective_oper
    real(REAL32), contiguous, intent(out) :: vals(..)
    
    call allreduce_wrap__typed(my_vals,collective_oper,vals,mpi_real)
  end subroutine

  subroutine allreduce_wrap__real64(my_vals,collective_oper,vals)
    real(REAL64), contiguous, intent(inout) :: my_vals(..)
    character(*), intent(in) :: collective_oper
    real(REAL64), contiguous, intent(out) :: vals(..)
    
    call allreduce_wrap__typed(my_vals,collective_oper,vals,mpi_double_precision)
  end subroutine

  subroutine allreduce_wrap__real128(my_vals,collective_oper,vals)
    real(REAL128), contiguous, intent(inout) :: my_vals(..)
    character(*), intent(in) :: collective_oper
    real(REAL128), contiguous, intent(out) :: vals(..)
    
    call allreduce_wrap__typed(my_vals,collective_oper,vals,mpi_real16)
  end subroutine

  subroutine allreduce_wrap__typed(my_vals,collective_oper,vals,datatype)
    ! https://www.open-mpi.org/doc/v4.0/man3/MPI_Allreduce.3.php
    type(*), contiguous, intent(in) :: my_vals(..)
    character(*), intent(in) :: collective_oper
    type(*), contiguous :: vals(..)
    type(MPI_datatype), intent(in) :: datatype

    integer :: count
    type(mpi_op) :: mpi_collective_oper
    integer :: ierr
    
    count = assert_eq(size(my_vals),size(vals))
    if (count == 0) return ! do not call mpi_reduce with count=0 (see https://github.com/open-mpi/ompi/issues/4135 )
    
    mpi_collective_oper = str2mpi_collective_oper(collective_oper)
    call mpi_allreduce(my_vals,vals,count,datatype,mpi_collective_oper,comm(),ierr)
    call assert(ierr==0,'allreduce_wrap: nonzero exit status from mpi_allreduce')
  end subroutine


  subroutine bcast_wrap__integer(buffer,root)
    integer, contiguous, intent(inout) :: buffer(..)
    integer, intent(in) :: root
    
    call bcast_wrap__typed(buffer,root,MPI_integer)
  end subroutine

  subroutine bcast_wrap__dp(buffer,root)
    real(dp), contiguous, intent(inout) :: buffer(..)
    integer, intent(in) :: root
    
    call bcast_wrap__typed(buffer,root,datatype_dp())
  end subroutine

  subroutine bcast_wrap__typed(buffer,root,datatype)
    type(*), contiguous, intent(inout) :: buffer(..)
    integer, intent(in) :: root
    type(MPI_datatype), intent(in) :: datatype
    
    integer :: count
    integer :: ierror
    
    count = size(buffer)
    if (count == 0) return
    
    call mpi_bcast(buffer, count, datatype, root, comm(), ierror)
    call assert(ierror == 0,'mpi_bcast: ierror /= 0')
  end subroutine


  pure type(mpi_comm) function comm()
    comm = mpi_comm(mpi_val=petsc_mpi_comm)
  end function

  pure type(mpi_op) function str2mpi_collective_oper(str) result(mpi_collective_oper)
    character(*), intent(in) :: str

    select case(str)
      ! https://www.mpi-forum.org/docs/mpi-3.1/mpi31-report/node459.htm
      case('sum');      mpi_collective_oper = mpi_sum
      case('min');      mpi_collective_oper = mpi_min
      case('max');      mpi_collective_oper = mpi_max
      case('prod');     mpi_collective_oper = mpi_prod
      case('maxloc');   mpi_collective_oper = mpi_maxloc
      case('minloc');   mpi_collective_oper = mpi_minloc
      case('band');     mpi_collective_oper = mpi_band
      case('bor');      mpi_collective_oper = mpi_bor
      case('bxor');     mpi_collective_oper = mpi_bxor
      case('land');     mpi_collective_oper = mpi_land
      case('lor');      mpi_collective_oper = mpi_lor
      case('lxor');     mpi_collective_oper = mpi_lxor
      case('replace');  mpi_collective_oper = mpi_replace
      ! case('no_op');    mpi_collective_oper = mpi_no_op    ! not implemented in our mpi version
      case default;     error stop 'str2mpi_collective_oper: unknown mpi collective operator'
    end select
  end function

  pure type(mpi_datatype) function datatype_dp()
    select case(dp)
      case(real32);     datatype_dp = mpi_real              ! should equal mpi_real4
      case(real64);     datatype_dp = mpi_double_precision  ! should equal mpi_real8
      ! case(real128);    datatype_dp = mpi_real16
      case default;     error stop 'datatype_dp: unknown mpi data type'
    end select
  end function
end module
