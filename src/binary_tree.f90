module binary_tree
  implicit none
  
  private
  public :: int_set
  
  type int_set
    ! Type to store integers like a set, rather than a list/array. The internal
    ! structure is that of a binary search tree.
    ! Useful for:
    !  - collecting values when the total number of (unique) values is unknown;
    !  - sorting [average comp. cost: O(n * log(n)); worst case: O(n**2)].
    private
    type(node_type), pointer :: head => null()
    
  contains
    generic :: insert => insert_in_bin_tree, insert_many
    procedure, non_overridable, private :: insert_in_bin_tree, insert_many
    procedure, non_overridable :: no_nodes => no_nodes_in_bin_tree
    procedure, non_overridable :: sorted_values
    procedure, non_overridable, private :: contains_value => value_in_binary_tree
    final :: free_binary_tree
    ! Obvious possisble extensions include:
    !  - a "delete()" method;
    !  - self-balancing: https://en.wikipedia.org/wiki/Self-balancing_binary_search_tree.
  end type int_set
  
  type node_type
    integer :: value
    type(node_type), pointer :: left => null()
    type(node_type), pointer :: right => null()
  end type node_type
  
contains
  pure subroutine insert_in_bin_tree(this,value)
    class(int_set), intent(inout) :: this
    integer, intent(in) :: value
    
    call insert_at_node(this%head, value)
    
  contains
    recursive pure subroutine insert_at_node(node, value)
      type(node_type), pointer, intent(inout) :: node
      integer, intent(in) :: value
      
      if (.not. associated(node)) then
        allocate(node)
        node%value = value
      elseif (value < node%value) then
        call insert_at_node(node%left, value)
      elseif (value > node%value) then
        call insert_at_node(node%right, value)
      else
        ! Do nothing; the binary tree will not contain nodes with duplicate values.
      end if
    end subroutine insert_at_node
  end subroutine insert_in_bin_tree
  
  subroutine insert_many(this,values)
    class(int_set), intent(inout) :: this
    integer, intent(in) :: values(:)

    integer :: i

    do i = 1, size(values)
      call this%insert(values(i))
    enddo
  end subroutine insert_many
  
  subroutine sorted_values(this, values)
    class(int_set), intent(in) :: this
    integer, allocatable, dimension(:), intent(out) :: values
    
    integer :: no_values
    integer :: first_idx
    
    no_values = this%no_nodes()
    allocate(values(no_values))
    if (associated(this%head)) then
      first_idx = 1
      call take_value_and_increase_idx(this%head,values,first_idx)
    endif
    
  contains
    recursive subroutine take_value_and_increase_idx(node,array,idx_in_array)
      type(node_type), pointer, intent(in):: node
      integer, dimension(no_values), intent(inout) :: array
      integer, intent(inout) :: idx_in_array
      
      if (associated(node)) then
        call take_value_and_increase_idx(node%left,array,idx_in_array)
        array(idx_in_array) = node%value
        idx_in_array = idx_in_array + 1
        call take_value_and_increase_idx(node%right,array,idx_in_array)
      endif
    end subroutine take_value_and_increase_idx
  end subroutine
  
  
  elemental integer function no_nodes_in_bin_tree(this) result(no_nodes)
    class(int_set), intent(in) :: this
    
    no_nodes = 0
    if (associated(this%head)) call count_node_and_children(this%head,no_nodes)
    
  contains
    pure recursive subroutine count_node_and_children(node,total_count)
      type(node_type), intent(in) :: node
      integer, intent(inout) :: total_count
      
      total_count = total_count + 1
      if (associated(node%left))  call count_node_and_children(node%left, total_count)
      if (associated(node%right)) call count_node_and_children(node%right,total_count)
    end subroutine count_node_and_children
  end function no_nodes_in_bin_tree
  
  
  pure logical function value_in_binary_tree(this,value)
    class(int_set), intent(in) :: this
    integer, intent(in) :: value
    
    if (associated(this%head)) then
      value_in_binary_tree = value_in_node_or_children(this%head,value)
    else
      value_in_binary_tree = .false.
    endif
    
  contains
    pure recursive logical function value_in_node_or_children(node,value) result(found)
      type(node_type), intent(in) :: node
      integer, intent(in) :: value
      
      if (value == node%value) then
        found = .true.
      elseif (value < node%value .and. associated(node%left)) then
        found = value_in_node_or_children(node%left,value)
      elseif (value > node%value .and. associated(node%right)) then
        found = value_in_node_or_children(node%right,value)
      else
        found = .false.
      endif
    end function value_in_node_or_children
  end function value_in_binary_tree
  
  
  pure subroutine free_binary_tree(this)
    type(int_set), intent(inout) :: this
    
    call free_node_and_children(this%head)
    
  contains
    pure recursive subroutine free_node_and_children(node)
      type(node_type), pointer, intent(inout) :: node
      
      if (associated(node)) then
        call free_node_and_children(node%left)
        call free_node_and_children(node%right)
        deallocate(node)
      endif
    end subroutine free_node_and_children
  end subroutine free_binary_tree
end module binary_tree

