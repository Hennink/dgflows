module rel_grad_of_scalfunc_mod
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only: vec_func_type
  implicit none
  
  private
  public :: rel_grad_as_vecfunc
  
  type, extends(vec_func_type) :: rel_grad_of_scalfunc_t
    class(scal_func_type), allocatable :: scal_func
  contains
    procedure :: get_values
    procedure :: no_dirs
  end type rel_grad_of_scalfunc_t
  
contains
  type (rel_grad_of_scalfunc_t) function rel_grad_as_vecfunc(scal_func)
    class(scal_func_type), intent(in) :: scal_func
    
    allocate(rel_grad_as_vecfunc%scal_func,source=scal_func)
  end function rel_grad_as_vecfunc
  

  pure integer function no_dirs(this)
    use run_data, only: mesh_dimen
    
    class(rel_grad_of_scalfunc_t), intent(in) :: this
    
    no_dirs = mesh_dimen
  end function no_dirs
  
  subroutine get_values(this,elem,pnt_set,time,values)
    use f90_kind, only: dp
    use mesh_objects, only: elem_list
    use pnt_set_mod, only: pnt_set_t
    
    class(rel_grad_of_scalfunc_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:,:)
    
    call this%scal_func%rel_grad(elem,pnt_set,time,values)
  end subroutine get_values
end module rel_grad_of_scalfunc_mod
