module matprop_lib_patel_channel
  ! https://github.com/Fluid-Dynamics-Of-Energy-Systems-Team/DNSDataVarProp
  ! 
  !     rho  =        (T / Twall)**exp_rho
  !     mu   = mu_0 * (T / Twall)**exp_mu
  !     k    =        (T / Twall)**exp_k
  ! with a constant Pr.
  ! 
  ! Define
  !     q   :=  1 + exp_k - exp_mu ;
  !     Ts  :=  T / Twall .
  ! Then
  !     cp   = k * Pr / mu 
  !          = (Pr / mu_0) * Ts**(q-1) ,
  ! and thus
  !     h    = Pr/(mu_0*q) * Twall * (Ts**q - 1)  -  h0
  ! so that h(T=Twall) = -h0.
  ! 
  ! The condition that
  !     d/dT (rho*h) > 0
  ! becomes
  !     exp_rho*h0*Ts**(exp_rho - 1)/Twall  <  Pr*Ts**(exp_rho - 1)*(exp_rho*Ts**q - exp_rho + q*Ts**q)/(mu_0*q) ,
  ! or, under the assumption that `exp_rho < 0`,
  !     h0  >  Pr*Twall*(exp_rho*Ts**q - exp_rho + q*Ts**q)/(exp_rho*mu_0*q) .
  !     <=>
  !     h0  >  Pr/(mu_0*q) * Twall * ((1+q/exp_rho) * Ts**q - 1) .
  ! 
  use assertions, only: assert, assert_eq
  use f90_kind, only: dp
  implicit none

  real(dp), parameter :: UNINITIALIZED = huge(1.0_dp)
  
  real(dp), parameter :: Twall = 1.0_dp
  real(dp) :: mu_0 = UNINITIALIZED
  real(dp) :: Pr = UNINITIALIZED
  real(dp) :: h0 = 0.0_dp
  real(dp) :: exp_rho = UNINITIALIZED, &
              exp_mu = UNINITIALIZED, &
              q = UNINITIALIZED
  
  real(dp) :: minval_spec_h = -huge(1.0_dp), maxval_spec_h = huge(1.0_dp)

contains
  subroutine get_patel_channel_matprop(unit,matprop_set)
    use functions_lib, only: const_func, zero_scalfunc
    use func_link_mod, only: func_link_t
    use func_set_expansion_mod, only: func_set_expansion_t
    use io_basics, only: REAL_FMT
    use run_data, only: constant_density
    use petsc_mod, only: i_am_master_process
    
    integer, intent(in) :: unit
    type(func_set_expansion_t), intent(out) :: matprop_set

    integer :: ios
    character(200) :: iomsg
    real(dp) :: exp_k
    real(dp) :: cap_T_at_min, cap_T_at_max
    logical :: const_rho

    real(dp) :: crit_h0_at_Tmin, crit_h0_at_Tmax, min_crit_h0, max_crit_h0

    namelist /patel_props/  &
        mu_0, Pr, h0, &
        exp_rho, exp_mu, &
        exp_k, &
        cap_T_at_min, cap_T_at_max

    exp_k = UNINITIALIZED
    cap_T_at_min = UNINITIALIZED
    cap_T_at_max = UNINITIALIZED
    read(unit,patel_props,iostat=ios,iomsg=iomsg)
    call assert(ios == 0,trim(iomsg))

    call assert(mu_0    /= UNINITIALIZED,'get_patel_channel_matprop: specify mu_0')
    call assert(Pr      /= UNINITIALIZED,'get_patel_channel_matprop: specify Pr')
    call assert(h0      /= UNINITIALIZED,'get_patel_channel_matprop: specify h0')
    call assert(exp_rho /= UNINITIALIZED,'get_patel_channel_matprop: specify exp_rho')
    call assert(exp_mu  /= UNINITIALIZED,'get_patel_channel_matprop: specify exp_mu')
    call assert(exp_k   /= UNINITIALIZED,'get_patel_channel_matprop: specify exp_k')
    call assert(cap_T_at_min /= UNINITIALIZED,'get_patel_channel_matprop: specify cap_T_at_min')
    call assert(cap_T_at_max /= UNINITIALIZED,'get_patel_channel_matprop: specify cap_T_at_max')
    call assert(cap_T_at_min < cap_T_at_max,"I expected `cap_T_at_min < cap_T_at_max`.")
    q = 1 + exp_k - exp_mu
    call assert(abs(q) >= 0,'get_patel_channel_matprop: I assumed `0 /= 1 + exp_k - exp_mu`.')

    ! cp > 0, so a minimum for T translates directly to a minimum for h:
    minval_spec_h = T2h(cap_T_at_min)
    maxval_spec_h = T2h(cap_T_at_max)

    ! Define the actual fluid properties:
    call matprop_set%add_func('temperature','spec_enthalpy',T2h)
    call matprop_set%add_func('spec_enthalpy','temperature',h2T)

    call assert(exp_rho <= 0.0_dp, 'Did not expect positive `exp_rho`. Are you sure you want a negative thermal expansion coeff.?')
    if (exp_rho < 0.0_dp .and. i_am_master_process()) then
      ! `1 + q/exp_rho` is nonzero, because `q` is nonzero.
      ! Therefore the d/dT of the critical h0 is nonzero (except at T=0, which never happens).
      ! Therefore the minimum value for `h0` can be determined from the mininum and maximum temperatures.
      crit_h0_at_Tmin = Pr/(mu_0*q) * Twall * ((1+q/exp_rho) * (cap_T_at_min/Twall)**q - 1)
      crit_h0_at_Tmax = Pr/(mu_0*q) * Twall * ((1+q/exp_rho) * (cap_T_at_max/Twall)**q - 1)
      min_crit_h0 = min(crit_h0_at_Tmin, crit_h0_at_Tmax)
      max_crit_h0 = max(crit_h0_at_Tmin, crit_h0_at_Tmax)
      
      write(*,'(a,f0.5,a,f0.5,a)') 'Critical h0 values for your temperature range lie in [', min_crit_h0, ', ', max_crit_h0, '].'
      write(*,'(/,a,/,a,'//REAL_FMT//',/,a,/)') 'Therefore set', '        h0 > ', max_crit_h0, 'to ensure `d/dT (rho*h) > 0`.'
    endif

    const_rho = assert_eq(constant_density, exp_rho == 0.0_dp, 'Expected global option `constant_density` iff `exp_rho == 0.0`.')
    if (const_rho) then
      call matprop_set%add_known_function('density',const_func(1.0_dp))
      call matprop_set%add_known_function('deriv_rho_h',zero_scalfunc())
    else
      call matprop_set%add_func('spec_enthalpy','density',h2rho)
      call matprop_set%add_func('spec_enthalpy','deriv_rho_h',h2deriv_rho_h)
    endif

    if (exp_mu == 0.0_dp) then
      call matprop_set%add_known_function('dynamic_viscosity',const_func(mu_0))
      call matprop_set%add_known_function('k_over_cp',const_func(mu_0 / Pr))
    else
      call matprop_set%add_func('spec_enthalpy','dynamic_viscosity',h2mu)
      call matprop_set%add_func('spec_enthalpy','k_over_cp',h2_k_over_cp)
    endif

    if (exp_rho == exp_mu) then
      call matprop_set%add_known_function('kinematic_viscosity',const_func(mu_0))
      call matprop_set%add_known_function('thermal_diffusivity',const_func(mu_0 / Pr))
    else
      call matprop_set%add_func('spec_enthalpy','kinematic_viscosity',h2nu)
      call matprop_set%add_func('spec_enthalpy','thermal_diffusivity',h2alpha)
    endif

    if (exp_k == 0.0_dp) then
      call matprop_set%add_known_function('thermal_conductivity',const_func(1.0_dp))
    else
      call matprop_set%add_func('spec_enthalpy','thermal_conductivity',h2k)
    endif
    
    if (exp_k == exp_mu) then
      call matprop_set%add_known_function('spec_heat_capacity',const_func(Pr / mu_0))
    else
      call matprop_set%add_func('spec_enthalpy','spec_heat_capacity',h2cp)
    endif
    
    call matprop_set%finalize()
  end subroutine

  
  real(dp) function T2h(T) result(spec_h)
    use math_const, only: SMALL
    real(dp), intent(in) :: T
    
    spec_h = Pr / (mu_0*q) * Twall * ((T/Twall)**q - 1) - h0
    
    ! This function is normally only used because the BC and IC are given in
    ! terms of the temperature, so we do not expect out-of-bounds values.
    call assert(spec_h >= minval_spec_h - SMALL, 'Temperature smaller than minimum.')
    call assert(spec_h <= maxval_spec_h + SMALL, 'Temperature larger than maximum.')
  end function

  pure real(dp) function h2T(spec_h) result(T)
    real(dp), intent(in) :: spec_h
    
    real(dp) :: capped_spec_h

    if (spec_h <= minval_spec_h) then
      capped_spec_h = minval_spec_h
    elseif (spec_h >= maxval_spec_h) then
      capped_spec_h = maxval_spec_h
    else
      capped_spec_h = spec_h
    endif
    T =  (Twall**(q-1) * (Twall + (capped_spec_h + h0) * mu_0*q/Pr))**(1/q)
  end function

  real(dp) function h2rho(spec_h) result(rho)
    real(dp), intent(in) :: spec_h
    
    rho  = (h2T(spec_h) / Twall)**exp_rho
  end function
  
  real(dp) function h2mu(spec_h) result(mu)
    real(dp), intent(in) :: spec_h
    
    mu  = mu_0 * (h2T(spec_h) / Twall)**exp_mu

    call assert(mu > 0,"matprop_lib_patel_channel: mu must be positive")
  end function

  real(dp) function h2nu(spec_h) result(nu)
    real(dp), intent(in) :: spec_h
    
    nu = h2mu(spec_h) / h2rho(spec_h)
  end function
  
  real(dp) function h2k(spec_h) result(k)
    real(dp), intent(in) :: spec_h
    
    real(dp) :: exp_k

    exp_k = q + exp_mu - 1
    k  = (h2T(spec_h) / Twall)**exp_k
  end function
  
  real(dp) function h2cp(spec_h) result(cp)
    real(dp), intent(in) :: spec_h
    
    cp  = (Pr / mu_0) * (h2T(spec_h) / Twall)**(q-1)
  end function

  real(dp) function h2alpha(spec_h) result(alpha)
    real(dp), intent(in) :: spec_h
    
    alpha = h2nu(spec_h) / Pr
  end function

  real(dp) function h2_k_over_cp(spec_h) result(k_over_cp)
    real(dp), intent(in) :: spec_h
    
    k_over_cp = h2mu(spec_h) / Pr
  end function

  real(dp) function h2deriv_rho_h(spec_h) result(deriv_rho_h)
    real(dp), intent(in) :: spec_h

    real(dp) :: T, deriv_rho_T
    
    T = h2T(spec_h)
    deriv_rho_T = exp_rho * (T/Twall)**exp_rho / T
    deriv_rho_h = deriv_rho_T / h2cp(spec_h)
  end function
end module
