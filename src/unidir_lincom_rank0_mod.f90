module unidir_lincom_rank0_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use pnt_set_mod, only: pnt_set_t
  implicit none

  private
  public :: unidir_lincom_rank0_t

  type :: unidir_lincom_rank0_t
    integer :: elem
    real(dp), allocatable :: stencil(:,:)
    integer, public :: time = 1 ! time at which to evaluate the lincom explicitly
  contains
    private
    procedure, non_overridable, public :: eval
    procedure, non_overridable, public :: nnod, np
    generic, public :: set_as_func => set_as_func__elem, set_as_func__face
    procedure, non_overridable :: set_as_func__elem, set_as_func__face
    procedure, non_overridable :: set_stencil_for_func
    generic, public :: set_as_vec_dot_gder => set_as_vec_dot_gder__elem, set_as_vec_dot_gder__face
    procedure, non_overridable :: set_as_vec_dot_gder__elem, set_as_vec_dot_gder__face
    procedure, non_overridable :: set_stencil_for_vecdotgder
  end type unidir_lincom_rank0_t

contains
  function eval(this,solvec) result(transposed_values)
    use solvec_mod, only: solvec_t
    
    class(unidir_lincom_rank0_t), intent(in) :: this
    type(solvec_t), intent(in) :: solvec
    real(dp) :: transposed_values(np(this),solvec%no_dirs())
    
    real(dp) :: values(solvec%no_dirs(),np(this))

    call solvec%dotprod(this%elem,this%stencil,this%time,values)
    transposed_values = transpose(values)
  end function eval

  subroutine set_as_func__elem(this,elem,handler)
    use dof_handler, only: dof_handler_type
    use mesh_objects, only: elem_list

    class(unidir_lincom_rank0_t), intent(out) :: this
    type(elem_list), intent(in) :: elem
    type(dof_handler_type), intent(in) :: handler

    this%elem = elem%get_active_elem_no(handler%only_fluid_mesh)
    call this%set_stencil_for_func(elem%quad_points,handler)
  end subroutine set_as_func__elem

  subroutine set_as_func__face(this,face,side,handler)
    use dof_handler, only: dof_handler_type
    use mesh_objects, only: face_list

    class(unidir_lincom_rank0_t), intent(out) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side
    type(dof_handler_type), intent(in) :: handler

    call assert(side == 1 .or. side == 2,BOUNDS_CHECK)

    associate(ngbr => face%elem_neighbors(side)%point)
      this%elem = ngbr%get_active_elem_no(handler%only_fluid_mesh)
    end associate
    call this%set_stencil_for_func(face%quad_points(side),handler)
  end subroutine set_as_func__face

  subroutine set_stencil_for_func(this,pnt_set,handler)
    use dof_handler, only: dof_handler_type

    class(unidir_lincom_rank0_t), intent(inout) :: this
    type(pnt_set_t), intent(in) :: pnt_set
    type(dof_handler_type), intent(in) :: handler

    integer :: nnod

    nnod = handler%no_nod_elem(this%elem)
    allocate(this%stencil,source=pnt_set%dg_funcs(:nnod,:))
  end subroutine set_stencil_for_func

  subroutine set_as_vec_dot_gder__elem(this,vec,elem,handler)
    use dof_handler, only: dof_handler_type
    use mesh_objects, only: elem_list

    class(unidir_lincom_rank0_t), intent(out) :: this
    real(dp), intent(in) :: vec(:,:)
    type(elem_list), intent(in) :: elem
    type(dof_handler_type), intent(in) :: handler

    call assert(size(vec,1) == elem%dimen(),BOUNDS_CHECK)
    call assert(size(vec,2) == elem%nqp,BOUNDS_CHECK)

    this%elem = elem%get_active_elem_no(handler%only_fluid_mesh)
    call this%set_stencil_for_vecdotgder(vec,elem%quad_points,handler)
  end subroutine set_as_vec_dot_gder__elem

  subroutine set_as_vec_dot_gder__face(this,vec,face,side,handler)
    use dof_handler, only: dof_handler_type
    use mesh_objects, only: face_list

    class(unidir_lincom_rank0_t), intent(out) :: this
    real(dp), intent(in) :: vec(:,:)
    type(face_list), intent(in) :: face
    integer, intent(in) :: side
    type(dof_handler_type), intent(in) :: handler

    call assert(side == 1 .or. side == 2,BOUNDS_CHECK)

    associate(ngbr => face%elem_neighbors(side)%point, &
              quad_points => face%quad_points(side))
      call assert(size(vec,1) == ngbr%dimen(),BOUNDS_CHECK)
      call assert(size(vec,2) == quad_points%np(),BOUNDS_CHECK)
      this%elem = ngbr%get_active_elem_no(handler%only_fluid_mesh)
      call this%set_stencil_for_vecdotgder(vec,quad_points,handler)
    end associate
  end subroutine set_as_vec_dot_gder__face

  subroutine set_stencil_for_vecdotgder(this,vec,pnt_set,handler)
    use dof_handler, only: dof_handler_type

    class(unidir_lincom_rank0_t), intent(inout) :: this
    real(dp), intent(in) :: vec(:,:)
    type(pnt_set_t), intent(in) :: pnt_set
    type(dof_handler_type), intent(in) :: handler

    integer :: p, np, nnod

    np = assert_eq(size(vec,2),pnt_set%np(),BOUNDS_CHECK)

    nnod = handler%no_nod_elem(this%elem)
    allocate(this%stencil(nnod,np))
    do p = 1, np
      this%stencil(:,p) = matmul(pnt_set%dg_transp_gders(:nnod,:,p),vec(:,p))
    enddo
  end subroutine

  pure integer function nnod(this)
    class(unidir_lincom_rank0_t), intent(in) :: this

    nnod = size(this%stencil,1)
  end function nnod

  pure integer function np(this) result(no_points)
    class(unidir_lincom_rank0_t), intent(in) :: this

    no_points = size(this%stencil,2)
  end function np
end module unidir_lincom_rank0_mod
