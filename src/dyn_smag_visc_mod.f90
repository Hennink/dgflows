module dyn_smag_visc_mod
  use assertions, only: assert, assert_eq, assert_normal, BOUNDS_CHECK
  use eqv_elem_groups_mod, only: elem_groups_t
  use f90_kind, only: dp
  use mesh_objects, only: elem_list
  use mesh_type, only: fem_mesh
  use pnt_set_mod, only: pnt_set_t
  implicit none

  private
  public :: init_dyn_les_model_from_file, initialized_dyn_smag, &
            dyn_smag_kin_visc, &
            recalc_avg_LM_MM_eqv_elems
  public :: elem_groups
  public :: LM_MM_in_group

  logical, protected :: initialized_dyn_smag = .false.
  
  type(elem_groups_t), protected :: elem_groups
  real(dp), allocatable, protected :: LM_MM_in_group(:)
  
  logical :: ONLY_FLUID = .true.
  character(100) :: pol_scaling_type = 'linear'
  logical :: clip_smag_param = .false., warn_if_negative = .true.

contains
  subroutine init_dyn_les_model_from_file(mesh,unit)
    type(fem_mesh), intent(in) :: mesh
    integer, intent(in) :: unit

    character(1000) :: group_identifying_vec
    namelist /dyn_smag_params/ pol_scaling_type, group_identifying_vec, clip_smag_param, warn_if_negative
    integer :: ios
    character(200) :: iomsg

    read(unit,dyn_smag_params,iostat=ios,iomsg=iomsg)
    call assert(ios==0,iomsg)

    call elem_groups%init(mesh,group_identifying_vec)
    call elem_groups%write_groups_to_gmsh('eqv_elems__dyn_smag',mesh)
    allocate(LM_MM_in_group(elem_groups%ngroups()))
    initialized_dyn_smag = .true.
  end subroutine init_dyn_les_model_from_file

  subroutine recalc_avg_LM_MM_eqv_elems(mesh)
    use exceptions, only: warning
    use petsc_mod, only: i_am_master_process
    use mpi_wrappers_mod, only: allreduce_wrap
    use math_const, only: SMALL
    use mesh_objects, only: elem_id, elem_list, elem_ptr
    use mesh_partitioning, only: my_first_last_elems
    use numerical_solutions, only: pol_order

    type(fem_mesh), intent(in) :: mesh

    integer :: group
    integer :: elem_no, first_elem, last_elem
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    type(elem_id), pointer :: active_elem_list(:)
    integer :: test_order
    real(dp), allocatable :: LM_at_qps(:), MM_at_qps(:)
    real(dp), dimension(elem_groups%ngroups()) :: my_int_LM, my_int_MM, int_LM, int_MM

    call assert(size(LM_MM_in_group) == elem_groups%ngroups())

    call my_first_last_elems(ONLY_FLUID,first_elem,last_elem)
    call mesh%get_active_elem_list_ptr(ONLY_FLUID,active_elem_list)
    
    my_int_LM = 0.0_dp
    my_int_MM = 0.0_dp
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      group = elem_groups%group_of_elem(elem)
      test_order = pol_order(elem,'mass_flux') / 2
      call least_squares_proj_LM_MM(elem,elem%quad_points,test_order,LM_at_qps,MM_at_qps)
      my_int_LM(group) = my_int_LM(group) + dot_product(LM_at_qps,elem%quot)
      my_int_MM(group) = my_int_MM(group) + dot_product(MM_at_qps,elem%quot)
    enddo
    call allreduce_wrap(my_int_LM,'sum',int_LM)
    call allreduce_wrap(my_int_MM,'sum',int_MM)
    call assert(abs(int_MM) >= SMALL,'MM seems to vanish in the dynamic Smag. model.')
    LM_MM_in_group = int_LM / int_MM

    call assert_normal(LM_MM_in_group,'LM_MM_in_group not normal in the dynamic Smag. model.')
    if (any(LM_MM_in_group < 0) .and. warn_if_negative .and. i_am_master_process()) call warning('C_s < 0 from dynamic procedure.')
  end subroutine recalc_avg_LM_MM_eqv_elems

  subroutine dyn_smag_kin_visc(elem,pnt_set,time,values)
    use numerical_solutions, only: grad_u_invariants

    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)

    real(dp) :: abs_stress(size(values)), LM_MM
    integer :: elem_no, group

    call assert(size(values) == pnt_set%np(), BOUNDS_CHECK)

    call grad_u_invariants(elem, pnt_set, time, frobnorm_ROS=abs_stress)
    abs_stress = sqrt(2.0_dp) * abs_stress
    
    elem_no = elem%get_active_elem_no(ONLY_FLUID)
    group = elem_groups%group_of_elem(elem)
    LM_MM = LM_MM_in_group(group)
    if (clip_smag_param) LM_MM = max(0.0_dp,LM_MM)
    values = 0.5_dp * LM_MM * abs_stress
  end subroutine dyn_smag_kin_visc

  subroutine least_squares_proj_LM_MM(elem,pnt_set,test_order,LM,MM)
    ! For all symmetric matrices, we only reference the lower triangular part.
    use array_opers_mod, only: copy_lower_triangular_part, frobnorm_symm_mtx, sum_entrywise_prod_symm_mtx
    use characteristic_len_mod, only: pol_scaling
    use local_elem, only: no_dg_functions
    use numerical_solutions, only: velocity, pol_order
    use vec_func_mod, only: vec_func_type
    use run_data, only: mesh_dimen
    use support, only: axpy_own_trace_x_I

    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: test_order
    real(dp), allocatable, intent(out) :: LM(:), MM(:)

    real(dp), allocatable :: u_at_qps(:,:), grad_u_at_qps(:,:,:)

    real(dp), dimension(mesh_dimen) :: u
    real(dp), dimension(mesh_dimen,mesh_dimen) :: gder_u, sij, sij_d
    real(dp) :: s_abs

    real(dp), dimension(:,:,:), allocatable :: uiuj_proj, &
                                               sij_d_proj, &
                                               s_sij_d_proj, &
                                               sij_proj
    real(dp), allocatable :: ui_proj(:,:)

    real(dp) :: ui_coarse(mesh_dimen), abs_s_coarse
    real(dp), dimension(mesh_dimen,mesh_dimen) :: uiuj_coarse, sij_d_coarse, s_sij_d_coarse, sij_coarse
    real(dp), dimension(mesh_dimen,mesh_dimen) :: mij, lij_d
    real(dp) :: alpha

    integer :: dimen
    integer :: nnod_filter, order_m
    integer :: i, j, p, qp, nqp
    integer, parameter :: TIME = 1

    order_m = pol_order(elem,'mass_flux')
    call assert(test_order >= 0,"test_order must be nonnegative")
    call assert(test_order < order_m,"test_order must be smaller than order_m")

    nqp = assert_eq(size(elem%quot),elem%nqp,elem%quad_points%np(),BOUNDS_CHECK)
    dimen = assert_eq(mesh_dimen,elem%dimen(),BOUNDS_CHECK)

    allocate(u_at_qps(dimen,nqp))
    allocate(grad_u_at_qps(dimen,dimen,nqp))
    call velocity%get_values(elem,elem%quad_points,TIME,u_at_qps)
    call velocity%get_gders(elem,elem%quad_points,TIME,grad_u_at_qps)

    nnod_filter = no_dg_functions(elem%eltype,test_order)
    allocate(uiuj_proj(nnod_filter,dimen,dimen), source=0.0_dp)
    allocate(ui_proj(nnod_filter,dimen), source=0.0_dp)
    allocate(sij_d_proj(nnod_filter,dimen,dimen), source=0.0_dp)
    allocate(s_sij_d_proj(nnod_filter,dimen,dimen), source=0.0_dp)
    allocate(sij_proj(nnod_filter,dimen,dimen), source=0.0_dp)

    ! Take inner products of the quantities with the low-order basis:
    do qp = 1, nqp
      u = u_at_qps(:,qp)
      gder_u = grad_u_at_qps(:,:,qp)
      do j = 1, dimen
        do i = j, dimen
          sij(i,j) = 0.5_dp * (gder_u(i,j) + gder_u(j,i))
        end do
      end do
      call copy_lower_triangular_part(sij,sij_d)
      call axpy_own_trace_x_I(sij_d,-1.0_dp/3.0_dp)
      s_abs = sqrt(2.0_dp) * frobnorm_symm_mtx(sij)

      associate(proj_funcs => elem%quad_points%dg_funcs(:nnod_filter,qp))
        do j = 1, dimen
          do i = j, dimen
            ! <f_m, u_i u_j >
            uiuj_proj(:,i,j) = uiuj_proj(:,i,j) &
                + elem%quot(qp) * proj_funcs * u(i) * u(j)
            ! <f_m, S_ij^d>
             sij_d_proj(:,i,j) = sij_d_proj(:,i,j) &
                + elem%quot(qp) * proj_funcs * sij_d(i,j)
            ! <f_m, |S|*S_ijd>
            s_sij_d_proj(:,i,j) = s_sij_d_proj(:,i,j) &
                + elem%quot(qp) * proj_funcs * s_abs * sij_d(i,j)
            ! <f_m, S_ij>
            sij_proj(:,i,j) = sij_proj(:,i,j) &
                + elem%quot(qp) * proj_funcs * sij(i,j)
          end do
          ! <f_m, u_j>
          ui_proj(:,j) = ui_proj(:,j) &
              + elem%quot(qp) * proj_funcs * u(j)
        end do
      end associate
    end do

    ! Transform the projection into solution coefficients by solving for the mass matrix:
    do j = 1, dimen
      call elem%solve_for_massmat(uiuj_proj(:,j:dimen,j))
      call elem%solve_for_massmat(sij_d_proj(:,j:dimen,j))
      call elem%solve_for_massmat(s_sij_d_proj(:,j:dimen,j))
      call elem%solve_for_massmat(sij_proj(:,j:dimen,j))
    enddo
    call elem%solve_for_massmat(ui_proj)

    alpha = (pol_scaling(elem,test_order,pol_scaling_type) / pol_scaling(elem,order_m,pol_scaling_type))**2
    allocate(LM(pnt_set%np()), MM(pnt_set%np()))
    do p = 1, pnt_set%np()
      ! Evaluate the coarsened quantities at the points with the coarsened coefficients:
      associate(funcs_at_p => pnt_set%dg_funcs(:nnod_filter,p))
        do j = 1, dimen
          uiuj_coarse(j:dimen,j)    = matmul(funcs_at_p,uiuj_proj(:,j:dimen,j))
          sij_d_coarse(j:dimen,j)   = matmul(funcs_at_p,sij_d_proj(:,j:dimen,j))
          s_sij_d_coarse(j:dimen,j) = matmul(funcs_at_p,s_sij_d_proj(:,j:dimen,j))
          sij_coarse(j:dimen,j)     = matmul(funcs_at_p,sij_proj(:,j:dimen,j))
        enddo
        ui_coarse = matmul(funcs_at_p,ui_proj)
      end associate
      abs_s_coarse = sqrt(2.0_dp) * frobnorm_symm_mtx(sij_coarse)

      ! Leonard stress tensor and M stress tensor:
      do j = 1, dimen
        do i = j, dimen
          lij_d(i,j) = uiuj_coarse(i,j) - ui_coarse(i) * ui_coarse(j)
          mij(i,j) = s_sij_d_coarse(i,j) - alpha * abs_s_coarse * sij_d_coarse(i,j)
        end do
      end do
      call axpy_own_trace_x_I(lij_d,-1.0_dp/3.0_dp)

      ! LM and MM:
      LM(p) = sum_entrywise_prod_symm_mtx(mij,lij_d)
      MM(p) = sum_entrywise_prod_symm_mtx(mij,mij)
    end do
    call assert_normal(LM,'dyn. smag. model: LM not IEEE normal')
    call assert_normal(MM,'dyn. smag. model: MM not IEEE normal')
  end subroutine least_squares_proj_LM_MM
end module dyn_smag_visc_mod
