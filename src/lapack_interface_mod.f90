module lapack_interface_mod
  ! Routines for BLAS, LAPACK, MAGMA:  http://www.icl.utk.edu/%7Emgates3/docs/lapack.html
  use lapack95, only: posv, &   ! http://www.netlib.org/lapack95/DOC/la_posv.txt
                      gesv, &   ! http://www.netlib.org/lapack95/DOC/la_gesv.txt
                      potrf, &  ! http://www.netlib.org/lapack95/lug95/node340.html
                      gelsy     ! http://www.netlib.org/lapack95/DOC/la_gelsy.txt
  use array_opers_mod, only: copy_lower_triangular_part
  use assertions, only: assert, assert_eq
  use f90_kind, only: dp
  implicit none
  
  private
  public :: solve_linsys_lose_matrix, &
            solve_linsys_keep_matrix
  public :: solve_SPD_linsys_lose_matrix, &
            solve_SPD_linsys_keep_matrix
  public :: least_squares_solve
  public :: cholesky_decomposition
  public :: UPPER, LOWER
  
  interface solve_linsys_lose_matrix
    module procedure solve_linsys_lose_matrix_one_rhs
    module procedure solve_linsys_lose_matrix_many_rhs
  end interface solve_linsys_lose_matrix
  
  interface solve_linsys_keep_matrix
    module procedure solve_linsys_keep_matrix_one_rhs
    module procedure solve_linsys_keep_matrix_many_rhs
  end interface solve_linsys_keep_matrix
  
  interface solve_SPD_linsys_lose_matrix
    module procedure solve_SPD_linsys_lose_matrix_one_rhs
    module procedure solve_SPD_linsys_lose_matrix_many_rhs
  end interface solve_SPD_linsys_lose_matrix
  
  interface solve_SPD_linsys_keep_matrix
    module procedure solve_SPD_linsys_keep_matrix_one_rhs
    module procedure solve_SPD_linsys_keep_matrix_many_rhs
  end interface solve_SPD_linsys_keep_matrix

  interface least_squares_solve
    module procedure least_squares_solve_one_rhs
  end interface least_squares_solve
  
  type uplo_t
    private
    character :: opt
  end type uplo_t
  
  type(uplo_t), parameter :: UPPER = uplo_t('U'), LOWER = uplo_t('L')
  
contains
  subroutine solve_SPD_linsys_lose_matrix_many_rhs(matrix,b,uplo)
    real(dp), dimension(:,:), intent(inout) :: matrix
    real(dp), dimension(:,:), intent(inout) :: b
    type(uplo_t), optional, intent(in) :: uplo
    
    character :: uplo_c
    integer :: info
    
    if (present(uplo)) then
      uplo_c = uplo%opt
    else
      uplo_c = 'U'
    endif
    
    call posv(matrix,b,uplo=uplo_c,info=info)
    call check_posv_lapack_info(info)
  end subroutine solve_SPD_linsys_lose_matrix_many_rhs
  
  subroutine solve_SPD_linsys_lose_matrix_one_rhs(matrix,b,uplo)
    real(dp), dimension(:,:), intent(inout) :: matrix
    real(dp), dimension(:), intent(inout) :: b
    type(uplo_t), optional, intent(in) :: uplo
    
    character :: uplo_c
    integer :: info
    
    if (present(uplo)) then
      uplo_c = uplo%opt
    else
      uplo_c = 'U'
    endif
    
    call posv(matrix,b,uplo=uplo_c,info=info)
    call check_posv_lapack_info(info)
  end subroutine solve_SPD_linsys_lose_matrix_one_rhs
  
  
  subroutine solve_SPD_linsys_keep_matrix_many_rhs(matrix,b)
    real(dp), intent(in) :: matrix(:,:)
    real(dp), intent(inout) :: b(:,:)

    real(dp) :: copy(size(matrix,1),size(matrix,2))

    call copy_lower_triangular_part(matrix,copy)
    call solve_SPD_linsys_lose_matrix_many_rhs(copy,b,LOWER)
  end subroutine solve_SPD_linsys_keep_matrix_many_rhs

  subroutine solve_SPD_linsys_keep_matrix_one_rhs(matrix,b)
    real(dp), intent(in) :: matrix(:,:)
    real(dp), intent(inout) :: b(:)

    real(dp) :: copy(size(matrix,1),size(matrix,2))

    call copy_lower_triangular_part(matrix,copy)
    call solve_SPD_linsys_lose_matrix_one_rhs(copy,b,LOWER)
  end subroutine solve_SPD_linsys_keep_matrix_one_rhs

  subroutine solve_linsys_lose_matrix_many_rhs(matrix,b)
    real(dp), dimension(:,:), intent(inout) :: matrix
    real(dp), dimension(:,:), intent(inout) :: b
    
    integer :: info
    
    call gesv(matrix,b,info=info)
    call check_gesv_lapack_info(info)
  end subroutine solve_linsys_lose_matrix_many_rhs
  
  subroutine solve_linsys_lose_matrix_one_rhs(matrix,b)
    real(dp), dimension(:,:), intent(inout) :: matrix
    real(dp), dimension(:), intent(inout) :: b
    
    integer :: info
    
    call gesv(matrix,b,info=info)
    call check_gesv_lapack_info(info)
  end subroutine solve_linsys_lose_matrix_one_rhs
  
  
  subroutine solve_linsys_keep_matrix_many_rhs(matrix,b)
    real(dp), dimension(:,:), intent(in) :: matrix
    real(dp), dimension(:,:), intent(inout) :: b
    
    real(dp), dimension(size(matrix,1),size(matrix,2)) :: copy_matrix
    
    copy_matrix = matrix
    call solve_linsys_lose_matrix_many_rhs(copy_matrix,b)
  end subroutine solve_linsys_keep_matrix_many_rhs
  
  subroutine solve_linsys_keep_matrix_one_rhs(matrix,b)
    real(dp), dimension(:,:), intent(in) :: matrix
    real(dp), dimension(:), intent(inout) :: b
    
    real(dp), dimension(size(matrix,1),size(matrix,2)) :: copy_matrix
    
    copy_matrix = matrix
    call solve_linsys_lose_matrix_one_rhs(copy_matrix,b)
  end subroutine solve_linsys_keep_matrix_one_rhs
  
  
  subroutine least_squares_solve_one_rhs(matrix,b)
    ! http://netlib.org/lapack/explore-html/d7/d3b/group__double_g_esolve_gaf7d1ceb7410cdc1425d5c79414343a47.html
    real(dp), intent(inout) :: matrix(:,:)
    real(dp), intent(inout) :: b(:)
    
    integer :: info
    
    call gelsy(A=matrix,B=b,INFO=info)
    
    call assert(info == 0)
    ! info < 0: the i-th argument in the FORTRAN 77 call (sgelsy/dgelsy) had
    !           an illegal value;
    ! info > 0: undocumented behavior
  end subroutine least_squares_solve_one_rhs
  
  subroutine cholesky_decomposition(matrix,uplo)
    ! Assuming A is SPD, computes a Cholesky decomposition, such that
    !     A = L * L^T  (for uplo == LOWER), or
    !     A = U * U^T  (for uplo == UPPER),
    ! and stores the result in a triangular part of A.
    ! Only the relevant triangular part of A is referenced.
    real(dp), intent(inout) :: matrix(:,:)
    type(uplo_t), intent(in) :: uplo

    integer :: info

    call assert(size(matrix,1) == size(matrix,2),'matrix must be symm. for Chol. decomp.')
    call POTRF(A=matrix,uplo=uplo%opt,info=info)
    call assert(info == 0,'info /=0 in Cholesky decomposition')
  end subroutine cholesky_decomposition
  
  subroutine check_gesv_lapack_info(info)
    integer, intent(in) :: info
    
    ! http://www.netlib.org/lapack95/DOC/la_gesv.txt
    ! 
    ! info = 0 : successful exit.
    ! info = i < 0 : i'th input argument of posv has a bad value
    ! info = i > 0 : The upper triangular matrix is singular (U(i,i) = 0)
    ! 
    call assert(info == 0)
  end subroutine check_gesv_lapack_info
  
  subroutine check_posv_lapack_info(info)
    integer, intent(in) :: info
    
    ! http://www.netlib.org/lapack95/DOC/la_posv.txt
    ! 
    ! info = 0 : successful exit.
    ! info = i < 0 : i'th input argument of posv has a bad value
    ! info = i > 0 : leading minor of order i of A is not positive definite
    ! 
    call assert(info == 0)
  end subroutine check_posv_lapack_info
end module lapack_interface_mod
