module petsc_ksp_mod
#include "petsc/finclude/petscksp.h"
  use petscksp
  
  use assertions, only: assert, assert_eq
  use exceptions, only: error
  use f90_kind, only: dp
  use petsc_basic_types_mod, only: fort2petsc, petsc2fort
  use petsc_pc_mod, only: petsc_pc_t
  use petsc_mat_mod, only: petsc_mat_type
  use petsc_mod, only: I_am_master_process, petsc_mpi_comm
  implicit none
  
  private
  public :: petsc_ksp_t
  ! Some procedures for the 'raw' PETSc object, because I am not sure how to 
  ! return an array of the `petsc_ksp_t` wrapper from `get_pc_subksps`:
  public :: get_type__raw, get_pc__raw, set_up__raw, set_options_prefix__raw, set_from_options__raw
  
  type :: petsc_ksp_t
    ! KSP manages the linear solves in PETSc (including all Krylov methods and
    ! direct solvers that do no use Krylov accelerators).
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSP.html#KSP
    type(tKSP) :: petsc_ksp
    logical :: initialized = .false.
  contains
    procedure, non_overridable :: create
    procedure, non_overridable :: set_operators
    procedure, non_overridable, private :: operators_set
    procedure, non_overridable :: replace_operator
    procedure, non_overridable :: set_type, get_type
    procedure, non_overridable :: set_options_prefix
    procedure, non_overridable :: options_prefix
    procedure, non_overridable :: get_pc
    procedure, non_overridable :: get_pc_subksps
    procedure, non_overridable :: set_from_options
    procedure, non_overridable :: set_up
    procedure, non_overridable :: set_reuse_preconditioner
    procedure, non_overridable :: converged_default_set_UIR_norm
    procedure, non_overridable, private :: set_tolerances
    procedure, non_overridable :: get_tolerances
    procedure, non_overridable :: set_initial_guess_nonzero
    procedure, non_overridable :: solve
    procedure, non_overridable :: get_iteration_number
    procedure, non_overridable :: view
    procedure, non_overridable :: converged_reason_view
    procedure, non_overridable :: verify_convergence
    procedure, non_overridable :: residual_norm
    final :: destroy
  end type petsc_ksp_t

contains
  subroutine create(this)
    ! Creates the default KSP context, which is GMRES with a restart of 30,
    ! using modified Gram-Schmidt orthogonalization.
    !
    ! Collective on MPI_Comm.
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPCreate.html
    use petscksp, only: KSPCreate
    
    class(petsc_ksp_t), intent(out) :: this
    
    PetscErrorCode :: ierr
    
    call KSPCreate(petsc_mpi_comm,this%petsc_ksp,ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPCreate')
    this%initialized = .true.
  end subroutine create
  
  subroutine set_type(this, ksptype)
    ! Builds KSP for a particular solver.
    !
    ! Logically Collective on KSP
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPSetType.html
    use petscksp, only: KSPSetType
    
    class(petsc_ksp_t), intent(inout) :: this
    character(*), intent(in) :: ksptype
    
    PetscErrorCode :: ierr
    
    call KSPSetType(this%petsc_ksp, f2p_ksptype(ksptype), ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPSetType')
  end subroutine

  function get_type(this) result(ksptype)
    class(petsc_ksp_t), intent(in) :: this
    character(:), allocatable :: ksptype

    ksptype = get_type__raw(this%petsc_ksp)
  end function

  function get_type__raw(petsc_ksp) result(ksptype)
    interface
      subroutine KSPGetType(ksp, type, ierr)
        use petscksp
        implicit none
        KSP, intent(in) :: ksp
        KSPType, intent(out) :: type
        PetscErrorCode, intent(out) :: ierr
      end subroutine
    end interface
    type(tKSP), intent(in) :: petsc_ksp
    character(:), allocatable :: ksptype

    PetscErrorCode :: ierr
    KSPType :: petsc_ksptype

    call KSPGetType(petsc_ksp, petsc_ksptype, ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPGetType')
    ksptype = p2f_ksptype(petsc_ksptype)
  end function

  subroutine set_options_prefix(this, prefix)
    class(petsc_ksp_t), intent(inout) :: this
    character(*), intent(in) :: prefix

    call set_options_prefix__raw(this%petsc_ksp, prefix)
  end subroutine

  subroutine set_options_prefix__raw(petsc_ksp, prefix)
    use, intrinsic :: iso_c_binding

    interface
      subroutine KSPSetOptionsPrefix(ksp, prefix, ierr)
        use petscksp
        implicit none
        KSP, intent(inout) :: ksp
        character(len=*,kind=c_char), intent(in) :: prefix ! array does not work... Maybe PETSc converts chars from Fortran to C.
        PetscErrorCode, intent(out) :: ierr
      end subroutine
    end interface
    type(tKSP), intent(inout) :: petsc_ksp
    character(*), intent(in) :: prefix

    PetscErrorCode :: ierr
    
    call KSPSetOptionsPrefix(petsc_ksp, prefix, ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPSetOptionsPrefix')
  end subroutine

  function options_prefix(this)
    use, intrinsic :: iso_c_binding

    interface
      subroutine KSPGetOptionsPrefix(ksp, prefix, ierr)
        ! The PETSc function anomolously returns an `int`, not a PetscErrorCode. No idea why.
        use petscksp
        implicit none
        KSP, intent(in) :: ksp
        character(len=*,kind=c_char), intent(inout) :: prefix ! should be long enough to hold the char. Array does not work...
                                                              ! Maybe PETSc converts chars from Fortran to C. (Thus INOUT.)
        PetscErrorCode, intent(out) :: ierr
      end subroutine
    end interface
    class(petsc_ksp_t), intent(inout) :: this
    character(:), allocatable :: options_prefix
    
    PetscErrorCode :: ierr
    character(100,kind=c_char) :: petsc_prefix

    petsc_prefix = ''
    call KSPGetOptionsPrefix(this%petsc_ksp, petsc_prefix, ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPGetOptionsPrefix')
    options_prefix = trim(petsc_prefix)
  end function

  function p2f_ksptype(petsc_ksptype) result(ksp)
    KSPType, intent(in) :: petsc_ksptype
    character(:), allocatable :: ksp

    select case(petsc_ksptype)
      case(KSPRICHARDSON); ksp = 'richardson'
      case(KSPCHEBYSHEV); ksp = 'chebyshev'
      case(KSPCG); ksp = 'cg'
      case(KSPGROPPCG); ksp = 'groppcg'
      case(KSPPIPECG); ksp = 'pipecg'
      case(KSPPIPECGRR); ksp = 'pipecgrr'
      case(KSPPIPELCG); ksp = 'pipelcg'
      ! case(KSPPIPEPRCG); ksp = 'pipeprcg'
      case(KSPPIPECG2); ksp = 'pipecg2'
      case(KSPCGNE); ksp = 'cgne'
      case(KSPNASH); ksp = 'nash'
      case(KSPSTCG); ksp = 'stcg'
      case(KSPGLTR); ksp = 'gltr'
      case(KSPFCG); ksp = 'fcg'
      case(KSPPIPEFCG); ksp = 'pipefcg'
      case(KSPGMRES); ksp = 'gmres'
      case(KSPPIPEFGMRES); ksp = 'pipefgmres'
      case(KSPFGMRES); ksp = 'fgmres'
      case(KSPLGMRES); ksp = 'lgmres'
      case(KSPDGMRES); ksp = 'dgmres'
      case(KSPPGMRES); ksp = 'pgmres'
      case(KSPTCQMR); ksp = 'tcqmr'
      case(KSPBCGS); ksp = 'bcgs'
      case(KSPIBCGS); ksp = 'ibcgs'
      case(KSPFBCGS); ksp = 'fbcgs'
      case(KSPFBCGSR); ksp = 'fbcgsr'
      case(KSPBCGSL); ksp = 'bcgsl'
      case(KSPPIPEBCGS); ksp = 'pipebcgs'
      case(KSPCGS); ksp = 'cgs'
      case(KSPTFQMR); ksp = 'tfqmr'
      case(KSPCR); ksp = 'cr'
      case(KSPPIPECR); ksp = 'pipecr'
      case(KSPLSQR); ksp = 'lsqr'
      case(KSPPREONLY); ksp = 'preonly'
      case(KSPQCG); ksp = 'qcg'
      case(KSPBICG); ksp = 'bicg'
      case(KSPMINRES); ksp = 'minres'
      case(KSPSYMMLQ); ksp = 'symmlq'
      case(KSPLCD); ksp = 'lcd'
      case(KSPPYTHON); ksp = 'python'
      case(KSPGCR); ksp = 'gcr'
      case(KSPPIPEGCR); ksp = 'pipegcr'
      case(KSPTSIRM); ksp = 'tsirm'
      case(KSPCGLS); ksp = 'cgls'
      case(KSPFETIDP); ksp = 'fetidp'
      case(KSPHPDDM); ksp = 'hpddm'
      case default;   call error('unknown PETSC KSP type: ksp=' // petsc_ksptype)
    end select
  end function




  function f2p_ksptype(ksp_fortran_char) result(ksptype)
    character(*), intent(in) :: ksp_fortran_char
    KSPType :: ksptype

    select case(trim(ksp_fortran_char))
      case('richardson');   ksptype = KSPRICHARDSON
      case('chebyshev');    ksptype = KSPCHEBYSHEV
      case('cg');           ksptype = KSPCG
      case('groppcg');      ksptype = KSPGROPPCG
      case('pipecg');       ksptype = KSPPIPECG
      case('pipecgrr');     ksptype = KSPPIPECGRR
      case('pipelcg');      ksptype = KSPPIPELCG
      ! case('pipeprcg');     ksptype = KSPPIPEPRCG
      case('pipecg2');      ksptype = KSPPIPECG2
      case('cgne');         ksptype = KSPCGNE
      case('nash');         ksptype = KSPNASH
      case('stcg');         ksptype = KSPSTCG
      case('gltr');         ksptype = KSPGLTR
      case('fcg');          ksptype = KSPFCG
      case('pipefcg');      ksptype = KSPPIPEFCG
      case('gmres');        ksptype = KSPGMRES
      case('pipefgmres');   ksptype = KSPPIPEFGMRES
      case('fgmres');       ksptype = KSPFGMRES
      case('lgmres');       ksptype = KSPLGMRES
      case('dgmres');       ksptype = KSPDGMRES
      case('pgmres');       ksptype = KSPPGMRES
      case('tcqmr');        ksptype = KSPTCQMR
      case('bcgs');         ksptype = KSPBCGS
      case('ibcgs');        ksptype = KSPIBCGS
      case('fbcgs');        ksptype = KSPFBCGS
      case('fbcgsr');       ksptype = KSPFBCGSR
      case('bcgsl');        ksptype = KSPBCGSL
      case('pipebcgs');     ksptype = KSPPIPEBCGS
      case('cgs');          ksptype = KSPCGS
      case('tfqmr');        ksptype = KSPTFQMR
      case('cr');           ksptype = KSPCR
      case('pipecr');       ksptype = KSPPIPECR
      case('lsqr');         ksptype = KSPLSQR
      case('preonly');      ksptype = KSPPREONLY
      case('qcg');          ksptype = KSPQCG
      case('bicg');         ksptype = KSPBICG
      case('minres');       ksptype = KSPMINRES
      case('symmlq');       ksptype = KSPSYMMLQ
      case('lcd');          ksptype = KSPLCD
      case('python');       ksptype = KSPPYTHON
      case('gcr');          ksptype = KSPGCR
      case('pipegcr');      ksptype = KSPPIPEGCR
      case('tsirm');        ksptype = KSPTSIRM
      case('cgls');         ksptype = KSPCGLS
      case('fetidp');       ksptype = KSPFETIDP
      case('hpddm');        ksptype = KSPHPDDM
      case default;         call error('Unknown KSPtype: '//trim(ksp_fortran_char))
    end select
  end function

  subroutine get_pc(this, pc)
    class(petsc_ksp_t), intent(inout) :: this
    type(petsc_pc_t), intent(inout) :: pc
    
    call get_pc__raw(this%petsc_ksp, pc)
  end subroutine
  
  subroutine get_pc__raw(petsc_ksp, pc)
    ! Returns a pointer to the preconditioner context set with KSPSetPC().
    !
    ! Not Collective.
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPGetPC.html
    use petscksp, only: KSPGetPC
    
    type(tKSP), intent(inout) :: petsc_ksp ! maybe just `intent(in)`, but PC is part of KSP
    type(petsc_pc_t), intent(inout) :: pc
    
    PetscErrorCode :: ierr
    
    call KSPGetPC(petsc_ksp, pc%petsc_pc, ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPGetPC')
  end subroutine
  
  subroutine get_pc_subksps(this,subksps)
    ! Gets the local KSP contexts for all blocks on this processor. 
    !
    ! Not Collective.
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPGetPC.html
    use petscsys, only: PETSC_NULL_INTEGER
    use petscksp, only: PCASMGetSubKSP, PCBJacobiGetSubKSP, PETSC_NULL_KSP
    
    class(petsc_ksp_t), intent(inout) :: this
    type(tKSP), allocatable, intent(out) :: subksps(:)
    
    type(petsc_pc_t) :: pc
    character(:), allocatable :: pctype
    PetscInt :: n_blocks
    PetscErrorCode :: ierr

    call this%get_pc(pc)
    pctype = pc%get_type()
    
    n_blocks = -(huge(n_blocks)-1) ! ensure something bad happens if the PETSc call does not work as expected
    if (pctype == 'asm') then
      call PCASMGetSubKSP    (pc%petsc_pc,n_blocks,PETSC_NULL_INTEGER(1),PETSC_NULL_KSP,ierr)
      call assert(ierr == 0,'ierr /= 0 in PCASMGetSubKSP')
    elseif (pctype == 'bjacobi') then
      call PCBJacobiGetSubKSP(pc%petsc_pc,n_blocks,PETSC_NULL_INTEGER(1),PETSC_NULL_KSP,ierr)
      call assert(ierr == 0,'ierr /= 0 in PCBJacobiGetSubKSP')
    else
      call error('cannot get subKSPs from a PC different from BJACOBI or ASM')
    endif
    
    call assert(petsc2fort(n_blocks) == 1,'We can handle only 1 block per processor')
    allocate(subksps(petsc2fort(n_blocks)))
    
    if (pctype == 'asm') then
      call PCASMGetSubKSP    (pc%petsc_pc,PETSC_NULL_INTEGER(1),PETSC_NULL_INTEGER(1),subksps,ierr)
      call assert(ierr == 0,'ierr /= 0 in PCASMGetSubKSP')
    elseif (pctype == 'bjacobi') then
      call PCBJacobiGetSubKSP(pc%petsc_pc,PETSC_NULL_INTEGER(1),PETSC_NULL_INTEGER(1),subksps,ierr)
      call assert(ierr == 0,'ierr /= 0 in PCBJacobiGetSubKSP')
    else
      call error('cannot get subKSPs from a PC different from BJACOBI or ASM')
    endif
  end subroutine
  
  subroutine set_from_options(this)
    class(petsc_ksp_t), intent(inout) :: this
    
    call set_from_options__raw(this%petsc_ksp)
  end subroutine

  subroutine set_from_options__raw(petsc_ksp)
    ! Sets KSP options from the options database. This routine must be called
    ! before KSPSetUp() if the user is to be allowed to set the Krylov type.
    !
    ! Collective on KSP
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPSetFromOptions.html
    use petscksp, only: KSPSetFromOptions

    type(tKSP), intent(inout) :: petsc_ksp
    
    PetscErrorCode :: ierr
    
    call KSPSetFromOptions(petsc_ksp, ierr)
    call assert(ierr == 0, 'ierr /= 0 in KSPSetFromOptions')
  end subroutine
  
  subroutine set_up(this)
    class(petsc_ksp_t), intent(inout) :: this
    
    call set_up__raw(this%petsc_ksp)
  end subroutine
  
  subroutine set_up__raw(petsc_ksp)
    ! Sets up the internal data structures for the later use of an iterative solver.
    !
    ! Collective on KSP
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPSetUp.html
    use petscksp, only: KSPSetUp

    type(tKSP), intent(inout) :: petsc_ksp
    
    PetscErrorCode :: ierr
    
    call KSPSetUp(petsc_ksp,ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPSetUp')
  end subroutine
  
  subroutine set_reuse_preconditioner(this,reuse)
    ! Reuse the current preconditioner, do not construct a new one 
    ! even if the operator changes 
    !
    ! Collective on KSP
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPSetReusePreconditioner.html
    use petscksp, only: KSPSetReusePreconditioner

    class(petsc_ksp_t), intent(inout) :: this
    logical, intent(in) :: reuse
    
    PetscErrorCode :: ierr
    
    call KSPSetReusePreconditioner(this%petsc_ksp,fort2petsc(reuse),ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPSetReusePreconditioner')
  end subroutine set_reuse_preconditioner
  
  subroutine converged_default_set_UIR_norm(this)
    ! U.I.R. = "Use Initial Residual Norm"
    ! 
    ! Makes the default convergence test use `|| B*(b - A*(initial guess))||` instead of `|| B*b ||`,
    ! where `B` is the left-preconditioner.
    ! 
    class(petsc_ksp_t), intent(inout) :: this

    PetscErrorCode :: ierr

    call KSPConvergedDefaultSetUIRNorm(this%petsc_ksp, ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPConvergedDefaultSetUIRNorm')
  end subroutine

  subroutine set_tolerances(this,rtol,abstol,dtol,maxits)
    ! Sets the relative, absolute, divergence, and maximum iteration
    ! tolerances used by the default KSP convergence testers.
    !
    ! rtol:   relative convergence tolerance, relative decrease in the
    !         (possibly preconditioned) residual norm
    ! abstol: absolute convergence tolerance absolute size of the
    !         (possibly preconditioned) residual norm
    ! dtol:   divergence tolerance, amount (possibly preconditioned) residual
    !         norm can increase before KSPConvergedDefault() concludes that
    !         the method is diverging
    ! maxits  maximum number of iterations to use
    !
    ! Logically Collective on KSP
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPSetTolerances.html
    use petscksp, only: KSPSetTolerances

    class(petsc_ksp_t), intent(inout) :: this
    real(dp), intent(in) :: rtol, abstol, dtol
    integer, intent(in) :: maxits
    
    PetscErrorCode :: ierr
    
    call KSPSetTolerances(this%petsc_ksp,fort2petsc(rtol),fort2petsc(abstol),fort2petsc(dtol),fort2petsc(maxits),ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPSetTolerances')
  end subroutine
  
  subroutine get_tolerances(this,rtol,abstol,dtol,maxits)
    ! Gets the relative, absolute, divergence, and maximum iteration
    ! tolerances used by the default KSP convergence tests.
    !
    ! rtol:    relative convergence tolerance
    ! abstol:  absolute convergence tolerance
    ! dtol:    divergence tolerance
    ! maxits:  maximum number of iterations
    !
    ! Not Collective
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPGetTolerances.html
    use petscksp, only: KSPGetTolerances
    
    class(petsc_ksp_t), intent(in) :: this
    real(dp), intent(out) :: rtol, abstol, dtol
    integer, intent(out) :: maxits
    
    PetscErrorCode :: ierr
    
    PetscReal :: petsc_rtol, petsc_abstol, petsc_dtol
    PetscInt :: petsc_maxits
    
    call KSPGetTolerances(this%petsc_ksp,petsc_rtol,petsc_abstol,petsc_dtol,petsc_maxits,ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPGetTolerances')
    rtol = petsc2fort(petsc_rtol)
    abstol = petsc2fort(petsc_abstol)
    dtol = petsc2fort(petsc_dtol)
    maxits = petsc2fort(petsc_maxits)
  end subroutine get_tolerances
  
  subroutine set_initial_guess_nonzero(this,flg)
    ! Tells the iterative solver that the initial guess is nonzero;
    ! otherwise KSP assumes the initial guess is to be zero (and thus zeros it
    ! out before solving).
    !
    ! Logically Collective on KSP
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPSetInitialGuessNonzero.html#KSPSetInitialGuessNonzero
    use petscksp, only: KSPSetInitialGuessNonzero
    
    class(petsc_ksp_t), intent(inout) :: this
    logical, intent(in) :: flg
    
    PetscErrorCode :: ierr
    
    call KSPSetInitialGuessNonzero(this%petsc_ksp,fort2petsc(flg),ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPSetInitialGuessNonzero')
  end subroutine set_initial_guess_nonzero
  
  subroutine solve(this,b,x)
    ! Solves linear system.
    ! b:  right hand side vector
    ! x:  solution
    !
    ! Though Petsc allows 'a' and 'b' to be the same, our INTENT statements
    ! assume that this is not the case.
    !
    ! Collective on KSP
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPSolve.html
    use petscksp, only: KSPSolve
    use petsc_vec_mod, only: petsc_vec_type
    
    class(petsc_ksp_t), intent(inout) :: this
    type(petsc_vec_type), intent(in) :: b
    type(petsc_vec_type), intent(inout) :: x
    
    PetscErrorCode :: ierr
    
    call KSPSolve(this%petsc_ksp,b%petsc_vec,x%petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPSolve')
  end subroutine solve
  
  integer function get_iteration_number(this) result(its)
    ! Gets the current iteration number.
    ! If the KSPSolve() is complete, returns the number of iterations used.
    !
    ! Not Collective
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPGetIterationNumber.html
    use petscksp, only: KSPGetIterationNumber

    class(petsc_ksp_t), intent(in) :: this
    
    PetscErrorCode :: ierr
    PetscInt :: petsc_its
    
    call KSPGetIterationNumber(this%petsc_ksp,petsc_its,ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPGetIterationNumber')
    its = petsc2fort(petsc_its)
  end function get_iteration_number
  
  subroutine view(this)
    ! Prints the KSP data structure
    ! 
    !!! COLLECTIVE ON KSP !!!
    use petscksp, only: KSPView, PETSC_VIEWER_STDOUT_WORLD

    class(petsc_ksp_t), intent(inout) :: this
    
    PetscErrorCode :: ierr

    call KSPView(this%petsc_ksp, PETSC_VIEWER_STDOUT_WORLD, ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPView')
  end subroutine

  subroutine converged_reason_view(this)
    use petscksp, only: PETSC_VIEWER_STDOUT_WORLD

    interface
      subroutine KSPConvergedReasonView(ksp, viewer, ierr)
        use petscksp
        implicit none
        KSP, intent(in) :: ksp
        PetscViewer, intent(in) :: viewer
        PetscErrorCode, intent(out) :: ierr
      end subroutine
    end interface
    class(petsc_ksp_t), intent(inout) :: this
    
    PetscErrorCode :: ierr

    call KSPConvergedReasonView(this%petsc_ksp, PETSC_VIEWER_STDOUT_WORLD, ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPConvergedReasonView')
  end subroutine

  subroutine verify_convergence(this)
    ! Prints whether the solver converged normally.
    !
    ! Not Collective
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPGetConvergedReason.html
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPConvergedReason.html#KSPConvergedReason
    use petscksp, only: KSPGetConvergedReason, &
        KSP_CONVERGED_ITERATING, KSP_DIVERGED_ITS, KSP_DIVERGED_DTOL, KSP_DIVERGED_BREAKDOWN, &
        KSP_DIVERGED_BREAKDOWN_BICG, KSP_DIVERGED_NONSYMMETRIC, KSP_DIVERGED_INDEFINITE_PC, KSP_DIVERGED_INDEFINITE_PC, &
        KSP_DIVERGED_NANORINF, KSP_DIVERGED_INDEFINITE_MAT, KSP_DIVERGED_PC_FAILED
    use exceptions, only: error, warning
    use string_manipulations, only: int2char
    
    class(petsc_ksp_t), intent(in) :: this
    
    PetscErrorCode :: ierr
    KSPConvergedReason :: reason
    character(:), allocatable :: problem
    logical :: converged
    integer :: iter_done, max_iter
    real(dp) :: rel_tol, abs_tol, div_tol
    
    call KSPGetConvergedReason(this%petsc_ksp,reason,ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPGetConvergedReason')
    converged = reason > 0
    if (converged) then
      iter_done = this%get_iteration_number()
      if (I_am_master_process()) write(*,'(a)') 'convergence in ' // int2char(iter_done)
    else
      select case(reason)
        case(KSP_CONVERGED_ITERATING); problem = "The solver is not yet finished."
        case(KSP_DIVERGED_ITS); problem = "required more than max. iterations to reach convergence"
        case(KSP_DIVERGED_DTOL); problem = "residual norm increased by a factor of divtol"
        case(KSP_DIVERGED_BREAKDOWN); problem = "generic breakdown in method"
        case(KSP_DIVERGED_BREAKDOWN_BICG); problem = "A breakdown in the KSPBICG method was &
            &detected, so the method could not continue to enlarge the Krylov space."
        case(KSP_DIVERGED_NONSYMMETRIC); problem = "It appears the operator or preconditioner is &
            &not symmetric and this Krylov method (KSPCG, KSPMINRES, KSPCR) requires symmetry."
        case(KSP_DIVERGED_INDEFINITE_PC); problem = "It appears the preconditioner is indefinite &
            &(has both positive and negative eigenvalues), and this Krylov method (KSPCG) requires &
            &it to be positive definite.   This can happen with the PCICC preconditioner, use &
            &-pc_factor_shift_positive_definite to force the PCICC preconditioner to generate a &
            &positive definite preconditioner "
        case(KSP_DIVERGED_NANORINF); problem = "residual norm became NaN or Inf (likely due to 0/0)"
        case(KSP_DIVERGED_INDEFINITE_MAT); problem = "indefinite matrix"
        case(KSP_DIVERGED_PC_FAILED); problem = "It was not possible to build the requested &
            &preconditioner. This is usually due to a zero pivot in a factorization. It can also &
            &result from a failure in a subpreconditioner inside a nested preconditioner such as &
            &PCFIELDSPLIT.   Notes: Run with -ksp_error_if_not_converged to stop the program when &
            &the error is detected and print an error message with details."
        case default
          call error("cannot interpret PETSC's KSPConvergedReason")
      end select
      if (I_am_master_process()) then
        if (reason == KSP_DIVERGED_NANORINF) then
          call error("NO CONVERGENCE: " // trim(problem))
        else
          call warning("NO CONVERGENCE: " // trim(problem))
        endif
      endif
      
      call this%get_tolerances(rel_tol,abs_tol,div_tol,max_iter)
      if (I_am_master_process()) then
        write(*,'(a)') 'Tolerances:'
        write(*,'(a,es8.1)') '    rel_tol = ', rel_tol
        write(*,'(a,es8.1)') '    abs_tol = ', abs_tol
        write(*,'(a,es8.1)') '    div_tol = ', div_tol
        write(*,'(a,i0)')    '    max_iter = ', max_iter
      endif
    endif
  end subroutine
  
  real(dp) function residual_norm(this) result(rnorm)
    ! Gets the last (approximate preconditioned) residual norm that has been computed.
    ! 
    ! To print the 2-norm of true linear system residual, use the command line option
    !     `-ksp_view_final_residual`
    !
    ! Not Collective
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPGetResidualNorm.html
    use petscksp, only: KSPGetResidualNorm

    class(petsc_ksp_t), intent(in) :: this
    
    PetscErrorCode :: ierr
    PetscReal :: petsc_rnorm
    
    call KSPGetResidualNorm(this%petsc_ksp,petsc_rnorm,ierr)
    call assert(ierr == 0,'ierr /=0 in KSPGetResidualNorm')

    rnorm = petsc2fort(petsc_rnorm)
  end function
  
  subroutine set_operators(this,A,P)
    ! Sets the matrix associated with the linear system and a (possibly)
    ! different one associated with the preconditioner.
    !
    ! A:  matrix that defines the linear system
    ! P:  matrix to be used in constructing the preconditioner, usually the same as A
    !
    ! Collective on KSP and Mat.
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPSetOperators.html
    use petscksp, only: KSPSetOperators

    class(petsc_ksp_t), intent(inout) :: this
    type(petsc_mat_type), intent(in) :: A, P
    
    PetscErrorCode :: ierr
    
    call assert(A%has_nullspace .eqv. P%has_nullspace, "Preconditioner should have nullspace iff Matrix does")

    call KSPSetOperators(this%petsc_ksp,A%petsc_mat,P%petsc_mat,ierr)
    call assert(ierr == 0,'ierr /= 0 in KSPSetOperators')
  end subroutine

  logical function operators_set(this)
    class(petsc_ksp_t), intent(in) :: this
    
    PetscErrorCode :: ierr
    PetscBool :: mat, pmat

    call error('seem like `KSPGetOperatorsSet` is not avaible for Fortran')
    ! call KSPGetOperatorsSet(this%petsc_ksp, mat, pmat, ierr)
    call assert(ierr == 0, 'ierr /= 0 in KSPGetOperatorsSet')
    
    operators_set = assert_eq(petsc2fort(mat), petsc2fort(pmat), "Expected neither or both Matrix and Preconditioner to be set.")
  end function
  
  subroutine replace_operator(this,tobe_replaced,mat)
    ! Replace one of the two operators in the KSP context, following the description in 
    ! KSPSetOperators manual page:
    !
    ! "If you wish to replace either Amat or Pmat but leave the other one untouched,
    !  then first call KSPGetOperators() to get the one you wish to keep, call PetscObjectReference() 
    !  on it and then pass it back in in your call to KSPSetOperators()"
    !
    ! Collective on KSP and Mat.
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPSetOperators.html
    use petscmat, only: PETSC_NULL_MAT
    use petscsys, only: PetscObjectReference
    
    class(petsc_ksp_t), intent(inout) :: this
    character(*), intent(in) :: tobe_replaced
    type(petsc_mat_type), intent(in) :: mat
    
    type(petsc_mat_type) :: other_mat
    
    PetscErrorCode :: ierr
    
    select case(tobe_replaced)
      case('Amat');  call KSPGetOperators(this%petsc_ksp,PETSC_NULL_MAT,other_mat%petsc_mat,ierr)
      case('Pmat');  call KSPGetOperators(this%petsc_ksp,other_mat%petsc_mat,PETSC_NULL_MAT,ierr)
      case default;  call error('unknown KSP operator: '//tobe_replaced)
    end select
    call assert(ierr == 0,'ierr /= 0 in KSPGetOperators')
    
    call PetscObjectReference(other_mat%petsc_mat,ierr)
    call assert(ierr == 0,'ierr /= 0 in PetscObjectReference')
    
    select case(tobe_replaced)
      case('Amat');  call KSPSetOperators(this%petsc_ksp,mat%petsc_mat,other_mat%petsc_mat,ierr)
      case('Pmat');  call KSPSetOperators(this%petsc_ksp,other_mat%petsc_mat,mat%petsc_mat,ierr)
      case default;  call error('unknown KSP operator: '//tobe_replaced)
    end select
    call assert(ierr == 0,'ierr /= 0 in KSPSetOperators')
  end subroutine replace_operator
  
  subroutine destroy(this)
    ! Destroys KSP context.
    !
    ! Collective on KSP
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/KSPDestroy.html
    use petscksp, only: KSPDestroy

    type(petsc_ksp_t), intent(inout) :: this
    
    PetscErrorCode :: ierr
    
    if (this%initialized) then
      call KSPDestroy(this%petsc_ksp,ierr)
      call assert(ierr == 0,'ierr /= 0 in KSPDestroy')
    endif
  end subroutine destroy
end module petsc_ksp_mod
