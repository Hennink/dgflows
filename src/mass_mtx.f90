module mass_mtx
  use dof_handler, only: dof_handler_type
  use f90_kind, only: dp
  use mesh_type, only: fem_mesh
  use petsc_mat_mod, only: petsc_mat_type
  implicit none
  
  private
  public :: get_mass_matrix
  public :: get_inv_mass_matrix
  
contains
  subroutine get_mass_matrix(mesh,handler,mass_matrix)
    type(fem_mesh), intent(in) :: mesh
    type(dof_handler_type), intent(in) :: handler
    type(petsc_mat_type), intent(out) :: mass_matrix
    
    call get_mass_matrix_or_inverse(mesh,handler,get_inverse=.false.,matrix=mass_matrix)
  end subroutine get_mass_matrix
  
  subroutine get_inv_mass_matrix(mesh,handler,inv_mass_matrix)
    type(fem_mesh), intent(in) :: mesh
    type(dof_handler_type), intent(in) :: handler
    type(petsc_mat_type), intent(out) :: inv_mass_matrix
    
    call get_mass_matrix_or_inverse(mesh,handler,get_inverse=.true.,matrix=inv_mass_matrix)
  end subroutine get_inv_mass_matrix
  
  subroutine get_mass_matrix_or_inverse(mesh,handler,get_inverse,matrix)
    use dof_handler, only: get_dof, set_type_and_preallocate
    use mesh_objects, only: elem_list, elem_id
    use mesh_partitioning, only: my_first_last_elems
    
    type(fem_mesh), intent(in) :: mesh
    type(dof_handler_type), intent(in) :: handler
    logical, intent(in) :: get_inverse
    type(petsc_mat_type), intent(out) :: matrix
    
    integer :: first_elem, last_elem, elem_no, dim
    type(elem_list), pointer :: elem
    type(elem_id) :: id
    integer, allocatable :: dofs(:)
    integer :: order
    real(dp), allocatable :: mat(:,:)
    
    call matrix%create()
    ! Use block size 1, because it needs a MatMult with the gradient operator:
    call set_type_and_preallocate(matrix, mesh, handler, handler, coupling_depth=0, symm=.false., force_aij = .true.)
    
    call my_first_last_elems(.true.,first_elem,last_elem)
    do elem_no = first_elem, last_elem
      id = mesh%active_fluid_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      order = handler%order(elem_no)
      if (get_inverse) then
        mat = elem%inv_massmat(order)
      else
        mat = elem%massmat(order)
      endif
      do dim = 1, handler%no_dirs
        call get_dof(handler,1,elem_no,dim,dofs)
        call matrix%add(dofs,dofs,mat)
      enddo
      deallocate(mat)
    enddo
    call matrix%finalize_assembly()
  end subroutine get_mass_matrix_or_inverse
end module mass_mtx
