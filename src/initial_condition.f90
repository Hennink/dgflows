module initial_condition
  use assertions, only: assert
  use exceptions, only: error
  use solvec_mod, only: solvec_t
  implicit none
  
  private
  public :: initialize
  
contains
  subroutine initialize(mesh)
    use func_set_mod, only: func_set_t
    use io_basics, only: open_input_file, read_line_from_input_file
    use matprop_mod, only: expand_func_set_with_matprop
    use mesh_partitioning, only: determine_old_new_permutations, io_elem_no_permutations
    use mesh_type, only: fem_mesh
    use numerical_solutions, only: io_solvec_for_qnty
    use phys_time_mod, only: init_phys_times
    use quantities_mod, only: sym2name
    use read_funcs_mod, only: read_block_as_func_specs
    use run_data, only: ic_file, randomize_ic_mmtum_press
    use string_manipulations, only: separate_first_word, split
    use string_mod, only: as_chars
    use time_step_mod, only: smoothen_mtum_press
    
    type(fem_mesh), intent(in) :: mesh

    integer :: unit_no
    character(:), allocatable :: solvec_line, solvec_basename, solvec_qnties_line, solvec_qnties(:)
    type(func_set_t) :: ic_set
    integer :: i

    call open_input_file(ic_file,unit_no)
      call read_block_as_func_specs(unit_no,ic_set)
      call expand_func_set_with_matprop(ic_set)
      
      call read_line_from_input_file(unit_no,solvec_line)
      if (solvec_line /= '') call separate_first_word(solvec_line,solvec_basename,solvec_qnties_line,':')
    close(unit_no)

    if (solvec_line == '') then
      call init_phys_times('')
    else
      call init_phys_times(solvec_basename // '_times')
    endif
    call project_icset_onto_solvecs(ic_set,mesh)
    if (solvec_line /= '') then
      call io_elem_no_permutations(file_path=solvec_basename//'_permutation',from_file=.true.)
      call determine_old_new_permutations()
      solvec_qnties = as_chars(split(solvec_qnties_line,','))
      do i = 1, size(solvec_qnties)
        call assert(.not.ic_set%has(solvec_qnties(i)),'IC: qnty specified twice: ' // solvec_qnties(i))
        call io_solvec_for_qnty(solvec_qnties(i),solvec_basename,from_file=.true.)
      enddo
    endif

    call smoothen_mtum_press(mesh)
    if (randomize_ic_mmtum_press) call randomize_mmtum_press(mesh)
  end subroutine initialize
  
  subroutine project_icset_onto_solvecs(ic_set,mesh)
    use func_set_mod, only: func_set_t
    use mesh_type, only: fem_mesh
    use numerical_solutions, only: all_qnty_names
    use string_mod, only: string_t
    use vec_func_mod, only: vec_func_type
    
    type(func_set_t), intent(in) :: ic_set
    type(fem_mesh), intent(in) :: mesh
    
    character(:), allocatable :: solvec_names(:)
    integer :: i
    class(vec_func_type), allocatable :: ic
    
    solvec_names = all_qnty_names()
    do i = 1, size(solvec_names)
      associate(name => solvec_names(i))
        if (ic_set%has(name)) then
          call ic_set%get_func(name,ic)
          call set_single_ic(mesh,name,ic)
        endif
      end associate
    enddo
  end subroutine project_icset_onto_solvecs
  
  subroutine set_single_ic(mesh,qnty_name,ic)
    use galerkin_projection, only: galerkin_proj
    use mesh_type, only: fem_mesh
    use numerical_solutions, only: get_solvec
    use string_mod, only: string_t
    use vec_func_mod, only: vec_func_type
    
    type(fem_mesh), intent(in) :: mesh
    character(*), intent(in) :: qnty_name
    class(vec_func_type), intent(in) :: ic
    
    type(solvec_t), pointer :: solvec_ptr
    
    solvec_ptr => get_solvec(qnty_name)
    ! Project the IC at time=2, which is the last 'old' time, so t=0.0 for a new calculation:
    call galerkin_proj(mesh,solvec_ptr%handler,ic,2,solvec_ptr%petsc_coeffs)
    call solvec_ptr%update_ghost_values()
    call solvec_ptr%copy_new_to_all_old_coeff()
  end subroutine set_single_ic
  
  subroutine randomize_mmtum_press(mesh)
    ! Add a random fluctuation to the newest mass flux, and do pressure correction.
    ! Also copy the newest values to the old values.
    use f90_kind, only: dp
    use mesh_type, only: fem_mesh
    use numerical_solutions, only: get_solvec, new2old_all_solvecs
    use pressure_corr, only: pressure_correction
    use run_data, only: dt, ic_pertubation_coeff

    type(fem_mesh), intent(in) :: mesh

    type(solvec_t), pointer :: mass_flux
    real(dp) :: rel_norm_Pcorr

    mass_flux => get_solvec('mass_flux')
    call mass_flux%petsc_coeffs%perturb_randomly(ic_pertubation_coeff)
    call mass_flux%update_ghost_values()
    call pressure_correction(mesh,dt,rel_norm_Pcorr)
    call new2old_all_solvecs()
  end subroutine randomize_mmtum_press
end module initial_condition
