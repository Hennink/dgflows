submodule(boundary_types) boundary_types__noslip_fixedQ_wall
  use vec_func_mod, only: vec_func_type
  implicit none
  
  type, extends(wall_bnd_t) :: noslip_fixedQ_wall_t
    class(vec_func_type), allocatable :: velocity
    character(:), allocatable :: heat_flux_input_line
  contains
    procedure :: read_specs_from_file
    procedure :: get_bc
  end type noslip_fixedQ_wall_t
  
contains
  module procedure noslip_fixedQ_wall
    allocate(noslip_fixedQ_wall_t :: bnd)
  end procedure noslip_fixedQ_wall
  
  subroutine read_specs_from_file(this,unit_number)
    use io_basics, only: read_line_from_input_file
    use read_funcs_mod, only: read_line_as_qnty_eq_func
    
    class(noslip_fixedQ_wall_t), intent(out) :: this
    integer, intent(in) :: unit_number
    
    call read_line_as_qnty_eq_func(unit_number,'velocity',this%velocity)
    call read_line_from_input_file(unit_number,this%heat_flux_input_line)
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types, only: bc_type, dirichlet_bc_type, inhom_neumann_bc_type
    use boundary_types_support_mod, only: assemble_heat_flux_neumann_bc
    use exceptions, only: error
    use func_operations_mod
    use functions_lib, only: zero_vecfunc
    use numerical_solutions, only: matprop_set
    use rans_mod, only: rans_model
    use scal_func_mod, only: scal_func_type
    
    class(noslip_fixedQ_wall_t), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    
    type(dirichlet_bc_type), allocatable :: dirichlet_bc
    type(inhom_neumann_bc_type), allocatable :: inhom_neumann_bc
    class(scal_func_type), allocatable :: rho
    
    select case(qnty_name)
      case('vol_enthalpy','spec_enthalpy','temperature')
        call assemble_heat_flux_neumann_bc(this%heat_flux_input_line,this%total_area,qnty_name,inhom_neumann_bc)
        call move_alloc(inhom_neumann_bc,bc)
      case('mass_flux','velocity')
        allocate(dirichlet_bc)
        call matprop_set%get_func('density',rho)
        allocate(dirichlet_bc%inhom_part,source= rho * this%velocity)
        call move_alloc(dirichlet_bc,bc)
      case('vol_turb1','vol_turb2')
        if (rans_model%is_low_Re()) then
          allocate(dirichlet_bc)
          allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(1))
          call move_alloc(dirichlet_bc,bc)
        else
          call error("NOT IMPLEMENTED: qnty_name=" // qnty_name)
        endif
      case('wall_dist')
        allocate(dirichlet_bc)
        allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(1))
        call move_alloc(dirichlet_bc,bc)
      case default
        write(*,'(a)') 'ERROR: no bc for '//qnty_name//' in noslip_fixedQ_wall'
        error stop
    end select
  end subroutine get_bc
end submodule boundary_types__noslip_fixedQ_wall
