submodule(boundary_types) boundary_types__noslip_isolated_wall
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only: vec_func_type
  implicit none
  
  type, extends(wall_bnd_t) :: noslip_isolated_wall_t
    class(vec_func_type), allocatable :: velocity
  contains
    procedure :: read_specs_from_file
    procedure :: get_bc
  end type noslip_isolated_wall_t
  
contains
  module procedure noslip_isolated_wall
    allocate(noslip_isolated_wall_t :: bnd)
  end procedure noslip_isolated_wall
  
  subroutine read_specs_from_file(this,unit_number)
    use read_funcs_mod, only: read_line_as_qnty_eq_func
    
    class(noslip_isolated_wall_t), intent(out) :: this
    integer, intent(in) :: unit_number
    
    call read_line_as_qnty_eq_func(unit_number,'velocity',this%velocity)
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types, only: bc_type, dirichlet_bc_type, hom_neumann_bc_type
    use exceptions, only: error
    use func_operations_mod, only: operator(*)
    use functions_lib, only: zero_vecfunc
    use numerical_solutions, only: matprop_set
    use rans_mod, only: rans_model
    use scal_func_mod, only: scal_func_type
    
    class(noslip_isolated_wall_t), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc

    type(dirichlet_bc_type), allocatable :: dirichlet_bc
    type(hom_neumann_bc_type), allocatable :: hom_neumann_bc
    class(scal_func_type), allocatable :: rho
    
    select case(qnty_name)
      case('temperature','spec_enthalpy','vol_enthalpy','density')
        allocate(hom_neumann_bc)
        call move_alloc(hom_neumann_bc,bc)
      case('mass_flux','velocity')
        allocate(dirichlet_bc)
        call matprop_set%get_func('density',rho)
        allocate(dirichlet_bc%inhom_part,source= rho * this%velocity)
        call move_alloc(dirichlet_bc,bc)
      case('vol_turb1','vol_turb2')
        if (rans_model%is_low_Re()) then
          allocate(dirichlet_bc)
          allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(1))
          call move_alloc(dirichlet_bc,bc)
        else
          allocate(bc,source=hom_neumann_bc_type())
        endif
      case('wall_dist')
        allocate(dirichlet_bc)
        allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(1))
        call move_alloc(dirichlet_bc,bc)
      case default
        call error('no bc for '//qnty_name//' in noslip_isolated_wall')
    end select
  end subroutine get_bc
end submodule boundary_types__noslip_isolated_wall
