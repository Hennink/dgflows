module boundary_types_support_mod
  use boundary_condition_types
  use func_operations_mod, only: operator(+), operator(-), operator(*), operator(/)
  use numerical_solutions, only: matprop_set
  use scal_func_mod, only: scal_func_type
  implicit none
  
  private
  public :: assemble_htc_robin_bc
  public :: assemble_heat_flux_neumann_bc
  
contains
  subroutine assemble_htc_robin_bc(qnty_name,htc,T_ext,robin_bc)
    use vec_func_scal_compon_mod, only: as_vecfunc
    
    character(*), intent(in) :: qnty_name
    class(scal_func_type), intent(in) :: htc, T_ext
    type(robin_bc_type), allocatable, intent(out) :: robin_bc
    
    class(scal_func_type), allocatable :: cp, h, T, rho
    
    call matprop_set%get_func('spec_heat_capacity',cp)
    call matprop_set%get_func('spec_enthalpy',h)
    call matprop_set%get_func('temperature',T)
    
    allocate(robin_bc)
    select case(qnty_name)
      case('temperature')
        ! Use an allocate statement, because the equivalent assignment statement gives a compilation error with ifort 18.0.1.
        ! ("error #7497: The variable must be type compatible with the expression and of the same rank.   [COEFF]")
        allocate(robin_bc%coeff,source=htc)
        robin_bc%inhom_part = as_vecfunc(htc * T_ext)
      case('spec_enthalpy')
        robin_bc%coeff = htc / cp
        robin_bc%inhom_part = as_vecfunc(htc * (T_ext - T + h/cp))
      case('vol_enthalpy')
        robin_bc%coeff = htc / (rho * cp)
        robin_bc%inhom_part = as_vecfunc(htc * (T_ext - T + h/cp))
      case default
        write(*,'(a)') 'ERROR: cannot apply a htc bc for '//qnty_name
        error stop
    end select
  end subroutine assemble_htc_robin_bc
  
  subroutine assemble_heat_flux_neumann_bc(heat_flux_specs,bnd_total_area,qnty_name,inhom_neumann_bc)
    use f90_kind, only: dp
    use functions_lib, only: const_func, string2func
    use string_manipulations, only: char2real, separate_first_word
    use vec_func_mod, only: vec_func_type
    
    character(*), intent(in) :: heat_flux_specs, qnty_name
    real(dp), intent(in) :: bnd_total_area
    type(inhom_neumann_bc_type), allocatable, intent(out) :: inhom_neumann_bc
    
    real(dp) :: total_flux, flux(1)
    character(:), allocatable :: lhs, rhs
    class(scal_func_type), allocatable :: rho, cp
    class(vec_func_type), allocatable :: aux_vecfunc
    
    allocate(inhom_neumann_bc)
    
    call separate_first_word(heat_flux_specs,lhs,rhs,'=')
    select case(lhs)
      case('heat_flux')
        call string2func(rhs,inhom_neumann_bc%inhom_part)
      case('total_power')
        total_flux = char2real(rhs)
        flux(1) = total_flux / bnd_total_area
        inhom_neumann_bc%inhom_part = const_func(flux)
      case default
        error stop 'ERROR assemble_heat_flux_neumann_bc: cannot interpret heat flux specs'
    end select
    if (qnty_name == 'temperature') then
      call move_alloc(inhom_neumann_bc%inhom_part,aux_vecfunc)
      call matprop_set%get_func('density',rho)
      call matprop_set%get_func('spec_heat_capacity',cp)
      inhom_neumann_bc%inhom_part = aux_vecfunc / (rho * cp)
    endif
  end subroutine assemble_heat_flux_neumann_bc
end module boundary_types_support_mod
