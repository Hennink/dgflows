module les_const_eddy_visc_mod
  ! [1] Berselli et al, 'math. of LES', Springer (2006)
  ! [2] Sagaut, 'LES for incompressible fluids', Springer (2006)
  ! [3] Garnier, Adams, Sagaut, 'LES for compressible flows', Springer (2009)
  ! [4] Ansys User manual: https://www.sharcnet.ca/Software/Ansys/17.0/en-us/help/cfx_thry/cfxTurbLargSubg.html
  ! [5] Ghorbaniasl G., Lacor C. (2008) "Sensitivity of SGS Models and of Quality of LES to Grid Irregularity."
  !     In: Meyers J., Geurts B.J., Sagaut P. (eds) "Quality and Reliability of Large-Eddy Simulations." 
  !     Ercoftac Series, vol 12. Springer, Dordrecht
  ! [6] ANSYS theory manual:  https://www.afs.enea.it/project/neptunius/docs/fluent/html/th/node55.htm
  use assertions
  use fortran_parser_interface_mod, only: add_eqparser, evaluate
  use characteristic_len_mod, only: anisotropic_charact_len
  use code_const, only: UNINITIALIZED_CHAR, UNINITIALIZED_INT
  use exceptions, only: error
  use f90_kind, only: dp
  use functions_lib, only: string2func
  use math_const, only: pi
  use mesh_objects, only: elem_list
  use numerical_solutions, only: grad_u_invariants, velocity
  use pnt_set_mod, only: pnt_set_t
  use run_data, only: les_xyz2yplus, mesh_dimen
  use scal_func_mod, only: scal_func_type
  use wall_dist_mod, only: wall_dist_func
  implicit none

  private
  public :: init_const_les_model_from_file, initialized_const_eddy_visc
  public :: can_calculate_smag
  public :: square_C_x_Delta__smag
  public :: const_sfs_nu, const_smag_sfs_nu, const_wale_sfs_nu, const_qr_sfs_nu

  logical, protected :: initialized_const_eddy_visc = .false.
  procedure(const_smag_sfs_nu), pointer, protected :: const_sfs_nu => null()
  
  real(dp) :: ratio_les_filter_grid_widths = 2.0_dp, &
              smagorinsky_constant = 0.10_dp, &           ! C_s = 0.17  in [1] (after Eq. 3.13)
                                                          ! C_s = 0.18  in [2] (Eq. 5.45) and [3] (Eq. 4.5)
              wale_constant = sqrt(10.6_dp) * 0.10_dp, &  ! C_W = sqrt(10.6) * C_s  in [3], page 88.
                                                          ! C_W = 0.5     was the OLD default in [4, Sect. 2.5.1.2], 
                                                          !               where they calibrated it using freely decaying isotropic
                                                          !               homogeneous turbulence.
                                                          ! C_W = 0.325   is now the ANSYS default
              QR_constant = 1 / pi
  character(100) :: damping_function = 'Driest'
  real(dp) :: wall_damping_factor = 25.0_dp

  real(dp) :: von_karman_constant = 0.4187    ! Negative value indicates there is no bound by simple mixing length model.
                                              ! 0.4187 in ANSYS [6]

contains
  subroutine init_const_les_model_from_file(unit)
    integer, intent(in) :: unit

    character(100) :: model
    integer :: iostat
    character(200) :: iomsg
    integer :: nyplus_specs
    namelist /const_eddy_visc_params/  &
        model, ratio_les_filter_grid_widths, &
        smagorinsky_constant, wale_constant, QR_constant, &
        damping_function, wall_damping_factor, von_karman_constant

    model = UNINITIALIZED_CHAR
    read(unit,const_eddy_visc_params,iostat=iostat,iomsg=iomsg)
    call assert(iostat==0,iomsg)

    select case(model)
      case('Smagorinsky')
        call assert(can_calculate_smag(), "Not enough info for the Smagorinsky model. (Maybe `les_xyz2yplus` is not given?)")
        const_sfs_nu => const_smag_sfs_nu
      case('WALE')
        const_sfs_nu => const_wale_sfs_nu
      case("QR")
        const_sfs_nu => const_QR_sfs_nu
      case default
        call error('unknown eddy viscosity model=' // model)
    end select
    initialized_const_eddy_visc = .true.
  end subroutine

  subroutine const_QR_sfs_nu(elem, pnt_set, time, values)
    ! nu_sfs = (C_QR * Delta)**2 * abs(tr(S**3)) / tr(S**2)
    ! 
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)

    real(dp), dimension(size(values)) :: frobnorm_ROS, trace_ROS3
    real(dp) :: square_C_x_Delta

    call assert(size(values) == pnt_set%np(), BOUNDS_CHECK)

    square_C_x_Delta = (QR_constant * undamped_filterwidth(elem))**2

    call grad_u_invariants(elem, pnt_set, time, frobnorm_ROS=frobnorm_ROS, trace_ROS3=trace_ROS3)
    values = square_C_x_Delta * abs(trace_ROS3) / frobnorm_ROS**2
  end subroutine

  subroutine const_wale_sfs_nu(elem,pnt_set,time,values)
    use math_const, only: SMALL

    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)

    real(dp), dimension(size(values)) :: S, S_d, denom
    real(dp) :: square_C_x_Delta

    call assert(size(values) == pnt_set%np(), BOUNDS_CHECK)

    square_C_x_Delta = (wale_constant * undamped_filterwidth(elem))**2
    
    call grad_u_invariants(elem, pnt_set, time, frobnorm_ROS=S, frobnorm_Sd=S_d)
    denom = S**5 + sqrt(S_d)**5
    call assert(denom >= SMALL, 'WALE MODEL: denominator is small')
    values = square_C_x_Delta * S_d**3 / denom
  end subroutine
  
  pure logical function can_calculate_smag()
    if (les_xyz2yplus == UNINITIALIZED_CHAR) then
      can_calculate_smag = .false.
    elseif (von_karman_constant > 0.0_dp .and. .not. allocated(wall_dist_func)) then
      can_calculate_smag = .false.
    else
      can_calculate_smag = .true.
    endif
  end function

  subroutine const_smag_sfs_nu(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)

    real(dp), dimension(size(values)) :: square_cs_x_delta, abs_stress

    call assert(size(values) == pnt_set%np(), BOUNDS_CHECK)
    
    call square_C_x_Delta__smag(elem, pnt_set, time, square_cs_x_delta)

    call grad_u_invariants(elem, pnt_set, time, frobnorm_ROS=abs_stress)
    call assert_normal(abs_stress, 'abs_stress')
    abs_stress = sqrt(2.0_dp) * abs_stress
    call assert_normal(square_cs_x_delta, 'square_cs_x_delta')
    values = square_cs_x_delta * abs_stress
  end subroutine

  subroutine square_C_x_Delta__smag(elem, pnt_set, time, values)
    ! (C_Smag * Delta)**2
    ! 
    ! This can be compared directly to the coefficient of a dynamic model.
    ! 
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)

    real(dp) :: yplus_vals(size(values)), y(size(values))

    call assert(size(values) == pnt_set%np(), BOUNDS_CHECK)
    call assert(can_calculate_smag(), "Cannot compute Smag. visc. ... Some info is missing.", BOUNDS_CHECK)

    call yplus(elem, pnt_set, time, yplus_vals)
    values = smagorinsky_constant * undamped_filterwidth(elem) * yplus2wall_damping(yplus_vals)
    if (von_karman_constant > 0.0_dp) then
      call wall_dist_func%get_values(elem, pnt_set, time, y)
      values = min(values, von_karman_constant * y)
    endif
    values = values**2
  end subroutine

  subroutine yplus(elem, pnt_set, time, values)
    ! This separate function is possibly slower and more complex than just storing
    ! `xyz2yplus_funcidx` in the module, but it simplifies the initialization, and `yplus` may be
    ! needed to output Smagorinsky values even when no LES model is used.
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    integer, save :: xyz2yplus_funcidx = UNINITIALIZED_INT
    character, parameter :: xyz(3) = ['x', 'y', 'z']
    integer :: p, np

    np = assert_eq(size(values), pnt_set%np(), BOUNDS_CHECK)
    call assert(les_xyz2yplus /= UNINITIALIZED_CHAR, "yplus (for LES): `les_xyz2yplus` not given", BOUNDS_CHECK)

    if (xyz2yplus_funcidx == UNINITIALIZED_INT) call add_eqparser(les_xyz2yplus, xyz(:mesh_dimen), xyz2yplus_funcidx)

    do p = 1, np
      values(p) = evaluate(xyz2yplus_funcidx, pnt_set%gcors(:,p))
    enddo
  end subroutine

  real(dp) function undamped_filterwidth(elem)
    ! LES filter width without wall damping
    type(elem_list), intent(in) :: elem

    real(dp) :: eff_len, anisot_eff_len(mesh_dimen)

    anisot_eff_len = anisotropic_charact_len(elem,'mass_flux',len_specs='vol',order_specs='linear')
    eff_len = assert_eq(anisot_eff_len,'filterwidth: expected isotropic measure for elem length',BOUNDS_CHECK)
    undamped_filterwidth = ratio_les_filter_grid_widths * eff_len
  end function
  
  elemental real(dp) function yplus2wall_damping(yplus) result(wall_damping)
    real(dp), intent(in) :: yplus

    real(dp) :: yp_over_A
    
    yp_over_A = yplus / wall_damping_factor
    select case(damping_function)
      case('Driest');     wall_damping = 1 - exp(-yp_over_A)
      case('Piomelli');   wall_damping = sqrt(1 - exp(-yp_over_A**3)) ! See [4,Sect. 2.5.1.1.1.]
      case default;       error stop 'wall_damping: unknown damping_function'
    end select
  end function
end module les_const_eddy_visc_mod
