module divergence_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only: vec_func_type
  implicit none

  private
  public :: divergence

  type, extends(scal_func_type) :: div_t
    class(vec_func_type), allocatable :: vecfield
  contains
    procedure :: get_values
  end type div_t

contains
  type(div_t) function divergence(vecfield)
    use run_data, only: mesh_dimen

    class(vec_func_type), intent(in) :: vecfield

    call assert(mesh_dimen == vecfield%no_dirs())
    allocate(divergence%vecfield,source=vecfield)
  end function divergence

  subroutine get_values(this,elem,pnt_set,time,values)
    use f90_kind, only: dp
    use mesh_objects, only: elem_list
    use pnt_set_mod, only: pnt_set_t
    use support, only: trace

    class(div_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)

    real(dp) :: gders_vecfield(elem%dimen(),this%vecfield%no_dirs(),size(values))
    integer :: p, no_points

    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    call this%vecfield%get_gders(elem,pnt_set,time,gders_vecfield)
    do p = 1, no_points
      values(p) = trace(gders_vecfield(:,:,p))
    enddo
  end subroutine get_values
end module divergence_mod
