module time_der_mod
  use f90_kind, only: dp
  use pde_term_mod, only: pde_term_t
  implicit none
  
  private
  public :: time_deriv_t
  public :: backward_fd, impl_time_deriv, expl_time_deriv
  
  type, extends(pde_term_t) :: time_deriv_t
    ! weights = [implicit wght, 1st explicit wght, 2nd explicit wgt, ...]
    real(dp), allocatable :: weights(:)
    logical :: include_implicit_term  = .true.
    logical :: include_explicit_terms = .true.
  contains
    procedure, non_overridable :: process_elem
    procedure, non_overridable :: ngbr_coupling_depth
  end type

contains
  type(time_deriv_t) function expl_time_deriv(weights)
    real(dp), intent(in) :: weights(:)

    expl_time_deriv%weights = weights
    expl_time_deriv%include_implicit_term = .false.
  end function

  pure integer function ngbr_coupling_depth(this)
    class(time_deriv_t), intent(in) :: this

    ngbr_coupling_depth = merge(0, -1, this%include_implicit_term)
  end function

  type(time_deriv_t) function impl_time_deriv(weight)
    real(dp), intent(in) :: weight

    impl_time_deriv%weights = [weight]
    impl_time_deriv%include_explicit_terms = .false.
  end function

  type(time_deriv_t) function backward_fd(dt,order)
    use phys_time_mod, only: BDF_weights

    real(dp), intent(in) :: dt
    integer, intent(in) :: order

    backward_fd%weights = BDF_weights(order,dt)
  end function backward_fd

  subroutine process_elem(this,elem,qnty,local_linsys)
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: elem_list
    use run_data, only: assume_affine_elems
    use solvec_mod, only: solvec_t

    class(time_deriv_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys

    integer :: elem_no
    integer :: dir, nnod, order, nw
    real(dp) :: massmat_factor
    real(dp), pointer :: coeffs(:,:)

    elem_no = elem%get_active_elem_no(qnty%handler%only_fluid_mesh)
    
    ! Implicit part:
    if (this%include_implicit_term) then
      order = qnty%handler%order(elem_no)
      call local_linsys%add_transposed_elemmat_all_dirs(elem_no, this%weights(1)*elem%massmat(order))
    endif
    
    ! Explicit part:
    if (this%include_explicit_terms) then
      nnod = qnty%handler%no_nod_elem(elem_no)
      nw = size(this%weights)
      do dir = 1, qnty%handler%no_dirs
        coeffs => qnty%ptr_to_coeffs(elem_no,dir)
        if (assume_affine_elems) then
          massmat_factor = elem%global_local_volume_ratio()
          local_linsys%rhs%vec(:,dir) = local_linsys%rhs%vec(:,dir) - massmat_factor * matmul(coeffs(:,2:nw),this%weights(2:nw))
        else
          call subtract_L_LT_A_vec(local_linsys%rhs%vec(:,dir),elem%chol_massmat(:nnod,:nnod),coeffs(:,2:nw),this%weights(2:nw))
        endif
      enddo
    endif
  end subroutine process_elem

  subroutine subtract_L_LT_A_vec(y,L,A,v)
    ! Assumuming L is square and lower triangular, performs
    !     y   -=   L * L^T * A * v
    !     y_k -=   L_ki  L_ji  A_jm  v_m
    ! The strictly upper part of L is not referenced.
    real(dp), intent(inout) :: y(:)
    real(dp), intent(in) :: L(:,:), A(:,:), v(:)

    call axpy_L_LT_A_vec(y,L,A,v,-1.0_dp)
  end subroutine subtract_L_LT_A_vec

  subroutine axpy_L_LT_A_vec(y,L,A,v,alpha)
    ! Assumuming L is square and lower triangular, performs
    !     y   += alpha * L * L^T * A * v
    !     y_k += alpha  L_ki  L_ji  A_jm  v_m
    ! The strictly upper part of L is not referenced.
    use assertions, only: assert_eq, BOUNDS_CHECK

    real(dp), intent(inout) :: y(:)
    real(dp), intent(in) :: L(:,:), A(:,:), v(:)
    real(dp), intent(in) :: alpha

    integer :: i, j, k, m, size_L, size_v

    size_L = assert_eq(size(L,1), size(L,2), size(A,1), size(y), BOUNDS_CHECK)
    size_v = assert_eq(size(A,2), size(v), BOUNDS_CHECK)

    do m = 1, size_v
      do i = 1, size_L
        do j = i, size_L
          do k = i, size_L
            y(k) = y(k) + alpha * L(k,i) * L(j,i) * A(j,m) * v(m)
          enddo
        enddo
      enddo
    enddo
  end subroutine axpy_L_LT_A_vec
end module time_der_mod
