module vec_func_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use exceptions, only: error
  use f90_kind, only: dp
  use mesh_objects, only: elem_list, face_list, side_of_internal_nghb_of_bnd_face
  use pnt_set_mod, only: pnt_set_t
  use scal_func_mod, only: scal_func_type
  implicit none
  
  private
  public :: vec_func_type
  
  type, abstract :: vec_func_type
    private
  contains
    procedure :: get_values, get_gders
    procedure(no_dirs_i), deferred :: no_dirs

    generic :: values_at_qps => values_at_qps__elem, values_at_qps__face
    procedure, non_overridable, private :: values_at_qps__elem
    procedure :: values_at_qps__face ! overwritable for fields that are defined only at the boundary
    procedure, non_overridable :: values_at_qps_E1_E2
    generic :: gders_at_qps => gders_at_qps__face
    procedure, non_overridable, private :: gders_at_qps__face
    
    procedure, non_overridable :: avg_at_qps, jump_at_qps
    procedure, non_overridable :: div_at_qps
    generic :: eval_avg_value => eval_avg_value_on_elem
    procedure, non_overridable, private :: eval_avg_value_on_elem
    
    procedure :: select_dir
  end type vec_func_type
  
  abstract interface
    pure integer function no_dirs_i(this)
      import :: vec_func_type
      
      class(vec_func_type), intent(in) :: this
    end function no_dirs_i
  end interface
  
contains
  subroutine get_values(this,elem,pnt_set,time,values)
    class(vec_func_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:,:)

    call assert(size(values,1) == this%no_dirs(),"get_values: bad size(values,1)",BOUNDS_CHECK)
    call assert(size(values,2) == pnt_set%np(),  "get_values: bad size(values,2)",BOUNDS_CHECK)

    call error('NOT IMPLEMENTED: get_values')
  end subroutine get_values

  subroutine get_gders(this,elem,pnt_set,time,gders)
    use run_data, only: mesh_dimen

    class(vec_func_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:,:)

    call assert(size(gders,1) == mesh_dimen,    "get_gders: bad size(gders,1)",BOUNDS_CHECK)
    call assert(size(gders,2) == this%no_dirs(),"get_gders: bad size(gders,2)",BOUNDS_CHECK)
    call assert(size(gders,3) == pnt_set%np(),  "get_gders: bad size(gders,3)",BOUNDS_CHECK)

    call error('NOT IMPLEMENTED: get_gders')
  end subroutine get_gders


  subroutine values_at_qps__elem(this,elem,time,values)
    class(vec_func_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:,:)

    call this%get_values(elem,elem%quad_points,time,values)
  end subroutine values_at_qps__elem
  
  subroutine values_at_qps__face(this,face,side,time,values)
    class(vec_func_type), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:,:)

    call assert(side == 1 .or. side == 2,BOUNDS_CHECK)

    associate(ngbr => face%elem_neighbors(side)%point)
      call this%get_values(ngbr,face%quad_points(side),time,values)
    end associate
  end subroutine values_at_qps__face
  
  subroutine gders_at_qps__face(this,face,side,time,gders)
    class(vec_func_type), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:,:)
    
    call assert(side == 1 .or. side == 2,BOUNDS_CHECK)

    associate(ngbr => face%elem_neighbors(side)%point)
      call this%get_gders(ngbr,face%quad_points(side),time,gders)
    end associate
  end subroutine gders_at_qps__face
  
  subroutine values_at_qps_E1_E2(this,face,only_fluid,time,values_E1,values_E2)
    class(vec_func_type), intent(in) :: this
    type(face_list), intent(in) :: face
    logical, intent(in) :: only_fluid
    integer, intent(in) :: time
    real(dp), intent(out) :: values_E1(:,:), values_E2(:,:)
    
    call assert(.not. face%is_boundary(only_fluid),BOUNDS_CHECK)
    
    associate(E1 => face%elem_neighbors(1)%point, &
              E2 => face%elem_neighbors(2)%point)
      call this%get_values(E1,face%quad_points(1),time,values_E1)
      call this%get_values(E2,face%quad_points(2),time,values_E2)
    end associate
  end subroutine values_at_qps_E1_E2
  
  
  subroutine avg_at_qps(this,face,only_fluid,time,avg)
    ! Returns the average of a function at face%qps.
    ! The average operator is defined as:
    !         / 0.5 * (f|_E1 + f|_E2), on internal faces,
    ! {f} := {
    !         \ f|_E1              , on boundary faces.
    class(vec_func_type), intent(in) :: this
    type(face_list), intent(in) :: face
    logical, intent(in) :: only_fluid
    integer, intent(in) :: time
    real(dp), intent(out) :: avg(:,:)
    
    real(dp) :: values_E1(this%no_dirs(),face%nqp), values_E2(this%no_dirs(),face%nqp)
    integer :: side
    
    call assert(size(avg,1)==this%no_dirs() .and. size(avg,2)==face%nqp,BOUNDS_CHECK)
    
    if (.not. face%is_boundary(only_fluid)) then
      call this%values_at_qps_E1_E2(face,only_fluid,time,values_E1,values_E2)
      avg = 0.5_dp * (values_E1 + values_E2)
    else
      side = side_of_internal_nghb_of_bnd_face(face,only_fluid)
      call this%values_at_qps(face,side,time,avg)
    endif
  end subroutine avg_at_qps
  
  subroutine jump_at_qps(this,face,only_fluid,time,jump)
    ! Returns the jump of a function at face%qps. 
    !The jump operator is defined as:
    !         / f|_E1 - f|_E2, on internal faces
    ! [f] := {
    !         \ f|_E1      , on boundary faces
    class(vec_func_type), intent(in) :: this
    type(face_list), intent(in) :: face
    logical, intent(in) :: only_fluid
    integer, intent(in) :: time
    real(dp), intent(out) :: jump(:,:)
    
    real(dp) :: values_E1(this%no_dirs(),face%nqp), values_E2(this%no_dirs(),face%nqp)
    integer :: side
    
    call assert(size(jump,1)==this%no_dirs() .and. size(jump,2)==face%nqp,BOUNDS_CHECK)
    
    if (.not. face%is_boundary(only_fluid)) then
      call this%values_at_qps_E1_E2(face,only_fluid,time,values_E1,values_E2)
      jump = values_E1 - values_E2
    else
      side = side_of_internal_nghb_of_bnd_face(face,only_fluid)
      call this%values_at_qps(face,side,time,jump)
    endif
  end subroutine jump_at_qps
  
  
  subroutine eval_avg_value_on_elem(this,elem,time,avg_value)
    class(vec_func_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: time
    real(dp), intent(out) :: avg_value(:)
    
    real(dp) :: values_qps(this%no_dirs(),elem%nqp)
    
    call this%values_at_qps(elem,time,values_qps)
    avg_value = matmul(values_qps,elem%quot) / elem%volume
  end subroutine eval_avg_value_on_elem
  
  
  function div_at_qps(this,elem,time) result(div)
    use support, only: trace
    
    class(vec_func_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: time
    real(dp) :: div(elem%nqp)
    
    real(dp) :: gders(elem%dimen(),this%no_dirs(),elem%nqp)
    integer :: p
    
    call this%get_gders(elem,elem%quad_points,time,gders)
    do p = 1, elem%nqp
      div(p) = trace(gders(:,:,p))
    enddo
  end function div_at_qps

  subroutine select_dir(this,dir,scalfunc)
    ! This cannot normally be implemented with acceptable performance, except 
    ! in very special cases.
    ! This interface is here because it's convenient to call the method on an abstract type.
    class(vec_func_type), intent(in) :: this
    integer, intent(in) :: dir
    class(scal_func_type), allocatable, intent(out) :: scalfunc

    call error('NOT IMPLEMENTED: select_dir')
  end subroutine select_dir
end module vec_func_mod
