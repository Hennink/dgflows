module mesh_partitioning
  use assertions, only: assert, assert_eq
  use exceptions, only: error
  use mesh_objects, only: elem_list, elem_id
  use mesh_type, only: fem_mesh
  use petsc_mod, only: i_am_master_process, petsc_proc_id
  use mpi_wrappers_mod, only: bcast_wrap
  implicit none
  
  private
  public :: elem_and_nghb_in_same_partition
  public :: elem_is_in_partition
  public :: my_first_last_elems
  public :: init_partition_and_renumber_mesh
  public :: partition_of_elem
  public :: io_elem_no_permutations
  public :: determine_old_new_permutations
  public :: old_new_permutation_all, old_new_permutation_fluid
  
  integer :: no_partitions
  integer, allocatable :: first_and_last_part_elems(:,:)
  integer, allocatable :: first_and_last_part_fluid_elems(:,:)
  integer, allocatable :: elem_no_permutation_all(:), old_elem_no_permutation_all(:)
  integer, allocatable :: elem_no_permutation_fluid(:), old_elem_no_permutation_fluid(:)
  integer, allocatable, protected :: old_new_permutation_all(:), old_new_permutation_fluid(:)
  
contains
  subroutine init_partition_and_renumber_mesh(mesh,no_parts)
    type(fem_mesh), intent(inout) :: mesh
    integer, intent(in) :: no_parts
    
    integer :: part_numbers(mesh%no_active_elem)
    
    no_partitions = no_parts
    
    if (i_am_master_process()) call partitioning_of_mesh(mesh,part_numbers)
    call bcast_wrap(part_numbers,0)
    
    call permutation_resulting_in_contiguous_parts(mesh,part_numbers)
    call process_permutation(mesh,part_numbers)
    call find_first_and_last_elems_for_all_partitions(mesh,.false.,part_numbers,first_and_last_part_elems) !global mesh
    call find_first_and_last_elems_for_all_partitions(mesh,.true.,part_numbers,first_and_last_part_fluid_elems) !only fluid mesh
  end subroutine init_partition_and_renumber_mesh
  
  subroutine partitioning_of_mesh(mesh,part_numbers)
    ! TODO: Assert that we have at least one fluid element in each partition,
    !       because we get an error otherwise.
    use csr_format, only: CSR_mesh_format_type, zero_based_indexing
    use metis_interfaces, only: METIS_IDX_T_KIND, part_mesh_dual, set_default_options, &
                                set_option, METIS_DBG_INFO, METIS_OBJTYPE_VOL
    use run_data, only: part_weight_fluid_elem, part_weight_solid_elem, &
                        comm_size_fluid_elem, comm_size_solid_elem
    
    type(fem_mesh), intent(in) :: mesh
    integer, intent(out) :: part_numbers(:)
    
    type(CSR_mesh_format_type) :: CSR_fmt
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    integer, parameter :: max_no_vtx_per_elem = 100
    integer, allocatable :: CSR_elem_list(:)
    integer, allocatable :: CSR_node_list(:)
    integer :: min_common_vtx_ngbrs
    integer :: elem_no
    integer :: elem_weights(mesh%no_active_elem), elem_sizes(mesh%no_active_elem)
    integer(METIS_IDX_T_KIND), target, allocatable :: metis_options(:)
    
    call assert(i_am_master_process(),'only the master calls Metis')

    call CSR_fmt%init(mesh%no_active_elem,max_no_vtx_per_elem)
    min_common_vtx_ngbrs = huge(1)
    do elem_no = 1, mesh%no_active_elem
      id = mesh%active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      call CSR_fmt%add_nodes_of_next_elem(elem%global_idx_vertices())
      min_common_vtx_ngbrs = min(min_common_vtx_ngbrs,elem%no_common_vtx_ngbrs())
      if (elem%is_fluid()) then
        elem_weights(elem_no) = part_weight_fluid_elem
        elem_sizes(elem_no) = comm_size_fluid_elem
      else
        elem_weights(elem_no) = part_weight_solid_elem
        elem_sizes(elem_no) = comm_size_solid_elem
      endif
    enddo
    call CSR_fmt%get_CSR_mesh_format(zero_based_indexing,CSR_elem_list,CSR_node_list)
    
    call set_default_options(metis_options)
    call set_option(metis_options,'debug',METIS_DBG_INFO)
    call set_option(metis_options,'objective',METIS_OBJTYPE_VOL)
    call part_mesh_dual(mesh%no_vertices,CSR_elem_list,CSR_node_list, &
                        min_common_vtx_ngbrs,no_partitions,part_numbers, &
                        elem_weights=elem_weights,elem_sizes=elem_sizes, &
                        metis_options=metis_options)
  end subroutine partitioning_of_mesh
  
  subroutine permutation_resulting_in_contiguous_parts(mesh,part_numbers)
    type(fem_mesh), intent(in) :: mesh
    integer, intent(in) :: part_numbers(:)
    
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    integer :: old_elem_no, new_elem_no, new_fluid_elem_no, part
    
    allocate(elem_no_permutation_all(mesh%no_active_elem))
    allocate(elem_no_permutation_fluid(mesh%no_active_fluid_elem))
    new_elem_no = 0
    new_fluid_elem_no = 0
    do part = 0, no_partitions-1
      do old_elem_no = 1, mesh%no_active_elem
        if (part == part_numbers(old_elem_no)) then
          new_elem_no = new_elem_no + 1
          id = mesh%active_elem_list(old_elem_no)
          elem => mesh%get_elem_ptr(id)
          elem_no_permutation_all(new_elem_no) = elem%active_elem_no
          if (elem%is_fluid()) then
            new_fluid_elem_no = new_fluid_elem_no + 1
            elem_no_permutation_fluid(new_fluid_elem_no) = elem%active_fluid_elem_no
          endif
        endif
      enddo
    enddo
    
    call assert(new_elem_no == mesh%no_active_elem,'We did not loop over all the mesh elements.')
    call assert(new_fluid_elem_no == mesh%no_active_fluid_elem,'We did not loop over all the mesh fluid elements.')
  end subroutine permutation_resulting_in_contiguous_parts
  
  subroutine process_permutation(mesh,part_numbers)
    type(fem_mesh), intent(inout) :: mesh
    integer, intent(inout) :: part_numbers(:)
    
    call assert(size(part_numbers) == mesh%no_active_elem)
    
    call renumber_non_refined_mesh(mesh)
    part_numbers = part_numbers(elem_no_permutation_all)
  end subroutine process_permutation
  
  subroutine renumber_non_refined_mesh(mesh)
    type(fem_mesh), intent(inout) :: mesh
    
    integer :: new_elem_no, old_elem_no
    type(elem_id) :: aux_active_elem_list(mesh%no_active_elem)
    type(elem_id) :: aux_active_fluid_elem_list(mesh%no_active_fluid_elem)
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    
    ! Copy the active element lists, for later renumbering:
    aux_active_elem_list = mesh%active_elem_list
    aux_active_fluid_elem_list = mesh%active_fluid_elem_list
    
    ! Reshuffle the active element lists according to the renumbering determined by METIS.
    ! The elements numbers need to be updated accordingly.
    do new_elem_no = 1, mesh%no_active_elem
      old_elem_no = elem_no_permutation_all(new_elem_no)
      mesh%active_elem_list(new_elem_no) = aux_active_elem_list(old_elem_no)
      id = mesh%active_elem_list(new_elem_no)
      elem => mesh%get_elem_ptr(id)
      elem%active_elem_no = new_elem_no
    enddo
    do new_elem_no = 1, mesh%no_active_fluid_elem
      old_elem_no = elem_no_permutation_fluid(new_elem_no)
      mesh%active_fluid_elem_list(new_elem_no) = aux_active_fluid_elem_list(old_elem_no)
      id = mesh%active_fluid_elem_list(new_elem_no)
      elem => mesh%get_elem_ptr(id)
      elem%active_fluid_elem_no = new_elem_no
    enddo
  end subroutine renumber_non_refined_mesh
  
  subroutine find_first_and_last_elems_for_all_partitions(mesh,only_fluid_mesh,part_numbers,first_and_last_part_elems)
    type(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid_mesh
    integer, intent(in) :: part_numbers(:)
    integer, allocatable, intent(out) :: first_and_last_part_elems(:,:)
    
    integer :: elem_no
    integer :: no_active_elem
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_id) :: id
    type(elem_list), pointer :: elem, next_elem
    integer :: current_part
    logical :: first_elem_found
    
    no_active_elem = mesh%get_no_active_elem(only_fluid_mesh)
    call mesh%get_active_elem_list_ptr(only_fluid_mesh,active_elem_list)
    
    allocate(first_and_last_part_elems(2,0:no_partitions-1))
    
    current_part = 0
    first_elem_found = .false.
    do elem_no = 1, no_active_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      
      if (part_numbers(elem%active_elem_no)==current_part .and. .not. first_elem_found) then
        first_and_last_part_elems(1,current_part) = elem_no ! first elem of current partition
        first_elem_found = .true.
      endif
      
      if (elem_no == no_active_elem) then
        call assert(first_elem_found,'No element found for at least one partition.')
        first_and_last_part_elems(2,current_part) = elem_no ! last elem of current partition
        exit
      else
        id = active_elem_list(elem_no+1)
        next_elem => mesh%get_elem_ptr(id)
        if (part_numbers(next_elem%active_elem_no) == current_part) then
          ! Do nothing
        elseif (part_numbers(next_elem%active_elem_no) == current_part + 1) then
          call assert(first_elem_found,'No element found for at least one partition.')
          first_and_last_part_elems(2,current_part) = elem_no ! last elem of current partition
          ! Move to next partition
          current_part = current_part + 1
          first_elem_found = .false.
        else
          call error('Partition numbers are not contiguous.')
        endif
      endif
    enddo
    call assert(current_part == no_partitions-1,'Not all partitions were found.')
  end subroutine find_first_and_last_elems_for_all_partitions
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  logical function elem_is_in_partition(elem,partition)
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: partition
    
    elem_is_in_partition = partition == partition_of_elem(elem)
  end function elem_is_in_partition
  
  logical function elem_and_nghb_in_same_partition(elem,nghb) result(same_partition)
    type(elem_list), intent(in) :: elem, nghb
    
    same_partition = partition_of_elem(elem) == partition_of_elem(nghb)
  end function elem_and_nghb_in_same_partition
  
  integer function partition_of_elem(elem) result(partition)
    type(elem_list), intent(in) :: elem
    
    do partition = 0, no_partitions-1
      if (elem%active_elem_no <= first_and_last_part_elems(2,partition)) return
    enddo
    call error('partition_of_elem: did not find partition')
  end function partition_of_elem
  
  subroutine my_first_last_elems(only_fluid,first_elem,last_elem)
    logical, intent(in) :: only_fluid
    integer, intent(out) :: first_elem, last_elem

    call first_and_last_elem_in_partition(petsc_proc_id(),only_fluid,first_elem,last_elem)
  end subroutine my_first_last_elems

  subroutine first_and_last_elem_in_partition(partition,only_fluid_mesh,first_elem,last_elem)
    integer, intent(in) :: partition
    logical, intent(in) :: only_fluid_mesh
    integer, intent(out) :: first_elem, last_elem
    
    call assert(0 <= partition .and. partition < no_partitions,'It must be 0 <= partition < no_partitions')
    
    if (only_fluid_mesh) then
      first_elem = first_and_last_part_fluid_elems(1,partition)
      last_elem = first_and_last_part_fluid_elems(2,partition)
    else
      first_elem = first_and_last_part_elems(1,partition)
      last_elem = first_and_last_part_elems(2,partition)
    endif
  end subroutine first_and_last_elem_in_partition
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine io_elem_no_permutations(file_path,from_file)
    use io_basics, only: INT_FMT
    
    character(*), intent(in) :: file_path
    logical, intent(in) :: from_file
    
    integer :: unit_number
    integer :: ios
    character(:), allocatable :: status
    character(:), allocatable :: action
    character(200) :: msg
    
    if (from_file) then 
      allocate(status,source='old')
      allocate(action,source='read')
      allocate(old_elem_no_permutation_all(size(elem_no_permutation_all)))
      allocate(old_elem_no_permutation_fluid(size(elem_no_permutation_fluid)))
    else
      allocate(status,source='replace')
      allocate(action,source='write')
    endif
    
    open(newunit=unit_number,file=file_path,status=status,action=action,iostat=ios,iomsg=msg)
    call assert(ios == 0,msg)
      if (from_file) then
        read(unit_number,*,iostat=ios,iomsg=msg)
        read(unit_number,'('//INT_FMT//')',iostat=ios,iomsg=msg) old_elem_no_permutation_all
        read(unit_number,*,iostat=ios,iomsg=msg)
        read(unit_number,'('//INT_FMT//')',iostat=ios,iomsg=msg) old_elem_no_permutation_fluid
      else
        write(unit_number,'(a)',iostat=ios,iomsg=msg) '******* FLUID+SOLID MESH *******'
        write(unit_number,'('//INT_FMT//')',iostat=ios,iomsg=msg) elem_no_permutation_all
        write(unit_number,'(a)',iostat=ios,iomsg=msg) '******* FLUID MESH *******'
        write(unit_number,'('//INT_FMT//')',iostat=ios,iomsg=msg) elem_no_permutation_fluid
      endif
      call assert(ios == 0,msg)
    close(unit_number,status='keep',iostat=ios,iomsg=msg)
    call assert(ios == 0,msg)
  end subroutine io_elem_no_permutations
  
  subroutine determine_old_new_permutations()
    integer :: old_elem_no, new_elem_no, no_elems, no_fluid_elems
    
    call assert(allocated(elem_no_permutation_all) .and. allocated(old_elem_no_permutation_all) .and. &
                allocated(elem_no_permutation_fluid) .and. allocated(old_elem_no_permutation_fluid), &
                'Cannot determine the old->new elem no permutations without the new and the old permutations.')
    
    no_elems = size(elem_no_permutation_all)
    if(allocated(old_new_permutation_all)) deallocate(old_new_permutation_all)
    allocate(old_new_permutation_all(no_elems))
    
    no_fluid_elems = size(elem_no_permutation_fluid)
    if(allocated(old_new_permutation_fluid)) deallocate(old_new_permutation_fluid)
    allocate(old_new_permutation_fluid(no_fluid_elems))
    
    do old_elem_no = 1, no_elems
      new_elem_no = findloc(elem_no_permutation_all(:),old_elem_no_permutation_all(old_elem_no),dim=1)
      old_new_permutation_all(old_elem_no) = new_elem_no
    enddo
    do old_elem_no = 1, no_fluid_elems
      new_elem_no = findloc(elem_no_permutation_fluid(:),old_elem_no_permutation_fluid(old_elem_no),dim=1)
      old_new_permutation_fluid(old_elem_no) = new_elem_no
    enddo
    deallocate(old_elem_no_permutation_all,old_elem_no_permutation_fluid)
  end subroutine determine_old_new_permutations
end module mesh_partitioning

