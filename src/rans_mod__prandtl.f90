submodule(rans_mod) rans_mod__prandtl
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use mesh_objects, only: elem_list
  use numerical_solutions, only: matprop_set, velocity
  use pnt_set_mod, only: pnt_set_t
  use run_data, only: L_max_Prandtl
  use support, only: true
  use wall_dist_mod, only: wall_dist_func
  implicit none
  
  ! Von-Karman constant
  real(dp), parameter :: Karman = 0.41_dp
  
  ! Model constants
  real(dp), parameter :: C_mu = 0.09_dp
  
  type, extends(rans_model_t) :: prandtl_model_t
  contains
    procedure, nopass :: is_low_Re => true
  end type prandtl_model_t
  
  class(scal_func_type), allocatable :: nu_molec
  
contains
  module procedure get_prandtl_model
    call matprop_set%get_func('kinematic_viscosity',nu_molec)
    
    allocate(prandtl_model_t :: rans_model)
    rans_model%kinem_visc%proc => kinem_visc_turb
    rans_model%no_eq = 0
    
    call assert(L_max_Prandtl >= 0.0_dp,'You forgot to specify L_max_Prandtl in the input')
  end procedure get_prandtl_model
  
  
  subroutine kinem_visc_turb(elem,pnt_set,time,values)
    ! The turbulent kinematic viscosity is bounded from below by a small
    ! fraction of the molecular viscosity
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: L_m, nu
    real(dp) :: grad_U(elem%dimen(),velocity%no_dirs(),size(values)), S
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call velocity%get_gders(elem,pnt_set,time,grad_U)
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call eval_L_m(elem,pnt_set,time,L_m)
    do p = 1, no_points
      S = sqrt(0.5_dp*sum((grad_U(:,:,p) + transpose(grad_U(:,:,p)))**2))
      values(p) = max(S*L_m(p)**2,1.0e-4_dp*nu(p))
    enddo
  end subroutine kinem_visc_turb
  
  subroutine eval_L_m(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: y, L_max
    integer :: p, no_points
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call wall_dist_func%get_values(elem,pnt_set,time,y)
    call eval_L_max(elem,pnt_set,time,L_max)
    do p = 1, no_points
      values(p) = min(C_mu*L_max(p),Karman*y(p))
    enddo
  end subroutine eval_L_m
  
  subroutine eval_L_max(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    integer :: p, no_points
    real(dp) :: local_max_mix_length
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    do p = 1, no_points
      local_max_mix_length = 0.0_dp ! THIS IS SOMETHING TO BE DEFINED FOR EACH PROBLEM
      values(p) = max(L_max_Prandtl,local_max_mix_length)
    enddo
  end subroutine eval_L_max
end submodule rans_mod__prandtl
