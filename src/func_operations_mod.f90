module func_operations_mod
  use dyad_of_scalfuncs_mod, only: dyad_of_scalfuncs, dyad_of_scalfuncs_t
  use dyad_of_vec_scal_funcs_mod, only: dyad_of_vec_scal_funcs, dyad_of_vec_scal_funcs_t
  use dyad_of_vecfuncs_mod, only: dyad_of_vecfuncs, dyad_of_vecfuncs_t
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only: vec_func_type
  
  ! gfortran9 only compiles if the types of the return values have global module scope.
  ! Probably some compiler bug connected to the interfaces.
  use monad_of_scalfunc_mod, only: monad_of_scalfunc_t, monad_of_scalfunc
  use dyad_ratio_mod, only: division_t
  use dyad_product_mod, only: product_t
  use monad_dyad_lib_mod, only: add_dyad, subtract_dyad, power_monad_t, sqrt_monad_t, max_monad_t
implicit none

  private
  public :: operator(**)
  public :: operator(+), operator(-), operator(*), operator(/)
  public :: sqrt_of, max_of

  interface operator(**)
    module procedure power_of_scalfunc
  end interface operator(**)
  
  interface sqrt_of
    module procedure sqrt_of_scalfunc
  end interface

  interface max_of
    module procedure max_scalfunc_and_value
  end interface

  interface operator(+)
    module procedure add_scalfuncs
    module procedure add_vecfuncs
  end interface operator(+)

  interface operator(-)
    module procedure subtract_scalfuncs
    module procedure subtract_vecfuncs
  end interface operator(-)

  interface operator(*)
    module procedure multiply_scalfuncs
    module procedure multiply_scalfunc_vecfunc
    module procedure multiply_vecfuncs
  end interface operator(*)

  interface operator(/)
    module procedure divide_scalfuncs
    module procedure divide_vecfunc_by_scalfunc
  end interface operator(/)

contains
  type(monad_of_scalfunc_t) function power_of_scalfunc(func,expon) result(square)
    class(scal_func_type), intent(in) :: func
    integer, intent(in) :: expon

    type(power_monad_t) :: power_monad

    power_monad%expon = expon
    square = monad_of_scalfunc(power_monad,func)
  end function power_of_scalfunc
  
  type(monad_of_scalfunc_t) function sqrt_of_scalfunc(func) result(sqrt)
    class(scal_func_type), intent(in) :: func
    
    type(sqrt_monad_t) :: sqrt_monad
    
    sqrt = monad_of_scalfunc(sqrt_monad,func)
  end function sqrt_of_scalfunc

  type(monad_of_scalfunc_t) function max_scalfunc_and_value(func,minimum)
    use f90_kind, only: dp
    
    class(scal_func_type), intent(in) :: func
    real(dp), intent(in) :: minimum

    type(max_monad_t) :: max_monad

    max_monad%minimum = minimum
    max_scalfunc_and_value = monad_of_scalfunc(max_monad,func)
  end function max_scalfunc_and_value

  type(dyad_of_scalfuncs_t) function add_scalfuncs(f1,f2) result(sumation)
    class(scal_func_type), intent(in) :: f1, f2

    sumation = dyad_of_scalfuncs(add_dyad,f1,f2)
  end function add_scalfuncs

  type(dyad_of_vecfuncs_t) function add_vecfuncs(f1,f2) result(sumation)
    class(vec_func_type), intent(in) :: f1, f2

    sumation = dyad_of_vecfuncs(add_dyad,f1,f2)
  end function add_vecfuncs

  type(dyad_of_scalfuncs_t) function subtract_scalfuncs(f1,f2) result(diff)
    class(scal_func_type), intent(in) :: f1, f2

    diff = dyad_of_scalfuncs(subtract_dyad,f1,f2)
  end function subtract_scalfuncs

  type(dyad_of_vecfuncs_t) function subtract_vecfuncs(f1,f2) result(diff)
    class(vec_func_type), intent(in) :: f1, f2

    diff = dyad_of_vecfuncs(subtract_dyad,f1,f2)
  end function subtract_vecfuncs

  pure type(dyad_of_scalfuncs_t) function multiply_scalfuncs(f1,f2) result(prod)
    class(scal_func_type), intent(in) :: f1, f2

    type(product_t) :: prod_dyad

    prod = dyad_of_scalfuncs(prod_dyad,f1,f2)
  end function multiply_scalfuncs

  pure type(dyad_of_vec_scal_funcs_t) function multiply_scalfunc_vecfunc(scal,vec) result(prod)
    class(scal_func_type), intent(in) :: scal
    class(vec_func_type), intent(in) :: vec

    type(product_t) :: prod_dyad

    prod = dyad_of_vec_scal_funcs(prod_dyad,vec,scal)
  end function multiply_scalfunc_vecfunc
  
  type(dyad_of_vecfuncs_t) function multiply_vecfuncs(f1,f2) result(prod)
    class(vec_func_type), intent(in) :: f1, f2
    
    type(product_t) :: prod_dyad
    
    prod = dyad_of_vecfuncs(prod_dyad,f1,f2)
  end function multiply_vecfuncs
  
  pure type(dyad_of_scalfuncs_t) function divide_scalfuncs(num,denom) result(ratio)
    class(scal_func_type), intent(in) :: num, denom

    type(division_t) :: division

    ratio = dyad_of_scalfuncs(division,num,denom)
  end function divide_scalfuncs

  pure type(dyad_of_vec_scal_funcs_t) function divide_vecfunc_by_scalfunc(num,denom) result(ratio)
    class(vec_func_type), intent(in) :: num
    class(scal_func_type), intent(in) :: denom

    type(division_t) :: division

    ratio = dyad_of_vec_scal_funcs(division,num,denom)
  end function divide_vecfunc_by_scalfunc
end module func_operations_mod
