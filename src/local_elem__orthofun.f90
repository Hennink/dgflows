submodule(local_elem) local_elem__orthofun
  use assertions, only: assert
  use f90_kind, only: dp
  implicit none
  
contains
  module subroutine get_orthofun_elem_functionality(functionality)
    type(local_elem_functionality), intent(out) :: functionality
    
    functionality%coord_in_domain => my_coord_in_domain
    functionality%get_dimen => my_get_dimen
    functionality%get_quadrature => my_get_quadrature
    functionality%local_dg_deriv => my_local_dg_deriv
    functionality%local_dg_functions => my_local_dg_functions
    functionality%local_dg_functions_single_point => my_local_dg_functions_single_point
    functionality%mutually_hierarchical_func_sets => my_mutually_hierarchical_func_sets
    functionality%no_dg_functions => my_no_dg_functions
    functionality%no_vertices => my_no_vertices
  end subroutine get_orthofun_elem_functionality
  
  
  logical function my_coord_in_domain(eltype,coord)
    use orthofun, only: orthofun_coord_in_domain => coord_in_domain
    
    integer, intent(in) :: eltype
    real(dp), dimension(:), intent(in) :: coord
    
    integer :: ierr

    call orthofun_coord_in_domain(eltype,coord,my_coord_in_domain,ierr)
    call assert(ierr==0,"ierr /= 0 in Orthofun' coord_in_domain")
  end function my_coord_in_domain
  
  pure integer function my_get_dimen(eltype)
    use orthofun, only: orthofun_get_dimen => get_dimen
    
    integer, intent(in) :: eltype
    
    integer :: ierr
    
    call orthofun_get_dimen(eltype,my_get_dimen,ierr)
    if (ierr /= 0) error stop "ERROR: found ierr /= 0 in OrthoFun's get_dimen"
  end function my_get_dimen
  
  subroutine my_get_quadrature(eltype,order,abss,weights)
    use orthofun, only: quadset
    
    integer, intent(in) :: eltype
    integer, intent(in) :: order
    real(dp), allocatable, intent(out) :: abss(:,:), weights(:)
    
    integer :: ierr
    
    call quadset(eltype,order,abss,weights,ierr)
    call assert(ierr==0,"ierr /= 0 in Orthofun's quadset")
  end subroutine my_get_quadrature
    
  subroutine my_local_dg_deriv(eltype,order,local_coord,local_deriv)
    use orthofun, only: dgderivs, orthofun_get_dimen => get_dimen, get_ndg
    
    integer, intent(in) :: eltype
    integer, intent(in) :: order
    real(dp), intent(in) :: local_coord(:,:)
    real(dp), allocatable, intent(out) :: local_deriv(:,:,:)
    
    integer :: ierr
    integer :: p, np, dimen, ndg
    
    call orthofun_get_dimen(eltype,dimen,ierr)
    call assert(ierr==0,"found ierr /= 0 in OrthoFun's get_dimen")
    call get_ndg(eltype,order,ndg,ierr)
    call assert(ierr==0,"found ierr /= 0 in OrthoFun's get_ndg")
    np = size(local_coord,2)
    allocate(local_deriv(dimen,ndg,np))
    do p = 1, np
      call dgderivs(eltype,local_coord(:,p),order,local_deriv(:,:,p),ierr)
      call assert(ierr==0,"ierr /= 0 in Orthofun's dgderivs")
    enddo
  end subroutine my_local_dg_deriv
  
  subroutine my_local_dg_functions(eltype,order,local_coord,func,local_deriv,in_domain)
    ! If 'in_domain', then verify that all local_coord are in the domain.
    use orthofun, only: dgfuncs, get_ndg

    integer, intent(in) :: eltype
    integer, intent(in) :: order
    real(dp), intent(in) :: local_coord(:,:)
    real(dp), allocatable, optional, intent(out) :: func(:,:), local_deriv(:,:,:)
    logical, optional, intent(in) :: in_domain
    
    integer :: ierr
    integer :: p, np, ndg

    np = size(local_coord,2)
    if (present(func)) then
      call get_ndg(eltype,order,ndg,ierr)
      call assert(ierr == 0,"ierr /= 0 in Orthofun's get_ndg")
      allocate(func(ndg,np))
      do p = 1, np
        call dgfuncs(eltype,local_coord(:,p),order,func(:,p),ierr)
        call assert(ierr == 0,"ierr /= 0 in Orthofun's dgfuncs")
      enddo
    endif
    if (present(local_deriv)) call my_local_dg_deriv(eltype,order,local_coord,local_deriv)
    if (present(in_domain)) then
      do p = 1, np
        if (in_domain) call assert(my_coord_in_domain(eltype,local_coord(:,p)))
      enddo
    endif
  end subroutine my_local_dg_functions
  
  subroutine my_local_dg_functions_single_point(eltype,order,local_coord,func,local_deriv)
    use orthofun, only: dgderivs, dgfuncs
    
    integer, intent(in) :: eltype
    integer, intent(in) :: order
    real(dp), intent(in) :: local_coord(:)
    real(dp), intent(out) :: func(:), local_deriv(:,:)
    
    integer :: ierr

    call dgfuncs(eltype,local_coord,order,func,ierr)
    call assert(ierr==0,"ierr /= 0 in Orthofun's dgfuncs")
    call dgderivs(eltype,local_coord,order,local_deriv,ierr)
    call assert(ierr==0,"ierr /= 0 in Orthofun's dgderivs")
  end subroutine my_local_dg_functions_single_point
    
  pure logical function my_mutually_hierarchical_func_sets(eltype,order_1,order_2)
    integer, intent(in) :: eltype
    integer, intent(in) :: order_1, order_2
    
    my_mutually_hierarchical_func_sets = .true.
  end function my_mutually_hierarchical_func_sets
  
  pure integer function my_no_dg_functions(eltype,max_order) result(no_func)
    use orthofun, only: get_ndg
    
    integer, intent(in) :: eltype
    integer, intent(in) :: max_order
    
    integer :: ierr
    
    call get_ndg(eltype,max_order,no_func,ierr)
    if (ierr /= 0) error stop "ERROR: found ierr /= 0 in OrthoFun's ndg"
  end function my_no_dg_functions
  
  pure integer function my_no_vertices(eltype) result(no_vertices)
    use orthofun, only: get_nvert
    
    integer, intent(in) :: eltype
    
    integer :: ierr

    call get_nvert(eltype,no_vertices,ierr)
    if (ierr /= 0) error stop "ERROR: found ierr /= 0 in OrthoFun's get_nvert"
  end function my_no_vertices
end submodule local_elem__orthofun
