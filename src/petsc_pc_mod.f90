module petsc_pc_mod
#include "petsc/finclude/petscpc.h"
  use petscpc
  use petscmat, only: MAT_SHIFT_NONE, MAT_SHIFT_NONZERO, MAT_SHIFT_POSITIVE_DEFINITE, MAT_SHIFT_INBLOCKS

  use assertions, only: assert, assert_eq
  use exceptions, only: error
  implicit none
  
  private
  public :: petsc_pc_t
  public :: PETSC_MAT_SHIFT_NONE, PETSC_MAT_SHIFT_NONZERO, &
            PETSC_MAT_SHIFT_POSITIVE_DEFINITE, PETSC_MAT_SHIFT_INBLOCKS
  
  type :: petsc_pc_t
    ! Abstract PETSc object that manages all preconditioners, including direct
    ! solvers such as PCLU
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/PC/PC.html#PC
    type(tPC) :: petsc_pc
  contains
    procedure, non_overridable :: set_up
    procedure, non_overridable :: set_type, get_type
    procedure, non_overridable :: set_factor_levels
    procedure, non_overridable :: set_factor_shifttype
    procedure, non_overridable, private :: set_reuse_ordering
    procedure, non_overridable, private :: set_reuse_fill
    procedure, non_overridable :: set_from_options
  end type

  type :: mat_factor_shifttype_t
    ! Numeric Shift.
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatFactorShiftType.html#MatFactorShiftType
    private
    MatFactorShiftType :: type
  end type mat_factor_shifttype_t
  type(mat_factor_shifttype_t), parameter ::                                                    &
      PETSC_MAT_SHIFT_NONE              = mat_factor_shifttype_t(MAT_SHIFT_NONE),               &
      PETSC_MAT_SHIFT_NONZERO           = mat_factor_shifttype_t(MAT_SHIFT_NONZERO),            &
      PETSC_MAT_SHIFT_POSITIVE_DEFINITE = mat_factor_shifttype_t(MAT_SHIFT_POSITIVE_DEFINITE),  &
      PETSC_MAT_SHIFT_INBLOCKS          = mat_factor_shifttype_t(MAT_SHIFT_INBLOCKS)

contains
  subroutine set_up(this)
    class(petsc_pc_t), intent(inout) :: this

    PetscErrorCode:: ierr
    call PCSetUp(this%petsc_pc, ierr)
    call assert(ierr == 0,'ierr /= 0 in PCSetUp')
  end subroutine

  subroutine set_type(this, pctype)
    ! Builds PC for a particular preconditioner type.
    !
    ! Collective on PC
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/PC/PCSetType.html
    use petscksp, only: PCSetType, PCASMSetType, PC_ASM_BASIC
    
    class(petsc_pc_t), intent(inout) :: this
    character(*), intent(in) :: pctype
    
    PCType :: petsc_pc
    PetscErrorCode :: ierr
    
    petsc_pc = f2p_pctype(pctype)
    call PCSetType(this%petsc_pc, petsc_pc, ierr)
    call assert(ierr == 0,'ierr /= 0 in PCSetType')

    select case(petsc_pc)
      case(PCASM, PCGASM)
        ! Set the type of restriction and interpolation used for local
        ! problems in the additive Schwarz Method.
        ! 
        ! `PC_ASM_BASIC` was the default in PETSc version <= 3.12.5 .
        call PCASMSetType(this%petsc_pc, PC_ASM_BASIC, ierr)
        call assert(ierr == 0,'ierr /= 0 in PCASMSetType')
    end select
  end subroutine
  
  function get_type(this) result(pc)
    ! Gets the PC method type and name (as a string) from the PC context. 
    interface
      subroutine PCGetType(pc, type, ierr)
        use petscpc
        implicit none
        PC, intent(in) :: pc
        PCType, intent(out) :: type
        PetscErrorCode, intent(out) :: ierr
      end subroutine
    end interface
    class(petsc_pc_t), intent(in) :: this
    character(:), allocatable :: pc
    
    PCType :: petsc_pctype
    PetscErrorCode :: ierr
    
    call PCGetType(this%petsc_pc, petsc_pctype, ierr)
    call assert(ierr == 0,'ierr /= 0 in PCGetType')
    pc = p2f_pctype(petsc_pctype)
  end function

  subroutine set_factor_levels(this,levels)
    ! Sets the number of levels of fill to use.
    !
    ! Logically Collective on PC
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/PC/PCFactorSetLevels.html
    use petscksp, only: PCFactorSetLevels
    use petsc_basic_types_mod, only: fort2petsc
    
    class(petsc_pc_t), intent(inout) :: this
    integer, intent(in) :: levels
    
    PetscErrorCode :: ierr
    
    call PCFactorSetLevels(this%petsc_pc,fort2petsc(levels),ierr)
    call assert(ierr == 0,'ierr /= 0 in PCFactorSetLevels')
  end subroutine set_factor_levels
  
  
  subroutine set_factor_shifttype(this,shifttype)
    ! Adds a particular type of quantity to the diagonal of the matrix during
    ! numerical factorization, thus the matrix has nonzero pivots.
    !
    ! Logically Collective on PC
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/PC/PCFactorSetShiftType.html#PCFactorSetShiftType
    use petscksp, only: PCFactorSetShiftType

    class(petsc_pc_t), intent(inout) :: this
    type(mat_factor_shifttype_t), intent(in) :: shifttype
    
    PetscErrorCode :: ierr
    
    call PCFactorSetShiftType(this%petsc_pc,shifttype%type,ierr)
    call assert(ierr == 0,'ierr /= 0 in PCFactorSetShiftType')
  end subroutine set_factor_shifttype
  
  
  subroutine set_reuse_ordering(this,flag)
    ! When similar matrices are factored, this causes the ordering computed 
    ! in the first factor to be used for all following factors. 
    !
    ! Logically Collective on PC
    ! http://www.mcs.anl.gov/petsc/petsc-3.8/docs/manualpages/PC/PCFactorSetReuseOrdering.html#PCFactorSetReuseOrdering
    use petscksp, only: PCFactorSetReuseOrdering
    use petsc_basic_types_mod, only: fort2petsc
    
    class(petsc_pc_t), intent(inout) :: this
    logical, intent(in) :: flag
    
    PetscErrorCode :: ierr
    
    call PCFactorSetReuseOrdering(this%petsc_pc,fort2petsc(flag),ierr)
    call assert(ierr == 0,'ierr /= 0 in PCFactorSetReuseOrdering')
  end subroutine set_reuse_ordering
  
  
  subroutine set_reuse_fill(this,flag)
    ! When matrices with different nonzero structure are factored, 
    ! this causes later ones to use the fill ratio computed in the 
    ! initial factorization. 
    !
    ! Logically Collective on PC
    ! http://www.mcs.anl.gov/petsc/petsc-3.8/docs/manualpages/PC/PCFactorSetReuseFill.html#PCFactorSetReuseFill
    use petscksp, only: PCFactorSetReuseFill
    use petsc_basic_types_mod, only: fort2petsc
    
    class(petsc_pc_t), intent(inout) :: this
    logical, intent(in) :: flag
    
    PetscErrorCode :: ierr
    
    call PCFactorSetReuseFill(this%petsc_pc,fort2petsc(flag),ierr)
    call assert(ierr == 0,'ierr /= 0 in PCFactorSetReuseFill')
  end subroutine set_reuse_fill
  
  subroutine set_from_options(this)
    ! Sets PC options from the options database. This routine must be called
    ! before PCSetUp() if the user is to be allowed to set the preconditioner
    ! method.
    !
    ! Collective on PC
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/PC/PCSetFromOptions.html
    use petscksp, only: PCSetFromOptions

    class(petsc_pc_t), intent(inout) :: this
    
    PetscErrorCode :: ierr
    
    call PCSetFromOptions(this%petsc_pc,ierr)
    call assert(ierr == 0,'ierr /= 0 in PCSetFromOptions')
  end subroutine

  function f2p_pctype(fortran_pc) result(pc)
    character(*), intent(in) :: fortran_pc
    PCType :: pc
    
    select case(trim(fortran_pc))
      case('none');   pc = PCNONE
      case('jacobi');   pc = PCJACOBI
      case('sor');   pc = PCSOR
      case('lu');   pc = PCLU
      case('shell');   pc = PCSHELL
      case('bjacobi');   pc = PCBJACOBI
      case('mg');   pc = PCMG
      case('eisenstat');   pc = PCEISENSTAT
      case('ilu');   pc = PCILU
      case('icc');   pc = PCICC
      case('asm');   pc = PCASM
      case('gasm');   pc = PCGASM
      case('ksp');   pc = PCKSP
      case('composite');   pc = PCCOMPOSITE
      case('redundant');   pc = PCREDUNDANT
      case('spai');   pc = PCSPAI
      case('nn');   pc = PCNN
      case('cholesky');   pc = PCCHOLESKY
      case('pbjacobi');   pc = PCPBJACOBI
      case('vpbjacobi');   pc = PCVPBJACOBI
      case('mat');   pc = PCMAT
      case('hypre');   pc = PCHYPRE
      case('parms');   pc = PCPARMS
      case('fieldsplit');   pc = PCFIELDSPLIT
      case('tfs');   pc = PCTFS
      case('ml');   pc = PCML
      case('galerkin');   pc = PCGALERKIN
      case('exotic');   pc = PCEXOTIC
      case('cp');   pc = PCCP
      case('bfbt');   pc = PCBFBT
      case('lsc');   pc = PCLSC
      case('python');   pc = PCPYTHON
      case('pfmg');   pc = PCPFMG
      case('syspfmg');   pc = PCSYSPFMG
      case('redistribute');   pc = PCREDISTRIBUTE
      case('svd');   pc = PCSVD
      case('gamg');   pc = PCGAMG
      case('chowiluviennacl');   pc = PCCHOWILUVIENNACL
      case('rowscalingviennacl');   pc = PCROWSCALINGVIENNACL
      case('saviennacl');   pc = PCSAVIENNACL
      case('bddc');   pc = PCBDDC
      case('kaczmarz');   pc = PCKACZMARZ
      case('telescope');   pc = PCTELESCOPE
      case('patch');   pc = PCPATCH
      case('lmvm');   pc = PCLMVM
      case('hmg');   pc = PCHMG
      case('deflation');   pc = PCDEFLATION
      case('hpddm');   pc = PCHPDDM
      case('hara');   pc = PCHARA
      case default;         call error('Unknown pctype: '//trim(fortran_pc))
    end select

    call assert(p2f_pctype(pc) == fortran_pc, 'internal error with f2p or p2f PC strings')
  end function

  function p2f_pctype(petsc_pc) result(pc)
    PCType, intent(in) :: petsc_pc
    character(:), allocatable :: pc
  
    select case(petsc_pc)
      case(PCNONE);   pc = 'none'
      case(PCJACOBI);   pc = 'jacobi'
      case(PCSOR);   pc = 'sor'
      case(PCLU);   pc = 'lu'
      case(PCSHELL);   pc = 'shell'
      case(PCBJACOBI);   pc = 'bjacobi'
      case(PCMG);   pc = 'mg'
      case(PCEISENSTAT);   pc = 'eisenstat'
      case(PCILU);   pc = 'ilu'
      case(PCICC);   pc = 'icc'
      case(PCASM);   pc = 'asm'
      case(PCGASM);   pc = 'gasm'
      case(PCKSP);   pc = 'ksp'
      case(PCCOMPOSITE);   pc = 'composite'
      case(PCREDUNDANT);   pc = 'redundant'
      case(PCSPAI);   pc = 'spai'
      case(PCNN);   pc = 'nn'
      case(PCCHOLESKY);   pc = 'cholesky'
      case(PCPBJACOBI);   pc = 'pbjacobi'
      case(PCVPBJACOBI);   pc = 'vpbjacobi'
      case(PCMAT);   pc = 'mat'
      case(PCHYPRE);   pc = 'hypre'
      case(PCPARMS);   pc = 'parms'
      case(PCFIELDSPLIT);   pc = 'fieldsplit'
      case(PCTFS);   pc = 'tfs'
      case(PCML);   pc = 'ml'
      case(PCGALERKIN);   pc = 'galerkin'
      case(PCEXOTIC);   pc = 'exotic'
      case(PCCP);   pc = 'cp'
      case(PCBFBT);   pc = 'bfbt'
      case(PCLSC);   pc = 'lsc'
      case(PCPYTHON);   pc = 'python'
      case(PCPFMG);   pc = 'pfmg'
      case(PCSYSPFMG);   pc = 'syspfmg'
      case(PCREDISTRIBUTE);   pc = 'redistribute'
      case(PCSVD);   pc = 'svd'
      case(PCGAMG);   pc = 'gamg'
      case(PCCHOWILUVIENNACL);   pc = 'chowiluviennacl'
      case(PCROWSCALINGVIENNACL);   pc = 'rowscalingviennacl'
      case(PCSAVIENNACL);   pc = 'saviennacl'
      case(PCBDDC);   pc = 'bddc'
      case(PCKACZMARZ);   pc = 'kaczmarz'
      case(PCTELESCOPE);   pc = 'telescope'
      case(PCPATCH);   pc = 'patch'
      case(PCLMVM);   pc = 'lmvm'
      case(PCHMG);   pc = 'hmg'
      case(PCDEFLATION);   pc = 'deflation'
      case(PCHPDDM);   pc = 'hpddm'
      case(PCHARA);   pc = 'hara'
      case default;   call error('Unknown PETSC pctype: petsc_pc='//petsc_pc)
    end select
  end function
end module petsc_pc_mod
