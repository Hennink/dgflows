module rans_mod
  use f90_kind, only: dp
  use scalfunc_from_proc_mod, only: scalfunc_from_proc_t
  use scal_func_mod, only: scal_func_type
  use support, only: bool_i
  implicit none
  
  private
  public :: initialize
  public :: rans_model
  public :: write_nu_t_to_gmsh
  
  type, abstract :: rans_model_t
    integer :: no_eq
    type(scalfunc_from_proc_t) :: kinem_visc, &
                                  thermal_cond_over_cp, &
                                  diff_coeff_1st_eq, &
                                  source_1st_eq, &
                                  react_coeff_1st_eq, &
                                  diff_coeff_2nd_eq, &
                                  source_2nd_eq, &
                                  react_coeff_2nd_eq
  contains
    procedure(bool_i), deferred, nopass :: is_low_Re
  end type rans_model_t
  
  class(rans_model_t), allocatable, protected :: rans_model
  class(scal_func_type), allocatable :: density

  ! Turbulent Prandtl number
  real(dp), parameter :: Pr_t = 0.85_dp
  
  
  interface
    module subroutine get_prandtl_model(rans_model)
      implicit none
      class(rans_model_t), allocatable, intent(out) :: rans_model
    end subroutine get_prandtl_model
    
    module subroutine get_SA_model(rans_model)
      implicit none
      class(rans_model_t), allocatable, intent(out) :: rans_model
    end subroutine get_SA_model
    
    module subroutine get_k_model(rans_model)
      implicit none
      class(rans_model_t), allocatable, intent(out) :: rans_model
    end subroutine get_k_model
    
    module subroutine get_k_eps_model(rans_model)
      implicit none
      class(rans_model_t), allocatable, intent(out) :: rans_model
    end subroutine get_k_eps_model
    
    module subroutine get_log_k_log_eps_model(rans_model)
      implicit none
      class(rans_model_t), allocatable, intent(out) :: rans_model
    end subroutine get_log_k_log_eps_model
    
    module subroutine get_k_omega_model(rans_model)
      implicit none
      class(rans_model_t), allocatable, intent(out) :: rans_model
    end subroutine get_k_omega_model
    
    module subroutine get_k_log_w_model(rans_model)
      implicit none
      class(rans_model_t), allocatable, intent(out) :: rans_model
    end subroutine get_k_log_w_model
  end interface
  
contains
  subroutine initialize()
    use exceptions, only: error
    use numerical_solutions, only: matprop_set
    use run_data, only: rans_model_description
    
    select case (rans_model_description)
      case('prandtl')
        call get_prandtl_model(rans_model)
      case('SA')
        call get_SA_model(rans_model)
      case('k')
        call get_k_model(rans_model)
      case('k-eps')
        call get_k_eps_model(rans_model)
      case('log-k-log-eps')
        call get_log_k_log_eps_model(rans_model)
      case('k-omega')
        call get_k_omega_model(rans_model)
      case('k-log-w')
        call get_k_log_w_model(rans_model)
      case default
        call error('rans_mod: RANS model = '//trim(rans_model_description)//' not implemented')
    end select
    
    ! Initialize here those procedures which are not model-dependent
    call matprop_set%get_func('density',density)
    rans_model%thermal_cond_over_cp%proc => thermal_cond_turb_over_cp
  end subroutine initialize
  
  subroutine thermal_cond_turb_over_cp(elem,pnt_set,time,values)
    use assertions, only: assert, BOUNDS_CHECK
    use mesh_objects, only: elem_list
    use pnt_set_mod, only: pnt_set_t
      
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu_t, rho
    
    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)
    
    ! the turbulent conductivity is defined only on fluid elements
    if (elem%is_fluid()) then
      call density%get_values(elem,pnt_set,time,rho)
      call rans_model%kinem_visc%get_values(elem,pnt_set,time,nu_t)
      values = (rho*nu_t)/Pr_t
    else
      values = 0.0_dp
    endif
  end subroutine thermal_cond_turb_over_cp
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine write_nu_t_to_gmsh(mesh)
    use gmsh_interface, only: write_to_gmsh_file
    use mesh_type, only: fem_mesh
    
    type(fem_mesh), intent(in) :: mesh
    
    call write_to_gmsh_file(rans_model%kinem_visc,mesh,'num-nu_t',only_fluid=.true.,time=1)
  end subroutine write_nu_t_to_gmsh
end module rans_mod
