module characteristic_len_mod
  use assertions, only: assert, assert_eq
  use exceptions, only: error
  use f90_kind, only:dp
  use mesh_objects, only: elem_list
  use run_data, only: mesh_dimen
  implicit none
  
  private
  public :: isotropic_charact_len, anisotropic_charact_len
  public :: pol_scaling
  public :: diam_bounding_sphere
  public :: output_avg_mesh_size
  
contains
  real(dp) function isotropic_charact_len(elem,qnty,len_specs,order_specs)
    use numerical_solutions, only: pol_order
    
    type(elem_list), intent(in) :: elem
    character(*), intent(in) :: qnty
    character(*), intent(in) :: len_specs, order_specs
    
    real(dp) :: anisotropic_len(mesh_dimen)
    
    call assert(len_specs/='diff_maxmin_vtx','isotropic_charact_len: "diff_maxmin_vtx" length is anisotropic!')
    
    anisotropic_len = anisotropic_charact_len(elem,qnty,len_specs,order_specs)
    isotropic_charact_len = anisotropic_len(1)
  end function isotropic_charact_len
  
  function anisotropic_charact_len(elem,qnty,len_specs,order_specs)
    use numerical_solutions, only: pol_order
      
    type(elem_list), intent(in) :: elem
    character(*), intent(in) :: qnty
    character(*), intent(in) :: len_specs, order_specs
    real(dp) :: anisotropic_charact_len(mesh_dimen)
    
    integer :: order
    real(dp) :: pscale

    order = pol_order(elem,qnty)
    pscale = pol_scaling(elem,order,order_specs)
    anisotropic_charact_len = pscale * geom_len(elem,len_specs)
  end function anisotropic_charact_len
  
  real(dp) function pol_scaling(elem,order,order_specs)
    use local_elem, only: no_dg_functions

    type(elem_list), intent(in) :: elem
    integer, intent(in) :: order
    character(*), intent(in) :: order_specs
    
    select case(order_specs)
      case('linear');     pol_scaling = 1.0_dp / (1 + order)
      case('quadratic');  pol_scaling = 1.0_dp / order**2
      case('eqv_ndofs');  pol_scaling = 1.0_dp / no_dg_functions(elem%eltype,order)**(1.0_dp/mesh_dimen)
      case default;       call error('pol_scaling: unknown order_specs=' // order_specs)
    end select
  end function pol_scaling
  
  function geom_len(elem,len_specs)
    type(elem_list), intent(in) :: elem
    character(*), intent(in) :: len_specs
    real(dp) :: geom_len(mesh_dimen)
    
    real(dp), allocatable :: vtx_gcors(:,:)
    
    select case(len_specs)
      case('vol')
        geom_len = elem%volume**(1.0_dp/mesh_dimen)
      case('diam')
        call elem%glob_coord_vertices(vtx_gcors)
        geom_len = diam_bounding_sphere(vtx_gcors)
      case('diff_maxmin_vtx')
        call elem%glob_coord_vertices(vtx_gcors)
        geom_len = maxval(vtx_gcors,dim=2) - minval(vtx_gcors,dim=2)
      case default
        call error('geom_len: unknown length specification')
    end select
  end function geom_len
  
  pure real(dp) function diam_bounding_sphere(gcors) result(diam)
    ! Returns the diameter of the smallest sphere that bounds all gcors.
    !
    ! The bounding sphere passes through N points.
    ! 1.  N > 4:  same as N == 4, just delete some points.
    ! 2.  N == 4: Now either
    !     a)  the points lie in a great circle segment of the sphere, which is the
    !         circumscribed circle of the points, or
    !     b)  the bounding sphere is the circumscribed sphere of the tetrahedron
    !         formed by the points.
    ! 3.  N == 3: The points must lie on a great circle of the bounding sphere,
    !            which is the circumscribed circle of the points.
    ! 4.  N < 3: only occurs when there are less than 3 coordinates. The
    !            diameter is the maximum distance.
    !
    ! We ignore case 2.b, as the algorithms in [1,4,5] are overly complicated.
    !
    ! [1]  http://realtimecollisiondetection.net/blog/?p=20
    ! [2]  https://www.gamedev.net/forums/topic/442767-minimum-bounding-circle/
    ! [3]  https://en.wikipedia.org/wiki/Circumscribed_circle
    ! [4]  https://math.stackexchange.com/a/2413725/314772
    ! [5]  www2.washjeff.edu/users/mwoltermann/Dorrie/70.pdf
    use support, only: cross_product

    real(dp), intent(in) :: gcors(:,:)

    integer :: no_gcors, i, j, k
    real(dp), dimension(size(gcors,1)) :: vec1, vec2, vec3
    real(dp) :: len1, len2, len3, area, diam_circumscribed_circle

    no_gcors = size(gcors,2)
    diam = 0.0_dp
    do i = 1, no_gcors
      do j = i+1, no_gcors
        vec1 = gcors(:,i) - gcors(:,j)
        len1 = norm2(vec1)
        diam = max(diam,len1)
        do k = j+1, no_gcors
          vec2 = gcors(:,j) - gcors(:,k)
          vec3 = gcors(:,k) - gcors(:,i)
          len2 = norm2(vec2)
          len3 = norm2(vec3)
          area = 0.5_dp * norm2(cross_product(vec1,vec2))
          diam_circumscribed_circle = (len1*len2*len3) / (2.0_dp*area) ! See [3].
          diam = max(diam,diam_circumscribed_circle)
        enddo
      enddo
    enddo
  end function diam_bounding_sphere
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine output_avg_mesh_size(mesh)
    use io_basics, only: REAL_FMT
    use mpi_wrappers_mod, only: reduce_wrap
    use mesh_objects, only: elem_id, elem_list
    use mesh_partitioning, only: my_first_last_elems
    use mesh_type, only: fem_mesh
    use petsc_mod, only: petsc_proc_id
    
    type(fem_mesh), intent(in) :: mesh
    
    real(dp) :: my_sum, avg_mesh_size, elem_size(mesh%dimen)
    integer :: first_elem, last_elem, elem_no
    type(elem_id) :: id
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_list), pointer :: elem
    
    call mesh%get_active_elem_list_ptr(.false.,active_elem_list)
    call my_first_last_elems(.false.,first_elem,last_elem)
    
    my_sum = 0.0_dp
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      elem_size = geom_len(elem,'vol')
      my_sum = my_sum + elem_size(1) / mesh%no_active_elem
    enddo
    
    call reduce_wrap(my_sum,'sum',0,avg_mesh_size)
    if (petsc_proc_id() == 0) write(*,'(a,'//REAL_FMT//')') 'Average mesh size: ', avg_mesh_size
  end subroutine output_avg_mesh_size
end module characteristic_len_mod

