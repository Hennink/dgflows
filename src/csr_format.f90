module csr_format
  implicit none
  
  private
  public :: CSR_mesh_format_type
  public :: zero_based_indexing, one_based_indexing
  
  type :: CSR_mesh_format_type
    private
    integer, allocatable :: elem_ptr(:)
    integer, allocatable :: eind(:)
    integer :: no_elem_added = 0
    integer :: no_nodes_added = 0
  
  contains
    private
    procedure, public :: init => init_CSR_mesh_format
    procedure, public :: add_nodes_of_next_elem
    procedure, private :: no_elem
    procedure, private :: max_idx_eind
    procedure, public :: get_CSR_mesh_format
  end type CSR_mesh_format_type
  
  type :: indexing_type
    private
    integer :: base_idx
  end type indexing_type
  
  type(indexing_type), parameter :: zero_based_indexing = indexing_type(0)
  type(indexing_type), parameter :: one_based_indexing = indexing_type(1)
  
contains
  subroutine init_CSR_mesh_format(this,no_elem,max_no_nodes_per_elem)
    class(CSR_mesh_format_type), intent(out) :: this
    integer, intent(in) :: no_elem
    integer, intent(in) :: max_no_nodes_per_elem
    
    integer :: max_size_eind
    
    allocate(this%elem_ptr(0:no_elem))
    this%elem_ptr(0) = 0
    max_size_eind = max_no_nodes_per_elem * no_elem
    allocate(this%eind(0:(max_size_eind-1)))
  end subroutine init_CSR_mesh_format
  
  pure subroutine add_nodes_of_next_elem(this,nodes)
    class(CSR_mesh_format_type), intent(inout) :: this
    integer, intent(in) :: nodes(:)
    
    integer :: last_idx
    integer :: i
    
    last_idx = this%no_nodes_added - 1
    do i = 1, size(nodes)
      this%eind(i+last_idx) = nodes(i) - 1
      this%no_nodes_added = this%no_nodes_added + 1
    enddo
    
    this%no_elem_added = this%no_elem_added + 1
    this%elem_ptr(this%no_elem_added) = this%no_nodes_added
  end subroutine add_nodes_of_next_elem
  
  pure integer function no_elem(this)
    class(CSR_mesh_format_type), intent(in) :: this
    
    no_elem = size(this%elem_ptr) - 1
  end function no_elem
  
  pure integer function max_idx_eind(this)
    class(CSR_mesh_format_type), intent(in) :: this
    
    max_idx_eind = this%no_nodes_added - 1
  end function max_idx_eind
  
  pure subroutine get_CSR_mesh_format(this,indexing,elem_ptr,eind)
    class(CSR_mesh_format_type), intent(in) :: this
    type(indexing_type), intent(in) :: indexing
    integer, allocatable, intent(out) :: elem_ptr(:)
    integer, allocatable, intent(out) :: eind(:)
    
    integer :: low_idx, up_idx
    
    low_idx = indexing%base_idx
    up_idx = this%no_elem() + indexing%base_idx
    allocate(elem_ptr(low_idx:up_idx))
    elem_ptr = this%elem_ptr + indexing%base_idx
    
    up_idx = this%max_idx_eind() + indexing%base_idx
    allocate(eind(low_idx:up_idx))
    eind = this%eind(0:this%max_idx_eind()) + indexing%base_idx
  end subroutine get_CSR_mesh_format
end module csr_format
