module timer_typedef
  use f90_kind, only: dp
  implicit none
  
  private
  public :: timer_type
  
  type :: timer_type
    private
    real(dp) :: time_of_last_start
    real(dp) :: stored_elapsed_time = 0.0_dp
    logical :: is_running = .false.
    
  contains
    procedure, non_overridable :: start, pauze, time, reset
    procedure, non_overridable, private :: unstored_elapsed_time
  end type timer_type
  
contains
  subroutine start(this)
    class(timer_type), intent(inout) :: this
    
    if (.not. this%is_running) then
      this%time_of_last_start = abs_time()
      this%is_running = .true.
    endif
  end subroutine start
  
  subroutine pauze(this)
    class(timer_type), intent(inout) :: this
    
    if (this%is_running) then
      this%stored_elapsed_time = this%stored_elapsed_time + this%unstored_elapsed_time()
      this%is_running = .false.
    endif
  end subroutine pauze
  
  impure elemental real(dp) function time(this)
    class(timer_type), intent(in) :: this
    
    time = this%stored_elapsed_time + this%unstored_elapsed_time()
  end function time
  
  real(dp) function unstored_elapsed_time(this) result(T)
    class(timer_type), intent(in) :: this
    
    real(dp) :: current_time
    
    if (this%is_running) then
      current_time = abs_time()
      T = current_time - this%time_of_last_start
    else
      T = 0.0_dp
    endif
  end function unstored_elapsed_time
  
  subroutine reset(this)
    class(timer_type), intent(out) :: this
  end subroutine reset

  real(REAL64) function abs_time()
    ! Returns the absolute time, from some fixed, arbitrary point in the past.
    use mpi_wrappers_mod, only: wtime
    use, intrinsic :: iso_fortran_env, only: REAL64

    logical, parameter :: use_mpi = .true.

    if (use_mpi) then
      abs_time = wtime()
    else
      call cpu_time(abs_time)
    endif
  end function
end module timer_typedef
