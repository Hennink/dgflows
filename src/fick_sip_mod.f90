module fick_sip_mod
  ! Regular SIP method, for processes governed by Fick's law.
  use gsip, only: gsip_t
  implicit none
  
  private
  public :: sip_ficks_law_t
  public :: shahbazi_pressure_op_t
  
  type, extends(gsip_t) :: sip_ficks_law_t
  contains
    procedure, nopass :: get_bc_in_gsip_context => get_fick_bc
    procedure, non_overridable :: stress_tensor_at_pnt_set => fick_stress_at_qps
    procedure, non_overridable :: nonFick_part_of_stress
  end type

  type, extends(sip_ficks_law_t) :: shahbazi_pressure_op_t
  contains
    procedure, non_overridable, nopass :: get_bc_in_gsip_context => get_shahbazi_bc
  end type
  
contains
  subroutine fick_stress_at_qps(this,elem,pnt_set,handler,stress)
    use dof_handler, only: dof_handler_type
    use mesh_objects, only: elem_list
    use lincom_rank2_mod, only: lincom_rank2_t
    use pnt_set_mod, only: pnt_set_t
    
    class(sip_ficks_law_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    type(dof_handler_type), intent(in) :: handler
    type(lincom_rank2_t), intent(out) :: stress
    
    integer :: active_elem_no

    active_elem_no = elem%get_active_elem_no(handler%only_fluid_mesh)
    call stress%set_as_gder_at_pnts(active_elem_no,pnt_set,handler)
  end subroutine
  
  subroutine nonFick_part_of_stress(this,elem,pnt_set,handler,stress)
    use dof_handler, only: dof_handler_type
    use mesh_objects, only: elem_list
    use lincom_rank2_mod, only: lincom_rank2_t
    use pnt_set_mod, only: pnt_set_t
    
    class(sip_ficks_law_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    type(dof_handler_type), intent(in) :: handler
    type(lincom_rank2_t), intent(out) :: stress
    
    call stress%init(elem%dimen(),handler%no_dirs,elem%active_elem_no,handler,elem%nqp)
  end subroutine
  
  subroutine get_shahbazi_bc(bc_id,qnty,bc)
    use assertions, only: assert
    use boundary_condition_types
    use boundaries, only: get_bc
    use functions_lib, only: zero_vecfunc
    use solvec_mod, only: solvec_t
    
    integer, intent(in) :: bc_id
    type(solvec_t), intent(in) :: qnty
    class(bc_type), allocatable, intent(out) :: bc
    
    class(bc_type), allocatable :: velocity_bc
    type(dirichlet_bc_type), allocatable :: dirichlet_bc
    
    call assert(qnty%name == 'pressure')

    call get_bc(bc_id,'velocity',velocity_bc)

    select type(velocity_bc)
      class is (dirichlet_bc_type)
        allocate(bc,source=hom_neumann_bc_type())
      class is (neumann_bc_type)
        allocate(dirichlet_bc)
        allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(qnty%no_dirs()))
        call move_alloc(dirichlet_bc,bc)
      class is (generalized_neumann_bc_t)
        allocate(dirichlet_bc)
        allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(qnty%no_dirs()))
        call move_alloc(dirichlet_bc,bc)
      class is (adjusted_traction_bc_t)
        allocate(dirichlet_bc)
        allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(qnty%no_dirs()))
        call move_alloc(dirichlet_bc,bc)
      class is (slip_bc_type)
        ! The slip bc is an homogeneous Dirichlet bc for the velocity in the normal direction =>
        ! it should be an homogeneous Neumann bc for the pressure
        allocate(bc,source=hom_neumann_bc_type())
      class default
        error stop 'Shahbazi pressure operator: unknown bc'
    end select
    
    call interpret_bc_in_Fick_context(bc,qnty%no_dirs())
  end subroutine

  subroutine get_fick_bc(bc_id,qnty,bc)
    use boundaries, only: get_bc
    use boundary_condition_types, only: bc_type
    use solvec_mod, only: solvec_t
    
    integer, intent(in) :: bc_id
    type(solvec_t), intent(in) :: qnty
    class(bc_type), allocatable, intent(out) :: bc
    
    call get_bc(bc_id,qnty%name,bc)
    call interpret_bc_in_Fick_context(bc,qnty%no_dirs())
  end subroutine
  
  subroutine interpret_bc_in_Fick_context(bc,no_dirs)
    use boundary_condition_types
    use exceptions, only: error
    use functions_lib, only: zero_scalfunc, zero_vecfunc
    
    class(bc_type), allocatable, intent(inout) :: bc
    integer, intent(in) :: no_dirs
    
    type(generalized_robin_bc_t), allocatable :: gen_robin_bc
    class(bc_type), allocatable :: new_bc

    allocate(gen_robin_bc)
    select type(bc)
      type is (dirichlet_bc_type)
        allocate(new_bc,source=bc)
      type is (hom_neumann_bc_type)
        allocate(gen_robin_bc%coeff,source=zero_scalfunc())
        allocate(gen_robin_bc%inhom_part,source=zero_vecfunc(no_dirs))
        call move_alloc(gen_robin_bc,new_bc)
      type is (inhom_neumann_bc_type)
        allocate(gen_robin_bc%coeff,source=zero_scalfunc())
        allocate(gen_robin_bc%inhom_part,source=bc%inhom_part)
        call move_alloc(gen_robin_bc,new_bc)
      type is (robin_bc_type)
        allocate(gen_robin_bc%coeff,source=bc%coeff)
        allocate(gen_robin_bc%inhom_part,source=bc%inhom_part)
        call move_alloc(gen_robin_bc,new_bc)
      class default
        call error('unknown B.C.')
    end select
    call move_alloc(new_bc,bc)
  end subroutine interpret_bc_in_Fick_context
end module
