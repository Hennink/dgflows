submodule(rans_mod) rans_mod__log_k_log_eps
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use exceptions, only: error
  use mesh_objects, only: elem_list
  use numerical_solutions, only: matprop_set, num_scalfunc_set, velocity, solve_for_energy
  use pnt_set_mod, only: pnt_set_t
  use run_data, only: boussinesq_approx, div_free_velocity, min_Pk_for_RANS
  use support, only: false, trace
  implicit none
  
  ! Von-Karman constant
  real(dp), parameter :: Karman = 0.41_dp
  
  ! Model constants
  real(dp), parameter :: C_mu = 0.09_dp
  real(dp), parameter :: C_eps1 = 1.44_dp
  real(dp), parameter :: C_eps2 = 1.92_dp
  real(dp), parameter :: C_eps3 = 1.0_dp
  real(dp), parameter :: sigma_k = 1.0_dp
  real(dp), parameter :: sigma_eps = 1.3_dp
  
  type, extends(rans_model_t) :: log_k_log_eps_model_t
  contains
    procedure, nopass :: is_low_Re => false
  end type log_k_log_eps_model_t
  
  class(scal_func_type), allocatable :: num_log_k, num_log_eps
  class(scal_func_type), allocatable :: nu_molec, beta_func, temperature
  logical :: add_buoyancy_term
  
contains
  module procedure get_log_k_log_eps_model
    call num_scalfunc_set%get_func('spec_turb1',num_log_k)
    call num_scalfunc_set%get_func('spec_turb2',num_log_eps)
    call matprop_set%get_func('kinematic_viscosity',nu_molec)
    
    call solve_for_energy(add_buoyancy_term)
    if (add_buoyancy_term .and. boussinesq_approx) then
      call matprop_set%get_func('thermal_expansion_coeff',beta_func)
      call matprop_set%get_func('temperature',temperature)
    endif
    
    allocate(log_k_log_eps_model_t :: rans_model)
    rans_model%kinem_visc%proc => kinem_visc_turb
    rans_model%diff_coeff_1st_eq%proc => diff_coeff_log_k
    rans_model%source_1st_eq%proc => source_log_k
    rans_model%diff_coeff_2nd_eq%proc => diff_coeff_log_eps
    rans_model%source_2nd_eq%proc => source_log_eps
    rans_model%no_eq = 2
  end procedure get_log_k_log_eps_model
  
  
  subroutine kinem_visc_turb(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu, log_k, log_eps
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call num_log_k%get_values(elem,pnt_set,time,log_k)
    call num_log_eps%get_values(elem,pnt_set,time,log_eps)
    do p = 1, no_points
      values(p) = max(C_mu*exp(2.0_dp*log_k(p)-log_eps(p)),1.0e-1_dp*nu(p))
    enddo
  end subroutine kinem_visc_turb
  
  
  subroutine diff_coeff_log_k(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu_t, nu
    
    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)
    
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    values = nu + nu_t/sigma_k
  end subroutine diff_coeff_log_k
  
  
  subroutine source_log_k(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: prod_values, diss_values, grad_grad_values
    
    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)
    
    call production_k(elem,pnt_set,time,prod_values)
    call dissipation_k(elem,pnt_set,time,diss_values)
    call grad_grad_log_k_term(elem,pnt_set,time,grad_grad_values)
    values = prod_values + diss_values + grad_grad_values
  end subroutine source_log_k
  
  subroutine production_k(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: Gk, log_k
    
    call assert(size(values)==pnt_set%np(),BOUNDS_CHECK)
    
    call num_log_k%get_values(elem,pnt_set,time,log_k)
    call shear_production(elem,pnt_set,time,values)
    if (add_buoyancy_term) then
      call buoyancy_term(elem,pnt_set,time,Gk)
      values = values + Gk
    endif
    values = values * exp(-log_k)
  end subroutine production_k
  
  subroutine shear_production(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu_t, rho, log_k
    real(dp) :: grad_U(elem%dimen(),velocity%no_dirs(),size(values))
    real(dp) :: div_U
    integer :: no_points, p
    character(200) :: err_msg
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    call density%get_values(elem,pnt_set,time,rho)
    call velocity%get_gders(elem,pnt_set,time,grad_U)
    if (.not. div_free_velocity) call num_log_k%get_values(elem,pnt_set,time,log_k)
    do p = 1, no_points
      ! Strain rate tensor part: mu_t*(dU_i/dx_j + dU_j/dx_i)*dU_i/dx_j = 2 * mu_t * S_ij**2
      values(p) = rho(p)*nu_t(p)*0.5_dp*sum((grad_U(:,:,p) + transpose(grad_U(:,:,p)))**2)
      if (.not. div_free_velocity) then
        ! Divergence part: -(2/3)*mu_t*(div.U)*delta_ij*dU_i/dx_j = -(2/3)*mu_t*(div.U)^2
        div_U = trace(grad_U(:,:,p))
        values(p) = values(p) - (2.0_dp/3.0_dp)*rho(p)*nu_t(p)*(div_U**2)
        ! Isotropic part: -(2/3)*rho*k*delta_ij*dU_i/dx_j = -(2/3)*rho*k*div.U
        values(p) = values(p) - (2.0_dp/3.0_dp)*rho(p)*exp(log_k(p))*div_U
      endif
      if (min_Pk_for_RANS <= values(p) .and. values(p) < 0.0_dp) then
        ! Cap small negative production terms to zero
        values(p) = 0.0_dp
      elseif (values(p) < min_Pk_for_RANS) then
        write(err_msg,"('shear turb. production = ',es10.3,' < ',es10.3)") values(p), min_Pk_for_RANS
        call error(trim(err_msg))
      endif
    enddo
  end subroutine shear_production
  
  subroutine buoyancy_term(elem,pnt_set,time,values)
    use sources_mod, only: spec_force_func
    
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu_t, rho, beta
    real(dp), dimension(elem%dimen(),size(values)) :: g, grad_rho, grad_T
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    call spec_force_func%get_values(elem,pnt_set,time,g)
    if (boussinesq_approx) then
      call density%get_values(elem,pnt_set,time,rho)
      call beta_func%get_values(elem,pnt_set,time,beta)
      call temperature%get_gders(elem,pnt_set,time,grad_T)
    else
      call density%get_gders(elem,pnt_set,time,grad_rho)
    endif
    do p = 1, no_points
      if (boussinesq_approx) then
        values(p) = (nu_t(p)/Pr_t) * rho(p) * beta(p) * dot_product(g(:,p),grad_T(:,p))
      else
        values(p) = -(nu_t(p)/Pr_t) * dot_product(g(:,p),grad_rho(:,p))
      endif
    enddo
  end subroutine buoyancy_term
  
  subroutine dissipation_k(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu_t, rho, log_k
    
    call assert(size(values)==pnt_set%np(),BOUNDS_CHECK)
    
    call density%get_values(elem,pnt_set,time,rho)
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    call num_log_k%get_values(elem,pnt_set,time,log_k)
    values = - rho * C_mu * exp(log_k) / nu_t
  end subroutine dissipation_k
  
  subroutine grad_grad_log_k_term(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(elem%dimen(),size(values)) :: grad_log_k
    real(dp), dimension(size(values)) :: diff_coeff, rho
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call num_log_k%get_gders(elem,pnt_set,time,grad_log_k)
    call diff_coeff_log_k(elem,pnt_set,time,diff_coeff)
    call density%get_values(elem,pnt_set,time,rho)
    do p = 1, no_points
      values(p) = rho(p)*diff_coeff(p)*dot_product(grad_log_k(:,p),grad_log_k(:,p))
    enddo
  end subroutine grad_grad_log_k_term
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine diff_coeff_log_eps(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu, nu_t
    
    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)
    
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    values = nu + nu_t/sigma_eps
  end subroutine diff_coeff_log_eps
  
  
  subroutine source_log_eps(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: prod_values, diss_values, grad_grad_values
    
    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)
    
    call production_eps(elem,pnt_set,time,prod_values)
    call dissipation_eps(elem,pnt_set,time,diss_values)
    call grad_grad_log_eps_term(elem,pnt_set,time,grad_grad_values)
    values = prod_values + diss_values + grad_grad_values
  end subroutine source_log_eps
  
  subroutine production_eps(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: log_k, Pk, Gk
    real(dp) :: P_tot
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call shear_production(elem,pnt_set,time,Pk)
    if (add_buoyancy_term) call buoyancy_term(elem,pnt_set,time,Gk)
    call num_log_k%get_values(elem,pnt_set,time,log_k)
    do p = 1, no_points
      P_tot = Pk(p)
      if (add_buoyancy_term) P_tot = P_tot + C_eps3 * max(Gk(p),0.0_dp)
      values(p) = C_eps1 * P_tot * exp(-log_k(p))
    enddo
  end subroutine production_eps
    
  subroutine dissipation_eps(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: log_k, log_eps, rho
    
    call assert(size(values)==pnt_set%np(),BOUNDS_CHECK)
    
    call num_log_k%get_values(elem,pnt_set,time,log_k)
    call num_log_eps%get_values(elem,pnt_set,time,log_eps)
    call density%get_values(elem,pnt_set,time,rho)
    values = - C_eps2 * rho * exp(log_eps - log_k)
  end subroutine dissipation_eps
  
  subroutine grad_grad_log_eps_term(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(elem%dimen(),size(values)) :: grad_log_eps
    real(dp), dimension(size(values)) :: diff_coeff, rho
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call num_log_eps%get_gders(elem,pnt_set,time,grad_log_eps)
    call diff_coeff_log_eps(elem,pnt_set,time,diff_coeff)
    call density%get_values(elem,pnt_set,time,rho)
    do p = 1, no_points
      values(p) = rho(p)*diff_coeff(p)*dot_product(grad_log_eps(:,p),grad_log_eps(:,p))
    enddo
  end subroutine grad_grad_log_eps_term
end submodule rans_mod__log_k_log_eps
