module string_mod
  implicit none
  
  private
  public :: as_chars
  public :: string_t
  public :: string_array_t
  
  integer, parameter :: max_no_strings = 16
  
  type string_t
    private
    character(:), allocatable :: word
  contains
    private
    procedure, public :: get
    procedure, public :: set
  end type string_t
  
  type string_array_t
    private
    type(string_t), dimension(max_no_strings) :: strings
    integer :: no_strings = 0
  contains
    private
    generic, public :: add => add_word, add_string
    procedure, private :: add_word
    procedure, private :: add_string
    procedure, public :: get_no_strings
    generic, public :: get => get_word, get_string, get_names
    procedure, private :: get_word
    procedure, private :: get_string
    procedure, private :: get_names
    procedure, public :: has
    procedure, public :: find_idx
  end type string_array_t
  
contains
  function as_chars(strings) result(chars)
    type(string_t), intent(in) :: strings(:)
    character(:), allocatable :: chars(:)
    
    integer :: i, n
    integer :: maxlen
    
    n = size(strings)
    maxlen = maxval([( len(strings(i)%word), i = 1, n )])
    allocate(character(maxlen) ::  chars(n))
    do i = 1, n
      chars(i) = strings(i)%word
    enddo
  end function as_chars

  pure function get(this) result(word)
    class(string_t), intent(in) :: this
    character(:), allocatable :: word
    
    allocate(word,source=this%word)
  end function get
  
  subroutine set(this,word)
    class(string_t), intent(out) :: this
    character(*), intent(in) :: word
    
    allocate(this%word,source=word)
  end subroutine set
  
  
  subroutine add_string(this,string)
    class(string_array_t), intent(inout) :: this
    type(string_t), intent(in) :: string
    
    call this%add_word(string%get())
  end subroutine add_string
  
  subroutine add_word(this,word)
    class(string_array_t), intent(inout) :: this
    character(*), intent(in) :: word
    
    if (this%has(word)) then
      ! We don't want duplicates => do nothing
    else
      if (this%no_strings >= max_no_strings) error stop 'Stash overflow in string_array'
      
      this%no_strings = this%no_strings + 1
      call this%strings(this%no_strings)%set(word)
    endif
  end subroutine add_word
  
  elemental logical function has(this,word)
    class(string_array_t), intent(in) :: this
    character(*), intent(in) :: word
    
    has = this%find_idx(word) /= 0
  end function has
  
  pure integer function find_idx(this,word) result(idx)
    class(string_array_t), intent(in) :: this
    character(*), intent(in) :: word
    
    do idx = 1, this%no_strings
      if (word == trim(this%strings(idx)%word)) return
    enddo
    idx = 0
  end function
  
  pure integer function get_no_strings(this)
    class(string_array_t), intent(in) :: this
    
    get_no_strings = this%no_strings
  end function get_no_strings
  
  subroutine get_string(this,string_no,string)
    class(string_array_t), intent(in) :: this
    integer, intent(in) :: string_no
    type(string_t), intent(out) :: string
    
    if (string_no > this%no_strings) error stop 'Out of bounds in string_array'
    string = this%strings(string_no)
  end subroutine get_string
  
  subroutine get_word(this,string_no,word)
    class(string_array_t), intent(in) :: this
    integer, intent(in) :: string_no
    character(:), allocatable, intent(out) :: word
    
    if (string_no > this%no_strings) error stop 'Out of bounds in string_array'
    allocate(word,source=trim(this%strings(string_no)%word))
  end subroutine get_word
  
  pure subroutine get_names(this,names)
    class(string_array_t), intent(in) :: this
    type(string_t), allocatable, intent(out) :: names(:)
    
    allocate(names,source=this%strings(1:this%no_strings))
  end subroutine get_names
end module string_mod

