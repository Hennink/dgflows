module vec_func_scal_compon_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use mesh_objects, only: elem_list
  use pnt_set_mod, only: pnt_set_t
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only: vec_func_type
  implicit none
  
  private
  public :: as_vecfunc
  public :: aggregate_vecfunc_t

  integer, parameter :: max_ndirs = 3
  
  type component_t
    ! Serves merely as a means to vectorize class(scal_func_type), if the 
    ! different components have seperate explicit types. This is analogous 
    ! to how arrays of pointers can be mimicked in Fortran.
    ! Ref. Metcalf et al., "Modern Fortran", 2011, section 14.3.3.
    class(scal_func_type), allocatable :: func
  end type component_t
  
  type, extends(vec_func_type) :: aggregate_vecfunc_t
    ! a vector-valued function, where each of its components can be seen as a 
    ! separate scalar function
    type(component_t) :: components(max_ndirs)
    integer :: ndirs = 0
  contains
    procedure, non_overridable :: get_values, get_gders, &
                                  no_dirs => get_ndirs, &
                                  select_dir
    procedure, non_overridable :: append_dir
  end type
  
contains
  function as_vecfunc(scal_func) result(vec_func)
    class(scal_func_type), intent(in) :: scal_func
    type(aggregate_vecfunc_t) :: vec_func
    
    call vec_func%append_dir(scal_func)
  end function as_vecfunc
  
  subroutine append_dir(this,scalfunc)
    class(aggregate_vecfunc_t), intent(inout) :: this
    class(scal_func_type), intent(in) :: scalfunc

    call assert(this%ndirs <= max_ndirs,'not space for new dir')

    this%ndirs = this%ndirs + 1
    allocate(this%components(this%ndirs)%func, source = scalfunc)
  end subroutine

  subroutine get_values(this,elem,pnt_set,time,values)
    class(aggregate_vecfunc_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), dimension(:,:), intent(out) :: values
    
    integer :: dir, no_dirs, np
    real(dp), allocatable :: values_one_dir(:)

    no_dirs = assert_eq(size(values,1),this%ndirs,BOUNDS_CHECK)
    np = assert_eq(size(values,2),pnt_set%np(),BOUNDS_CHECK)

    ! No way to do this completely column-major
    allocate(values_one_dir(np))
    do dir = 1, no_dirs
      call this%components(dir)%func%get_values(elem,pnt_set,time,values_one_dir)
      values(dir,:) = values_one_dir
    enddo
  end subroutine
  
  subroutine get_gders(this,elem,pnt_set,time,gders)
    class(aggregate_vecfunc_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:,:)
    
    integer :: dimen, dir, no_dirs, np
    real(dp), allocatable :: gders_one_dir(:,:)
    
    dimen = assert_eq(size(gders,1),elem%dimen(),BOUNDS_CHECK)
    no_dirs = assert_eq(size(gders,2),this%ndirs,BOUNDS_CHECK)
    np = assert_eq(size(gders,3),pnt_set%np(),BOUNDS_CHECK)

    ! No way to do this completely column-major
    allocate(gders_one_dir(dimen,np))
    do dir = 1, no_dirs
      call this%components(dir)%func%get_gders(elem,pnt_set,time,gders_one_dir)
      gders(:,dir,:) = gders_one_dir
    enddo
  end subroutine
  
  pure integer function get_ndirs(this) result(no_dirs)
    class(aggregate_vecfunc_t), intent(in) :: this
    
    no_dirs = this%ndirs
  end function
  
  subroutine select_dir(this,dir,scalfunc)
    use assertions, only: assert
    use scal_func_mod, only: scal_func_type
    
    class(aggregate_vecfunc_t), intent(in) :: this
    integer, intent(in) :: dir
    class(scal_func_type), allocatable, intent(out) :: scalfunc
    
    call assert(1 <= dir .and. dir <= this%ndirs,"dir does not exist")

    allocate(scalfunc, source = this%components(dir)%func)
  end subroutine select_dir
end module vec_func_scal_compon_mod
