module monad_of_scalfunc_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use monad_mod, only: monad_t
  use mesh_objects, only: elem_list
  use pnt_set_mod, only: pnt_set_t
  use scal_func_mod, only: scal_func_type
  implicit none
  
  private
  public :: monad_of_scalfunc_t, monad_of_scalfunc
  
  type, extends(scal_func_type) :: monad_of_scalfunc_t
    ! Create a function of the form f = f(arg). Here f is a monad.
    private
    class(scal_func_type), allocatable :: arg
    class(monad_t), allocatable :: monad
  contains
    procedure :: get_values, get_gders
  end type monad_of_scalfunc_t
  
contains
  pure function monad_of_scalfunc(monad,arg) result(new_scalfunc)
    use monad_mod, only: monad_t
    use scal_func_mod, only: scal_func_type
    
    class(monad_t), intent(in) :: monad
    class(scal_func_type), intent(in) :: arg
    type(monad_of_scalfunc_t) :: new_scalfunc
    
    allocate(new_scalfunc%monad,source=monad)
    allocate(new_scalfunc%arg,source=arg)
  end function monad_of_scalfunc
  

  subroutine get_values(this,elem,pnt_set,time,values)
    class(monad_of_scalfunc_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)

    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)

    call this%arg%get_values(elem,pnt_set,time,values)
    values = this%monad%value(values)
  end subroutine get_values

  subroutine get_gders(this,elem,pnt_set,time,gders)
    class(monad_of_scalfunc_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:)

    real(dp) :: args(size(gders,2))
    integer :: p, np

    call assert(size(gders,1) == elem%dimen(),BOUNDS_CHECK)
    np = assert_eq(size(gders,2),pnt_set%np(),BOUNDS_CHECK)

    call this%arg%get_values(elem,pnt_set,time,args)
    call this%arg%get_gders(elem,pnt_set,time,gders)
    do p = 1, np
      gders(:,p) = gders(:,p) * this%monad%deriv(args(p))
    enddo
  end subroutine get_gders
end module monad_of_scalfunc_mod
