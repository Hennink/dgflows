submodule(boundary_types) boundary_types__wall_func_fixedT
  ! This boundary type is compatible with internal walls bcs
  ! in conjugate heat transfer calculations.
  ! In fact, it is possible to do CHT with temperature/enthalpy
  ! wall functions on internal walls.
  use scal_func_mod, only: scal_func_type
  implicit none
  
  type, extends(wall_bnd_t) :: wall_func_fixedT_type
    class(scal_func_type), allocatable :: T_w
  contains
    procedure :: read_specs_from_file
    procedure :: get_bc
  end type wall_func_fixedT_type
  
contains
  module procedure alloc_wall_func_fixedT
    allocate(wall_func_fixedT_type :: bnd)
  end procedure alloc_wall_func_fixedT
  
  subroutine read_specs_from_file(this,unit_number)
    use rans_wall_functions_mod, only: init_rans_wall_func_mod => initialize
    use read_funcs_mod, only: read_line_as_qnty_eq_func
    
    class(wall_func_fixedT_type), intent(out) :: this
    integer, intent(in) :: unit_number
    
    call read_line_as_qnty_eq_func(unit_number,'T_wall',this%T_w)
    
    !This command introduces a "side-effect" in the subroutine.
    !However, I think it is the best place where to put this initialization
    !command. In this way, the rans_wall_functions module is initialized
    !if and only if this kind of bc is used. Moreover, it seemed conceptually
    !correct, since wall-functions bc cannot be initialized from a file, but 
    !indeed from the specific RANS module. 
    call init_rans_wall_func_mod()
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types
    use func_operations_mod
    use functions_lib, only: zero_vecfunc
    use numerical_solutions, only: matprop_set
    use rans_wall_functions_mod, only: wf_slipcoeff, &
                                       wf_bc_vol_turb2_qnty, &
                                       u_star_over_T_plus
    use vec_func_scal_compon_mod, only: as_vecfunc
    
    class(wall_func_fixedT_type), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    
    type(dirichlet_bc_type), allocatable :: dirichlet_bc
    type(slip_bc_type), allocatable :: slip_bc
    class(scal_func_type), allocatable :: cp, h, T, rho
    type(robin_bc_type), allocatable :: robin_bc
    
    call matprop_set%get_func('spec_heat_capacity',cp)
    call matprop_set%get_func('spec_enthalpy',h)
    call matprop_set%get_func('temperature',T)
    call matprop_set%get_func('density',rho)
    
    select case(qnty_name)
      case('temperature','spec_enthalpy','vol_enthalpy')
        if (qnty_name == 'temperature') then
          robin_bc%coeff = u_star_over_T_plus * rho * cp
        elseif (qnty_name == 'spec_enthalpy') then
          robin_bc%coeff = u_star_over_T_plus * rho
        else
          robin_bc%coeff = u_star_over_T_plus
        endif
        robin_bc%inhom_part = as_vecfunc(u_star_over_T_plus * rho * cp * this%T_w)
        call move_alloc(robin_bc,bc)
      case('mass_flux')
        allocate(slip_bc)
        allocate(slip_bc%slip_coeff, source=wf_slipcoeff)
        call move_alloc(slip_bc,bc)
      case('vol_turb1')
        !bc for k: homogeneous Neumann
        allocate(bc,source=hom_neumann_bc_type())
      case('vol_turb2')
        !bc for rho*eps/rho*omega: Dirichlet bc
        allocate(dirichlet_bc)
        allocate(dirichlet_bc%inhom_part, source=wf_bc_vol_turb2_qnty)
        call move_alloc(dirichlet_bc,bc)
      case('wall_dist')
        allocate(dirichlet_bc)
        allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(1)) !Should we add delta???
        call move_alloc(dirichlet_bc,bc)
      case default
        write(*,'(a)') 'ERROR: no bc for '//qnty_name//' in wall_functions'
        error stop
    end select
  end subroutine get_bc
end submodule boundary_types__wall_func_fixedT

