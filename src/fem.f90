module fem
  use assertions, only: assert, assert_eq
  use f90_kind, only: dp
  use qnty_handler_mod, only: max_sol_order
  implicit none
  
  logical, parameter :: ONLY_FLUID = .false.
  
contains
  subroutine gen_quadrature(mesh)
    use mesh_objects, only: elem_id, elem_list, face_list, elem_ptr, face_ptr
    use mesh_partitioning, only: elem_and_nghb_in_same_partition, my_first_last_elems
    use mesh_type, only: fem_mesh
    use petsc_mod, only: I_am_master_process
    
    type(fem_mesh) :: mesh
    
    integer :: active_elem, first_elem, last_elem
    integer :: face_index
    type(elem_id) :: id
    type(elem_list), pointer :: elem, nghb
    type(elem_ptr), allocatable :: active_neighbors(:)
    type(face_ptr), allocatable :: active_subfaces(:)
    type(face_list), pointer :: face
    
    ! Quadrature for the volumetric integrals
    call my_first_last_elems(ONLY_FLUID,first_elem,last_elem)
    if (I_am_master_process()) write(*,"('Volume Integrals...')",advance='no')
    do active_elem = first_elem, last_elem
      id = mesh%active_elem_list(active_elem)
      elem => mesh%get_elem_ptr(id)
      call calc_vol_quadrature(elem)
    enddo
    if (I_am_master_process()) write(*,"('done.')")
    
    ! Quadrature for the face integrals
    ! Note that these are done after the volumetric integrals, as the former rely on the latter. 
    ! Hence, one still needs to be sure that volumetric integrals are also computed on 1st neighbors.
    if (I_am_master_process()) write(*,"('Face Integrals...')",advance='no')
    do active_elem = first_elem, last_elem
      id = mesh%active_elem_list(active_elem)
      elem => mesh%get_elem_ptr(id)
      
      call elem%active_subfaces(active_subfaces,active_neighbors)
      do face_index = 1, size(active_subfaces)
        face => active_subfaces(face_index)%point
        if (.not. face%is_boundary(ONLY_FLUID)) then
          nghb => active_neighbors(face_index)%point
          if (.not. elem_and_nghb_in_same_partition(elem,nghb)) call calc_vol_quadrature(nghb)
        endif
        call calc_face_quadrature(face,mesh%dimen)
      enddo
    enddo
    if (I_am_master_process()) write(*,"('done.')")
  end subroutine gen_quadrature
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine calc_vol_quadrature(elem)
    use lapack_interface_mod, only: cholesky_decomposition, LOWER
    use local_elem, only: get_quadrature
    use local_global_mappings, only: lcors2pnt_set
    use local_integrals, only: int_fi_fj_on_elem
    use mesh_objects, only: elem_list
    use run_data, only: assume_affine_elems
    
    type(elem_list), intent(inout) :: elem
    
    integer :: quad_order, max_polorder
    real(dp), allocatable :: lcors(:,:), det_jac(:)
    
    max_polorder = max_sol_order(elem)
    call quad_orders(max_polorder,elem_quad_order=quad_order)
    if (allocated(elem%quot)) deallocate(elem%quot)
    call get_quadrature(elem%eltype,quad_order,lcors,elem%quot)
    call lcors2pnt_set(elem,max_polorder,lcors,elem%quad_points,det_jac)
    call elem%quad_points%set_tag('standard_vol_quad')
    elem%quot = elem%quot * abs(det_jac)
    elem%nqp = assert_eq(size(elem%quot),elem%quad_points%np())
    elem%volume = sum(elem%quot)
    if (.not. assume_affine_elems) then
      call int_fi_fj_on_elem(elem,max_polorder,elem%chol_massmat)
      call cholesky_decomposition(elem%chol_massmat,LOWER)
      call nan_in_stricktly_upper_triangular_part(elem%chol_massmat)
    endif
  end subroutine calc_vol_quadrature
  
  subroutine calc_face_quadrature(face,dimen)
    use characteristic_len_mod, only: diam_bounding_sphere
    use local_elem, only: get_quadrature, &
                          loc_idx_of_face_vertices_in_elem, &
                          get_opposite_vertex, &
                          nodal_functions
    use local_global_mappings, only: face_lcor2ngbr_lcor, lcors2pnt_set
    use mesh_objects, only: eltype_of_faces_of_elem, elem_list, face_list
    use string_manipulations, only: int2char
    
    type(face_list), intent(inout) :: face
    integer, intent(in) :: dimen
    
    integer :: order_face, quad_order_face
    integer :: eltype_face
    integer :: opposite_node
    integer :: iquad
    real(dp) :: det
    real(dp), dimension(dimen-1,dimen) :: jac
    real(dp), allocatable, dimension(:,:,:) :: lder_nodal
    real(dp), allocatable, dimension(:,:) :: lcors_E1, lcors_E2, glob_coord_of_face
    real(dp), dimension(dimen) :: normal
    real(dp), dimension(dimen) :: vector
    character(100) :: tag
    type(elem_list), pointer :: E1, E2
    
    E1 => face%elem_neighbors(1)%point
    if (.not. face%is_boundary(ONLY_FLUID)) then
      E2 => face%elem_neighbors(2)%point
      order_face = max(max_sol_order(E1),max_sol_order(E2))
    else
      order_face = max_sol_order(E1)
    endif
    call quad_orders(order_face,face_quad_order=quad_order_face)
    eltype_face = eltype_of_faces_of_elem(E1%eltype)
    call get_quadrature(eltype_face,quad_order_face,face%face_lcors,face%quot)
    face%nqp = size(face%quot)
    if (allocated(face%quad_points)) deallocate(face%quad_points)
    allocate(face%quad_points(size(face%elem_neighbors)))
    allocate(lcors_E1(dimen,face%nqp))
    if (face%is_boundary(ONLY_FLUID)) then
      call face_lcor2ngbr_lcor(face,face%face_lcors,lcors_E1)
    else
      allocate(lcors_E2,mold = lcors_E1)
      call face_lcor2ngbr_lcor(face,face%face_lcors,lcors_E1,lcors_E2)
    endif
    
    call face%glob_coord_vertices(.false.,glob_coord_of_face)
    if (allocated(face%normal)) deallocate(face%normal)
    allocate(face%normal(dimen,face%nqp))
    call lcors2pnt_set(E1,max_sol_order(E1),lcors_E1,face%quad_points(1))
    tag = 'standard_face_quad' // int2char(face%neighbor_loc_face_no(1))
    call face%quad_points(1)%set_tag(trim(tag))
    if (.not. face%is_boundary(ONLY_FLUID)) then
      call lcors2pnt_set(E2,max_sol_order(E2),lcors_E2,face%quad_points(2))
      tag = 'standard_face_quad' // int2char(face%neighbor_loc_face_no(2))
      call face%quad_points(2)%set_tag(trim(tag))
    endif
    
    ! Take a vector connecting a node on the face and some other node of the element.
    opposite_node = get_opposite_vertex(E1%eltype,face%neighbor_loc_face_no(1))
    vector = glob_coord_of_face(:,1) - E1%vertices(opposite_node)%point%coordinate%xyz(1:dimen)
    
    call nodal_functions(eltype_face,face%face_lcors,local_deriv=lder_nodal)
    do iquad = 1, face%nqp
      jac = matmul(lder_nodal(:,:,iquad), transpose(glob_coord_of_face))
      if (dimen==2) then
        normal(1) =  jac(1,2)
        normal(2) = -jac(1,1)
      else
        normal(1) =    jac(1,2)*jac(2,3) - jac(2,2)*jac(1,3)
        normal(2) = - (jac(1,1)*jac(2,3) - jac(2,1)*jac(1,3))
        normal(3) =    jac(1,1)*jac(2,2) - jac(2,1)*jac(1,2)
      endif
      det = norm2(normal)
      normal = normal / det
      face%quot(iquad) = face%quot(iquad) * abs(det)
      
      ! Derive the correct sign of the unit normal by investigating the sign
      ! of the inner product with the connection vector found before
      if (dot_product(vector,normal) < 0.0_dp) normal = - normal
      face%normal(:,iquad) = normal
    enddo
  
    face%area = sum(face%quot)
    face%diameter = diam_bounding_sphere(glob_coord_of_face)
  end subroutine calc_face_quadrature

  subroutine quad_orders(dg_poly_order,elem_quad_order,face_quad_order)
    ! Returns the orders of the quadrature sets on faces and elements.
    use run_data, only: extra_quad_order_elem, extra_quad_order_face, min_quad_order

    integer, intent(in) :: dg_poly_order
    integer, optional, intent(out) :: elem_quad_order, face_quad_order

    integer :: inv_density_order
    integer :: face_conv_order, elem_conv_order

    call assert(dg_poly_order >= 0)

    ! The nonlinear convective terms have the highest order.
    !
    ! Note that many functions inside the inprods are not constant, for example
    !   n \cdot (m/rho)  and  (m/rho) \cdot \nabla  in the convective terms;
    !   normal, penalty and diff. param             in the diff. terms;
    !   epsilon/k                                   in the turbulent production.
    ! These terms should normally vary less than an individual basis function.

    ! The 'inv_density_order' term is because we compute the velocity as
    ! u := m / rho.  We assume (1/rho) is fairly constant.
    inv_density_order = 0
    ! Face conv. terms are inprods of three functions and a normal vector.
    face_conv_order = 3 * dg_poly_order + inv_density_order
    ! Volumetric conv. is an inprod of three functions, one of wich is a grad.
    elem_conv_order = 3 * dg_poly_order - 1 + inv_density_order
    if (present(elem_quad_order)) elem_quad_order = elem_conv_order + extra_quad_order_elem
    if (present(face_quad_order)) face_quad_order = face_conv_order + extra_quad_order_face

    if (present(elem_quad_order)) elem_quad_order = max(elem_quad_order,min_quad_order)
    if (present(face_quad_order)) face_quad_order = max(face_quad_order,min_quad_order)
  end subroutine quad_orders

  subroutine nan_in_stricktly_upper_triangular_part(mat)
    use, intrinsic :: ieee_arithmetic, only: ieee_value, IEEE_SIGNALING_NAN

    real(dp), intent(inout) :: mat(:,:)

    integer :: i, j, n

    n = assert_eq(shape(mat))
    do j = 1, n
      do i = 1, j-1
        mat(i,j) = ieee_value(1.0_dp,IEEE_SIGNALING_NAN)
      enddo
    enddo
  end subroutine nan_in_stricktly_upper_triangular_part
end module fem
