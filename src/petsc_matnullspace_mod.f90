module petsc_matnullspace_mod
#include "petsc/finclude/petscmat.h"
  use petscmat, only: tMatNullSpace

  use assertions, only: assert
  implicit none

  private
  public :: petsc_matnullspace_t

  type petsc_matnullspace_t
    ! Object that removes a null space from a vector, that is, orthogonalizes the vector to a subspace.
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatNullSpace.html#MatNullSpace
    private
    type(tMatNullSpace), public :: petsc_matnullspace
    logical :: initialized = .false.
  contains
    procedure :: create
    final :: destroy
  end type petsc_matnullspace_t

contains
  subroutine create(this,vec)
    ! Creates a data structure used to project vectors out of null spaces.
    ! Vec must be normalized. It is NOT copied, so do not change it after this call.
    !
    ! Collective on MPI_Comm.
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatNullSpaceCreate.html#MatNullSpaceCreate
    use petscmat, only: MatNullSpaceCreate

    use petsc_basic_types_mod, only: fort2petsc
    use petsc_mod, only: petsc_mpi_comm
    use petsc_vec_mod, only: petsc_vec_type

    class(petsc_matnullspace_t), intent(out) :: this
    type(petsc_vec_type), intent(in) :: vec

    PetscErrorCode :: ierr

    call MatNullSpaceCreate(petsc_mpi_comm,fort2petsc(.false.),fort2petsc(1),vec%petsc_vec,this%petsc_matnullspace,ierr)
    call assert(ierr == 0,'ierr /= 0 in MatNullSpaceCreate')
    this%initialized = .true.
  end subroutine create
  
  subroutine destroy(this)
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatNullSpaceDestroy.html
    use petscmat, only: MatNullSpaceDestroy

    type(petsc_matnullspace_t), intent(inout) :: this
    
    PetscErrorCode :: ierr
    
    if (this%initialized) then
      call MatNullSpaceDestroy(this%petsc_matnullspace,ierr)
      call assert(ierr == 0,'ierr /= 0 in MatNullSpaceDestroy')
    endif
  end subroutine destroy
end module petsc_matnullspace_mod

