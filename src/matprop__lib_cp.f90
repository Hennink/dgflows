submodule(matprop_mod) matprop__lib_cp
  use f90_kind, only: dp
  use quantities_mod, only: name2sym
  implicit none

  character(:), allocatable :: fluid
  real(dp) :: pressure
  real(dp) :: temp_range(2)
  real(dp) :: spec_h_offset = 0.0_dp

  logical :: output_figs = .false.

contains
  module subroutine get_cp_matprop(unit_number,matprop_set)
    use coolprop_mod, only: cp_value

    integer, intent(in) :: unit_number
    type(func_set_expansion_t), intent(out) :: matprop_set

    real(dp) :: h_range(2)

    print *, 'initializing matprops ...'
    call read_fluid_props(unit_number)
    h_range(1) = cp_value('spec_enthalpy','temperature',temp_range(1),'pressure',pressure,fluid)
    h_range(2) = cp_value('spec_enthalpy','temperature',temp_range(2),'pressure',pressure,fluid)

    ! Don't map directly from T, but do it via h, because
    ! 1.  mapping to the same quantity from different indep. qnty's is
    !     inconsistent due to the error in the splines. This leads to large
    !     errors in the time scheme;
    ! 2   the temperature is only used to interface with the user (in IC's,
    !     BC's and the output).
    call add_link(matprop_set,'temperature',temp_range,'spec_enthalpy')
    call add_link(matprop_set,'spec_enthalpy',h_range,'temperature')
    call add_link(matprop_set,'spec_enthalpy',h_range,'dynamic_viscosity')
    call add_link(matprop_set,'spec_enthalpy',h_range,'kinematic_viscosity')
    call add_link(matprop_set,'spec_enthalpy',h_range,'thermal_conductivity')
    call add_link(matprop_set,'spec_enthalpy',h_range,'density')
    call add_link(matprop_set,'spec_enthalpy',h_range,'thermal_diffusivity')
    call add_link(matprop_set,'spec_enthalpy',h_range,'prandtl')
    call add_link(matprop_set,'spec_enthalpy',h_range,'spec_heat_capacity')
    call add_link(matprop_set,'spec_enthalpy',h_range,'k_over_cp')
    call add_link(matprop_set,'spec_enthalpy',h_range,'deriv_rho_h')
    call matprop_set%finalize()
    write(*,'(a)') 'Done initializing matprops.'
  end subroutine get_cp_matprop

  subroutine add_link(matprop_set,from,xrange,to)
    type(func_set_expansion_t), intent(inout) :: matprop_set
    character(*), intent(in) :: from, to
    real(dp), intent(in) :: xrange(2)

    call matprop_set%add_func_link(func_link(from,xrange,to))
    write(*,'(a)') name2sym(from) // ' --> ' // name2sym(to)
  end subroutine add_link

  subroutine read_fluid_props(unit_number)
    use assertions, only: assert
    use coolprop_mod, only: cp_value, set_reference_state
    use io_basics, only: nreals_fmt
    use petsc_mod, only: i_am_master_process

    integer, intent(in) :: unit_number

    real(dp) :: temperature_at_ref_state, spec_h_offset_at_T, ref_rhomolar
    integer :: ios
    character(200) :: iomsg
    character(:), allocatable :: temp
    namelist /fluid_specs/ temperature_at_ref_state, spec_h_offset_at_T, &
                           fluid, pressure, temp_range, &
                           output_figs

    allocate(character(1000) :: fluid)
    temperature_at_ref_state = -1.0_dp
    spec_h_offset_at_T = -1.0_dp
    read(unit_number,fluid_specs,iostat=ios,iomsg=iomsg)
    call assert(ios == 0,'cannot read CoolProp specs; iomsg=' // trim(iomsg))
    allocate(temp,source=trim(fluid))
    call move_alloc(temp,fluid)

    if (temperature_at_ref_state > 0.0_dp) then
      ref_rhomolar = cp_value('molar_density','temperature',temperature_at_ref_state,'pressure',pressure,fluid)
      if (i_am_master_process()) then
        write(*,'(A)', advance='no') 'CoolProp: Set reference at T, p, rhomolar = '
        write(*,nreals_fmt(3)) temperature_at_ref_state, pressure, ref_rhomolar
      endif
      call set_reference_state(fluid,temperature_at_ref_state,ref_rhomolar,hmolar0=0.0_dp,smolar0=0.0_dp)
    endif
    if (spec_h_offset_at_T > 0.0_dp) then
      spec_h_offset = cp_value('spec_enthalpy','temperature',spec_h_offset_at_T,'pressure',pressure,fluid)
      if (i_am_master_process()) then
        write(*,'(A)', advance='no') 'CoolProp:  Offset h by '
        write(*,nreals_fmt(1)) spec_h_offset
      endif
    endif
  end subroutine read_fluid_props

  type(func_link_t) function func_link(indep_qnty,domain,dep_qnty)
    use coolprop_with_fixed_qnty_mod, only: cp_with_fixed_qnty_t
    use func_link_mod, only: func_link_t
    use spline_mod, only: cubic_spline_t

    character(*), intent(in) :: indep_qnty, dep_qnty
    real(dp), intent(in) :: domain(2)

    type(cp_with_fixed_qnty_t) :: cp_with_fixed_qnty
    type(cubic_spline_t) :: spline

    cp_with_fixed_qnty%fluidname = fluid
    cp_with_fixed_qnty%fixed_qnty = 'pressure'
    cp_with_fixed_qnty%fixed_value = pressure
    cp_with_fixed_qnty%indep_qnty = indep_qnty
    cp_with_fixed_qnty%dep_qnty = dep_qnty
    call cp_with_fixed_qnty%get_spline(domain,spline)
    
    if (indep_qnty == 'spec_enthalpy')  spline%xa = spline%xa - spec_h_offset
    if (dep_qnty == 'spec_enthalpy')    spline%ya = spline%ya - spec_h_offset
    call func_link%init(indep_qnty,dep_qnty,spline)

    if (output_figs) call spline%plot(name2sym(indep_qnty),name2sym(dep_qnty))
  end function func_link
end submodule matprop__lib_cp
