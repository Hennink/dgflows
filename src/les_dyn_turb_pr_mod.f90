module les_dyn_turb_pr_mod
  use assertions, only: assert, assert_eq, assert_normal, BOUNDS_CHECK
  use eqv_elem_groups_mod, only: elem_groups_t
  use f90_kind, only: dp
  use les_eddy_viscosity_mod, only: eddy_kin_viscosity
  use mesh_objects, only: elem_id, elem_list, elem_ptr
  use mesh_type, only: fem_mesh
  use numerical_solutions, only: matprop_set, mass_flux
  use pnt_set_mod, only: pnt_set_t
  use run_data, only: mesh_dimen
  use scal_func_mod, only: scal_func_type
  implicit none

  public :: dyn_sfs_k_over_cp, &
            init_dyn_turb_pr_from_file, &
            recalc_avg_LM_MM_eqv_elems, &
            write_dyn_turb_pr_to_gmsh
  private

  type(elem_groups_t) :: elem_groups
  real(dp), allocatable :: LM_MM_in_group(:)
  
  logical :: ONLY_FLUID = .true.
  integer :: order_test_filter
  logical :: clip_turb_Pr = .false., warn_if_negative = .true.

contains
  subroutine init_dyn_turb_pr_from_file(unit,mesh)
    integer, intent(in) :: unit
    type(fem_mesh), intent(in) :: mesh

    character(1000) :: group_identifying_vec
    namelist /dyn_turb_pr_params/ group_identifying_vec, order_test_filter, clip_turb_Pr, warn_if_negative
    integer :: iostat
    character(200) :: iomsg

    read(unit,dyn_turb_pr_params,iostat=iostat,iomsg=iomsg)
    call assert(iostat==0,iomsg)

    call elem_groups%init(mesh,group_identifying_vec)
    call elem_groups%write_groups_to_gmsh('eqv_elems__dyn_pr',mesh)
    allocate(LM_MM_in_group(elem_groups%ngroups()))

    call recalc_avg_LM_MM_eqv_elems(mesh) ! Do this now, just to make sure that 'LM_MM_in_group' is not undefined.
  end subroutine init_dyn_turb_pr_from_file

  subroutine recalc_avg_LM_MM_eqv_elems(mesh)
    use exceptions, only: warning
    use mpi_wrappers_mod, only: allreduce_wrap
    use mesh_partitioning, only: my_first_last_elems
    use petsc_mod, only: i_am_master_process

    type(fem_mesh), intent(in) :: mesh

    integer :: group
    integer :: elem_no, first_elem, last_elem
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    type(elem_id), pointer :: active_elem_list(:)
    real(dp), allocatable :: LM_at_qps(:), MM_at_qps(:)
    real(dp), dimension(elem_groups%ngroups()) :: my_int_LM, my_int_MM, int_LM, int_MM

    call assert(size(LM_MM_in_group) == elem_groups%ngroups())

    call my_first_last_elems(ONLY_FLUID,first_elem,last_elem)
    call mesh%get_active_elem_list_ptr(ONLY_FLUID,active_elem_list)
    
    my_int_LM = 0.0_dp
    my_int_MM = 0.0_dp
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      group = elem_groups%group_of_elem(elem)
      call least_squares_proj_LM_MM(elem,elem%quad_points,LM_at_qps,MM_at_qps)
      my_int_LM(group) = my_int_LM(group) + dot_product(LM_at_qps,elem%quot)
      my_int_MM(group) = my_int_MM(group) + dot_product(MM_at_qps,elem%quot)
    enddo
    call allreduce_wrap(my_int_LM,'sum',int_LM)
    call allreduce_wrap(my_int_MM,'sum',int_MM)
    LM_MM_in_group = int_LM / int_MM

    if (any(LM_MM_in_group < 0) .and. warn_if_negative .and. i_am_master_process()) call warning('1/Pr < 0 from dynamic procedure.')
  end subroutine recalc_avg_LM_MM_eqv_elems

  subroutine dyn_sfs_k_over_cp(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)

    class(scal_func_type), allocatable :: rho_func
    real(dp), dimension(size(values)) :: rho, nu_sfs
    real(dp) :: LM_MM, inv_turb_Pr
    integer :: group

    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)

    group = elem_groups%group_of_elem(elem)
    LM_MM = LM_MM_in_group(group)
    call matprop_set%get_func('density',rho_func)
    call rho_func%get_values(elem,pnt_set,time,rho)
    call eddy_kin_viscosity%get_values(elem,pnt_set,time,nu_sfs)
    if (clip_turb_Pr) then
      inv_turb_Pr = max(LM_MM,0.0_dp)
    else
      inv_turb_Pr = LM_MM
    endif
    values = rho * nu_sfs * inv_turb_Pr
  end subroutine dyn_sfs_k_over_cp

  subroutine write_dyn_turb_pr_to_gmsh(mesh)
    type(fem_mesh), intent(in) :: mesh

    call elem_groups%write_vals_on_groups_to_gmsh('uncapped_turb_pr',mesh,1/LM_MM_in_group)
  end subroutine write_dyn_turb_pr_to_gmsh

  subroutine least_squares_proj_LM_MM(elem,pnt_set,LM,MM)
    use les_eddy_viscosity_mod, only: eddy_kin_viscosity
    use local_elem, only: no_dg_functions
    use pnt_set_mod, only: pnt_set_t
    use vec_func_mod, only: vec_func_type

    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    real(dp), allocatable, intent(out) :: LM(:), MM(:)

    class(scal_func_type), allocatable :: rho_func, T_func
    real(dp), allocatable :: rho_at_qps(:), T_at_qps(:), gderT_at_qps(:,:), nusfs_at_qps(:), m_at_qps(:,:)

    real(dp), allocatable :: rho_proj(:), nusfs_proj(:), gderT_proj(:,:), rho_nusfs_gderT_proj(:,:), &
                             rho_T_proj(:), m_proj(:,:), m_T_proj(:,:)

    real(dp) :: rho_coarse, nusfs_coarse, gderT_coarse(mesh_dimen), rho_nusfs_gderT_coarse(mesh_dimen), &
                rho_T_coarse, m_coarse(mesh_dimen), m_T_coarse(mesh_dimen)
    real(dp), dimension(mesh_dimen) :: mi, li

    integer :: dimen
    integer :: nnod_filter
    integer :: i, p, qp, nqp
    integer, parameter :: TIME = 1

    nqp = assert_eq(size(elem%quot),elem%nqp,elem%quad_points%np(),BOUNDS_CHECK)
    dimen = assert_eq(mesh_dimen,elem%dimen(),BOUNDS_CHECK)

    ! Evaluate all quantities that need to be projected at the quadrature points:
    call matprop_set%get_func('density',rho_func)
    call matprop_set%get_func('temperature',T_func)
    allocate(rho_at_qps(nqp))
    allocate(m_at_qps(dimen,nqp))
    allocate(T_at_qps(nqp))
    allocate(gderT_at_qps(dimen,nqp))
    allocate(nusfs_at_qps(nqp))
    call rho_func%get_values(elem,elem%quad_points,TIME,rho_at_qps)
    call mass_flux%get_values(elem,elem%quad_points,TIME,m_at_qps)
    call T_func%get_values(elem,elem%quad_points,TIME,T_at_qps)
    call T_func%get_gders(elem,elem%quad_points,TIME,gderT_at_qps)
    call eddy_kin_viscosity%get_values(elem,elem%quad_points,TIME,nusfs_at_qps)

    nnod_filter = no_dg_functions(elem%eltype,order_test_filter)
    allocate(rho_proj(nnod_filter), source=0.0_dp)
    allocate(nusfs_proj(nnod_filter), source=0.0_dp)
    allocate(gderT_proj(nnod_filter,dimen), source=0.0_dp)
    allocate(rho_nusfs_gderT_proj(nnod_filter,dimen), source=0.0_dp)
    allocate(rho_T_proj(nnod_filter), source=0.0_dp)
    allocate(m_proj(nnod_filter,dimen), source=0.0_dp)
    allocate(m_T_proj(nnod_filter,dimen), source=0.0_dp)

    ! Take inner products of the quantities with the low-order basis:
    do qp = 1, nqp
      associate(proj_funcs => elem%quad_points%dg_funcs(:nnod_filter,qp))
        rho_proj = rho_proj &
            + elem%quot(qp) * proj_funcs * rho_at_qps(qp)
        nusfs_proj = nusfs_proj &
            + elem%quot(qp) * proj_funcs * nusfs_at_qps(qp)
        rho_T_proj = rho_T_proj &
            + elem%quot(qp) * proj_funcs * rho_at_qps(qp) * T_at_qps(qp)
        do i = 1, dimen
          gderT_proj(:,i) = gderT_proj(:,i) &
              + elem%quot(qp) * proj_funcs * gderT_at_qps(i,qp)
          rho_nusfs_gderT_proj(:,i) = rho_nusfs_gderT_proj(:,i) &
              + elem%quot(qp) * proj_funcs * rho_at_qps(qp) * nusfs_at_qps(qp) * gderT_at_qps(i,qp)
          m_proj(:,i) = m_proj(:,i) &
              + elem%quot(qp) * proj_funcs * m_at_qps(i,qp)
          m_T_proj(:,i) = m_T_proj(:,i) &
              + elem%quot(qp) * proj_funcs * m_at_qps(i,qp) * T_at_qps(qp)
        end do
      end associate
    end do

    ! Transform the projection into solution coefficients by solving for the mass matrix:
    call elem%solve_for_massmat(rho_proj)
    call elem%solve_for_massmat(nusfs_proj)
    call elem%solve_for_massmat(gderT_proj)
    call elem%solve_for_massmat(rho_nusfs_gderT_proj)
    call elem%solve_for_massmat(rho_T_proj)
    call elem%solve_for_massmat(m_proj)
    call elem%solve_for_massmat(m_T_proj)

    allocate(LM(pnt_set%np()), MM(pnt_set%np()))
    do p = 1, pnt_set%np()
      ! Evaluate the coarsened quantities at the points with the coarsened coefficients:
      associate(funcs_at_p => pnt_set%dg_funcs(:nnod_filter,p))
        rho_coarse             = dot_product(funcs_at_p,rho_proj)
        nusfs_coarse           = dot_product(funcs_at_p,nusfs_proj)
        gderT_coarse           = matmul(funcs_at_p,gderT_proj)
        rho_nusfs_gderT_coarse = matmul(funcs_at_p,rho_nusfs_gderT_proj)
        rho_T_coarse           = dot_product(funcs_at_p,rho_T_proj)
        m_coarse               = matmul(funcs_at_p,m_proj)
        m_T_coarse             = matmul(funcs_at_p,m_T_proj)
      end associate

      li = m_T_coarse - m_coarse * rho_T_coarse / rho_coarse
      mi = rho_nusfs_gderT_coarse - rho_coarse * nusfs_coarse * gderT_coarse
      LM(p) = dot_product(li,mi)
      MM(p) = dot_product(mi,mi)
    end do
    call assert_normal(LM,'dyn. turb. Pr model: LM not IEEE normal')
    call assert_normal(MM,'dyn. turb. Pr model: MM not IEEE normal')
  end subroutine least_squares_proj_LM_MM
end module les_dyn_turb_pr_mod

