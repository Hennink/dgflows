module bnd_t_periodic_mod
  use boundary_types, only: bnd_type
  use exceptions, only: error
  use f90_kind, only: dp
  use support, only: bool_i, false, true
  implicit none

  private
  public :: periodic_bnd_t

  type, extends(bnd_type) :: periodic_bnd_t
    logical :: is_primary
    integer :: dir
    integer :: couple_id

  contains
    procedure, nopass :: fixes_absolute_pressure => false
    procedure, nopass :: has_inflow => false
    procedure :: n_dot_velocity_at_qps
    procedure :: get_bc
    procedure :: read_specs_from_file
  end type periodic_bnd_t

contains
  function n_dot_velocity_at_qps(this,face,fluid_side,time)
    use mesh_objects, only: face_list

    class(periodic_bnd_t), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: fluid_side
    integer, intent(in) :: time
    real(dp) :: n_dot_velocity_at_qps(face%nqp)

    call error('not implemented for periodic boundary: n_dot_velocity_at_qps')
  end function n_dot_velocity_at_qps

  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types, only: bc_type

    class(periodic_bnd_t), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc

    call error('not implemented for periodic boundary: get_bc')
  end subroutine get_bc

  subroutine read_specs_from_file(this,unit_number)
    use assertions, only: assert
    use io_basics, only: read_line_as_label_eq_specs
    use run_data, only: mesh_dimen
    use string_manipulations, only: int2char

    class(periodic_bnd_t), intent(out) :: this
    integer, intent(in) :: unit_number

    character(:), allocatable :: dir_specs, is_primary_specs, couple_id_specs
    character(200) :: iomsg
    integer :: ios

    call read_line_as_label_eq_specs(unit_number,'is_primary',is_primary_specs)
    read(is_primary_specs,*,iostat=ios,iomsg=iomsg) this%is_primary
    call assert(ios==0,'PROBLEM INTERPRETING PERIODIC BND: ' // trim(iomsg))

    call read_line_as_label_eq_specs(unit_number, 'couple_id', couple_id_specs)
    read(couple_id_specs,*, iostat=ios,iomsg=iomsg) this%couple_id
    call assert(ios==0,'PROBLEM INTERPRETING PERIODIC BND: ' // trim(iomsg))

    if (this%is_primary) then
      call read_line_as_label_eq_specs(unit_number,'dir',dir_specs)
      read(dir_specs,*,iostat=ios,iomsg=iomsg) this%dir
      call assert(ios==0,'PROBLEM INTERPRETING PERIODIC BND: ' // trim(iomsg))
      call assert(1 <= this%dir .and. this%dir <= mesh_dimen,'bad dir: ' // int2char(this%dir))
    endif
  end subroutine read_specs_from_file
end module bnd_t_periodic_mod
