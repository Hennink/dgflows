submodule(local_elem) local_elem__old
  implicit none
  
  interface
    module pure subroutine old_quadrature_and_nqp(eltype,order,abss,weight,nqp)
      use f90_kind, only: dp
      integer, intent(in) :: eltype
      integer, intent(in) :: order
      real(dp), allocatable, dimension(:,:), intent(out) :: abss
      real(dp), allocatable, dimension(:), intent(out) :: weight
      integer, intent(out) :: nqp
    end subroutine old_quadrature_and_nqp
  end interface
  
contains
  module subroutine get_old_local_elem_functionality(functionality)
    type(local_elem_functionality), intent(out) :: functionality
    
    functionality%coord_in_domain => old_coord_in_domain
    functionality%get_dimen => old_get_dimen
    functionality%get_quadrature => old_get_quadrature
    functionality%local_dg_deriv => old_local_dg_deriv
    functionality%local_dg_functions => old_local_dg_functions
    functionality%local_dg_functions_single_point => old_local_dg_functions_single_point
    functionality%mutually_hierarchical_func_sets => old_mutually_hierarchical_func_sets
    functionality%no_dg_functions => old_no_dg_functions
    functionality%no_vertices => old_no_vertices
  end subroutine get_old_local_elem_functionality
  
  
  pure subroutine old_get_quadrature(eltype,order,abss,weights)
    use f90_kind, only: dp
    integer, intent(in) :: eltype
    integer, intent(in) :: order
    real(dp), allocatable, dimension(:,:), intent(out) :: abss
    real(dp), allocatable, dimension(:), intent(out) :: weights
    
    integer :: dummy
    
    call old_quadrature_and_nqp(eltype,order,abss,weights,dummy)
  end subroutine old_get_quadrature
  
  pure integer function old_no_dg_functions(eltype,max_order) result(no_func)
    integer, intent(in) :: eltype
    integer, intent(in) :: max_order

    select case (eltype)
      case(11,12,13)
        no_func = max_order+1
        
      case(23,26)
        no_func = 0
        if (max_order == 0) no_func = 1
        if (max_order >= 1) no_func = no_func + 3
        if (max_order >= 2) no_func = no_func + 3*(max_order-1)
        if (max_order >= 3) no_func = no_func + (max_order-1)*(max_order-2)/2

      case(24,28,29)
        no_func = (max_order+1)*(max_order+2)/2

      case(34,310)
        no_func = 0
        if (max_order == 0) no_func = 1
        if (max_order >= 1) no_func = no_func + 4
        if (max_order >= 2) no_func = no_func + 6*(max_order-1)
        if (max_order >= 3) no_func = no_func + 4*(max_order-1)*(max_order-2)/2
        if (max_order >= 4) no_func = no_func + (max_order-1)*(max_order-2)*(max_order-3)/6
        
      case(38,320,327)
        no_func = (max_order+1)*(max_order+2)*(max_order+3)/6
      
      case default
        error stop "unknown eltype: old_no_dg_functions"
    end select
  end function old_no_dg_functions
  
  subroutine old_local_dg_functions(eltype,order,local_coord,func,local_deriv,in_domain)
    use f90_kind, only: dp
    
    integer, intent(in) :: eltype
    integer, intent(in) :: order
    real(dp), intent(in) :: local_coord(:,:)
    real(dp), allocatable, optional, intent(out) :: func(:,:)
    real(dp), allocatable, optional, intent(out) :: local_deriv(:,:,:)
    logical, optional, intent(in) :: in_domain
    
    integer :: dimen
    integer :: no_points
    integer :: i
    integer :: no_functions
    real(dp), allocatable :: calc_func(:,:)
    real(dp), allocatable :: calc_local_deriv(:,:,:)
    
    dimen = size(local_coord,1)
    no_points = size(local_coord,2)
    no_functions = no_dg_functions(eltype,order)
    allocate(calc_func(no_functions,no_points))
    allocate(calc_local_deriv(dimen,no_functions,no_points))
    do i = 1, no_points
      call local_dg_functions_single_point(eltype,order,local_coord(:,i),calc_func(:,i),calc_local_deriv(:,:,i))
    enddo
    if (present(func)) call move_alloc(calc_func,func)
    if (present(local_deriv)) call move_alloc(calc_local_deriv,local_deriv)
  end subroutine old_local_dg_functions
  
  pure subroutine old_local_dg_functions_single_point(eltype,order,local_coord,func,local_deriv)
    !---------------------------------------------------------------------
    ! This function returns the values of the shape functions at the
    ! local coordinate.
    ! These shape functions constitute the functions up to order 'order'.
    ! This is much cheaper than using the full tensor product for the
    ! quad and hex elements while convergence is similar.
    !---------------------------------------------------------------------
    use f90_kind, only: dp
    
    integer, intent(in) :: eltype
    integer, intent(in) :: order
    real(dp), intent(in) :: local_coord(:)
    real(dp), intent(out) :: func(:)
    real(dp), intent(out) :: local_deriv(:,:)
    
    integer :: p
    integer :: p1
    integer :: p2
    integer :: p3
    integer :: p_xi1
    integer :: p_xi2
    integer :: p_xi3
    integer :: p_xi2plusxi3
    integer :: counter
    real(dp) :: lambda1
    real(dp) :: lambda2
    real(dp) :: lambda3
    real(dp) :: lambda4
    real(dp) :: dlambda1_dxi1
    real(dp) :: dlambda1_dxi2
    real(dp) :: dlambda1_dxi3
    real(dp) :: dlambda2_dxi1
    real(dp) :: dlambda2_dxi2
    real(dp) :: dlambda2_dxi3
    real(dp) :: dlambda3_dxi1
    real(dp) :: dlambda3_dxi2
    real(dp) :: dlambda3_dxi3
    real(dp) :: dlambda4_dxi1
    real(dp) :: dlambda4_dxi2
    real(dp) :: dlambda4_dxi3
    
    associate(xi => local_coord)
    local_deriv = 0.0_dp
    
    select case (eltype)

    ! 1D line element

    case(11,12,13)

      if (order>=0) then
        do p_xi1=0,order
          func(p_xi1+1)    = fun_1d(p_xi1,xi(1),.false.)
          local_deriv(1,p_xi1+1) = fun_1d(p_xi1,xi(1),.true.)
        enddo
      else
        error stop "ERROR in old_local_dg_functions_single_point"
      endif

    ! 2D quad element

    case(24,28,29)

      if (order>=0) then
        counter = 1
        do p=0,order
          do p_xi1=0,p
            p_xi2 = p - p_xi1
            func(counter)    = fun_1d(p_xi1,xi(1),.false.)*fun_1d(p_xi2,xi(2),.false.)
            local_deriv(1,counter) = fun_1d(p_xi1,xi(1),.true.) *fun_1d(p_xi2,xi(2),.false.)
            local_deriv(2,counter) = fun_1d(p_xi1,xi(1),.false.)*fun_1d(p_xi2,xi(2),.true.)
            counter = counter + 1
          enddo
        enddo
      else
        error stop "old_local_dg_functions_single_point: unknown eltype/order"
      endif

    ! 2D triangle element

    case(23,26)

      if (order>=0) then
        if (order==0) then
          func(1) = 1.0_dp
          local_deriv(1,1) = 0.0_dp
          local_deriv(2,1) = 0.0_dp
        else
          lambda1 = xi(1)
          lambda2 = xi(2)
          lambda3 = 1.0_dp - xi(1) - xi(2)

          dlambda1_dxi1 = +1.0_dp
          dlambda1_dxi2 = +0.0_dp

          dlambda2_dxi1 = +0.0_dp
          dlambda2_dxi2 = +1.0_dp

          dlambda3_dxi1 = -1.0_dp
          dlambda3_dxi2 = -1.0_dp

    ! vertex functions

          func(1) = lambda1
          local_deriv(1,1) = dlambda1_dxi1
          local_deriv(2,1) = dlambda1_dxi2

          func(2) = lambda2
          local_deriv(1,2) = dlambda2_dxi1
          local_deriv(2,2) = dlambda2_dxi2

          func(3) = lambda3
          local_deriv(1,3) = dlambda3_dxi1
          local_deriv(2,3) = dlambda3_dxi2

          counter = 4

          do p=2,order

    ! edge functions

            func(counter) = lambda2*lambda3*phi(p-2,lambda3-lambda2,.false.)
            local_deriv(1,counter) = lambda2*lambda3*phi(p-2,lambda3-lambda2,.true.)*(dlambda3_dxi1-dlambda2_dxi1) + &
                              phi(p-2,lambda3-lambda2,.false.)*(lambda2*dlambda3_dxi1 + dlambda2_dxi1*lambda3)
            local_deriv(2,counter) = lambda2*lambda3*phi(p-2,lambda3-lambda2,.true.)*(dlambda3_dxi2-dlambda2_dxi2) + &
                              phi(p-2,lambda3-lambda2,.false.)*(lambda2*dlambda3_dxi2 + dlambda2_dxi2*lambda3)
            counter = counter+1

            func(counter) = lambda3*lambda1*phi(p-2,lambda1-lambda3,.false.)
            local_deriv(1,counter) = lambda3*lambda1*phi(p-2,lambda1-lambda3,.true.)*(dlambda1_dxi1-dlambda3_dxi1) + &
                              phi(p-2,lambda1-lambda3,.false.)*(lambda3*dlambda1_dxi1 + dlambda3_dxi1*lambda1)
            local_deriv(2,counter) = lambda3*lambda1*phi(p-2,lambda1-lambda3,.true.)*(dlambda1_dxi2-dlambda3_dxi2) + &
                              phi(p-2,lambda1-lambda3,.false.)*(lambda3*dlambda1_dxi2 + dlambda3_dxi2*lambda1)
            counter = counter+1

            func(counter) = lambda1*lambda2*phi(p-2,lambda2-lambda1,.false.)
            local_deriv(1,counter) = lambda1*lambda2*phi(p-2,lambda2-lambda1,.true.)*(dlambda2_dxi1-dlambda1_dxi1) + &
                              phi(p-2,lambda2-lambda1,.false.)*(lambda1*dlambda2_dxi1 + dlambda1_dxi1*lambda2)
            local_deriv(2,counter) = lambda1*lambda2*phi(p-2,lambda2-lambda1,.true.)*(dlambda2_dxi2-dlambda1_dxi2) + &
                              phi(p-2,lambda2-lambda1,.false.)*(lambda1*dlambda2_dxi2 + dlambda1_dxi2*lambda2)
            counter = counter+1

    ! bubble functions

            do p1=1,p-1-1
              do p2=p-1-p1,p-1-p1
                func(counter) = lambda1*lambda2*lambda3*phi(p1-1,lambda3-lambda2,.false.)*phi(p2-1,lambda2-lambda1,.false.)
                local_deriv(1,counter) =     lambda1*lambda2*lambda3*phi(p1-1,lambda3-lambda2,.true.) &
                                    * (dlambda3_dxi1-dlambda2_dxi1)*phi(p2-1,lambda2-lambda1,.false.) &
                                  + lambda1*lambda2*lambda3*phi(p1-1,lambda3-lambda2,.false.)* &
                                    phi(p2-1,lambda2-lambda1,.true.) * (dlambda2_dxi1-dlambda1_dxi1) + &
                                  (dlambda1_dxi1*lambda2*lambda3 + lambda1*dlambda2_dxi1*lambda3 + lambda1*lambda2*dlambda3_dxi1) &
                                  *phi(p1-1,lambda3-lambda2,.false.)*phi(p2-1,lambda2-lambda1,.false.)
                local_deriv(2,counter) = lambda1*lambda2*lambda3*phi(p1-1,lambda3-lambda2,.true.)*(dlambda3_dxi2-dlambda2_dxi2) * & 
                                  phi(p2-1,lambda2-lambda1,.false.) + &
                                  lambda1*lambda2*lambda3*phi(p1-1,lambda3-lambda2,.false.) &
                                  * phi(p2-1,lambda2-lambda1,.true.) &
                                  * (dlambda2_dxi2-dlambda1_dxi2) +&
                                  (dlambda1_dxi2*lambda2*lambda3 + lambda1*dlambda2_dxi2*lambda3 + lambda1*lambda2*dlambda3_dxi2) &
                                  * phi(p1-1,lambda3-lambda2,.false.)*phi(p2-1,lambda2-lambda1,.false.)
                counter = counter+1
              enddo
            enddo
          enddo

        endif
      else
        error stop "old_local_dg_functions_single_point: unknown eltype/order"
      endif

    ! 3D hex element

    case(38,320,327)

      if (order>=0) then
        counter = 1
        do p=0,order
          do p_xi1=0,p
            p_xi2plusxi3 = p - p_xi1
            do p_xi2=0,p_xi2plusxi3
              p_xi3 = p_xi2plusxi3 - p_xi2
              func(counter)    = fun_1d(p_xi1,xi(1),.false.)*fun_1d(p_xi2,xi(2),.false.)*fun_1d(p_xi3,xi(3),.false.)
    !          print *,counter,p_xi1,p_xi2,p_xi3
              local_deriv(1,counter) = fun_1d(p_xi1,xi(1),.true.) *fun_1d(p_xi2,xi(2),.false.)*fun_1d(p_xi3,xi(3),.false.)
              local_deriv(2,counter) = fun_1d(p_xi1,xi(1),.false.)*fun_1d(p_xi2,xi(2),.true.) *fun_1d(p_xi3,xi(3),.false.)
              local_deriv(3,counter) = fun_1d(p_xi1,xi(1),.false.)*fun_1d(p_xi2,xi(2),.false.)*fun_1d(p_xi3,xi(3),.true.)
              counter = counter + 1
            enddo
          enddo
        enddo
      else
        error stop "old_local_dg_functions_single_point: unknown eltype/order"
      endif

    ! 3D tet element

    case(34,310)

      if (order>=0) then
        if (order==0) then
          func(1) = 1.0_dp
          local_deriv(1,1) = 0.0_dp
          local_deriv(2,1) = 0.0_dp
          local_deriv(3,1) = 0.0_dp
        else
          lambda1 = xi(1)
          lambda2 = xi(2)
          lambda3 = xi(3)
          lambda4 = 1.0_dp - xi(1) - xi(2) - xi(3)

          dlambda1_dxi1 = +1.0_dp
          dlambda1_dxi2 = +0.0_dp
          dlambda1_dxi3 = +0.0_dp

          dlambda2_dxi1 = +0.0_dp
          dlambda2_dxi2 = +1.0_dp
          dlambda2_dxi3 = +0.0_dp

          dlambda3_dxi1 = +0.0_dp
          dlambda3_dxi2 = +0.0_dp
          dlambda3_dxi3 = +1.0_dp

          dlambda4_dxi1 = -1.0_dp
          dlambda4_dxi2 = -1.0_dp
          dlambda4_dxi3 = -1.0_dp

    ! vertex functions

          func(1) = lambda1
          local_deriv(1,1) = dlambda1_dxi1
          local_deriv(2,1) = dlambda1_dxi2
          local_deriv(3,1) = dlambda1_dxi3

          func(2) = lambda2
          local_deriv(1,2) = dlambda2_dxi1
          local_deriv(2,2) = dlambda2_dxi2
          local_deriv(3,2) = dlambda2_dxi3

          func(3) = lambda3
          local_deriv(1,3) = dlambda3_dxi1
          local_deriv(2,3) = dlambda3_dxi2
          local_deriv(3,3) = dlambda3_dxi3

          func(4) = lambda4
          local_deriv(1,4) = dlambda4_dxi1
          local_deriv(2,4) = dlambda4_dxi2
          local_deriv(3,4) = dlambda4_dxi3

          counter = 5

          do p=2,order

    ! edge functions

    ! 1

            func(counter) = lambda3*lambda4*phi(p-2,lambda3-lambda4,.false.)
            local_deriv(1,counter) = lambda3*lambda4*phi(p-2,lambda3-lambda4,.true.)*(dlambda3_dxi1-dlambda4_dxi1) + &
                              phi(p-2,lambda3-lambda4,.false.)*(lambda3*dlambda4_dxi1 + dlambda3_dxi1*lambda4)
            local_deriv(2,counter) = lambda3*lambda4*phi(p-2,lambda3-lambda4,.true.)*(dlambda3_dxi2-dlambda4_dxi2) + &
                              phi(p-2,lambda3-lambda4,.false.)*(lambda3*dlambda4_dxi2 + dlambda3_dxi2*lambda4)
            local_deriv(3,counter) = lambda3*lambda4*phi(p-2,lambda3-lambda4,.true.)*(dlambda3_dxi3-dlambda4_dxi3) + &
                              phi(p-2,lambda3-lambda4,.false.)*(lambda3*dlambda4_dxi3 + dlambda3_dxi3*lambda4)
            counter = counter+1

    ! 2

            func(counter) = lambda1*lambda4*phi(p-2,lambda1-lambda4,.false.)
            local_deriv(1,counter) = lambda1*lambda4*phi(p-2,lambda1-lambda4,.true.)*(dlambda1_dxi1-dlambda4_dxi1) + &
                              phi(p-2,lambda1-lambda4,.false.)*(lambda1*dlambda4_dxi1 + dlambda1_dxi1*lambda4)
            local_deriv(2,counter) = lambda1*lambda4*phi(p-2,lambda1-lambda4,.true.)*(dlambda1_dxi2-dlambda4_dxi2) + &
                              phi(p-2,lambda1-lambda4,.false.)*(lambda1*dlambda4_dxi2 + dlambda1_dxi2*lambda4)
            local_deriv(3,counter) = lambda1*lambda4*phi(p-2,lambda1-lambda4,.true.)*(dlambda1_dxi3-dlambda4_dxi3) + &
                              phi(p-2,lambda1-lambda4,.false.)*(lambda1*dlambda4_dxi3 + dlambda1_dxi3*lambda4)
            counter = counter+1

    ! 3

            func(counter) = lambda2*lambda4*phi(p-2,lambda2-lambda4,.false.)
            local_deriv(1,counter) = lambda2*lambda4*phi(p-2,lambda2-lambda4,.true.)*(dlambda2_dxi1-dlambda4_dxi1) + &
                              phi(p-2,lambda2-lambda4,.false.)*(lambda2*dlambda4_dxi1 + dlambda2_dxi1*lambda4)
            local_deriv(2,counter) = lambda2*lambda4*phi(p-2,lambda2-lambda4,.true.)*(dlambda2_dxi2-dlambda4_dxi2) + &
                              phi(p-2,lambda2-lambda4,.false.)*(lambda2*dlambda4_dxi2 + dlambda2_dxi2*lambda4)
            local_deriv(3,counter) = lambda2*lambda4*phi(p-2,lambda2-lambda4,.true.)*(dlambda2_dxi3-dlambda4_dxi3) + &
                              phi(p-2,lambda2-lambda4,.false.)*(lambda2*dlambda4_dxi3 + dlambda2_dxi3*lambda4)
            counter = counter+1

    ! 4

            func(counter) = lambda2*lambda3*phi(p-2,lambda2-lambda3,.false.)
            local_deriv(1,counter) = lambda2*lambda3*phi(p-2,lambda2-lambda3,.true.)*(dlambda2_dxi1-dlambda3_dxi1) + &
                              phi(p-2,lambda2-lambda3,.false.)*(lambda2*dlambda3_dxi1 + dlambda2_dxi1*lambda3)
            local_deriv(2,counter) = lambda2*lambda3*phi(p-2,lambda2-lambda3,.true.)*(dlambda2_dxi2-dlambda3_dxi2) + &
                              phi(p-2,lambda2-lambda3,.false.)*(lambda2*dlambda3_dxi2 + dlambda2_dxi2*lambda3)
            local_deriv(3,counter) = lambda2*lambda3*phi(p-2,lambda2-lambda3,.true.)*(dlambda2_dxi3-dlambda3_dxi3) + &
                              phi(p-2,lambda2-lambda3,.false.)*(lambda2*dlambda3_dxi3 + dlambda2_dxi3*lambda3)
            counter = counter+1

    ! 5

            func(counter) = lambda1*lambda3*phi(p-2,lambda1-lambda3,.false.)
            local_deriv(1,counter) = lambda1*lambda3*phi(p-2,lambda1-lambda3,.true.)*(dlambda1_dxi1-dlambda3_dxi1) + &
                              phi(p-2,lambda1-lambda3,.false.)*(lambda1*dlambda3_dxi1 + dlambda1_dxi1*lambda3)
            local_deriv(2,counter) = lambda1*lambda3*phi(p-2,lambda1-lambda3,.true.)*(dlambda1_dxi2-dlambda3_dxi2) + &
                              phi(p-2,lambda1-lambda3,.false.)*(lambda1*dlambda3_dxi2 + dlambda1_dxi2*lambda3)
            local_deriv(3,counter) = lambda1*lambda3*phi(p-2,lambda1-lambda3,.true.)*(dlambda1_dxi3-dlambda3_dxi3) + &
                              phi(p-2,lambda1-lambda3,.false.)*(lambda1*dlambda3_dxi3 + dlambda1_dxi3*lambda3)
            counter = counter+1

    ! 6

            func(counter) = lambda1*lambda2*phi(p-2,lambda1-lambda2,.false.)
            local_deriv(1,counter) = lambda1*lambda2*phi(p-2,lambda1-lambda2,.true.)*(dlambda1_dxi1-dlambda2_dxi1) + &
                              phi(p-2,lambda1-lambda2,.false.)*(lambda1*dlambda2_dxi1 + dlambda1_dxi1*lambda2)
            local_deriv(2,counter) = lambda1*lambda2*phi(p-2,lambda1-lambda2,.true.)*(dlambda1_dxi2-dlambda2_dxi2) + &
                              phi(p-2,lambda1-lambda2,.false.)*(lambda1*dlambda2_dxi2 + dlambda1_dxi2*lambda2)
            local_deriv(3,counter) = lambda1*lambda2*phi(p-2,lambda1-lambda2,.true.)*(dlambda1_dxi3-dlambda2_dxi3) + &
                              phi(p-2,lambda1-lambda2,.false.)*(lambda1*dlambda2_dxi3 + dlambda1_dxi3*lambda2)
            counter = counter+1

    ! face functions

            do p1=1,p-1-1
              do p2=p-1-p1,p-1-p1

    ! 1

                func(counter) = lambda2*lambda3*lambda4*phi(p1-1,lambda3-lambda2,.false.)*phi(p2-1,lambda2-lambda4,.false.)
                local_deriv(1,counter) = lambda2*lambda3*lambda4*phi(p1-1,lambda3-lambda2,.true.) & 
                                  *(dlambda3_dxi1-dlambda2_dxi1)*phi(p2-1,lambda2-lambda4,.false.) + &
                                  lambda2*lambda3*lambda4*phi(p1-1,lambda3-lambda2,.false.) * &
                                  phi(p2-1,lambda2-lambda4,.true.)*(dlambda2_dxi1-dlambda4_dxi1) +&
                                  (dlambda2_dxi1*lambda3*lambda4 + lambda2*dlambda3_dxi1*lambda4 + lambda2*lambda3*dlambda4_dxi1) &
                                  * phi(p1-1,lambda3-lambda2,.false.)*phi(p2-1,lambda2-lambda4,.false.)
                local_deriv(2,counter) = lambda2*lambda3*lambda4*phi(p1-1,lambda3-lambda2,.true.)*(dlambda3_dxi2-dlambda2_dxi2) * &
                                  phi(p2-1,lambda2-lambda4,.false.) +&
                                  lambda2*lambda3*lambda4*phi(p1-1,lambda3-lambda2,.false.) &
                                  * phi(p2-1,lambda2-lambda4,.true.)*(dlambda2_dxi2-dlambda4_dxi2) +&
                                  (dlambda2_dxi2*lambda3*lambda4 + lambda2*dlambda3_dxi2*lambda4 + lambda2*lambda3*dlambda4_dxi2) &
                                  * phi(p1-1,lambda3-lambda2,.false.)*phi(p2-1,lambda2-lambda4,.false.)
                local_deriv(3,counter) = lambda2*lambda3*lambda4*phi(p1-1,lambda3-lambda2,.true.)*(dlambda3_dxi3-dlambda2_dxi3) &
                                  * phi(p2-1,lambda2-lambda4,.false.) +&
                                  lambda2*lambda3*lambda4*phi(p1-1,lambda3-lambda2,.false.) &
                                  * phi(p2-1,lambda2-lambda4,.true.)*(dlambda2_dxi3-dlambda4_dxi3) +&
                                  (dlambda2_dxi3*lambda3*lambda4+lambda2*dlambda3_dxi3*lambda4+lambda2*lambda3*dlambda4_dxi3) &
                                  * phi(p1-1,lambda3-lambda2,.false.)*phi(p2-1,lambda2-lambda4,.false.)
                counter = counter+1

    ! 2

                func(counter) = lambda1*lambda3*lambda4*phi(p1-1,lambda3-lambda1,.false.)*phi(p2-1,lambda1-lambda4,.false.)
                local_deriv(1,counter) = lambda1*lambda3*lambda4*phi(p1-1,lambda3-lambda1,.true.)*(dlambda3_dxi1-dlambda1_dxi1) &
                                  * phi(p2-1,lambda1-lambda4,.false.) +&
                                  lambda1*lambda3*lambda4*phi(p1-1,lambda3-lambda1,.false.) &
                                  * phi(p2-1,lambda1-lambda4,.true.)*(dlambda1_dxi1-dlambda4_dxi1) +&
                                  (dlambda1_dxi1*lambda3*lambda4 + lambda1*dlambda3_dxi1*lambda4 + lambda1*lambda3*dlambda4_dxi1) &
                                  *phi(p1-1,lambda3-lambda1,.false.)*phi(p2-1,lambda1-lambda4,.false.)
                local_deriv(2,counter) = lambda1*lambda3*lambda4*phi(p1-1,lambda3-lambda1,.true.)*(dlambda3_dxi2-dlambda1_dxi2) &
                                  * phi(p2-1,lambda1-lambda4,.false.) +&
                                  lambda1*lambda3*lambda4*phi(p1-1,lambda3-lambda1,.false.) &
                                  * phi(p2-1,lambda1-lambda4,.true.)*(dlambda1_dxi2-dlambda4_dxi2) +&
                                  (dlambda1_dxi2*lambda3*lambda4 + lambda1*dlambda3_dxi2*lambda4 + lambda1*lambda3*dlambda4_dxi2) &
                                  * phi(p1-1,lambda3-lambda1,.false.)*phi(p2-1,lambda1-lambda4,.false.)
                local_deriv(3,counter) = lambda1*lambda3*lambda4*phi(p1-1,lambda3-lambda1,.true.)*(dlambda3_dxi3-dlambda1_dxi3) &
                                  * phi(p2-1,lambda1-lambda4,.false.) +&
                                  lambda1*lambda3*lambda4*phi(p1-1,lambda3-lambda1,.false.) &
                                  * phi(p2-1,lambda1-lambda4,.true.)*(dlambda1_dxi3-dlambda4_dxi3) +&
                                  (dlambda1_dxi3*lambda3*lambda4 + lambda1*dlambda3_dxi3*lambda4 + lambda1*lambda3*dlambda4_dxi3) &
                                  * phi(p1-1,lambda3-lambda1,.false.)*phi(p2-1,lambda1-lambda4,.false.)
                counter = counter+1

    ! 3
                func(counter) = lambda1*lambda2*lambda4*phi(p1-1,lambda2-lambda1,.false.)*phi(p2-1,lambda1-lambda4,.false.)
                local_deriv(1,counter) = lambda1*lambda2*lambda4*phi(p1-1,lambda2-lambda1,.true.)*(dlambda2_dxi1-dlambda1_dxi1) & 
                                  * phi(p2-1,lambda1-lambda4,.false.) +&
                                  lambda1*lambda2*lambda4*phi(p1-1,lambda2-lambda1,.false.) &
                                  * phi(p2-1,lambda1-lambda4,.true.)*(dlambda1_dxi1-dlambda4_dxi1) +&
                                  (dlambda1_dxi1*lambda2*lambda4 + lambda1*dlambda2_dxi1*lambda4 + lambda1*lambda2*dlambda4_dxi1) &
                                  * phi(p1-1,lambda2-lambda1,.false.)*phi(p2-1,lambda1-lambda4,.false.)
                local_deriv(2,counter) = lambda1*lambda2*lambda4*phi(p1-1,lambda2-lambda1,.true.)*(dlambda2_dxi2-dlambda1_dxi2) &
                                  * phi(p2-1,lambda1-lambda4,.false.) +&
                                  lambda1*lambda2*lambda4*phi(p1-1,lambda2-lambda1,.false.) &
                                  * phi(p2-1,lambda1-lambda4,.true.)*(dlambda1_dxi2-dlambda4_dxi2) +&
                                  (dlambda1_dxi2*lambda2*lambda4 + lambda1*dlambda2_dxi2*lambda4 + lambda1*lambda2*dlambda4_dxi2) &
                                  * phi(p1-1,lambda2-lambda1,.false.)*phi(p2-1,lambda1-lambda4,.false.)
                local_deriv(3,counter) = lambda1*lambda2*lambda4*phi(p1-1,lambda2-lambda1,.true.)*(dlambda2_dxi3-dlambda1_dxi3) &
                                  * phi(p2-1,lambda1-lambda4,.false.) +&
                                  lambda1*lambda2*lambda4*phi(p1-1,lambda2-lambda1,.false.) &
                                  * phi(p2-1,lambda1-lambda4,.true.)*(dlambda1_dxi3-dlambda4_dxi3) + &
                                  (dlambda1_dxi3*lambda2*lambda4 + lambda1*dlambda2_dxi3*lambda4 + lambda1*lambda2*dlambda4_dxi3) &
                                  * phi(p1-1,lambda2-lambda1,.false.)*phi(p2-1,lambda1-lambda4,.false.)
                counter = counter+1
    ! 4
                func(counter) = lambda1*lambda2*lambda3*phi(p1-1,lambda2-lambda1,.false.)*phi(p2-1,lambda1-lambda3,.false.)
                local_deriv(1,counter) = lambda1*lambda2*lambda3*phi(p1-1,lambda2-lambda1,.true.)*(dlambda2_dxi1-dlambda1_dxi1) &
                                  * phi(p2-1,lambda1-lambda3,.false.) +&
                                  lambda1*lambda2*lambda3*phi(p1-1,lambda2-lambda1,.false.) &
                                  * phi(p2-1,lambda1-lambda3,.true.)*(dlambda1_dxi1-dlambda3_dxi1) +&
                                  (dlambda1_dxi1*lambda2*lambda3 + lambda1*dlambda2_dxi1*lambda3 + lambda1*lambda2*dlambda3_dxi1) &
                                  * phi(p1-1,lambda2-lambda1,.false.)*phi(p2-1,lambda1-lambda3,.false.)
                local_deriv(2,counter) = lambda1*lambda2*lambda3*phi(p1-1,lambda2-lambda1,.true.)*(dlambda2_dxi2-dlambda1_dxi2) &
                                  * phi(p2-1,lambda1-lambda3,.false.) +&
                                  lambda1*lambda2*lambda3*phi(p1-1,lambda2-lambda1,.false.) &
                                  * phi(p2-1,lambda1-lambda3,.true.)*(dlambda1_dxi2-dlambda3_dxi2) +&
                                  (dlambda1_dxi2*lambda2*lambda3 + lambda1*dlambda2_dxi2*lambda3 + lambda1*lambda2*dlambda3_dxi2) &
                                  * phi(p1-1,lambda2-lambda1,.false.)*phi(p2-1,lambda1-lambda3,.false.)
                local_deriv(3,counter) = lambda1*lambda2*lambda3*phi(p1-1,lambda2-lambda1,.true.)*(dlambda2_dxi3-dlambda1_dxi3) &
                                  * phi(p2-1,lambda1-lambda3,.false.) +&
                                  lambda1*lambda2*lambda3*phi(p1-1,lambda2-lambda1,.false.) & 
                                  *phi(p2-1,lambda1-lambda3,.true.)*(dlambda1_dxi3-dlambda3_dxi3) +&
                                  (dlambda1_dxi3*lambda2*lambda3 + lambda1*dlambda2_dxi3*lambda3 + lambda1*lambda2*dlambda3_dxi3) &
                                  * phi(p1-1,lambda2-lambda1,.false.)*phi(p2-1,lambda1-lambda3,.false.)
                counter = counter+1
              enddo
            enddo

    ! bubble functions

            do p1=1,p-1-1-1
            do p2=1,p-1-p1-1
            do p3=p-1-p1-p2,p-1-p1-p2
              func(counter) = lambda1*lambda2*lambda3*lambda4*phi(p1-1,lambda1-lambda2,.false.) &
                             * phi(p2-1,lambda3-lambda2,.false.)*phi(p3-1,lambda4-lambda2,.false.)
              local_deriv(1,counter) = lambda1*lambda2*lambda3*lambda4*phi(p1-1,lambda1-lambda2,.true.) &
                                * (dlambda1_dxi1-dlambda2_dxi1)*phi(p2-1,lambda3-lambda2,.false.) &
                                * phi(p3-1,lambda4-lambda2,.false.) +&
                                lambda1*lambda2*lambda3*lambda4*phi(p1-1,lambda1-lambda2,.false.) &
                                * phi(p2-1,lambda3-lambda2,.true.)*(dlambda3_dxi1-dlambda2_dxi1) &
                                * phi(p3-1,lambda4-lambda2,.false.) +&
                                lambda1*lambda2*lambda3*lambda4*phi(p1-1,lambda1-lambda2,.false.) &
                                * phi(p2-1,lambda3-lambda2,.false.)*phi(p3-1,lambda4-lambda2,.true.) &
                                * (dlambda4_dxi1-dlambda2_dxi1) +&
                                (dlambda1_dxi1*lambda2*lambda3*lambda4 + lambda1*dlambda2_dxi1*lambda3*lambda4 + &
                                 lambda1*lambda2*dlambda3_dxi1*lambda4 + lambda1*lambda2*lambda3*dlambda4_dxi1)*&
                                phi(p1-1,lambda1-lambda2,.false.) * &
                                phi(p2-1,lambda3-lambda2,.false.) * &
                                phi(p3-1,lambda4-lambda2,.false.)
              local_deriv(2,counter) = lambda1*lambda2*lambda3*lambda4*phi(p1-1,lambda1-lambda2,.true.) &
                                * (dlambda1_dxi2-dlambda2_dxi2) &
                                * phi(p2-1,lambda3-lambda2,.false.)*phi(p3-1,lambda4-lambda2,.false.) +&
                                lambda1*lambda2*lambda3*lambda4*phi(p1-1,lambda1-lambda2,.false.) &
                                * phi(p2-1,lambda3-lambda2,.true.)*(dlambda3_dxi2-dlambda2_dxi2) &
                                * phi(p3-1,lambda4-lambda2,.false.) +&
                                lambda1*lambda2*lambda3*lambda4*phi(p1-1,lambda1-lambda2,.false.) &
                                * phi(p2-1,lambda3-lambda2,.false.)*phi(p3-1,lambda4-lambda2,.true.) &
                                * (dlambda4_dxi2-dlambda2_dxi2) +&
                                (  dlambda1_dxi2*lambda2*lambda3*lambda4 + lambda1*dlambda2_dxi2*lambda3*lambda4 &
                                 + lambda1*lambda2*dlambda3_dxi2*lambda4 + lambda1*lambda2*lambda3*dlambda4_dxi2) * &
                                phi(p1-1,lambda1-lambda2,.false.)*&
                                phi(p2-1,lambda3-lambda2,.false.)*&
                                phi(p3-1,lambda4-lambda2,.false.)
              local_deriv(3,counter) = lambda1*lambda2*lambda3*lambda4*phi(p1-1,lambda1-lambda2,.true.) &
                                * (dlambda1_dxi3-dlambda2_dxi3) &
                                *phi(p2-1,lambda3-lambda2,.false.)*phi(p3-1,lambda4-lambda2,.false.) +&
                                lambda1*lambda2*lambda3*lambda4*phi(p1-1,lambda1-lambda2,.false.) &
                                * phi(p2-1,lambda3-lambda2,.true.)*(dlambda3_dxi3-dlambda2_dxi3) &
                                * phi(p3-1,lambda4-lambda2,.false.) +&
                                lambda1*lambda2*lambda3*lambda4*phi(p1-1,lambda1-lambda2,.false.) &
                                * phi(p2-1,lambda3-lambda2,.false.)*phi(p3-1,lambda4-lambda2,.true.) &
                                * (dlambda4_dxi3-dlambda2_dxi3) +&
                                ( dlambda1_dxi3*lambda2*lambda3*lambda4 + lambda1*dlambda2_dxi3*lambda3*lambda4 &
                                  + lambda1*lambda2*dlambda3_dxi3*lambda4 + lambda1*lambda2*lambda3*dlambda4_dxi3)*&
                                phi(p1-1,lambda1-lambda2,.false.)*&
                                phi(p2-1,lambda3-lambda2,.false.)*&
                                phi(p3-1,lambda4-lambda2,.false.)
              counter = counter+1
            enddo
            enddo
            enddo

          enddo

        endif
      else
        error stop "old_local_dg_functions_single_point: unknown eltype/order"
      endif

    case default
      error stop "old_local_dg_functions_single_point: unknown eltype/order"

    end select
    end associate
  end subroutine old_local_dg_functions_single_point
  
  subroutine old_local_dg_deriv(eltype,order,local_coord,local_deriv)
    use f90_kind, only: dp
    integer, intent(in) :: eltype
    integer, intent(in) :: order
    real(dp), intent(in) :: local_coord(:,:)
    real(dp), allocatable, intent(out) :: local_deriv(:,:,:)
    
    call local_dg_functions(eltype,order,local_coord,local_deriv=local_deriv)
  end subroutine old_local_dg_deriv
  
  pure logical function old_mutually_hierarchical_func_sets(eltype,order_1,order_2) result(is_hier)
    use mesh_objects, only: get_1st_order_eltype
    
    integer, intent(in) :: eltype
    integer, intent(in) :: order_1, order_2
    
    logical :: triangle
    logical :: orders_zero_and_one
    
    triangle = get_1st_order_eltype(eltype) == 23
    orders_zero_and_one =      (order_1 == 0 .and. order_2 == 1) &
                          .or. (order_1 == 1 .and. order_2 == 0)
    is_hier = .not. (triangle .and. orders_zero_and_one)
  end function old_mutually_hierarchical_func_sets
  
  pure logical function old_coord_in_domain(eltype,coord)
    use f90_kind, only: dp
    use mesh_objects, only: get_1st_order_eltype
    
    integer, intent(in) :: eltype
    real(dp), dimension(:), intent(in) :: coord
    
    real(dp), parameter :: tol = 1.0e-13_dp
    
    associate(xi => coord)
      select case(get_1st_order_eltype(eltype))
        case(12)
          ! 1D element
          old_coord_in_domain = abs(xi(1))<= 1.0_dp + tol
          
        case(23)
          ! 2D triangular element
          old_coord_in_domain = xi(1)>=0.0_dp - tol .and. &
                                xi(2)>=0.0_dp - tol .and. &
                                xi(1)+xi(2)<=1.0_dp + tol
          
        case(24)
          ! 2D quad element
          old_coord_in_domain = abs(xi(1))<=1.0_dp + tol .and. &
                                abs(xi(2))<=1.0_dp + tol
          
        case(34)
          ! 3D tet element
          old_coord_in_domain = xi(1)>=0.0_dp - tol .and. &
                                xi(2)>=0.0_dp - tol .and. &
                                xi(3)>=0.0_dp - tol .and. &
                                xi(1)+xi(2)+xi(3)<=1.0_dp + tol
          
        case(38)
          ! 3D hex element
          old_coord_in_domain = abs(xi(1))<=1.0_dp + tol .and. &
                                abs(xi(2))<=1.0_dp + tol .and. &
                                abs(xi(3))<=1.0_dp + tol
          
        case default
          error stop "old_coord_in_domain: unknown 1st order eltype"
      end select
    end associate
  end function old_coord_in_domain
  
  pure integer function old_get_dimen(eltype)
    use mesh_objects, only: get_1st_order_eltype
    
    integer, intent(in) :: eltype
    
    select case(get_1st_order_eltype(eltype))
      case(12); old_get_dimen = 1
      case(23,24); old_get_dimen = 2
      case(34,38); old_get_dimen = 3
      case default; error stop "old_get_dimen: unknown eltype"
    end select
  end function old_get_dimen
  
  pure integer function old_no_vertices(eltype) result(no_vertices)
    integer, intent(in) :: eltype
    
    select case(eltype)
      case(12)
        no_vertices = 2
      case(13)
        no_vertices = 3
      case(23)
        no_vertices = 3
      case(24)
        no_vertices = 4
      case(26)
        no_vertices = 6
      case(28)
        no_vertices = 8
      case(29)
        no_vertices = 9
      case(34)
        no_vertices = 4
      case(38)
        no_vertices = 8
      case(310)
        no_vertices = 10
      case(320)
        no_vertices = 20
      case(327)
        no_vertices = 27
      case default
        error stop "old_no_vertices: unknown eltype"
    end select
  end function old_no_vertices
  
  module pure subroutine nodal_functions(eltype,local_coord,func,local_deriv,in_domain)
    use f90_kind, only: dp
    
    integer, intent(in) :: eltype
    real(dp), intent(in) :: local_coord(:,:)
    real(dp), allocatable, optional, intent(out) :: func(:,:)
    real(dp), allocatable, optional, intent(out) :: local_deriv(:,:,:)
    logical, optional, intent(in) :: in_domain
    
    integer :: p, no_points
    integer :: no_vertices
    real(dp), allocatable :: calc_func(:,:)
    real(dp), allocatable :: calc_local_deriv(:,:,:)
    
    no_points = size(local_coord,2)
    no_vertices = old_no_vertices(eltype)
    allocate(calc_func(no_vertices,no_points))
    allocate(calc_local_deriv(old_get_dimen(eltype),no_vertices,no_points))
    do p = 1, no_points
      call nodal_functions_single_point(eltype,local_coord(:,p),calc_func(:,p),calc_local_deriv(:,:,p))
    enddo
    if (present(func)) call move_alloc(calc_func,func)
    if (present(local_deriv)) call move_alloc(calc_local_deriv,local_deriv)
  end subroutine nodal_functions
  
  module pure subroutine nodal_functions_single_point(eltype,local_coord,func,local_deriv)
    use f90_kind, only: dp
    
    integer, intent(in) :: eltype
    real(dp), intent(in) :: local_coord(:)
    real(dp), intent(out) :: func(:)
    real(dp), optional, intent(out) :: local_deriv(:,:)
    
    real(dp) :: xi3, xi4
    real(dp) :: f1_l, f1_0, f1_r, &
                f2_l, f2_0, f2_r, &
                f3_l, f3_0, f3_r
    real(dp) :: lder1_l, lder1_0, lder1_r, &
                lder2_l, lder2_0, lder2_r, &
                lder3_l,lder3_0,lder3_r
    
    associate(xi => local_coord, fun => func)
    
    !---------------------------------------------------------------------
    ! 0D 1-node point element
    !---------------------------------------------------------------------
    
    if (eltype == 01) then
      fun(1) = 1.0_dp
    
    !---------------------------------------------------------------------
    ! 1D 1-node constant element
    !---------------------------------------------------------------------
    
    elseif (eltype == 11) then
      fun(1) = 1.0_dp
    
    ! I am not sure if this is correct, it is in fact a fix
    ! to get the quadrature right
      if (present(local_deriv)) then
        local_deriv(1,1) = 1.0_dp
      endif
    
    !---------------------------------------------------------------------
    ! 1D 2-node linear element
    !---------------------------------------------------------------------
    
    elseif (eltype == 12) then
      fun(1) = 0.5_dp*(1.0_dp - xi(1))
      fun(2) = 0.5_dp*(1.0_dp + xi(1))
    
      if (present(local_deriv)) then
        local_deriv(1,1) = -0.5_dp
        local_deriv(1,2) = +0.5_dp
      endif
    
    !---------------------------------------------------------------------
    ! 1D 3-node quadratic element
    !---------------------------------------------------------------------
    
    elseif (eltype == 13) then
      fun(1) = -0.5_dp*xi(1)*(1.0_dp - xi(1))
      fun(2) =  0.5_dp*xi(1)*(1.0_dp + xi(1))
      fun(3) = (1.0_dp + xi(1))*(1.0_dp - xi(1))
      
      if (present(local_deriv)) then
        local_deriv(1,1) = -0.5_dp + xi(1)
        local_deriv(1,2) =  0.5_dp + xi(1)
        local_deriv(1,3) = -2.0_dp*xi(1)
      endif
    
    !---------------------------------------------------------------------
    ! 2D 3-node linear triangular element
    !---------------------------------------------------------------------
    
    elseif (eltype == 23) then
      xi3 = 1.0_dp - xi(1) - xi(2)
      fun(1) = xi(1)
      fun(2) = xi(2)
      fun(3) = xi3
      
      if (present(local_deriv)) then
        local_deriv(1,1) = +1.0_dp
        local_deriv(1,2) =  0.0_dp
        local_deriv(1,3) = -1.0_dp
        
        local_deriv(2,1) =  0.0_dp
        local_deriv(2,2) = +1.0_dp
        local_deriv(2,3) = -1.0_dp
      endif
    
    !---------------------------------------------------------------------
    ! 2D 6-node quadratic triangular element
    !---------------------------------------------------------------------
    
    elseif (eltype == 26) then
      xi3 = 1.0_dp - xi(1) - xi(2)
      fun(1) = (2.0_dp*xi(1) - 1.0_dp)*xi(1)
      fun(2) = (2.0_dp*xi(2) - 1.0_dp)*xi(2)
      fun(3) = (2.0_dp*xi3   - 1.0_dp)*xi3
      fun(4) = 4.0_dp*xi(1)*xi(2)
      fun(5) = 4.0_dp*xi(2)*xi3
      fun(6) = 4.0_dp*xi(1)*xi3
      
      if (present(local_deriv)) then
        local_deriv(1,1) = 4.0_dp*xi(1) - 1.0_dp
        local_deriv(1,2) = 0.0_dp
        local_deriv(1,3) = -3.0_dp + 4.0_dp*xi(2) + 4.0_dp*xi(1)
        local_deriv(1,4) = 4.0_dp*xi(2)
        local_deriv(1,5) = -4.0_dp*xi(2)
        local_deriv(1,6) = 4.0_dp - 8.0_dp*xi(1) - 4.0_dp*xi(2)
        
        local_deriv(2,1) = 0.0_dp
        local_deriv(2,2) = 4.0_dp*xi(2) - 1.0_dp
        local_deriv(2,3) = -3.0_dp + 4.0_dp*xi(1) + 4.0_dp*xi(2)
        local_deriv(2,4) = 4.0_dp*xi(1)
        local_deriv(2,5) = 4.0_dp - 8.0_dp*xi(2) - 4.0_dp*xi(1)
        local_deriv(2,6) = -4.0_dp*xi(1)
      endif
    
    !---------------------------------------------------------------------
    ! 2D 4-node linear quadrilateral element
    !---------------------------------------------------------------------
    
    elseif (eltype == 24) then
      fun(1) = 0.25_dp*(1.0_dp - xi(1))*(1.0_dp - xi(2))
      fun(2) = 0.25_dp*(1.0_dp + xi(1))*(1.0_dp - xi(2))
      fun(3) = 0.25_dp*(1.0_dp + xi(1))*(1.0_dp + xi(2))
      fun(4) = 0.25_dp*(1.0_dp - xi(1))*(1.0_dp + xi(2))
      
      if (present(local_deriv)) then
        local_deriv(1,1) = -0.25_dp*(1.0_dp - xi(2))
        local_deriv(1,2) = +0.25_dp*(1.0_dp - xi(2))
        local_deriv(1,3) = +0.25_dp*(1.0_dp + xi(2))
        local_deriv(1,4) = -0.25_dp*(1.0_dp + xi(2))
        
        local_deriv(2,1) = -0.25_dp*(1.0_dp - xi(1))
        local_deriv(2,2) = -0.25_dp*(1.0_dp + xi(1))
        local_deriv(2,3) = +0.25_dp*(1.0_dp + xi(1))
        local_deriv(2,4) = +0.25_dp*(1.0_dp - xi(1))
      endif
    
    !---------------------------------------------------------------------
    ! 2D 8-node quadratic quadrilateral element
    !---------------------------------------------------------------------
    
    elseif (eltype == 28) then
      fun(1) = -0.25_dp*(1.0_dp - xi(1))*(1.0_dp - xi(2))*(1.0_dp + xi(1) + xi(2))
      fun(2) =  0.25_dp*(1.0_dp + xi(1))*(1.0_dp - xi(2))*(xi(1) - xi(2) - 1.0_dp)
      fun(3) =  0.25_dp*(1.0_dp + xi(1))*(1.0_dp + xi(2))*(xi(1) + xi(2) - 1.0_dp)
      fun(4) =  0.25_dp*(1.0_dp - xi(1))*(1.0_dp + xi(2))*(xi(2) - xi(1) - 1.0_dp)
      fun(5) =  0.50_dp*(1.0_dp - xi(1)**2)*(1.0_dp - xi(2))
      fun(6) =  0.50_dp*(1.0_dp + xi(1))*(1.0_dp - xi(2)**2)
      fun(7) =  0.50_dp*(1.0_dp - xi(1)**2)*(1.0_dp + xi(2))
      fun(8) =  0.50_dp*(1.0_dp - xi(1))*(1.0_dp - xi(2)**2)
      
      if (present(local_deriv)) then
        local_deriv(1,1) = -0.25_dp*(xi(2) - 1.0_dp)*(2.0_dp*xi(1) + xi(2))
        local_deriv(1,2) =  0.25_dp*(1.0_dp - xi(2))*(2.0_dp*xi(1) - xi(2))
        local_deriv(1,3) =  0.25_dp*(1.0_dp + xi(2))*(2.0_dp*xi(1) + xi(2))
        local_deriv(1,4) =  0.25_dp*(1.0_dp + xi(2))*(2.0_dp*xi(1) - xi(2))
        local_deriv(1,5) = -xi(1)*(1.0_dp - xi(2))
        local_deriv(1,6) =  0.5_dp*(1.0_dp - xi(2)**2)
        local_deriv(1,7) = -xi(1)*(1.0_dp + xi(2))
        local_deriv(1,8) =  0.5_dp*(xi(2)**2 - 1.0)
        
        local_deriv(2,1) = -0.25_dp*(xi(1) - 1.0_dp)*(xi(1) + 2.0_dp*xi(2))
        local_deriv(2,2) =  0.25_dp*(1.0_dp + xi(1))*(2.0_dp*xi(2) - xi(1))
        local_deriv(2,3) =  0.25_dp*(1.0_dp + xi(1))*(2.0_dp*xi(2) + xi(1))
        local_deriv(2,4) =  0.25_dp*(xi(1) - 1.0_dp)*(xi(1) - 2.0_dp*xi(2))
        local_deriv(2,5) =  0.5_dp*(xi(1)**2 - 1.0_dp)
        local_deriv(2,6) = -xi(2)*(1.0_dp + xi(1))
        local_deriv(2,7) =  0.5_dp*(1.0_dp - xi(1)**2)
        local_deriv(2,8) = -xi(2)*(1.0_dp - xi(1))
      endif
    
    !---------------------------------------------------------------------
    ! 2D 9-node quadratic quadrilateral element
    !---------------------------------------------------------------------
    
    elseif (eltype == 29) then
      f1_l = -0.5_dp*xi(1)*(1.0_dp - xi(1))
      f1_r =  0.5_dp*xi(1)*(1.0_dp + xi(1))
      f1_0 = (1.0_dp - xi(1)**2)
      
      lder1_l = -0.5_dp + xi(1)
      lder1_r =  0.5_dp + xi(1)
      lder1_0 = -2.0_dp*xi(1)
      
      f2_l = -0.5_dp*xi(2)*(1.0_dp - xi(2))
      f2_r =  0.5_dp*xi(2)*(1.0_dp + xi(2))
      f2_0 = (1.0_dp - xi(2)**2)
      
      lder2_l = -0.5_dp + xi(2)
      lder2_r =  0.5_dp + xi(2)
      lder2_0 = -2.0_dp*xi(2)
      
      fun(1) = f1_l*f2_l
      fun(2) = f1_r*f2_l
      fun(3) = f1_r*f2_r
      fun(4) = f1_l*f2_r
      fun(5) = f1_0*f2_l
      fun(6) = f1_r*f2_0
      fun(7) = f1_0*f2_r
      fun(8) = f1_l*f2_0
      fun(9) = f1_0*f2_0
      
      if (present(local_deriv)) then
        local_deriv(1,1) = lder1_l*f2_l
        local_deriv(1,2) = lder1_r*f2_l
        local_deriv(1,3) = lder1_r*f2_r
        local_deriv(1,4) = lder1_l*f2_r
        local_deriv(1,5) = lder1_0*f2_l
        local_deriv(1,6) = lder1_r*f2_0
        local_deriv(1,7) = lder1_0*f2_r
        local_deriv(1,8) = lder1_l*f2_0
        local_deriv(1,9) = lder1_0*f2_0
        
        local_deriv(2,1) = f1_l*lder2_l
        local_deriv(2,2) = f1_r*lder2_l
        local_deriv(2,3) = f1_r*lder2_r
        local_deriv(2,4) = f1_l*lder2_r
        local_deriv(2,5) = f1_0*lder2_l
        local_deriv(2,6) = f1_r*lder2_0
        local_deriv(2,7) = f1_0*lder2_r
        local_deriv(2,8) = f1_l*lder2_0
        local_deriv(2,9) = f1_0*lder2_0
      endif
    
    !---------------------------------------------------------------------
    ! 3D 4-node linear tetrahedral element
    !---------------------------------------------------------------------
    
    elseif (eltype == 34) then
      xi4 = 1.0_dp - xi(1) - xi(2) - xi(3)
      fun(1) = xi(1)
      fun(2) = xi(2)
      fun(3) = xi(3)
      fun(4) = xi4
      
      if (present(local_deriv)) then
        local_deriv(1,1) = +1.0_dp
        local_deriv(1,2) =  0.0_dp
        local_deriv(1,3) =  0.0_dp
        local_deriv(1,4) = -1.0_dp
        
        local_deriv(2,1) =  0.0_dp
        local_deriv(2,2) = +1.0_dp
        local_deriv(2,3) =  0.0_dp
        local_deriv(2,4) = -1.0_dp
        
        local_deriv(3,1) =  0.0_dp
        local_deriv(3,2) =  0.0_dp
        local_deriv(3,3) = +1.0_dp
        local_deriv(3,4) = -1.0_dp
      endif

    !---------------------------------------------------------------------
    ! 3D 10-node quadratic tetrahedral element
    !---------------------------------------------------------------------

    elseif (eltype == 310) then
      xi4 = 1.0 - xi(1) - xi(2) - xi(3)
      fun(1)  = (2.0*xi(1) - 1.0)*xi(1)
      fun(2)  = (2.0*xi(2) - 1.0)*xi(2)
      fun(3)  = (2.0*xi(3) - 1.0)*xi(3)
      fun(4)  = (2.0*xi4 - 1.0)*xi4
      fun(5)  = 4.0*xi(1)*xi(2)
      fun(6)  = 4.0*xi(2)*xi(3)
      fun(7)  = 4.0*xi(1)*xi(3)
      fun(8)  = 4.0*xi(1)*xi4
      fun(9)  = 4.0*xi(2)*xi4
      fun(10) = 4.0*xi(3)*xi4
      
      if (present(local_deriv)) then
        local_deriv(1,1)  =  4.0*xi(1) - 1.0_dp
        local_deriv(1,2)  =  0.0_dp
        local_deriv(1,3)  =  0.0_dp
        local_deriv(1,4)  = -4.0_dp*xi4 + 1.0_dp
        local_deriv(1,5)  =  4.0_dp*xi(2)
        local_deriv(1,6)  =  0.0_dp
        local_deriv(1,7)  =  4.0_dp*xi(3)
        local_deriv(1,8)  = -4.0_dp*xi(1) + 4.0_dp*xi4
        local_deriv(1,9)  = -4.0_dp*xi(2)
        local_deriv(1,10) = -4.0_dp*xi(3)
        
        local_deriv(2,1)  =  0.0_dp
        local_deriv(2,2)  =  4.0_dp*xi(2) - 1.0_dp
        local_deriv(2,3)  =  0.0_dp
        local_deriv(2,4)  = -4.0_dp*xi4 + 1.0_dp
        local_deriv(2,5)  =  4.0_dp*xi(1)
        local_deriv(2,6)  =  4.0_dp*xi(3)
        local_deriv(2,7)  =  0.0_dp
        local_deriv(2,8)  = -4.0_dp*xi(1)
        local_deriv(2,9)  = -4.0_dp*xi(2) + 4.0_dp*xi4
        local_deriv(2,10) = -4.0_dp*xi(3)
        
        local_deriv(3,1)  =  0.0_dp
        local_deriv(3,2)  =  0.0_dp
        local_deriv(3,3)  =  4.0_dp*xi(3) - 1.0_dp
        local_deriv(3,4)  = -4.0_dp*xi4 + 1.0_dp
        local_deriv(3,5)  =  0.0_dp
        local_deriv(3,6)  =  4.0_dp*xi(2)
        local_deriv(3,7)  =  4.0_dp*xi(1)
        local_deriv(3,8)  = -4.0_dp*xi(1)
        local_deriv(3,9)  = -4.0_dp*xi(2)
        local_deriv(3,10) = -4.0_dp*xi(3) + 4.0_dp*xi4
      endif
    
    !---------------------------------------------------------------------
    ! 3D 8-node hexahedral element
    !---------------------------------------------------------------------
    
    elseif (eltype == 38) then
      fun(1) = (1.0_dp - xi(1))*(1.0_dp - xi(2))*(1.0_dp - xi(3))/8.0_dp
      fun(2) = (1.0_dp + xi(1))*(1.0_dp - xi(2))*(1.0_dp - xi(3))/8.0_dp
      fun(3) = (1.0_dp + xi(1))*(1.0_dp + xi(2))*(1.0_dp - xi(3))/8.0_dp
      fun(4) = (1.0_dp - xi(1))*(1.0_dp + xi(2))*(1.0_dp - xi(3))/8.0_dp
      fun(5) = (1.0_dp - xi(1))*(1.0_dp - xi(2))*(1.0_dp + xi(3))/8.0_dp
      fun(6) = (1.0_dp + xi(1))*(1.0_dp - xi(2))*(1.0_dp + xi(3))/8.0_dp
      fun(7) = (1.0_dp + xi(1))*(1.0_dp + xi(2))*(1.0_dp + xi(3))/8.0_dp
      fun(8) = (1.0_dp - xi(1))*(1.0_dp + xi(2))*(1.0_dp + xi(3))/8.0_dp
      
      if (present(local_deriv)) then
        local_deriv(1,1) = -(1.0_dp - xi(2))*(1.0_dp - xi(3))/8.0_dp
        local_deriv(1,2) = +(1.0_dp - xi(2))*(1.0_dp - xi(3))/8.0_dp
        local_deriv(1,3) = +(1.0_dp + xi(2))*(1.0_dp - xi(3))/8.0_dp
        local_deriv(1,4) = -(1.0_dp + xi(2))*(1.0_dp - xi(3))/8.0_dp
        local_deriv(1,5) = -(1.0_dp - xi(2))*(1.0_dp + xi(3))/8.0_dp
        local_deriv(1,6) = +(1.0_dp - xi(2))*(1.0_dp + xi(3))/8.0_dp
        local_deriv(1,7) = +(1.0_dp + xi(2))*(1.0_dp + xi(3))/8.0_dp
        local_deriv(1,8) = -(1.0_dp + xi(2))*(1.0_dp + xi(3))/8.0_dp
        
        local_deriv(2,1) = -(1.0_dp - xi(1))*(1.0_dp - xi(3))/8.0_dp
        local_deriv(2,2) = -(1.0_dp + xi(1))*(1.0_dp - xi(3))/8.0_dp
        local_deriv(2,3) = +(1.0_dp + xi(1))*(1.0_dp - xi(3))/8.0_dp
        local_deriv(2,4) = +(1.0_dp - xi(1))*(1.0_dp - xi(3))/8.0_dp
        local_deriv(2,5) = -(1.0_dp - xi(1))*(1.0_dp + xi(3))/8.0_dp
        local_deriv(2,6) = -(1.0_dp + xi(1))*(1.0_dp + xi(3))/8.0_dp
        local_deriv(2,7) = +(1.0_dp + xi(1))*(1.0_dp + xi(3))/8.0_dp
        local_deriv(2,8) = +(1.0_dp - xi(1))*(1.0_dp + xi(3))/8.0_dp
        
        local_deriv(3,1) = -(1.0_dp - xi(1))*(1.0_dp - xi(2))/8.0_dp
        local_deriv(3,2) = -(1.0_dp + xi(1))*(1.0_dp - xi(2))/8.0_dp
        local_deriv(3,3) = -(1.0_dp + xi(1))*(1.0_dp + xi(2))/8.0_dp
        local_deriv(3,4) = -(1.0_dp - xi(1))*(1.0_dp + xi(2))/8.0_dp
        local_deriv(3,5) = +(1.0_dp - xi(1))*(1.0_dp - xi(2))/8.0_dp
        local_deriv(3,6) = +(1.0_dp + xi(1))*(1.0_dp - xi(2))/8.0_dp
        local_deriv(3,7) = +(1.0_dp + xi(1))*(1.0_dp + xi(2))/8.0_dp
        local_deriv(3,8) = +(1.0_dp - xi(1))*(1.0_dp + xi(2))/8.0_dp
      endif
    
    !---------------------------------------------------------------------
    ! 3D 20-node quadratic hexahedral element
    !---------------------------------------------------------------------
    
    elseif (eltype == 320) then
      fun(1)  = (1.0_dp - xi(1))   *(1.0_dp - xi(2))   *(1.0_dp - xi(3))*(-xi(1) - xi(2) - xi(3) - 2.0_dp)/8.0_dp
      fun(2)  = (1.0_dp + xi(1))   *(1.0_dp - xi(2))   *(1.0_dp - xi(3))*(+xi(1) - xi(2) - xi(3) - 2.0_dp)/8.0_dp
      fun(3)  = (1.0_dp + xi(1))   *(1.0_dp + xi(2))   *(1.0_dp - xi(3))*(+xi(1) + xi(2) - xi(3) - 2.0_dp)/8.0_dp
      fun(4)  = (1.0_dp - xi(1))   *(1.0_dp + xi(2))   *(1.0_dp - xi(3))*(-xi(1) + xi(2) - xi(3) - 2.0_dp)/8.0_dp
      fun(5)  = (1.0_dp - xi(1))   *(1.0_dp - xi(2))   *(1.0_dp + xi(3))*(-xi(1) - xi(2) + xi(3) - 2.0_dp)/8.0_dp
      fun(6)  = (1.0_dp + xi(1))   *(1.0_dp - xi(2))   *(1.0_dp + xi(3))*(+xi(1) - xi(2) + xi(3) - 2.0_dp)/8.0_dp
      fun(7)  = (1.0_dp + xi(1))   *(1.0_dp + xi(2))   *(1.0_dp + xi(3))*(+xi(1) + xi(2) + xi(3) - 2.0_dp)/8.0_dp
      fun(8)  = (1.0_dp - xi(1))   *(1.0_dp + xi(2))   *(1.0_dp + xi(3))*(-xi(1) + xi(2) + xi(3) - 2.0_dp)/8.0_dp
      fun(9)  = (1.0_dp - xi(1)**2)*(1.0_dp - xi(2))   *(1.0_dp - xi(3))/4.0_dp
      fun(10) = (1.0_dp + xi(1))   *(1.0_dp - xi(2)**2)*(1.0_dp - xi(3))/4.0_dp
      fun(11) = (1.0_dp - xi(1)**2)*(1.0_dp + xi(2))   *(1.0_dp - xi(3))/4.0_dp
      fun(12) = (1.0_dp - xi(1))   *(1.0_dp - xi(2)**2)*(1.0_dp - xi(3))/4.0_dp
      fun(13) = (1.0_dp - xi(1)**2)*(1.0_dp - xi(2))   *(1.0_dp + xi(3))/4.0_dp
      fun(14) = (1.0_dp + xi(1))   *(1.0_dp - xi(2)**2)*(1.0_dp + xi(3))/4.0_dp
      fun(15) = (1.0_dp - xi(1)**2)*(1.0_dp + xi(2))   *(1.0_dp + xi(3))/4.0_dp
      fun(16) = (1.0_dp - xi(1))   *(1.0_dp - xi(2)**2)*(1.0_dp + xi(3))/4.0_dp
      fun(17) = (1.0_dp - xi(1))   *(1.0_dp - xi(2))   *(1.0_dp - xi(3)**2)/4.0_dp
      fun(18) = (1.0_dp + xi(1))   *(1.0_dp - xi(2))   *(1.0_dp - xi(3)**2)/4.0_dp
      fun(19) = (1.0_dp + xi(1))   *(1.0_dp + xi(2))   *(1.0_dp - xi(3)**2)/4.0_dp
      fun(20) = (1.0_dp - xi(1))   *(1.0_dp + xi(2))   *(1.0_dp - xi(3)**2)/4.0_dp
      
      if (present(local_deriv)) then
        local_deriv(1,1)  = (1.0_dp - xi(2))*(1.0_dp - xi(3))   *(-(-xi(1) - xi(2) - xi(3) - 2.0_dp) - (1.0_dp - xi(1)))/8.0_dp
        local_deriv(1,2)  = (1.0_dp - xi(2))*(1.0_dp - xi(3))   *(+(+xi(1) - xi(2) - xi(3) - 2.0_dp) + (1.0_dp + xi(1)))/8.0_dp
        local_deriv(1,3)  = (1.0_dp + xi(2))*(1.0_dp - xi(3))   *(+(+xi(1) + xi(2) - xi(3) - 2.0_dp) + (1.0_dp + xi(1)))/8.0_dp
        local_deriv(1,4)  = (1.0_dp + xi(2))*(1.0_dp - xi(3))   *(-(-xi(1) + xi(2) - xi(3) - 2.0_dp) - (1.0_dp - xi(1)))/8.0_dp
        local_deriv(1,5)  = (1.0_dp - xi(2))*(1.0_dp + xi(3))   *(-(-xi(1) - xi(2) + xi(3) - 2.0_dp) - (1.0_dp - xi(1)))/8.0_dp
        local_deriv(1,6)  = (1.0_dp - xi(2))*(1.0_dp + xi(3))   *(+(+xi(1) - xi(2) + xi(3) - 2.0_dp) + (1.0_dp + xi(1)))/8.0_dp
        local_deriv(1,7)  = (1.0_dp + xi(2))*(1.0_dp + xi(3))   *(+(+xi(1) + xi(2) + xi(3) - 2.0_dp) + (1.0_dp + xi(1)))/8.0_dp
        local_deriv(1,8)  = (1.0_dp + xi(2))*(1.0_dp + xi(3))   *(-(-xi(1) + xi(2) + xi(3) - 2.0_dp) - (1.0_dp - xi(1)))/8.0_dp
        local_deriv(1,9)  = (-2.0_dp*xi(1)) *(1.0_dp - xi(2))   *(1.0_dp - xi(3))/4.0_dp
        local_deriv(1,10) = ( 1.0_dp)       *(1.0_dp - xi(2)**2)*(1.0_dp - xi(3))/4.0_dp
        local_deriv(1,11) = (-2.0_dp*xi(1)) *(1.0_dp + xi(2))   *(1.0_dp - xi(3))/4.0_dp
        local_deriv(1,12) = (-1.0_dp)       *(1.0_dp - xi(2)**2)*(1.0_dp - xi(3))/4.0_dp
        local_deriv(1,13) = (-2.0_dp*xi(1)) *(1.0_dp - xi(2))   *(1.0_dp + xi(3))/4.0_dp
        local_deriv(1,14) = ( 1.0_dp)       *(1.0_dp - xi(2)**2)*(1.0_dp + xi(3))/4.0_dp
        local_deriv(1,15) = (-2.0_dp*xi(1)) *(1.0_dp + xi(2))   *(1.0_dp + xi(3))/4.0_dp
        local_deriv(1,16) = (-1.0_dp)       *(1.0_dp - xi(2)**2)*(1.0_dp + xi(3))/4.0_dp
        local_deriv(1,17) = (-1.0_dp)       *(1.0_dp - xi(2))   *(1.0_dp - xi(3)**2)/4.0_dp
        local_deriv(1,18) = ( 1.0_dp)       *(1.0_dp - xi(2))   *(1.0_dp - xi(3)**2)/4.0_dp
        local_deriv(1,19) = ( 1.0_dp)       *(1.0_dp + xi(2))   *(1.0_dp - xi(3)**2)/4.0_dp
        local_deriv(1,20) = (-1.0_dp)       *(1.0_dp + xi(2))   *(1.0_dp - xi(3)**2)/4.0_dp
        
        local_deriv(2,1)  = (1.0_dp - xi(1))   *(1.0_dp - xi(3))*(-(-xi(1) - xi(2) - xi(3) - 2.0_dp) - (1.0_dp - xi(2)))/8.0_dp
        local_deriv(2,2)  = (1.0_dp + xi(1))   *(1.0_dp - xi(3))*(-(+xi(1) - xi(2) - xi(3) - 2.0_dp) - (1.0_dp - xi(2)))/8.0_dp
        local_deriv(2,3)  = (1.0_dp + xi(1))   *(1.0_dp - xi(3))*(+(+xi(1) + xi(2) - xi(3) - 2.0_dp) + (1.0_dp + xi(2)))/8.0_dp
        local_deriv(2,4)  = (1.0_dp - xi(1))   *(1.0_dp - xi(3))*(+(-xi(1) + xi(2) - xi(3) - 2.0_dp) + (1.0_dp + xi(2)))/8.0_dp
        local_deriv(2,5)  = (1.0_dp - xi(1))   *(1.0_dp + xi(3))*(-(-xi(1) - xi(2) + xi(3) - 2.0_dp) - (1.0_dp - xi(2)))/8.0_dp
        local_deriv(2,6)  = (1.0_dp + xi(1))   *(1.0_dp + xi(3))*(-(+xi(1) - xi(2) + xi(3) - 2.0_dp) - (1.0_dp - xi(2)))/8.0_dp
        local_deriv(2,7)  = (1.0_dp + xi(1))   *(1.0_dp + xi(3))*(+(+xi(1) + xi(2) + xi(3) - 2.0_dp) + (1.0_dp + xi(2)))/8.0_dp
        local_deriv(2,8)  = (1.0_dp - xi(1))   *(1.0_dp + xi(3))*(+(-xi(1) + xi(2) + xi(3) - 2.0_dp) + (1.0_dp + xi(2)))/8.0_dp
        local_deriv(2,9)  = (1.0_dp - xi(1)**2)*(-1.0_dp)       *(1.0_dp - xi(3))/4.0_dp
        local_deriv(2,10) = (1.0_dp + xi(1))   *(-2.0_dp*xi(2)) *(1.0_dp - xi(3))/4.0_dp
        local_deriv(2,11) = (1.0_dp - xi(1)**2)*( 1.0_dp)       *(1.0_dp - xi(3))/4.0_dp
        local_deriv(2,12) = (1.0_dp - xi(1))   *(-2.0_dp*xi(2)) *(1.0_dp - xi(3))/4.0_dp
        local_deriv(2,13) = (1.0_dp - xi(1)**2)*(-1.0_dp)       *(1.0_dp + xi(3))/4.0_dp
        local_deriv(2,14) = (1.0_dp + xi(1))   *(-2.0_dp*xi(2)) *(1.0_dp + xi(3))/4.0_dp
        local_deriv(2,15) = (1.0_dp - xi(1)**2)*( 1.0_dp)       *(1.0_dp + xi(3))/4.0_dp
        local_deriv(2,16) = (1.0_dp - xi(1))   *(-2.0_dp*xi(2)) *(1.0_dp + xi(3))/4.0_dp
        local_deriv(2,17) = (1.0_dp - xi(1))   *(-1.0_dp)       *(1.0_dp - xi(3)**2)/4.0_dp
        local_deriv(2,18) = (1.0_dp + xi(1))   *(-1.0_dp)       *(1.0_dp - xi(3)**2)/4.0_dp
        local_deriv(2,19) = (1.0_dp + xi(1))   *( 1.0_dp)       *(1.0_dp - xi(3)**2)/4.0_dp
        local_deriv(2,20) = (1.0_dp - xi(1))   *( 1.0_dp)       *(1.0_dp - xi(3)**2)/4.0_dp
        
        local_deriv(3,1)  = (1.0_dp - xi(1))   *(1.0_dp - xi(2))   *(-(-xi(1) - xi(2) - xi(3) - 2.0_dp) - (1.0_dp - xi(3)))/8.0_dp
        local_deriv(3,2)  = (1.0_dp + xi(1))   *(1.0_dp - xi(2))   *(-(+xi(1) - xi(2) - xi(3) - 2.0_dp) - (1.0_dp - xi(3)))/8.0_dp
        local_deriv(3,3)  = (1.0_dp + xi(1))   *(1.0_dp + xi(2))   *(-(+xi(1) + xi(2) - xi(3) - 2.0_dp) - (1.0_dp - xi(3)))/8.0_dp
        local_deriv(3,4)  = (1.0_dp - xi(1))   *(1.0_dp + xi(2))   *(-(-xi(1) + xi(2) - xi(3) - 2.0_dp) - (1.0_dp - xi(3)))/8.0_dp
        local_deriv(3,5)  = (1.0_dp - xi(1))   *(1.0_dp - xi(2))   *(+(-xi(1) - xi(2) + xi(3) - 2.0_dp) + (1.0_dp + xi(3)))/8.0_dp
        local_deriv(3,6)  = (1.0_dp + xi(1))   *(1.0_dp - xi(2))   *(+(+xi(1) - xi(2) + xi(3) - 2.0_dp) + (1.0_dp + xi(3)))/8.0_dp
        local_deriv(3,7)  = (1.0_dp + xi(1))   *(1.0_dp + xi(2))   *(+(+xi(1) + xi(2) + xi(3) - 2.0_dp) + (1.0_dp + xi(3)))/8.0_dp
        local_deriv(3,8)  = (1.0_dp - xi(1))   *(1.0_dp + xi(2))   *(+(-xi(1) + xi(2) + xi(3) - 2.0_dp) + (1.0_dp + xi(3)))/8.0_dp
        local_deriv(3,9)  = (1.0_dp - xi(1)**2)*(1.0_dp - xi(2))   *(-1.0_dp)/4.0_dp
        local_deriv(3,10) = (1.0_dp + xi(1))   *(1.0_dp - xi(2)**2)*(-1.0_dp)/4.0_dp
        local_deriv(3,11) = (1.0_dp - xi(1)**2)*(1.0_dp + xi(2))   *(-1.0_dp)/4.0_dp
        local_deriv(3,12) = (1.0_dp - xi(1))   *(1.0_dp - xi(2)**2)*(-1.0_dp)/4.0_dp
        local_deriv(3,13) = (1.0_dp - xi(1)**2)*(1.0_dp - xi(2))   *( 1.0_dp)/4.0_dp
        local_deriv(3,14) = (1.0_dp + xi(1))   *(1.0_dp - xi(2)**2)*( 1.0_dp)/4.0_dp
        local_deriv(3,15) = (1.0_dp - xi(1)**2)*(1.0_dp + xi(2))   *( 1.0_dp)/4.0_dp
        local_deriv(3,16) = (1.0_dp - xi(1))   *(1.0_dp - xi(2)**2)*( 1.0_dp)/4.0_dp
        local_deriv(3,17) = (1.0_dp - xi(1))   *(1.0_dp - xi(2))   *(-2.0_dp*xi(3))/4.0_dp
        local_deriv(3,18) = (1.0_dp + xi(1))   *(1.0_dp - xi(2))   *(-2.0_dp*xi(3))/4.0_dp
        local_deriv(3,19) = (1.0_dp + xi(1))   *(1.0_dp + xi(2))   *(-2.0_dp*xi(3))/4.0_dp
        local_deriv(3,20) = (1.0_dp - xi(1))   *(1.0_dp + xi(2))   *(-2.0_dp*xi(3))/4.0_dp
      endif
    
    !---------------------------------------------------------------------
    ! 3D 27-node quadratic hexahedral element
    !---------------------------------------------------------------------
    
    elseif (eltype == 327) then
      f1_l = -0.5_dp*xi(1)*(1.0_dp - xi(1))
      f1_r =  0.5_dp*xi(1)*(1.0_dp + xi(1))
      f1_0 = (1.0_dp - xi(1)**2)
      
      lder1_l = -0.5_dp + xi(1)
      lder1_r =  0.5_dp + xi(1)
      lder1_0 = -2.0_dp*xi(1)
      
      f2_l = -0.5_dp*xi(2)*(1.0_dp - xi(2))
      f2_r =  0.5_dp*xi(2)*(1.0_dp + xi(2))
      f2_0 = (1.0_dp - xi(2)**2)
      
      lder2_l = -0.5_dp + xi(2)
      lder2_r =  0.5_dp + xi(2)
      lder2_0 = -2.0_dp*xi(2)
      
      f3_l = -0.5_dp*xi(3)*(1.0_dp - xi(3))
      f3_r =  0.5_dp*xi(3)*(1.0_dp + xi(3))
      f3_0 = (1.0_dp - xi(3)**2)
      
      lder3_l = -0.5_dp + xi(3)
      lder3_r =  0.5_dp + xi(3)
      lder3_0 = -2.0_dp*xi(3)
      
      fun(1)  = f1_l*f2_l*f3_l
      fun(2)  = f1_r*f2_l*f3_l
      fun(3)  = f1_r*f2_r*f3_l
      fun(4)  = f1_l*f2_r*f3_l
      fun(5)  = f1_l*f2_l*f3_r
      fun(6)  = f1_r*f2_l*f3_r
      fun(7)  = f1_r*f2_r*f3_r
      fun(8)  = f1_l*f2_r*f3_r
      fun(9)  = f1_0*f2_l*f3_l
      fun(10) = f1_r*f2_0*f3_l
      fun(11) = f1_0*f2_r*f3_l
      fun(12) = f1_l*f2_0*f3_l
      fun(13) = f1_0*f2_l*f3_r
      fun(14) = f1_r*f2_0*f3_r
      fun(15) = f1_0*f2_r*f3_r
      fun(16) = f1_l*f2_0*f3_r
      fun(17) = f1_l*f2_l*f3_0
      fun(18) = f1_r*f2_l*f3_0
      fun(19) = f1_r*f2_r*f3_0
      fun(20) = f1_l*f2_r*f3_0
      fun(21) = f1_0*f2_0*f3_l ! z-
      fun(22) = f1_0*f2_0*f3_r ! z+
      fun(23) = f1_0*f2_l*f3_0 ! y-
      fun(24) = f1_0*f2_r*f3_0 ! y+
      fun(25) = f1_l*f2_0*f3_0 ! x-
      fun(26) = f1_r*f2_0*f3_0 ! x+
      fun(27) = f1_0*f2_0*f3_0 ! center
      
      if (present(local_deriv)) then
        local_deriv(1,1)  = lder1_l*f2_l*f3_l
        local_deriv(1,2)  = lder1_r*f2_l*f3_l
        local_deriv(1,3)  = lder1_r*f2_r*f3_l
        local_deriv(1,4)  = lder1_l*f2_r*f3_l
        local_deriv(1,5)  = lder1_l*f2_l*f3_r
        local_deriv(1,6)  = lder1_r*f2_l*f3_r
        local_deriv(1,7)  = lder1_r*f2_r*f3_r
        local_deriv(1,8)  = lder1_l*f2_r*f3_r
        local_deriv(1,9)  = lder1_0*f2_l*f3_l
        local_deriv(1,10) = lder1_r*f2_0*f3_l
        local_deriv(1,11) = lder1_0*f2_r*f3_l
        local_deriv(1,12) = lder1_l*f2_0*f3_l
        local_deriv(1,13) = lder1_0*f2_l*f3_r
        local_deriv(1,14) = lder1_r*f2_0*f3_r
        local_deriv(1,15) = lder1_0*f2_r*f3_r
        local_deriv(1,16) = lder1_l*f2_0*f3_r
        local_deriv(1,17) = lder1_l*f2_l*f3_0
        local_deriv(1,18) = lder1_r*f2_l*f3_0
        local_deriv(1,19) = lder1_r*f2_r*f3_0
        local_deriv(1,20) = lder1_l*f2_r*f3_0
        local_deriv(1,21) = lder1_0*f2_0*f3_l
        local_deriv(1,22) = lder1_0*f2_0*f3_r
        local_deriv(1,23) = lder1_0*f2_l*f3_0
        local_deriv(1,24) = lder1_0*f2_r*f3_0
        local_deriv(1,25) = lder1_l*f2_0*f3_0
        local_deriv(1,26) = lder1_r*f2_0*f3_0
        local_deriv(1,27) = lder1_0*f2_0*f3_0
        
        local_deriv(2,1)  = f1_l*lder2_l*f3_l
        local_deriv(2,2)  = f1_r*lder2_l*f3_l
        local_deriv(2,3)  = f1_r*lder2_r*f3_l
        local_deriv(2,4)  = f1_l*lder2_r*f3_l
        local_deriv(2,5)  = f1_l*lder2_l*f3_r
        local_deriv(2,6)  = f1_r*lder2_l*f3_r
        local_deriv(2,7)  = f1_r*lder2_r*f3_r
        local_deriv(2,8)  = f1_l*lder2_r*f3_r
        local_deriv(2,9)  = f1_0*lder2_l*f3_l
        local_deriv(2,10) = f1_r*lder2_0*f3_l
        local_deriv(2,11) = f1_0*lder2_r*f3_l
        local_deriv(2,12) = f1_l*lder2_0*f3_l
        local_deriv(2,13) = f1_0*lder2_l*f3_r
        local_deriv(2,14) = f1_r*lder2_0*f3_r
        local_deriv(2,15) = f1_0*lder2_r*f3_r
        local_deriv(2,16) = f1_l*lder2_0*f3_r
        local_deriv(2,17) = f1_l*lder2_l*f3_0
        local_deriv(2,18) = f1_r*lder2_l*f3_0
        local_deriv(2,19) = f1_r*lder2_r*f3_0
        local_deriv(2,20) = f1_l*lder2_r*f3_0
        local_deriv(2,21) = f1_0*lder2_0*f3_l
        local_deriv(2,22) = f1_0*lder2_0*f3_r
        local_deriv(2,23) = f1_0*lder2_l*f3_0
        local_deriv(2,24) = f1_0*lder2_r*f3_0
        local_deriv(2,25) = f1_l*lder2_0*f3_0
        local_deriv(2,26) = f1_r*lder2_0*f3_0
        local_deriv(2,27) = f1_0*lder2_0*f3_0
        
        local_deriv(3,1)  = f1_l*f2_l*lder3_l
        local_deriv(3,2)  = f1_r*f2_l*lder3_l
        local_deriv(3,3)  = f1_r*f2_r*lder3_l
        local_deriv(3,4)  = f1_l*f2_r*lder3_l
        local_deriv(3,5)  = f1_l*f2_l*lder3_r
        local_deriv(3,6)  = f1_r*f2_l*lder3_r
        local_deriv(3,7)  = f1_r*f2_r*lder3_r
        local_deriv(3,8)  = f1_l*f2_r*lder3_r
        local_deriv(3,9)  = f1_0*f2_l*lder3_l
        local_deriv(3,10) = f1_r*f2_0*lder3_l
        local_deriv(3,11) = f1_0*f2_r*lder3_l
        local_deriv(3,12) = f1_l*f2_0*lder3_l
        local_deriv(3,13) = f1_0*f2_l*lder3_r
        local_deriv(3,14) = f1_r*f2_0*lder3_r
        local_deriv(3,15) = f1_0*f2_r*lder3_r
        local_deriv(3,16) = f1_l*f2_0*lder3_r
        local_deriv(3,17) = f1_l*f2_l*lder3_0
        local_deriv(3,18) = f1_r*f2_l*lder3_0
        local_deriv(3,19) = f1_r*f2_r*lder3_0
        local_deriv(3,20) = f1_l*f2_r*lder3_0
        local_deriv(3,21) = f1_0*f2_0*lder3_l
        local_deriv(3,22) = f1_0*f2_0*lder3_r
        local_deriv(3,23) = f1_0*f2_l*lder3_0
        local_deriv(3,24) = f1_0*f2_r*lder3_0
        local_deriv(3,25) = f1_l*f2_0*lder3_0
        local_deriv(3,26) = f1_r*f2_0*lder3_0
        local_deriv(3,27) = f1_0*f2_0*lder3_0
      endif
    else
      error stop "nodal_functions_single_point: unknown eltype"
    endif
    
    end associate
  end subroutine nodal_functions_single_point
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  pure real(dp) function fun_1d(p,x,deriv)
    ! This function returns the values of the basis function in 1D that is
    ! of degree p.
    use f90_kind, only: dp
    
    integer, intent(in) :: p
    real(dp), intent(in) :: x
    logical, intent(in) :: deriv

    real(dp) :: help1
    real(dp) :: help2
    real(dp) :: help1_deriv
    real(dp) :: help2_deriv

    if (.not.deriv) then
      select case(p)
        case(0)
          fun_1d = 1.0_dp
        case(1)
          fun_1d = x
        case(2:)
          help1 = 0.5_dp*(1.0_dp - x)
          help2 = 0.5_dp*(1.0_dp + x)
          fun_1d = help1*help2*phi(p-2,x,.false.)
        case default
          error stop "ERROR in fun_1d"
      end select

    else
      select case(p)
        case(0)
          fun_1d = 0.0_dp
        case(1)
          fun_1d = 1.0_dp
        case(2:)
          help1 = 0.5_dp*(1.0_dp - x)
          help2 = 0.5_dp*(1.0_dp + x)
          help1_deriv = - 0.5_dp
          help2_deriv = + 0.5_dp
          fun_1d = help1*help2*phi(p-2,x,.true.) + &
                   help1_deriv*help2*phi(p-2,x,.false.) + &
                   help1*help2_deriv*phi(p-2,x,.false.)
        case default
          error stop "ERROR in fun_1d"
      end select
    endif
  end function fun_1d
  
  
  pure real(dp) function phi(p,x,deriv)
    !---------------------------------------------------------------------
    ! This function returns the values of the phi_0 function as defined
    ! on page 27 of the book: Higher-Order Finite Element Methods
    ! of Solin, Segeth and Dolozel.
    !---------------------------------------------------------------------
    use f90_kind, only: dp
    
    integer, intent(in) :: p
    real(dp), intent(in) :: x
    logical, intent(in) :: deriv

    if (.not.deriv) then
      select case (p)
        case(0)
          phi = -2.0_dp*sqrt(3.0_dp/2.0_dp)
        case(1)
          phi = -2.0_dp*sqrt(5.0_dp/2.0_dp)*x
        case(2)
          phi = -(1.0_dp/2.0_dp)*sqrt(7.0_dp/2.0_dp)*(5.0_dp*(x**2) - 1.0_dp)
        case(3)
          phi = -(1.0_dp/2.0_dp)*sqrt(9.0_dp/2.0_dp)*(7.0_dp*(x**2) - 3.0_dp)*x
        case(4)
          phi = -(1.0_dp/4.0_dp)*sqrt(11.0_dp/2.0_dp)*(21.0_dp*(x**4) - 14.0_dp*(x**2) + 1.0_dp)
        case(5)
          phi = -(1.0_dp/4.0_dp)*sqrt(13.0_dp/2.0_dp)*(33.0_dp*(x**4) - 30.0_dp*(x**2) + 5.0_dp)*x
        case(6)
          phi = -(1.0_dp/32.0_dp)*sqrt(15.0_dp/2.0_dp)*(429.0_dp*(x**6) - 495.0_dp*(x**4) + 135.0_dp*(x**2) - 5.0_dp)
        case(7)
          phi = -(1.0_dp/32.0_dp)*sqrt(17.0_dp/2.0_dp)*(715.0_dp*(x**6) - 1001.0_dp*(x**4) + 385.0_dp*(x**2) - 35.0_dp)*x
        case(8)
          phi =   -(1.0_dp/64.0_dp)*sqrt(19.0_dp/2.0_dp)*(2431.0_dp*(x**8) &
                - 4004.0_dp*(x**6) + 2002.0_dp*(x**4) - 308.0_dp*(x**2) + 7.0_dp)
        case default
          error stop "NOT IMPLEMENTED: phi function of order > 8"
      end select

    else
      select case (p)
        case(0)
          phi = 0.0_dp
        case(1)
          phi = -2.0_dp*sqrt(5.0_dp/2.0_dp)
        case(2)
          phi = -(1.0_dp/2.0_dp)*sqrt(7.0_dp/2.0_dp)*(2.0_dp*5.0_dp*(x**1))
        case(3)
          phi = -(1.0_dp/2.0_dp)*sqrt(9.0_dp/2.0_dp)*(3.0_dp*7.0_dp*(x**2) - 3.0_dp)
        case(4)
          phi = -(1.0_dp/4.0_dp)*sqrt(11.0_dp/2.0_dp)*(4.0_dp*21.0_dp*(x**3) - 2.0_dp*14.0_dp*(x**1))
        case(5)
          phi = -(1.0_dp/4.0_dp)*sqrt(13.0_dp/2.0_dp)*(5.0_dp*33.0_dp*(x**4) - 3.0_dp*30.0_dp*(x**2) + 5.0_dp)
        case(6)
          phi = -(1.0_dp/32.0_dp)*sqrt(15.0_dp/2.0_dp)*(6.0_dp*429.0_dp*(x**5) - 4.0_dp*495.0_dp*(x**3) + 2.0_dp*135.0_dp*(x**1))
        case(7)
          phi =   -(1.0_dp/32.0_dp)*sqrt(17.0_dp/2.0_dp)*(7.0_dp*715.0_dp*(x**6) &
                - 5.0_dp*1001.0_dp*(x**4) + 3.0_dp*385.0_dp*(x**2) - 35.0_dp)
        case(8)
          phi =   -(1.0_dp/64.0_dp)*sqrt(19.0_dp/2.0_dp)*(8.0_dp*2431.0_dp*(x**7) - 6.0_dp*4004.0_dp*(x**5) &
                + 4.0_dp*2002.0_dp*(x**3) - 2.0_dp*308.0_dp*(x**1))
        case default
          error stop "NOT IMPLEMENTED: phi function of order > 8"
      end select
    endif
  end function phi
end submodule local_elem__old

