module dof_handler
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use binary_tree, only: int_set
  use code_const, only: UNINITIALIZED_INT
  use exceptions, only: error
  use mesh_objects, only: elem_list, elem_ptr, elem_id, face_list, face_ptr
  use mesh_partitioning, only: elem_and_nghb_in_same_partition, my_first_last_elems
  use mesh_type, only: fem_mesh, mesh_state
  use petsc_mod, only: I_am_master_process
  implicit none
  
  private
  public :: dof, dof_offset_within_elem
  public :: get_dof
  public :: dof_handler_type
  public :: set_type_and_preallocate
  public :: init_dof_handler, init_dof_handler_inhom_orders, init_dof_handler_from_mesh_state
  public :: get_indexes_of_ghost_points
  
  type dof_handler_type
    ! structure containing the generic dof handler
    ! single/multi group cases can be handled by changing no_grps
    ! multi-ordinate and diffusion can be handled by changing no_dirs
    integer :: no_grps
    integer :: no_dirs
    integer :: no_dof_1grp
    integer :: no_dof_mgrp
    integer :: first_dof_in_partition
    integer :: last_dof_in_partition
    integer, allocatable :: order(:), no_nod_elem(:)  !! The size equals the number of elems in all processes.
    integer, allocatable :: elem_start_index(:)       !! The size equals the number of elems in all processes.
    logical :: only_fluid_mesh                        !! Are the DOF's all in the fluid?
  contains
    procedure, non_overridable :: no_dof_in_elem
    procedure, non_overridable :: flat_dof
    procedure, non_overridable :: no_dof_in_partition, total_ndofs_per_polorder
  end type dof_handler_type
  
  interface dof
    module procedure dof_one_node
    module procedure dof_all_nodes
    module procedure dof_all_nodes_and_dirs
  end interface dof
  
  interface get_dof
    module procedure get_dof_one_node
    module procedure get_dof_all_nodes
    module procedure get_dof_all_nodes_and_dirs
  end interface get_dof
  
contains
  elemental integer function no_dof_in_elem(this,elem)
    class(dof_handler_type), intent(in) :: this
    integer, intent(in) :: elem

    no_dof_in_elem = this%no_nod_elem(elem) * this%no_dirs
  end function

  pure function flat_dof(this,gr,elem)
    class(dof_handler_type), intent(in) :: this
    integer, intent(in) :: gr, elem
    integer :: flat_dof(this%no_dof_in_elem(elem))
    
    associate(dofs => dof_all_nodes_and_dirs(this,gr,elem))
      flat_dof = reshape(dofs,[size(dofs)])
    end associate
  end function flat_dof
  
  pure integer function no_dof_in_partition(this)
    class(dof_handler_type), intent(in) :: this
    
    no_dof_in_partition = this%last_dof_in_partition - this%first_dof_in_partition + 1
  end function no_dof_in_partition
  
  function total_ndofs_per_polorder(this) result(totals)
    ! The p'th entry in the output array is the total number of DOF's in 
    ! elements with order `p`.
    ! Returns a total for all elements, not just those in my processes.
    class(dof_handler_type), intent(in) :: this
    integer :: totals(0:maxval(this%order))

    integer :: elem_no, p

    totals = 0
    do elem_no = 1, assert_eq(size(this%order),size(this%no_nod_elem))
      p = this%order(elem_no)
      totals(p) = totals(p) + this%no_nod_elem(elem_no)
    enddo
  end function total_ndofs_per_polorder

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  pure integer function dof_offset_within_elem(handler,elem,dir,node) result(offset)
    ! Returns a number 'f' in the range
    !     0 <= f <= (total #DOF in elem) .
    type(dof_handler_type), intent(in) :: handler
    integer, intent(in) :: elem, dir, node

    offset = (dir-1) * handler%no_nod_elem(elem) + node - 1
  end function

  pure integer function dof_one_node(handler,gr,elem,dir,node) result(dof)
    type(dof_handler_type), intent(in) :: handler
    integer, intent(in) :: gr, elem, dir, node
    
    dof =                                               &
        (gr-1) * handler%no_dof_1grp                    &
        + handler%elem_start_index(elem)                &
        + dof_offset_within_elem(handler,elem,dir,node)
  end function dof_one_node
  
  pure function dof_all_nodes(handler,gr,elem,dir) result(dofs)
    type(dof_handler_type), intent(in) :: handler
    integer, intent(in) :: gr, elem, dir
    integer, dimension(handler%no_nod_elem(elem)) :: dofs
    
    integer :: node
    
    do node = 1, size(dofs)
      dofs(node) = dof_one_node(handler,gr,elem,dir,node)
    enddo
  end function dof_all_nodes
  
  pure function dof_all_nodes_and_dirs(handler,gr,elem) result(dofs)
    type(dof_handler_type), intent(in) :: handler
    integer, intent(in) :: gr, elem
    integer, dimension(handler%no_nod_elem(elem),handler%no_dirs) :: dofs
    
    integer :: dir
    
    do dir = 1, size(dofs,2)
      dofs(:,dir) = dof_all_nodes(handler,gr,elem,dir)
    enddo
  end function dof_all_nodes_and_dirs
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  pure subroutine get_dof_one_node(handler,gr,elem,dir,node,dof)
    type(dof_handler_type), intent(in) :: handler
    integer, intent(in) :: gr
    integer, intent(in) :: elem
    integer, intent(in) :: dir
    integer, intent(in) :: node
    integer, intent(out) :: dof
    
    dof = dof_one_node(handler,gr,elem,dir,node)
  end subroutine get_dof_one_node
  
  pure subroutine get_dof_all_nodes(handler,gr,elem,dir,dofs)
    type(dof_handler_type), intent(in) :: handler
    integer, intent(in) :: gr
    integer, intent(in) :: elem
    integer, intent(in) :: dir
    integer, allocatable, dimension(:), intent(out) :: dofs
    
    allocate(dofs(handler%no_nod_elem(elem)))
    dofs = dof_all_nodes(handler,gr,elem,dir)
  end subroutine get_dof_all_nodes
  
  pure subroutine get_dof_all_nodes_and_dirs(handler,gr,elem,dofs)
    type(dof_handler_type), intent(in) :: handler
    integer, intent(in) :: gr
    integer, intent(in) :: elem
    integer, allocatable, dimension(:,:), intent(out) :: dofs
    
    allocate(dofs(handler%no_nod_elem(elem),handler%no_dirs))
    dofs = dof_all_nodes_and_dirs(handler,gr,elem)
  end subroutine get_dof_all_nodes_and_dirs
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine init_dof_handler(handler,mesh,no_grps,no_dirs,order,only_fluid_mesh)
    type(dof_handler_type), intent(out) :: handler
    type(fem_mesh), intent(in) :: mesh
    integer, intent(in) :: no_grps, no_dirs
    integer, intent(in) :: order
    logical, intent(in) :: only_fluid_mesh

    integer :: nelem
    integer, allocatable :: orders(:)

    nelem = mesh%get_no_active_elem(only_fluid_mesh)
    allocate(orders(nelem), source = order)
    call init_dof_handler_inhom_orders(handler,mesh,no_grps,no_dirs,orders,only_fluid_mesh)
  end subroutine init_dof_handler

  subroutine init_dof_handler_inhom_orders(handler,mesh,no_grps,no_dirs,orders,only_fluid_mesh)
    use local_elem, only: no_dg_functions
    
    type(dof_handler_type), intent(out) :: handler
    type(fem_mesh), intent(in) :: mesh
    integer, intent(in) :: no_grps, no_dirs
    integer, intent(in) :: orders(:)
    logical, intent(in) :: only_fluid_mesh
    
    integer :: active_elem
    integer :: first_elem, last_elem
    integer :: eltype
    integer :: no_active_elem
    type(elem_id) :: id
    type(elem_id), pointer, dimension(:) :: active_elem_list
    type(elem_list), pointer :: elem
    
    handler%only_fluid_mesh = only_fluid_mesh
    handler%no_grps = no_grps
    handler%no_dirs = no_dirs
    
    no_active_elem = assert_eq(size(orders),mesh%get_no_active_elem(handler%only_fluid_mesh))
    call mesh%get_active_elem_list_ptr(handler%only_fluid_mesh,active_elem_list)
    
    allocate(handler%order(no_active_elem))
    allocate(handler%no_nod_elem(no_active_elem))
    allocate(handler%elem_start_index(no_active_elem))
    handler%order = orders
    
    do active_elem = 1, no_active_elem
      id = active_elem_list(active_elem)
      elem => mesh%get_elem_ptr(id)
      eltype = elem%eltype
      handler%no_nod_elem(active_elem) = no_dg_functions(eltype,orders(active_elem))
    enddo
    
    handler%elem_start_index(1) = 1
    do active_elem = 2, no_active_elem
      handler%elem_start_index(active_elem) = handler%elem_start_index(active_elem-1) + handler%no_nod_elem(active_elem-1)*no_dirs
    enddo
    
    handler%no_dof_1grp = 0
    do active_elem = 1, no_active_elem
      handler%no_dof_1grp = handler%no_dof_1grp + no_dirs*handler%no_nod_elem(active_elem)
    enddo
    
    handler%no_dof_mgrp = no_grps*handler%no_dof_1grp
    
    ! Determine first and last dof in partition
    call my_first_last_elems(handler%only_fluid_mesh,first_elem,last_elem)
    handler%first_dof_in_partition = minval(dof(handler,1,first_elem))
    handler%last_dof_in_partition = maxval(dof(handler,1,last_elem))
  end subroutine init_dof_handler_inhom_orders
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine init_dof_handler_from_mesh_state(mesh,mesh_st,no_grps,no_dirs,order,elem_list_type,handler)
    use local_elem, only: no_dg_functions
    
    type(fem_mesh), intent(in) :: mesh
    type(mesh_state), intent(in) :: mesh_st
    integer, intent(in) :: no_grps, no_dirs
    integer, intent(in) :: order
    character(*), intent(in) :: elem_list_type
    type(dof_handler_type), intent(out) :: handler
    
    integer :: elem_no, no_elems
    type(elem_id) :: id
    type(elem_id), allocatable :: elems_list(:)
    type(elem_list), pointer :: elem
    
    select case(elem_list_type)
      case('total_mesh')
        handler%only_fluid_mesh = .false.
        no_elems = mesh_st%tot_no_elem
        allocate(elems_list,source=mesh_st%tot_elem_list)
      case('active_mesh')
        handler%only_fluid_mesh = .false.
        no_elems = mesh_st%no_active_elem
        allocate(elems_list,source=mesh_st%active_elem_list)
      case('fluid_mesh')
        handler%only_fluid_mesh = .true.
        no_elems = mesh_st%no_active_fluid_elem
        allocate(elems_list,source=mesh_st%active_fluid_elem_list)
      case default
        call error('Unknown mesh element list type:'//elem_list_type)
    end select
    
    allocate(handler%no_nod_elem(no_elems))
    allocate(handler%elem_start_index(no_elems))
    allocate(handler%order(no_elems))
    handler%order = order
    handler%no_grps = no_grps
    handler%no_dirs = no_dirs
    
    do elem_no = 1, no_elems
      id = elems_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      handler%no_nod_elem(elem_no) = no_dg_functions(elem%eltype,handler%order(elem_no))
    enddo
    
    handler%elem_start_index(1) = 1
    do elem_no = 2, no_elems
      handler%elem_start_index(elem_no) = handler%elem_start_index(elem_no-1) + handler%no_nod_elem(elem_no-1)*no_dirs
    enddo
    
    handler%no_dof_1grp = 0
    do elem_no = 1, no_elems
      handler%no_dof_1grp = handler%no_dof_1grp + no_dirs*handler%no_nod_elem(elem_no)
    enddo
    
    handler%no_dof_mgrp = no_grps*handler%no_dof_1grp
  end subroutine init_dof_handler_from_mesh_state

  subroutine build_dof_handler_after_refinement(mesh,handler_old,handler_new)
    ! Takes a dof_handler and builds another for the refined mesh
    ! TODO: take into account that handler_old could be defined only in the fluid mesh
    type(fem_mesh), intent(in) :: mesh
    type(dof_handler_type), intent(in) :: handler_old
    type(dof_handler_type), intent(out) :: handler_new
    
    integer :: child_index
    integer :: active_elem
    integer :: first_elem, last_elem
    integer :: no_grps
    integer :: no_dirs
    integer :: elem_no_new
    integer :: elem_no_old
    type(elem_id) :: id_old
    type(elem_id) :: id_new
    type(elem_list), pointer :: elem_old
    type(elem_list), pointer :: elem_new
    logical :: casei
    
    no_grps = handler_old%no_grps
    no_dirs = handler_old%no_dirs
    handler_new%only_fluid_mesh = handler_old%only_fluid_mesh
    handler_new%no_grps = handler_old%no_grps
    handler_new%no_dirs = handler_old%no_dirs
    allocate(handler_new%order(mesh%no_active_elem))
    allocate(handler_new%no_nod_elem(mesh%no_active_elem))
    allocate(handler_new%elem_start_index(mesh%no_active_elem))
    
    elem_no_new = 1
    do elem_no_old = 1, mesh%old_no_active_elem
      id_old = mesh%old_active_elem_list(elem_no_old)
      id_new = mesh%active_elem_list(elem_no_new)
      elem_old => mesh%get_elem_ptr(id_old)
      elem_new => mesh%get_elem_ptr(id_new)
      casei = associated(elem_old,elem_new)
      
      if (casei) then
        ! case i: element has not been refined and is in both the old and new sets
        handler_new%order(elem_no_new) = handler_old%order(elem_no_old)
        handler_new%no_nod_elem(elem_no_new) = handler_old%no_nod_elem(elem_no_old)
        elem_no_new = elem_no_new + 1
      else
        ! case ii: element in new set must come from refinement of elem_old
        ! Implicitly we assume that the order of the solutions on these elements are
        ! equal.
        do child_index = 1, size(elem_old%children)
          handler_new%order(elem_no_new) = handler_old%order(elem_no_old)
          handler_new%no_nod_elem(elem_no_new) = handler_old%no_nod_elem(elem_no_old)
          elem_no_new = elem_no_new + 1
        enddo
      endif
    enddo
    
    handler_new%elem_start_index(1) = 1
    do active_elem = 2, mesh%no_active_elem
      handler_new%elem_start_index(active_elem) =   handler_new%elem_start_index(active_elem-1) &
                                                  + handler_new%no_nod_elem(active_elem-1)*no_dirs
    enddo
    
    handler_new%no_dof_1grp = 0
    do active_elem = 1, mesh%no_active_elem
      handler_new%no_dof_1grp = handler_new%no_dof_1grp + no_dirs*handler_new%no_nod_elem(active_elem)
    enddo
    
    handler_new%no_dof_mgrp = no_grps*handler_new%no_dof_1grp
    
    ! Determine first and last dof in partition
    call my_first_last_elems(handler_new%only_fluid_mesh,first_elem,last_elem)
    handler_new%first_dof_in_partition = minval(dof(handler_new,1,first_elem))
    handler_new%last_dof_in_partition = maxval(dof(handler_new,1,last_elem))
  end subroutine build_dof_handler_after_refinement
  
  subroutine build_dof_handler_with_high_p(mesh,handler_old,handler_new)
    ! TODO: take into account that handler_old could be defined only in the fluid mesh
    use local_elem, only: no_dg_functions
    
    type(fem_mesh), intent(in) :: mesh
    type(dof_handler_type), intent(in) :: handler_old
    type(dof_handler_type), intent(out) :: handler_new
    
    integer :: active_elem
    integer :: first_elem, last_elem
    integer :: eltype
    integer :: new_order
    integer :: no_grps
    integer :: no_dirs
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    
    no_grps = handler_old%no_grps
    no_dirs = handler_old%no_dirs
    handler_new%only_fluid_mesh = handler_old%only_fluid_mesh
    handler_new%no_grps = handler_old%no_grps
    handler_new%no_dirs = handler_old%no_dirs
    allocate(handler_new%order(size(handler_old%order)))
    allocate(handler_new%no_nod_elem(size(handler_old%no_nod_elem)))
    allocate(handler_new%elem_start_index(size(handler_old%elem_start_index)))
    
    do active_elem = 1, mesh%no_active_elem
      id = mesh%active_elem_list(active_elem)
      elem => mesh%get_elem_ptr(id)
      eltype = elem%eltype
      new_order = handler_old%order(active_elem) + 1
      handler_new%order(active_elem) = new_order
      handler_new%no_nod_elem(active_elem) = no_dg_functions(eltype,new_order)
    enddo
    
    handler_new%elem_start_index(1) = 1
    do active_elem = 2, mesh%no_active_elem
      handler_new%elem_start_index(active_elem) =   handler_new%elem_start_index(active_elem-1) &
                                                  + handler_new%no_nod_elem(active_elem-1)*no_dirs
    enddo
    
    handler_new%no_dof_1grp = 0
    do active_elem = 1, mesh%no_active_elem
      handler_new%no_dof_1grp = handler_new%no_dof_1grp + no_dirs*handler_new%no_nod_elem(active_elem)
    enddo
    
    handler_new%no_dof_mgrp = no_grps*handler_new%no_dof_1grp
    
    ! Determine first and last dof in partition
    call my_first_last_elems(handler_new%only_fluid_mesh,first_elem,last_elem)
    handler_new%first_dof_in_partition = minval(dof(handler_new,1,first_elem))
    handler_new%last_dof_in_partition = maxval(dof(handler_new,1,last_elem))
  end subroutine build_dof_handler_with_high_p
  
  subroutine set_type_and_preallocate(mat, mesh, rowh, colh, coupling_depth, symm, force_aij)
    ! Automatically chooses a PETSc Mat type, and preallocates space for the
    ! matrix, given its row and column spaces.
    ! 
    ! `mat` must already have been created.
    ! 
    ! coupling_depth = 0:  DOFs within an element are coupled, but no inter-element coupling
    ! coupling_depth = 1:  coupling between the DOFs of an element and its direct neighbors
    ! 
    use petsc_mat_mod, only: petsc_mat_type, &
        mat_type_t, AIJ, BAIJ, SBAIJ, &
        SYMMETRIC, SYMMETRY_ETERNAL, SPD

    type(fem_mesh), intent(in) :: mesh
    type(petsc_mat_type), intent(inout) :: mat
    type(dof_handler_type) :: rowh, colh
    integer, intent(in) :: coupling_depth
    logical, intent(in) :: symm, force_aij

    type(mat_type_t) :: mattype
    logical :: only_fluid
    integer :: row_bs, col_bs, petsc_bs
    logical :: row_homogeneous_orders, col_homogeneous_orders
    integer :: my_nrows, my_ncols
    type(int_set), allocatable :: elems_d(:), elems_o(:), elems_du(:), elems_ou(:)
    integer, allocatable :: d_nnz(:), o_nnz(:), d_nnz_upper(:), o_nnz_upper(:)
    
    call assert(coupling_depth >= 0, "set_type_and_preallocate: `coupling_depth < 0` indicates that the LHS matrix is empty.")
    only_fluid = assert_eq(rowh%only_fluid_mesh, colh%only_fluid_mesh)

    row_homogeneous_orders = all(rowh%no_nod_elem(1) == rowh%no_nod_elem(2:))
    col_homogeneous_orders = all(colh%no_nod_elem(1) == colh%no_nod_elem(2:))
    if (row_homogeneous_orders .and. col_homogeneous_orders) then
      row_bs = rowh%no_nod_elem(1) * rowh%no_dirs
      col_bs = colh%no_nod_elem(1) * colh%no_dirs
      if (row_bs /= col_bs) then
        petsc_bs = 1
      else
        petsc_bs = row_bs
      endif
    else
      if (i_am_master_process()) write(*,'(a)') 'inhomogeneous number of nodes => falling back to block size 1 and mattype AIJ'
      row_bs = 1
      col_bs = 1
      petsc_bs = 1
    endif

    if (petsc_bs == 1 .or. force_aij) then
      ! PETSc can still detect rows with an identical nonzero structure ('inodes'),
      ! but only with an AIJ matrix (and no more than 5 sequential rows).
      mattype = aij
    else
      mattype = merge(sbaij, baij, symm)
    endif
    call mat%set_type(mattype)
    call mat%set_from_options()
    
    my_nrows = rowh%no_dof_in_partition()
    my_ncols = colh%no_dof_in_partition()
    call mat%set_sizes(my_nrows, my_ncols)

    select case(petsc_bs)
      case(1)
        call get_no_nonzeros_in_rows(mesh, rowh, colh, coupling_depth, d_nnz, o_nnz, d_nnz_upper, o_nnz_upper)
      case default
        call assert(row_bs == col_bs, "`get_sparsity_pattern` is based on square blocks.")
        call get_sparsity_pattern(mesh, only_fluid, coupling_depth, elems_d, elems_o, elems_du, elems_ou)
        d_nnz = elems_d%no_nodes()
        o_nnz = elems_o%no_nodes()
        d_nnz_upper = elems_du%no_nodes()
        o_nnz_upper = elems_ou%no_nodes()
        mat%assemble_elemmat_as_block = .true.
    end select
    call mat%XAIJ_set_preallocation(petsc_bs, dnnz=d_nnz, onnz=o_nnz, dnnzu=d_nnz_upper, onnzu=o_nnz_upper)
    if (mattype%t == aij%t) call mat%set_block_sizes(row_bs, col_bs)
    call mat%set_essential_options()
    if (symm) then
      call mat%set_option(SYMMETRIC,.true.)
      call mat%set_option(SYMMETRY_ETERNAL,.true.)
      call mat%set_option(SPD,.true.)
    endif

    call mat%zero_entries()
  end subroutine

  subroutine get_no_nonzeros_in_rows(mesh, handler_rows, handler_cols, coupling_depth, &
                                    d_nnz, o_nnz, d_nnz_upper, o_nnz_upper)
    type(fem_mesh), intent(in) :: mesh
    type(dof_handler_type), intent(in) :: handler_rows, handler_cols
    integer, intent(in) :: coupling_depth
    integer, allocatable, intent(out) :: d_nnz(:), o_nnz(:), d_nnz_upper(:), o_nnz_upper(:)
    
    logical :: only_fluid
    integer :: my_nrows
    integer :: first_elem, last_elem
    type(int_set), allocatable :: elems_d(:), elems_o(:), elems_du(:), elems_ou(:)
    integer :: elem_no
    integer :: elem_nnz_d, elem_nnz_o, elem_nnz_du, elem_nnz_ou
    integer :: dir, node, row
    
    only_fluid = assert_eq(handler_rows%only_fluid_mesh, handler_cols%only_fluid_mesh)
    
    my_nrows = handler_rows%no_dof_in_partition()
    ! Something should go wrong just in case we miss one of the rows:
    allocate(d_nnz(my_nrows), source=UNINITIALIZED_INT)
    allocate(o_nnz(my_nrows), source=UNINITIALIZED_INT)
    allocate(d_nnz_upper(my_nrows), source=UNINITIALIZED_INT)
    allocate(o_nnz_upper(my_nrows), source=UNINITIALIZED_INT)
    
    call get_sparsity_pattern(mesh, only_fluid, coupling_depth, elems_d, elems_o, elems_du, elems_ou)
    call my_first_last_elems(only_fluid, first_elem, last_elem)
    do elem_no = first_elem, last_elem
      elem_nnz_d = sum_ndofs_elemset(elems_d(elem_no))
      elem_nnz_o = sum_ndofs_elemset(elems_o(elem_no))
      elem_nnz_du = sum_ndofs_elemset(elems_du(elem_no))
      elem_nnz_ou = sum_ndofs_elemset(elems_ou(elem_no))
      
      do dir = 1, handler_rows%no_dirs
        do node = 1, handler_rows%no_nod_elem(elem_no)
          row = dof(handler_rows,1,elem_no,dir,node) - handler_rows%first_dof_in_partition + 1
          d_nnz(row) = elem_nnz_d
          o_nnz(row) = elem_nnz_o
          d_nnz_upper(row) = elem_nnz_du
          o_nnz_upper(row) = elem_nnz_ou
        enddo
      enddo
    enddo

  contains
    integer function sum_ndofs_elemset(elemset)
      type(int_set), intent(in) :: elemset
      
      integer, allocatable :: elem_no(:)

      call elemset%sorted_values(elem_no)
      sum_ndofs_elemset = sum(handler_cols%no_dof_in_elem(elem_no))
    end function
  end subroutine

  subroutine get_sparsity_pattern(mesh, only_fluid, coupling_depth, elems_d, elems_o, elems_du, elems_ou)
    type(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid
    integer, intent(in) :: coupling_depth
    type(int_set), allocatable, intent(out) :: elems_d(:), elems_o(:), elems_du(:), elems_ou(:)
    
    type(int_set), allocatable :: nsets(:)
    integer :: elem_no, first_elem, last_elem, i, c_no
    integer, allocatable :: coupled_elems(:)
    logical :: mine, upper_triangle

    call list_coupled_elems(mesh, only_fluid, coupling_depth, nsets)

    call my_first_last_elems(only_fluid, first_elem, last_elem)
    allocate(elems_d (first_elem:last_elem))
    allocate(elems_o (first_elem:last_elem))
    allocate(elems_du(first_elem:last_elem))
    allocate(elems_ou(first_elem:last_elem))

    do elem_no = first_elem, last_elem
      call nsets(elem_no)%sorted_values(coupled_elems)
      do i = 1, size(coupled_elems)
        c_no = coupled_elems(i)
        mine = first_elem <= c_no .and. c_no <= last_elem
        upper_triangle = c_no >= elem_no  ! not the strictly upper triangular part
        if (mine) then
          call elems_d(elem_no)%insert(c_no)
          if (upper_triangle) call elems_du(elem_no)%insert(c_no)
        else
          call elems_o(elem_no)%insert(c_no)
          if (upper_triangle) call elems_ou(elem_no)%insert(c_no)
        endif
      enddo
    enddo
  end subroutine

  subroutine list_coupled_elems(mesh, only_fluid, coupling_depth, nsets)
    type(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid
    integer, intent(in) :: coupling_depth
    type(int_set), allocatable, intent(out) :: nsets(:)

    integer :: elem_no, first_elem, last_elem
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_id) :: id
    type(elem_list), pointer :: elem

    call my_first_last_elems(only_fluid, first_elem, last_elem)

    allocate(nsets(first_elem:last_elem))

    call mesh%get_active_elem_list_ptr(only_fluid, active_elem_list)
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)

      if (coupling_depth < 0) then
        ! `coupling_depth < 0` indicates that there is no lhs in the global linear system.
      elseif (coupling_depth == 0) then
        call nsets(elem_no)%insert(elem_no)
      elseif (coupling_depth == 1) then
        call nghb_numbers(elem, only_fluid, nsets(elem_no))
      else
        call error('NOT IMPLEMENTED: coupling_depth > 1') ! could be done recursively after listing neighbors for all elems
      endif
    enddo
  end subroutine

  subroutine nghb_numbers(elem, only_fluid, nset)
    ! Lists the numbers of all neighbors, including that of `elem` itself.
    ! 
    type(elem_list), intent(in) :: elem
    logical, intent(in) :: only_fluid
    type(int_set), intent(out) :: nset

    integer :: elem_no, nghb_no
    type(elem_ptr), allocatable :: active_neighbors(:)
    type(face_ptr), allocatable :: active_subfaces(:)
    type(face_list), pointer :: sub_face
    type(elem_list), pointer :: nghb
    integer :: i
    
    elem_no = elem%get_active_elem_no(only_fluid)
    call nset%insert(elem_no)

    call elem%active_subfaces(active_subfaces, active_neighbors)
    do i = 1, size(active_subfaces)
      sub_face => active_subfaces(i)%point
      if (.not. (sub_face%is_boundary(only_fluid))) then
        nghb => active_neighbors(i)%point
        if (only_fluid .and. .not. nghb%is_fluid()) cycle

        nghb_no = nghb%get_active_elem_no(only_fluid)
        call nset%insert(nghb_no)
      endif
    enddo
  end subroutine
  
  subroutine get_indexes_of_ghost_points(mesh,handler,ghost_idxs)
    type(fem_mesh), intent(in) :: mesh
    type(dof_handler_type), intent(in) :: handler
    integer, allocatable, intent(out) :: ghost_idxs(:)
    
    type(elem_id) :: id
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_list), pointer :: elem, nghb
    type(face_list), pointer :: sub_face
    type(elem_ptr), allocatable :: active_neighbors(:)
    type(face_ptr), allocatable :: active_subfaces(:)
    integer :: first_elem, last_elem
    integer :: elem_no, face_no, nghb_no
    integer :: idx_start, idx_end
    integer :: no_ghosts
    
    call mesh%get_active_elem_list_ptr(handler%only_fluid_mesh,active_elem_list)
    call my_first_last_elems(handler%only_fluid_mesh,first_elem,last_elem)
    
    ! Calculate how many ghost points we have in the current processor
    no_ghosts = 0
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      call elem%active_subfaces(active_subfaces,active_neighbors)
      do face_no = 1, size(active_subfaces)
        sub_face => active_subfaces(face_no)%point
        if (.not. sub_face%is_boundary(handler%only_fluid_mesh)) then
          nghb => active_neighbors(face_no)%point
          nghb_no = nghb%get_active_elem_no(handler%only_fluid_mesh)
          if (.not. elem_and_nghb_in_same_partition(elem,nghb)) then
            no_ghosts = no_ghosts + handler%no_dof_in_elem(nghb_no)
          endif
        endif
      enddo
    enddo
    
    ! Store ghost indexes
    allocate(ghost_idxs(no_ghosts))
    idx_end = 0
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      call elem%active_subfaces(active_subfaces,active_neighbors)
      do face_no = 1, size(active_subfaces)
        sub_face => active_subfaces(face_no)%point
        if (.not. sub_face%is_boundary(handler%only_fluid_mesh)) then
          nghb => active_neighbors(face_no)%point
          nghb_no = nghb%get_active_elem_no(handler%only_fluid_mesh)
          if (.not. elem_and_nghb_in_same_partition(elem,nghb)) then
            idx_start = idx_end + 1
            idx_end = idx_end + handler%no_dof_in_elem(nghb_no)
            ghost_idxs(idx_start:idx_end) = handler%flat_dof(1,nghb_no)
          endif
        endif
      enddo
    enddo
  end subroutine get_indexes_of_ghost_points
end module dof_handler

