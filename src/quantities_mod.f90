module quantities_mod
  use assertions, only: assert
  use exceptions, only: error
  implicit none

  private
  public :: idx, idx2name
  public :: name2sym, sym2name
  public :: fluid_name, no_dirs

  character(100), parameter :: NAMESYM_LIST(23,2) = transpose(reshape([character(100) ::  &
    'density', 'rho', &
    'density_predictor', 'rhop', &
    'velocity', 'u', &
    'mass_flux', 'm', &
    'vorticity', 'w', &
    'pressure', 'p', &
    'spec_enthalpy', 'h', &
    'vol_enthalpy', 'H', &
    'temperature', 'T', &
    'wall_dist', 'WD', &
    'dynamic_viscosity', 'mu', &
    'kinematic_viscosity', 'nu', &
    'thermal_conductivity', 'k', &
    'thermal_diffusivity', 'alpha', &
    'spec_heat_capacity', 'c_p', &
    'prandtl', 'pr', & 
    'k_over_cp', '(k/c_p)', &
    'vol_heat_capacity', '(d/dT H)', &
    'deriv_H_h', '(d/dh H)', &
    'deriv_rho_h', '(d/dh rho)', &
    'thermal_expansion_coeff', 'beta', &
    'spec_force', 'f', &
    'vol_force', 'F' &
  ],[2,23]))

  integer, parameter, public :: nquantities = size(NAMESYM_LIST,1) + 4 ! because of special treatment of two RANS quantities

contains
  type(string_t) impure elemental function idx2name(idx) result(name)
    use string_mod, only: string_t, as_chars

    integer, intent(in) :: idx
    character(:), allocatable :: char

    call assert(1 <= idx .and. idx <= nquantities,"idx2name: bad idx")
    
    select case(idx)
      case(1:size(NAMESYM_LIST,1));   char = trim(NAMESYM_LIST(idx,1))
      case(size(NAMESYM_LIST,1)+1);   char = 'spec_turb1'
      case(size(NAMESYM_LIST,1)+2);   char = 'spec_turb2'
      case(size(NAMESYM_LIST,1)+3);   char = 'vol_turb1'
      case(size(NAMESYM_LIST,1)+4);   char = 'vol_turb2'
      case default;                   call error("idx2name: bad idx")
    end select
    call name%set(char)
  end function

  integer function idx(name)
    character(*), intent(in) :: name

    select case(name)
      case('spec_turb1');   idx = size(NAMESYM_LIST,1) + 1
      case('spec_turb2');   idx = size(NAMESYM_LIST,1) + 2
      case('vol_turb1');    idx = size(NAMESYM_LIST,1) + 3
      case('vol_turb2');    idx = size(NAMESYM_LIST,1) + 4
      case default
        idx = findloc(NAMESYM_LIST(:,1),name,dim=1)
        call assert(idx /= 0,'idx: cannot find name=' // name)
    end select
  end function

  function sym2name(sym) result(name)
    character(*), intent(in) :: sym
    character(:), allocatable :: name

    integer :: idx

    idx = findloc(NAMESYM_LIST(:,2),sym,dim=1)
    call assert(idx /= 0,'sym2name: cannot find sym=' // sym)
    name = trim(NAMESYM_LIST(idx,1))
  end function sym2name

  function name2sym(name) result(sym)
    use run_data, only: rans_model_description
    
    character(*), intent(in) :: name
    character(:), allocatable :: sym

    character(:), allocatable :: turb1, turb2
    integer :: idx

    if (name == 'vol_turb1' .or. name == 'vol_turb2') then
      select case (rans_model_description)
        case('SA')
          turb1 = 'nu_tilde'
        case('k')
          turb1 = 'rho*k'
        case('k-eps')
          turb1 = 'rho*k'
          turb2 = 'rho*eps'
        case('log-k-log-eps')
          turb1 = 'rho*log_k'
          turb2 = 'rho*log_eps'
        case('k-omega')
          turb1 = 'rho*k'
          turb2 = 'rho*w'
        case('k-log-w')
          turb1 = 'rho*k'
          turb2 = 'rho*log_w'
      end select
      select case(name)
        case('vol_turb1'); sym = turb1
        case('vol_turb2'); sym = turb2
      end select
    else
      idx = findloc(NAMESYM_LIST(:,1),name,dim=1)
      call assert(idx /= 0,'name2sym: cannot find name=' // name)
      sym = trim(NAMESYM_LIST(idx,2))
    endif
  end function name2sym
  
  logical function fluid_name(name)
    character(*), intent(in) :: name
    
    select case(name)
      case('density_predictor','ddt_rho','velocity','mass_flux','pressure', &
           'vol_turb1','vol_turb2','spec_turb1','spec_turb2', &
           'wall_dist','vorticity', &
           'dynamic_viscosity','kinematic_viscosity', 'thermal_expansion_coeff','prandtl', 'deriv_rho_h')
        fluid_name = .true.
      
      case('spec_enthalpy','vol_enthalpy','temperature','density', &
           'spec_heat_capacity','vol_heat_capacity','thermal_conductivity', &
           'thermal_diffusivity','k_over_cp')
        fluid_name = .false.
      case default
        call error('I do not know whether this is a fluid quantity: ' // name)
    end select
  end function fluid_name
  
  integer function no_dirs(qnty_name)
    use run_data, only: mesh_dimen
    
    character(*), intent(in) :: qnty_name
    
    select case(qnty_name)
      case('mass_flux','velocity')
        no_dirs = mesh_dimen
      case('vorticity')
        no_dirs = 3
      case('pressure','density_predictor', &
           'vol_enthalpy','spec_enthalpy','temperature', &
           'wall_dist','vol_turb1','vol_turb2')
        no_dirs = 1
      case default
        call error('no_dirs: unknown qnty_name=' // qnty_name)
    end select
  end function no_dirs
end module quantities_mod

