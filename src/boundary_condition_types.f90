module boundary_condition_types
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only: vec_func_type
  implicit none
  
  private
  public :: bc_type
  public :: dirichlet_bc_type
  public :: neumann_bc_type
  public :: hom_neumann_bc_type, inhom_neumann_bc_type, generalized_neumann_bc_t
  public :: adjusted_traction_bc_t
  public :: robin_bc_type, generalized_robin_bc_t
  public :: slip_bc_type
  public :: null_bc_type
  
  type, abstract :: bc_type
  end type bc_type
  
  type, extends(bc_type) :: null_bc_type
    ! indicates that there is no boundary condition
  end type null_bc_type
  
  type, extends(bc_type) :: dirichlet_bc_type
    class(vec_func_type), allocatable :: inhom_part
  end type dirichlet_bc_type
  
  
  type, extends(bc_type), abstract :: neumann_bc_type
  end type neumann_bc_type
  
  type, extends(neumann_bc_type) :: hom_neumann_bc_type
    ! n \cdot \nabla u = 0
  end type hom_neumann_bc_type 
  
  type, extends(neumann_bc_type) :: inhom_neumann_bc_type
    ! \n cdot K \nabla u = inhom_part
    ! K := diffusion constant
    class(vec_func_type), allocatable :: inhom_part
  end type inhom_neumann_bc_type
  
  type, extends(bc_type) :: generalized_neumann_bc_t
    ! n \cdot stress(u) = inhom_part
    class(vec_func_type), allocatable :: inhom_part
  end type generalized_neumann_bc_t
  
  type, extends(bc_type) :: adjusted_traction_bc_t
    ! (K \nabla u) cdot \n = inhom_part
    ! K := diffusion constant
    class(vec_func_type), allocatable :: inhom_part
  end type adjusted_traction_bc_t
  
  type, extends(bc_type) :: robin_bc_type
    ! "coeff" u + (K \nabla u) cdot \n = inhom_part
    ! K := diffusion constant
    class(scal_func_type), allocatable :: coeff
    class(vec_func_type), allocatable :: inhom_part
  end type robin_bc_type
  
  type, extends(bc_type) :: generalized_robin_bc_t
    ! "coeff" u + n \cdot stress(u) = inhom_part
    class(scal_func_type), allocatable :: coeff
    class(vec_func_type), allocatable :: inhom_part
  end type generalized_robin_bc_t
  
  type, extends(bc_type) :: slip_bc_type
    !This bc is valid only for the momentum equation.
    !It consists of a Dirichlet part (zero normal velocity)
    !plus a Robin part (shear stress proportional to tang velocity)
    !We can store just the slip coefficient
    class(scal_func_type), allocatable :: slip_coeff
  end type slip_bc_type
end module boundary_condition_types
