module matprop_mod
  use assertions, only: assert
  use func_set_mod, only: func_set_t
  use func_set_expansion_mod, only: func_set_expansion_t
  use vec_func_mod, only: vec_func_type
  implicit none
  
  private
  public :: initialize
  public :: all_specified_mat_ids
  public :: expand_func_set_with_matprop
  public :: infer_matprops_from_func_set
  public :: local_id_of_mat_id
  public :: fluid_matprops_ptr, matprops_ptr

  public :: add_material ! used in submodule, so gfortran9 needs it to be public
  
  type material_t
    integer :: id
    type(func_set_expansion_t) :: matprops_exp
  end type material_t
  
  integer, parameter :: MAX_NO_MAT_IDS = 4
  integer :: no_mat_ids = 0
  type(material_t), target :: materials(MAX_NO_MAT_IDS)
  
  interface
    module subroutine initialize()
    end subroutine initialize
  end interface
  
  interface
    module subroutine get_linear_matprop(unit_number,matprop_set)
      implicit none
      integer, intent(in) :: unit_number
      type(func_set_expansion_t), intent(out) :: matprop_set
    end subroutine get_linear_matprop
    
    module subroutine get_boussinesq_matprop(matprop_set)
      implicit none
      type(func_set_expansion_t), intent(out) :: matprop_set
    end subroutine get_boussinesq_matprop
    
    module subroutine get_metal_matprop(unit_number,matprop_set)
      implicit none
      integer, intent(in) :: unit_number
      type(func_set_expansion_t), intent(out) :: matprop_set
    end subroutine get_metal_matprop
    
    module subroutine get_MSFR_salt_matprop(matprop_set)
      implicit none
      type(func_set_expansion_t), intent(out) :: matprop_set
    end subroutine get_MSFR_salt_matprop

    module subroutine get_cp_matprop(unit_number,matprop_set)
      implicit none
      integer, intent(in) :: unit_number
      type(func_set_expansion_t), intent(out) :: matprop_set
    end subroutine get_cp_matprop
  end interface
  
contains
  function all_specified_mat_ids()
    integer :: all_specified_mat_ids(no_mat_ids)

    integer :: i

    do i = 1, no_mat_ids
      all_specified_mat_ids(i) = materials(i)%id
    enddo
  end function all_specified_mat_ids

  subroutine expand_func_set_with_matprop(func_set)
    use func_set_mod, only: func_set_t
    
    type(func_set_t), intent(inout) :: func_set
    
    type(func_set_t) :: matprop_set
    
    call infer_matprops_from_func_set(func_set,matprop_set)
    call func_set%merge_exl_duplicates(matprop_set)
    call func_set%expand_with_known_relations()
  end subroutine expand_func_set_with_matprop
  
  subroutine infer_matprops_from_func_set(func_set,g_matprop_set)
    use func_set_mod, only: func_set_t
    use matprop_local2global_mod, only: build_global_set_from_local_matprop_sets
    use mesh_objects, only: mat_id_is_fluid_id
    
    type(func_set_t), intent(in) :: func_set
    type(func_set_t), intent(out) :: g_matprop_set
    
    integer :: idx
    integer :: material_ids(no_mat_ids)
    type(func_set_t), allocatable :: l_matprop_sets(:)
    type(func_set_t) :: aux_func_set
    
    allocate(l_matprop_sets(no_mat_ids))
    do idx = 1, no_mat_ids
      material_ids(idx) = materials(idx)%id
      aux_func_set = func_set
      if (.not. mat_id_is_fluid_id(materials(idx)%id)) then
        ! The material is not fluid, therefore properties must be inferred
        ! without numerical density, velocity and/or mass_flux, pressure.
        if (func_set%has('density')) aux_func_set = aux_func_set%reduced_set('density')
        if (func_set%has('velocity')) aux_func_set = aux_func_set%reduced_set('velocity')
        if (func_set%has('mass_flux')) aux_func_set = aux_func_set%reduced_set('mass_flux')
        if (func_set%has('pressure')) aux_func_set = aux_func_set%reduced_set('pressure')
      endif
      call materials(idx)%matprops_exp%infer_from_func_set(aux_func_set,l_matprop_sets(idx))
    enddo
    call build_global_set_from_local_matprop_sets(material_ids,l_matprop_sets,g_matprop_set)
  end subroutine infer_matprops_from_func_set
  
  subroutine add_material(id,matprops_exp)
    use mesh_objects, only: mat_id_is_fluid_id

    integer, intent(in) :: id
    type(func_set_expansion_t), intent(in) :: matprops_exp

    call assert(no_mat_ids < MAX_NO_MAT_IDS,'stack overflow: no space for extra material ID')
    call assert(local_id_of_mat_id(id) == 0,'material ID cannot be specified more than once')

    no_mat_ids = no_mat_ids + 1
    associate(mat => materials(no_mat_ids))
      mat%id = id
      mat%matprops_exp = matprops_exp
    end associate
  end subroutine

  function fluid_matprops_ptr()
    use mesh_objects, only: mat_id_is_fluid_id

    type(func_set_expansion_t), pointer :: fluid_matprops_ptr

    integer, allocatable :: all_matids(:)
    logical, allocatable :: is_fluid(:)
    integer :: idx
    
    all_matids = all_specified_mat_ids()
    is_fluid = mat_id_is_fluid_id(all_matids)
    call assert(count(is_fluid) == 1,'There should be exactly one fluid.')
    idx = findloc(is_fluid,.true.,dim=1)
    fluid_matprops_ptr => materials(idx)%matprops_exp
  end function fluid_matprops_ptr

  function matprops_ptr(mat_id)
    integer, intent(in) :: mat_id
    type(func_set_expansion_t), pointer :: matprops_ptr

    integer :: idx

    idx = local_id_of_mat_id(mat_id)
    call assert(idx /= 0,'cannot find matprop for mat_id')
    matprops_ptr => materials(idx)%matprops_exp
  end function matprops_ptr

  pure integer function local_id_of_mat_id(id) result(idx)
    integer, intent(in) :: id
    
    do idx = 1, no_mat_ids
      if (id == materials(idx)%id) return
    enddo
    idx = 0
  end function local_id_of_mat_id
end module matprop_mod
