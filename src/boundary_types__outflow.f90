submodule(boundary_types) boundary_types__outflow
  use assertions, only: assert
  use func_set_mod, only: func_set_t
  use scal_func_mod, only: scal_func_type
  use boundary_condition_types, only: bc_type
  implicit none
  
  type, extends(nonwall_bnd_t) :: outflow_bnd_t
    class(bc_type), allocatable :: momentum_bc
    type(func_set_t) :: knowns
    class(scal_func_type), allocatable :: fourier_heat_flux
  contains
    procedure, nopass :: fixes_absolute_pressure => true
    procedure, nopass :: has_inflow => false
    procedure :: n_dot_velocity_at_qps
    procedure :: read_specs_from_file
    procedure :: get_bc
  end type outflow_bnd_t
  
contains
  module procedure alloc_outflow
    allocate(outflow_bnd_t :: bnd)
  end procedure alloc_outflow
  
  subroutine read_specs_from_file(this,unit_number)
    use boundary_condition_types, only: adjusted_traction_bc_t, generalized_neumann_bc_t
    use exceptions, only: error
    use functions_lib, only: string2func, zero_scalfunc
    use matprop_mod, only: expand_func_set_with_matprop
    use read_funcs_mod, only: read_block_as_func_specs
    use io_basics, only: read_line_from_input_file
    use string_manipulations, only: separate_first_word
    
    class(outflow_bnd_t), intent(out) :: this
    integer, intent(in) :: unit_number
    
    character(:), allocatable :: traction_specs, traction_type, traction
    type(generalized_neumann_bc_t) :: tract_bc
    type(adjusted_traction_bc_t) :: adj_tract_bc
    character(:), allocatable :: fhf_specs, fhf_type, inhom_part
    class(scal_func_type), allocatable :: func
    
    call read_line_from_input_file(unit_number,traction_specs)
    call separate_first_word(traction_specs,traction_type,traction,'=')
    select case(traction_type)
      case('traction')
        call string2func(traction,tract_bc%inhom_part)
        allocate(this%momentum_bc,source=tract_bc)
      case('adjusted_traction')
        call string2func(traction,adj_tract_bc%inhom_part)
        allocate(this%momentum_bc,source=adj_tract_bc)
      case default
        call error('expected type of traction for velocity, but got: ' // traction_type)
    end select

    call read_line_from_input_file(unit_number,fhf_specs)
    if (fhf_specs == '') then
      ! Assume that not specifying the diffusive flux means that it is zero:
      this%fourier_heat_flux = zero_scalfunc()
    else
      call separate_first_word(fhf_specs,fhf_type,inhom_part,'=')
      select case(fhf_type)
        case('temperature')
          call string2func(inhom_part,func)
          call this%knowns%add_func('temperature',func)
          call expand_func_set_with_matprop(this%knowns)
        case('fhf')
          call string2func(inhom_part,this%fourier_heat_flux)
        case default
          call error('expected temperature BC, but got line=' // fhf_specs)
      end select
    endif
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types, only: dirichlet_bc_type, hom_neumann_bc_type, inhom_neumann_bc_type
    use vec_func_scal_compon_mod, only: as_vecfunc
    
    class(outflow_bnd_t), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    
    type(dirichlet_bc_type), allocatable :: dirichlet_bc
    type(inhom_neumann_bc_type), allocatable :: neumann_bc
    
    if (qnty_name == 'velocity' .or. qnty_name == 'mass_flux') then
      allocate(bc,source=this%momentum_bc)
    elseif (qnty_name == 'vol_enthalpy' .or. qnty_name == 'spec_enthalpy' .or. qnty_name == 'temperature') then
      call assert(allocated(this%fourier_heat_flux) .neqv. this%knowns%has(qnty_name),'either heat flux or temperature at outlet')
      if (this%knowns%has(qnty_name)) then
        allocate(dirichlet_bc)
        call this%knowns%get_func(qnty_name,dirichlet_bc%inhom_part)
        call move_alloc(dirichlet_bc,bc)
      else
        ! Assumes you are solving a conservative transport equation, in which 
        ! case the (generalized) Neumann BC is always the same:
        allocate(neumann_bc)
        allocate(neumann_bc%inhom_part,source=as_vecfunc(this%fourier_heat_flux))
        call move_alloc(neumann_bc,bc)
      endif
    else
      allocate(bc,source=hom_neumann_bc_type())
    endif
  end subroutine get_bc

  function n_dot_velocity_at_qps(this,face,fluid_side,time)
    use code_const, only: BOUNDARY
    use f90_kind, only: dp
    use mesh_objects, only: face_list
    use numerical_solutions, only: velocity
    use run_data, only: cap_inflow_at_outlet, min_u_dot_n_at_outflow_bc, mesh_dimen
    use vec_func_mod, only: vec_func_type
    
    class(outflow_bnd_t), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: fluid_side
    integer, intent(in) :: time
    real(dp) :: n_dot_velocity_at_qps(face%nqp)

    real(dp) :: velocity_at_qps(mesh_dimen,face%nqp)
    integer :: p

    call assert(face%ftype == BOUNDARY)
    call assert(fluid_side == 1)

    call velocity%values_at_qps(face,1,time,velocity_at_qps)
    do p = 1, face%nqp
      n_dot_velocity_at_qps(p) = dot_product(face%normal(:,p),velocity_at_qps(:,p))
    end do

    if (minval(n_dot_velocity_at_qps) < min_u_dot_n_at_outflow_bc) then
      block
        use exceptions, only: error
        character(200) :: err_msg
        write(err_msg,"('inflow at outflow BC: (n .dot. u) = ',es8.1,' < ',es8.1)") &
            minval(n_dot_velocity_at_qps), min_u_dot_n_at_outflow_bc
        call error(trim(err_msg))
      end block
    elseif (cap_inflow_at_outlet) then
      n_dot_velocity_at_qps = max(0.0_dp,n_dot_velocity_at_qps)
    endif
  end function n_dot_velocity_at_qps
end submodule
