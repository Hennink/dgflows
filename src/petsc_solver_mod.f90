module petsc_solver_mod
#include "petsc/finclude/petscksp.h"
  use petscksp
  use petscis

  use assertions, only: assert, assert_eq, BOUNDS_CHECK, perform_all_bc
  use exceptions, only: error
  use f90_kind, only: dp
  use petsc_ksp_mod, only: petsc_ksp_t
  use petsc_mod, only: i_am_master_process
  use petsc_pc_mod, only: petsc_pc_t
  use petsc_vec_mod, only: petsc_vec_type
  use petsc_mat_mod, only: petsc_mat_type
  use timer_typedef, only: timer_type
  implicit none
  
  private
  public :: solve_system
  public :: set_up
  public :: set_up_FIELDSPLIT_solver
  
  type(timer_type), public, protected :: solve_timer

contains
  subroutine set_up(ksp, prefix)
    type(petsc_ksp_t), intent(inout) :: ksp
    character(*), intent(in) :: prefix
    
    call ksp%create()

    call ksp%set_initial_guess_nonzero(.true.)
    call ksp%converged_default_set_UIR_norm()
    
    call ksp%set_options_prefix(prefix // '_')
  end subroutine
  
  subroutine solve_system(ksp, x, rhs)
    ! Solves the system of equations with a given right hand side
    ! Initial guess can be provided in x
    use code_const, only: UNINITIALIZED_CHAR
    
    type(petsc_ksp_t), intent(inout) :: ksp
    type(petsc_vec_type), intent(inout) :: x
    type(petsc_vec_type), intent(in) :: rhs
    
    character(100), save :: printed_prefixes(10) = UNINITIALIZED_CHAR
    integer, save :: nprinted = 0
    character(:), allocatable :: prefix

    call solve_timer%start()
    prefix = ksp%options_prefix()

    if (.not. any(prefix == printed_prefixes)) call ksp%set_from_options() ! can be called now that an operator has been set
    call ksp%solve(b=rhs,x=x)
    
    if (.not. any(prefix == printed_prefixes)) then
      if (i_am_master_process()) then
        write(*,'(a)') '------------ This KSP structure was used to solve for quantity with prexix: ' // prefix // ' -----------'
      endif
      call ksp%view()
      if (i_am_master_process()) then
        write(*,'(a)') '-------------------------------- END OF KSP STRUCTURE ----------------------' // '--'   // '------------'
      endif

      nprinted = nprinted + 1
      call assert(nprinted <= size(printed_prefixes), "I did not expect so many different KSP prefixes. The last prefix="//prefix)
      printed_prefixes(nprinted) = prefix
    endif
    
    call ksp%converged_reason_view()
    if (i_am_master_process()) write(*,'(a,es10.3)') prefix //  ' Krylov residual norm = ', ksp%residual_norm()
    call solve_timer%pauze()
  end subroutine
 
  
  subroutine set_up_FIELDSPLIT_solver(ksp, mat, precond, field_names, field_ISs, approx_S)
    use petsc_is_mod, only: petsc_is_t
    use petsc_basic_types_mod, only: petsc2fort, fort2petsc
    use run_data, only: use_approx_schur_PC, use_approx_schur_KSP
    
    interface
      subroutine PCFieldSplitGetSubKSP(pc, n, subksp, ierr)
        use petscpc
        implicit none
        PC, intent(in) :: pc
        PetscInt :: n       ! used as input, unless `subksp` is not null
        KSP :: subksp(*)    ! can be null, in which case `n` is set
        PetscErrorCode, intent(out) :: ierr
      end subroutine
    end interface
    type(petsc_ksp_t), intent(inout) :: ksp
    type(petsc_mat_type), intent(in) :: mat, precond
    character(*), intent(in) :: field_names(:)
    type(petsc_is_t), intent(in) :: field_ISs(:)
    type(petsc_mat_type), intent(in) :: approx_S
    
    integer :: i, nfields
    type(petsc_pc_t) :: parallel_pc
    type(tKSP), allocatable :: subksps(:)
    PetscInt :: n_subksps
    PetscErrorCode :: ierr
    
    call ksp%create()
    call ksp%set_operators(mat,precond)
    call ksp%set_type('fgmres')
    call ksp%get_pc(parallel_pc)
    call parallel_pc%set_type('fieldsplit')
    call ksp%set_from_options()
    call parallel_pc%set_from_options()
    
    nfields = assert_eq(size(field_ISs),size(field_names))
    do i = 1, nfields
      call PCFieldSplitSetIS(parallel_pc%petsc_pc,field_names(i),field_ISs(i)%petsc_is,ierr)
      call assert(ierr == 0,'ierr /= 0 in PCFieldSplitSetIS')
    enddo
    
    if (use_approx_schur_PC) then
      call PCFieldSplitSetSchurPre(parallel_pc%petsc_pc,PC_FIELDSPLIT_SCHUR_PRE_USER,approx_S%petsc_mat,ierr)
      call assert(ierr == 0,'ierr /= 0 in PCFieldSplitSetSchurPre')
    endif
    if (use_approx_schur_KSP) then
      call PCSetUp(parallel_pc%petsc_pc,ierr)
      call assert(ierr == 0,'ierr /= 0 in PCSetUp')
      
      ! The docs to `PCFieldSplitGetSubKSP` erroneously claim that PETSC_NULL_KSP can be passed
      ! instead of subksp, but PETSC_NULL_KSP is a scalar, so the implicit interface is
      ! inconsistent. Therefore just pass [PETSC_NULL_KSP], which is OK because PETSc does not
      ! change this argument.
      call PCFieldSplitGetSubKSP(parallel_pc%petsc_pc,n_subksps,[PETSC_NULL_KSP],ierr)
      call assert(ierr == 0,'ierr /= 0 in PCFieldSplitGetSubKSP')

      call assert(petsc2fort(n_subksps) == nfields,'PCFieldSplitGetSubKSP: n_subksps /= nfields')
      allocate(subksps(petsc2fort(n_subksps)))

      call PCFieldSplitGetSubKSP(parallel_pc%petsc_pc,n_subksps,subksps,ierr)
      call assert(ierr == 0,'ierr /= 0 in PCFieldSplitGetSubKSP')
      
      call KSPSetOperators(subksps(2),approx_S%petsc_mat,approx_S%petsc_mat,ierr)
      call assert(ierr == 0,'ierr /= 0 in KSPSetOperators')
    endif
  end subroutine
end module petsc_solver_mod
