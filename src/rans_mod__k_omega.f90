submodule(rans_mod) rans_mod__k_omega
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use exceptions, only: error
  use math_const, only: SMALL
  use mesh_objects, only: elem_list
  use numerical_solutions, only: matprop_set, num_scalfunc_set, velocity, solve_for_energy
  use pnt_set_mod, only: pnt_set_t
  use run_data, only: boussinesq_approx, div_free_velocity, min_Pk_for_RANS
  use support, only: false, trace
  implicit none
  
  ! Von-Karman constant
  real(dp), parameter :: Karman = 0.41_dp
  
  ! Model constants
  real(dp), parameter :: alpha = 5.0_dp/9.0_dp
  real(dp), parameter :: beta = 3.0_dp/40.0_dp
  real(dp), parameter :: beta_star = 9.0_dp/100.0_dp
  real(dp), parameter :: sigma = 1.0_dp/2.0_dp
  real(dp), parameter :: sigma_star = 1.0_dp/2.0_dp
  
  type, extends(rans_model_t) :: k_omega_model_t
  contains
    procedure, nopass :: is_low_Re => false
  end type k_omega_model_t
  
  class(scal_func_type), allocatable :: num_k, num_rho_k, &
                                        num_omega, num_rho_omega
  class(scal_func_type), allocatable :: nu_molec, beta_func, temperature
  logical :: add_buoyancy_term
  
contains
  module procedure get_k_omega_model
    call num_scalfunc_set%get_func('vol_turb1',num_rho_k)
    call num_scalfunc_set%get_func('vol_turb2',num_rho_omega)
    call num_scalfunc_set%get_func('spec_turb1',num_k)
    call num_scalfunc_set%get_func('spec_turb2',num_omega)
    call matprop_set%get_func('kinematic_viscosity',nu_molec)
    
    call solve_for_energy(add_buoyancy_term)
    if (add_buoyancy_term .and. boussinesq_approx) then
      call matprop_set%get_func('thermal_expansion_coeff',beta_func)
      call matprop_set%get_func('temperature',temperature)
    endif
    
    allocate(k_omega_model_t :: rans_model)
    rans_model%kinem_visc%proc => kinem_visc_turb
    rans_model%diff_coeff_1st_eq%proc => diff_coeff_k
    rans_model%source_1st_eq%proc => source_k
    rans_model%react_coeff_1st_eq%proc => react_coeff_k
    rans_model%diff_coeff_2nd_eq%proc => diff_coeff_omega
    rans_model%source_2nd_eq%proc => source_omega
    rans_model%react_coeff_2nd_eq%proc => react_coeff_omega
    rans_model%no_eq = 2
  end procedure get_k_omega_model
  
  
  subroutine kinem_visc_turb(elem,pnt_set,time,values)
    ! The turbulent kinematic viscosity is bounded from below by a small
    ! fraction of the molecular viscosity (see Lew A.J. et al. - "A note on 
    ! the numerical treatment of the k-epsilon turbulence model")
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu, k, omega
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call num_k%get_values(elem,pnt_set,time,k)
    call num_omega%get_values(elem,pnt_set,time,omega)
    do p = 1, no_points
      if (omega(p) < 0.0_dp) then
        values(p) = 1.0e-4_dp*nu(p)
      else
        values(p) = max(max(k(p),0.0_dp)/max(omega(p),SMALL),1.0e-4_dp*nu(p))
      endif
    enddo
  end subroutine kinem_visc_turb
  
  
  subroutine diff_coeff_k(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu_t, nu
    
    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)
    
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    values = nu + sigma_star*nu_t
  end subroutine diff_coeff_k
  
  
  subroutine source_k(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: prod_values, diss_values
    
    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)
    
    call production_k(elem,pnt_set,time,prod_values)
    call dissipation_k(elem,pnt_set,time,diss_values)
    values = prod_values + diss_values
  end subroutine source_k
  
  subroutine production_k(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: Gk
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call shear_production(elem,pnt_set,time,values)
    if (add_buoyancy_term) then
      call buoyancy_term(elem,pnt_set,time,Gk)
      do p = 1, no_points
        values(p) = values(p) + max(Gk(p),0.0_dp)
      enddo
    endif
  end subroutine production_k
  
  subroutine shear_production(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu_t, rho, rho_k
    real(dp) :: grad_U(elem%dimen(),velocity%no_dirs(),size(values))
    real(dp) :: div_U
    integer :: no_points, p
    character(200) :: err_msg
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    call density%get_values(elem,pnt_set,time,rho)
    call num_rho_k%get_values(elem,pnt_set,time,rho_k)
    call velocity%get_gders(elem,pnt_set,time,grad_U)
    do p = 1, no_points
      ! Strain rate tensor part: mu_t*(dU_i/dx_j + dU_j/dx_i)*dU_i/dx_j = 2 * mu_t * S_ij**2
      values(p) = rho(p)*nu_t(p)*0.5_dp*sum((grad_U(:,:,p) + transpose(grad_U(:,:,p)))**2)
      if (.not. div_free_velocity) then
        ! Divergence part: -(2/3)*mu_t*(div.U)*delta_ij*dU_i/dx_j = -(2/3)*mu_t*(div.U)^2
        div_U = trace(grad_U(:,:,p))
        values(p) = values(p) - (2.0_dp/3.0_dp)*rho(p)*nu_t(p)*(div_U**2)
        ! Isotropic part: -(2/3)*rho*k*delta_ij*dU_i/dx_j = -(2/3)*rho*k*div.U
        values(p) = values(p) - (2.0_dp/3.0_dp)*max(rho_k(p),0.0_dp)*div_U
      endif
      if (min_Pk_for_RANS <= values(p) .and. values(p) < 0.0_dp) then
        ! Cap small negative production terms to zero
        values(p) = 0.0_dp
      elseif (values(p) < min_Pk_for_RANS) then
        write(err_msg,"('shear turb. production = ',es10.3,' < ',es10.3)") values(p), min_Pk_for_RANS
        call error(trim(err_msg))
      endif
    enddo
  end subroutine shear_production
  
  subroutine buoyancy_term(elem,pnt_set,time,values)
    use sources_mod, only: spec_force_func
    
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu_t, rho, beta
    real(dp), dimension(elem%dimen(),size(values)) :: g, grad_rho, grad_T
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    call spec_force_func%get_values(elem,pnt_set,time,g)
    if (boussinesq_approx) then
      call density%get_values(elem,pnt_set,time,rho)
      call beta_func%get_values(elem,pnt_set,time,beta)
      call temperature%get_gders(elem,pnt_set,time,grad_T)
    else
      call density%get_gders(elem,pnt_set,time,grad_rho)
    endif
    do p = 1, no_points
      if (boussinesq_approx) then
        values(p) = (nu_t(p)/Pr_t) * rho(p) * beta(p) * dot_product(g(:,p),grad_T(:,p))
      else
        values(p) = -(nu_t(p)/Pr_t) * dot_product(g(:,p),grad_rho(:,p))
      endif
    enddo
  end subroutine buoyancy_term
    
  subroutine dissipation_k(elem,pnt_set,time,values)
    ! The dissipation term in k-equation is bounded from below to
    ! to enforce its positivity (see Lew A.J. et al. - "A note on 
    ! the numerical treatment of the k-epsilon turbulence model")
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: rho_k, omega
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call num_rho_k%get_values(elem,pnt_set,time,rho_k)
    call num_omega%get_values(elem,pnt_set,time,omega)
    do p = 1, no_points
      values(p) = beta_star * max(rho_k(p),0.0_dp) * max(omega(p),0.0_dp)
    enddo
  end subroutine dissipation_k
  
  
  subroutine react_coeff_k(elem,pnt_set,time,values)
    ! The reaction coefficient derives from the linearization of the 
    ! dissipation term at the r.h.s. of the k-equation (see Zijlema M. -
    ! "Computational Modeling of Turbulent Flows in General Domains", p.91)
    ! It is bounded from below to enforce its positivity (see Lew A.J. 
    ! et al. - "A note on the numerical treatment of the k-epsilon 
    ! turbulence model")
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: omega, rho_k, Gk
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call num_omega%get_values(elem,pnt_set,time,omega)
    if (add_buoyancy_term) then
      call buoyancy_term(elem,pnt_set,time,Gk)
      call num_rho_k%get_values(elem,pnt_set,time,rho_k)
    endif
    do p = 1, no_points
      values(p) = 2.0_dp * beta_star * max(omega(p),0.0_dp)
      if (add_buoyancy_term) then
        if (rho_k(p) > 0.0_dp) values(p) = values(p) - min(Gk(p),0.0_dp) / max(rho_k(p),SMALL)
      endif
    enddo
  end subroutine react_coeff_k
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine diff_coeff_omega(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu_t, nu
    
    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)
    
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    values = nu + sigma*nu_t
  end subroutine diff_coeff_omega
  
  
  subroutine source_omega(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: prod_values, diss_values
    
    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)
    
    call production_omega(elem,pnt_set,time,prod_values)
    call dissipation_omega(elem,pnt_set,time,diss_values)
    values = prod_values + diss_values
  end subroutine source_omega
  
  subroutine production_omega(elem,pnt_set,time,values)
    ! The production term in omega-equation is bounded from below to
    ! to enforce its positivity (see Lew A.J. et al. - "A note on 
    ! the numerical treatment of the k-epsilon turbulence model")
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: Pk, rho_k, rho_omega, Gk
    real(dp) :: P_tot
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call shear_production(elem,pnt_set,time,Pk)
    if (add_buoyancy_term) call buoyancy_term(elem,pnt_set,time,Gk)
    call num_rho_k%get_values(elem,pnt_set,time,rho_k)
    call num_rho_omega%get_values(elem,pnt_set,time,rho_omega)
    do p = 1, no_points
      if (rho_k(p) < 0.0_dp) then
        values(p) = 0.0_dp
      else
        P_tot = Pk(p)
        if (add_buoyancy_term) P_tot = P_tot + max(Gk(p),0.0_dp)
        values(p) = alpha * P_tot * max(rho_omega(p),0.0_dp) / max(rho_k(p),SMALL)
      endif
    enddo
  end subroutine production_omega
    
  subroutine dissipation_omega(elem,pnt_set,time,values)
    ! The dissipation term in omega-equation is bounded from below to
    ! to enforce its positivity (see Lew A.J. et al. - "A note on 
    ! the numerical treatment of the k-epsilon turbulence model")
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: omega, rho_omega
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call num_omega%get_values(elem,pnt_set,time,omega)
    call num_rho_omega%get_values(elem,pnt_set,time,rho_omega)
    do p = 1, no_points
      values(p) = beta * omega(p) * max(rho_omega(p),0.0_dp)
    enddo
  end subroutine dissipation_omega
  
  
  subroutine react_coeff_omega(elem,pnt_set,time,values)
    ! The reaction coefficient derives from the linearization of the 
    ! dissipation term at the r.h.s. of the omega-equation (see Zijlema M. -
    ! "Computational Modeling of Turbulent Flows in General Domains", p.91)
    ! It is bounded from below to enforce its positivity (see Lew A.J. 
    ! et al. - "A note on the numerical treatment of the k-epsilon
    ! turbulence model")
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: omega
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call num_omega%get_values(elem,pnt_set,time,omega)
    do p = 1, no_points
      values(p) = 2.0_dp * beta * max(omega(p),0.0_dp)
    enddo
  end subroutine react_coeff_omega
end submodule rans_mod__k_omega
