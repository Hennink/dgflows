module penalty_param_mod
  use assertions, only: assert
  use exceptions, only: error
  use dof_handler, only: dof_handler_type
  use f90_kind, only: dp
  use mesh_objects, only: face_list, side_of_internal_nghb_of_bnd_face
  implicit none
  
  private
  public :: gsip_penalty_parameter
  public :: maxval_of_sides_at_qps

contains
  function gsip_penalty_parameter(face,handler,diff_coeff,time) result(penalty)
    use run_data, only: sip_penalty_safety_factor
    use scal_func_mod, only: scal_func_type
    
    type(face_list), intent(in) :: face
    type(dof_handler_type), intent(in) :: handler
    class(scal_func_type), intent(in) :: diff_coeff
    integer, intent(in) :: time ! time at which to evaluate the diff_coeff
    
    real(dp) :: IP_over_length_scale, diff_scale(face%nqp)
    real(dp) :: penalty(face%nqp)
    
    IP_over_length_scale = IP_over_length(face,handler)
    diff_scale = maxval_of_sides_at_qps(diff_coeff,face,handler%only_fluid_mesh,time)
    penalty = sip_penalty_safety_factor * IP_over_length_scale * diff_scale
  end function gsip_penalty_parameter
  
  real(dp) function IP_over_length(face,handler)
    !Reference: K. Hillewaert, PhD thesis, pp. 32-33.
    type(face_list), intent(in) :: face
    type(dof_handler_type), intent(in) :: handler
    
    integer :: side_Ein
    
    if (face%is_boundary(handler%only_fluid_mesh)) then
      side_Ein = side_of_internal_nghb_of_bnd_face(face,handler%only_fluid_mesh)
      IP_over_length = IP_over_length_single_side(face,side_Ein,handler)
    else
      IP_over_length = max(IP_over_length_single_side(face,1,handler), &
                           IP_over_length_single_side(face,2,handler))
    endif
  end function IP_over_length
  
  real(dp) function IP_over_length_single_side(face,side,handler)
    ! Returns the ratio of the IP-factor and the anisotropic length scale on
    ! one side of the face. This includes the number of faces of the element.
    use mesh_objects, only: no_faces_of_elem

    type(face_list), intent(in) :: face
    integer, intent(in) :: side
    type(dof_handler_type), intent(in) :: handler

    integer :: active_elem_no
    integer :: order, no_faces
    real(dp) :: ip, length

    call assert(side == 1 .or. side == 2)

    associate(elem => face%elem_neighbors(side)%point)
      active_elem_no = elem%get_active_elem_no(handler%only_fluid_mesh)
      order = handler%order(active_elem_no)
      no_faces = no_faces_of_elem(elem%eltype)
      length = elem%volume / face%area
      if (.not. face%is_boundary(handler%only_fluid_mesh)) length = 2.0_dp * length
      ip = IP_factor(elem%dimen(),elem%eltype,order)
    end associate
    IP_over_length_single_side = no_faces * ip / length
  end function IP_over_length_single_side

  real(dp) function IP_factor(dimen,eltype,order)
    !Reference: K. Hillewaert, PhD thesis, p.30.
    integer, intent(in) :: dimen, eltype, order
    
    select case(eltype)
      case(12,24,38)
        !Tensor product elements: lines, quads, hexs
        IP_factor = real((order+1)**2,dp)
      case(23,34)
        !Simplicial elements: triangles, tets
        IP_factor = real((order+1)*(order+dimen),dp)/real(dimen,dp)
      case default
        call error('unknown eltype')
    end select
  end function IP_factor
  
  function maxval_of_sides_at_qps(scalfunc,face,only_fluid_mesh,time)
    use scal_func_mod, only: scal_func_type
    
    class(scal_func_type), intent(in) :: scalfunc
    type(face_list), intent(in) :: face
    logical, intent(in) :: only_fluid_mesh
    integer, intent(in) :: time
    
    real(dp) :: maxval_of_sides_at_qps(face%nqp)
    
    integer :: side_Ein
    
    if (face%is_boundary(only_fluid_mesh)) then
      side_Ein = side_of_internal_nghb_of_bnd_face(face,only_fluid_mesh)
      maxval_of_sides_at_qps = scalfunc%values_at_qps(face,side_Ein,time)
    else
      maxval_of_sides_at_qps = max(scalfunc%values_at_qps(face,1,time), &
                                   scalfunc%values_at_qps(face,2,time))
    endif
  end function maxval_of_sides_at_qps
end module penalty_param_mod
