module scal_func_masked_mod
  ! Scalar function defined only on a portion of the mesh, identified by a 
  ! particular mat_id
  use f90_kind, only: dp
  use mesh_objects, only: elem_list
  use pnt_set_mod, only: pnt_set_t
  use scal_func_mod, only: scal_func_type
  implicit none
  
  private
  public :: scal_func_masked_t
  
  type, extends(scal_func_type) :: scal_func_masked_t
    class(scal_func_type), allocatable :: scal_func
    integer :: mat_id
  contains
    procedure, non_overridable :: get_values, get_gders
  end type scal_func_masked_t
  
contains
  pure type(scal_func_masked_t) function as_masked_scalfunc(scal_func,mat_id) result(scal_func_masked)
    class(scal_func_type), intent(in) :: scal_func
    integer, intent(in) :: mat_id
    
    allocate(scal_func_masked%scal_func,source=scal_func)
    scal_func_masked%mat_id = mat_id
  end function as_masked_scalfunc
  
  
  subroutine get_values(this,elem,pnt_set,time,values)
    class(scal_func_masked_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    if (elem%mat_id == this%mat_id) then
      call this%scal_func%get_values(elem,pnt_set,time,values)
    else
      values = 0.0_dp
    endif
  end subroutine get_values
  
  subroutine get_gders(this,elem,pnt_set,time,gders)
    class(scal_func_masked_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:)
    
    if (elem%mat_id == this%mat_id) then
      call this%scal_func%get_gders(elem,pnt_set,time,gders)
    else
      gders = 0.0_dp
    endif
  end subroutine get_gders
end module scal_func_masked_mod

