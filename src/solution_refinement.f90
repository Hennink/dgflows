module solution_refinement
  ! THE ENTIRE MODULE NEEDS TO BE UPDATED ACCORDING TO THE MOST 
  ! RECENT DEVELOPMENTS IN THE RADIATION-TRANSPORT CODE.
  implicit none
  
  private
  
contains
  subroutine refine_solution(mesh,solution,old_handler,new_handler)
    use local_elem, only: local_dg_functions_single_point, no_dg_functions
    use f90_kind
    use support
    use local_global_mappings, only: elem_global_to_local_coord, elem_lcor2gcor
    use mesh_type, only: fem_mesh
    use mesh_objects, only: elem_id, elem_list
    use dof_handler
    implicit none

    type(fem_mesh), pointer :: mesh
    real(dp), allocatable, dimension(:) :: solution
    type(dof_handler_type) :: old_handler
    type(dof_handler_type) :: new_handler

    integer :: gr
    integer :: dir
    integer :: node
    integer :: no_nod_elem
    integer :: max_no_nod_elem
    integer :: eltype
    integer :: elem_no_new
    integer :: elem_no_old
    integer :: index_old
    integer :: index_new
    integer :: dimen
    integer :: order
    integer :: iquad
    integer :: child_index
    real(dp) :: phi_old
    real(dp) :: quot
    real(dp), dimension(3) :: coord
    real(dp), dimension(3) :: xi_old
    real(dp), dimension(3) :: xi_new
    real(dp), allocatable, dimension(:) :: solution_copy
    real(dp), allocatable, dimension(:) :: rhs
    type(elem_id) :: id_old
    type(elem_id) :: id_new
    type(elem_list), pointer :: elem_old
    type(elem_list), pointer :: elem_new
    real(dp), allocatable, dimension(:) :: fun_dg
    real(dp), allocatable, dimension(:,:) :: lder_dg
    real(dp), allocatable, dimension(:) :: fun_dg_old
    real(dp), allocatable, dimension(:,:) :: lder_dg_old
    logical :: casei

    dimen = mesh%dimen

    max_no_nod_elem = no_dg_functions(38,8)
    allocate(rhs(max_no_nod_elem))
    allocate(fun_dg(max_no_nod_elem))
    allocate(lder_dg(dimen,max_no_nod_elem))
    allocate(fun_dg_old(max_no_nod_elem))
    allocate(lder_dg_old(dimen,max_no_nod_elem))

    dimen = mesh%dimen

    ! make copy of solution

    allocate(solution_copy(old_handler%no_dof_mgrp))
    solution_copy = solution
    deallocate(solution)
    allocate(solution(new_handler%no_dof_mgrp),source=0.0_dp)

    ! Project the solution on the new solution vector by the following steps:
    ! i) run over new elements in the mesh
    ! ii) find the mother from which it originates
    ! iii) project
    !
    ! We assume that coarsening does not take place.

    elem_no_new = 1
    do elem_no_old=1,mesh%old_no_active_elem
      casei = .false.
      id_old = mesh%old_active_elem_list(elem_no_old)
      id_new = mesh%active_elem_list(elem_no_new)
      elem_old => mesh%get_elem_ptr(id_old)
      elem_new => mesh%get_elem_ptr(id_new)
      eltype = elem_old%eltype
      no_nod_elem = old_handler%no_nod_elem(elem_no_old)
      order = old_handler%order(elem_no_old)
      if (associated(elem_old,elem_new)) then
        casei = .true.
      endif

      if (casei) then

        ! case i: element has not been refined and is in both the old and new sets

        do gr=1,new_handler%no_grps
          do dir=1,new_handler%no_dirs
            do node=1,no_nod_elem
              index_new = dof(new_handler,gr,elem_no_new,dir,node)
              index_old = dof(old_handler,gr,elem_no_old,dir,node)
              solution(index_new) = solution_copy(index_old)
            enddo
          enddo
        enddo
        elem_no_new = elem_no_new+1

      else

        ! case ii: element in new set must come from refinement of elem_old
        ! Implicitly we assume that the order of the solutions on these elements are equal.

        do child_index=1,size(elem_old%children)
          id_new = mesh%active_elem_list(elem_no_new)
          elem_new => mesh%get_elem_ptr(id_new)
          do gr=1,new_handler%no_grps
            do dir=1,new_handler%no_dirs
              ! calculate rhs
              rhs = 0.0_dp
              do iquad=1,elem_new%nqp
                xi_new(1:dimen) = elem_new%quad_points%lcors(1:dimen,iquad)
                call local_dg_functions_single_point(eltype,order,xi_new(1:dimen),fun_dg,lder_dg)
                quot = elem_new%quot(iquad)
                call elem_lcor2gcor(elem_new,xi_new,coord)
                call elem_global_to_local_coord(elem_old,dimen,coord,xi_old)
            
                call local_dg_functions_single_point(eltype,order,xi_old(1:dimen),fun_dg_old,lder_dg_old)
            
                phi_old = 0.0_dp
                do node=1,no_nod_elem
                  index_old = dof(old_handler,gr,elem_no_old,dir,node)
                  phi_old = phi_old + solution_copy(index_old)*fun_dg_old(node)
                enddo
                rhs(1:no_nod_elem) = rhs(1:no_nod_elem) + fun_dg(1:no_nod_elem)*quot*phi_old
              enddo

              ! project coarse solution on the fine basis

              call elem_new%solve_for_massmat(rhs)

              ! put in right locations

              do node=1,no_nod_elem
                index_new = dof(new_handler,gr,elem_no_new,dir,node)
                solution(index_new) = rhs(node)
              enddo

            enddo
          enddo

          elem_no_new = elem_no_new + 1
        enddo

      endif

    enddo

    ! cleanup

    deallocate(solution_copy)

  end subroutine refine_solution




  subroutine project_solution_to_high_p(mesh,solution_low,low_handler,solution_high,high_handler)
    use f90_kind
    use support
    use mesh_type, only: fem_mesh
    use dof_handler
    implicit none

    type(fem_mesh), pointer :: mesh
    real(dp), allocatable, dimension(:) :: solution_low
    type(dof_handler_type) :: low_handler
    real(dp), allocatable, dimension(:) :: solution_high
    type(dof_handler_type) :: high_handler

    integer :: elem_no
    integer :: gr
    integer :: dir
    integer :: node
    integer :: no_nod_elem
    integer :: index_old
    integer :: index_new

    do elem_no=1,mesh%no_active_elem
      no_nod_elem = low_handler%no_nod_elem(elem_no)
      do gr=1,low_handler%no_grps
        do dir=1,low_handler%no_dirs
          do node=1,no_nod_elem
            index_new = dof(high_handler,gr,elem_no,dir,node)
            index_old = dof(low_handler,gr,elem_no,dir,node)
            solution_high(index_new) = solution_low(index_old)
          enddo
        enddo
      enddo
    enddo

  end subroutine project_solution_to_high_p

  subroutine project_solution_to_low_p(elem,phi_high,phi_low,order_low)
    use f90_kind
    use local_elem, only: no_dg_functions
    use mesh_objects, only: elem_list

    type(elem_list), intent(in) :: elem
    real(dp), intent(in) :: phi_high(:)
    real(dp), intent(inout) :: phi_low(:)  ! could theoretically be overdimensionalized, so intent(out) could destroy values
    integer, intent(in) :: order_low

    integer :: order_high
    integer :: nnod_low, nnod_high
    real(dp), allocatable :: proj_rhs(:)

    order_high = order_low + 1
    nnod_low = no_dg_functions(elem%eltype,order_low)
    nnod_high = no_dg_functions(elem%eltype,order_high)
    allocate(proj_rhs(nnod_low))
    call elem%massmatmult(phi_high(:nnod_high),proj_rhs)
    call elem%solve_for_massmat(proj_rhs)
    phi_low(:nnod_low) = proj_rhs
  end subroutine project_solution_to_low_p
end module solution_refinement
