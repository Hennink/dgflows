module source_pde_term_mod
  use pde_term_mod, only: pde_term_t
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only: vec_func_type
  implicit none

  private
  public :: source_t

  type, extends(pde_term_t) :: source_t
    ! Discretizes a term
    !     S
    ! on the rhs of a pde, where 'S' is the source (= production).
    class(vec_func_type), allocatable :: src
    class(scal_func_type), allocatable :: src_all_dirs
    integer :: time_source = 1
  contains
    procedure, non_overridable :: process_elem
    procedure, non_overridable :: ngbr_coupling_depth
  end type

contains
  pure integer function ngbr_coupling_depth(this)
    class(source_t), intent(in) :: this

    ngbr_coupling_depth = -1
  end function

  subroutine process_elem(this,elem,qnty,local_linsys)
    use assertions, only: assert, BOUNDS_CHECK
    use f90_kind, only: dp
    use lincom_rank1_mod, only: lincom_rank1_t
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: elem_list
    use solvec_mod, only: solvec_t
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t

    class(source_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys
    
    type(lincom_rank1_t) :: alldir_func
    type(unidir_lincom_rank0_t) :: unidir_func
    real(dp) :: src(qnty%handler%no_dirs,elem%nqp), src_all_dirs(elem%nqp)

    if (allocated(this%src)) then
      call assert(qnty%handler%no_dirs == this%src%no_dirs(),BOUNDS_CHECK)
      call alldir_func%set_as_func(elem,qnty%handler)
      call this%src%values_at_qps(elem,this%time_source,src)
      call local_linsys%add_int_dotprod_to_rhs(alldir_func,src,elem%quot)
    endif
    if (allocated(this%src_all_dirs)) then
      call unidir_func%set_as_func(elem,qnty%handler)
      src_all_dirs = this%src_all_dirs%values_at_elem_qps(elem,this%time_source)
      call local_linsys%add_inprod_to_rhs_all_dirs(unidir_func,src_all_dirs,elem%quot)
    endif
  end subroutine
end module
