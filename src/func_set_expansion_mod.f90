module func_set_expansion_mod
  use func_link_mod, only: func_link_t
  use func_set_mod, only: func_set_t
  implicit none
  
  private
  public :: func_set_expansion_t
  
  integer, parameter :: max_no_links = 16
  
  type :: func_set_expansion_t
    private
    type(func_set_t), public :: known_func
    type(func_link_t) :: links(max_no_links)
    integer :: no_links = 0
    logical :: finalized = .false.
  contains
    procedure, non_overridable :: add_known_function, add_known_functions
    procedure, non_overridable :: add_func_link, add_func
    procedure, non_overridable :: get_monad
    procedure, non_overridable :: infer_from_func_set
    procedure, non_overridable :: finalize
    generic :: assignment(=) => deepcopy
    procedure, private :: deepcopy
  end type func_set_expansion_t
  
contains
  subroutine add_known_function(this,name,func)
    use assertions, only: assert
    use scal_func_mod, only: scal_func_type
    
    class(func_set_expansion_t), intent(inout) :: this
    character(*), intent(in) :: name
    class(scal_func_type), intent(in) :: func
    
    call assert(.not. this%finalized)
    
    call this%known_func%add_func(name,func)
  end subroutine add_known_function
  
  subroutine add_known_functions(this,known_functions)
    use assertions, only: assert
    use scal_func_mod, only: scal_func_type
    
    class(func_set_expansion_t), intent(inout) :: this
    type(func_set_t), intent(in) :: known_functions
    
    call assert(.not. this%finalized)
    
    call this%known_func%union(known_functions)
  end subroutine add_known_functions
  
  subroutine add_func_link(this,link)
    use assertions, only: assert
    
    class(func_set_expansion_t), intent(inout) :: this
    type(func_link_t), intent(in) :: link
    
    call assert(.not. this%finalized)
    call assert(this%no_links < max_no_links)
    
    this%no_links = this%no_links + 1
    this%links(this%no_links) = link
  end subroutine add_func_link

  subroutine add_func(this,from,to,func)
    use func_ptr2monad_mod, only: real2real_i

    class(func_set_expansion_t), intent(inout) :: this
    character(*), intent(in) :: from, to
    procedure(real2real_i) :: func
    
    type(func_link_t) :: func_link

    call func_link%init(from,to,func)
    call this%add_func_link(func_link)
  end subroutine add_func
  
  subroutine get_monad(this,from,to,monad)
    use exceptions, only: error
    use monad_mod, only: monad_t

    class(func_set_expansion_t), target, intent(in) :: this
    character(*), intent(in) :: from, to
    class(monad_t), pointer, intent(out) :: monad

    integer :: i

    do i = 1, this%no_links
      if (this%links(i)%from == from .and. this%links(i)%to == to) then
        monad => this%links(i)%monad
        return
      endif
    enddo
    call error('cannot find link from '//from//' to '//to)
  end subroutine get_monad

  subroutine infer_from_func_set(this,given_funcs,inferred_funcs)
    use assertions, only: assert
    use exceptions, only: warning
    use petsc_mod, only: I_am_master_process
    
    class(func_set_expansion_t), intent(in) :: this
    type(func_set_t), intent(in) :: given_funcs
    type(func_set_t), intent(out) :: inferred_funcs
    
    type(func_set_t) :: expanded_given
    integer :: l
    type(func_link_t) :: link
    
    call assert(this%finalized)
    
    expanded_given = given_funcs
    call expanded_given%merge_exl_duplicates(this%known_func)
    call expanded_given%expand_with_known_relations()

    call inferred_funcs%union(this%known_func)
    call inferred_funcs%expand_with_known_relations()
    do l = 1, this%no_links
      link = this%links(l)
      call link%map_if_possible(expanded_given,inferred_funcs)
      call inferred_funcs%expand_with_known_relations()
    enddo

    do l = 1, this%no_links
      link = this%links(l)
      call link%expand_if_possible(inferred_funcs)
      call inferred_funcs%expand_with_known_relations()
    enddo

    do l = 1, this%no_links
      link = this%links(l)
      if (      .not. inferred_funcs%has(link%to) &
          .and. .not. expanded_given%has(link%to) &
          .and. I_am_master_process()) then
        call warning('I did not infer ' // link%to)
        call warning('from funcset, despite ' // link%repr())
      endif
    enddo
  end subroutine infer_from_func_set
  
  subroutine finalize(this)
    use assertions, only: assert
    
    class(func_set_expansion_t), intent(inout) :: this
    
    call assert(.not. this%finalized)
    this%finalized = .true.
  end subroutine finalize

  subroutine deepcopy(this,other)
    class(func_set_expansion_t), intent(out) :: this
    type(func_set_expansion_t), intent(in) :: other

    integer :: k

    this%no_links = other%no_links
    do k = 1, other%no_links
      this%links(k) = other%links(k)
    enddo
    this%known_func = other%known_func
    this%finalized = other%finalized
  end subroutine deepcopy
end module func_set_expansion_mod
