submodule(boundary_types) boundary_types__fixed_velocity
  use func_set_mod, only: func_set_t
  implicit none
  
  type, extends(nonwall_bnd_t) :: fixed_velocity_type
    type(func_set_t) :: fixed_values
  contains
    procedure, nopass :: fixes_absolute_pressure => false
    procedure, nopass :: has_inflow => true
    procedure :: n_dot_velocity_at_qps
    procedure :: read_specs_from_file
    procedure :: get_bc
  end type fixed_velocity_type
  
contains
  module procedure alloc_fixed_velocity
    allocate(fixed_velocity_type :: bnd)
  end procedure alloc_fixed_velocity
  
  subroutine read_specs_from_file(this,unit_number)
    use read_funcs_mod, only: read_block_as_func_specs
    use matprop_mod, only: expand_func_set_with_matprop
    
    class(fixed_velocity_type), intent(out) :: this
    integer, intent(in) :: unit_number
    
    call read_block_as_func_specs(unit_number,this%fixed_values)
    call expand_func_set_with_matprop(this%fixed_values)
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types, only: bc_type, dirichlet_bc_type
    
    class(fixed_velocity_type), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    
    type(dirichlet_bc_type), allocatable :: dirichlet_bc
    character(:), allocatable :: my_name
    
    if (qnty_name == 'density_predictor') then
      my_name = 'density'
    else
      my_name = qnty_name
    endif
    allocate(dirichlet_bc)
    call this%fixed_values%get_func(my_name,dirichlet_bc%inhom_part)
    call move_alloc(dirichlet_bc,bc)
  end subroutine get_bc

  function n_dot_velocity_at_qps(this,face,fluid_side,time)
    use assertions, only: assert
    use f90_kind, only: dp
    use mesh_objects, only: face_list
    use run_data, only: mesh_dimen
    use vec_func_mod, only: vec_func_type

    class(fixed_velocity_type), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: fluid_side
    integer, intent(in) :: time
    real(dp) :: n_dot_velocity_at_qps(face%nqp)

    class(vec_func_type), allocatable :: velocity
    real(dp) :: velocity_at_qps(mesh_dimen,face%nqp)
    real(dp) :: normal(mesh_dimen,face%nqp)
    integer :: p

    normal = face%normal
    if (fluid_side /= 1) normal = - normal
    call this%fixed_values%get_func('velocity',velocity)
    call velocity%values_at_qps(face,fluid_side,time,velocity_at_qps)
    do p = 1, face%nqp
      n_dot_velocity_at_qps(p) = dot_product(normal(:,p),velocity_at_qps(:,p))
    end do
  end function n_dot_velocity_at_qps
end submodule boundary_types__fixed_velocity

