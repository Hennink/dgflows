submodule(boundary_types) boundary_types__solid_htc
  use scal_func_mod, only: scal_func_type
  implicit none
  
  type, extends(wall_bnd_t) :: solid_htc_t
    class(scal_func_type), allocatable :: htc
    class(scal_func_type), allocatable :: T_ext
  contains
    procedure :: read_specs_from_file
    procedure :: get_bc
  end type solid_htc_t
  
contains
  module procedure solid_htc
    allocate(solid_htc_t :: bnd)
  end procedure solid_htc
  
  subroutine read_specs_from_file(this,unit_number)
    use read_funcs_mod, only: read_line_as_qnty_eq_func
    
    class(solid_htc_t), intent(out) :: this
    integer, intent(in) :: unit_number
    
    call read_line_as_qnty_eq_func(unit_number,'heat_transf_coeff',this%htc)
    call read_line_as_qnty_eq_func(unit_number,'T_external',this%T_ext)
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types, only: bc_type, robin_bc_type
    use boundary_types_support_mod, only: assemble_htc_robin_bc
    
    class(solid_htc_t), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    
    type(robin_bc_type), allocatable :: robin_bc
    
    select case(qnty_name)
      case('temperature','spec_enthalpy','vol_enthalpy')
        call assemble_htc_robin_bc(qnty_name,this%htc,this%T_ext,robin_bc)
      case default
        write(*,'(a)') 'ERROR: no bc for '//qnty_name//' in solid_htc'
        error stop
    end select
    call move_alloc(robin_bc,bc)
  end subroutine get_bc
end submodule boundary_types__solid_htc
