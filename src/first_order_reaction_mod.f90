module first_order_reaction_mod
  use pde_term_mod, only: pde_term_t
  use scal_func_mod, only: scal_func_type
  implicit none

  private
  public :: first_order_reaction_t

  type, extends(pde_term_t) :: first_order_reaction_t
    ! Discretizes a first order reaction term:
    !     k * u ,
    ! where
    ! * 'u' is the unknown;
    ! * 'k' is the rate constant, with
    !   **  production:  k < 0;
    !   **  destruction: k > 0.
    !
    ! We cannot bind the latest density as a scalfunc, since it can change
    ! duing the iteration in a time step. Therefore there is a special option
    ! for this quantity.
    class(scal_func_type), allocatable :: rate
    integer :: time_rate = 1
  contains
    procedure, non_overridable :: process_elem
    procedure, non_overridable :: ngbr_coupling_depth
  end type

contains
  pure integer function ngbr_coupling_depth(this)
    class(first_order_reaction_t), intent(in) :: this
    
    ngbr_coupling_depth = 0
  end function

  subroutine process_elem(this,elem,qnty,local_linsys)
    use f90_kind, only: dp
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: elem_list
    use solvec_mod, only: solvec_t
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t

    class(first_order_reaction_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys

    type(unidir_lincom_rank0_t) :: func
    real(dp) :: quot_x_rate(elem%nqp)

    call func%set_as_func(elem,qnty%handler)
    quot_x_rate = elem%quot * this%rate%values_at_elem_qps(elem,this%time_rate)
    ! \int  k u_i v_i
    call local_linsys%add_inprod_to_lhs_all_dirs(func,func,quot_x_rate,.false.,1.0_dp)
  end subroutine
end module
