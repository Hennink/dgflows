module mtx_block_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK, perform_all_bc
  use array_opers_mod, only: add_Aik_Bjk_wk, gemm_classic
  use f90_kind,  only: dp
  implicit none
  
  private
  public :: mtx_block_t
  
  type :: mtx_block_t
    private
    integer :: lhs_gr, rhs_gr
    integer :: lhs_elem
    integer, public :: rhs_elem
    real(dp), contiguous, pointer :: tmat(:,:,:,:) => null() ! rhs_node, rhs_dir,  lhs_node, lhs_dir
    real(dp), allocatable :: tdiag(:,:)
    
  contains
    procedure, non_overridable :: init
    procedure, non_overridable :: add_inprod, &
                                  add_int_dotprod
    procedure, non_overridable :: add_transposed_elemmats_for_dirs, &
                                  add_transposed_elemmat_all_dirs
    procedure, non_overridable :: add_to_glob_mtx
    procedure, non_overridable, private :: process_diag
    final :: final_mtx_block
  end type mtx_block_t
  
contains
  subroutine init(this,lhs_elem,rhs_elem,handler)
    use dof_handler, only: dof_handler_type
    
    class(mtx_block_t), intent(out) :: this
    integer, intent(in) :: lhs_elem, rhs_elem
    type(dof_handler_type), intent(in) :: handler
    
    integer :: lhs_nnod, rhs_nnod

    call assert(handler%no_grps == 1,"don't know to which groups I belong")
    this%lhs_gr = 1
    this%rhs_gr = 1
    this%lhs_elem = lhs_elem
    this%rhs_elem = rhs_elem
    lhs_nnod = handler%no_nod_elem(lhs_elem)
    rhs_nnod = handler%no_nod_elem(rhs_elem)
    allocate(this%tmat(rhs_nnod, handler%no_dirs, lhs_nnod, handler%no_dirs))
    this%tmat = 0.0_dp
    allocate(this%tdiag(rhs_nnod, lhs_nnod))
    this%tdiag = 0.0_dp
  end subroutine init
  
  subroutine add_inprod(this,scal1,scal2,weights)
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t

    class(mtx_block_t), intent(inout) :: this
    type(unidir_lincom_rank0_t), intent(in) :: scal1, scal2
    real(dp), intent(in) :: weights(:)

    call assert(this%lhs_elem == scal1%elem, BOUNDS_CHECK)
    call assert(this%rhs_elem == scal2%elem, BOUNDS_CHECK)
    call assert(size(weights) == scal1%np(), BOUNDS_CHECK)
    call assert(size(weights) == scal2%np(), BOUNDS_CHECK)

    call add_Aik_Bjk_wk(this%tdiag, scal2%stencil, scal1%stencil, weights, 'BLAS')
  end subroutine add_inprod

  subroutine add_int_dotprod(this,vec1,vec2,quad_weights,alpha)
    use lincom_rank1_mod, only: lincom_rank1_t
    
    logical, parameter :: use_blas = .true.

    class(mtx_block_t), intent(inout) :: this
    type(lincom_rank1_t), intent(in) :: vec1, vec2
    real(dp), intent(in) :: quad_weights(:)
    real(dp), intent(in) :: alpha
    
    integer :: p, np, dir, ndirs_vec, n1, nnod1, n2, nnod2, d1, ndirs1, d2, ndirs2
    
    call assert(this%lhs_elem == vec1%elem,BOUNDS_CHECK)
    call assert(this%rhs_elem == vec2%elem,BOUNDS_CHECK)
    np = assert_eq(size(quad_weights),vec1%np(),vec2%np(),BOUNDS_CHECK)
    ndirs_vec = assert_eq(vec1%no_dirs(),vec2%no_dirs(),BOUNDS_CHECK)
    nnod2 = assert_eq(size(this%tmat,1),size(vec2%stencil,1),BOUNDS_CHECK)
    ndirs2 = assert_eq(size(this%tmat,2),size(vec2%stencil,2),BOUNDS_CHECK)
    nnod1 = assert_eq(size(this%tmat,3),size(vec1%stencil,1),BOUNDS_CHECK)
    ndirs1 = assert_eq(size(this%tmat,4),size(vec1%stencil,2),BOUNDS_CHECK)

    if (use_blas) then
      block
        real(dp) :: weighted_vec1(size(vec1%stencil,1), size(vec1%stencil,2), size(vec1%stencil,3), size(vec1%stencil,4))
        do p = 1, np
          weighted_vec1(:,:,:,p) = vec1%stencil(:,:,:,p) * quad_weights(p) * alpha
        enddo
        call gemm_classic(transA=.false., transB=.true., &
            M=ndirs2*nnod2, N=ndirs1*nnod1, K=np*ndirs_vec, &
            ALPHA=1.0_dp, A=vec2%stencil, LDA=ndirs2*nnod2, B=weighted_vec1, LDB=ndirs1*nnod1, &
            BETA=1.0_dp,  C=this%tmat,    LDC=ndirs2*nnod2)
      end block

    else
      do p = 1, np
        do dir = 1, ndirs_vec
          do d1 = 1, ndirs1
            do n1 = 1, nnod1
              do d2 = 1, ndirs2
                do n2 = 1, nnod2
                  this%tmat(n2,d2,n1,d1) = this%tmat(n2,d2,n1,d1) &
                      + vec2%stencil(n2,d2,dir,p) * vec1%stencil(n1,d1,dir,p) * quad_weights(p) * alpha
                enddo
              enddo
            enddo
          enddo
        enddo
      enddo
    endif
  end subroutine add_int_dotprod
  
  subroutine add_transposed_elemmats_for_dirs(this,transposed_elemmats_for_dirs)
    class(mtx_block_t), intent(inout) :: this
    real(dp), intent(in) :: transposed_elemmats_for_dirs(:,:,:,:)

    call assert(all(shape(this%tmat) == shape(transposed_elemmats_for_dirs)), BOUNDS_CHECK)
    this%tmat = this%tmat + transposed_elemmats_for_dirs
  end subroutine

  subroutine add_transposed_elemmat_all_dirs(this, transposed_elemmat)
    class(mtx_block_t), intent(inout) :: this
    real(dp), intent(in) :: transposed_elemmat(:,:)

    call assert(all(shape(this%tdiag) == shape(transposed_elemmat)), BOUNDS_CHECK)

    this%tdiag = this%tdiag + transposed_elemmat
  end subroutine

  subroutine add_to_glob_mtx(this,petsc_mat,handler)
    use dof_handler, only: dof_handler_type
    use petsc_mat_mod, only: petsc_mat_type

    class(mtx_block_t), intent(inout) :: this
    type(petsc_mat_type), intent(inout) :: petsc_mat
    type(dof_handler_type), intent(in) :: handler
    
    integer :: bsizes(2)
    real(dp), contiguous, pointer :: flat_mat(:), flat_mat2(:,:)
    integer :: no_dirs, lhs_nnod, rhs_nnod
    integer, allocatable :: row_dofs(:), col_dofs(:)

    call this%process_diag()

    if (petsc_mat%assemble_elemmat_as_block) then
      if (perform_all_bc) then
        ! The assumption is that all element matrices exactly correspond to one block in the PETSc Mat.
        bsizes = petsc_mat%block_sizes()
        call assert(bsizes(1) == handler%no_dof_in_elem(this%lhs_elem), BOUNDS_CHECK)
        call assert(bsizes(2) == handler%no_dof_in_elem(this%rhs_elem), BOUNDS_CHECK)
      endif
      flat_mat(1:size(this%tmat)) => this%tmat
      call petsc_mat%set_values_blocked(this%lhs_elem, this%rhs_elem, flat_mat)
    else
      rhs_nnod = assert_eq(size(this%tmat,1),handler%no_nod_elem(this%rhs_elem),BOUNDS_CHECK)
      no_dirs = assert_eq(size(this%tmat,2),size(this%tmat,4),handler%no_dirs,BOUNDS_CHECK)
      lhs_nnod = assert_eq(size(this%tmat,3),handler%no_nod_elem(this%lhs_elem),BOUNDS_CHECK)

      row_dofs = handler%flat_dof(this%lhs_gr,this%lhs_elem)
      col_dofs = handler%flat_dof(this%rhs_gr,this%rhs_elem)
      flat_mat2(1:rhs_nnod*no_dirs, 1:lhs_nnod*no_dirs) => this%tmat
      
      call petsc_mat%add(row_dofs, col_dofs, flat_mat2)
    endif
  end subroutine
  
  subroutine process_diag(this)
    class(mtx_block_t), intent(inout) :: this

    integer :: dir, no_dirs

    no_dirs = assert_eq(size(this%tmat,2),size(this%tmat,4),BOUNDS_CHECK)

    do dir = 1, no_dirs
      this%tmat(:,dir,:,dir) = this%tmat(:,dir,:,dir) + this%tdiag
    enddo
    this%tdiag = 0.0_dp
  end subroutine process_diag

  elemental subroutine final_mtx_block(this)
    type(mtx_block_t), intent(inout) :: this

    if (associated(this%tmat)) deallocate(this%tmat)
  end subroutine
end module mtx_block_mod
