module local_elem
  ! contains everything that is related to the choice of the reference elements
  implicit none
  
  private
  public :: initialize
  
  public :: coord_in_domain
  public :: convert_face_to_vol_local_coord
  public :: get_dimen
  public :: get_quadrature
  public :: loc_idx_of_face_vertices_in_elem
  public :: get_opposite_vertex
  public :: local_dg_deriv
  public :: local_dg_functions
  public :: local_dg_functions_single_point
  public :: mutually_hierarchical_func_sets
  public :: no_dg_functions
  public :: nodal_functions
  public :: nodal_functions_single_point
  public :: no_vertices

  public :: local_elem_volume
  
  procedure(coord_in_domain_i), pointer, protected, save :: coord_in_domain !  => null()
  procedure(get_dimen_i), pointer, protected, save :: get_dimen => null()
  procedure(get_quadrature_i), pointer, protected, save :: get_quadrature => null()
  procedure(local_dg_deriv_i), pointer, protected, save :: local_dg_deriv => null()
  procedure(local_dg_functions_i), pointer, protected, save :: local_dg_functions => null()
  procedure(local_dg_functions_single_point_i), pointer, protected, save :: local_dg_functions_single_point => null()
  procedure(mutually_hierarchical_func_sets_i), pointer, protected, save :: mutually_hierarchical_func_sets ! => null()
  procedure(no_dg_functions_i), pointer, protected, save :: no_dg_functions => null()
  procedure(no_vertices_i), pointer, protected, save :: no_vertices => null()
  
  type :: local_elem_functionality
    procedure(coord_in_domain_i), nopass, pointer :: coord_in_domain
    procedure(get_dimen_i), nopass, pointer :: get_dimen
    procedure(get_quadrature_i), nopass, pointer :: get_quadrature
    procedure(local_dg_deriv_i), nopass, pointer :: local_dg_deriv
    procedure(local_dg_functions_i), nopass, pointer :: local_dg_functions
    procedure(local_dg_functions_single_point_i), nopass, pointer :: local_dg_functions_single_point
    procedure(mutually_hierarchical_func_sets_i), nopass, pointer :: mutually_hierarchical_func_sets
    procedure(no_dg_functions_i), nopass, pointer :: no_dg_functions
    procedure(no_vertices_i), nopass, pointer :: no_vertices
  end type local_elem_functionality
  
  type(local_elem_functionality), save :: functionality
  
  interface
    module subroutine get_old_local_elem_functionality(functionality)
      implicit none
      type(local_elem_functionality), intent(out) :: functionality
    end subroutine get_old_local_elem_functionality
    
    module subroutine get_orthofun_elem_functionality(functionality)
      implicit none
      type(local_elem_functionality), intent(out) :: functionality
    end subroutine get_orthofun_elem_functionality
  end interface
  
  abstract interface
    logical function coord_in_domain_i(eltype,coord)
      use f90_kind, only: dp
      implicit none
      integer, intent(in) :: eltype
      real(dp), dimension(:), intent(in) :: coord
    end function coord_in_domain_i
    
    pure integer function get_dimen_i(eltype)
      implicit none
      integer, intent(in) :: eltype
    end function get_dimen_i
    
    subroutine get_quadrature_i(eltype,order,abss,weights)
      use f90_kind, only: dp
      implicit none
      integer, intent(in) :: eltype
      integer, intent(in) :: order
      real(dp), allocatable, dimension(:,:), intent(out) :: abss
      real(dp), allocatable, dimension(:), intent(out) :: weights
    end subroutine get_quadrature_i
    
    subroutine local_dg_deriv_i(eltype,order,local_coord,local_deriv)
      use f90_kind, only: dp
      implicit none
      integer, intent(in) :: eltype
      integer, intent(in) :: order
      real(dp), intent(in) :: local_coord(:,:)
      real(dp), allocatable, intent(out) :: local_deriv(:,:,:)
    end subroutine local_dg_deriv_i
    
    subroutine local_dg_functions_i(eltype,order,local_coord,func,local_deriv,in_domain)
      use f90_kind, only: dp
      implicit none
      integer, intent(in) :: eltype
      integer, intent(in) :: order
      real(dp), intent(in) :: local_coord(:,:)
      real(dp), allocatable, optional, intent(out) :: func(:,:)
      real(dp), allocatable, optional, intent(out) :: local_deriv(:,:,:)
      logical, optional, intent(in) :: in_domain
    end subroutine local_dg_functions_i
    
    subroutine local_dg_functions_single_point_i(eltype,order,local_coord,func,local_deriv)
      use f90_kind, only: dp
      implicit none
      integer, intent(in) :: eltype
      integer, intent(in) :: order
      real(dp), intent(in) :: local_coord(:)
      real(dp), intent(out) :: func(:)
      real(dp), intent(out) :: local_deriv(:,:)
    end subroutine local_dg_functions_single_point_i
    
    logical function mutually_hierarchical_func_sets_i(eltype,order_1,order_2)
      ! Do basis function sets belonging to different orders always have the same first functions?
      implicit none
      integer, intent(in) :: eltype
      integer, intent(in) :: order_1, order_2
    end function mutually_hierarchical_func_sets_i
    
    pure integer function no_dg_functions_i(eltype,max_order) result(no_func)
      implicit none
      integer, intent(in) :: eltype
      integer, intent(in) :: max_order
    end function no_dg_functions_i
    
    pure integer function no_vertices_i(eltype)
      implicit none
      integer, intent(in) :: eltype
    end function no_vertices_i
  end interface
  
  interface
    ! These procedures depend on the vertices of local elements, and are thus duplicit.
    module pure subroutine convert_face_to_vol_local_coord(elem_eltype,loc_idx_face,dimen,xi_face,xi_vol)
      use f90_kind, only: dp
      implicit none
      integer, intent(in) :: elem_eltype
      integer, intent(in) :: loc_idx_face
      integer, intent(in) :: dimen
      real(dp), dimension(dimen-1), intent(in) :: xi_face
      real(dp), dimension(dimen), intent(out) :: xi_vol
    end subroutine convert_face_to_vol_local_coord
    
    module pure subroutine loc_idx_of_face_vertices_in_elem(eltype, loc_idx_face, loc_idx_vertices)
      implicit none
      integer, intent(in) :: eltype
      integer, intent(in) :: loc_idx_face
      integer, dimension(:), allocatable, intent(out) :: loc_idx_vertices
    end subroutine loc_idx_of_face_vertices_in_elem
    
    module pure integer function get_opposite_vertex(eltype,loc_idx_face) result(loc_idx_opposite_vtx)
      implicit none
      integer, intent(in) :: eltype
      integer, intent(in) :: loc_idx_face
    end function get_opposite_vertex
    
    module pure subroutine nodal_functions(eltype,local_coord,func,local_deriv,in_domain)
      use f90_kind, only: dp
      implicit none
      integer, intent(in) :: eltype
      real(dp), intent(in) :: local_coord(:,:)
      real(dp), allocatable, optional, intent(out) :: func(:,:)
      real(dp), allocatable, optional, intent(out) :: local_deriv(:,:,:)
      logical, optional, intent(in) :: in_domain
    end subroutine nodal_functions
    
    module pure subroutine nodal_functions_single_point(eltype,local_coord,func,local_deriv)
      use f90_kind, only: dp
      integer, intent(in) :: eltype
      real(dp), intent(in) :: local_coord(:)
      real(dp), intent(out) :: func(:)
      real(dp), optional, intent(out) :: local_deriv(:,:)
    end subroutine nodal_functions_single_point
  end interface
  
contains
  subroutine initialize()
    use assertions, only: assert
    use run_data, only: assume_affine_elems, basis_funcs
    
    select case(trim(basis_funcs))
      case('orthofun')
        call get_orthofun_elem_functionality(functionality)
      case('old')
        call assert(.not. assume_affine_elems, &
            "Assumptions for affine elems might not all work, because the DG basis is not &
            &hierarchical on triangles. This may still be fine on other elements though.")
        call get_old_local_elem_functionality(functionality)
    end select
    
    coord_in_domain => functionality%coord_in_domain
    get_dimen => functionality%get_dimen
    get_quadrature => functionality%get_quadrature
    local_dg_deriv => functionality%local_dg_deriv
    local_dg_functions => functionality%local_dg_functions
    local_dg_functions_single_point => functionality%local_dg_functions_single_point
    mutually_hierarchical_func_sets => functionality%mutually_hierarchical_func_sets
    no_dg_functions => functionality%no_dg_functions
    no_vertices => functionality%no_vertices
  end subroutine initialize

  real(dp) function local_elem_volume(eltype)
    use exceptions, only: error
    use f90_kind, only: dp
    use string_manipulations, only: int2char

    integer, intent(in) :: eltype

    select case(eltype)
      case(12);       local_elem_volume = 2.0_dp
      case(24);       local_elem_volume = 4.0_dp
      case(38);       local_elem_volume = 8.0_dp
      case default;   call error('local_elem_volume: not implemented for this eltype: ' // int2char(eltype))
    end select
  end function local_elem_volume
end module local_elem

