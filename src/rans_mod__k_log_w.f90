submodule(rans_mod) rans_mod__k_log_w
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use exceptions, only: error
  use math_const, only: SMALL
  use mesh_objects, only: elem_list
  use numerical_solutions, only: matprop_set, num_scalfunc_set, velocity, solve_for_energy
  use pnt_set_mod, only: pnt_set_t
  use run_data, only: boussinesq_approx, div_free_velocity, min_Pk_for_RANS, mesh_dimen
  use support, only: false, trace, axpy_own_trace_x_I
  implicit none
  
  ! Von-Karman constant
  real(dp), parameter :: Karman = 0.41_dp
  
  ! Model constants
  real(dp), parameter :: alpha = 5.0_dp/9.0_dp
  real(dp), parameter :: beta = 3.0_dp/40.0_dp
  real(dp), parameter :: beta_star = 9.0_dp/100.0_dp
  real(dp), parameter :: sigma = 1.0_dp/2.0_dp
  real(dp), parameter :: sigma_star = 1.0_dp/2.0_dp
  
  type, extends(rans_model_t) :: k_log_w_model_t
  contains
    procedure, nopass :: is_low_Re => false
  end type k_log_w_model_t
  
  class(scal_func_type), allocatable :: num_k, num_rho_k, num_log_w
  class(scal_func_type), allocatable :: nu_molec, beta_func, temperature
  logical :: add_buoyancy_term
  
contains
  module procedure get_k_log_w_model
    call num_scalfunc_set%get_func('vol_turb1',num_rho_k)
    call num_scalfunc_set%get_func('spec_turb1',num_k)
    call num_scalfunc_set%get_func('spec_turb2',num_log_w)
    call matprop_set%get_func('kinematic_viscosity',nu_molec)
    
    call solve_for_energy(add_buoyancy_term)
    if (add_buoyancy_term .and. boussinesq_approx) then
      call matprop_set%get_func('thermal_expansion_coeff',beta_func)
      call matprop_set%get_func('temperature',temperature)
    endif
    
    allocate(k_log_w_model_t :: rans_model)
    rans_model%kinem_visc%proc => kinem_visc_turb
    rans_model%diff_coeff_1st_eq%proc => diff_coeff_k
    rans_model%source_1st_eq%proc => source_k
    rans_model%react_coeff_1st_eq%proc => react_coeff_k
    rans_model%diff_coeff_2nd_eq%proc => diff_coeff_log_w
    rans_model%source_2nd_eq%proc => source_log_w
    rans_model%no_eq = 2
  end procedure get_k_log_w_model
  
  
  subroutine kinem_visc_turb(elem,pnt_set,time,values)
    ! The turbulent kinematic viscosity is bounded from below by a small
    ! fraction of the molecular viscosity (see Lew A.J. et al. - "A note on 
    ! the numerical treatment of the k-epsilon turbulence model")
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu, k, log_wr
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call num_k%get_values(elem,pnt_set,time,k)
    call log_w_realizable(elem,pnt_set,time,log_wr)
    do p = 1, no_points
      values(p) = max(max(k(p),0.0_dp)*exp(-log_wr(p)),1.0e-1_dp*nu(p))
    enddo
  end subroutine kinem_visc_turb
  
  
  subroutine diff_coeff_k(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu_t, nu
    
    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)
    
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    values = nu + sigma_star*nu_t
  end subroutine diff_coeff_k
  
  
  subroutine source_k(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: prod_values, diss_values
    
    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)
    
    call production_k(elem,pnt_set,time,prod_values)
    call dissipation_k(elem,pnt_set,time,diss_values)
    values = prod_values + diss_values
  end subroutine source_k
  
  subroutine production_k(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: Gk
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call shear_production(elem,pnt_set,time,values)
    if (add_buoyancy_term) then
      call buoyancy_term(elem,pnt_set,time,Gk)
      do p = 1, no_points
        values(p) = values(p) + max(Gk(p),0.0_dp)
      enddo
    endif
  end subroutine production_k
  
  subroutine shear_production(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu_t, rho, rho_k
    real(dp) :: grad_U(elem%dimen(),velocity%no_dirs(),size(values))
    real(dp) :: div_U
    integer :: no_points, p
    character(200) :: err_msg
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    call density%get_values(elem,pnt_set,time,rho)
    call num_rho_k%get_values(elem,pnt_set,time,rho_k)
    call velocity%get_gders(elem,pnt_set,time,grad_U)
    do p = 1, no_points
      ! Strain rate tensor part: mu_t*(dU_i/dx_j + dU_j/dx_i)*dU_i/dx_j = 2 * mu_t * S_ij**2
      values(p) = rho(p)*nu_t(p)*0.5_dp*sum((grad_U(:,:,p) + transpose(grad_U(:,:,p)))**2)
      if (.not. div_free_velocity) then
        ! Divergence part: -(2/3)*mu_t*(div.U)*delta_ij*dU_i/dx_j = -(2/3)*mu_t*(div.U)^2
        div_U = trace(grad_U(:,:,p))
        values(p) = values(p) - (2.0_dp/3.0_dp)*rho(p)*nu_t(p)*(div_U**2)
        ! Isotropic part: -(2/3)*rho*k*delta_ij*dU_i/dx_j = -(2/3)*rho*k*div.U
        values(p) = values(p) - (2.0_dp/3.0_dp)*max(rho_k(p),0.0_dp)*div_U
      endif
      if (min_Pk_for_RANS <= values(p) .and. values(p) < 0.0_dp) then
        ! Cap small negative production terms to zero
        values(p) = 0.0_dp
      elseif (values(p) < min_Pk_for_RANS) then
        write(err_msg,"('shear turb. production = ',es10.3,' < ',es10.3)") values(p), min_Pk_for_RANS
        call error(trim(err_msg))
      endif
    enddo
  end subroutine shear_production
  
  subroutine buoyancy_term(elem,pnt_set,time,values)
    use sources_mod, only: spec_force_func
    
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu_t, rho, beta
    real(dp), dimension(elem%dimen(),size(values)) :: g, grad_rho, grad_T
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    call spec_force_func%get_values(elem,pnt_set,time,g)
    if (boussinesq_approx) then
      call density%get_values(elem,pnt_set,time,rho)
      call beta_func%get_values(elem,pnt_set,time,beta)
      call temperature%get_gders(elem,pnt_set,time,grad_T)
    else
      call density%get_gders(elem,pnt_set,time,grad_rho)
    endif
    do p = 1, no_points
      if (boussinesq_approx) then
        values(p) = (nu_t(p)/Pr_t) * rho(p) * beta(p) * dot_product(g(:,p),grad_T(:,p))
      else
        values(p) = -(nu_t(p)/Pr_t) * dot_product(g(:,p),grad_rho(:,p))
      endif
    enddo
  end subroutine buoyancy_term
    
  subroutine dissipation_k(elem,pnt_set,time,values)
    ! The dissipation term in k-equation is bounded from below to
    ! to enforce its positivity (see Lew A.J. et al. - "A note on 
    ! the numerical treatment of the k-epsilon turbulence model")
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: rho_k, log_wr
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call num_rho_k%get_values(elem,pnt_set,time,rho_k)
    call log_w_realizable(elem,pnt_set,time,log_wr)
    do p = 1, no_points
      values(p) = beta_star * max(rho_k(p),0.0_dp) * exp(log_wr(p))
    enddo
  end subroutine dissipation_k
  
  
  subroutine react_coeff_k(elem,pnt_set,time,values)
    ! The reaction coefficient derives from the linearization of the 
    ! dissipation term at the r.h.s. of the k-equation (see Zijlema M. -
    ! "Computational Modeling of Turbulent Flows in General Domains", p.91)
    ! It is bounded from below to enforce its positivity (see Lew A.J. 
    ! et al. - "A note on the numerical treatment of the k-epsilon 
    ! turbulence model")
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: log_wr, rho_k, Gk
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call log_w_realizable(elem,pnt_set,time,log_wr)
    if (add_buoyancy_term) then
      call buoyancy_term(elem,pnt_set,time,Gk)
      call num_rho_k%get_values(elem,pnt_set,time,rho_k)
    endif
    do p = 1, no_points
      values(p) = 2.0_dp * beta_star * exp(log_wr(p))
      if (add_buoyancy_term) then
        if (rho_k(p) > 0.0_dp) values(p) = values(p) - min(Gk(p),0.0_dp) / max(rho_k(p),SMALL)
      endif
    enddo
  end subroutine react_coeff_k
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine diff_coeff_log_w(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: nu_t, nu
    
    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)
    
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    values = nu + sigma*nu_t
  end subroutine diff_coeff_log_w
  
  
  subroutine source_log_w(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: prod_values, diss_values, grad_grad_log_w_values
    
    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)
    
    call production_log_w(elem,pnt_set,time,prod_values)
    call grad_grad_log_w_term(elem,pnt_set,time,grad_grad_log_w_values)
    call dissipation_log_w(elem,pnt_set,time,diss_values)
    values = prod_values + grad_grad_log_w_values + diss_values
  end subroutine source_log_w
  
  subroutine production_log_w(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: Pk, log_wr, nu_t, Gk
    real(dp) :: P_tot
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call shear_production(elem,pnt_set,time,Pk)
    if (add_buoyancy_term) call buoyancy_term(elem,pnt_set,time,Gk)
    call kinem_visc_turb(elem,pnt_set,time,nu_t)
    call log_w_realizable(elem,pnt_set,time,log_wr)
    do p = 1, no_points
      P_tot = Pk(p)
      if (add_buoyancy_term) P_tot = P_tot + max(Gk(p),0.0_dp)
      values(p) = alpha * P_tot / max(nu_t(p)*exp(log_wr(p)),SMALL)
    enddo
  end subroutine production_log_w
  
  subroutine grad_grad_log_w_term(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(elem%dimen(),size(values)) :: grad_log_w
    real(dp), dimension(size(values)) :: diff_coeff, rho
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call num_log_w%get_gders(elem,pnt_set,time,grad_log_w)
    call diff_coeff_log_w(elem,pnt_set,time,diff_coeff)
    call density%get_values(elem,pnt_set,time,rho)
    do p = 1, no_points
      values(p) = rho(p)*diff_coeff(p)*dot_product(grad_log_w(:,p),grad_log_w(:,p))
    enddo
  end subroutine grad_grad_log_w_term
  
  subroutine dissipation_log_w(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: log_wr, rho
    
    call log_w_realizable(elem,pnt_set,time,log_wr)
    call density%get_values(elem,pnt_set,time,rho)
    values =  - beta * rho * exp(log_wr)
  end subroutine dissipation_log_w
  
  
  subroutine log_w_realizable(elem,pnt_set,time,values)
    ! Follow the notation of Schoenawa and Hartmann, JCP 262(2014) 194-216
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp) :: grad_U(mesh_dimen,mesh_dimen,size(values)), log_w(size(values))
    real(dp) :: S(mesh_dimen,mesh_dimen)
    real(dp) :: w_r0, a_ij, b_ij
    integer :: no_points, p, i, j
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call num_log_w%get_values(elem,pnt_set,time,log_w)
    call velocity%get_gders(elem,pnt_set,time,grad_U)
    do p = 1, no_points
      w_r0 = SMALL
      S = grad_U(:,:,p) + transpose(grad_U(:,:,p))
      if (.not. div_free_velocity) call axpy_own_trace_x_I(S,-1.0_dp/3.0_dp)
      do i = 1, mesh_dimen
        w_r0 = max(w_r0,1.5_dp*S(i,i))
        do j = 1, mesh_dimen
          if (j /= i) then
            a_ij = S(i,i) + S(j,j)
            b_ij = S(i,i)*S(j,j) - S(i,j)**2
            w_r0 = max(w_r0,1.5_dp*(0.5_dp*a_ij + sqrt(0.25_dp*a_ij**2 - b_ij)))
          endif
        enddo
      enddo
      values(p) = max(log_w(p),log(w_r0))
    enddo
  end subroutine log_w_realizable
end submodule rans_mod__k_log_w
