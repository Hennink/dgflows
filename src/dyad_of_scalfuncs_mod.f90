module dyad_of_scalfuncs_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use dyad_mod, only: dyad_t
  use f90_kind, only: dp
  use mesh_objects, only: elem_list
  use pnt_set_mod, only: pnt_set_t
  use scal_func_mod, only: scal_func_type
  implicit none
  
  private
  public :: dyad_of_scalfuncs_t, dyad_of_scalfuncs
  
  type, extends(scal_func_type) :: dyad_of_scalfuncs_t
    ! Create a function of the form f = f(arg1,arg2). Here f is a dyad.
    private
    class(scal_func_type), allocatable :: arg1, arg2
    class(dyad_t), allocatable :: dyad
  contains
    procedure :: get_values, get_gders
  end type dyad_of_scalfuncs_t
  
contains
  pure type(dyad_of_scalfuncs_t) function dyad_of_scalfuncs(dyad,arg1,arg2)
    use scal_func_mod, only: scal_func_type

    class(dyad_t), intent(in) :: dyad
    class(scal_func_type), intent(in) :: arg1, arg2

    allocate(dyad_of_scalfuncs%dyad,source=dyad)
    allocate(dyad_of_scalfuncs%arg1,source=arg1)
    allocate(dyad_of_scalfuncs%arg2,source=arg2)
  end function dyad_of_scalfuncs

  subroutine get_values(this,elem,pnt_set,time,values)
    class(dyad_of_scalfuncs_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)

    real(dp) :: args_1(size(values)), args_2(size(values))

    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)

    call this%arg1%get_values(elem,pnt_set,time,args_1)
    call this%arg2%get_values(elem,pnt_set,time,args_2)
    values = this%dyad%func(args_1,args_2)
  end subroutine get_values

  subroutine get_gders(this,elem,pnt_set,time,gders)
    class(dyad_of_scalfuncs_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:)

    real(dp), dimension(size(gders,2)) :: args_1, args_2
    real(dp), dimension(elem%dimen(),size(gders,2)) :: gder_arg1, gder_arg2
    real(dp), dimension(size(gders,2)) :: derivs_wrt_arg1, derivs_wrt_arg2
    integer :: p, np

    call assert(size(gders,1) == elem%dimen(),BOUNDS_CHECK)
    np = assert_eq(size(gders,2),pnt_set%np(),BOUNDS_CHECK)
    
    call this%arg1%get_values(elem,pnt_set,time,args_1)
    call this%arg2%get_values(elem,pnt_set,time,args_2)
    call this%arg1%get_gders(elem,pnt_set,time,gder_arg1)
    call this%arg2%get_gders(elem,pnt_set,time,gder_arg2)
    derivs_wrt_arg1 = this%dyad%deriv_wrt_arg1(args_1,args_2)
    derivs_wrt_arg2 = this%dyad%deriv_wrt_arg2(args_1,args_2)
    do p = 1, np
      gders(:,p) =   gder_arg1(:,p) * derivs_wrt_arg1(p) &
                   + gder_arg2(:,p) * derivs_wrt_arg2(p)
    enddo
  end subroutine get_gders
end module dyad_of_scalfuncs_mod
