module h_solver_mod
  use assertions, only: assert
  use exceptions, only: error , warning
  use f90_kind, only: dp
  use numerical_solutions, only: matprop_set, solve_for_energy, temporal_order_for_qnty
  use pde_mod, only: pde_t
  use qnty_solver_mod, only: qnty_solver_t
  use run_data, only: constant_density, constant_k_over_cp, &
                      corrective_convection_enthalpy, dt, &
                      clip_k_over_cp, les_specs_file, use_rans_model
  use scal_func_mod, only: scal_func_type
  use support, only: false
  implicit none

  private
  public :: h_solver_t, eff_k_over_cp
  public :: temporal_weights

  type, extends(qnty_solver_t) :: h_solver_t
    ! Solver for the specific enthalpy from the conservative equation:
    !     d/dt (rho h) + div(m h)  =  div((lambda/cp) grad(H/rho)) + vol_source + ...
    
    logical :: include_time_term = .true.
    character(:), allocatable :: impl_strategy
    class(scal_func_type), pointer ::  newrho_predictor => null()
  contains
    procedure, non_overridable :: collect_pde_varfrozconst
    procedure, non_overridable, private :: impl_time
  end type

contains
  subroutine collect_pde_varfrozconst(this, variable_terms, frozen_terms, constant_terms)
    use boundaries, only: bc_are_time_independent
    use convection, only: conv_t
    use density_weighted_time_mod, only: expl_time
    use fick_sip_mod, only: sip_ficks_law_t
    use func_operations_mod, only: operator(/)
    use interface_continuity_mod, only: interface_continuity_t
    use petsc_mod, only: i_am_master_process
    use mesh_type, only: there_is_an_interface
    use numerical_solutions, only: implicit_convection_for_qnty, find_if_add_conv_corr_for_qnty
    use sources_mod, only: add_tot_enthalpy_src_to_pde, enthalpy_src_is_constant
    use solvec_mod, only: solvec_t
    use time_der_mod, only: time_deriv_t

    class(h_solver_t), intent(in) :: this
    type(pde_t), intent(out) :: variable_terms, frozen_terms, constant_terms

    type(sip_ficks_law_t) :: sip
    type(interface_continuity_t) :: interface_continuity
    type(conv_t) :: conv
    real(dp), allocatable :: weights(:)
    type(time_deriv_t) :: conservative_ddt_impl, conservative_ddt_expl

    if (this%qnty == 'vol_enthalpy') then
      sip%diff_param = thermal_diffusivity()
      sip%add_mass_flux_term = .not. (constant_density .or. corrective_convection_enthalpy)
    elseif (this%qnty == 'spec_enthalpy') then
      call assert(.not. corrective_convection_enthalpy,'No CC for spec. enthalpy.')
      if (this%include_time_term) then
        call eff_k_over_cp(sip%diff_param)
      else
        if ((les_specs_file /= '' .or. use_rans_model) .and. i_am_master_process()) call warning('skip LES/RANS in h when d/dt=0')
        call matprop_set%get_func('k_over_cp', sip%diff_param)
      endif
      sip%add_mass_flux_term = .false.
    else
      call error('NOT IMPLEMENTED: sip for qnty='//trim(this%qnty))
    endif
    sip%explicit = .false.
    if (.not. bc_are_time_independent) then
      call frozen_terms%add_term(sip)
    elseif (this%qnty == 'spec_enthalpy' .and. constant_k_over_cp) then
      call constant_terms%add_term(sip)
    elseif (this%qnty == 'vol_enthalpy' .and. constant_k_over_cp .and. constant_density) then ! technically only alpha need be const
      call constant_terms%add_term(sip)
    else
      call frozen_terms%add_term(sip)
    endif

    if (there_is_an_interface) then
      interface_continuity%explicit = .false.
      call variable_terms%add_term(interface_continuity)
    endif

    if (enthalpy_src_is_constant) then
      call add_tot_enthalpy_src_to_pde(constant_terms,'volumetric')
    else
      call add_tot_enthalpy_src_to_pde(frozen_terms,'volumetric')
    endif

    call find_if_add_conv_corr_for_qnty(this%qnty,conv%add_vol_div_corr,conv%add_face_jump_corr)
    if (this%qnty == 'vol_enthalpy') then
      conv%convecting_qnty = 'velocity'
    elseif (this%qnty == 'spec_enthalpy') then
      conv%convecting_qnty = 'mass_flux'
    else
      call error('NOT IMPLEMENTED: convecting_qnty for '//trim(this%qnty))
    endif
    conv%explicit = .not. implicit_convection_for_qnty(this%qnty)
    if (corrective_convection_enthalpy) call conv%set_cc_coeff(thermal_diffusivity())
    call variable_terms%add_term(conv)

    if (this%include_time_term) then
      weights = temporal_weights(dt)
      if (conserved_qnty(this%qnty)) then
        conservative_ddt_impl%weights = weights
        conservative_ddt_impl%include_explicit_terms = .false.
        call constant_terms%add_term(conservative_ddt_impl)
        conservative_ddt_expl%weights = weights
        conservative_ddt_expl%include_implicit_term = .false.
        call frozen_terms%add_term(conservative_ddt_expl)
      else
        call frozen_terms%add_term(expl_time('rho_x_spec_h',weights(2:)))
        if (constant_density) then
          call constant_terms%add_term(this%impl_time(weights(1)))
        else
          call variable_terms%add_term(this%impl_time(weights(1)))
        endif
      endif
    endif
  end subroutine

  function thermal_diffusivity() result(alpha)
    use func_operations_mod, only: operator(*), operator(/)

    class(scal_func_type), allocatable :: alpha

    class(scal_func_type), allocatable :: k_over_cp, rho
    
    if (les_specs_file /= '' .or. use_rans_model) then
      call matprop_set%get_func('density',rho)
      call eff_k_over_cp(k_over_cp)
      alpha = k_over_cp / rho
    else
      call matprop_set%get_func('thermal_diffusivity',alpha)
    endif
  end function

  subroutine eff_k_over_cp(k_over_cp)
    ! Returns the effective thermal conductivity over the specific heat capacity:
    !     k_eff / c_p .
    use func_operations_mod
    use les_heat_flux_mod, only: k_over_cp_sfs
    use numerical_solutions, only: matprop_set
    use rans_mod, only: rans_model

    class(scal_func_type), allocatable, intent(out) :: k_over_cp

    class(scal_func_type), allocatable :: k_over_cp_mol, k_over_cp_clipped

    call assert(.not. (les_specs_file /= '' .and. use_rans_model), "Don't mix LES and RANS.")

    call matprop_set%get_func('k_over_cp', k_over_cp_mol)
    if (use_rans_model) then
      k_over_cp = k_over_cp_mol + rans_model%thermal_cond_over_cp
    elseif (les_specs_file /= '' ) then
      call assert(allocated(k_over_cp_sfs), "attempting to use `k_over_cp_sfs`, but it's not yet allocated")
      k_over_cp = k_over_cp_mol + k_over_cp_sfs
    else
      call move_alloc(k_over_cp_mol, k_over_cp)
    endif

    if (clip_k_over_cp) then
      k_over_cp_clipped = max_of(k_over_cp,0.0_dp)
      call move_alloc(k_over_cp_clipped,k_over_cp)
    endif
  end subroutine

  type(spec_h_implicit_time_t) function impl_time(this,weight)
    use spec_h_implicit_time_mod, only: spec_h_implicit_time_t

    class(h_solver_t), intent(in) :: this
    real(dp), intent(in) :: weight
    
    impl_time%weight = weight
    impl_time%strategy = this%impl_strategy
    impl_time%newrho_predictor => this%newrho_predictor
  end function

  function temporal_weights(dt)
    use phys_time_mod, only: BDF_weights

    real(dp), intent(in) :: dt
    real(dp), allocatable :: temporal_weights(:)

    integer :: order
    logical :: solve_for_engy
    character(:), allocatable :: engy_qnty

    call solve_for_energy(solve_for_engy,engy_qnty)
    call assert(solve_for_engy, 'no temporal_weights for H/h when not solving for H/h')

    order = temporal_order_for_qnty(engy_qnty)
    temporal_weights = BDF_weights(order,dt)
  end function

  logical function conserved_qnty(qnty)
    character(*), intent(in) :: qnty

    select case(qnty)
      case('spec_enthalpy');  conserved_qnty = .false.
      case('vol_enthalpy ');  conserved_qnty = .true.
      case default;           call error('unknown qnty=' // qnty)
    end select
  end function
end module
