module pde_mod
  use pde_term_mod, only: pde_term_t
  implicit none

  private
  public :: pde_t
  public :: move_terms_with_lhs

  type :: pde_term_holder_t
    class(pde_term_t), allocatable :: pde_term
  end type pde_term_holder_t

  integer, parameter :: max_no_terms = 8

  type :: pde_t
    private
    integer :: no_terms = 0
    type(pde_term_holder_t), dimension(max_no_terms) :: pde_terms
  contains
    procedure, non_overridable :: add_term
    procedure, non_overridable :: add_to_glob_linsys
    procedure, non_overridable :: ngbr_coupling_depth
    procedure, non_overridable :: has_terms
    procedure, non_overridable, private :: build_loc_linsys
    procedure, non_overridable, private :: merge_terms
    generic :: operator(+) => add_pdes
    procedure, non_overridable, private :: add_pdes
    generic :: assignment(=) => copy_pde
    procedure, non_overridable, private :: copy_pde
  end type pde_t

contains
  subroutine move_terms_with_lhs(a, b)
    ! All terms in `a` that have a LHS are moved from `a` to `b`.
    type(pde_t), intent(inout) :: a, b

    type(pde_t) :: new_a
    integer :: t

    do t = 1, a%no_terms
      associate(term => a%pde_terms(t)%pde_term)
        if (term%ngbr_coupling_depth() >= 0) then
          call b%add_term(term)
        else
          call new_a%add_term(term)
        endif
      end associate
    enddo
    a = new_a
  end subroutine

  subroutine add_term(this,pde_term)
    use assertions, only: assert

    class(pde_t), intent(inout) :: this
    class(pde_term_t), intent(in) :: pde_term

    call assert(this%no_terms < max_no_terms)

    this%no_terms = this%no_terms + 1
    allocate(this%pde_terms(this%no_terms)%pde_term,source=pde_term)
  end subroutine add_term

  type(pde_t) function add_pdes(this,other) result(sum_pdes)
    class(pde_t), intent(in) :: this
    type(pde_t), intent(in) :: other

    call sum_pdes%merge_terms(this)
    call sum_pdes%merge_terms(other)
  end function add_pdes

  subroutine copy_pde(this,other)
    class(pde_t), intent(out) :: this
    type(pde_t), intent(in) :: other

    call this%merge_terms(other)
  end subroutine copy_pde

  subroutine merge_terms(this,other)
    use assertions, only: assert

    class(pde_t), intent(inout) :: this
    type(pde_t), intent(in) :: other

    integer :: i

    do i = 1, other%no_terms
      call this%add_term(other%pde_terms(i)%pde_term)
    enddo
  end subroutine merge_terms

  subroutine add_to_glob_linsys(this,qnty,mesh,lhs,rhs)
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: elem_list, elem_id
    use mesh_partitioning, only: my_first_last_elems
    use mesh_type, only: fem_mesh
    use petsc_mat_mod, only: petsc_mat_type
    use petsc_vec_mod, only: petsc_vec_type
    use solvec_mod, only: solvec_t

    class(pde_t), intent(in) :: this
    type(solvec_t), intent(in) :: qnty
    type(fem_mesh), intent(in) :: mesh
    type(petsc_mat_type), intent(inout) :: lhs
    type(petsc_vec_type), intent(inout) :: rhs

    integer :: elem_no
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    type(local_linsys_t) :: local_linsys
    integer :: first_elem, last_elem
    type(elem_id), pointer :: active_elem_list(:)
    
    call my_first_last_elems(qnty%handler%only_fluid_mesh,first_elem,last_elem)
    
    call mesh%get_active_elem_list_ptr(qnty%handler%only_fluid_mesh,active_elem_list)
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      call local_linsys%init(qnty,elem_no)
      call this%build_loc_linsys(qnty,elem,local_linsys)
      call local_linsys%add_to_glob_linsys(lhs,rhs)
    enddo
  end subroutine add_to_glob_linsys

  integer function ngbr_coupling_depth(this) result(depth)
    class(pde_t), intent(in) :: this
    
    integer :: i

    depth = maxval([( this%pde_terms(i)%pde_term%ngbr_coupling_depth(), i = 1, this%no_terms )])
  end function

  pure logical function has_terms(this)
    class(pde_t), intent(in) :: this

    has_terms = this%no_terms >= 1
  end function

  subroutine build_loc_linsys(this,qnty,elem,local_linsys)
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: elem_list, elem_ptr, elem_id, &
                            face_list, face_ptr
    use solvec_mod, only: solvec_t

    class(pde_t), intent(in) :: this
    type(solvec_t), intent(in) :: qnty
    type(elem_list), pointer, intent(in) :: elem
    type(local_linsys_t), intent(inout) :: local_linsys

    type(elem_ptr), allocatable, dimension(:) :: active_neighbors
    type(face_ptr), allocatable, dimension(:) :: active_subfaces
    integer :: i, t
    type(face_list), pointer :: sub_face

    do t = 1, this%no_terms
      associate(pde_term => this%pde_terms(t)%pde_term)
        call pde_term%process_elem(elem,qnty,local_linsys)
      end associate
    enddo

    call elem%active_subfaces(active_subfaces,active_neighbors)
    do i = 1, size(active_subfaces)
      sub_face => active_subfaces(i)%point
      do t = 1, this%no_terms
        associate(pde_term => this%pde_terms(t)%pde_term)
          call pde_term%process_face(sub_face,sub_face%elem_is_E1(elem),qnty,local_linsys)
        end associate
      enddo
    enddo
  end subroutine build_loc_linsys
end module pde_mod
