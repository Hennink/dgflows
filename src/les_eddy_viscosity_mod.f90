module les_eddy_viscosity_mod
  use f90_kind, only: dp
  use les_const_eddy_visc_mod, only: can_calculate_smag
  use mesh_type, only: fem_mesh
  use scalfunc_from_proc_mod, only: scalfunc_from_proc_t
  implicit none

  private
  public :: read_les_momentum_model_from_file
  public :: recalc_eddy_viscosity
  public :: write_LES_models_to_gmsh

  type(scalfunc_from_proc_t), allocatable, protected, public :: eddy_kin_viscosity

  ! This should be called before solving for the mass flux, to give LES models
  ! a chance to precalculate some stuff like averages:
  procedure(do_nothing), pointer, protected :: recalc_eddy_viscosity => null()

contains
  subroutine read_les_momentum_model_from_file(unit,mesh)
    ! Initializes LES models with specifications from a file. The first model 
    ! is used for the calculations.
    ! The other models can be used for comparison, or perhaps to switch during 
    ! a calculation.
    use dyn_smag_visc_mod, only: init_dyn_les_model_from_file, dyn_smag_kin_visc, recalc_avg_LM_MM_eqv_elems
    use exceptions, only: error
    use io_basics, only: read_line_from_input_file
    use les_const_eddy_visc_mod, only: const_sfs_nu, init_const_les_model_from_file
    use string_manipulations, only: split
    use string_mod, only: as_chars

    integer, intent(in) :: unit
    type(fem_mesh), intent(in) :: mesh

    character(:), allocatable :: header, model_specs(:)
    integer :: i

    call read_line_from_input_file(unit,header)
    model_specs = as_chars(split(header,','))
    do i = 1, size(model_specs)
      select case(model_specs(i))
        case('const_eddy_visc');  call init_const_les_model_from_file(unit)
        case('dyn_smag');         call init_dyn_les_model_from_file(mesh,unit)
        case default;             call error('unknown LES mmtm model: ' // model_specs(i))
      end select
    enddo

    allocate(eddy_kin_viscosity)
    select case(model_specs(1))
      case('const_eddy_visc')
        eddy_kin_viscosity%proc => const_sfs_nu
        recalc_eddy_viscosity => do_nothing
      case('dyn_smag')
        eddy_kin_viscosity%proc => dyn_smag_kin_visc
        recalc_eddy_viscosity => recalc_avg_LM_MM_eqv_elems
      case default
        call error('unknown LES mmtm model: ' // model_specs(1))
    end select
  end subroutine read_les_momentum_model_from_file

  subroutine do_nothing(mesh)
    type(fem_mesh), intent(in) :: mesh
  end subroutine do_nothing

  subroutine write_LES_models_to_gmsh(mesh)
    ! Outputs stuff for the initialized LES models.
    ! Careful with sade effects: this might recalculate dynamic parameters.
    use gmsh_interface, only: write_to_gmsh_file
    use dyn_smag_visc_mod, only: initialized_dyn_smag, LM_MM_in_group, recalc_avg_LM_MM_eqv_elems, dsm_groups => elem_groups
    
    type(fem_mesh), intent(in) :: mesh

    character(:), allocatable :: names(:)
    real(dp) :: cs_delta__dyn(size(LM_MM_in_group))
    
    if (can_calculate_smag()) then
      names = [character(100) :: 'rel_nu_sfs', 'rel_nu_wale', 'rel_nu_qr', 'square_c_delta__smag', 'rel_nu_smag']
    else
      names = [character(100) :: 'rel_nu_sfs', 'rel_nu_wale', 'rel_nu_qr']
    endif
    call write_to_gmsh_file(mesh,names,.true.,get_interesting_les_vals)

    if (initialized_dyn_smag) then
      call recalc_avg_LM_MM_eqv_elems(mesh)
      where (LM_MM_in_group > 0.0_dp)
        cs_delta__dyn = sqrt(0.5_dp * LM_MM_in_group)
      elsewhere
        cs_delta__dyn = 0.0_dp
      end where
      call dsm_groups%write_vals_on_groups_to_gmsh('cs_delta__dyn',mesh,cs_delta__dyn)
    endif
  end subroutine write_LES_models_to_gmsh

  subroutine get_interesting_les_vals(elem,pnt_set,vals)
    use assertions, only: assert_eq
    use les_const_eddy_visc_mod, only: const_smag_sfs_nu, const_wale_sfs_nu, const_qr_sfs_nu, square_C_x_Delta__smag
    use math_const, only: SMALL
    use mesh_objects, only: elem_list
    use numerical_solutions, only: matprop_set
    use pnt_set_mod, only: pnt_set_t
    use scal_func_mod, only: scal_func_type
    
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    real(dp), intent(out) :: vals(:,:)
    
    integer, parameter :: TIME = 2
    integer :: np
    class(scal_func_type), pointer :: nu_molec_func
    real(dp), allocatable, dimension(:) :: nu_molec, nu_sfs, sfs_nu_smag, sfs_nu_wale, sfs_nu_QR, smag_const_prefact

    np = assert_eq(size(vals,1),pnt_set%np())

    allocate(nu_molec(np), nu_sfs(np), sfs_nu_smag(np), sfs_nu_wale(np), sfs_nu_QR(np), smag_const_prefact(np))
    nu_molec_func => matprop_set%func_ptr('kinematic_viscosity')
    call nu_molec_func%get_values(elem,pnt_set,TIME,nu_molec)
    call eddy_kin_viscosity%get_values(elem,pnt_set,TIME,nu_sfs)
    if (can_calculate_smag()) then
      call square_C_x_Delta__smag(elem, pnt_set, TIME, smag_const_prefact)
      call const_smag_sfs_nu(elem,pnt_set,TIME,sfs_nu_smag)
    endif
    call const_wale_sfs_nu(elem, pnt_set, TIME, sfs_nu_wale)
    call const_qr_sfs_nu(elem, pnt_set, TIME, sfs_nu_QR)

    vals(:,1) = nu_sfs      / nu_molec
    vals(:,2) = sfs_nu_wale / nu_molec
    vals(:,3) = sfs_nu_QR   / nu_molec
    if (can_calculate_smag()) then
      vals(:,4) = smag_const_prefact
      vals(:,5) = sfs_nu_smag / nu_molec
    endif
  end subroutine get_interesting_les_vals
end module les_eddy_viscosity_mod
