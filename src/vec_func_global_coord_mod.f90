module vec_func_global_coord_mod
  ! This module defines an abstract base class from which one could inherit to
  ! implement a vec_func_type in terms of a global coordinate.
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use mesh_objects, only: elem_list
  use pnt_set_mod, only: pnt_set_t
  use vec_func_mod, only: vec_func_type
  implicit none
  
  private
  public :: vec_func_global_coord_type
  
  type, abstract, extends(vec_func_type) :: vec_func_global_coord_type
  contains
    procedure, non_overridable :: get_values, get_gders
    procedure(value_at_gcor_i), deferred, public :: value_at_gcor
    procedure :: gder_at_gcor
  end type vec_func_global_coord_type
  
  abstract interface
    function value_at_gcor_i(this,gcor,dimen) result(value)
      use f90_kind, only: dp
      import :: vec_func_global_coord_type
      implicit none
      
      class(vec_func_global_coord_type), intent(in) :: this
      real(dp), intent(in) :: gcor(:)
      integer, intent(in) :: dimen
      real(dp) :: value(dimen)
    end function value_at_gcor_i
  end interface
  
contains
  subroutine get_values(this,elem,pnt_set,time,values)
    class(vec_func_global_coord_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:,:)
    
    integer :: no_points, p, dimen
    
    dimen = assert_eq(size(values,1),this%no_dirs(),BOUNDS_CHECK)
    no_points = assert_eq(size(values,2),pnt_set%np(),BOUNDS_CHECK)
    
    do p = 1, no_points
      values(:,p) = this%value_at_gcor(pnt_set%gcors(:,p),dimen)
    enddo
  end subroutine get_values
  
  subroutine get_gders(this,elem,pnt_set,time,gders)
    class(vec_func_global_coord_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:,:)
    
    integer :: no_points, p, dimen
    
    call assert(size(gders,1) == elem%dimen() .and. size(pnt_set%lcors,1) ==  elem%dimen(),BOUNDS_CHECK)
    no_points = assert_eq(size(gders,3),pnt_set%np(),BOUNDS_CHECK)
    dimen = assert_eq(size(gders,2),this%no_dirs(),BOUNDS_CHECK)
    
    do p = 1, no_points
      gders(:,:,p) = this%gder_at_gcor(pnt_set%gcors(:,p),dimen)
    enddo
  end subroutine get_gders
  
  function gder_at_gcor(this,gcor,dimen) result(gder)
    use f90_kind, only: dp

    class(vec_func_global_coord_type), intent(in) :: this
    real(dp), intent(in) :: gcor(:)
    integer, intent(in) :: dimen
    real(dp) :: gder(size(gcor),dimen)

    real(dp), parameter :: h = sqrt(epsilon(1.0d0))
    integer :: d
    real(dp), dimension(size(gcor)) :: x_low, x_up
    real(dp) :: diff_vals(dimen)
    
    do d = 1, size(gcor)
      x_low = gcor
      x_up = gcor
      x_low(d) = x_low(d) - h
      x_up(d) = x_up(d) + h
      diff_vals = this%value_at_gcor(x_up,dimen) - this%value_at_gcor(x_low,dimen)
      gder(d,:) = diff_vals / (2.0d0 * h)
    enddo
  end function gder_at_gcor
end module vec_func_global_coord_mod

