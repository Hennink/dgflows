module dyad_of_vec_scal_funcs_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use dyad_mod, only: dyad_t
  use f90_kind, only: dp
  use mesh_objects, only: elem_list
  use pnt_set_mod, only: pnt_set_t
  use run_data, only: mesh_dimen
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only: vec_func_type
  implicit none

  private
  public :: dyad_of_vec_scal_funcs_t, dyad_of_vec_scal_funcs

  type, extends(vec_func_type) :: dyad_of_vec_scal_funcs_t
    ! A vec_func with components dyad(vec_i,scal)
    private
    class(vec_func_type), allocatable :: vec
    class(scal_func_type), allocatable :: scal
    class(dyad_t), allocatable :: dyad
  contains
    procedure, non_overridable :: no_dirs
    procedure, non_overridable :: get_values, get_gders
  end type dyad_of_vec_scal_funcs_t

contains
  pure type(dyad_of_vec_scal_funcs_t) function dyad_of_vec_scal_funcs(dyad,vec,scal)
    class(dyad_t), intent(in) :: dyad
    class(vec_func_type), intent(in) :: vec
    class(scal_func_type), intent(in) :: scal

    allocate(dyad_of_vec_scal_funcs%dyad,source=dyad)
    allocate(dyad_of_vec_scal_funcs%vec,source=vec)
    allocate(dyad_of_vec_scal_funcs%scal,source=scal)
  end function dyad_of_vec_scal_funcs


  pure integer function no_dirs(this)
    class(dyad_of_vec_scal_funcs_t), intent(in) :: this

    no_dirs = this%vec%no_dirs()
  end function no_dirs

  subroutine get_values(this,elem,pnt_set,time,values)
    class(dyad_of_vec_scal_funcs_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:,:)

    real(dp) :: scal_values(size(values,2))
    integer :: p, np

    call assert(size(values,1) == this%no_dirs(),BOUNDS_CHECK)
    np = assert_eq(size(values,2),pnt_set%np(),BOUNDS_CHECK)
    
    call this%vec%get_values(elem,pnt_set,time,values)
    call this%scal%get_values(elem,pnt_set,time,scal_values)
    do p = 1, np
      values(:,p) = this%dyad%func(values(:,p),scal_values(p))
    enddo
  end subroutine get_values
  
  subroutine get_gders(this,elem,pnt_set,time,gders)
    class(dyad_of_vec_scal_funcs_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:,:)

    real(dp), allocatable :: &
        deriv1(:,:), deriv2(:,:), &
        scal_vals(:), scal_gders(:,:), &
        vec_vals(:,:), vec_gders(:,:,:)
    integer :: p, np, dir, no_dirs, d

    call assert(size(gders,1) == mesh_dimen, elem%dimen() == mesh_dimen, BOUNDS_CHECK)
    no_dirs = assert_eq(size(gders,2), this%no_dirs(), BOUNDS_CHECK)
    np = assert_eq(size(gders,3), pnt_set%np(), BOUNDS_CHECK)

    allocate(deriv1(no_dirs,np), deriv2(no_dirs,np))
    allocate(scal_vals(np), scal_gders(mesh_dimen,np))
    allocate(vec_vals(no_dirs,np), vec_gders(mesh_dimen,no_dirs,np))

    call this%scal%get_values(elem,pnt_set,time,scal_vals)
    call this%scal%get_gders(elem,pnt_set,time,scal_gders)
    call this%vec%get_values(elem,pnt_set,time,vec_vals)
    call this%vec%get_gders(elem,pnt_set,time,vec_gders)

    do p = 1, np
      do dir = 1, no_dirs
        deriv1(dir,p) = this%dyad%deriv_wrt_arg1(vec_vals(dir,p), scal_vals(p))
        deriv2(dir,p) = this%dyad%deriv_wrt_arg2(vec_vals(dir,p), scal_vals(p))
      enddo
    enddo
    do p = 1, np
      do dir = 1, no_dirs
        do d = 1, mesh_dimen
          gders(d,dir,p) = deriv1(dir,p) * vec_gders(d,dir,p) + deriv2(dir,p) * scal_gders(d,p)
        enddo
      enddo
    enddo
  end subroutine
end module dyad_of_vec_scal_funcs_mod
