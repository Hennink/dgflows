module dyad_product_mod
  use dyad_mod, only: dyad_t
  implicit none

  private
  public :: product_t

  type, extends(dyad_t) :: product_t
  contains
    procedure :: func => mult
    procedure :: deriv_wrt_arg1
    procedure :: deriv_wrt_arg2
  end type product_t

contains
  elemental real(dp) function mult(this,arg1,arg2)
    use f90_kind, only: dp

    class(product_t), intent(in) :: this
    real(dp), intent(in) :: arg1, arg2

    mult = arg1 * arg2
  end function mult

  elemental real(dp) function deriv_wrt_arg1(this,arg1,arg2)
    use f90_kind, only: dp

    class(product_t), intent(in) :: this
    real(dp), intent(in) :: arg1, arg2

    deriv_wrt_arg1 = arg2
  end function deriv_wrt_arg1

  elemental real(dp) function deriv_wrt_arg2(this,arg1,arg2)
    use f90_kind, only: dp

    class(product_t), intent(in) :: this
    real(dp), intent(in) :: arg1, arg2

    deriv_wrt_arg2 = arg1
  end function deriv_wrt_arg2
end module dyad_product_mod
