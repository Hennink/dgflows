module math_const

  !---------------------------------------------------------------------
  ! This module contains some math constants
  !---------------------------------------------------------------------

  use f90_kind
  implicit none

  real(dp), parameter :: SMALL = sqrt(epsilon(1.0_dp))

  real(dp), parameter :: pi = 3.14159265358979_dp
  real(dp), parameter :: twopi = 2.0_dp*pi
  
  real(dp), parameter :: FROM_CM3_TO_M3 = 1.0e-6_dp
  real(dp), parameter :: FROM_CM2_TO_M2 = 1.0e-4_dp
  
contains
  pure function levi_civita_2D()
    integer, dimension(2,2) :: levi_civita_2D
    
    levi_civita_2D = 0
    levi_civita_2D(2,1) = -1
    levi_civita_2D(1,2) = +1
  end function levi_civita_2D
  
  pure function levi_civita_3D()
    integer, dimension(3,3,3) :: levi_civita_3d
    
    levi_civita_3D = 0
    levi_civita_3D(3,2,1) = -1
    levi_civita_3D(2,3,1) = +1
    levi_civita_3D(3,1,2) = +1
    levi_civita_3D(1,3,2) = -1
    levi_civita_3D(2,1,3) = -1
    levi_civita_3D(1,2,3) = +1
  end function levi_civita_3D
end module math_const

