module gsip_viscous_stress_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use gsip, only: gsip_t
  use scal_func_mod, only: scal_func_type
  use run_data, only: div_free_velocity, rm_transp_stress_term, mesh_dimen
  implicit none
  
  private
  public :: gsip_viscous_stress_t

  type, extends(gsip_t) :: gsip_viscous_stress_t
  contains
    procedure, non_overridable :: process_elem, process_internal_face
    procedure, non_overridable :: stress_tensor_at_pnt_set
    procedure, non_overridable :: nonFick_part_of_stress
    procedure, non_overridable :: grad_velocity_at_qps
    procedure, non_overridable, nopass :: get_bc_in_gsip_context
  end type gsip_viscous_stress_t
  
contains
  subroutine process_elem(this,elem,qnty,local_linsys)
    use array_opers_mod, only: add_weighted_tensprod, gemm_classic
    use exceptions, only: error
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: elem_list
    use solvec_mod, only: solvec_t

    class(gsip_viscous_stress_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys

    logical, parameter :: use_blas = .true.
    integer :: elem_no
    integer :: nnod, p, i, j, dir
    real(dp), allocatable :: adj_gders(:,:,:)
    real(dp) :: quot_x_D(elem%nqp)
    real(dp), allocatable :: t_elemmats(:,:,:,:), tdiag(:,:)
    real(dp), allocatable :: w_x_transp_gders(:,:,:)

    elem_no = elem%get_active_elem_no(qnty%handler%only_fluid_mesh)
    call assert(qnty%handler%order(elem_no) >= 1,"(G)SIP is only for higher order functions.")

    call this%density_adjusted_grad(elem,elem%quad_points,qnty%handler,adj_gders)
    quot_x_D = elem%quot * this%diff_param%values_at_elem_qps(elem,this%time_diff_param)
    nnod = qnty%handler%no_nod_elem(elem_no)
    allocate(t_elemmats(nnod, mesh_dimen, nnod, mesh_dimen),source=0.0_dp)
    allocate(tdiag(nnod,nnod),source=0.0_dp)

    allocate(w_x_transp_gders(nnod, mesh_dimen, elem%nqp))
    do p = 1, elem%nqp
      w_x_transp_gders(:,:,p) = elem%quad_points%dg_transp_gders(:nnod,:,p) * quot_x_D(p)
    enddo

    if (use_blas) then
      call gemm_classic(TRANSA=.false., TRANSB=.true., &
          M=nnod, N=nnod, K=mesh_dimen*elem%nqp, &
          ALPHA=1.0_dp, A=adj_gders, LDA=nnod, B=w_x_transp_gders, LDB=nnod, &
          BETA=1.0_dp,  C=tdiag, LDC=nnod)

    else
      do p = 1, elem%nqp
        do dir = 1, mesh_dimen
          call add_weighted_tensprod(tdiag, adj_gders(:,dir,p), w_x_transp_gders(:,dir,p), 1.0_dp)
        enddo
      enddo
    endif

    if (.not. (rm_transp_stress_term .and. div_free_velocity)) then
      do p = 1, elem%nqp
        do i = 1, mesh_dimen
          do j = 1, mesh_dimen
            if (.not. rm_transp_stress_term) &
                call add_weighted_tensprod(t_elemmats(:,j,:,i), adj_gders(:,i,p), w_x_transp_gders(:,j,p), 1.0_dp)
            if (.not. div_free_velocity) &
                call add_weighted_tensprod(t_elemmats(:,j,:,i), adj_gders(:,j,p), w_x_transp_gders(:,i,p), -2/3.0_dp)
          enddo
        enddo
      enddo
    endif
    call local_linsys%add_transposed_elemmat_all_dirs(elem_no, tdiag)
    call local_linsys%add_transposed_elemmats_for_dirs(elem_no, t_elemmats)
  end subroutine process_elem

  subroutine process_internal_face(this,face,Ein_is_E1,qnty,local_linsys)
    use array_opers_mod, only: add_tensprod, subtract_tensprod, add_weighted_tensprod
    use gsip, only: parent_proc => process_internal_face
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: face_list
    use solvec_mod, only: solvec_t

    class(gsip_viscous_stress_t), intent(in) :: this
    type(face_list), intent(in) :: face
    logical, intent(in) :: Ein_is_E1
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys

    integer :: side_Ein, side_Eout
    real(dp) :: normal(mesh_dimen,face%nqp)
    real(dp) :: quot_x_D_Ein(face%nqp), quot_x_D_Eout(face%nqp)
    integer :: elem_no, ngbr_no
    integer :: nnod_elem, nnod_ngbr
    real(dp), allocatable :: dg_funcs_Ein(:,:), dg_funcs_Eout(:,:)
    real(dp), allocatable :: adj_grad_Ein(:,:,:), adj_grad_Eout(:,:,:)
    real(dp), allocatable, dimension(:,:,:,:) :: t_elemmats_ee, t_elemmats_en
    real(dp), allocatable :: vec_elem1(:), vec_elem2(:), vec_ngbr(:)
    real(dp), allocatable :: big_help_elem(:,:,:), big_help_ngbr(:,:,:)
    integer :: p, i, j

    call assert(.not. face%is_boundary(only_fluid_mesh=.true.))

    call parent_proc(this,face,Ein_is_E1,qnty,local_linsys)
    if (rm_transp_stress_term .and. div_free_velocity) return

    if (Ein_is_E1) then
      side_Ein = 1; side_Eout = 2
      normal = face%normal
    else
      side_Ein = 2; side_Eout = 1
      normal = - face%normal
    endif
    associate(elem => face%elem_neighbors(side_Ein)%point, &
              ngbr => face%elem_neighbors(side_Eout)%point, &
              handler => qnty%handler, &
              points_Ein => face%quad_points(side_Ein), &
              points_Eout => face%quad_points(side_Eout))
      elem_no = elem%get_active_elem_no(handler%only_fluid_mesh)
      ngbr_no = ngbr%get_active_elem_no(handler%only_fluid_mesh)
      nnod_elem = handler%no_nod_elem(elem_no)
      nnod_ngbr = handler%no_nod_elem(ngbr_no)
      allocate(dg_funcs_Ein,source=points_Ein%dg_funcs(1:nnod_elem,:))
      allocate(dg_funcs_Eout,source=points_Eout%dg_funcs(1:nnod_ngbr,:))
      call this%density_adjusted_grad(elem,points_Ein,handler,adj_grad_Ein)
      call this%density_adjusted_grad(ngbr,points_Eout,handler,adj_grad_Eout)
    end associate
    quot_x_D_Ein = face%quot * this%diff_param%values_at_qps(face,side_Ein,this%time_diff_param)
    quot_x_D_Eout = face%quot * this%diff_param%values_at_qps(face,side_Eout,this%time_diff_param)

    allocate(t_elemmats_ee(nnod_elem,mesh_dimen,nnod_elem,mesh_dimen),source=0.0_dp)
    allocate(t_elemmats_en(nnod_ngbr,mesh_dimen,nnod_elem,mesh_dimen),source=0.0_dp)
    allocate(vec_elem1(nnod_elem),vec_elem2(nnod_elem),vec_ngbr(nnod_ngbr))
    allocate(big_help_elem(nnod_elem,mesh_dimen,mesh_dimen),big_help_ngbr(nnod_ngbr,mesh_dimen,mesh_dimen))
    
    ! non-Fick stress components:
    do p = 1, face%nqp
      do j = 1, mesh_dimen
        do i = 1, mesh_dimen
          big_help_elem(:,i,j) = 0.5_dp*quot_x_D_Ein(p)  * normal(i,p) * adj_grad_Ein(:,j,p)
          big_help_ngbr(:,i,j) = 0.5_dp*quot_x_D_Eout(p) * normal(i,p) * adj_grad_Eout(:,j,p)
        enddo
      enddo
      do i = 1, mesh_dimen
        do j = 1, mesh_dimen
          vec_elem1 = 0.0_dp
          vec_elem2 = 0.0_dp
          vec_ngbr = 0.0_dp
          if (.not. rm_transp_stress_term) then
            vec_elem1 = vec_elem1 - big_help_elem(:,j,i)
            vec_elem2 = vec_elem2 - big_help_elem(:,i,j)
            vec_ngbr = vec_ngbr - big_help_ngbr(:,j,i)
          endif
          if (.not. div_free_velocity) then
            vec_elem1 = vec_elem1 - (-2.0_dp/3.0_dp) * big_help_elem(:,i,j)
            vec_elem2 = vec_elem2 - (-2.0_dp/3.0_dp) * big_help_elem(:,j,i)
            vec_ngbr = vec_ngbr - (-2.0_dp/3.0_dp) * big_help_ngbr(:,i,j)
          endif
          ! Consistency term
          call add_tensprod(t_elemmats_ee(:,j,:,i), vec_elem1, dg_funcs_Ein(:,p))
          call add_tensprod(t_elemmats_en(:,j,:,i), vec_ngbr,  dg_funcs_Ein(:,p))
          ! Symmetry term
          call add_tensprod(t_elemmats_ee(:,j,:,i), dg_funcs_Ein(:,p), vec_elem2)
          call subtract_tensprod(t_elemmats_en(:,j,:,i), dg_funcs_Eout(:,p), vec_elem2)
        enddo
      enddo
    enddo
    call local_linsys%add_transposed_elemmats_for_dirs(elem_no, t_elemmats_ee)
    call local_linsys%add_transposed_elemmats_for_dirs(ngbr_no, t_elemmats_en)
  end subroutine process_internal_face

  subroutine stress_tensor_at_pnt_set(this,elem,pnt_set,handler,stress)
    use dof_handler, only: dof_handler_type
    use lincom_rank2_mod, only: lincom_rank2_t
    use mesh_objects, only: elem_list
    use pnt_set_mod, only: pnt_set_t
    
    class(gsip_viscous_stress_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    type(dof_handler_type), intent(in) :: handler
    type(lincom_rank2_t), intent(out) :: stress
    
    stress = this%grad_velocity_at_qps(elem,pnt_set,handler)
    if (.not. div_free_velocity) call stress%axpy_own_trace_x_I(-1.0_dp/3.0_dp)
    if (.not. rm_transp_stress_term) call stress%add_own_transpose()
  end subroutine
  
  subroutine nonFick_part_of_stress(this,elem,pnt_set,handler,stress)
    use dof_handler, only: dof_handler_type
    use mesh_objects, only: elem_list
    use lincom_rank2_mod, only: lincom_rank2_t
    use pnt_set_mod, only: pnt_set_t
    
    class(gsip_viscous_stress_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    type(dof_handler_type), intent(in) :: handler
    type(lincom_rank2_t), intent(out) :: stress
    
    type(lincom_rank2_t) :: fick_part, full_stress
    
    fick_part = this%grad_velocity_at_qps(elem,pnt_set,handler)
    call this%stress_tensor_at_pnt_set(elem,pnt_set,handler,full_stress)
    stress = full_stress - fick_part
  end subroutine
  
  function grad_velocity_at_qps(this,elem,pnt_set,handler)
    use dof_handler, only: dof_handler_type
    use mesh_objects, only: elem_list
    use numerical_solutions, only: matprop_set
    use lincom_rank1_mod, only: lincom_rank1_t
    use lincom_rank2_mod, only: lincom_rank2_t
    use pnt_set_mod, only: pnt_set_t
    
    class(gsip_viscous_stress_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    type(dof_handler_type), intent(in) :: handler
    type(lincom_rank2_t) :: grad_velocity_at_qps
    
    integer :: active_elem_no
    class(scal_func_type), pointer :: rho_func
    real(dp), allocatable :: rel_grad_rho(:,:)
    type(lincom_rank1_t) :: mass_flux
    integer :: np

    active_elem_no = elem%get_active_elem_no(handler%only_fluid_mesh)
    call grad_velocity_at_qps%set_as_gder_at_pnts(active_elem_no,pnt_set,handler)
    
    if (this%add_mass_flux_term) then
      np = assert_eq(grad_velocity_at_qps%no_points(),pnt_set%np(),BOUNDS_CHECK)
      allocate(rel_grad_rho(elem%dimen(),np))
      rho_func => matprop_set%func_ptr('density')
      call rho_func%rel_grad(elem,pnt_set,this%time_density,rel_grad_rho)
      call mass_flux%set_stencil_for_func(active_elem_no,handler,pnt_set)
      call grad_velocity_at_qps%add_weighted_tensor_prod(rel_grad_rho,mass_flux,-1.0_dp)
    endif
  end function grad_velocity_at_qps
  
  subroutine get_bc_in_gsip_context(bc_id,qnty,bc)
    use boundaries, only: get_bc
    use boundary_condition_types, only: bc_type
    use solvec_mod, only: solvec_t
    
    integer, intent(in) :: bc_id
    type(solvec_t), intent(in) :: qnty
    class(bc_type), allocatable, intent(out) :: bc
    
    call get_bc(bc_id,qnty%name,bc)
  end subroutine get_bc_in_gsip_context
end module gsip_viscous_stress_mod

