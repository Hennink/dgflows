module pressure_corr
  use assertions, only: assert
  use boundaries, only: is_periodic
  use div_cont_penalties_and_err, only: init_M_corr_linsys, solve_M_corr_linsys
  use dof_handler, only: dof_handler_type, set_type_and_preallocate
  use f90_kind, only: dp
  use mass_mtx, only: get_inv_mass_matrix
  use math_const, only: SMALL
  use mesh_objects, only: elem_list, elem_ptr, elem_id, &
                          face_list, face_ptr
  use mesh_partitioning, only: my_first_last_elems
  use mesh_type, only: fem_mesh
  use numerical_solutions, only: get_solvec, get_dof_handler, temporal_order_for_qnty, &
                                 pressure_mtx_is_singular, pressure_needs_penalization
  use petsc_ksp_mod, only: petsc_ksp_t
  use petsc_mat_mod, only: petsc_mat_type, mat_type_t, SYMMETRY_ETERNAL, SPD, &
                           mat_struct_opt_type, same_nonzero_pattern_opt, subset_nonzero_pattern_opt, SHARE_NONZERO_PATTERN
  use petsc_mod, only: I_am_master_process, no_petsc_procs
  use petsc_solver_mod, only: set_up, solve_system
  use petsc_vec_mod, only: petsc_vec_type
  use phys_time_mod, only: ith_BDF_weight, constant_ith_BDF_weight, BDF_weights_are_constant_from_now_on
  use pressure, only: rhs_continuity_eq, init_pressure_nullspace_vec, &
                      const_pressure_proj, gradient_oper
  use run_data, only: constant_kinematic_viscosity, &
                      check_mat_symm, print_mtx_sparsity_info, &
                      replace_local_dg_by_sip, add_m_penalties, m_penalties_in
  use solvec_mod, only: solvec_t
  use timer_typedef, only: timer_type
  implicit none
  
  private
  public :: init_pressure_correction
  public :: pressure_correction
  public :: create_p_penalty_mat, penalty_is_constant_from_now_on
  public :: init_LDG_p_mtx, init_SIP_p_mtx

  type(timer_type), public, protected :: p_solve_timer, pc_timer

  logical :: constant_PMat_is_complete
  type(petsc_mat_type), target :: constant_PMat ! Part of the pressure matrix that is constant.
  type(petsc_mat_type), allocatable, target :: constant_penalization
  type(petsc_ksp_t) :: ksp

  type(petsc_vec_type) :: poisson_sol
  
  type(petsc_mat_type) :: min_inv_M_x_grad ! := - [mass_matrix]^-1 * [gradient_oper]

  logical, parameter :: FORCE_AIJ = .false. ! PETSc's algebraic multigrid only works with `AIJ`

contains
  subroutine pressure_correction(mesh,dt,rel_norm_corr)
    type(fem_mesh), intent(in) :: mesh
    real(dp), intent(in) :: dt
    real(dp), optional, intent(out) :: rel_norm_corr
    
    type(petsc_mat_type), target :: nonconstant_PMat
    
    type(petsc_mat_type), pointer :: penalization
    type(petsc_mat_type), target :: nonconstant_penalization

    type(petsc_mat_type), pointer :: PMat
    type(petsc_vec_type) :: Prhs

    type(solvec_t), pointer :: P, M
    integer :: M_time_order
    real(dp) :: alpha_over_dt
    type(mat_struct_opt_type) :: structure_opt
    
    call pc_timer%start()

    P => get_solvec('pressure')
    M => get_solvec('mass_flux')

    M_time_order = temporal_order_for_qnty('mass_flux')
    alpha_over_dt = ith_BDF_weight(M_time_order,dt,i=1)

    call precalc_full_PMat_if_possible(mesh)

    ! PMat is either constant, or we add penalization:
    if (constant_PMat_is_complete) then
      PMat => constant_PMat
      if (pressure_needs_penalization) penalization => constant_penalization
    else
      call assert(pressure_needs_penalization, "why is PMat not complete when you don't need to add penalization?")
      
      call create_p_penalty_mat(mesh,P%handler,nonconstant_penalization)

      call constant_PMat%duplicate(SHARE_NONZERO_PATTERN,nonconstant_PMat)
      call constant_PMat%copy(nonconstant_PMat,same_nonzero_pattern_opt)
      structure_opt = merge(same_nonzero_pattern_opt, subset_nonzero_pattern_opt, replace_local_dg_by_sip)
      call nonconstant_PMat%axpy(alpha_over_dt,nonconstant_penalization,structure_opt)
      if (check_mat_symm) call assert(nonconstant_PMat%is_symm(),'nonconstant_PMat is not symmetric')
      if (print_mtx_sparsity_info) then
        write(*,'(a)',advance='no') 'nonconstant_PMat: '
        call nonconstant_PMat%print_sparsity_info()
      endif

      if (pressure_mtx_is_singular()) call nonconstant_PMat%set_nullspace_vec(const_pressure_proj)

      PMat => nonconstant_PMat
      penalization => nonconstant_penalization
      call ksp%set_operators(PMat, PMat)
    endif
    
    ! P_src  =  - g  +  div(M^~)  -  pressure_stab * P^~
    call PMat%init_compatible_vecs(left_vec=Prhs)
    call rhs_continuity_eq(mesh,P%handler,dt,Prhs)
    if (pressure_needs_penalization) call penalization%mult_add(P%petsc_coeffs,Prhs,Prhs)
    call Prhs%mult_with_scal(-1.0_dp)
    call gradient_oper%mult_transpose_add(M%petsc_coeffs,Prhs,Prhs)
    
    ! Solve the system
    call p_solve_timer%start()
    call solve_system(ksp, poisson_sol, Prhs)
    call p_solve_timer%pauze()

    ! This is very inefficient due to double `norm_2` call, both of which are blocking and have a very uneven process load.
    ! Asynchronous computation with `VecNormBegin` and `VecNormEnd` should already be much better.
    if (present(rel_norm_corr)) rel_norm_corr = alpha_over_dt * poisson_sol%norm_2() / max(P%petsc_coeffs%norm_2(),SMALL)
    
    ! Pressure correction: P = P + alpha/dt * poisson_sol
    call P%petsc_coeffs%axpy(alpha_over_dt,poisson_sol)
    call P%update_ghost_values()
    
    ! Mass flux correction
    if (add_m_penalties > 0 .and. m_penalties_in == 'm-postproc') then
      call solve_M_corr_linsys(mesh,dt,poisson_sol)
    else
      ! Compute: M = M - inv_mass_matrix * gradient_operator * poisson_sol
      call min_inv_M_x_grad%mult_add(poisson_sol,M%petsc_coeffs,M%petsc_coeffs)
      call M%update_ghost_values()
    endif

    call pc_timer%pauze()
  end subroutine pressure_correction
  
  subroutine precalc_full_PMat_if_possible(mesh)
    ! Repeated calls have no effect.
    ! 
    use run_data, only: dt

    type(fem_mesh), intent(in) :: mesh

    integer :: M_time_order
    real(dp) :: alpha_over_dt
    
    if (constant_PMat_is_complete) return
    call assert(pressure_needs_penalization, "Why is 'constant_PMat_is_complete' false, when p doesn't need stabilization?")
    
    if (.not. penalty_is_constant_from_now_on()) return

    ! Form a complete, constant PMat:
    ! (The matrix already has a null space, no need to add it here.)
    if (I_am_master_process()) write(*,'(/,a,/)') "=== FORMING A CONSTANT PRESSURE MATRIX ==="
    allocate(constant_penalization)
    call create_p_penalty_mat(mesh,get_dof_handler('pressure'),constant_penalization)
    M_time_order = temporal_order_for_qnty('mass_flux')
    alpha_over_dt = constant_ith_BDF_weight(M_time_order,dt,i=1)
    call constant_PMat%axpy(alpha_over_dt,constant_penalization,subset_nonzero_pattern_opt)
    
    ! Some checks:
    if (check_mat_symm) call assert(constant_PMat%is_symm(),'constant_PMat is not symmetric')
    if (print_mtx_sparsity_info) then
      write(*,'(a)',advance='no') 'constant_PMat: '
      call constant_PMat%print_sparsity_info()
    endif

    call ksp%set_operators(constant_PMat, constant_PMat)
    
    constant_PMat_is_complete = .true.
  end subroutine

  logical function penalty_is_constant_from_now_on()
    call assert(pressure_needs_penalization, "penalty_is_constant_from_now_on: did not expect a call unless you need penal")

    penalty_is_constant_from_now_on = constant_kinematic_viscosity .and. BDF_weights_are_constant_from_now_on()
  end function

  subroutine init_pressure_correction(mesh)
    use run_data, only: dt

    type(fem_mesh), intent(in) :: mesh
    
    type(dof_handler_type), pointer :: P_dof_handler, M_dof_handler
    type(solvec_t), pointer :: P_solvec_ptr
    type(petsc_mat_type) :: inv_mass_matrix
    
    M_dof_handler => get_dof_handler('mass_flux')
    P_dof_handler => get_dof_handler('pressure')
    P_solvec_ptr => get_solvec('pressure')
    
    if (I_am_master_process()) print *, 'Init pressure correction matrices...'
 
    call get_inv_mass_matrix(mesh,M_dof_handler,inv_mass_matrix)
 
    if (add_m_penalties > 0 .and. m_penalties_in == 'm-postproc') then
      call init_M_corr_linsys(mesh)
    else
      call inv_mass_matrix%mult_with_mat(gradient_oper, min_inv_M_x_grad)
      call min_inv_M_x_grad%mult_with_scal(-1.0_dp)
    endif
    
    if (replace_local_dg_by_sip) then
      call init_SIP_p_mtx(mesh,P_solvec_ptr,constant_PMat)
    else
      call init_LDG_p_mtx(mesh,P_dof_handler,inv_mass_matrix,constant_PMat)
    endif

    if (check_mat_symm) call assert(constant_PMat%is_symm(),'constant_PMat is not symmetric')
    
    if (pressure_mtx_is_singular()) then
      call init_pressure_nullspace_vec(mesh,constant_PMat,P_dof_handler)
      call constant_PMat%set_nullspace_vec(const_pressure_proj)
    endif
    
    if (print_mtx_sparsity_info) then
      write(*,'(a)',advance='no') 'constant_PMat: '
      call constant_PMat%print_sparsity_info()
    endif
    
    call P_solvec_ptr%petsc_coeffs%duplicate(poisson_sol)

    constant_PMat_is_complete = .not. pressure_needs_penalization

    call set_up(ksp, 'p')
    if (constant_PMat_is_complete) call ksp%set_operators(constant_PMat, constant_PMat)
    
    if (I_am_master_process()) print *, 'done.'
  end subroutine
  
  subroutine init_LDG_p_mtx(mesh,P_handler,inv_mass_matrix,LDG_p_mat)
    ! Construct LDG matrix = gradient_oper^T * M^-1 * gradient_oper
    !
    use petsc_mat_mod, only: Pt_A_P, mat_type_t, aij, sbaij

    type(fem_mesh), intent(in) :: mesh
    type(dof_handler_type), intent(in) :: P_handler
    type(petsc_mat_type), intent(in) :: inv_mass_matrix
    type(petsc_mat_type), intent(out) :: LDG_p_mat

    type(mat_type_t) :: P_mat_type

    call Pt_A_P(A=inv_mass_matrix,P=gradient_oper,C=LDG_p_mat)
    call LDG_p_mat%set_option(SYMMETRY_ETERNAL,.true.) ! not really needed after SPD
    call LDG_p_mat%set_option(SPD,.true.)
    P_mat_type = merge(aij, sbaij, force_aij)
    call LDG_p_mat%convert_inplace(P_mat_type)
  end subroutine

  subroutine init_SIP_p_mtx(mesh,P_solvec,SIP_p_mat)
    use fick_sip_mod, only: shahbazi_pressure_op_t
    use functions_lib, only: const_func
    use local_linsys_mod, only: local_linsys_t
    use run_data, only: p_SIP_penal_safety_factor

    type(fem_mesh), intent(in) :: mesh
    type(solvec_t), intent(in) :: P_solvec
    type(petsc_mat_type), intent(out) :: SIP_p_mat
    
    type(shahbazi_pressure_op_t) :: sip
    integer :: first_elem, last_elem, elem_no
    type(elem_ptr), allocatable :: active_neighbors(:)
    type(face_ptr), allocatable :: active_subfaces(:)
    integer :: i
    type(face_list), pointer :: sub_face
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    type(local_linsys_t) :: local_linsys
    
    call SIP_p_mat%create()
    call set_type_and_preallocate(SIP_p_mat, mesh, P_solvec%handler, P_solvec%handler, coupling_depth=1, &
        symm=.true., force_aij = FORCE_AIJ)
    
    sip%diff_param = const_func(1.0_dp)
    sip%explicit = .false.
    sip%penalty_mult_factor = p_SIP_penal_safety_factor
    
    call my_first_last_elems(.true.,first_elem,last_elem)
    do elem_no = first_elem, last_elem
      id = mesh%active_fluid_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      call local_linsys%init(P_solvec,elem_no)
      
      call sip%process_elem(elem,P_solvec,local_linsys)
      
      call elem%active_subfaces(active_subfaces,active_neighbors)
      do i = 1, size(active_subfaces)
        sub_face => active_subfaces(i)%point
        call sip%process_face(sub_face,sub_face%elem_is_E1(elem),P_solvec,local_linsys)
      enddo
      
      call local_linsys%add_to_glob_linsys(SIP_p_mat)
    enddo
    
    call SIP_p_mat%finalize_assembly()
  end subroutine
  
  subroutine create_p_penalty_mat(mesh,P_handler,P_stab_mat)
    ! Assembles penalty matrix with
    !     p_stab_safety_factor * sub_face%diameter / nu
    ! as a penalty parameter.
    use lincom_rank1_mod, only: lincom_rank1_t
    use math_const, only: SMALL
    use m_solver_mod, only: eff_kinematic_viscosity
    use mtx_block_mod, only: mtx_block_t
    use run_data, only: p_stab_safety_factor
    use scal_func_mod, only: scal_func_type

    type(fem_mesh), intent(in) :: mesh
    type(dof_handler_type), intent(in) :: P_handler
    type(petsc_mat_type), intent(out) :: P_stab_mat
    
    integer :: first_elem, last_elem
    type(mtx_block_t) :: mtx_ee, mtx_en
    type(lincom_rank1_t) :: P_func_Ein, P_func_Eout
    integer :: elem_no, nghb_no
    type(elem_list), pointer :: elem, nghb
    type(elem_ptr), allocatable :: active_neighbors(:)
    type(face_ptr), allocatable :: active_subfaces(:)
    type(face_list), pointer :: sub_face
    type(elem_id) :: id
    integer :: i
    integer :: side_Ein, side_Eout
    class(scal_func_type), allocatable :: nu_func
    real(dp), allocatable :: nu(:,:), quot_over_nu(:)
    real(dp) :: factor
    
    integer, parameter :: TIME_NU = 2

    call P_stab_mat%create()
    call set_type_and_preallocate(P_stab_mat, mesh, P_handler, P_handler, coupling_depth=1, symm=.true., force_aij = FORCE_AIJ)
    
    call eff_kinematic_viscosity(nu_func)
    
    call my_first_last_elems(.true.,first_elem,last_elem)
    do elem_no = first_elem, last_elem
      id = mesh%active_fluid_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      call mtx_ee%init(elem_no,elem_no,P_handler)
      
      ! Loop over internal faces:
      call elem%active_subfaces(active_subfaces,active_neighbors)
      do i = 1, size(active_subfaces)
        sub_face => active_subfaces(i)%point
        if (sub_face%is_boundary(only_fluid_mesh=.true.)) cycle

        nghb => active_neighbors(i)%point
        nghb_no = nghb%active_fluid_elem_no
        call mtx_en%init(elem_no,nghb_no,P_handler)
        if (sub_face%elem_is_E1(elem)) then
          side_Ein = 1; side_Eout = 2
        else
          side_Ein = 2; side_Eout = 1
        endif
        call P_func_Ein%set_as_func(sub_face,side_Ein,P_handler)
        call P_func_Eout%set_as_func(sub_face,side_Eout,P_handler)
        factor = p_stab_safety_factor * sub_face%diameter

        ! Use the minimal value of `nu` across the face:
        allocate(nu(sub_face%nqp, 2))
        nu(:,1) = nu_func%values_at_qps(sub_face,1,TIME_NU)
        nu(:,2) = nu_func%values_at_qps(sub_face,2,TIME_NU)
        call assert(all(nu >= SMALL), "Need to divide by `nu` in pressure stabilization, but `nu` is small.")
        quot_over_nu = sub_face%quot / minval(nu,dim=2)
        
        call mtx_ee%add_int_dotprod(P_func_Ein,P_func_Ein,quot_over_nu,factor)
        call mtx_en%add_int_dotprod(P_func_Ein,P_func_Eout,quot_over_nu,-factor)
        deallocate(nu, quot_over_nu)

        call mtx_en%add_to_glob_mtx(P_stab_mat,P_handler)
      enddo
      call mtx_ee%add_to_glob_mtx(P_stab_mat,P_handler)
    enddo
    
    call P_stab_mat%finalize_assembly()
  end subroutine
end module pressure_corr
