submodule(boundary_types) boundary_types__wall_func_adiabatic
  ! This boundary type is compatible with internal walls bcs
  ! in conjugate heat transfer calculations.
  ! In fact, it is possible to do CHT with temperature/enthalpy
  ! wall functions on internal walls.
  implicit none
  
  type, extends(wall_bnd_t) :: wall_func_adiabatic_type
  contains
    procedure :: read_specs_from_file
    procedure :: get_bc
  end type wall_func_adiabatic_type
  
contains
  module procedure alloc_wall_func_adiabatic
    allocate(wall_func_adiabatic_type :: bnd)
  end procedure alloc_wall_func_adiabatic
  
  subroutine read_specs_from_file(this,unit_number)
    use rans_wall_functions_mod, only: init_rans_wall_func_mod => initialize
    
    class(wall_func_adiabatic_type), intent(out) :: this
    integer, intent(in) :: unit_number
    
    !This command introduces a "side-effect" in the subroutine.
    !However, I think it is the best place where to put this initialization
    !command. In this way, the rans_wall_functions module is initialized
    !if and only if this kind of bc is used. Moreover, it seemed conceptually
    !correct, since wall-functions bc cannot be initialized from a file, but 
    !indeed from the specific RANS module. 
    call init_rans_wall_func_mod()
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types
    use functions_lib, only: zero_vecfunc
    use rans_wall_functions_mod, only: wf_slipcoeff, &
                                       wf_bc_vol_turb2_qnty
    
    class(wall_func_adiabatic_type), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    
    type(dirichlet_bc_type), allocatable :: dirichlet_bc
    type(hom_neumann_bc_type), allocatable :: hom_neumann_bc
    type(slip_bc_type), allocatable :: slip_bc
    
    select case(qnty_name)
      case('temperature','spec_enthalpy','vol_enthalpy','density')
        allocate(hom_neumann_bc)
        call move_alloc(hom_neumann_bc,bc)
      case('mass_flux')
        allocate(slip_bc)
        allocate(slip_bc%slip_coeff, source=wf_slipcoeff)
        call move_alloc(slip_bc,bc)
      case('velocity')
        ! This is just a trick to get the Shahbazi operator to work
        allocate(slip_bc)
        call move_alloc(slip_bc,bc)
      case('vol_turb1')
        !bc for k: homogeneous Neumann
        allocate(bc,source=hom_neumann_bc_type())
      case('vol_turb2')
        !bc for rho*eps/rho*omega: Dirichlet bc
        allocate(dirichlet_bc)
        allocate(dirichlet_bc%inhom_part, source=wf_bc_vol_turb2_qnty)
        call move_alloc(dirichlet_bc,bc)
      case('wall_dist')
        allocate(dirichlet_bc)
        allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(1)) !Should we add delta???
        call move_alloc(dirichlet_bc,bc)
      case default
        write(*,'(a)') 'ERROR: no bc for '//qnty_name//' in wall_functions'
        error stop
    end select
  end subroutine get_bc
end submodule boundary_types__wall_func_adiabatic

