module dyad_ratio_mod
  use dyad_mod, only: dyad_t
  implicit none

  private
  public :: division_t

  type, extends(dyad_t) :: division_t
  contains
    procedure :: func => divide
    procedure :: deriv_wrt_arg1
    procedure :: deriv_wrt_arg2
  end type division_t

contains
  elemental real(dp) function divide(this,arg1,arg2)
    use f90_kind, only: dp

    class(division_t), intent(in) :: this
    real(dp), intent(in) :: arg1, arg2

    divide = arg1 / arg2
  end function divide

  elemental real(dp) function deriv_wrt_arg1(this,arg1,arg2)
    use f90_kind, only: dp

    class(division_t), intent(in) :: this
    real(dp), intent(in) :: arg1, arg2

    deriv_wrt_arg1 = 1.0_dp / arg2
  end function deriv_wrt_arg1

  elemental real(dp) function deriv_wrt_arg2(this,arg1,arg2)
    use f90_kind, only: dp

    class(division_t), intent(in) :: this
    real(dp), intent(in) :: arg1, arg2

    deriv_wrt_arg2 = - arg1 / arg2**2
  end function deriv_wrt_arg2
end module dyad_ratio_mod
