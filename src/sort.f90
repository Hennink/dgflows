module sort

contains

  subroutine sort_2d_array(array1,array2,length)

    !-------------------------------------------------------------------------------
    ! This routine sorts 2 arrays (of the same length). The easiest interpretation
    ! is that array1 has the row index and array2 the column index. After sorting
    ! the elements are sorted row by row.
    !-------------------------------------------------------------------------------

    use f90_kind
    implicit none

    integer :: length
    integer, dimension(*) :: array1
    integer, dimension(*) :: array2

    integer :: i
    integer :: start_index
    integer :: end_index
    real(dp) :: point1
    real(dp) :: point2
    real(dp) :: point3

    call cpu_time(point1)

    !-------------------------------------------------------------------------------
    ! Sort on index 1 using heapsort routine
    !-------------------------------------------------------------------------------

    call hpsort2(length,array1,array2)

    call cpu_time(point2)

    !-------------------------------------------------------------------------------
    ! Sort on index 2 but only within constant i
    !-------------------------------------------------------------------------------

    start_index=1
    do i=2,length
      if (array1(i)/=array1(i-1)) then
        end_index = i-1
        call hpsort(array2(start_index:end_index))
        start_index = i
      endif
      if (i==length) then
        end_index = length
        call hpsort(array2(start_index:end_index))
      endif
    enddo

    call cpu_time(point3)

  end subroutine sort_2d_array

  !-------------------------------------------------------------------------------

  subroutine hpsort(ra)
    ! Sorts 'RA' in increasing order.
    implicit none

    integer, intent(inout) :: RA(:)

    integer :: N, L
    integer :: RRA
    integer :: IR
    integer :: I, J

    N = size(RA)
    if (n<2) return
    L=N/2+1
    IR=N
    DO
      IF (L > 1) THEN
        L=L-1
        RRA=RA(L)
      ELSE
        RRA=RA(IR)
        RA(IR)=RA(1)
        IR=IR-1
        IF (IR == 1) THEN
          RA(1)=RRA
          RETURN
        ENDIF
      ENDIF
      I=L
      J=L+L
      DO
        IF (.not.(J <= IR)) EXIT
        IF (J < IR) THEN
          IF (RA(J) < RA(J+1)) J=J+1
        ENDIF
        IF (RRA < RA(J)) THEN
          RA(I)=RA(J)
          I=J
          J=J+J
        ELSE
          J=IR+1
        ENDIF
      ENDDO
      RA(I)=RRA
    ENDDO

  end subroutine hpsort

  !-------------------------------------------------------------------------------

  subroutine hpsort2(n,ra,rb)

    !-------------------------------------------------------------------------------
    ! This routine sorts an integer array ra of length n in increasing order and
    ! permutes the integer array rb at the same time.
    !-------------------------------------------------------------------------------

    implicit none

    integer :: n
    integer, dimension(*) :: ra
    integer, dimension(*) :: rb

    integer :: L
    integer :: RRA
    integer :: RRB
    integer :: IR
    integer :: I
    integer :: J

    if (n<2) return
    L=N/2+1
    IR=N
    DO
      IF (L > 1) THEN
        L=L-1
        RRA=RA(L)
        RRB=RB(L)
      ELSE
        RRA=RA(IR)
        RRB=RB(IR)
        RA(IR)=RA(1)
        RB(IR)=RB(1)
        IR=IR-1
        IF (IR == 1) THEN
          RA(1)=RRA
          RB(1)=RRB
          RETURN
        ENDIF
      ENDIF
      I=L
      J=L+L
      DO
        IF (.not.(J <= IR)) EXIT
        IF (J < IR) THEN
          IF (RA(J) < RA(J+1)) J=J+1
        ENDIF
        IF (RRA < RA(J)) THEN
          RA(I)=RA(J)
          RB(I)=RB(J)
          I=J
          J=J+J
        ELSE
          J=IR+1
        ENDIF
      ENDDO
      RA(I)=RRA
      RB(I)=RRB
    ENDDO

  end subroutine hpsort2

  !-------------------------------------------------------------------------------

  subroutine hpsort2_real(n,ra,rb)

    !-------------------------------------------------------------------------------
    ! This routine sorts an integer array ra of length n in increasing order and
    ! permutes the real array rb at the same time.
    !-------------------------------------------------------------------------------

    use f90_kind
    implicit none

    integer :: n
    integer, dimension(*) :: ra
    real(dp), dimension(*) :: rb

    integer :: L
    integer :: RRA
    integer :: IR
    integer :: I
    integer :: J
    real(dp) :: rrb

    if (n<2) return
    L=N/2+1
    IR=N
    DO
      IF (L > 1) THEN
        L=L-1
        RRA=RA(L)
        RRB=RB(L)
      ELSE
        RRA=RA(IR)
        RRB=RB(IR)
        RA(IR)=RA(1)
        RB(IR)=RB(1)
        IR=IR-1
        IF (IR == 1) THEN
          RA(1)=RRA
          RB(1)=RRB
          RETURN
        ENDIF
      ENDIF
      I=L
      J=L+L
      DO
        IF (.not.(J <= IR)) EXIT
        IF (J < IR) THEN
          IF (RA(J) < RA(J+1)) J=J+1
        ENDIF
        IF (RRA < RA(J)) THEN
          RA(I)=RA(J)
          RB(I)=RB(J)
          I=J
          J=J+J
        ELSE
          J=IR+1
        ENDIF
      ENDDO
      RA(I)=RRA
      RB(I)=RRB
    ENDDO

  end subroutine hpsort2_real


end module sort
