module functions_lib
  use exceptions, only: error
  use f90_kind, only: dp
  use mesh_objects, only: elem_list
  use pnt_set_mod, only: pnt_set_t
  use scal_func_mod, only: scal_func_type
  use scalfunc_gcors_mod, only: scalfunc_gcors_t
  use vec_func_global_coord_mod, only: vec_func_global_coord_type
  implicit none
  
  private
  public :: const_func
  public :: zero_vecfunc
  public :: zero_scalfunc
  public :: string2func
  public :: mansol_scalfunc_t
  
  interface string2func
    module procedure :: string2scal_func
    module procedure :: string2vec_func
  end interface string2func
  
  interface const_func
    module procedure :: const_scalfunc
    module procedure :: const_vecfunc
  end interface const_func
  
  type, extends(scal_func_type) :: const_scalfunc_t
    private
    real(dp) :: const
  contains
    procedure, non_overridable :: get_values => get_const_scal, &
                                  get_gders => get_zero_gders
  end type const_scalfunc_t

  type, extends(vec_func_global_coord_type) :: const_vecfunc_t
    private
    real(dp), allocatable :: const_vec(:)
  contains
    procedure, non_overridable :: value_at_gcor => const_vec, &
                                  no_dirs => no_dirs_const_vec
  end type const_vecfunc_t
  
  type, extends(scalfunc_gcors_t) :: mansol_scalfunc_t
    character(:), allocatable :: name
  contains
    procedure, non_overridable :: value_at_gcor => mansol_value
  end type

contains
  pure type(const_scalfunc_t) function zero_scalfunc()
    zero_scalfunc%const = 0.0_dp
  end function zero_scalfunc
  
  pure type(const_vecfunc_t) function zero_vecfunc(no_dirs)
    integer, intent(in) :: no_dirs
    
    allocate(zero_vecfunc%const_vec(no_dirs),source=0.0_dp)
  end function zero_vecfunc
  
  pure type(const_scalfunc_t) function const_scalfunc(const)
    real(dp), intent(in) :: const
    const_scalfunc%const = const
  end function const_scalfunc
  
  pure type(const_vecfunc_t) function const_vecfunc(const_vec)
    real(dp), dimension(:), intent(in) :: const_vec
    
    allocate(const_vecfunc%const_vec,source=const_vec)
  end function const_vecfunc
  
  subroutine string2scal_func(string,scal_func)
    use assertions, only: assert
    use exceptions, only: error
    use scal_func_masked_mod, only: scal_func_masked_t
    use string_manipulations, only: char2int, separate_first_word
    
    character(*), intent(in) :: string
    class(scal_func_type), allocatable, intent(out) :: scal_func
    
    type(scal_func_masked_t) :: masked_scal_func
    character(:), allocatable :: mat_id_string, label, description, mat_id_char
    
    if (index(string,':') /= 0 ) then
      ! The function is masked
      call separate_first_word(string,mat_id_string,description,':')
      call separate_first_word(mat_id_string,label,mat_id_char)
      call assert(label=='MaskMatID', 'Unknown input label for a masked function:'//label)
      masked_scal_func%mat_id = char2int(mat_id_char)
      call description2scal_func(description,masked_scal_func%scal_func)
      allocate(scal_func,source=masked_scal_func)
    else
      ! The function is global
      call description2scal_func(string,scal_func)
    endif
  end subroutine string2scal_func
  
  subroutine description2scal_func(description,scal_func)
    use assertions, only: assert
    use exceptions, only: error
    use fortran_parser_mod, only: parse_str2func
    use string_manipulations, only: separate_first_word
    use scalfunc_from_datafile_mod, only: datafile_func_t
    
    character(*), intent(in) :: description
    class(scal_func_type), allocatable, intent(out) :: scal_func
    
    character(:), allocatable :: label, specifications
    real(dp) :: offset
    type(mansol_scalfunc_t), allocatable :: mansol_scalfunc
    
    call separate_first_word(description,label,specifications)
    
    select case(label)
      case('ExtParser')
        allocate(scal_func,source=parse_str2func(specifications))

      case('zero')
        allocate(scal_func,source=zero_scalfunc())
        
      case('constant')
        read(specifications,*) offset
        allocate(scal_func,source=const_scalfunc(offset))
      
      case('mansol')
        allocate(mansol_scalfunc)
        mansol_scalfunc%name = specifications
        call move_alloc(mansol_scalfunc,scal_func)

      case('from_datafile')
        scal_func = datafile_func_t()

      case default
        call error('unknown function label in description=' // description)
    end select
  end subroutine description2scal_func
  
  subroutine string2vec_func(string,vec_func)
    use vec_func_mod, only: vec_func_type
    use vec_func_scal_compon_mod, only: aggregate_vecfunc_t
    use string_manipulations, only: split
    use string_mod, only: string_t
    
    character(*), intent(in) :: string
    class(vec_func_type), allocatable, intent(out) :: vec_func
    
    type(string_t), allocatable, dimension(:) :: strings_for_dirs
    integer :: dir, no_dirs
    type(aggregate_vecfunc_t), allocatable :: aggregate_vecfunc
    class(scal_func_type), allocatable :: func
    
    strings_for_dirs = split(string,',')
    no_dirs = size(strings_for_dirs)
    
    allocate(aggregate_vecfunc)
    do dir = 1, no_dirs
      call string2scal_func(strings_for_dirs(dir)%get(),func)
      call aggregate_vecfunc%append_dir(func)
    enddo
    call move_alloc(aggregate_vecfunc,vec_func)
  end subroutine string2vec_func
  
  
  
  subroutine get_const_scal(this,elem,pnt_set,time,values)
    class(const_scalfunc_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    values = this%const
  end subroutine get_const_scal
  
  subroutine get_zero_gders(this,elem,pnt_set,time,gders)
    class(const_scalfunc_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:)

    gders = 0.0_dp
  end subroutine get_zero_gders
  
  
  function const_vec(this,gcor,dimen) result(value)
    class(const_vecfunc_t), intent(in) :: this
    real(dp), intent(in) :: gcor(:)
    integer, intent(in) :: dimen
    real(dp) :: value(dimen)
    
    value = this%const_vec
  end function const_vec
  
  pure integer function no_dirs_const_vec(this) result(no_dirs)
    class(const_vecfunc_t), intent(in) :: this
    
    no_dirs = size(this%const_vec)
  end function no_dirs_const_vec
  
  subroutine mansol_value(this,gcor,time,value)
    use phys_time_mod, only: phys_time

    class(mansol_scalfunc_t), intent(in) :: this
    real(dp), intent(in) :: gcor(:)
    integer, intent(in) :: time
    real(dp), intent(out) :: value

    value = hardcoded_solution(this%name,gcor,phys_time(time))
  end subroutine

! ========= THIS SECTION WAS AUTOMATICALLY GENERATED BY A PYTHON SCRIPT, =========
! ========= AND WAS NOT INTENDED TO BE CHANGED MANUALLY                  =========
pure real(dp) function hardcoded_solution(name,coord,t) result(value)
character(*), intent(in) :: name
real(dp), intent(in) :: coord(:)
real(dp), intent(in) :: t
real(dp) :: x, y
real(dp), parameter :: pi = 3.141592653589793238462643383279502884_dp
real(dp), parameter :: L = 1.0_dp
real(dp), parameter :: N = 1.0_dp
real(dp), parameter :: rho0 = 1.0_dp
real(dp), parameter :: mu = 0.01_dp
real(dp), parameter :: cp = 1.0_dp
real(dp), parameter :: T0 = 0.0_dp
real(dp), parameter :: Pr = 1.0_dp
x = coord(0+1)
y = coord(1+1)
select case(name)
case default; error stop 'cannot evaluate mansol function: unknown name'
case('p'); value = &
    -rho0*(cos(2*pi*N*x/L) + cos(2*pi*N*y/L))*exp(-4*pi**2*N**2*mu*t/(L**2*rho0))/4
case('h'); value = &
    -T0*cp + cp*exp(-2*pi**2*N**2*mu*t/(L**2*Pr*rho0))*cos(pi*N*x/L)*cos(pi*N*y/L)
case('T'); value = &
    exp(-2*pi**2*N**2*mu*t/(L**2*Pr*rho0))*cos(pi*N*x/L)*cos(pi*N*y/L)
case('rho'); value = &
    rho0
case('ddt_rho'); value = &
    0
case('mu'); value = &
    mu
case('nu'); value = &
    mu/rho0
case('k'); value = &
    cp*mu/Pr
case('k_over_cp'); value = &
    mu/Pr
case('alpha'); value = &
    mu/(Pr*rho0)
case('deriv_rho_h'); value = &
    0
case('vol_enthalpy_src'); value = &
    0
case('u_0'); value = &
    -exp(-2*pi**2*N**2*mu*t/(L**2*rho0))*sin(pi*N*y/L)*cos(pi*N*x/L)
case('u_1'); value = &
    exp(-2*pi**2*N**2*mu*t/(L**2*rho0))*sin(pi*N*x/L)*cos(pi*N*y/L)
case('vol_force_0'); value = &
    0
case('vol_force_1'); value = &
    0
case('outflow_stress_0'); value = &
    (L*rho0*(cos(2*pi*N*x/L) + cos(2*pi*N*y/L))/4 +&
    pi*N*mu*exp(2*pi**2*N**2*mu*t/(L**2*rho0))*sin(pi*N*x/L)*sin(pi*N*y/L))*exp(-4*pi**2*N**2*mu*t/(L**2*rho0))/L
case('outflow_stress_1'); value = &
    pi*N*mu*exp(-2*pi**2*N**2*mu*t/(L**2*rho0))*cos(pi*N*x/L)*cos(pi*N*y/L)/L
case('outflow_fhf'); value = &
    -pi*N*cp*mu*exp(-2*pi**2*N**2*mu*t/(L**2*Pr*rho0))*sin(pi*N*x/L)*cos(pi*N*y/L)/(L*Pr)
end select
end function
  ! ================================================================================
end module functions_lib
