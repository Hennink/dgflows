module avg_over_elem_groups_mod
  ! x, y, z :     gcor in mesh
  ! r1, r2, r3:   group-identifying coordinates
  use array_opers_mod, only: add_Aik_Bjk_wk
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use eqv_elem_groups_mod, only: elem_groups_t
  use f90_kind, only: dp
  use math_const, only: SMALL
  use mesh_objects, only: elem_id, elem_list, elem_ptr
  use mesh_partitioning, only: my_first_last_elems
  use mesh_type, only: fem_mesh
  use mpi_wrappers_mod, only: allreduce_wrap
  use pnt_set_mod, only: pnt_set_t
  use run_data, only: polorder_group_avg
  implicit none

  private
  public :: elemgroup_averager_t

  type :: elemgroup_averager_t
    private
    type(elem_groups_t) :: elem_groups
    real(dp), allocatable :: min_r(:,:), max_r(:,:)
    real(dp), allocatable :: chol_massmats(:,:,:)
    character(:), allocatable :: names(:)
    procedure(vals_at_pnts_i), pointer, nopass :: get_vals

    logical :: unreduced_avg_coeff = .false.
    real(dp) :: total_avg_time = 0.0_dp, latest_reduce_time, latest_update_time
    real(dp), allocatable :: my_new_coeffs(:,:,:), avg_coeffs(:,:,:)
  contains
    private
    procedure, public :: init
    procedure :: basisfuncs_at_elem
    procedure, public :: update_avg
    procedure :: reduce_coeffs
    procedure, public :: write_plottable
    procedure :: get_plottable
  end type elemgroup_averager_t

  logical, parameter :: ONLY_FLUID = .TRUE.

  interface
    subroutine vals_at_pnts_i(elem,pnt_set,time,vals)
      use f90_kind, only: dp
      use mesh_objects, only: elem_list
      use pnt_set_mod, only: pnt_set_t

      type(elem_list), intent(in) :: elem
      type(pnt_set_t), intent(in) :: pnt_set
      integer, intent(in) :: time
      real(dp), intent(out) :: vals(:,:)
    end subroutine vals_at_pnts_i
  end interface

contains
  subroutine init(this,mesh,elem_groups,time,get_vals,names)
    use lapack_interface_mod, only: cholesky_decomposition, LOWER

    class(elemgroup_averager_t), intent(out) :: this
    type(fem_mesh), intent(in) :: mesh
    type(elem_groups_t), intent(in) :: elem_groups
    real(dp), intent(in) :: time
    procedure(vals_at_pnts_i) :: get_vals
    character(*), intent(in) :: names(:)

    integer :: group, ngroups
    integer :: elem_no, first_elem, last_elem
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    type(pnt_set_t) :: quad_points
    real(dp), allocatable :: quot(:)
    real(dp), allocatable :: my_massmats(:,:,:)
    real(dp), allocatable :: funcs_at_qps(:,:)
    integer :: dimen_r, nf
    
    this%elem_groups = elem_groups
    this%get_vals => get_vals
    this%names = names
    ngroups = elem_groups%ngroups()
    call minmax_r_in_groups(mesh,elem_groups,min_r=this%min_r,max_r=this%max_r)
    
    dimen_r = elem_groups%size_id_vec()
    nf = nfuncs(dimen_r)
    
    ! Set to some arbitrary values, so that multiplying this by zero is not undefined:
    allocate(this%my_new_coeffs(nf,size(names),ngroups),source=0.0_dp)
    allocate(this%avg_coeffs(nf,size(names),ngroups),source=0.0_dp)
    this%latest_update_time = time
    this%latest_reduce_time = time

    ! Store the Cholesky decomposition of the Gramm matrix (mass matrix) of the basis functions for each group:
    allocate(my_massmats(nf,nf,ngroups), source=0.0_dp)
    call my_first_last_elems(ONLY_FLUID,first_elem,last_elem)
    call mesh%get_active_elem_list_ptr(ONLY_FLUID,active_elem_list)
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      
      call get_quadset(elem, func_order=0, quad_order=2*polorder_group_avg, &
                               quad_points=quad_points, quot=quot)
      call assert(size(quot) == quad_points%np())
      call this%basisfuncs_at_elem(elem,quad_points,funcs_at_qps)
      group = elem_groups%group_of_elem(elem)
      call add_Aik_Bjk_wk(my_massmats(:,:,group), funcs_at_qps, funcs_at_qps, quot, 'loop')
    enddo
    allocate(this%chol_massmats,mold=my_massmats)
    call allreduce_wrap(my_massmats,'sum',this%chol_massmats)
    do group = 1, ngroups
      call cholesky_decomposition(this%chol_massmats(:,:,group),LOWER)
    enddo
  end subroutine init
  
  subroutine write_plottable(this)
    use, intrinsic :: ieee_arithmetic, only: ieee_value, IEEE_QUIET_NAN
    use petsc_mod, only: i_am_master_process
    use io_basics, only: nreals_fmt

    class(elemgroup_averager_t), intent(inout) :: this

    character(*), parameter :: name = 'group_plottables'
    integer :: dimen_r
    integer :: unit
    integer :: iostat
    character(200) :: iomsg
    real(dp), allocatable :: r(:,:,:), vals(:,:,:)
    integer :: group, ngroups, p, np, i, nvals
    
    dimen_r = this%elem_groups%size_id_vec()
    call assert(dimen_r == 1,'get_plottable: not sure how to plot 2D stuff')

    call this%reduce_coeffs()
    
    if (i_am_master_process()) then
      write(*,*) "Writing group averages to '" // name // "' ..."
      open(file=name, newunit=unit, &
           status='replace', action='write', pad='no', form='formatted', &
           iostat=iostat, iomsg=iomsg)
      call assert(iostat==0,iomsg)

      call this%get_plottable(r,vals)
      np = assert_eq(size(r,2),size(vals,2))
      ngroups = assert_eq(size(r,3),size(vals,3),this%elem_groups%ngroups())
      
      ! Header with all the names:
      write(unit,'(a,i0)',advance='no',iostat=iostat,iomsg=iomsg) 'r_', 1
      call assert(iostat==0,iomsg)
      do i = 1, size(this%names)
        write(unit,'(a)',advance='no',iostat=iostat,iomsg=iomsg) ',' // trim(this%names(i))
        call assert(iostat==0,iomsg)
      enddo
      write(unit,'(a)') ''
      write(unit,nreals_fmt(1),iostat=iostat,iomsg=iomsg) this%total_avg_time
      call assert(iostat==0,iomsg)

      ! Coordinates and values for all points in all groups:
      nvals = size(this%names)
      do group = 1, ngroups
        do p = 1, np
          write(unit,nreals_fmt(1),advance='no',iostat=iostat,iomsg=iomsg) r(1,p,group)
          call assert(iostat==0,iomsg)
          write(unit,nreals_fmt(nvals),iostat=iostat,iomsg=iomsg) vals(:,p,group)
          call assert(iostat==0,iomsg)
        enddo
        if (group < ngroups) then
          write(unit,nreals_fmt(1),advance='no',iostat=iostat,iomsg=iomsg) r(1,np,group) + SMALL
          call assert(iostat==0,iomsg)
          write(unit,nreals_fmt(nvals),iostat=iostat,iomsg=iomsg) [(ieee_value(1.0_dp,IEEE_QUIET_NAN), i = 1, nvals)]
          call assert(iostat==0,iomsg)
        endif
      enddo

      close(unit=unit,iostat=iostat,iomsg=iomsg)
      call assert(iostat==0,iomsg)
    endif
  end subroutine write_plottable
  
  subroutine get_plottable(this,r,vals)
    use support, only: linspace

    class(elemgroup_averager_t), intent(inout) :: this
    real(dp), allocatable, intent(out) :: r(:,:,:), vals(:,:,:)
    
    integer :: p, np_per_group
    integer :: dimen_r
    integer :: g, ngroups
    real(dp), allocatable :: funcs(:,:)
    
    dimen_r = this%elem_groups%size_id_vec()
    call assert(dimen_r == 1,'get_plottable: only implemented for 1d r')

    call this%reduce_coeffs()

    np_per_group = 4 * (1 + polorder_group_avg)
    ngroups = this%elem_groups%ngroups()
    allocate(r(dimen_r,np_per_group,ngroups))
    allocate(vals(size(this%names),np_per_group,ngroups))
    do g = 1, ngroups
      r(1,:,g) = linspace(this%min_r(1,g)+SMALL, this%max_r(1,g)-SMALL, np_per_group)
      call basisfuncs_at_selected_gcors(this%min_r(:,g),this%max_r(:,g),r(:,:,g),funcs)
      do p = 1, np_per_group
        vals(:,p,g) = matmul(funcs(:,p),this%avg_coeffs(:,:,g))
      enddo
    enddo
  end subroutine get_plottable

  subroutine update_avg(this,mesh)
    ! Updates the averages with values from the last completed time step.
    use phys_time_mod, only: phys_time

    class(elemgroup_averager_t), intent(inout) :: this
    type(fem_mesh), intent(in) :: mesh
    
    integer, parameter :: TIME_IDX = 2
    real(dp) :: new_time
    real(dp), allocatable :: my_coeffs(:,:,:)
    real(dp) :: old_duration, new_duration
    
    new_time = phys_time(TIME_IDX)
    call assert(new_time > this%latest_update_time,'update_avg: should go forward in time')
    
    allocate(my_coeffs,mold=this%my_new_coeffs)
    call project(this,mesh,TIME_IDX,my_coeffs)
    old_duration = this%latest_update_time - this%latest_reduce_time
    new_duration = new_time                - this%latest_reduce_time
    this%my_new_coeffs =   this%my_new_coeffs * (old_duration / new_duration) &
                         + my_coeffs          * ((new_duration - old_duration) / new_duration)
    this%latest_update_time = new_time
    this%unreduced_avg_coeff = .true.
  end subroutine
  
  subroutine reduce_coeffs(this)
    class(elemgroup_averager_t), intent(inout) :: this

    real(dp) :: old_duration, new_duration
    real(dp), allocatable :: new_coeffs(:,:,:)

    if (this%unreduced_avg_coeff) then
      allocate(new_coeffs,mold=this%my_new_coeffs)
      call allreduce_wrap(this%my_new_coeffs,'sum',new_coeffs)
      
      old_duration = this%total_avg_time
      new_duration = this%total_avg_time + this%latest_update_time - this%latest_reduce_time
      this%avg_coeffs =   this%avg_coeffs * (old_duration / new_duration) &
                        + new_coeffs      * ((new_duration - old_duration) / new_duration)
      this%my_new_coeffs = 0.0_dp
      this%total_avg_time = new_duration
      this%latest_reduce_time = this%latest_update_time
      this%unreduced_avg_coeff = .false.
    endif
  end subroutine reduce_coeffs

  subroutine project(this,mesh,time_idx,my_coeffs)
    use lapack95, only: solve_given_chol => potrs ! http://www.netlib.org/lapack95/lug95/node340.html
    use qnty_handler_mod, only: max_sol_order

    class(elemgroup_averager_t), intent(in) :: this
    type(fem_mesh), intent(in) :: mesh
    integer, intent(in) :: time_idx
    real(dp), intent(out) :: my_coeffs(:,:,:)

    integer :: elem_no, first_elem, last_elem
    integer :: nvals, group, ngroups
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    type(pnt_set_t) :: quad_points
    real(dp), allocatable :: quot(:)
    integer :: func_order, quad_order
    real(dp), allocatable :: vals_at_qps(:,:), funcs_at_qps(:,:)
    integer :: dimen_r
    integer :: nqp
    integer :: info
    
    dimen_r = this%elem_groups%size_id_vec()
    call assert(size(my_coeffs,1) == nfuncs(dimen_r))
    nvals = assert_eq(size(my_coeffs,2),size(this%names))
    ngroups = assert_eq(size(my_coeffs,3),this%elem_groups%ngroups())

    call my_first_last_elems(ONLY_FLUID,first_elem,last_elem)
    call mesh%get_active_elem_list_ptr(ONLY_FLUID,active_elem_list)
    my_coeffs = 0.0_dp
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)

      func_order = max_sol_order(elem)
      quad_order = polorder_group_avg + 3 * func_order    ! Assuming we're averaging third-order moments.
      call get_quadset(elem,func_order,quad_order,quad_points,quot)
      nqp = assert_eq(size(quot),quad_points%np(),BOUNDS_CHECK)
      call this%basisfuncs_at_elem(elem,quad_points,funcs_at_qps)
      allocate(vals_at_qps(nvals,nqp))
      call this%get_vals(elem,quad_points,time_idx,vals_at_qps)
      group = this%elem_groups%group_of_elem(elem)
      call add_Aik_Bjk_wk(my_coeffs(:,:,group), funcs_at_qps, vals_at_qps, quot, 'BLAS')
      deallocate(vals_at_qps)
    enddo
    
    do group = 1, ngroups
      call solve_given_chol(this%chol_massmats(:,:,group),my_coeffs(:,:,group),'L',info)
      call assert(info == 0,'averager: project: POTRS info /= 0')
    enddo
  end subroutine project

  subroutine get_quadset(elem,func_order,quad_order,quad_points,quot)
    use local_elem, only: get_quadrature
    use local_global_mappings, only: lcors2pnt_set

    type(elem_list), intent(in) :: elem
    integer, intent(in) :: func_order, quad_order
    type(pnt_set_t), intent(out) :: quad_points
    real(dp), allocatable, intent(out) :: quot(:)

    real(dp), allocatable :: lcors(:,:), det_jac(:)

    call get_quadrature(elem%eltype,quad_order,lcors,quot)
    call lcors2pnt_set(elem,func_order,lcors,quad_points,det_jac)
    call quad_points%set_tag('group_avg_quad')
    quot = quot * abs(det_jac)
  end subroutine

  subroutine minmax_r_in_groups(mesh,elem_groups,min_r,max_r)
    type(fem_mesh), intent(in) :: mesh
    type(elem_groups_t), intent(in) :: elem_groups
    real(dp), allocatable, intent(out) :: min_r(:,:), max_r(:,:)
    
    real(dp), allocatable :: r(:), my_min_r(:,:), my_max_r(:,:)
    integer :: elem_no, first_elem, last_elem
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    integer :: group
    integer :: dimen_r
    integer :: v
    
    dimen_r = elem_groups%size_id_vec()
    allocate(r(dimen_r))
    allocate(my_min_r(dimen_r,elem_groups%ngroups()), source=huge(1.0_dp))
    allocate(my_max_r(dimen_r,elem_groups%ngroups()), source=-huge(1.0_dp))

    call my_first_last_elems(ONLY_FLUID,first_elem,last_elem)
    call mesh%get_active_elem_list_ptr(ONLY_FLUID,active_elem_list)
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      group = elem_groups%group_of_elem(elem)

      ! Determine the min/max values of 'r' by looking at the vertices and the center:
      do v = 1, size(elem%vertices)
        associate(xyz => elem%vertices(v)%point%coordinate%xyz)
          r = elem_groups%idvec_at_gcor(xyz)
          my_min_r(:,group) = min(my_min_r(:,group),r)
          my_max_r(:,group) = max(my_max_r(:,group),r)
        end associate
      enddo
      r = elem_groups%idvec_at_gcor(elem%avg_vtx())
      my_min_r(:,group) = min(my_min_r(:,group),r)
      my_max_r(:,group) = max(my_max_r(:,group),r)
    enddo

    allocate(min_r,mold=my_min_r)
    allocate(max_r,mold=my_max_r)
    call allreduce_wrap(my_min_r,'min',min_r)
    call allreduce_wrap(my_max_r,'max',max_r)
  end subroutine

  subroutine basisfuncs_at_elem(this,elem,pnt_set,funcs)
    class(elemgroup_averager_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    real(dp), allocatable, intent(out) :: funcs(:,:)

    integer :: group
    integer :: p, np, dimen_r
    real(dp), allocatable :: r(:,:)
    
    call assert(allocated(pnt_set%gcors),'I need gcors in the pnt_set to compute group basisfuncs.')

    group = this%elem_groups%group_of_elem(elem)
    dimen_r = this%elem_groups%size_id_vec()
    np = pnt_set%np()
    allocate(r(dimen_r,np))
    do p = 1, np
      r(:,p) = this%elem_groups%idvec_at_gcor(pnt_set%gcors(:,p))
    enddo
    call basisfuncs_at_selected_gcors(this%min_r(:,group),this%max_r(:,group),r,funcs)
  end subroutine

  subroutine basisfuncs_at_selected_gcors(min_r,max_r,r,funcs)
    ! Returns basis function values at 'r', given minimum and maximum values that 'r' can take.
    use orthofun, only: dgfuncs

    real(dp), intent(in) :: min_r(:), max_r(:), r(:,:)
    real(dp), allocatable, intent(out) :: funcs(:,:)

    integer :: p, np
    integer :: dimen_r
    real(dp), allocatable :: mids(:), halfwidths(:)
    real(dp), allocatable :: lcor(:)
    integer :: ierr
    
    dimen_r = assert_eq(size(min_r),size(max_r),size(r,1),BOUNDS_CHECK)
    
    np = size(r,2)
    mids       = (min_r + max_r) / 2
    halfwidths = (max_r - min_r) / 2
    allocate(lcor(dimen_r))
    allocate(funcs(nfuncs(dimen_r),np))
    do p = 1, np
      lcor = (r(:,p) - mids) / halfwidths
      call dgfuncs(eltype(dimen_r),lcor,polorder_group_avg,funcs(:,p),ierr)
      call assert(ierr==0,"basisfuncs_at_selected_gcors: ierr /= 0 in 'dgfuncs'")
    enddo
  end subroutine

  pure integer function nfuncs(dimen_r)
    use orthofun, only: get_ndg

    integer, intent(in) :: dimen_r

    integer :: ierr
    
    call get_ndg(eltype(dimen_r),polorder_group_avg,nfuncs,ierr)
    if (ierr /= 0) error stop "averager: ierr /= 0 in 'get_ndg'"
  end function nfuncs

  pure integer function eltype(dimen_r)
    integer, intent(in) :: dimen_r

    select case(dimen_r)
      case(1);        eltype = 12
      case(2);        eltype = 24
      case default;   error stop 'avg_over_elem_groups_mod: cannot find appropriate eltype'
    end select
  end function eltype
end module avg_over_elem_groups_mod
