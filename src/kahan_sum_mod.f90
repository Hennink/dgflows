module kahan_sum_mod
  ! Module for computing a sum with the Kahan algorithm, which has a much
  ! smaller rounding error than a naive summation.
  ! 
  ! https://en.wikipedia.org/wiki/Kahan_summation_algorithm
  ! 
  use, intrinsic :: iso_fortran_env, only: real64, real128
  use f90_kind, only: dp
  implicit none

  private
  public :: kahan_sum_t

  integer, parameter :: ip = merge(real128, real64, real128 > 0) ! internal precision. Default to highest available.

  type kahan_sum_t
    private
    real(ip) :: ksum = 0, c = 0
  contains
    procedure, non_overridable :: add, total
  end type

contains
  subroutine add(this,x)
    class(kahan_sum_t), intent(inout) :: this
    real(dp), intent(in) :: x

    real(ip) :: y, t

    y = real(x,ip) - this%c
    t = this%ksum + y
    this%c = (t - this%ksum) - y
    this%ksum = t
  end subroutine

  real(dp) function total(this)
    class(kahan_sum_t), intent(in) :: this

    total = real(this%ksum,dp)
  end function
end module
