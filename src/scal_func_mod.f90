module scal_func_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use exceptions, only: error
  use f90_kind, only: dp
  use mesh_objects, only: elem_list, face_list
  use pnt_set_mod, only: pnt_set_t
  use run_data, only: mesh_dimen
  implicit none
  
  private
  public :: scal_func_type
  
  type, abstract :: scal_func_type
  contains
    procedure :: get_values, get_gders
    
    generic :: values_at_qps => values_at_elem_qps, values_at_qps__face
    procedure, non_overridable :: values_at_elem_qps
    procedure :: values_at_qps__face ! overwritable for fields that are defined only at the boundary
    procedure, non_overridable :: values_at_qps_E1_E2
    
    generic :: gders_at_qps => gders_at_qps__elem, gders_at_face_qps
    procedure, non_overridable :: gders_at_qps__elem, gders_at_face_qps
    
    procedure, non_overridable :: avg_at_qps, jump_at_qps
    procedure, non_overridable :: rel_grad, rel_grad_at_qps
    generic, public :: eval_avg_value => eval_avg_value_on_elem, &
                                         eval_avg_value_at_face_side, &
                                         eval_avg_value_at_both_face_sides
    procedure, non_overridable, private :: eval_avg_value_on_elem, &
                                           eval_avg_value_at_face_side, &
                                           eval_avg_value_at_both_face_sides
  end type scal_func_type

contains
  subroutine get_values(this,elem,pnt_set,time,values)
    class(scal_func_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)

    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)

    call error('NOT IMPLEMENTED: get_values')
  end subroutine get_values
  
  subroutine get_gders(this,elem,pnt_set,time,gders)
    class(scal_func_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:)
    
    call assert(size(gders,1) == mesh_dimen,BOUNDS_CHECK)
    call assert(size(gders,2) == pnt_set%np(),BOUNDS_CHECK)

    call error('NOT IMPLEMENTED: get_gders')
  end subroutine get_gders
  
  
  function values_at_elem_qps(this,elem,time) result(values)
    class(scal_func_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: time
    real(dp) :: values(elem%nqp)
    
    call this%get_values(elem,elem%quad_points,time,values)
  end function
  
  function gders_at_qps__elem(this,elem,time) result(gders)
    class(scal_func_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: time
    real(dp) :: gders(elem%dimen(),elem%nqp)
    
    call this%get_gders(elem,elem%quad_points,time,gders)
  end function gders_at_qps__elem
  
  function values_at_qps__face(this,face,side,time) result(values)
    class(scal_func_type), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side
    integer, intent(in) :: time
    real(dp) :: values(face%nqp)

    call assert(side == 1 .or. side == 2,BOUNDS_CHECK)
    
    call this%get_values(                     &
      elem = face%elem_neighbors(side)%point, &
      pnt_set = face%quad_points(side),       &
      time = time,                            &
      values = values                         &
    )
  end function values_at_qps__face
  
  function gders_at_face_qps(this,face,side,time) result(gders)
    class(scal_func_type), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side
    integer, intent(in) :: time
    real(dp) :: gders(mesh_dimen,face%nqp)
    
    call assert(side == 1 .or. side == 2,BOUNDS_CHECK)

    call this%get_gders(                      &
      elem = face%elem_neighbors(side)%point, &
      pnt_set = face%quad_points(side),       &
      time = time,                            &
      gders = gders                           &
    )
  end function
  
  subroutine values_at_qps_E1_E2(this,face,only_fluid,time,values_E1,values_E2)
    class(scal_func_type), intent(in) :: this
    type(face_list), intent(in) :: face
    logical, intent(in) :: only_fluid
    integer, intent(in) :: time
    real(dp), intent(out) :: values_E1(:), values_E2(:)
    
    type(elem_list), pointer :: E1, E2

    call assert(.not. face%is_boundary(only_fluid),BOUNDS_CHECK)
    
    E1 => face%elem_neighbors(1)%point
    E2 => face%elem_neighbors(2)%point
    call this%get_values(E1,face%quad_points(1),time,values_E1)
    call this%get_values(E2,face%quad_points(2),time,values_E2)
  end subroutine values_at_qps_E1_E2
  
  subroutine avg_at_qps(this,face,only_fluid,time,avg)
    ! Returns the average of a function at face%qps.
    ! The average operator is defined as:
    !         / 0.5 * (f|_E1 + f|_E2), on internal faces,
    ! {f} := {
    !         \ f|_E1              , on boundary faces.
    use mesh_objects, only: side_of_internal_nghb_of_bnd_face
    
    class(scal_func_type), intent(in) :: this
    type(face_list), intent(in) :: face
    logical, intent(in) :: only_fluid
    integer, intent(in) :: time
    real(dp), intent(out) :: avg(:)
    
    real(dp) :: values_E1(face%nqp), values_E2(face%nqp)
    integer :: side
    
    call assert(size(avg) == face%nqp,BOUNDS_CHECK)
    
    if (.not. face%is_boundary(only_fluid)) then
      call this%values_at_qps_E1_E2(face,only_fluid,time,values_E1,values_E2)
      avg = 0.5_dp * (values_E1 + values_E2)
    else
      side = side_of_internal_nghb_of_bnd_face(face,only_fluid)
      avg = this%values_at_qps(face,side,time)
    endif
  end subroutine avg_at_qps
  
  subroutine jump_at_qps(this,face,only_fluid,time,jump)
    ! Returns the jump of a function at face%qps. 
    !The jump operator is defined as:
    !         / f|_E1 - f|_E2, on internal faces
    ! [f] := {
    !         \ f|_E1      , on boundary faces
    use mesh_objects, only: side_of_internal_nghb_of_bnd_face
    
    class(scal_func_type), intent(in) :: this
    type(face_list), intent(in) :: face
    logical, intent(in) :: only_fluid
    integer, intent(in) :: time
    real(dp), intent(out) :: jump(:)
    
    real(dp) :: values_E1(face%nqp), values_E2(face%nqp)
    integer :: side
    
    call assert(size(jump) == face%nqp,BOUNDS_CHECK)
    
    if (.not. face%is_boundary(only_fluid)) then
      call this%values_at_qps_E1_E2(face,only_fluid,time,values_E1,values_E2)
      jump = values_E1 - values_E2
    else
      side = side_of_internal_nghb_of_bnd_face(face,only_fluid)
      jump = this%values_at_qps(face,side,time)
    endif
  end subroutine jump_at_qps
  
  
  subroutine rel_grad(this,elem,pnt_set,time,rel_gders)
    ! Returns (1/this)*grad(this) for scalar function 'this' at 'pnt_set'
    class(scal_func_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: rel_gders(:,:)
    
    real(dp) :: values(size(rel_gders,2))
    integer :: p, np
    
    call assert(size(rel_gders,1) == elem%dimen(),BOUNDS_CHECK)
    np = assert_eq(size(rel_gders,2),pnt_set%np(),BOUNDS_CHECK)

    call this%get_values(elem,pnt_set,time,values)
    call this%get_gders(elem,pnt_set,time,rel_gders)
    do p = 1, np
      rel_gders(:,p) = rel_gders(:,p) / values(p)
    enddo
  end subroutine rel_grad
  
  function rel_grad_at_qps(this,elem,time) result(rel_gders)
    ! Returns (1/this)*grad(this) for scalar function 'this' at quad. points of 'elem'.
    class(scal_func_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: time
    real(dp) :: rel_gders(elem%dimen(),elem%nqp)
    
    call this%rel_grad(elem,elem%quad_points,time,rel_gders)
  end function rel_grad_at_qps
  
  
  subroutine eval_avg_value_on_elem(this,elem,time,avg_value)
    class(scal_func_type), intent(in) :: this
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: time
    real(dp), intent(out) :: avg_value

    avg_value = dot_product(elem%quot,this%values_at_elem_qps(elem,time)) / elem%volume
  end subroutine eval_avg_value_on_elem
  
  subroutine eval_avg_value_at_face_side(this,face,side,time,avg_value)
    class(scal_func_type), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side
    integer, intent(in) :: time
    real(dp), intent(out) :: avg_value
    
    avg_value = dot_product(face%quot,this%values_at_qps(face,side,time)) / face%area
  end subroutine eval_avg_value_at_face_side
  
  subroutine eval_avg_value_at_both_face_sides(this,face,time,avg_value_E1,avg_value_E2)
    class(scal_func_type), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: time
    real(dp), intent(out) :: avg_value_E1, avg_value_E2
    
    call this%eval_avg_value_at_face_side(face,side=1,time=time,avg_value=avg_value_E1)
    call this%eval_avg_value_at_face_side(face,side=2,time=time,avg_value=avg_value_E2)
  end subroutine eval_avg_value_at_both_face_sides
end module scal_func_mod

