module numerical_solutions
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use curl_mod, only: curl_t, curl
  use dof_handler, only: dof_handler_type
  use dyad_of_vec_scal_funcs_mod, only: dyad_of_vec_scal_funcs_t
  use f90_kind, only: dp
  use func_operations_mod, only: operator(*), operator(/)
  use func_set_mod, only: func_set_t
  use mesh_objects, only: elem_list
  use mesh_type, only: fem_mesh
  use petsc_mod, only: I_am_master_process
  use quantities_mod, only: name2sym
  use qnty_handler_mod, only: quantities, findloc_qnty
  use scal_func_mod, only: scal_func_type
  use solvec_as_vecfunc_mod, only: solvec_as_vecfunc, num_vec_func_t
  use solvec_mod, only: solvec_t
  use string_manipulations, only: char2int, int2char, char2bool, separate_first_word, split
  use string_mod, only: string_t, as_chars
  use vec_func_mod, only: vec_func_type
  implicit none
  
  private
  public :: print_max_ddt
  public :: get_solvec
  public :: get_dof_handler
  public :: initialize
  public :: new2old_all_solvecs
  public :: io_solvec_for_qnty, io_all_coeffs, read_qnties_from_single_file
  public :: all_qnty_names
  public :: num_scalfunc_set, matprop_set, mass_flux, velocity
  public :: progress_time_and_all_coeffs, update_old_coeff
  public :: predict_new_coeff
  public :: postprocess, &
            write_numsols_and_matprops_to_gmsh_files, &
            print_total_ndofs
  public :: we_solve_for
  public :: solve_for_mtum_and_press
  public :: solve_for_energy
  public :: pol_order
  
  public :: pressure_needs_penalization, pressure_mtx_is_singular
  
  public :: maxorder_for, temporal_order_for_qnty, &
            symm_of_qnty_linsys, &
            implicit_convection_for_qnty, conv_num_flux_chosen_for_qnty, find_if_add_conv_corr_for_qnty, &
            tvd_limit_for_qnty
  
  public :: grad_u_invariants
  
  type(func_set_t), protected, target :: num_scalfunc_set, matprop_set
  type(num_vec_func_t), protected :: mass_flux
  type(dyad_of_vec_scal_funcs_t), protected :: velocity
  type(curl_t), protected :: vorticity
  
  logical, protected :: pressure_needs_penalization

contains
  function get_solvec(qnty_name) result(solvec_ptr)
    character(*), intent(in) :: qnty_name
    type(solvec_t), pointer :: solvec_ptr
    
    integer :: idx
    
    idx = findloc_qnty(qnty_name)
    call assert(idx > 0,'could not find solvec; qnty=' // qnty_name)
    solvec_ptr => quantities(idx)%solvec
  end function get_solvec
  
  function get_dof_handler(qnty_name) result(handler)
    character(*), intent(in) :: qnty_name
    type(dof_handler_type), pointer :: handler
    
    integer :: idx
    
    idx = findloc_qnty(qnty_name)
    call assert(idx > 0,'could not find handler; qnty=' // qnty_name)
    handler => quantities(idx)%solvec%handler
  end function get_dof_handler
  
  elemental logical function we_solve_for(qnty_name)
    use run_data, only: quantities_frozen_at_ic
    
    character(*), intent(in) :: qnty_name
    
    we_solve_for = findloc_qnty(qnty_name) /= 0 .and. findloc(quantities_frozen_at_ic,qnty_name,dim=1) == 0
  end function we_solve_for
  
  subroutine print_max_ddt(mesh)
    use run_data, only: dt
    use phys_time_mod, only: BDF_weights
    
    type(fem_mesh), intent(in) :: mesh
    
    integer :: i, order
    real(dp), allocatable :: max_ddt_qnty(:)
    
    do i = 1, size(quantities)
      associate(solvec => quantities(i)%solvec)
        if (we_solve_for(solvec%name) .and. solvec%ncoeffs() > 1) then
          order = temporal_order_for_qnty(solvec%name)
          allocate(max_ddt_qnty,source=solvec%max_weighted_sum(mesh,BDF_weights(order,dt)))
          if (I_am_master_process()) write(*,"('max d/dt ',A,' = ',3es13.6)") name2sym(solvec%name), max_ddt_qnty
          deallocate(max_ddt_qnty)
        endif
      end associate
    enddo
  end subroutine print_max_ddt
  
  subroutine progress_time_and_all_coeffs()
    use phys_time_mod, only: add_step_with_dt
    use run_data, only: dt

    integer :: i
    
    do i = 1, size(quantities)
      call quantities(i)%solvec%new_to_old_coeff()
    enddo
    call add_step_with_dt(dt)
  end subroutine progress_time_and_all_coeffs
  
  subroutine update_old_coeff(qnty_name)
    character(*), intent(in) :: qnty_name
    
    type(solvec_t), pointer :: solvec_ptr
    
    solvec_ptr => get_solvec(qnty_name)
    call solvec_ptr%new_to_old_coeff()
  end subroutine update_old_coeff
  
  subroutine predict_new_coeff(qnty_name)
    character(*), intent(in) :: qnty_name
    
    type(solvec_t), pointer :: solvec_ptr
    
    solvec_ptr => get_solvec(qnty_name)
    call solvec_ptr%extrapolate_new_from_old_coeff()
  end subroutine predict_new_coeff
  
  
  subroutine initialize(mesh)
    use matprop_mod, only: infer_matprops_from_func_set
    use run_data, only: p_stab

    type(fem_mesh), intent(in) :: mesh
    
    type(dof_handler_type), pointer :: m_handler, p_handler

    call init_quantities(mesh)
    call init_num_scalfunc_set()

    call infer_matprops_from_func_set(num_scalfunc_set,matprop_set)
    call add_spec_qnties_to_numfuncset()
    
    if (findloc_qnty('mass_flux') /= 0) then
      mass_flux = solvec_as_vecfunc(get_solvec('mass_flux'))
      velocity = mass_flux / matprop_set%func_ptr('density')
      vorticity = curl(velocity)
    endif

    ! Determine whether we need pressure stabilization:
    if (solve_for_mtum_and_press()) then
      if (p_stab == 'auto') then
        m_handler => get_dof_handler("mass_flux")
        p_handler => get_dof_handler("pressure")
        pressure_needs_penalization = .not. assert_eq(m_handler%order > p_handler%order)
        ! adding the following msg to `assert_eq` triggers a Gfortran-10 compiler bug:
        !     "Some elements have inf-sup stable solution spaces for m and p, and some elements do not.  &
        !     &You probably need pressure stabilization on an element-by-element basis, but this is not implemented.  &
        !     &Try running with `p_stab='T'` (instead of `'auto'`), to add stabilization for all elements.")

      else
        pressure_needs_penalization = char2bool(p_stab)
      endif
      if (i_am_master_process()) write(*,'(a,L1)') 'pressure_needs_penalization = ', pressure_needs_penalization
    endif
  end subroutine
  
  subroutine init_quantities(mesh)
    use io_basics, only: open_input_file, read_line_from_input_file
    use run_data, only: file_path_unknowns_specs
    
    type(fem_mesh), intent(in) :: mesh
    
    integer :: unit_no
    character(:), allocatable :: unknowns
    type(string_t), allocatable :: qnty_list(:)
    integer :: q, no_qnty
    
    call open_input_file(file_path_unknowns_specs,unit_no)
    call read_line_from_input_file(unit_no,unknowns)
    qnty_list = split(unknowns,',')
    no_qnty = size(qnty_list)
    allocate(quantities(no_qnty))
    do q = 1, no_qnty
      call quantities(q)%init(qnty_list(q),unit_no,mesh)
    enddo
    close(unit_no)
  end subroutine init_quantities
  
  subroutine init_num_scalfunc_set()
    use solvec_as_scalfunc_mod, only: solvec_as_scalfunc
    
    integer :: i
    class(scal_func_type), allocatable :: scal_func
    
    do i = 1, size(quantities)
      associate(solvec => quantities(i)%solvec)
        if (solvec%handler%no_dirs /= 1) then
          call assert(solvec%name == 'mass_flux',"init_num_scalfunc_set: a non-m vector?")
          cycle
        endif
        call solvec_as_scalfunc(solvec,scal_func)
        call num_scalfunc_set%add_func(solvec%name,scal_func)
      end associate
    enddo
  end subroutine init_num_scalfunc_set
  
  subroutine add_spec_qnties_to_numfuncset()
    ! Add specific quantities to num_scalfunc_set, based on a density that is 
    ! either constant, or computed from the specific enthalpy.
    ! 
    ! Rather than a 'plain' scal_func_t, this 'rho_fluid_nonfluid' is a wrapper
    ! that works for all mat_id's. This is due to the implementation of the
    ! material properties, where all mat_id's have different properties, even 
    ! within the fluid.
    class(scal_func_type), allocatable :: rho_fluid_nonfluid
    class(scal_func_type), allocatable :: spec_h, vol_H, vol_turb1, vol_turb2
    
    call matprop_set%get_func('density',rho_fluid_nonfluid)
    if (num_scalfunc_set%has('spec_enthalpy')) then
      call assert(.not. num_scalfunc_set%has('vol_enthalpy'),'should not solve for both h and H')
      call num_scalfunc_set%get_func('spec_enthalpy',spec_h)
      call num_scalfunc_set%add_func('vol_enthalpy',spec_h * rho_fluid_nonfluid)
    elseif (num_scalfunc_set%has('vol_enthalpy')) then
      call num_scalfunc_set%get_func('vol_enthalpy',vol_H)
      call num_scalfunc_set%add_func('spec_enthalpy',vol_H / rho_fluid_nonfluid)
    endif
    
    if (num_scalfunc_set%has('vol_turb1')) then
      call num_scalfunc_set%get_func('vol_turb1',vol_turb1)
      call num_scalfunc_set%add_func('spec_turb1',vol_turb1 / rho_fluid_nonfluid)
    endif
    if (num_scalfunc_set%has('vol_turb2')) then
      call num_scalfunc_set%get_func('vol_turb2',vol_turb2)
      call num_scalfunc_set%add_func('spec_turb2',vol_turb2 / rho_fluid_nonfluid)
    endif
  end subroutine add_spec_qnties_to_numfuncset

  subroutine postprocess(mesh)
    ! Input format:
    ! 
    !   change_polorders_in_postprocessing = 'pressure:  3, 3, 3, 4',
    !                                        'mass_flux: 4, 4, 4, 5',
    ! 
    use qnty_handler_mod, only: change_solspace
    use run_data, only: change_polorders_in_postprocessing

    type(fem_mesh), intent(in) :: mesh
    
    integer :: i, idx
    character(len(change_polorders_in_postprocessing)) :: specs
    character(:), allocatable :: name, orders_char

    do i = 1, size(change_polorders_in_postprocessing)
      specs = change_polorders_in_postprocessing(i)
      if (specs == '') cycle

      if (i_am_master_process()) write(*,'(a)') 'changing pol. orders; specs=' // specs
      call separate_first_word(specs,name,orders_char,':')
      idx = findloc_qnty(name)
      call change_solspace(  &
          quantities(idx), mesh,  &
          new_orders_near_bnd = char2int(as_chars(split(orders_char,',')))  &
      )
    enddo
  end subroutine

  subroutine write_numsols_and_matprops_to_gmsh_files(mesh)
    ! Writes all quantities in this module to Gmsh files.
    use gmsh_interface, only: write_to_gmsh_file
    use run_data, only: constant_density, gmsh_output_includes_all_fields
    
    type(fem_mesh), intent(in) :: mesh
    
    integer, parameter :: TIME = 1
    logical :: solve_for_engy
    type(func_set_t) :: aux_func_set
    
    if (we_solve_for('mass_flux')) then
      call write_to_gmsh_file(velocity,mesh,'num-u',only_fluid=.true.,time=TIME)
      if (.not. constant_density) call write_to_gmsh_file(mass_flux,mesh,'num-m',only_fluid=.true.,time=TIME)
    endif
    ! Give the two func. sets a different prefix, to avoid name clashes with h or H.
    ! Of all fluid-solid quantities, print only the temperature and the specific enthalpy.
    if (I_am_master_process()) write(*,'(a)',advance='no') 'writing num. scalfuncs and matprops to Gmsh files ...'
    call num_scalfunc_set%write_to_gmsh_files(mesh,'num-')
    aux_func_set = matprop_set%reduced_set('spec_enthalpy')
    aux_func_set = aux_func_set%reduced_set('temperature')
    if (gmsh_output_includes_all_fields) call aux_func_set%write_to_gmsh_files(mesh,'mat-')
    call solve_for_energy(solve_for_engy)
    if (solve_for_engy) then
      call write_to_gmsh_file(matprop_set%func_ptr('temperature'),mesh,'mat-temperature',only_fluid=.false.,time=TIME)
      if (gmsh_output_includes_all_fields) then
        call write_to_gmsh_file(num_scalfunc_set%func_ptr('spec_enthalpy'), mesh, 'mat-spec_enthalpy', &
            only_fluid=.false. ,time=TIME)
      endif
    endif
    if (I_am_master_process()) write(*,'(a)') 'done'
  end subroutine write_numsols_and_matprops_to_gmsh_files
  
  subroutine print_total_ndofs()
    ! Prints # DOF's per pol. order for all elements.
    ! 
    ! Call with one process (dof_handler_type is not parallel).
    integer :: i
    type(solvec_t), pointer :: solvec
    type(dof_handler_type), pointer :: handler
    integer :: p
    integer :: iostat
    character(200) :: iomsg

    write(*,"(a10, 10i7)",iostat=iostat,iomsg=iomsg) '#DOFS,p=', [(p, p = 0, 9)]
    call assert(iostat==0,iomsg)
    do i = 1, size(quantities)
      solvec => quantities(i)%solvec
      handler => solvec%handler
      write(*,"(a10, 10(es7.1e1))",iostat=iostat,iomsg=iomsg) name2sym(solvec%name), real(handler%total_ndofs_per_polorder())
      call assert(iostat==0,iomsg)
    enddo
  end subroutine print_total_ndofs

  subroutine new2old_all_solvecs()
    integer :: i
    
    do i = 1, size(quantities)
      call quantities(i)%solvec%copy_new_to_all_old_coeff()
    enddo
  end subroutine new2old_all_solvecs
  
  pure function all_qnty_names()
    character(100) :: all_qnty_names(size(quantities))
    
    integer :: i

    do i = 1, size(quantities)
      all_qnty_names(i) = quantities(i)%solvec%name
    enddo
  end function all_qnty_names

  subroutine io_all_coeffs(file_basename,from_file)
    character(*), intent(in) :: file_basename
    logical, intent(in) :: from_file

    integer :: i

    do i = 1, size(quantities)
      call io_solvec_for_qnty(quantities(i)%solvec%name,file_basename,from_file)
    enddo
  end subroutine io_all_coeffs

  subroutine io_solvec_for_qnty(qnty_name,file_basename,from_file)
    ! Reads/writes the solvec for a quantity. Does nothing if you try to read 
    ! from a noexisting solvec file.
    character(*), intent(in) :: qnty_name
    character(*), intent(in) :: file_basename
    logical, intent(in) :: from_file
    
    character(:), allocatable :: status, action, filepath
    type(solvec_t), pointer :: solvec_ptr
    integer :: unit_number
    logical :: exists
    integer :: ios
    character(200) :: msg
    
    call assert(file_basename /= '',"I/O solvec: file_basename should not be empty")

    filepath = trim(file_basename) // '_' // name2sym(qnty_name)
    if (from_file) then 
      inquire(file=filepath,exist=exists,iostat=ios,iomsg=msg)
      call assert(ios==0,msg)
      if (.not. exists) then
        write(*,*) 'Cannot read solvec from nonexistent filepath=' // filepath
        write(*,*) 'Make sure you specify the IC some other way.'
        return
      endif
      status = 'old'
      action = 'read'
    else
      status = 'replace'
      action = 'write'
    endif
    if (I_am_master_process()) write(*,"(a,' ',a,' ...')",advance='no') action, filepath
    
    solvec_ptr => get_solvec(qnty_name)
    open(newunit=unit_number, file=filepath, &
         status=status, access='sequential', form='unformatted', action=action, position='rewind', &
         iostat=ios, iomsg=msg)
    call assert(ios==0,msg)
    call solvec_ptr%io(unit_number,from_file)
    close(unit_number,status='keep',iostat=ios,iomsg=msg)
    call assert(ios==0,msg)
    if (I_am_master_process()) write(*,'(a)') 'done'
  end subroutine io_solvec_for_qnty

  subroutine read_qnties_from_single_file(qnty_names,file_path)
    use petsc_mod, only: I_am_master_process
    
    character(*), intent(in) :: qnty_names(:)
    character(*), intent(in) :: file_path
    
    integer :: unit_number
    integer :: ios
    character(200) :: msg
    integer :: i
    type(solvec_t), pointer :: solvec_ptr
    
    open(newunit=unit_number, file=file_path, &
         status='old', access='sequential', form='unformatted', action='read', position='rewind', &
         iostat=ios, iomsg=msg)
    call assert(ios==0,msg)
    do i = 1, size(qnty_names)
      if (I_am_master_process()) write(*,'(a)') 'reading ' // qnty_names(i) // ' from ' // file_path
      solvec_ptr => get_solvec(qnty_names(i))
      call solvec_ptr%io(unit_number,from_file=.true.)
    enddo
    close(unit_number,iostat=ios,iomsg=msg)
    call assert(ios==0,msg)
  end subroutine read_qnties_from_single_file
  
  
  
  integer function maxorder_for(qnty_name) result(max_polorder)
    character(*), intent(in) :: qnty_name
    
    type(dof_handler_type), pointer :: handler
    
    handler => get_dof_handler(qnty_name)
    max_polorder = maxval(handler%order)
  end function
  
  integer function temporal_order_for_qnty(qnty_name) result(temporal_order)
    use phys_time_mod, only: nold_times
    
    character(*), intent(in) :: qnty_name
    
    integer :: idx
    
    call assert(nold_times >= 1, 'temporal_order_for_qnty: was supposed to be used when nold_times >= 1')

    idx = findloc_qnty(qnty_name)
    call assert(idx > 0,'could not find temporal order; qnty=' // qnty_name)
    temporal_order = min(quantities(idx)%temporal_order,nold_times)
  end function
  
  logical function symm_of_qnty_linsys(qnty_name) result(symm)
    character(*), intent(in) :: qnty_name
    
    integer :: idx
    
    idx = findloc_qnty(qnty_name)
    call assert(idx > 0,'could not find symm; qnty=' // qnty_name)
    symm = quantities(idx)%symm
  end function symm_of_qnty_linsys
  
  logical function implicit_convection_for_qnty(qnty_name) result(implicit_convection)
    character(*), intent(in) :: qnty_name
    
    integer :: idx
    
    idx = findloc_qnty(qnty_name)
    call assert(idx > 0,'could not find implicit convection; qnty=' // qnty_name)
    implicit_convection = quantities(idx)%implicit_convection
  end function implicit_convection_for_qnty
  
  integer function conv_num_flux_chosen_for_qnty(qnty_name) result(conv_num_flux)
    character(*), intent(in) :: qnty_name
    
    integer :: idx
    
    idx = findloc_qnty(qnty_name)
    call assert(idx > 0,'could not find conv numflux; qnty=' // qnty_name)
    conv_num_flux = quantities(idx)%conv_num_flux
  end function conv_num_flux_chosen_for_qnty
  
  logical function tvd_limit_for_qnty(qnty_name) result(tvd_limit)
    character(*), intent(in) :: qnty_name
    
    integer :: idx
    
    idx = findloc_qnty(qnty_name)
    call assert(idx > 0,'could not find tvd limiting; qnty=' // qnty_name)
    tvd_limit = quantities(idx)%tvd_limit
  end function tvd_limit_for_qnty
  
  subroutine find_if_add_conv_corr_for_qnty(qnty_name,add_vol_div_corr,add_face_jump_corr)
    character(*), intent(in) :: qnty_name
    logical, intent(out) :: add_vol_div_corr, add_face_jump_corr
    
    integer :: idx
    
    idx = findloc_qnty(qnty_name)
    call assert(idx > 0,'could not find_if_add_conv_corr_for_qnty; qnty=' // qnty_name)
    
    add_vol_div_corr = quantities(idx)%add_conv_vol_corr
    add_face_jump_corr = quantities(idx)%add_conv_face_corr
  end subroutine find_if_add_conv_corr_for_qnty
  
  integer function pol_order(elem,qnty_name)
    type(elem_list), intent(in) :: elem
    character(*), intent(in) :: qnty_name
    
    type(dof_handler_type), pointer :: handler
    integer :: elem_no
    
    handler => get_dof_handler(qnty_name)
    elem_no = elem%get_active_elem_no(handler%only_fluid_mesh)
    pol_order = handler%order(elem_no)
  end function pol_order
  
  logical function pressure_mtx_is_singular()
    use boundaries, only: absolute_pressure_is_fixed
    
    pressure_mtx_is_singular = .not. absolute_pressure_is_fixed()
  end function pressure_mtx_is_singular
  
  logical function solve_for_mtum_and_press()
    logical :: solve_for_m, solve_for_u, solve_for_p
    
    solve_for_m = we_solve_for('mass_flux')
    solve_for_u = we_solve_for('velocity')
    solve_for_p = we_solve_for('pressure')
    
    call assert(.not. (solve_for_m .and. solve_for_u))
    solve_for_mtum_and_press = assert_eq(solve_for_p, solve_for_m .or. solve_for_u)
  end function solve_for_mtum_and_press
  
  subroutine solve_for_energy(solve_for_engy,qnty)
    use assertions, only: assert
    
    logical, intent(out) :: solve_for_engy
    character(:), allocatable, optional, intent(out) :: qnty
    
    character(13), parameter :: qnties(3) = ['vol_enthalpy ','spec_enthalpy','temperature  ']
    logical :: HhT_solves(size(qnties))
    integer :: i
    
    HhT_solves = we_solve_for(qnties)
    call assert(count(HhT_solves) <= 1,'cannot solve multiple enthalpy Eq.')
    solve_for_engy = count(HhT_solves) > 0
    if (present(qnty)) then
      if (solve_for_engy) then
        do i = 1, size(qnties)
          if (HhT_solves(i)) allocate(qnty,source= trim(qnties(i)))
        enddo
      else
        allocate(qnty,source='none')
      endif
    endif
  end subroutine solve_for_energy
  
  subroutine grad_u_invariants(elem, pnt_set, time, frobnorm_ROS, trace_ROS3, frobnorm_Sd, eigenvals_ROS)
    !     D_ij    :=  partial_i  u_j
    !     S_ij    := (D_ij + D_ji) / 2      ("rate-of-strain tensor", ROS)
    ! 
    !     Sd_ij   := (g_ij + g_ji) / 2  -  g_mn g_mn delta_ij / 3
    !     g_ij    :=  D_im Dmj
    ! 
    ! For `g` and `Sd`, see Eq. 13 in [5]  or  Eq. 2-176 in [4]. This is wrong in Eq. 4.32 in [3]!
    ! 
    ! `trace_ROS3` is trace(S^3) = S_ij S_jk S_ki
    ! 
    ! Frobenius norm:
    !     |A|_Frob := sqrt(A_ij^2) = norm2(eigenvalues)
    ! 
    ! [3] Garnier, Adams, Sagaut, 'LES for compressible flows', Springer (2009)
    ! [4] Ansys User manual: https://www.sharcnet.ca/Software/Ansys/17.0/en-us/help/cfx_thry/cfxTurbLargSubg.html
    ! [5] Ghorbaniasl G., Lacor C. (2008) "Sensitivity of SGS Models and of Quality of LES to Grid Irregularity."
    !     In: Meyers J., Geurts B.J., Sagaut P. (eds) "Quality and Reliability of Large-Eddy Simulations." 
    !     Ercoftac Series, vol 12. Springer, Dordrecht
    use array_opers_mod, only: frobnorm_symm_mtx
    use pnt_set_mod, only: pnt_set_t
    use run_data, only: mesh_dimen
    use support, only: axpy_own_trace_x_I, eigenvals_symm, trace_power_symm
    
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out), optional :: frobnorm_ROS(:), trace_ROS3(:), frobnorm_Sd(:), eigenvals_ROS(:,:)
    
    real(dp), allocatable :: gder_u(:,:,:)
    real(dp) :: S(mesh_dimen, mesh_dimen), Sd(mesh_dimen, mesh_dimen)
    integer :: p, np

    call assert(mesh_dimen == elem%dimen(), BOUNDS_CHECK)
    if (present(eigenvals_ROS)) call assert(mesh_dimen == size(eigenvals_ROS,1), BOUNDS_CHECK)

    if (present(frobnorm_ROS)) then
      np = size(frobnorm_ROS)
    elseif (present(trace_ROS3)) then
      np = size(trace_ROS3)
    elseif (present(frobnorm_Sd)) then
      np = size(frobnorm_Sd)
    elseif (present(eigenvals_ROS)) then
      np = size(eigenvals_ROS, 2)
    else
      error stop "grad_u_invariants: expected at least one thing to compute"
    endif
    call assert(np == pnt_set%np(), BOUNDS_CHECK)

    if (present(frobnorm_ROS) .or. present(trace_ROS3)) call assert(.not. present(eigenvals_ROS), "It is not efficient &
        &to compute |S|_Frob = trace(S^2) or trace(S^3) separately when they can also be determined from the eigenvalues.")

    allocate(gder_u(mesh_dimen, mesh_dimen, np))
    call velocity%get_gders(elem,pnt_set,time,gder_u)

    if (present(eigenvals_ROS) .or. present(frobnorm_ROS) .or. present(trace_ROS3)) then
      do p = 1, np
        call symm_part(gder_u(:,:,p), S)
        if (present(frobnorm_ROS))  frobnorm_ROS(p) = sqrt(trace_power_symm(S, 2))
        if (present(trace_ROS3))    trace_ROS3(p) = trace_power_symm(S, 3)
        if (present(eigenvals_ROS)) call eigenvals_symm(S, eigenvals_ROS(:,p)) ! destroys `S`
      enddo
    endif

    if (present(frobnorm_Sd)) then
      do p = 1, np
        Sd = matmul(gder_u(:,:,p), gder_u(:,:,p))
        call add_own_transpose(Sd)
        call axpy_own_trace_x_I(Sd, -1/3.0_dp)
        frobnorm_Sd(p) = frobnorm_symm_mtx(Sd) / 2
      enddo
    endif
  
  contains
    subroutine symm_part(A, S)
      ! Only computes the lower triangular part.
      real(dp), intent(in) :: A(:,:)
      real(dp), intent(out) :: S(:,:)

      integer :: i, j, n

      n = assert_eq(size(A,1), size(A,2), size(S,1), size(S,2), BOUNDS_CHECK)

      do j = 1, mesh_dimen
        do i = j, mesh_dimen
          S(i,j) = (A(i,j) + A(j,i)) / 2
        enddo
      enddo
    end subroutine
  end subroutine

  subroutine add_own_transpose(A)
    ! A  <--  A + transpose(A)
    ! The strictly upper triangular part is not modified, as A + A^T is symmetric anyway.
    real(dp), intent(inout) :: A(:,:)
    
    integer :: n, i, j

    n = assert_eq(size(A,1),size(A,2),BOUNDS_CHECK)
    do j = 1, n
      do i = j, n
        A(i,j) = A(i,j) + A(j,i)
      enddo
    enddo
  end subroutine add_own_transpose
end module numerical_solutions
