module solvec_mod
  use assertions, only: assert, assert_eq, assert_normal, BOUNDS_CHECK, perform_all_bc
  use dof_handler, only: dof_handler_type, dof, dof_offset_within_elem
  use exceptions, only: error
  use f90_kind, only: dp
  use mesh_objects, only: elem_list, elem_id
  use mesh_type, only: fem_mesh
  use petsc_is_mod, only: petsc_Loc2Glob_map_t
  use petsc_vec_mod, only: petsc_vec_type
  use petsc_mod, only: no_petsc_procs
  use phys_time_mod, only: EX_weights, nold_times
  use run_data, only: dt
  use string_manipulations, only: int2char
  implicit none
  
  private
  public :: solvec_t, deepcopy
  
  type :: solvec_t
    ! The local coefficients are allocatable, because we need to return a pointer to them.
    private
    character(:), allocatable, public :: name
    type(dof_handler_type), public :: handler
    type(petsc_vec_type), public :: petsc_coeffs
    type(petsc_Loc2Glob_map_t) :: coeffs_Loc2Glob_map
    real(dp), allocatable, public :: local_F90_coeffs(:,:)
  contains
    procedure, non_overridable :: no_dirs => no_dirs_in_handler, ncoeffs
    procedure, non_overridable :: init_to_zero
    procedure, non_overridable :: io
    procedure, non_overridable :: max_weighted_sum
    procedure, non_overridable :: extrapolate_new_from_old_coeff
    procedure, non_overridable :: new_to_old_coeff, copy_new_to_all_old_coeff
    procedure, non_overridable :: damp_new_coeff
    procedure, non_overridable :: multiply_coeffs_by
    procedure, non_overridable :: update_ghost_values, update_ghost_values_to_f90idx
    procedure, non_overridable :: dotprod
    procedure, non_overridable :: matmult
    procedure, non_overridable :: nodes_dirs_ptr, ptr_to_coeffs
    procedure, non_overridable, private :: first_local_dof
  end type solvec_t
  
contains
  subroutine deepcopy(original,copy)
    type(solvec_t), intent(in) :: original
    type(solvec_t), intent(out) :: copy

    copy%name = original%name
    copy%handler = original%handler
    call original%petsc_coeffs%duplicate(copy%petsc_coeffs)
    call original%petsc_coeffs%copy(copy%petsc_coeffs)
    if (no_petsc_procs() > 1) call copy%petsc_coeffs%get_Loc2Glob_mapping(copy%coeffs_Loc2Glob_map)
    allocate(copy%local_F90_coeffs,source=original%local_F90_coeffs)
  end subroutine

  pure integer function no_dirs_in_handler(this) result(no_dirs)
    class(solvec_t), intent(in) :: this
    
    no_dirs = this%handler%no_dirs
  end function no_dirs_in_handler
  
  pure integer function ncoeffs(this)
    class(solvec_t), intent(in) :: this

    ncoeffs = size(this%local_F90_coeffs,2)
  end function ncoeffs

  subroutine init_to_zero(this,name,mesh,ncoeffs,no_dirs,orders,only_fluid_mesh)
    use dof_handler, only: get_indexes_of_ghost_points, init_dof_handler_inhom_orders
    
    class(solvec_t), intent(out) :: this
    character(*), intent(in) :: name
    type(fem_mesh), intent(in) :: mesh
    integer, intent(in) :: ncoeffs
    integer, intent(in) :: no_dirs
    integer, intent(in) :: orders(:)
    logical, intent(in) :: only_fluid_mesh
    
    integer :: my_ndof
    integer, allocatable :: ghost_idxs(:)
    
    this%name = name
    call init_dof_handler_inhom_orders(this%handler,mesh,1,no_dirs,orders,only_fluid_mesh)
    call get_indexes_of_ghost_points(mesh,this%handler,ghost_idxs)
    call this%petsc_coeffs%init(no_local_rows=this%handler%no_dof_in_partition(),ghost_idxs=ghost_idxs)
    if (no_petsc_procs() > 1) call this%petsc_coeffs%get_Loc2Glob_mapping(this%coeffs_Loc2Glob_map)
    my_ndof = this%handler%no_dof_in_partition() + size(ghost_idxs)
    allocate(this%local_F90_coeffs(my_ndof,ncoeffs),source=0.0_dp)
    call this%update_ghost_values()
    call this%copy_new_to_all_old_coeff()
  end subroutine init_to_zero
  
  subroutine update_ghost_values(this)
    class(solvec_t), intent(inout) :: this
    
    call this%update_ghost_values_to_f90idx(coeff_idx=1)
  end subroutine update_ghost_values

  subroutine update_ghost_values_to_f90idx(this,coeff_idx)
    class(solvec_t), intent(inout) :: this
    integer, intent(in) :: coeff_idx
    
    call this%petsc_coeffs%update_ghost_values()
    call this%petsc_coeffs%get_values_including_ghosts(this%local_F90_coeffs(:,coeff_idx))
  end subroutine
  
  subroutine new_to_old_coeff(this)
    class(solvec_t), intent(inout) :: this
    
    integer :: t, nt
    
    nt = this%ncoeffs()
    do t = nt, 2, -1
      this%local_F90_coeffs(:,t) = this%local_F90_coeffs(:,t-1)
    enddo
  end subroutine new_to_old_coeff
  
  subroutine copy_new_to_all_old_coeff(this)
    class(solvec_t), intent(inout) :: this
    
    integer :: i
    
    do i = 2, this%ncoeffs()
      this%local_F90_coeffs(:,i) = this%local_F90_coeffs(:,1)
    enddo
  end subroutine copy_new_to_all_old_coeff
  
  subroutine extrapolate_new_from_old_coeff(this)
    class(solvec_t), intent(inout) :: this
    
    integer :: ndofs, order
    
    call assert(this%ncoeffs() > 1,'solvec: no old time steps from which to extrapolate')
    
    order = min(this%ncoeffs()-1,nold_times)
    ndofs = this%handler%no_dof_in_partition()
    this%local_F90_coeffs(1:ndofs,1) = matmul(this%local_F90_coeffs(1:ndofs,2:order+1),EX_weights(order,dt))
    call this%petsc_coeffs%copy_from_local_fort_vec(this%local_F90_coeffs(1:ndofs,1))
  end subroutine extrapolate_new_from_old_coeff
  
  subroutine damp_new_coeff(this,gamma)
    use assertions, only: assert
    
    class(solvec_t), intent(inout) :: this
    real(dp), intent(in) :: gamma
    
    call assert(this%ncoeffs() == 2,'Don''t know how to damp solvec coefficients with more than 2 set of coeffs')
    
    this%local_F90_coeffs(:,1) = gamma*this%local_F90_coeffs(:,1) + (1-gamma)*this%local_F90_coeffs(:,2)
  end subroutine damp_new_coeff
  
  subroutine multiply_coeffs_by(this,factor)
    class(solvec_t), intent(inout) :: this
    real(dp), intent(in) :: factor
    
    call this%petsc_coeffs%mult_with_scal(factor)
    this%local_F90_coeffs = factor * this%local_F90_coeffs
  end subroutine multiply_coeffs_by

  subroutine io(this,unit_number,from_file)
    use petsc_mod, only: I_am_master_process
    
    class(solvec_t), intent(inout) :: this
    integer, intent(in) :: unit_number
    logical, intent(in) :: from_file
    
    integer :: ios, i
    character(200) :: msg
    real(dp), allocatable :: fortran_global_coeffs(:), old_fortran_global_coeffs(:)
    type(petsc_vec_type) :: seq_petsc_coeffs, aux_petsc_vec
    
    allocate(fortran_global_coeffs(this%handler%no_dof_1grp))
    if (from_file) allocate(old_fortran_global_coeffs,mold=fortran_global_coeffs)

    call this%petsc_coeffs%duplicate(aux_petsc_vec)
    
    do i = 1, this%ncoeffs()
      if (from_file) then
        read(unit_number,iostat=ios,iomsg=msg) old_fortran_global_coeffs
        call assert(ios == 0,'in solvec I/O, iomsg=' // trim(msg))
        ! First, rearrange the coefficients according to the new elem permutation
        call rearrange_coeffs_based_on_new_permutation(this%handler,old_fortran_global_coeffs,fortran_global_coeffs)
        ! Then, initialize the solvec coefficients
        call aux_petsc_vec%copy_from_global_fort_vec(fortran_global_coeffs)
        call aux_petsc_vec%update_ghost_values()
        call aux_petsc_vec%get_values_including_ghosts(this%local_F90_coeffs(:,i))
        if (i ==1) call aux_petsc_vec%copy(this%petsc_coeffs)
      else
        ! First, copy the local_F90_coeffs into the aux_petsc_vec
        call aux_petsc_vec%copy_from_local_fort_vec(this%local_F90_coeffs(1:this%handler%no_dof_in_partition(),i))
        ! Second, scatter the auxiliary vector onto the master process,
        ! get the values and write them into the binary file
        call aux_petsc_vec%to_seq_vec_on_0th_process(seq_petsc_coeffs)
        if (I_am_master_process()) then
          call seq_petsc_coeffs%get_local_values(fortran_global_coeffs)
          call assert_normal(fortran_global_coeffs,'I/O in solvec: f90 coeffs are not normal')
          write(unit_number,iostat=ios,iomsg=msg) fortran_global_coeffs
          call assert(ios == 0,'in solvec I/O, iomsg=' // trim(msg))
        endif
      endif
    enddo
  end subroutine io
  
  subroutine rearrange_coeffs_based_on_new_permutation(handler,old_coeffs,new_coeffs)
    use mesh_partitioning, only: old_new_permutation_all, old_new_permutation_fluid
    
    type(dof_handler_type), intent(in) :: handler
    real(dp), intent(in) :: old_coeffs(:)
    real(dp), intent(out) :: new_coeffs(:)
    
    integer :: old_elem_no, new_elem_no
    integer :: old_start_idx, old_end_idx, new_start_idx, new_end_idx
    integer, allocatable :: permutation(:)
    
    call assert(size(new_coeffs) == size(old_coeffs),'Incompatible size between new and old coeffs')
    
    if (handler%only_fluid_mesh) then
      permutation = old_new_permutation_fluid
    else
      permutation = old_new_permutation_all
    endif
    
    old_start_idx = 1
    do old_elem_no = 1, size(permutation)
      new_elem_no = permutation(old_elem_no)
      old_end_idx = handler%no_dof_in_elem(new_elem_no) + old_start_idx - 1
      new_start_idx = handler%elem_start_index(new_elem_no)
      new_end_idx = handler%no_dof_in_elem(new_elem_no) + new_start_idx - 1
      new_coeffs(new_start_idx:new_end_idx) = old_coeffs(old_start_idx:old_end_idx)
      old_start_idx = old_end_idx + 1
    enddo
  end subroutine rearrange_coeffs_based_on_new_permutation
  
  function max_weighted_sum(this,mesh,weights)
    use mesh_partitioning, only: my_first_last_elems
    use mpi_wrappers_mod, only: allreduce_wrap
    
    class(solvec_t), intent(in) :: this
    type(fem_mesh), intent(in) :: mesh
    real(dp) :: weights(:)
    real(dp) :: max_weighted_sum(this%no_dirs())
    
    logical :: only_fluid_mesh
    real(dp) :: int_weighted_sum(this%no_dirs()), weighted_sum_coeff
    integer :: elem_no, first_elem, last_elem
    integer :: order
    integer :: idx
    integer :: dir, nod, nnod
    integer :: no_eff_coeffs
    type(elem_list), pointer :: elem
    type(elem_id) :: id
    type(elem_id), pointer, dimension(:) :: active_elem_list
    real(dp), allocatable, dimension(:) :: elem_int_fi
    real(dp) :: my_max_weighted_sum(this%no_dirs())
    
    call assert(this%ncoeffs() > 1)
    
    no_eff_coeffs = min(this%ncoeffs(),nold_times+1)
    call assert(size(weights) == no_eff_coeffs)
    
    only_fluid_mesh = this%handler%only_fluid_mesh
    call mesh%get_active_elem_list_ptr(only_fluid_mesh,active_elem_list)
    call my_first_last_elems(only_fluid_mesh,first_elem,last_elem)
    
    my_max_weighted_sum = 0.0_dp
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      order = this%handler%order(elem_no)
      call int_fi_on_elem(elem,order,elem_int_fi)
      nnod = assert_eq(size(elem_int_fi),this%handler%no_nod_elem(elem_no),BOUNDS_CHECK)
      int_weighted_sum = 0.0_dp
      do dir = 1, this%no_dirs()
        do nod = 1, nnod
          idx = dof(this%handler,1,elem_no,dir,nod) - this%handler%first_dof_in_partition + 1
          weighted_sum_coeff = dot_product(this%local_F90_coeffs(idx,:no_eff_coeffs),weights)
          int_weighted_sum(dir) = int_weighted_sum(dir) + weighted_sum_coeff * elem_int_fi(nod)
        enddo
      enddo
      my_max_weighted_sum = max(my_max_weighted_sum,abs(int_weighted_sum)/elem%volume)
    enddo
    
    call allreduce_wrap(my_max_weighted_sum,'max',max_weighted_sum)
  end function max_weighted_sum
  
  pure subroutine int_fi_on_elem(elem,order,elem_int_fi)
    use local_elem, only: no_dg_functions
    
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: order
    real(dp), allocatable, dimension(:), intent(out) :: elem_int_fi
    
    integer :: nnod
    
    nnod = no_dg_functions(elem%eltype,order)
    allocate(elem_int_fi(nnod))
    elem_int_fi = matmul(elem%quad_points%dg_funcs(:nnod,:), elem%quot)
  end subroutine int_fi_on_elem
  
  subroutine matmult(this,elem_no,dg_funcs_mat,time,values)
    class(solvec_t), intent(in) :: this
    integer, intent(in) :: elem_no
    real(dp), intent(in) :: dg_funcs_mat(:,:,:)
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:,:,:)
    
    integer :: nnod, fdof
    integer :: node, idx, v, nv, t, dir, ndirs, p, np
    real(dp), allocatable :: weights(:)
    integer :: order
    
    nnod = assert_eq(size(dg_funcs_mat,1), this%handler%no_nod_elem(elem_no), BOUNDS_CHECK)
    nv = assert_eq(size(dg_funcs_mat,2), size(values,1), BOUNDS_CHECK)
    ndirs = assert_eq(size(values,2), this%no_dirs(), BOUNDS_CHECK)
    np = assert_eq(size(dg_funcs_mat,3),size(values,3),BOUNDS_CHECK)
    
    fdof = this%first_local_dof(elem_no,dir=1)

    if (0 < time .and. time <= this%ncoeffs()) then
      values = 0.0_dp
      do p = 1, np
        do dir = 1, ndirs
          do v = 1, nv
            do node = 1, nnod
              idx = fdof + dof_offset_within_elem(this%handler,elem_no,dir,node)
              values(v,dir,p) = values(v,dir,p) + dg_funcs_mat(node,v,p) * this%local_F90_coeffs(idx,time)
            enddo
          enddo
        enddo
      enddo

    elseif (time == 0) then
      order = min(this%ncoeffs()-1,nold_times)
      weights = EX_weights(order,dt)
      values = 0.0_dp
      do t = 1, assert_eq(order,size(weights),BOUNDS_CHECK)
        do p = 1, np
          do dir = 1, ndirs
            do v = 1, nv
              do node = 1, nnod
                idx = fdof + dof_offset_within_elem(this%handler,elem_no,dir,node)
                values(v,dir,p) = values(v,dir,p) + dg_funcs_mat(node,v,p) * this%local_F90_coeffs(idx,t+1) * weights(t)
              enddo
            enddo
          enddo
        enddo
      enddo
    
    else
      call error('Cannot compute solvec%matmult at time='//int2char(time)//&
                 ' (solvec%noeffs='//int2char(this%ncoeffs())//')')
    endif
  end subroutine
  
  subroutine dotprod(this,elem_no,dg_funcs_vec,time,values)
    ! Evaluates the solvec at time t, according to this convention:
    ! o t >= 2, use solvec coefficients at time t
    ! o t = 1, use solvec coefficients at time 1, which are the extrapolated ones
    !          or the most recent ones in an inner loop
    ! o t = 0, use coefficients extrapolated from those at time t >= 2
    class(solvec_t), intent(in) :: this
    integer, intent(in) :: elem_no
    real(dp), intent(in) :: dg_funcs_vec(:,:)
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:,:)
    
    integer :: node, nnod, dir, ndirs, p, np, t
    integer :: fdof, idx
    integer :: order
    real(dp), allocatable :: weights(:)
    
    nnod = assert_eq(size(dg_funcs_vec,1), this%handler%no_nod_elem(elem_no), BOUNDS_CHECK)
    ndirs = assert_eq(size(values,1), this%no_dirs(), BOUNDS_CHECK)
    np = assert_eq(size(values,2), size(dg_funcs_vec,2), BOUNDS_CHECK)
    
    fdof = this%first_local_dof(elem_no,dir=1)

    if (0 < time .and. time <= this%ncoeffs()) then
      values = 0.0_dp
      do p = 1, np
        do dir = 1, ndirs
          do node = 1, nnod
            idx = fdof + dof_offset_within_elem(this%handler,elem_no,dir,node)
            values(dir,p) = values(dir,p) + this%local_F90_coeffs(idx,time) * dg_funcs_vec(node,p)
          enddo
        enddo
      enddo
    
    elseif (time == 0) then
      order = min(this%ncoeffs()-1,nold_times)
      weights = EX_weights(order,dt)
      values = 0.0_dp
      do t = 1, assert_eq(order,size(weights),BOUNDS_CHECK)
        do p = 1, np
          do dir = 1, ndirs
            do node = 1, nnod
              idx = fdof + dof_offset_within_elem(this%handler,elem_no,dir,node)
              values(dir,p) = values(dir,p) + this%local_F90_coeffs(idx,t+1) * dg_funcs_vec(node,p) * weights(t)
          enddo
        enddo
      enddo
    enddo
        
    else
      call error('Cannot compute solvec%dotprod for '//this%name// &
                  ' at time='//int2char(time)//' (solvec%noeffs='//int2char(this%ncoeffs())//')')
    endif
  end subroutine
  
  function nodes_dirs_ptr(this,elem_no,time) result(ptr)
    class(solvec_t), target, intent(in) :: this
    integer, intent(in) :: elem_no, time
    real(dp), contiguous, pointer :: ptr(:,:)

    integer :: first_ldof, nnod

    nnod = this%handler%no_nod_elem(elem_no)
    first_ldof = first_local_dof(this,elem_no,dir=1)
    ptr(1:nnod,1:this%no_dirs()) => this%local_F90_coeffs(first_ldof:first_ldof+this%no_dirs()*nnod-1,time)
  end function

  function ptr_to_coeffs(this,elem_no,dir) result(ptr)
    class(solvec_t), target, intent(in) :: this
    integer, intent(in) :: elem_no, dir
    real(dp), pointer :: ptr(:,:)

    integer :: first_ldof, nnod

    nnod = this%handler%no_nod_elem(elem_no)
    first_ldof = first_local_dof(this,elem_no,dir)
    ptr => this%local_F90_coeffs(first_ldof:first_ldof+nnod-1,:)
  end function

  integer function first_local_dof(this,elem_no,dir)
    class(solvec_t), intent(in) :: this
    integer, intent(in) :: elem_no, dir

    integer :: first_global_dof

    first_global_dof = dof(this%handler,1,elem_no,dir,node=1)
    if (no_petsc_procs() > 1) then
      call this%coeffs_Loc2Glob_map%apply_Glob2toLoc_mapping(first_global_dof,first_local_dof)
      
      ! Verify that the nodes/dirs form a contiguous list by checking the last indices.
      ! This is probably very expensive.
      if (perform_all_bc) then
        block
          integer :: last_gnode, last_gdir, last_lnode, last_ldir, nnod
          
          nnod = this%handler%no_nod_elem(elem_no)
          last_gnode = dof(this%handler,1,elem_no,dir=1,             node=nnod)
          last_gdir  = dof(this%handler,1,elem_no,dir=this%no_dirs(),node=nnod)
          call this%coeffs_Loc2Glob_map%apply_Glob2toLoc_mapping(last_gnode,last_lnode)
          call this%coeffs_Loc2Glob_map%apply_Glob2toLoc_mapping(last_gdir,last_ldir)
          call assert(last_lnode - first_local_dof == last_gnode - first_global_dof,"nodes should be contiguous in solvec")
          call assert(last_ldir  - first_local_dof == last_gdir  - first_global_dof,"dirs should be contiguous in solvec")
        end block
      endif
    
    else
      first_local_dof = first_global_dof
    endif
  end function
end module solvec_mod
