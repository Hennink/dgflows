module fortran_parser_mod
  use assertions, only: assert
  use f90_kind, only: dp
  use fortran_parser_interface_mod, only : add_eqparser, evaluate
  use run_data, only: mesh_dimen
  use scalfunc_gcors_mod, only: scalfunc_gcors_t
  implicit none

  private
  public :: parse_str2func

  type, extends(scalfunc_gcors_t) :: parsed_func_t
    private
    integer :: parser_idx = 0
  contains
    procedure, non_overridable :: value_at_gcor
  end type parsed_func_t

contains
  type(parsed_func_t) function parse_str2func(eq_str) result(func)
    character(*), intent(in) :: eq_str

    character, parameter :: xyz(3) = ['x','y','z']

    call assert(mesh_dimen == 2 .or. mesh_dimen == 3,'weird mesh dimension')
    call add_eqparser(eq_str,[xyz(:mesh_dimen),'t'],func%parser_idx)
  end function parse_str2func


  subroutine value_at_gcor(this,gcor,time,value)
    use phys_time_mod, only: phys_time

    class(parsed_func_t), intent(in) :: this
    real(dp), intent(in) :: gcor(:)
    integer, intent(in) :: time
    real(dp), intent(out) :: value

    call assert(size(gcor) == mesh_dimen,'fortran_parser_mod: bad size(gcor)')

    value = evaluate(this%parser_idx,[gcor,phys_time(time)])
  end subroutine
end module fortran_parser_mod
