module matprop_scalfunc_mod
  use assertions, only: assert
  use exceptions, only: error
  use f90_kind, only: dp
  use mesh_objects, only: elem_list
  use pnt_set_mod, only: pnt_set_t
  use scal_func_mod, only: scal_func_type
  implicit none
  
  private
  public :: scalfuncs_of_matprop_t
  
  type :: id_matprop_pair_t
    integer :: id
    class(scal_func_type), allocatable :: matprop
  end type id_matprop_pair_t
  
  integer, parameter :: MAX_NPAIRS = 10

  type, extends(scal_func_type) :: scalfuncs_of_matprop_t
    ! Create a function that returns the correct value of a matprop
    ! in a specific material
    private
    integer :: npairs = 0
    type(id_matprop_pair_t) :: pairs(MAX_NPAIRS)
  contains
    procedure, non_overridable :: get_values, get_gders
    procedure, non_overridable :: add_id_matprop
    procedure, non_overridable, private :: find_id
  end type scalfuncs_of_matprop_t
  
contains
  subroutine get_values(this,elem,pnt_set,time,values)
    class(scalfuncs_of_matprop_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    integer :: loc_id
    
    loc_id = this%find_id(elem%mat_id)
    call assert(loc_id /= 0, 'cannot find id for matprop in scalfunc')
    call this%pairs(loc_id)%matprop%get_values(elem,pnt_set,time,values)
  end subroutine get_values
  
  subroutine get_gders(this,elem,pnt_set,time,gders)
    class(scalfuncs_of_matprop_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:)
    
    integer :: loc_id
    
    loc_id = this%find_id(elem%mat_id)
    call assert(loc_id /= 0, 'cannot find id for matprop in scalfunc')
    call this%pairs(loc_id)%matprop%get_gders(elem,pnt_set,time,gders)
  end subroutine get_gders
  
  subroutine add_id_matprop(this,id,matprop)
    class(scalfuncs_of_matprop_t), intent(inout) :: this
    integer, intent(in) :: id
    class(scal_func_type), intent(in) :: matprop

    call assert(this%find_id(id) == 0,'add_id_matprop: id already present')
    call assert(this%npairs < MAX_NPAIRS,'add_id_matprop: cannot add more (id,matprop) pairs')

    this%npairs = this%npairs + 1
    this%pairs(this%npairs)%id = id
    this%pairs(this%npairs)%matprop = matprop
  end subroutine add_id_matprop

  integer function find_id(this,id)
    class(scalfuncs_of_matprop_t), intent(in) :: this
    integer, intent(in) :: id

    do find_id = 1, this%npairs
      if (id == this%pairs(find_id)%id) return
    enddo
    find_id = 0
  end function find_id
end module matprop_scalfunc_mod
