module time_step_mod
  use assertions, only: assert
  use exceptions, only: error
  use f90_kind, only: dp
  use h_solver_mod, only: h_solver_t
  use mesh_type, only: fem_mesh
  use m_solver_mod, only: new_m_solver, include_m_convection
  use numerical_solutions, only: symm_of_qnty_linsys, &
                                 we_solve_for, solve_for_energy, solve_for_mtum_and_press, &
                                 update_old_coeff, predict_new_coeff, get_solvec, &
                                 matprop_set, num_scalfunc_set
  use phys_time_mod, only: add_step_with_dt, nold_times, old_times
  use petsc_mod, only: I_am_master_process
  use petsc_vec_mod, only: petsc_vec_type
  use pressure_corr, only: pressure_correction
  use qnty_solver_mod, only: qnty_solver_t
  use run_data, only: dt, spec_h_strategy
  implicit none
  
  private
  public :: smoothen_mtum_press
  public :: print_max_cfl
  public :: init_solvers
  public :: time_step, process_quantities

  class(qnty_solver_t), allocatable, target :: m_solver
  type(h_solver_t), allocatable, target :: h_solver

  logical, public :: main_calc = .false.

contains
  subroutine switch_logstage_main_calc(logstage_name)
    ! Wrapper around `switch_logstage` differentiates between the first few time
    ! steps and the main calculations, based on the global flag `main_calc`.
    ! 
    use petsc_mod, only: switch_logstage

    character(*), intent(in) :: logstage_name
    
    if (main_calc) then
      call switch_logstage(logstage_name)
    else
      call switch_logstage('first_steps')
    endif
  end subroutine

  subroutine init_solvers()
    logical :: solve_for_engy
    character(:), allocatable :: engy_qnty

    if (solve_for_mtum_and_press()) then
      m_solver = new_m_solver()
      m_solver%keep_separate_constant = .true.
      call m_solver%set_solver_specs(expect_symm=symm_of_qnty_linsys(m_solver%qnty))
    endif

    call solve_for_energy(solve_for_engy,engy_qnty)
    if (solve_for_engy) then
      allocate(h_solver)
      call init_h_solver(engy_qnty, h_solver)
      h_solver%keep_separate_constant = .true.
      h_solver%impl_strategy = spec_h_strategy
    endif
  end subroutine

  subroutine smoothen_mtum_press(mesh)
    ! Process the instructions in 'process_ic' sequentially.
    use run_data, only: process_ic
    
    type(fem_mesh), intent(in) :: mesh

    integer :: i

    do i = 1, size(process_ic)
      call process_solver_specs(mesh,trim(process_ic(i)))
    enddo
  end subroutine smoothen_mtum_press

  subroutine process_solver_specs(mesh,solver_specs)
    type(fem_mesh), intent(in) :: mesh
    character(*), intent(in) :: solver_specs

    logical :: solve_for_engy
    character(:), allocatable :: engy_qnty
    real(dp) :: rel_norm_corr

    ! Solvers as local variables, because they may be different from the standard
    ! ones that are stored in the module (e.g., stokes flow has no convection).
    class(qnty_solver_t), allocatable, target :: m_solver
    type(h_solver_t), target :: h_solver

    m_solver = new_m_solver()
    call m_solver%set_solver_specs(expect_symm=symm_of_qnty_linsys('mass_flux'))
    
    if (I_am_master_process()) write(*,"(a)") repeat('*',20) // '  ' // solver_specs // '  ' // repeat('*',20)
    select case(solver_specs)
      case('stokes','nse')
        include_m_convection = solver_specs == 'nse'
        call m_solver%assemble_and_solve(mesh)
        call pressure_correction(mesh,dt,rel_norm_corr)
        if (I_am_master_process()) write(*,"(a,es8.1)") '||P_corr||_2 / ||P||_2 = ', rel_norm_corr
        call update_old_coeff('mass_flux')
        call update_old_coeff('pressure')
      
      case('nse_until_smooth', 'stokes_until_smooth')
        include_m_convection = solver_specs == 'nse_until_smooth'
        call progress_mtum_press_until_smooth(m_solver,mesh)
      
      case('loosely_coupled')
        include_m_convection = .false.
        call loosely_coupled_time_steps(m_solver,mesh,no_steps=1)
      
      case('stat_enthalpy', 'enthalpy')
        call solve_for_energy(solve_for_engy, engy_qnty)
        call assert(solve_for_engy, 'process_solver_specs: cannot process IC for h when not solving for h')
        call init_h_solver(engy_qnty, h_solver)
        
        h_solver%include_time_term = solver_specs == 'enthalpy'
        call h_solver%assemble_and_solve(mesh)
        call update_old_coeff(h_solver%qnty)

      case default
        call error('cannot interpret solve_specs='//solver_specs)
    end select
  end subroutine

  subroutine loosely_coupled_time_steps(m_solver,mesh,no_steps)
    ! Progresses quantities in a loosely coupled fashion: first solve the
    ! enthalpy equation, then progress (m,p) without updating h.
    ! Repeat for 'no_steps'.
    !
    ! Using the time-independent enthalpy equation avoids the d/dt rho term in the
    ! PC, thereby enabling the loose coupling.
    ! 
    ! The stationary `h` solver can be very stiff, so you might need to set a 
    ! large maximum number of iterations.
    ! 
    class(qnty_solver_t), target, intent(inout) :: m_solver
    type(fem_mesh), intent(in) :: mesh
    integer, intent(in) :: no_steps
    
    logical :: solve_for_engy
    character(:), allocatable :: engy_qnty
    type(h_solver_t), target :: h_solver
    integer :: i
    
    call solve_for_energy(solve_for_engy,engy_qnty)
    call assert(solve_for_engy,'loosely_coupled_time_steps: no engy?')

    call init_h_solver(engy_qnty, h_solver)
    h_solver%include_time_term = .false.
    do i = 1, no_steps
      call h_solver%assemble_and_solve(mesh)
      call update_old_coeff(h_solver%qnty)
      
      call progress_mtum_press_until_smooth(m_solver,mesh)
    enddo
  end subroutine
  
  subroutine progress_mtum_press_until_smooth(m_solver,mesh)
    use exceptions, only: warning
    use iter_manager_mod, only: iter_manager_t
    
    class(qnty_solver_t), target, intent(inout) :: m_solver
    type(fem_mesh), intent(in) :: mesh

    integer :: i
    type(iter_manager_t) :: iter_man
    
    call iter_man%init('||P_corr||_2 / ||P||_2',max_iter=50,tol=0.01_dp)
    do i = 1, 20
      call iter_only_mtum_press(m_solver,mesh,iter_man)
      call update_old_coeff('mass_flux')
      call update_old_coeff('pressure')
      if (iter_man%iter_done() <= 1) return
    enddo
    if (I_am_master_process()) call warning('always keep needing more then one iter when progressing (m,P)')
  end subroutine progress_mtum_press_until_smooth
  
  subroutine time_step(mesh)
    ! Calculates the 'new' coefficients in the solvecs by solving the coupled 
    ! transport equations.
    ! 
    ! Treats the new coefficients like mutable variables that can be updated during an iteration.
    use les_eddy_viscosity_mod, only: recalc_eddy_viscosity
    use les_heat_flux_mod, only: recalc_turb_Pr
    use rans_mod, only: rans_model
    use run_data, only: max_iter_within_time_step, no_iter_Turb_in_timestep, no_iter_NS_Turb_in_timestep, &
                        les_specs_file, use_rans_model
    use timer_typedef, only: timer_type
    use turb_quantities, only: turb_solver_t
    
    type(fem_mesh), intent(in) :: mesh
    
    logical :: solve_for_engy, solve_turb1_eq, solve_turb2_eq
    character(:), allocatable :: engy_qnty
    type(turb_solver_t), target :: turb1_solver, turb2_solver
    integer :: l, i
    type(timer_type) :: timer
    
    call timer%start()
    call switch_logstage_main_calc('time_step_misc')

    include_m_convection = .true.

    if (.not. solve_for_mtum_and_press()) then
      call assert(max_iter_within_time_step == 1, 'max_iter_within_time_step > 1 but not solving for M and P')
      call assert(no_iter_NS_Turb_in_timestep == 1, 'no_iter_NS_Turb_in_timestep > 1 but not solving for M and P')
    endif
    
    call solve_for_energy(solve_for_engy,engy_qnty)
    
    if (solve_for_engy) call predict_new_coeff(engy_qnty)
    if (solve_for_mtum_and_press()) then
      call predict_new_coeff('mass_flux')
      call predict_new_coeff('pressure')
    endif
    if (use_rans_model) then
      solve_turb1_eq = rans_model%no_eq >= 1 .and. we_solve_for('vol_turb1')
      solve_turb2_eq = rans_model%no_eq >= 2 .and. we_solve_for('vol_turb2')
      if (solve_turb1_eq) call predict_new_coeff('vol_turb1')
      if (solve_turb2_eq) call predict_new_coeff('vol_turb2')
      if (.not. (solve_turb1_eq .or. solve_turb2_eq)) then
        call assert(no_iter_Turb_in_timestep == 1, 'no_iter_Turb_in_timestep > 1 but not solving for turb. quantities')
        call assert(no_iter_NS_Turb_in_timestep == 1, 'no_iter_NS_Turb_in_timestep > 1 but not solving for turb. quantities')
      endif
    endif
    
    if (les_specs_file /= '') call recalc_eddy_viscosity(mesh)
    
    if (solve_for_engy) then
      if (les_specs_file /= '') call recalc_turb_Pr(mesh)
      call switch_logstage_main_calc(engy_qnty)
      if (engy_qnty == 'spec_enthalpy') then
        call first_engy_solve_of_time_step(mesh)
      else
        call h_solver%assemble_and_solve(mesh)
      endif
    endif
    
    do i = 1, no_iter_NS_Turb_in_timestep
      if (solve_for_mtum_and_press()) call manage_m_p_equations(mesh)
      
      if (use_rans_model) then
        call switch_logstage_main_calc('time_step_misc')
        if (solve_turb1_eq) call init_turb_solver('1st_eq',turb1_solver)
        if (solve_turb2_eq) call init_turb_solver('2nd_eq',turb2_solver)
        
        do l = 1, no_iter_Turb_in_timestep
          if (solve_turb1_eq) then
            call switch_logstage_main_calc('vol_turb1');  call turb1_solver%assemble_and_solve(mesh)
          endif
          if (solve_turb2_eq) then
            call switch_logstage_main_calc('vol_turb2');  call turb2_solver%assemble_and_solve(mesh)
          endif
        enddo
      endif
    enddo

    if (I_am_master_process()) write(*,'(a,ES10.3)') 'comp. time this step = ', timer%time()
  end subroutine time_step
  
  subroutine first_engy_solve_of_time_step(mesh)
    ! Solves the spec. h equation after setting the strategy.
    use density_mod, only: density_solver_t
    use galerkin_projection, only: galerkin_proj
    use functionals, only: continuous_rel_l2_diff
    use run_data, only: constant_density
    use scal_func_mod, only: scal_func_type
    use solvec_mod, only: solvec_t

    type(fem_mesh), intent(in) :: mesh
    
    type(solvec_t), pointer :: rhop_solvec
    type(density_solver_t), target :: rhop_solver
    class(scal_func_type), pointer :: matprop_rho
    real(dp) :: rel_cont_norm_rho
    
    call assert(h_solver%qnty == 'spec_enthalpy', "'first_engy_solve_of_time_step' handles the variable density &
        &in the temporal derivative. There is no point in calling it for a conservec quantity.")
    
    if (constant_density) then
      call assert(spec_h_strategy == '','Why do you need a spec_h_strategy when rho is constant?')
    
    elseif (spec_h_strategy == 'deriv_rho_h') then
      
    elseif (spec_h_strategy == 'rho_predictor') then
      if (we_solve_for('density_predictor')) then
        ! Project the matprop rho onto the predictor solvec, to help the solver:
        matprop_rho => matprop_set%func_ptr('density')
        rhop_solvec => get_solvec('density_predictor')
        call galerkin_proj(mesh,rhop_solvec%handler,matprop_rho,time=0,discrete_proj=rhop_solvec%petsc_coeffs)
        ! Solve the specific enthalpy equation with the rho predictor:
        rhop_solver%qnty = 'density_predictor'
        call rhop_solver%set_solver_specs(expect_symm=symm_of_qnty_linsys('density_predictor'))
        call rhop_solver%assemble_and_solve(mesh)
        h_solver%newrho_predictor => num_scalfunc_set%func_ptr('density_predictor')
        
      else
        h_solver%newrho_predictor => matprop_set%func_ptr('density')
      endif
      
    else
      call error('first_engy_solve_of_time_step: unknown spec_h_strategy=' // spec_h_strategy)
    endif

    call h_solver%assemble_and_solve(mesh)

    if (spec_h_strategy == 'rho_predictor' .and. we_solve_for('density_predictor')) then
      ! Determine the error in the rho prediction:
      rel_cont_norm_rho = continuous_rel_l2_diff(                         &
          mesh, only_fluid=.true.,                                        &
          new=matprop_set%func_ptr('density'), time_new=1,                &
          old=num_scalfunc_set%func_ptr('density_predictor'), time_old=1, &
          bcast=.false. &
      )
      if (I_am_master_process()) print *, '|rho_corr|_2 / |rho|_2 = ', rel_cont_norm_rho
    endif
  end subroutine
  
  subroutine manage_m_p_equations(mesh)
    use iter_manager_mod, only: iter_manager_t
    use run_data, only: calc_norm_P_corr, max_iter_within_time_step, tol_rel_Pcorr, m_p_strategy, tol_rel_coupled_NS
    
    type(fem_mesh), intent(in) :: mesh
    
    logical, save :: expect_multiple_m_p_iter = .true.
    type(iter_manager_t) :: iter_man
    
    character(:), allocatable :: corr_name
    real(dp) :: tol, rel_norm_corr
    logical :: print_corr_norm

    if (m_p_strategy == 'pressure-correction') then
      corr_name = '||P_corr||_2/||P||_2'
      tol = tol_rel_Pcorr
    elseif (m_p_strategy == 'fieldsplit') then
      corr_name = '||X^(k+1) - X^(k)||_2/||X^(k)||_2'
      tol = tol_rel_coupled_NS
    else
      call error('manage_m_p_equations: unknown: m_p_strategy=' // m_p_strategy)
    endif

    if (m_p_strategy == 'pressure-correction' .and. max_iter_within_time_step == 1) then
      ! shortcut to avoid calculating P correction norm
      if (calc_norm_P_corr <= 0) then
        print_corr_norm = .false.
      else
        print_corr_norm = mod(nold_times-1, calc_norm_P_corr) == 0
      endif
      call switch_logstage_main_calc('mass_flux')
      call m_solver%assemble_and_solve(mesh)
      call switch_logstage_main_calc('pressure')
      if (print_corr_norm) then
        call pressure_correction(mesh, dt, rel_norm_corr)
        if (i_am_master_process()) write(*,"(a,' = ',es8.1)") corr_name, rel_norm_corr
      else
        call pressure_correction(mesh, dt)
      endif
    
    else
      call switch_logstage_main_calc('mass_flux') ! lump p in with m
      call iter_man%init(corr_name, max_iter_within_time_step, tol)
      call iter_only_mtum_press(m_solver,mesh,iter_man)

      expect_multiple_m_p_iter = iter_man%iter_done() > 1
    endif
  end subroutine manage_m_p_equations
  
  subroutine iter_only_mtum_press(m_solver,mesh,iter_man)
    use fieldsplit_mod, only: solve_NS_coupled_sys
    use iter_manager_mod, only: iter_manager_t
    use run_data, only: m_p_strategy
    
    class(qnty_solver_t), target, intent(inout) :: m_solver
    type(fem_mesh), intent(in) :: mesh
    type(iter_manager_t), intent(inout) :: iter_man
    
    real(dp) :: rel_norm_corr, rel_diff_sol
    
    call assert(solve_for_mtum_and_press())
    
    call iter_man%start_new_iteration()
    do
      if (m_p_strategy == 'pressure-correction') then
        call m_solver%assemble_and_solve(mesh)
        call pressure_correction(mesh,dt,rel_norm_corr)
        call iter_man%add_iter(rel_norm_corr)
      elseif (m_p_strategy == 'fieldsplit') then
        call solve_NS_coupled_sys(mesh,dt,m_solver,rel_diff_sol)
        call iter_man%add_iter(rel_diff_sol)
      else
        call error('unknown m_p_strategy=' // m_p_strategy)
      endif
      if (iter_man%finished()) exit
    enddo
    if (I_am_master_process()) call iter_man%print_errors()
  end subroutine
  
  subroutine init_h_solver(engy_qnty, h_solver)
    ! Sets up the type of equation, and the linear solver tolerances, but does not 
    ! set the options in the solver.
    character(*), intent(in) :: engy_qnty
    type(h_solver_t), target, intent(out) :: h_solver
    
    h_solver%qnty = engy_qnty
    call h_solver%set_solver_specs(expect_symm=symm_of_qnty_linsys(engy_qnty))
  end subroutine
  
  subroutine init_turb_solver(equation,turb_solver)
    use turb_quantities, only: turb_solver_t
    
    character(*), intent(in) :: equation
    class(turb_solver_t), target, intent(out) :: turb_solver
    
    character(9) :: qnty
    
    turb_solver%equation = equation
    qnty = merge('vol_turb1','vol_turb2',equation == '1st_eq')
    turb_solver%qnty = qnty
    call turb_solver%set_solver_specs(expect_symm=symm_of_qnty_linsys(qnty))
  end subroutine init_turb_solver
  
  
  
  subroutine process_quantities(mesh)
    ! Processes the quantities after the new solvecs are computed.
    ! Updates avergares, printing diagnostic information.
    use div_cont_penalties_and_err, only: output_div_and_continuity_errors
    use face_integrals_mod, only: write_my_face_integrals
    use run_data, only: calc_cfl, steps_per_group_avg, print_div_and_continuity_errs
    use vol_averages_mod, only: update_averages

    type(fem_mesh), intent(in) :: mesh
    
    if (mod(nold_times,steps_per_group_avg) == 0) call update_averages(mesh,old_times(1))
    if (solve_for_mtum_and_press() .and. print_div_and_continuity_errs > 0) then
      if (mod(nold_times,print_div_and_continuity_errs) == 0) call output_div_and_continuity_errors(mesh)
    endif
    if (solve_for_mtum_and_press() .and. calc_cfl > 0) then
      if (mod(nold_times,calc_cfl) == 0) call print_max_cfl(mesh)
    endif
    if (mod(nold_times,5) == 0) call write_my_face_integrals()
  end subroutine process_quantities
  
  
  subroutine print_max_cfl(mesh)
    use characteristic_len_mod, only: anisotropic_charact_len
    use exceptions, only: warning
    use math_const, only: SMALL
    use mesh_objects, only: elem_id, elem_list
    use mesh_partitioning, only: my_first_last_elems
    use mpi_wrappers_mod, only: reduce_wrap
    use numerical_solutions, only: get_dof_handler, velocity
    use petsc_mod, only: petsc_proc_id
    use vec_func_mod, only: vec_func_type
    
    type(fem_mesh), intent(in) :: mesh
    
    integer, parameter :: TIME = 1
    integer :: elem_no, first_elem, last_elem
    type(elem_id) :: id
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_list), pointer :: elem
    real(dp), dimension(mesh%dimen) :: length_scale, U_avg, flowthrough_times, local_cfl, my_max_cfl, max_cfl
    
    call mesh%get_active_elem_list_ptr(.true.,active_elem_list)
    call my_first_last_elems(.true.,first_elem,last_elem)
    
    my_max_cfl = - huge(1.0_dp)
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      call velocity%eval_avg_value(elem,TIME,U_avg)
      length_scale = anisotropic_charact_len(elem,qnty='mass_flux',len_specs='diff_maxmin_vtx',order_specs='quadratic')
      flowthrough_times = length_scale / max(abs(U_avg),SMALL)
      local_cfl = dt / flowthrough_times
      my_max_cfl = max(my_max_cfl,local_cfl)
    enddo
    call reduce_wrap(my_max_cfl,'max',0,max_cfl)

    if (petsc_proc_id() == 0) write(*,'(a,3es10.3)') 'max CFL = ', max_cfl
  end subroutine print_max_cfl
end module time_step_mod
