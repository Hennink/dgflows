module assertions
  ! Program defensively: use assertions whenever you can verify that the input
  ! of a procedure is sensible. This is especially important when reading 
  ! input, or interfacing with external software.
  ! 
  ! If the assertion is expensive, and the procedure performance-critical, 
  ! then add the `BOUNDS_CHECK` argument, which allows the preprocessor to 
  ! factor out the assertion.
  ! 
  ! Make sure that the first actual argument in an `assert_eq` function is the
  ! cheapest to evaluate.
  ! 
  use, intrinsic :: ieee_arithmetic, only: ieee_is_normal
  use f90_kind, only: dp
  implicit none
  
  private
  public :: assert
  public :: assert_small
  public :: assert_eq
  public :: assert_normal
  public :: BOUNDS_CHECK

  public :: perform_all_bc

  type :: bounds_check_t
  end type
  type(bounds_check_t), parameter :: BOUNDS_CHECK = bounds_check_t()

  logical, parameter :: perform_all_bc = &
#ifdef INTERNAL_BOUNDS_CHECKS
      .true.
#else
      .false.
#endif

  interface assert
    module procedure assert_l_msg
    module procedure assert_l
    module procedure assert_ll
    module procedure assert_l_rank1_msg
  end interface
  
  interface assert_eq
    module procedure assert_eq_c_rank1
    module procedure assert_eq_ll
    module procedure assert_eq_ll_msg
    module procedure assert_eq_l_rank1
    module procedure assert_eq_l_rank1_msg
    module procedure assert_eq_ii
    module procedure assert_eq_ii_msg
    module procedure assert_eq_iii
    module procedure assert_eq_iiii
    module procedure assert_eq_iiiii
    module procedure assert_eq_i_rank1
    module procedure assert_eq_i_rank1_msg
    module procedure assert_eq_r_rank1_msg
  end interface
  
  interface assert_normal
    module procedure assert_normal_r
    module procedure assert_normal_r_msg
  end interface
  
contains
  subroutine assert_l_msg(statement,msg,this_is_a_bc)
    use exceptions, only: error

    logical, intent(in) :: statement
    character(*), intent(in) :: msg
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

#ifdef ASSERTIONS
    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      if (.not. statement) call error('ASSERTION ERROR: ' // msg)
    endif
#endif
  end subroutine

  subroutine assert_l(n1,this_is_a_bc)
    logical, intent(in) :: n1
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert_l_msg(n1,'')
    endif
  end subroutine
  
  subroutine assert_ll(n1,n2,this_is_a_bc)
    logical, intent(in) :: n1,n2
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert(n1,'(arg # 1)')
      call assert(n2,'(arg # 1)')
    endif
  end subroutine
  
  subroutine assert_l_rank1_msg(nn,msg,this_is_a_bc)
    logical, intent(in) :: nn(:)
    character(*), intent(in) :: msg
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert(all(nn),msg)
    endif
  end subroutine

  impure elemental subroutine assert_small(value,msg)
    use math_const, only: SMALL

    real(dp), intent(in) :: value
    character(*), intent(in) :: msg

    call assert(abs(value) <= SMALL,msg)
  end subroutine


  function assert_eq_c_rank1(chars,this_is_a_bc) result(value)
    character(*), intent(in) :: chars(:)
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc
    character(len(chars)) :: value

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert(size(chars) >= 1,'(assert_eq_c_rank1): need at least one entry')
      call assert(chars(1) == chars(2:),'(assert_eq_c_rank1)')
    endif
    value = chars(1)
  end function

  logical function assert_eq_ll(l1,l2,this_is_a_bc) result(value)
    logical, intent(in) :: l1, l2
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert(l1 .eqv. l2, '(assert_eq_ll)')
    endif
    value = l1
  end function

  logical function assert_eq_ll_msg(l1,l2,msg,this_is_a_bc) result(value)
    logical, intent(in) :: l1, l2
    character(*), intent(in) :: msg
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert(l1 .eqv. l2, msg)
    endif
    value = l1
  end function

  logical function assert_eq_l_rank1_msg(bools,msg,this_is_a_bc) result(val)
    logical, intent(in) :: bools(:)
    character(*), intent(in) :: msg
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert(size(bools) >= 1,msg // ' (size(bools)==0)')
      call assert(bools(1) .eqv. bools(2:),msg)
    endif
    val = bools(1)
  end function

  integer function assert_eq_ii(n1,n2,this_is_a_bc) result(value)
    integer, intent(in) :: n1, n2
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert(n1 == n2,'(assert_eq_ii)')
    endif
    value = n1
  end function
  
  integer function assert_eq_ii_msg(n1,n2,msg,this_is_a_bc) result(value)
    integer, intent(in) :: n1, n2
    character(*), intent(in) :: msg
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert(n1 == n2,msg)
    endif
    value = n1
  end function
  
  integer function assert_eq_iii(n1,n2,n3,this_is_a_bc) result(value)
    integer, intent(in) :: n1, n2, n3
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert(n1 == n2)
      call assert(n1 == n3)
    endif
    value = n1
  end function
  
  integer function assert_eq_iiii(n1,n2,n3,n4,this_is_a_bc) result(value)
    integer, intent(in) :: n1, n2, n3, n4
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert(n1 == n2)
      call assert(n1 == n3)
      call assert(n1 == n4)
    endif
    value = n1
  end function
  
  integer function assert_eq_iiiii(n1,n2,n3,n4,n5,this_is_a_bc) result(value)
    integer, intent(in) :: n1, n2, n3, n4, n5
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert(n1 == n2)
      call assert(n1 == n3)
      call assert(n1 == n4)
      call assert(n1 == n5)
    endif
    value = n1
  end function

  logical function assert_eq_l_rank1(bools) result(value)
    logical, intent(in) :: bools(:)

    call assert(size(bools) >= 1, "assert_eq_l_rank1: did not expect empty array", BOUNDS_CHECK)
    call assert(bools(1) .eqv. bools(2:),'(assert_eq_l_rank1)')
    value = bools(1)
  end function

  integer function assert_eq_i_rank1(ints,this_is_a_bc) result(value)
    integer, intent(in) :: ints(:)
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    value = assert_eq_i_rank1_msg(ints,'',this_is_a_bc)
  end function

  integer function assert_eq_i_rank1_msg(ints,msg,this_is_a_bc) result(value)
    integer, intent(in) :: ints(:)
    character(*), intent(in) :: msg
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert(size(ints) >= 1, '`assert_eq` need at least one entry. msg=' // msg)
      call assert(ints(1) == ints(2:), msg)
    endif
    value = ints(1)
  end function

  real(dp) function assert_eq_r_rank1_msg(reals,msg,this_is_a_bc) result(avg)
    real(dp), intent(in) :: reals(:)
    character(*), intent(in) :: msg
    type(bounds_check_t), optional, intent(in) :: this_is_a_bc

    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert(size(reals) >= 1,msg // ' (size(reals)==0)')
    endif
    avg = sum(reals) / size(reals)
    if (perform_all_bc .or. .not. present(this_is_a_bc)) then
      call assert_small(abs(reals - avg),msg)
    endif
  end function

  
  impure elemental subroutine assert_normal_r(value)
    real(dp), intent(in) :: value
    
    call assert_normal(value,'not a IEEE normal number')
  end subroutine
  
  impure elemental subroutine assert_normal_r_msg(value,msg)
    real(dp), intent(in) :: value
    character(*), intent(in) :: msg
    
    call assert(ieee_is_normal(value),msg)
  end subroutine assert_normal_r_msg
end module

