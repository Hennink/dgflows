module mansol_mod
  use assertions, only: assert
  use func_set_mod, only: func_set_t
  use petsc_mod, only: I_am_master_process
  implicit none
  
  private
  public :: read_mansol_set
  public :: compare_solution_with_mansol

  type(func_set_t), public, protected :: mansol_set
  
contains
  subroutine read_mansol_set()
    use functionals, only: continuous_rel_l2_diff
    use matprop_mod, only: expand_func_set_with_matprop
    use read_funcs_mod, only: read_block_as_func_specs
    use run_data, only: mansol_specs_file
      
    integer :: unit, iostat
    character(200) :: iomsg
    
    if (I_am_master_process()) print *, 'reading mansol set ...'
    open(newunit=unit, file=mansol_specs_file, &
         status='old', access='sequential', form='formatted', action='read', position='rewind', &
         iostat=iostat, iomsg=iomsg)
    call assert(iostat==0,iomsg)
      call read_block_as_func_specs(unit,mansol_set)
    close(unit,iostat=iostat,iomsg=iomsg)
    call assert(iostat==0,iomsg)
    call expand_func_set_with_matprop(mansol_set)
  end subroutine read_mansol_set

  subroutine compare_solution_with_mansol(mesh)
    use exceptions, only: warning
    use f90_kind, only: dp
    use functionals, only: continuous_rel_l2_diff
    use io_basics, only: REAL_FMT
    use mesh_type, only: fem_mesh
    use numerical_solutions, only: matprop_set, num_scalfunc_set, velocity, mass_flux
    use quantities_mod, only: fluid_name
    use scal_func_mod, only: scal_func_type
    use string_mod, only: string_t
    use vec_func_mod, only: vec_func_type
      
    type(fem_mesh), intent(in) :: mesh
    
    logical, parameter :: ONLY_FLUID = .true.
    type(string_t), allocatable :: names(:)
    character(:), allocatable :: name
    integer :: i
    class(vec_func_type), allocatable :: numfunc, exfunc
    real(dp) :: error
    
    if (I_am_master_process()) then
      print *, '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
      print *, '     Compare numerical solution with mansol      '
      write(*,'(a20,a)') 'quantity:', '    relative L2-error:'
    endif
    
    call mansol_set%get_names(names)
    do i = 1, size(names)
      name = names(i)%get()
      if (name == 'velocity') then
        allocate(numfunc,source=velocity)
      elseif(name == 'mass_flux') then
        allocate(numfunc,source=mass_flux)
      elseif (num_scalfunc_set%has(name)) then
        call num_scalfunc_set%get_func(name,numfunc)
      elseif (matprop_set%has(name)) then
        call matprop_set%get_func(name,numfunc)
      else
        call warning('No numerical result with which to compare the mansol! qnty=' // name)
      endif
      call mansol_set%get_func(name,exfunc)

      error = continuous_rel_l2_diff(mesh,ONLY_FLUID,new=exfunc,time_new=2,old=numfunc,time_old=2,bcast=.false.)
      if (I_am_master_process()) write(*,'(a20,'//REAL_FMT//')') name, error
    enddo

    if (I_am_master_process()) print *, '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
  end subroutine compare_solution_with_mansol
end module mansol_mod
