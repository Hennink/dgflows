module spec_h_implicit_time_mod
  use f90_kind, only: dp
  use pde_term_mod, only: pde_term_t
  use scal_func_mod, only: scal_func_type
  implicit none

  private
  public :: spec_h_implicit_time_t

  type, extends(pde_term_t) :: spec_h_implicit_time_t
    real(dp) :: weight
    character(:), allocatable :: strategy
    class(scal_func_type), pointer ::  newrho_predictor => null()
  contains
    procedure, non_overridable :: process_elem
    procedure, non_overridable :: ngbr_coupling_depth
  end type

contains
  pure integer function ngbr_coupling_depth(this)
    class(spec_h_implicit_time_t), intent(in) :: this

    ngbr_coupling_depth = 0
  end function

  subroutine process_elem(this,elem,qnty,local_linsys)
    use assertions, only: assert
    use exceptions, only: error
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: elem_list
    use mansol_mod, only: mansol_set
    use numerical_solutions, only: matprop_set, num_scalfunc_set
    use run_data, only: constant_density, mansol_specs_file
    use solvec_mod, only: solvec_t
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t

    class(spec_h_implicit_time_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys

    class(scal_func_type), pointer :: rho_func, spec_h_func, ddh_rho_func
    real(dp), dimension(elem%nqp) :: rho, spec_h, ddh_rho
    real(dp), dimension(elem%nqp) :: explicit, implicit
    type(unidir_lincom_rank0_t) :: func

    integer, parameter :: time_predictor = 1

    call func%set_as_func(elem,qnty%handler)

    if (constant_density .or. .not. elem%is_fluid()) then
      ! There is some overhead here, because the density is very probably 
      ! constant in space, so we could have used the stored mass matrix.
      rho_func => matprop_set%func_ptr('density')
      rho = rho_func%values_at_elem_qps(elem,time_predictor)
      call local_linsys%add_inprod_to_lhs_all_dirs(func,func,rho*elem%quot,.false.,this%weight)
    
    elseif (this%strategy == 'deriv_rho_h') then
      rho_func => matprop_set%func_ptr('density')
      spec_h_func => num_scalfunc_set%func_ptr('spec_enthalpy')
      ddh_rho_func => matprop_set%func_ptr('deriv_rho_h')
      rho = rho_func%values_at_elem_qps(elem,time_predictor)
      spec_h = spec_h_func%values_at_elem_qps(elem,time_predictor)
      ddh_rho = ddh_rho_func%values_at_elem_qps(elem,time_predictor)
      implicit = rho + ddh_rho * spec_h
      explicit = ddh_rho * spec_h**2
      call assert(implicit > 0,'deriv_rho_h: implicit weight should probablybe strictly positive')
      call local_linsys%add_inprod_to_lhs_all_dirs(func,func,implicit*elem%quot,.false.,this%weight)
      call local_linsys%add_inprod_to_rhs_all_dirs(func,explicit,elem%quot,this%weight)
    
    elseif (this%strategy == 'rho_predictor') then
      call assert(associated(this%newrho_predictor),'newrho_predictor is not set')
      rho_func => this%newrho_predictor
      rho = rho_func%values_at_elem_qps(elem,time_predictor)
      call local_linsys%add_inprod_to_lhs_all_dirs(func,func,rho*elem%quot,.false.,this%weight)

    elseif (this%strategy == 'mansol_rho') then
      call assert(mansol_specs_file /= '','The "mansol_rho" strategy applies to manufactured solutions.')
      rho_func => mansol_set%func_ptr('density')
      rho = rho_func%values_at_elem_qps(elem,time=1)
      call local_linsys%add_inprod_to_lhs_all_dirs(func,func,rho*elem%quot,.false.,this%weight)
    
    else
      call error('spec_h_implicit_time: unknown strategy=' // this%strategy)
    endif
  end subroutine process_elem
end module spec_h_implicit_time_mod
