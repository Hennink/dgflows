module les_heat_flux_mod
  use assertions, only: assert, BOUNDS_CHECK
  use exceptions, only: error
  use f90_kind, only: dp
  use mesh_type, only: fem_mesh
  use numerical_solutions, only: matprop_set
  use scal_func_mod, only: scal_func_type
  use scalfunc_from_proc_mod, only: scalfunc_from_proc_t
  implicit none

  private
  public :: read_les_enthalpy_model_from_file
  public :: recalc_turb_Pr
  public :: write_heat_flux_model_to_gmsh

  character(:), allocatable :: les_hf_model

  type(scalfunc_from_proc_t), allocatable, protected, public :: k_over_cp_sfs

  ! This should be called before solving for the enthalpy, to give LES models
  ! a chance to precalculate some stuff like averages:
  procedure(do_nothing), pointer :: recalc_turb_Pr => null()
  
  real(dp) :: const_turb_Pr

contains
  subroutine read_les_enthalpy_model_from_file(unit,mesh)
    use func_operations_mod
    use functions_lib, only: const_func
    use io_basics, only: read_line_from_input_file
    use les_dyn_turb_pr_mod, only: dyn_sfs_k_over_cp, init_dyn_turb_pr_from_file, &
                                   recalc_avg_LM_MM_eqv_elems
    use scalfunc_from_proc_mod, only: as_scalfunc

    integer, intent(in) :: unit
    type(fem_mesh), intent(in) :: mesh

    namelist /const_turb_Pr_params/ const_turb_pr
    integer :: iostat
    character(200) :: iomsg

    call read_line_from_input_file(unit,les_hf_model)
    allocate(k_over_cp_sfs)
    select case(les_hf_model)
      case('const_turb_Pr')
        read(unit,const_turb_Pr_params,iostat=iostat,iomsg=iomsg)
        call assert(iostat==0,iomsg)
        k_over_cp_sfs%proc => const_sfs_k_over_cp
        recalc_turb_Pr => do_nothing

      case('dyn_turb_Pr')
        call init_dyn_turb_pr_from_file(unit,mesh)
        k_over_cp_sfs%proc => dyn_sfs_k_over_cp
        recalc_turb_Pr => recalc_avg_LM_MM_eqv_elems

      case('')
        call error('read_les_enthalpy_model_from_file: found empty line where I expected les_hf_model')
      
      case default
        call error('read_les_enthalpy_model_from_file: unknown les_hf_model=' // les_hf_model)
    end select
  end subroutine read_les_enthalpy_model_from_file

  subroutine write_heat_flux_model_to_gmsh(mesh)
    use func_operations_mod
    use gmsh_interface, only: write_to_gmsh_file
    use les_dyn_turb_pr_mod, only: write_dyn_turb_pr_to_gmsh

    type(fem_mesh), intent(in) :: mesh

    class(scal_func_type), pointer :: k_over_cp_mol

    k_over_cp_mol => matprop_set%func_ptr('k_over_cp')
    call write_to_gmsh_file(k_over_cp_sfs/k_over_cp_mol,mesh,'rel_k_over_cp',only_fluid=.true.,time=1)

    select case(les_hf_model)
      case('const_turb_Pr')
      case('dyn_turb_Pr');    call write_dyn_turb_pr_to_gmsh(mesh)
      case default;           call error('write_heat_flux_model_to_gmsh: unknown LES model')
    end select
  end subroutine write_heat_flux_model_to_gmsh

  subroutine const_sfs_k_over_cp(elem,pnt_set,time,values)
    use les_eddy_viscosity_mod, only: eddy_kin_viscosity
    use mesh_objects, only: elem_list
    use pnt_set_mod, only: pnt_set_t

    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)

    class(scal_func_type), pointer :: rho_func
    real(dp) :: rho(size(values)), nu_sfs(size(values))

    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)

    rho_func => matprop_set%func_ptr('density')
    call rho_func%get_values(elem,pnt_set,time,rho)
    call eddy_kin_viscosity%get_values(elem,pnt_set,time,nu_sfs)
    values = rho * nu_sfs / const_turb_Pr
  end subroutine const_sfs_k_over_cp

  subroutine do_nothing(mesh)
    type(fem_mesh), intent(in) :: mesh
  end subroutine do_nothing
end module les_heat_flux_mod
