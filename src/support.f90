module support
  use, intrinsic :: iso_fortran_env, only: REAL128
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use exceptions, only: error
  use f90_kind, only: dp
  implicit none
  
  private
  public :: cartesian_unit_vector
  public :: cross_product
  public :: axpy_own_trace_x_I
  public :: eigenvals_symm, determinant, determinant_symm, trace, minmod
  public :: double_dot_operator
  public :: identity_matrix
  public :: iseq
  public :: linspace, range
  public :: bool_i, false, true, int_i, negative, zero, one
  
  public :: trace_power_symm ! from included auto-generated code

  abstract interface
    pure logical function bool_i()
      implicit none
    end function bool_i

    pure integer function int_i()
      implicit none
    end function
  end interface

  interface double_dot_operator
    module procedure double_dot_operator_2x3
    module procedure double_dot_operator_3x3
  end interface double_dot_operator

contains
include "../utils/trace_powers.f90"

  pure logical function false()
    false = .false.
  end function false
  
  pure logical function true()
    true = .true.
  end function true

  pure integer function negative()
    negative = -1
  end function

  pure integer function zero()
    zero = 0
  end function

  pure integer function one()
    one = 1
  end function

  function linspace(x_min,x_max,no_points)
    ! Returns evenly spaced points in [x_min,x_max], including the ends.
    real(dp), intent(in) :: x_min, x_max
    integer, intent(in) :: no_points
    real(dp), dimension(no_points) :: linspace

    integer :: i

    call assert(no_points >= 2,'linspace: need at least two points')

    do i = 1, no_points
      linspace(i) = x_min + (x_max - x_min) * real(i-1,dp) / real(no_points-1,dp)
    enddo
  end function linspace
  
  function cartesian_unit_vector(dimen,dir) result(vec)
    integer, intent(in) :: dimen
    integer, intent(in) :: dir
    real(dp), dimension(dimen) :: vec
    
    real(dp), dimension(3), parameter :: e_x = [1.0_dp, 0.0_dp, 0.0_dp]
    real(dp), dimension(3), parameter :: e_y = [0.0_dp, 1.0_dp, 0.0_dp]
    real(dp), dimension(3), parameter :: e_z = [0.0_dp, 0.0_dp, 1.0_dp]
    
    select case(dir)
      case(1)
        vec = e_x(1:dimen)
      case(2)
        call assert(dimen > 1)
        vec = e_y(1:dimen)
      case(3)
        call assert(dimen > 2)
        vec = e_z(1:dimen)
      case default
        call error()
    end select
  end function cartesian_unit_vector
  
  subroutine axpy_own_trace_x_I(mat,alpha)
    ! mat += alpha * trace(mat) * I .
    ! Pass `alpha = -1/3` to transform `mat` into its deviatoric part.
    real(dp), intent(inout) :: mat(:,:)
    real(dp), intent(in) :: alpha

    real(dp) :: alpha_x_trace
    integer :: i

    alpha_x_trace = alpha * trace(mat)
    do i = 1, assert_eq(size(mat,1),size(mat,2))
      mat(i,i) = mat(i,i) + alpha_x_trace
    enddo
  end subroutine axpy_own_trace_x_I

  pure function cross_product(vec1,vec2) result(cp)
    real(dp), dimension(3), intent(in) :: vec1
    real(dp), dimension(3), intent(in) :: vec2
    real(dp), dimension(3) :: cp
    
    cp(1) = vec1(2)*vec2(3) - vec1(3)*vec2(2)
    cp(2) = vec1(3)*vec2(1) - vec1(1)*vec2(3)
    cp(3) = vec1(1)*vec2(2) - vec1(2)*vec2(1)
  end function cross_product
  
  real(dp) function trace(matrix)
    real(dp), intent(in) :: matrix(:,:)
    
    integer :: n, i

    n = assert_eq(size(matrix,1),size(matrix,2),BOUNDS_CHECK)
    trace = 0.0_dp
    do i = 1, n
      trace = trace + matrix(i,i)
    enddo
  end function trace

  subroutine eigenvals_symm(A, ev)
    ! `A` is assumed symmetric, and gets destroyed by this routine.
    use lapack95, only: syevd

    real(dp), intent(inout) :: A(:,:)
    real(dp), intent(out) :: ev(:)

    integer :: info

    call assert(size(A,1) == size(A,2), size(A,2) == size(ev), BOUNDS_CHECK)

    call syevd(A, w=ev, jobz='N', uplo='L', info=info) ! `jobz='V'` to get the eigenvectors as well
    call assert(info == 0, "syevd: info /= 0", BOUNDS_CHECK)
  end subroutine
  
  real(dp) function determinant(A)
    real(dp), intent(in) :: A(:,:)

    integer :: n

    n = assert_eq(size(A,1),size(A,2), BOUNDS_CHECK)

    ! There's no simple general algorithm.
    select case(n)
      case(0)
        ! https://en.wikipedia.org/wiki/Matrix_(mathematics)#Empty_matrices
        determinant = 1
      case(1)
        determinant = A(1,1)
      case(2)
        determinant = A(1,1)*A(2,2) - A(1,2)*A(2,1)
      case(3)
        determinant =   A(1,1)*A(2,2)*A(3,3) - A(1,1)*A(2,3)*A(3,2) &
                      - A(1,2)*A(2,1)*A(3,3) + A(1,2)*A(2,3)*A(3,1) &
                      + A(1,3)*A(2,1)*A(3,2) - A(1,3)*A(2,2)*A(3,1)
      case(4)
        determinant = + A(1,1) * (   A(2,2)*(A(3,3)*A(4,4)-A(3,4)*A(4,3))   &
                                   + A(2,3)*(A(3,4)*A(4,2)-A(3,2)*A(4,4))   &
                                   + A(2,4)*(A(3,2)*A(4,3)-A(3,3)*A(4,2)) ) &
                      - A(1,2) * (   A(2,1)*(A(3,3)*A(4,4)-A(3,4)*A(4,3))   &
                                   + A(2,3)*(A(3,4)*A(4,1)-A(3,1)*A(4,4))   &
                                   + A(2,4)*(A(3,1)*A(4,3)-A(3,3)*A(4,1)) ) &
                      + A(1,3) * (   A(2,1)*(A(3,2)*A(4,4)-A(3,4)*A(4,2))   &
                                   + A(2,2)*(A(3,4)*A(4,1)-A(3,1)*A(4,4))   &
                                   + A(2,4)*(A(3,1)*A(4,2)-A(3,2)*A(4,1)) ) &
                      - A(1,4) * (  A(2,1)*(A(3,2)*A(4,3)-A(3,3)*A(4,2))    &
                                   + A(2,2)*(A(3,3)*A(4,1)-A(3,1)*A(4,3))   &
                                   + A(2,3)*(A(3,1)*A(4,2)-A(3,2)*A(4,1)) )
      case default
        call error('NOT IMPLEMENTED')
    end select
  end function determinant
  
  real(dp) function determinant_symm(S) result(determinant)
    ! Determinant of a symmetric matrix `S`; only the lower triangular part is referenced.
    real(dp), intent(in) :: S(:,:)

    integer :: n

    n = assert_eq(size(S,1), size(S,2), BOUNDS_CHECK)

    select case(n)
      case(0)
        ! https://en.wikipedia.org/wiki/Matrix_(mathematics)#Empty_matrices
        determinant = 1
      case(1)
        determinant = S(1,1)
      case(2)
        determinant = S(1,1)*S(2,2) - S(2,1)*S(2,1)
      case(3)
        determinant =   S(1,1)*S(2,2)*S(3,3) - S(1,1)*S(3,2)*S(3,2) &
                      - S(2,1)*S(2,1)*S(3,3) + S(2,1)*S(3,2)*S(3,1) &
                      + S(3,1)*S(2,1)*S(3,2) - S(3,1)*S(2,2)*S(3,1)
      case(4)
        determinant = + S(1,1) * (   S(2,2)*(S(3,3)*S(4,4)-S(4,3)*S(4,3))   &
                                   + S(3,2)*(S(4,3)*S(4,2)-S(3,2)*S(4,4))   &
                                   + S(4,2)*(S(3,2)*S(4,3)-S(3,3)*S(4,2)) ) &
                      - S(2,1) * (   S(2,1)*(S(3,3)*S(4,4)-S(4,3)*S(4,3))   &
                                   + S(3,2)*(S(4,3)*S(4,1)-S(3,1)*S(4,4))   &
                                   + S(4,2)*(S(3,1)*S(4,3)-S(3,3)*S(4,1)) ) &
                      + S(3,1) * (   S(2,1)*(S(3,2)*S(4,4)-S(4,3)*S(4,2))   &
                                   + S(2,2)*(S(4,3)*S(4,1)-S(3,1)*S(4,4))   &
                                   + S(4,2)*(S(3,1)*S(4,2)-S(3,2)*S(4,1)) ) &
                      - S(4,1) * (  S(2,1)*(S(3,2)*S(4,3)-S(3,3)*S(4,2))    &
                                   + S(2,2)*(S(3,3)*S(4,1)-S(3,1)*S(4,3))   &
                                   + S(3,2)*(S(3,1)*S(4,2)-S(3,2)*S(4,1)) )
      case default
        call error('NOT IMPLEMENTED')
    end select
  end function
  
  function double_dot_operator_2x3(A,B) result(A_colon_B)
    ! [A_colon_B]_{m} = [A]_{ij} [B]_{ijm}    (Einstein notation)
    ! This is basically a double dot operator. A and B need not truly be tensors.
    real(dp), dimension(:,:), intent(in) :: A
    real(dp), dimension(:,:,:), intent(in) :: B
    real(dp), dimension(size(B,3)) :: A_colon_B
    
    integer :: m
    
    call assert(size(A,1) == size(B,1),BOUNDS_CHECK)
    call assert(size(A,2) == size(B,2),BOUNDS_CHECK)
    
    do m = 1, size(B,3)
      A_colon_B(m) = A_colon_B(m) + sum(A(:,:) * B(:,:,m))
    enddo
  end function double_dot_operator_2x3
  
  function double_dot_operator_3x3(A,B) result(A_colon_B)
    ! [A_colon_B]_{mn} = [A]_{mij} [B]_{ijn}    (Einstein notation)
    ! This is basically a double dot operator. A and B need not truly be tensors.
    real(dp), dimension(:,:,:), intent(in) :: A
    real(dp), dimension(:,:,:), intent(in) :: B
    real(dp), dimension(size(A,1),size(B,3)) :: A_colon_B
    
    integer :: m, n
    
    call assert(size(A,2) == size(B,1),BOUNDS_CHECK)
    call assert(size(A,3) == size(B,2),BOUNDS_CHECK)
    
    do n = 1, size(B,3)
      do m = 1, size(A,1)
        A_colon_B(m,n) = A_colon_B(m,n) + sum(A(m,:,:) * B(:,:,n))
      enddo
    enddo
  end function double_dot_operator_3x3
  
  pure function identity_matrix(N)
    integer, intent(in) :: N
    real(dp), dimension(N,N) :: identity_matrix
    
    integer :: i
    
    identity_matrix = 0.0_dp
    do i = 1, N
      identity_matrix(i,i) = 1.0_dp
    enddo
  end function identity_matrix
  
  elemental logical function iseq(v1,v2,tolerance)
    real(dp), intent(in):: v1, v2
    real(dp), optional, intent(in) :: tolerance
    
    real(dp), parameter :: SMALL = sqrt(epsilon(1.0_dp))
    real(dp), parameter :: DEFAULT_TOLERANCE = sqrt(epsilon(1.0_dp))
    real(dp) :: used_tolerance
    real(dp) :: min_abs, abs_diff
    
    if (present(tolerance)) then
      used_tolerance = tolerance
    else
      used_tolerance = DEFAULT_TOLERANCE
    endif
    
    min_abs = min(abs(v1),abs(v2))
    abs_diff = abs(v1-v2)
    if (min_abs <= SMALL) then
      iseq = abs_diff <= SMALL
    else
      iseq = abs_diff / min_abs <= used_tolerance
    endif
  end function iseq
  
  pure real(dp) function minmod(a1_to_aN)
    ! Minmod function (used in slope limiting procedures):
    !   minmod = sign(a1) * min(abs(a_i)) if all signs equal
    !   minmod = 0 otherwise
    real(dp), dimension(:), intent(in) :: a1_to_aN
    
    logical :: all_same_sign
    integer :: sign_a1
    
    all_same_sign = all(a1_to_aN >= 0) .or. all(a1_to_aN < 0)
    if (all_same_sign) then
      sign_a1 = merge(1,-1,a1_to_aN(1) >= 0)
      minmod = sign_a1 * minval(abs(a1_to_aN))
    else
      minmod = 0.0_dp
    endif
  end function minmod
  
  pure subroutine range(length,vector)
    integer, intent(in) :: length
    integer, allocatable, intent(out) :: vector(:)
    
    integer :: i
    
    allocate(vector(length))
    do i = 1, length
      vector(i) = i
    enddo
  end subroutine range
end module support

