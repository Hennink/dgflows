module f90_kind
  use, intrinsic :: iso_fortran_env, only: real64
  implicit none
  
  private
  public :: dp
  
  integer, parameter :: dp = real64
end module f90_kind

