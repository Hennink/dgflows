module scalfunc_from_proc_mod
  use scal_func_mod, only: scal_func_type
  implicit none

  private
  public :: as_scalfunc, scalfunc_from_proc_t

  type, extends(scal_func_type) :: scalfunc_from_proc_t
    procedure(values_at_pnt_set_i), pointer, nopass :: proc => null()
  contains
    procedure :: get_values => rerout_to_proc_ptr
  end type scalfunc_from_proc_t

  abstract interface
    subroutine values_at_pnt_set_i(elem,pnt_set,time,values)
      use f90_kind, only: dp
      use mesh_objects, only: elem_list
      use pnt_set_mod, only: pnt_set_t
      implicit none

      type(elem_list), intent(in) :: elem
      type(pnt_set_t), intent(in) :: pnt_set
      integer, intent(in) :: time
      real(dp), intent(out) :: values(:)
    end subroutine values_at_pnt_set_i
  end interface

contains
  type(scalfunc_from_proc_t) function as_scalfunc(proc)
    procedure(values_at_pnt_set_i) :: proc

    as_scalfunc%proc => proc
  end function

  subroutine rerout_to_proc_ptr(this,elem,pnt_set,time,values)
    use f90_kind, only: dp
    use mesh_objects, only: elem_list
    use pnt_set_mod, only: pnt_set_t

    class(scalfunc_from_proc_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)

    call this%proc(elem,pnt_set,time,values)
  end subroutine rerout_to_proc_ptr
end module scalfunc_from_proc_mod

