submodule(boundary_types) boundary_types__noslip_internal_wall
  ! This boundary type is necessary for conjugate heat transfer 
  ! calculations. On internal walls, we must specify only the velocity
  ! value, because they are not boundaries for temperature or enthalpy.
  use vec_func_mod, only: vec_func_type
  implicit none
  
  type, extends(wall_bnd_t) :: noslip_internal_wall_t
    class(vec_func_type), allocatable :: velocity
  contains
    procedure :: read_specs_from_file
    procedure :: get_bc
  end type noslip_internal_wall_t
  
contains
  module procedure noslip_internal_wall
    allocate(noslip_internal_wall_t :: bnd)
  end procedure noslip_internal_wall
  
  subroutine read_specs_from_file(this,unit_number)
    use read_funcs_mod, only: read_line_as_qnty_eq_func
    
    class(noslip_internal_wall_t), intent(out) :: this
    integer, intent(in) :: unit_number
    
    call read_line_as_qnty_eq_func(unit_number,'velocity',this%velocity)
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types, only: bc_type, dirichlet_bc_type
    use func_operations_mod, only: operator(*)
    use functions_lib, only: zero_vecfunc
    use numerical_solutions, only: matprop_set
    use scal_func_mod, only: scal_func_type
    
    class(noslip_internal_wall_t), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    class(scal_func_type), allocatable :: rho

    type(dirichlet_bc_type), allocatable :: dirichlet_bc
    
    select case(qnty_name)
      case('mass_flux','velocity')
        allocate(dirichlet_bc)
        call matprop_set%get_func('density',rho)
        allocate(dirichlet_bc%inhom_part,source= rho * this%velocity)
        call move_alloc(dirichlet_bc,bc)
      case('wall_dist')
        allocate(dirichlet_bc)
        allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(1))
        call move_alloc(dirichlet_bc,bc)
      case default
        write(*,'(a)') 'ERROR: no bc for '//qnty_name//' in noslip_internal_wall'
        error stop
    end select
  end subroutine get_bc
end submodule boundary_types__noslip_internal_wall
