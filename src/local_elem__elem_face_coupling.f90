submodule(local_elem) local_elem__elem_face_coupling
  implicit none
  
contains
  module pure subroutine convert_face_to_vol_local_coord(elem_eltype,loc_idx_face,dimen,xi_face,xi_vol)
    !
    ! This routine converts the local coordinates (xi_face) to local 
    ! coordinates of the element (xi_vol).
    ! We use the fact that actual orientations do not matter as the quadrature
    ! sets are symmetric. 
    ! The only thing that matters is that there is consistency with the 
    ! neighbors which is ensured through the local -> global mapping and back
    ! in the neighbor.
    !
    use f90_kind, only: dp
    
    integer, intent(in) :: elem_eltype
    integer, intent(in) :: loc_idx_face
    integer, intent(in) :: dimen
    real(dp), dimension(dimen-1), intent(in) :: xi_face
    real(dp), dimension(dimen), intent(out) :: xi_vol
    
    select case (elem_eltype)
      case(24,28,29)
        select case(loc_idx_face)
          case(1)
            xi_vol(1) = xi_face(1)
            xi_vol(2) = -1.0_dp
          case(2)
            xi_vol(1) = +1.0_dp
            xi_vol(2) = xi_face(1)
          case(3)
            xi_vol(1) = xi_face(1)
            xi_vol(2) = +1.0_dp
          case(4)
            xi_vol(1) = -1.0_dp
            xi_vol(2) = xi_face(1)
          case default
            error stop "convert_face_to_vol_local_coord: unknown eltype/loc_idx_face combination"
        end select
      
      case(23,26)
        select case(loc_idx_face)
          case(1)
            xi_vol(1) = 1.0_dp - (xi_face(1) + 1.0_dp)/2.0_dp
            xi_vol(2) = (xi_face(1) + 1.0_dp)/2.0_dp
          case(2)
            xi_vol(1) = 0.0_dp
            xi_vol(2) = (xi_face(1) + 1.0_dp)/2.0_dp
          case(3)
            xi_vol(1) = (xi_face(1) + 1.0_dp)/2.0_dp
            xi_vol(2) = 0.0_dp
          case default
            error stop "convert_face_to_vol_local_coord: unknown eltype/loc_idx_face combination"
        end select
        
      case(38,320,327)
        select case(loc_idx_face)
          case(1)
            xi_vol(1) = xi_face(1)
            xi_vol(2) = xi_face(2)
            xi_vol(3) = -1.0_dp
          case(2)
            xi_vol(1) = xi_face(1)
            xi_vol(2) = xi_face(2)
            xi_vol(3) = +1.0_dp
          case(3)
            xi_vol(1) = xi_face(1)
            xi_vol(2) = -1.0_dp
            xi_vol(3) = xi_face(2)
          case(4)
            xi_vol(1) = +1.0_dp
            xi_vol(2) = xi_face(1)
            xi_vol(3) = xi_face(2)
          case(5)
            xi_vol(1) = xi_face(1)
            xi_vol(2) = +1.0_dp
            xi_vol(3) = xi_face(2)
          case(6)
            xi_vol(1) = -1.0_dp
            xi_vol(2) = xi_face(1)
            xi_vol(3) = xi_face(2)
          case default
            error stop "convert_face_to_vol_local_coord: unknown eltype/loc_idx_face combination"
        end select
      
      case(34,310)
        select case(loc_idx_face)
          case(1)
            xi_vol(1) = 0.0_dp
            xi_vol(2) = xi_face(1)
            xi_vol(3) = xi_face(2)
          case(2)
            xi_vol(1) = xi_face(1)
            xi_vol(2) = 0.0_dp
            xi_vol(3) = xi_face(2)
          case(3)
            xi_vol(1) = xi_face(1)
            xi_vol(2) = xi_face(2)
            xi_vol(3) = 0.0_dp
          case(4)
            xi_vol(1) = xi_face(1)
            xi_vol(2) = xi_face(2)
            xi_vol(3) = 1.0_dp - xi_face(1) - xi_face(2)
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select
      
      case default
        error stop "convert_face_to_vol_local_coord: unknown eltype"
    end select
  end subroutine convert_face_to_vol_local_coord
  
  module pure subroutine loc_idx_of_face_vertices_in_elem(eltype, loc_idx_face, loc_idx_vertices)
    use mesh_objects, only: eltype_of_faces_of_elem
    
    integer, intent(in) :: eltype
    integer, intent(in) :: loc_idx_face
    integer, dimension(:), allocatable, intent(out) :: loc_idx_vertices
    
    integer :: no_face_vertices
    
    no_face_vertices = no_vertices(eltype_of_faces_of_elem(eltype))
    allocate(loc_idx_vertices(no_face_vertices))
    
    select case (eltype)
      case(12)
        select case(loc_idx_face)
          case(1)
            loc_idx_vertices(1) = 1
          case(2)
            loc_idx_vertices(1) = 2
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select
      
      case(13)
        select case(loc_idx_face)
          case(1)
            loc_idx_vertices(1) = 1
          case(2)
            loc_idx_vertices(1) = 2
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select
      
      case(23)
        select case(loc_idx_face)
          case(1)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 2
          case(2)
            loc_idx_vertices(1) = 2
            loc_idx_vertices(2) = 3
          case(3)
            loc_idx_vertices(1) = 3
            loc_idx_vertices(2) = 1
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select
      
      case(26)
        select case(loc_idx_face)
          case(1)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 2
            loc_idx_vertices(3) = 4
          case(2)
            loc_idx_vertices(1) = 2
            loc_idx_vertices(2) = 3
            loc_idx_vertices(3) = 5
          case(3)
            loc_idx_vertices(1) = 3
            loc_idx_vertices(2) = 1
            loc_idx_vertices(3) = 6
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select
      
      case(24)
        select case(loc_idx_face)
          case(1)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 2
          case(2)
            loc_idx_vertices(1) = 2
            loc_idx_vertices(2) = 3
          case(3)
            loc_idx_vertices(1) = 3
            loc_idx_vertices(2) = 4
          case(4)
            loc_idx_vertices(1) = 4
            loc_idx_vertices(2) = 1
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select
      
      case(28,29)
        select case(loc_idx_face)
          case(1)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 2
            loc_idx_vertices(3) = 5
          case(2)
            loc_idx_vertices(1) = 2
            loc_idx_vertices(2) = 3
            loc_idx_vertices(3) = 6
          case(3)
            loc_idx_vertices(1) = 3
            loc_idx_vertices(2) = 4
            loc_idx_vertices(3) = 7
          case(4)
            loc_idx_vertices(1) = 4
            loc_idx_vertices(2) = 1
            loc_idx_vertices(3) = 8
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select
      
      case(34)
        select case(loc_idx_face)
          case(1)
            loc_idx_vertices(1) = 2
            loc_idx_vertices(2) = 3
            loc_idx_vertices(3) = 4
          case(2)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 3
            loc_idx_vertices(3) = 4
          case(3)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 2
            loc_idx_vertices(3) = 4
          case(4)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 2
            loc_idx_vertices(3) = 3
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select
      
      case(310)
        error stop "unknown eltype"
      
      case(38)
        select case(loc_idx_face)
          case(1)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 2
            loc_idx_vertices(3) = 3
            loc_idx_vertices(4) = 4
          case(2)
            loc_idx_vertices(1) = 5
            loc_idx_vertices(2) = 6
            loc_idx_vertices(3) = 7
            loc_idx_vertices(4) = 8
          case(3)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 2
            loc_idx_vertices(3) = 6
            loc_idx_vertices(4) = 5
          case(4)
            loc_idx_vertices(1) = 2
            loc_idx_vertices(2) = 3
            loc_idx_vertices(3) = 7
            loc_idx_vertices(4) = 6
          case(5)
            loc_idx_vertices(1) = 4
            loc_idx_vertices(2) = 3
            loc_idx_vertices(3) = 7
            loc_idx_vertices(4) = 8
          case(6)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 4
            loc_idx_vertices(3) = 8
            loc_idx_vertices(4) = 5
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select
      
      case(320)
        select case(loc_idx_face)
          case(1)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 2
            loc_idx_vertices(3) = 3
            loc_idx_vertices(4) = 4
            loc_idx_vertices(5) = 9
            loc_idx_vertices(6) = 10
            loc_idx_vertices(7) = 11
            loc_idx_vertices(8) = 12
          case(2)
            loc_idx_vertices(1) = 5
            loc_idx_vertices(2) = 8
            loc_idx_vertices(3) = 7
            loc_idx_vertices(4) = 6
            loc_idx_vertices(5) = 16
            loc_idx_vertices(6) = 15
            loc_idx_vertices(7) = 14
            loc_idx_vertices(8) = 13
          case(3)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 5
            loc_idx_vertices(3) = 6
            loc_idx_vertices(4) = 2
            loc_idx_vertices(5) = 17
            loc_idx_vertices(6) = 13
            loc_idx_vertices(7) = 18
            loc_idx_vertices(8) = 9
          case(4)
            loc_idx_vertices(1) = 2
            loc_idx_vertices(2) = 6
            loc_idx_vertices(3) = 7
            loc_idx_vertices(4) = 3
            loc_idx_vertices(5) = 18
            loc_idx_vertices(6) = 14
            loc_idx_vertices(7) = 19
            loc_idx_vertices(8) = 10
          case(5)
            loc_idx_vertices(1) = 3
            loc_idx_vertices(2) = 7
            loc_idx_vertices(3) = 8
            loc_idx_vertices(4) = 4
            loc_idx_vertices(5) = 19
            loc_idx_vertices(6) = 15
            loc_idx_vertices(7) = 20
            loc_idx_vertices(8) = 11
          case(6)
            loc_idx_vertices(1) = 4
            loc_idx_vertices(2) = 8
            loc_idx_vertices(3) = 5
            loc_idx_vertices(4) = 1
            loc_idx_vertices(5) = 20
            loc_idx_vertices(6) = 16
            loc_idx_vertices(7) = 17
            loc_idx_vertices(8) = 12
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select
      
      case(327)
        select case(loc_idx_face)
          case(1)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 2
            loc_idx_vertices(3) = 3
            loc_idx_vertices(4) = 4
            loc_idx_vertices(5) = 9
            loc_idx_vertices(6) = 10
            loc_idx_vertices(7) = 11
            loc_idx_vertices(8) = 12
            loc_idx_vertices(9) = 21
          case(2)
            loc_idx_vertices(1) = 5
            loc_idx_vertices(2) = 8
            loc_idx_vertices(3) = 7
            loc_idx_vertices(4) = 6
            loc_idx_vertices(5) = 16
            loc_idx_vertices(6) = 15
            loc_idx_vertices(7) = 14
            loc_idx_vertices(8) = 13
            loc_idx_vertices(9) = 22
          case(3)
            loc_idx_vertices(1) = 1
            loc_idx_vertices(2) = 5
            loc_idx_vertices(3) = 6
            loc_idx_vertices(4) = 2
            loc_idx_vertices(5) = 17
            loc_idx_vertices(6) = 13
            loc_idx_vertices(7) = 18
            loc_idx_vertices(8) = 9
            loc_idx_vertices(9) = 23
          case(4)
            loc_idx_vertices(1) = 2
            loc_idx_vertices(2) = 6
            loc_idx_vertices(3) = 7
            loc_idx_vertices(4) = 3
            loc_idx_vertices(5) = 18
            loc_idx_vertices(6) = 14
            loc_idx_vertices(7) = 19
            loc_idx_vertices(8) = 10
            loc_idx_vertices(9) = 26
          case(5)
            loc_idx_vertices(1) = 3
            loc_idx_vertices(2) = 7
            loc_idx_vertices(3) = 8
            loc_idx_vertices(4) = 4
            loc_idx_vertices(5) = 19
            loc_idx_vertices(6) = 15
            loc_idx_vertices(7) = 20
            loc_idx_vertices(8) = 11
            loc_idx_vertices(9) = 24
          case(6)
            loc_idx_vertices(1) = 4
            loc_idx_vertices(2) = 8
            loc_idx_vertices(3) = 5
            loc_idx_vertices(4) = 1
            loc_idx_vertices(5) = 20
            loc_idx_vertices(6) = 16
            loc_idx_vertices(7) = 17
            loc_idx_vertices(8) = 12
            loc_idx_vertices(9) = 25
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select
      
      case default
        error stop "unknown eltype/loc_idx_face combination"
    end select
  end subroutine loc_idx_of_face_vertices_in_elem
  
  module pure integer function get_opposite_vertex(eltype,loc_idx_face) result(loc_idx_opposite_vtx)
    !---------------------------------------------------------------------
    ! This function returns a node that is opposite to the given face. It 
    ! is used for computing the outward element normal.
    !---------------------------------------------------------------------
    integer, intent(in) :: eltype
    integer, intent(in) :: loc_idx_face

    select case(eltype)
      
      case(12)
        select case(loc_idx_face)
          case(1)
            loc_idx_opposite_vtx = 2
          case(2)
            loc_idx_opposite_vtx = 1
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select

      case(13)
        select case(loc_idx_face)
          case(1)
            loc_idx_opposite_vtx = 2
          case(2)
            loc_idx_opposite_vtx = 1
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select
      
      case(23,26)
        select case(loc_idx_face)
          case(1)
            loc_idx_opposite_vtx = 3
          case(2)
            loc_idx_opposite_vtx = 1
          case(3)
            loc_idx_opposite_vtx = 2
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select

      case(24,28,29)
        select case(loc_idx_face)
          case(1)
            loc_idx_opposite_vtx = 3
          case(2)
            loc_idx_opposite_vtx = 4
          case(3)
            loc_idx_opposite_vtx = 1
          case(4)
            loc_idx_opposite_vtx = 2
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select

      case(34,310)
        select case(loc_idx_face)
          case(1)
            loc_idx_opposite_vtx = 1
          case(2)
            loc_idx_opposite_vtx = 2
          case(3)
            loc_idx_opposite_vtx = 3
          case(4)
            loc_idx_opposite_vtx = 4
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select
      
      case(38,320,327)
        select case(loc_idx_face)
          case(1)
            loc_idx_opposite_vtx = 5
          case(2)
            loc_idx_opposite_vtx = 1
          case(3)
            loc_idx_opposite_vtx = 3
          case(4)
            loc_idx_opposite_vtx = 1
          case(5)
            loc_idx_opposite_vtx = 1
          case(6)
            loc_idx_opposite_vtx = 2
          case default
            error stop "unknown eltype/loc_idx_face combination"
        end select

      case default
        error stop "unknown eltype/loc_idx_face combination"
    end select
  end function get_opposite_vertex
  
end submodule local_elem__elem_face_coupling

