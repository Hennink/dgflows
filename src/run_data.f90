module run_data
  use assertions, only: assert, assert_eq
  use code_const, only: UNINITIALIZED_CHAR
  use f90_kind, only: dp
  use io_basics, only: read_line_from_input_file
  use string_mod, only: as_chars, string_t
  use, intrinsic :: iso_fortran_env, only: REAL64
  implicit none
  
  real(dp) :: dt
  integer :: no_time_steps
  
  character(:), allocatable, protected :: quantities_frozen_at_ic(:)
  
  character(:), allocatable, protected :: petsc_options(:)

  character(:), allocatable, protected :: file_path_unknowns_specs, &
                                          file_path_bc_specs, &
                                          file_path_matprop_specs, &
                                          file_path_sources_specs, &
                                          ic_file

  ! Put some LES specs here, instead of in the les_specs_file, because they might be useful even
  ! in cases without an LES model (e.g., for plotting what nu_Smagorinsky would have been).
  character(200), protected :: les_specs_file
  character(200), protected :: les_xyz2yplus ! should probably be expressed in terms of wall distance `w`, not just [x,y,z]

  character(200), protected :: mansol_specs_file
  character(200), protected :: wall_dist_specs_file
  logical, protected :: clip_viscosity, clip_k_over_cp
  real(dp), protected :: coolprop_spline_tol = 1.0e-04_dp
  logical, protected :: use_rans_model
  logical, protected :: eval_grad_rho_k_term_in_momentum_eq
  real(dp), protected :: min_Pk_for_RANS ! This has to be small compared to ~ rho * U_bulk**3 / characteristic_dimension
                                         ! So, it is estimated as 0.5% of rho * U_bulk**3 / characteristic_dimension
  integer, allocatable, protected :: yplus_bnd_ids(:)
  integer, protected :: yplus_nghb_depth
  
  character(:), allocatable, protected :: rans_model_description
  real(dp), protected :: L_max_Prandtl

#ifdef HARDCODE_DIMEN
  integer, parameter :: mesh_dimen = 3
#else
  integer, protected :: mesh_dimen
#endif

  character(1000), protected :: input_mesh = ''
  logical, protected :: assume_affine_elems = .false.
  ! tolerance for when two vertices on opposite ends of a periodic mesh are considered equivalent:
  real(dp), protected :: mesh_periodic_equiv_tol = real(epsilon(1.0_REAL64)**0.5_REAL64,dp) ! based on 64 bit because it's from Gmsh
  character(:), allocatable :: bnd_refinement_list(:)
  integer, protected :: max_mesh_irregularity
  real(dp), protected :: eqv_elem_group_abstol = real(epsilon(1.0_REAL64)**0.5_REAL64,dp) ! based on 64 bit because it's from Gmsh
  
  character(200), protected :: basis_funcs

  character(:), allocatable, protected :: output_solvecs_to
  logical, protected :: average_m, average_restress
  character(200), protected :: avg_over_groups_with_idvec
  integer, protected :: polorder_group_avg, steps_per_group_avg
  character(:), allocatable, protected :: write_face_splines_to, read_face_splines_from
  character(:), allocatable, protected :: face_integrals(:)
  
  integer, protected :: part_weight_fluid_elem, part_weight_solid_elem, &
                        comm_size_fluid_elem, comm_size_solid_elem
  integer, protected :: extra_quad_order_elem = 0, extra_quad_order_face = 0, min_quad_order = -1
  
  logical, protected :: boussinesq_approx, boussinesq_force
  logical, protected :: div_free_velocity, rm_transp_stress_term, constant_density
  logical, protected :: constant_kinematic_viscosity = .false., &
                        constant_k_over_cp = .false.
  logical, protected :: multiphysics_mode, prec_conv_on_CFD_mesh, extrapolate_T_for_RT_calc
  
  integer, protected :: calc_max_ddt, calc_cfl, calc_norm_P_corr = 1
  integer, protected :: print_div_and_continuity_errs
  real(dp), protected :: charac_length

  logical :: m_continuity_penal_is_time_independent = .false.
  character(1000) :: m_continuity_penal_preset_norm_u_func = ''
  integer, protected :: add_m_penalties = 0
  character(20), protected :: m_penalties_in = UNINITIALIZED_CHAR
  logical, protected :: div_m_pen_pointwise = .false.
  character(100), protected :: cont_m_pen_mode = 'avg_on_neighbors'
  real(dp), protected :: div_m_pen_safety_factor = 1.0_dp, cont_m_pen_safety_factor = 1.0_dp
  
  character(:), allocatable :: datafile_for_scalfunc
  logical :: datafile_for_scalfunc_plot = .false.

  logical, protected :: replace_local_dg_by_sip
  real(dp), protected :: p_SIP_penal_safety_factor = 1.0_dp
  logical, protected :: corrective_convection_enthalpy
  character(:), allocatable, protected :: spec_h_strategy, m_p_strategy
  
  integer, allocatable, protected :: ignore_elems_with_mat_id(:)
  
  character(:), allocatable, protected :: process_ic(:)
  logical, protected :: randomize_ic_mmtum_press
  real(dp), protected :: ic_pertubation_coeff
  
  integer, protected :: max_iter_within_time_step, no_iter_Turb_in_timestep, no_iter_NS_Turb_in_timestep
  real(dp), protected :: tol_rel_Pcorr, tol_rel_density_corr, tol_rel_coupled_NS
  real(dp), protected :: sip_penalty_safety_factor
  
  character(10), protected :: p_stab = 'auto' ! Penalty term in the P matrix of the form (length / nu), Can be `F`, `T`, or `auto`.
  real(dp), protected :: p_stab_safety_factor = 1.0_dp

  real(dp), protected :: min_u_dot_n_at_outflow_bc = 0.0_dp
  logical, protected :: cap_inflow_at_outlet = .false.
  
  real(dp), protected :: boussinesq_T_ref, avg_T_in_ignored_elems
  
  logical, protected :: check_mat_symm
  logical, protected :: print_mtx_sparsity_info
  
  logical, protected :: use_approx_schur_PC, use_approx_schur_KSP
  
  character(100) :: change_polorders_in_postprocessing(4) = ''
  logical, protected :: output_fields_to_gmsh, gmsh_output_includes_all_fields = .false.

contains
  subroutine read_input()
    ! Reads the main input file, and outputs all settings to a separate file.
    ! 
    ! It would be better read everything into a single `control_params` namelist,
    ! but some important parameters are not included for hystorical reasons.
    use petsc_mod, only: i_am_master_process
    use string_manipulations, only: split, char2bool, char2int
    use system_commands, only: synchronous_commands_mode
    
    namelist /control_params/ petsc_options, &
                              extra_quad_order_elem, extra_quad_order_face, min_quad_order, &
                              part_weight_fluid_elem, part_weight_solid_elem, &
                              comm_size_fluid_elem, comm_size_solid_elem, &
                              input_mesh, assume_affine_elems, mesh_periodic_equiv_tol, wall_dist_specs_file, &
                              basis_funcs, &
                              datafile_for_scalfunc, datafile_for_scalfunc_plot, &
                              output_solvecs_to, &
                              average_m, average_restress, &
                              avg_over_groups_with_idvec, polorder_group_avg, steps_per_group_avg, &
                              write_face_splines_to, read_face_splines_from, &
                              face_integrals, &
                              quantities_frozen_at_ic, &
                              bnd_refinement_list, max_mesh_irregularity, &
                              eqv_elem_group_abstol, &
                              ignore_elems_with_mat_id, &
                              process_ic, randomize_ic_mmtum_press, ic_pertubation_coeff, &
                              replace_local_dg_by_sip, p_SIP_penal_safety_factor, &
                              corrective_convection_enthalpy, spec_h_strategy, &
                              m_p_strategy, &
                              max_iter_within_time_step, no_iter_Turb_in_timestep, no_iter_NS_Turb_in_timestep, &
                              tol_rel_Pcorr, tol_rel_density_corr, tol_rel_coupled_NS, &
                              sip_penalty_safety_factor, &
                              p_stab, p_stab_safety_factor, &
                              boussinesq_approx, &
                              boussinesq_force, &
                              boussinesq_T_ref, &
                              div_free_velocity, rm_transp_stress_term, &
                              constant_density, &
                              constant_kinematic_viscosity, constant_k_over_cp, &
                              multiphysics_mode, prec_conv_on_CFD_mesh, extrapolate_T_for_RT_calc, &
                              min_u_dot_n_at_outflow_bc, cap_inflow_at_outlet, &
                              min_Pk_for_RANS, &
                              les_specs_file, les_xyz2yplus, &
                              clip_viscosity, clip_k_over_cp, &
                              coolprop_spline_tol, &
                              mansol_specs_file, &
                              eval_grad_rho_k_term_in_momentum_eq, &
                              yplus_bnd_ids, yplus_nghb_depth, &
                              check_mat_symm, &
                              print_mtx_sparsity_info, &
                              calc_max_ddt, calc_cfl, calc_norm_P_corr, &
                              print_div_and_continuity_errs, &
                              charac_length, &
                              m_continuity_penal_is_time_independent, &
                              m_continuity_penal_preset_norm_u_func, &
                              add_m_penalties, m_penalties_in, &
                              div_m_pen_pointwise, cont_m_pen_mode, &
                              div_m_pen_safety_factor, cont_m_pen_safety_factor, &
                              use_approx_schur_PC, use_approx_schur_KSP, &
                              avg_T_in_ignored_elems, &
                              L_max_Prandtl, &
                              change_polorders_in_postprocessing, &
                              output_fields_to_gmsh, gmsh_output_includes_all_fields, &
                              synchronous_commands_mode
    
    namelist /miscl_params/ &
        rans_model_description, &
        file_path_unknowns_specs, &
        file_path_matprop_specs, &
        file_path_bc_specs, &
        ic_file, &
        file_path_sources_specs, &
        dt, no_time_steps
#ifndef HARDCODE_DIMEN
    namelist /miscl_params/ mesh_dimen
#endif

    integer :: unit
    integer :: iostat
    character(200) :: iomsg
    integer :: i
    character(:), allocatable :: line, temp
    integer, allocatable :: temp_int(:)
    
    ! Set Default values:
    ! (Note that we cannot read a namelist with unallocated variables.)

    petsc_options = [character(100) :: &
        '-options_left true', &
        spread('', dim=1, ncopies=100) &
    ]
    quantities_frozen_at_ic = [character(100) :: ('', i = 1, 100)]
    basis_funcs = 'orthofun'
    part_weight_fluid_elem = 1
    part_weight_solid_elem = 1
    comm_size_fluid_elem = 1
    comm_size_solid_elem = 1
    wall_dist_specs_file = ''
    output_solvecs_to = repeat(' ',1000)
    average_m = .false.
    average_restress = .false.
    avg_over_groups_with_idvec = ''
    polorder_group_avg = 1
    steps_per_group_avg = 5
    write_face_splines_to = repeat(' ',1000)
    read_face_splines_from = repeat(' ',1000)
    face_integrals = [character(100) :: ('', i = 1, 100)]
    bnd_refinement_list = [character(100) :: ('', i = 1, 100)]
    max_mesh_irregularity = 1
    ignore_elems_with_mat_id = [(-1, i = 1, 1000)]
    datafile_for_scalfunc = repeat(' ',1000)
    replace_local_dg_by_sip = .false.
    corrective_convection_enthalpy = .false.
    spec_h_strategy = repeat(' ',1000)
    m_p_strategy = repeat(' ',1000)
    process_ic = [character(100) :: ('', i = 1, 1000)]
    randomize_ic_mmtum_press = .false.
    ic_pertubation_coeff = 0.9
    max_iter_within_time_step = 1
    no_iter_Turb_in_timestep = 1
    no_iter_NS_Turb_in_timestep = 1
    tol_rel_Pcorr = huge(1.0_dp)
    tol_rel_density_corr = 0.02_dp
    tol_rel_coupled_NS = huge(1.0_dp)
    sip_penalty_safety_factor = 1.0_dp
    boussinesq_approx = .false.
    boussinesq_force = .false.
    boussinesq_T_ref = 0.0_dp
    div_free_velocity = .false.
    rm_transp_stress_term = .false.
    constant_density = .false.
    multiphysics_mode = .false.
    extrapolate_T_for_RT_calc = .false.
    prec_conv_on_CFD_mesh = .false.
    min_Pk_for_RANS = 0.0_dp
    les_specs_file = ''
    les_xyz2yplus = UNINITIALIZED_CHAR
    mansol_specs_file = ''
    clip_viscosity = .false.
    clip_k_over_cp = .false.
    eval_grad_rho_k_term_in_momentum_eq = .false.
    allocate(yplus_bnd_ids(1000),source=-1)
    yplus_nghb_depth = 1
    check_mat_symm = .false.
    print_mtx_sparsity_info = .false.
    calc_max_ddt = 0
    calc_cfl = 0
    print_div_and_continuity_errs = 0
    charac_length = 0.0_dp
    use_approx_schur_PC = .false.
    use_approx_schur_KSP = .false.
    avg_T_in_ignored_elems = 0.0_dp
    L_max_Prandtl = -1.0_dp
    output_fields_to_gmsh = .true.
    
    
    open(file='fort.1', newunit=unit, &
         status='old', action='read', access='sequential', form='formatted', &
         iostat=iostat, iomsg=iomsg)
    call assert(iostat==0,iomsg)
      ! ========  Overwrite default control parameters with namelist: =======
      read(unit,control_params,iostat=iostat,iomsg=iomsg)
      call assert(iostat==0,iomsg)
      
      ! ========  Trim some input from the namelist: =======
      call trim_char(output_solvecs_to)
      call trim_char(write_face_splines_to)
      call trim_char(datafile_for_scalfunc)
      call trim_char(read_face_splines_from)
      call trim_char(spec_h_strategy)
      call trim_char(m_p_strategy)
      call delete_blanks(petsc_options)
      call delete_blanks(face_integrals)
      call delete_blanks(process_ic)
      call delete_blanks(quantities_frozen_at_ic)
      call delete_blanks(bnd_refinement_list)
      
      temp_int = pack(ignore_elems_with_mat_id,ignore_elems_with_mat_id >= 0)
      call move_alloc(temp_int,ignore_elems_with_mat_id)
      
      temp_int = pack(yplus_bnd_ids,yplus_bnd_ids >= 0)
      call move_alloc(temp_int,yplus_bnd_ids)
      
      ! ========  Overwrite control params with environment: ======== 
      call env_var('DGF_RANDOMIZE_IC_MMTUM_PRESS',temp)
      if (allocated(temp)) randomize_ic_mmtum_press = char2bool(temp)
      call env_var('DGF_PROCESS_IC',temp)
      if (allocated(temp)) then
        if (.not. char2bool(temp)) process_ic = ''
      endif
      
      ! ======= Read miscillaneous variables in fort.1: =======
      call read_line_from_input_file(unit, line)
#ifdef HARDCODE_DIMEN
      call assert(mesh_dimen == char2int(line), 'With `HARDCODE_DIMEN`, the mesh dimension must be 3, not ' // line)
#else
      mesh_dimen = char2int(line)
#endif
      call assert(mesh_dimen == 2 .or. mesh_dimen == 3, 'weird mesh_dimen')

      call read_line_from_input_file(unit,rans_model_description)
      use_rans_model = rans_model_description /= ''
      call read_line_from_input_file(unit,file_path_unknowns_specs)
      call read_line_from_input_file(unit,file_path_matprop_specs)
      call read_line_from_input_file(unit,file_path_bc_specs)
      call read_line_from_input_file(unit,ic_file)
      call read_line_from_input_file(unit,file_path_sources_specs)
      read(unit,*) dt
      read(unit,*) no_time_steps
    close(unit,iostat=iostat,iomsg=iomsg)
    call assert(iostat==0,iomsg)
    
    ! =======  Sanity checks: =======
    if (boussinesq_approx) then
      boussinesq_force = .true.
      div_free_velocity = .true.
      constant_density = .true.
    endif
    if (multiphysics_mode) call assert(basis_funcs=='old','You have to use old basis functions in multiphysics mode')
    if (print_div_and_continuity_errs>0) call assert(charac_length>0.0_dp,'Specify a charact. length to compute the div error.')
    if (add_m_penalties > 0) then
      call assert(m_penalties_in /= UNINITIALIZED_CHAR,'Specify in which eq. div and continuity penalties must be added.')
      call assert(m_penalties_in == 'momentum' .or. m_penalties_in == 'm-postproc',&
                  'You can add div and continuity penalties only in the momentum eq or while prost-processing the mass-flux.')
    endif
    if (use_rans_model) call assert(.not.rm_transp_stress_term,'In RANS, you must take the stress transp. term into account')
    if (.not. use_rans_model) call assert(no_iter_NS_Turb_in_timestep == 1, 'no_iter_NS_Turb_in_timestep > 1 but no solving RANS')
    
    ! ========  Overwrite miscellanaous variables with with environment: ======== 
    call env_var('DGF_IC_FILE',temp)
    if (allocated(temp)) call move_alloc(temp,ic_file)
    
    ! ========  Output the main control parameters: ======== 
    if (i_am_master_process()) then
      open( &
          file='dgf_control_params', newunit=unit,  &
          status='replace', action='write', form='formatted',  &
          iostat=iostat, iomsg=iomsg  &
      )
      call assert(iostat==0,iomsg)
        write(unit,control_params,iostat=iostat,iomsg=iomsg)
        call assert(iostat==0,iomsg)
        write(unit,miscl_params,iostat=iostat,iomsg=iomsg)
        call assert(iostat==0,iomsg)
      close(unit,iostat=iostat,iomsg=iomsg)
      call assert(iostat==0,iomsg)
    endif
  end subroutine read_input
  
  subroutine env_var(name,value)
    ! Same as intrinsic 'get_environment_variable', but handles the errors, and trims the char.
    ! Allocates 'value' iff the env. variable exists.
    use exceptions, only: error
    
    character(*), intent(in) :: name
    character(:), allocatable, intent(out) :: value
    
    integer :: length
    integer :: status
    character(:), allocatable :: temp
    
    allocate(character(1000) :: value)
    call get_environment_variable(name,value=value,length=length,status=status,trim_name=.true.)
    
    call assert(status == -1 .eqv. length > len(value),'env_var: I do not understand status -1')
    select case(status)
      case(0)
        temp = value(:length)
        call move_alloc(temp,value)
      case(1)
        deallocate(value)
      case(-1)
        call error('env_var: value is too long; value=' // value)
      case(2)
        call error('env_var: processor does not support environment variables')
      case(3:)
        call error('env_var: an error has occured')
      case default
        call error('env_var: weird (non-standard?) status')
    end select
  end subroutine env_var
  
  subroutine trim_char(c)
    character(:), allocatable, intent(inout) :: c
    
    character(:), allocatable :: temp
    
    allocate(temp, source = trim(c))
    call move_alloc(temp,c)
  end subroutine

  subroutine delete_blanks(chars)
    character(:), allocatable, intent(inout) :: chars(:)

    character(:), allocatable :: temp(:)

    temp = pack(chars, chars /= '')
    call move_alloc(temp,chars)
  end subroutine
end module run_data
