submodule(mesh_objects) mesh_objects__active_subfaces
  implicit none
  
contains
  module subroutine active_subfaces_of_elem(this,subfaces,neighbors)
    class(elem_list), intent(in) :: this
    type(face_ptr), allocatable, dimension(:), intent(out) :: subfaces
    type(elem_ptr), allocatable, dimension(:), intent(out) :: neighbors
    
    integer :: no_subfaces
    integer :: i, j
    type(face_ptr), allocatable, dimension(:) :: active_subfaces
    type(face_list), pointer :: sub_face
    type(elem_ptr), pointer :: ngbrs(:)
    integer :: idx
    
    idx = 1
    no_subfaces = no_active_subfaces_of_elem(this)
    allocate(subfaces(no_subfaces))
    allocate(neighbors(no_subfaces))
    do i = 1, size(this%faces)
      call active_subfaces_of_face(this%faces(i)%point,active_subfaces)
      
      do j = 1, size(active_subfaces)
        sub_face => active_subfaces(j)%point
        ngbrs => sub_face%elem_neighbors
        
        subfaces(idx)%point => sub_face
        
        if (sub_face%ftype == BOUNDARY) then
          neighbors(idx)%point => null()
        elseif (same_id(this%id,ngbrs(1)%point%id)) then
          neighbors(idx)%point => ngbrs(2)%point
        elseif (same_id(this%id,ngbrs(2)%point%id)) then
          neighbors(idx)%point => ngbrs(1)%point
        else
          error stop 'active_subfaces_of_elem: internal error'
        endif

        idx = idx + 1
      enddo
    enddo
  end subroutine active_subfaces_of_elem
  
  pure integer function no_active_subfaces_of_elem(elem) result(no_subfaces)
    type(elem_list), intent(in) :: elem
    
    integer :: i
    
    no_subfaces = 0
    do i = 1, size(elem%faces)
      no_subfaces = no_subfaces + no_active_subfaces_of_face(elem%faces(i)%point)
    enddo
  end function no_active_subfaces_of_elem
  
  pure recursive function no_active_subfaces_of_face(face) result(no_children)
    type(face_list), intent(in) :: face
    integer :: no_children
    
    integer :: i
    
    if (face%active) then
      no_children = 1
    else
      no_children = 0
      do i = 1, size(face%children)
        no_children = no_children + no_active_subfaces_of_face(face%children(i)%point)
      enddo
    endif
  end function no_active_subfaces_of_face
  
  subroutine active_subfaces_of_face(face,subfaces)
    type(face_list), intent(in) :: face
    type(face_ptr), allocatable, dimension(:), intent(out) :: subfaces
    
    integer :: no_subfaces
    integer :: first_idx
    
    no_subfaces = no_active_subfaces_of_face(face)
    allocate(subfaces(no_subfaces))
    first_idx = 1
    call insert_subfaces_and_increase_idx(subfaces,first_idx,face)
  end subroutine active_subfaces_of_face
  
  recursive subroutine insert_subfaces_and_increase_idx(active_subfaces,idx,top_face)
    type(face_ptr), dimension(:), intent(inout) :: active_subfaces
    integer, intent(inout) :: idx
    type(face_list), target, intent(in) :: top_face
    
    integer :: i
    
    if (top_face%active) then
      active_subfaces(idx)%point => top_face
      idx = idx + 1
    else
      do i = 1, size(top_face%children)
        call insert_subfaces_and_increase_idx(active_subfaces,idx,top_face%children(i)%point)
      enddo
    endif
  end subroutine insert_subfaces_and_increase_idx
end submodule mesh_objects__active_subfaces

