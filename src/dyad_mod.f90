module dyad_mod
  use assertions, only: assert
  use f90_kind, only: dp
  implicit none
  
  private
  public :: dyad_t
  
  type, abstract :: dyad_t
    logical :: use_finite_diff = .false.
  contains
    private
    procedure(dyad_i), deferred, public :: func
    procedure, public :: deriv_wrt_arg1
    procedure, public :: deriv_wrt_arg2
  end type dyad_t
  
  interface 
    elemental real(dp) function dyad_i(this,arg1,arg2)
      use f90_kind, only: dp
      import :: dyad_t
      implicit none
      class(dyad_t), intent(in) :: this
      real(dp), intent(in) :: arg1, arg2
    end function dyad_i
  end interface
  
  real(dp), parameter :: h = sqrt(epsilon(1.0d0))
  
contains
  impure elemental real(dp) function deriv_wrt_arg1(this,arg1,arg2)
    class(dyad_t), intent(in) :: this
    real(dp), intent(in) :: arg1, arg2
    
    call assert(this%use_finite_diff)
    deriv_wrt_arg1 = (this%func(arg1+h,arg2) - this%func(arg1-h,arg2)) / (2.0d0 * h)
  end function deriv_wrt_arg1
  
  impure elemental real(dp) function deriv_wrt_arg2(this,arg1,arg2)
    class(dyad_t), intent(in) :: this
    real(dp), intent(in) :: arg1, arg2
    
    call assert(this%use_finite_diff)
    deriv_wrt_arg2 = (this%func(arg1,arg2+h) - this%func(arg1,arg2-h)) / (2.0d0 * h)
  end function deriv_wrt_arg2
end module dyad_mod

