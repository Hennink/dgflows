module rans_wall_functions_mod
  use assertions, only: assert, BOUNDS_CHECK
  use exceptions, only: error
  use f90_kind, only: dp
  use math_const, only: SMALL
  use mesh_objects, only: face_list
  use numerical_solutions, only: matprop_set, num_scalfunc_set
  use run_data, only: rans_model_description
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only:  vec_func_type
  implicit none
  
  private
  public :: initialize
  public :: wf_slipcoeff, wf_bc_vol_turb2_qnty, u_star_over_T_plus
  public :: eval_u_k
  
  ! Von-Karman constant
  real(dp), parameter :: kappa = 0.41_dp
  ! Turbulent Prandtl number
  real(dp), parameter :: Pr_t = 0.85_dp
  ! k-eps useful model constants
  real(dp), parameter :: C_mu = 0.09_dp
  ! k-omega useful model constants
  real(dp), parameter :: beta_star = 9.0_dp/100.0_dp
  
  ! Coefficient in the law of the wall related to smooth walls
  real(dp), parameter :: E = 9.0_dp
  ! Distance at which the log and linear profiles match
  real(dp), parameter :: delta_w_plus_c = 11.06_dp
  
  type, extends(scal_func_type) :: wf_slipcoeff_t
  contains
    procedure, non_overridable :: values_at_qps__face => wf_slipcoeff_at_face_qps
  end type wf_slipcoeff_t
  
  type, extends(scal_func_type) :: u_star_over_T_plus_t
  contains
    procedure, non_overridable :: values_at_qps__face => u_star_over_T_plus_at_face_qps
  end type u_star_over_T_plus_t
  
  !vec_funcs for the rho_eps/rho_omega wall-function bc. These are vectorial representations
  !of scalars. Unfortunately, it's not possible to simply use as_vecfunc(scal_func) since
  !eval_at_face must be overridden. 
  type, extends(vec_func_type) :: wf_bc_rho_eps_t
  contains
    procedure, non_overridable :: no_dirs => no_dirs_wf_bc_rho_eps, &
                                  values_at_qps__face => wf_bc_rho_eps_at_face_qps
  end type wf_bc_rho_eps_t
  
  type, extends(vec_func_type) :: wf_bc_rho_log_eps_t
  contains
    procedure, non_overridable :: no_dirs => no_dirs_wf_bc_rho_log_eps, &
                                  values_at_qps__face => wf_bc_rho_log_eps_at_face_qps
  end type wf_bc_rho_log_eps_t
  
  type, extends(vec_func_type) :: wf_bc_rho_omega_t
  contains
    procedure, non_overridable :: no_dirs =>  no_dirs_wf_bc_rho_omega, &
                                  values_at_qps__face => wf_bc_rho_omega_at_face_qps
  end type wf_bc_rho_omega_t
  
  type, extends(vec_func_type) :: wf_bc_rho_log_w_t
  contains
    procedure, non_overridable :: no_dirs =>  no_dirs_wf_bc_rho_log_w, &
                                  values_at_qps__face => wf_bc_rho_log_w_at_face_qps
  end type wf_bc_rho_log_w_t
  
  type(wf_slipcoeff_t) :: wf_slipcoeff
  class(vec_func_type), allocatable :: wf_bc_vol_turb2_qnty
  type(u_star_over_T_plus_t) :: u_star_over_T_plus
  
  logical, save :: module_initialized = .false.
  
contains
  subroutine initialize()
    if (module_initialized) return
    
    select case (rans_model_description)
      case('k-eps')
        allocate(wf_bc_rho_eps_t :: wf_bc_vol_turb2_qnty)
      case('log-k-log-eps')
        allocate(wf_bc_rho_log_eps_t :: wf_bc_vol_turb2_qnty)
      case('k-omega')
        allocate(wf_bc_rho_omega_t :: wf_bc_vol_turb2_qnty)
      case('k-log-w')
        allocate(wf_bc_rho_log_w_t :: wf_bc_vol_turb2_qnty)
      case default
        call error('No wall functions available for RANS model = '//trim(rans_model_description))
    end select
    
    module_initialized = .true.
  end subroutine initialize
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  function wf_slipcoeff_at_face_qps(this,face,side,time) result(values)
    class(wf_slipcoeff_t), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side
    integer, intent(in) :: time
    real(dp) :: values(face%nqp)
    
    real(dp) :: u_k(face%nqp), u_plus(face%nqp)
    
    call assert(face%elem_neighbors(side)%point%is_fluid())
    call assert(face%is_boundary(.true.))
    
    call eval_u_k(face,side,time,u_k)
    call eval_u_plus(face,side,time,u_plus)
    values = u_k / max(u_plus,SMALL)
  end function wf_slipcoeff_at_face_qps
  
  
  function u_star_over_T_plus_at_face_qps(this,face,side,time) result(values)
    class(u_star_over_T_plus_t), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side, time
    real(dp) :: values(face%nqp)
    
    real(dp) :: u_k(face%nqp), T_plus(face%nqp)
    
    call assert(face%elem_neighbors(side)%point%is_fluid())
    call assert(face%is_boundary(.true.))
    
    call eval_u_k(face,side,time,u_k)
    call eval_T_plus(face,side,time,T_plus)
    values = u_k / max(T_plus,SMALL)
  end function u_star_over_T_plus_at_face_qps
  
  
  subroutine wf_bc_rho_eps_at_face_qps(this,face,side,time,values)
    class(wf_bc_rho_eps_t), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side, time
    real(dp), intent(out) :: values(:,:)
    
    class(scal_func_type), pointer :: rho_func
    integer :: dir
    real(dp) :: delta
    real(dp) :: u_k(face%nqp), rho(face%nqp)
    
    call assert(face%elem_neighbors(side)%point%is_fluid())
    call assert(face%is_boundary(.true.))
    call assert(size(values,1) == this%no_dirs(),BOUNDS_CHECK)
    call assert(size(values,2) == face%nqp,BOUNDS_CHECK)
    
    rho_func => matprop_set%func_ptr('density')
    delta = face%distance_from_elem_centroid(side,only_fluid_mesh=.true.)
    call eval_u_k(face,side,time,u_k)
    rho = rho_func%values_at_qps(face,side,time)
    do dir = 1, this%no_dirs()
      values(dir,:) = rho * (u_k**3) / (kappa*delta)
    enddo
  end subroutine wf_bc_rho_eps_at_face_qps
  
  subroutine wf_bc_rho_log_eps_at_face_qps(this,face,side,time,values)
    class(wf_bc_rho_log_eps_t), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side, time
    real(dp), intent(out) :: values(:,:)
    
    class(scal_func_type), pointer :: rho_func
    integer :: dir
    real(dp) :: delta
    real(dp) :: u_k(face%nqp), rho(face%nqp)
    
    call assert(face%elem_neighbors(side)%point%is_fluid())
    call assert(face%is_boundary(.true.))
    call assert(size(values,1) == this%no_dirs(),BOUNDS_CHECK)
    call assert(size(values,2) == face%nqp,BOUNDS_CHECK)
    
    rho_func => matprop_set%func_ptr('density')
    delta = face%distance_from_elem_centroid(side,only_fluid_mesh=.true.)
    call eval_u_k(face,side,time,u_k)
    rho = rho_func%values_at_qps(face,side,time)
    do dir = 1, this%no_dirs()
      values(dir,:) = rho * log(u_k**3 / (kappa*delta))
    enddo
  end subroutine wf_bc_rho_log_eps_at_face_qps
  
  subroutine wf_bc_rho_omega_at_face_qps(this,face,side,time,values)
    class(wf_bc_rho_omega_t), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side, time
    real(dp), intent(out) :: values(:,:)
    
    class(scal_func_type), pointer :: rho_func
    integer :: dir
    real(dp) :: delta
    real(dp) :: u_k(face%nqp), rho(face%nqp)
    
    call assert(face%elem_neighbors(side)%point%is_fluid())
    call assert(face%is_boundary(.true.))
    call assert(size(values,1) == this%no_dirs(),BOUNDS_CHECK)
    call assert(size(values,2) == face%nqp,BOUNDS_CHECK)
    
    delta = face%distance_from_elem_centroid(side,only_fluid_mesh=.true.)
    call eval_u_k(face,side,time,u_k)
    rho_func => matprop_set%func_ptr('density')
    rho = rho_func%values_at_qps(face,side,time)
    do dir = 1, this%no_dirs()
      values(dir,:) = rho * u_k / (sqrt(beta_star)*kappa*delta)
    enddo
  end subroutine wf_bc_rho_omega_at_face_qps
  
  subroutine wf_bc_rho_log_w_at_face_qps(this,face,side,time,values)
    class(wf_bc_rho_log_w_t), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side, time
    real(dp), intent(out) :: values(:,:)
    
    class(scal_func_type), pointer :: rho_func
    integer :: dir
    real(dp) :: delta
    real(dp) :: u_k(face%nqp), rho(face%nqp)
    
    call assert(face%elem_neighbors(side)%point%is_fluid())
    call assert(face%is_boundary(.true.))
    call assert(size(values,1) == this%no_dirs(),BOUNDS_CHECK)
    call assert(size(values,2) == face%nqp,BOUNDS_CHECK)
    
    delta = face%distance_from_elem_centroid(side,only_fluid_mesh=.true.)
    call eval_u_k(face,side,time,u_k)
    rho_func => matprop_set%func_ptr('density')
    rho = rho_func%values_at_qps(face,side,time)
    do dir = 1, this%no_dirs()
      values(dir,:) = rho * log(u_k / (sqrt(beta_star)*kappa*delta))
    enddo
  end subroutine wf_bc_rho_log_w_at_face_qps
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine eval_u_plus(face,side,time,values)
    type(face_list), intent(in) :: face
    integer, intent(in) :: side, time
    real(dp), intent(out) :: values(:)
    
    real(dp) :: delta_w_plus(face%nqp)
    integer :: p
    
    call eval_delta_w_plus(face,side,time,delta_w_plus)
    do p = 1, face%nqp
      if (delta_w_plus(p) < delta_w_plus_c) then
        values(p) = delta_w_plus(p)
      else
        values(p) = (1.0_dp/kappa)*log(E*delta_w_plus(p))
      endif
    enddo
  end subroutine eval_u_plus
  
  subroutine eval_T_plus(face,side,time,values)
    ! From Ignat L. et al., "Adaptive computations of turbulent forced convection",
    ! Numerical Heat Transfer, Part A, 34: 847-871, 1998.
    type(face_list), intent(in) :: face
    integer, intent(in) :: side, time
    real(dp), intent(out) :: values(:)
    
    real(dp) :: delta_w_plus(face%nqp), nu(face%nqp), alpha(face%nqp)
    real(dp) :: Pr, delta_w_plus1, beta
    real(dp), parameter :: delta_w_plus2 = 10.0_dp * sqrt(10.0_dp * kappa / Pr_t)
    class(scal_func_type), pointer :: kinem_visc, therm_diff
    integer :: p
    
    kinem_visc => matprop_set%func_ptr('kinematic_viscosity')
    therm_diff => matprop_set%func_ptr('thermal_diffusivity')
    
    call eval_delta_w_plus(face,side,time,delta_w_plus)
    nu =  kinem_visc%values_at_qps(face,side,time)
    alpha =  therm_diff%values_at_qps(face,side,time)
    do p = 1, face%nqp
      Pr = nu(p) / alpha(p)
      delta_w_plus1 = 10.0_dp/(Pr**(1.0_dp/3.0_dp))
      beta = 15.0_dp*Pr**(2.0_dp/3.0_dp) - (Pr_t/(2.0_dp*kappa))*(1.0_dp + log(1.0e3_dp*kappa/Pr_t))
      
      if (delta_w_plus(p) < delta_w_plus1) then
        values(p) = Pr * delta_w_plus(p)
      elseif (delta_w_plus1 <= delta_w_plus(p) .and. delta_w_plus(p) < delta_w_plus2) then
        values(p) = 15.0_dp*Pr**(2.0_dp/3.0_dp) - 500.0_dp/(delta_w_plus(p)**2)
      else
        values(p) = (Pr_t/kappa)*log(delta_w_plus(p)) + beta
      endif
    enddo
  end subroutine eval_T_plus
  
  subroutine eval_delta_w_plus(face,side,time,values)
    type(face_list), intent(in) :: face
    integer, intent(in) :: side, time
    real(dp), intent(out) :: values(:)
    
    class(scal_func_type), pointer :: kinem_visc
    real(dp) :: u_k(face%nqp), nu(face%nqp)
    real(dp) :: delta
    
    delta = face%distance_from_elem_centroid(side,only_fluid_mesh=.true.)
    call assert(delta > 0.0_dp, 'wall functions: delta <= 0.0')
    call eval_u_k(face,side,time,u_k)
    kinem_visc => matprop_set%func_ptr('kinematic_viscosity')
    nu =  kinem_visc%values_at_qps(face,side,time)
    values = u_k * delta / nu
  end subroutine eval_delta_w_plus
  
  subroutine eval_u_k(face,side,time,values)
    type(face_list), intent(in) :: face
    integer, intent(in) :: side, time
    real(dp), intent(out) :: values(:)
    
    class(scal_func_type), pointer :: num_k, num_log_k
    real(dp) :: k(face%nqp), log_k(face%nqp)
    integer :: p
    
    select case(rans_model_description)
      case('k-eps','k-omega','k-log-w')
        num_k => num_scalfunc_set%func_ptr('spec_turb1')
        k =  num_k%values_at_qps(face,side,time)
        do p = 1, face%nqp
          k(p) = max(k(p),SMALL) !Do not cap it to zero to avoid null y_plus and slip coeff.
        enddo
      case('log-k-log-eps')
        num_log_k => num_scalfunc_set%func_ptr('spec_turb1')
        log_k =  num_log_k%values_at_qps(face,side,time)
      case default
        call error('Unkown rans_model_description: ' // rans_model_description)
    end select
      
    select case(rans_model_description)
      case('k-eps')
        values = (C_mu**0.25_dp) * sqrt(k)
      case('k-omega','k-log-w')
        values = (beta_star**0.25_dp) * sqrt(k)
      case('log-k-log-eps')
        values = (C_mu**0.25_dp) * exp(log_k/2.0_dp)
      case default
        call error('Unkown rans_model_description: ' // rans_model_description)
    end select
  end subroutine eval_u_k
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  pure integer function  no_dirs_wf_bc_rho_eps(this) result(no_dirs)
    class(wf_bc_rho_eps_t), intent(in) :: this
    
    no_dirs = 1
  end function  no_dirs_wf_bc_rho_eps
  
  pure integer function  no_dirs_wf_bc_rho_log_eps(this) result(no_dirs)
    class(wf_bc_rho_log_eps_t), intent(in) :: this
    
    no_dirs = 1
  end function  no_dirs_wf_bc_rho_log_eps
  
  pure integer function  no_dirs_wf_bc_rho_omega(this) result(no_dirs)
    class(wf_bc_rho_omega_t), intent(in) :: this
    
    no_dirs = 1
  end function  no_dirs_wf_bc_rho_omega
  
  pure integer function  no_dirs_wf_bc_rho_log_w(this) result(no_dirs)
    class(wf_bc_rho_log_w_t), intent(in) :: this
    
    no_dirs = 1
  end function  no_dirs_wf_bc_rho_log_w
end module rans_wall_functions_mod
