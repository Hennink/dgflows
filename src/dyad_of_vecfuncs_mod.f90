module dyad_of_vecfuncs_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use dyad_mod, only: dyad_t
  use vec_func_mod, only: vec_func_type
  implicit none

  private
  public :: dyad_of_vecfuncs_t, dyad_of_vecfuncs

  type, extends(vec_func_type) :: dyad_of_vecfuncs_t
    ! vec_func 'v' with components
    !     v_i = dyad(arg1_i,arg2_i)
    private
    class(vec_func_type), allocatable :: arg1, arg2
    class(dyad_t), allocatable :: dyad
  contains
    procedure :: get_values
    procedure :: no_dirs
  end type dyad_of_vecfuncs_t

contains
  type(dyad_of_vecfuncs_t) function dyad_of_vecfuncs(dyad,arg1,arg2)
    class(dyad_t), intent(in) :: dyad
    class(vec_func_type), intent(in) :: arg1, arg2

    call assert(arg1%no_dirs() == arg2%no_dirs())

    allocate(dyad_of_vecfuncs%dyad,source=dyad)
    allocate(dyad_of_vecfuncs%arg1,source=arg1)
    allocate(dyad_of_vecfuncs%arg2,source=arg2)
  end function dyad_of_vecfuncs


  pure integer function no_dirs(this)
    class(dyad_of_vecfuncs_t), intent(in) :: this

    no_dirs = this%arg1%no_dirs()
    if (no_dirs /= this%arg2%no_dirs()) error stop "ERROR in dyad_of_vecfuncs_mod:no_dirs"
  end function no_dirs

  subroutine get_values(this,elem,pnt_set,time,values)
    use f90_kind, only: dp
    use mesh_objects, only: elem_list
    use pnt_set_mod, only: pnt_set_t

    class(dyad_of_vecfuncs_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:,:)

    real(dp) :: values_2(size(values,1),size(values,2))

    call assert(size(values,1) == this%no_dirs(),BOUNDS_CHECK)
    call assert(size(values,2) == pnt_set%np(),BOUNDS_CHECK)

    call this%arg1%get_values(elem,pnt_set,time,values)
    call this%arg2%get_values(elem,pnt_set,time,values_2)
    values = this%dyad%func(values,values_2)
  end subroutine get_values
end module dyad_of_vecfuncs_mod
