module lincom_rank1_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use dof_handler, only: dof_handler_type
  use mesh_objects, only: face_list, elem_list
  implicit none
  
  private
  public :: lincom_rank1_t
  public :: lincom_for_n_dot_rank1
  
  type :: lincom_rank1_t
    integer :: elem
    real(dp), allocatable :: stencil(:,:,:,:)
    integer :: time = 1 ! time at which to evaluate the lincom explicitly
  contains
    procedure, non_overridable :: init
    procedure, non_overridable :: no_dirs, np
    procedure, non_overridable :: mult => mult_scalar
    procedure, non_overridable :: eval

    procedure, non_overridable :: set_stencil_for_func
    generic :: set_as_func => set_as_func_on_elem, set_as_func_on_face
    procedure, non_overridable :: set_as_func_minus_normal_component
    procedure, non_overridable, private :: set_as_func_on_elem, set_as_func_on_face
  end type lincom_rank1_t
  
  interface operator(.dot.)
    module procedure known_vec_dot_lincom_rank1
  end interface operator(.dot.)
  
contains
  subroutine set_as_func_on_elem(this,elem,handler)
    class(lincom_rank1_t), intent(out) :: this
    type(elem_list), intent(in) :: elem
    type(dof_handler_type), intent(in) :: handler
    
    integer :: elem_no

    elem_no = elem%get_active_elem_no(handler%only_fluid_mesh)
    call this%set_stencil_for_func(elem_no,handler,elem%quad_points)
  end subroutine set_as_func_on_elem
  
  subroutine set_as_func_on_face(this,face,side,handler)
    class(lincom_rank1_t), intent(out) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side
    type(dof_handler_type), intent(in) :: handler
    
    integer :: elem_no

    associate(elem => face%elem_neighbors(side)%point)
      elem_no = elem%get_active_elem_no(handler%only_fluid_mesh)
      call this%set_stencil_for_func(elem_no,handler,face%quad_points(side))
    end associate
  end subroutine set_as_func_on_face
  
  subroutine set_stencil_for_func(this,elem_no,handler,pnt_set)
    use pnt_set_mod, only: pnt_set_t
    
    class(lincom_rank1_t), intent(out) :: this
    integer, intent(in) :: elem_no
    type(dof_handler_type), intent(in) :: handler
    type(pnt_set_t), intent(in) :: pnt_set
    
    integer :: dir, p, nnod
    
    nnod = handler%no_nod_elem(elem_no)
    call this%init(handler%no_dirs,elem_no,handler,pnt_set%np())
    do p = 1, pnt_set%np()
      do dir = 1, this%no_dirs()
        this%stencil(:,dir,dir,p) = pnt_set%dg_funcs(:nnod,p)
      enddo
    enddo
  end subroutine set_stencil_for_func
  
  subroutine init(this,no_dirs,elem_no,handler,no_points)
    class(lincom_rank1_t), intent(out) :: this
    integer, intent(in) :: no_dirs
    integer, intent(in) :: elem_no
    type(dof_handler_type), intent(in) :: handler
    integer, intent(in) :: no_points
    
    integer :: no_nod
    
    if (handler%no_grps /= 1) error stop "don't know to which group I belong"
    
    this%elem = elem_no
    no_nod = handler%no_nod_elem(elem_no)
    allocate(this%stencil(no_nod,handler%no_dirs,no_dirs,no_points))
    this%stencil = 0.0_dp
  end subroutine init
  
  pure integer function no_dirs(this)
    class(lincom_rank1_t), intent(in) :: this
    
    no_dirs = size(this%stencil,3)
  end function no_dirs
  
  pure integer function np(this)
    class(lincom_rank1_t), intent(in) :: this
    
    np = size(this%stencil,4)
  end function
  
  pure subroutine mult_scalar(this,scalar)
    class(lincom_rank1_t), intent(inout) :: this
    real(dp), intent(in) :: scalar
    
    this%stencil = this%stencil * scalar
  end subroutine mult_scalar
  
  function eval(this,solvec) result(values)
    use solvec_mod, only: solvec_t
    
    class(lincom_rank1_t), intent(in) :: this
    type(solvec_t), intent(in) :: solvec
    real(dp) :: values(this%no_dirs(),this%np())

    real(dp), contiguous, pointer :: sol_coeffs(:,:)
    integer :: p, dir
    
    sol_coeffs => solvec%nodes_dirs_ptr(this%elem,this%time)

    do p = 1, this%np()
      do dir = 1, this%no_dirs()
        values(dir,p) = sum(sol_coeffs * this%stencil(:,:,dir,p))
      enddo
    enddo
  end function eval
  
  subroutine set_as_func_minus_normal_component(this,face,side,func)
    class(lincom_rank1_t), intent(out) :: this
    type(face_list), target, intent(in) :: face
    integer, intent(in) :: side
    type(lincom_rank1_t), intent(in) :: func
    
    integer :: p, dir
    real(dp):: n_dot_func(size(func%stencil,1),size(func%stencil,2))
    
    call assert(side == 1 .or. side == 2)
    
    this%elem = func%elem
    allocate(this%stencil,mold=func%stencil)
    this%stencil = 0.0_dp
    do p = 1, func%np()
      n_dot_func = 0.0_dp
      do dir = 1, func%no_dirs()
        n_dot_func = n_dot_func + func%stencil(:,:,dir,p) * face%normal(dir,p)
      enddo
      do dir = 1, func%no_dirs()
        this%stencil(:,:,dir,p) = func%stencil(:,:,dir,p) - n_dot_func * face%normal(dir,p)
      enddo
    enddo
  end subroutine set_as_func_minus_normal_component
  
  function lincom_for_n_dot_rank1(face,side,lincom_rank1) result(n_dot_rank1)
    type(face_list), target, intent(in) :: face
    integer, intent(in) :: side
    type(lincom_rank1_t), intent(in) :: lincom_rank1
    type(lincom_rank1_t) :: n_dot_rank1
    
    call assert(side == 1 .or. side == 2)
    n_dot_rank1 = face%normal .dot. lincom_rank1
  end function lincom_for_n_dot_rank1
  
  type(lincom_rank1_t) function known_vec_dot_lincom_rank1(vec_values,lincom_rank1) result(dotprod)
    class(lincom_rank1_t), intent(in) :: lincom_rank1
    real(dp), intent(in) :: vec_values(:,:)
    
    integer :: np, no_dirs
    integer :: p, dir
    
    no_dirs = assert_eq(size(vec_values,1),lincom_rank1%no_dirs(),BOUNDS_CHECK)
    np = assert_eq(size(vec_values,2),lincom_rank1%np(),BOUNDS_CHECK)
    
    dotprod%elem = lincom_rank1%elem
    allocate(dotprod%stencil(size(lincom_rank1%stencil,1),size(lincom_rank1%stencil,2),1,np))
    dotprod%stencil = 0.0_dp
    do p = 1, np
      do dir = 1, no_dirs
        dotprod%stencil(:,:,1,p) = dotprod%stencil(:,:,1,p) + lincom_rank1%stencil(:,:,dir,p) * vec_values(dir,p)
      enddo
    enddo
  end function known_vec_dot_lincom_rank1
end module lincom_rank1_mod
