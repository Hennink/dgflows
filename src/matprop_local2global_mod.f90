module matprop_local2global_mod
  use assertions, only: assert, assert_eq
  use func_set_mod, only: func_set_t
  use matprop_scalfunc_mod, only: scalfuncs_of_matprop_t
  implicit none
  
  private
  public :: build_global_set_from_local_matprop_sets

contains
  subroutine build_global_set_from_local_matprop_sets(material_ids,l_matprop_sets,g_matprop_set)
    use string_mod, only: string_array_t, string_t
    use scal_func_mod, only: scal_func_type

    integer, intent(in) :: material_ids(:)
    type(func_set_t), intent(in) :: l_matprop_sets(:)
    type(func_set_t), intent(out) :: g_matprop_set

    type(scalfuncs_of_matprop_t) :: global_matprop_func
    type(string_array_t) :: list_matprop_names
    type(string_t), dimension(:), allocatable :: matprop_names
    integer :: idx

    ! ATTENTION: THE FOLLOWING STEPS COULD DEFINITELY NOT BE THE BEST SOLUTION
    ! Several operations must be carried out:
    ! 1) Each set correspond to the list of matprop functions in a specific material.
    !    Some materials have more matprops than others (e.g., viscosity is not defined
    !    in a solid region). Some others are in common to all materials (e.g., density).
    !    We first need to create a list of all possible matprops throughout the domain.
    ! 2) Then we loop over the matprop names in this list
    ! 3) For each name, we check in which materials this matprop is actually defined,
    !    building a suitable variable called id_matprop_pairs, which stores the mat_id
    !    and the matprop function in that material
    ! 4) We create a suitable scalfunc storing all of these pairs id_matprop
    ! 5) This function is finally added to the global_matprop_set

    call assert(size(material_ids) == size(l_matprop_sets))

    !STEP 1)
    call build_list_of_all_unique_matprop_names(l_matprop_sets,list_matprop_names)

    !STEP 2)
    call list_matprop_names%get(matprop_names)
    do idx = 1, size(matprop_names)
      !STEP 3/4)
      call build_global_matprop_func(material_ids,l_matprop_sets,matprop_names(idx)%get(),global_matprop_func)
      !STEP 5)
      call g_matprop_set%add_func(matprop_names(idx)%get(),global_matprop_func)
    enddo
  end subroutine build_global_set_from_local_matprop_sets

  subroutine build_list_of_all_unique_matprop_names(l_matprop_sets,list_matprop_names)
    use func_set_mod, only: func_set_t
    use string_mod, only: string_t, string_array_t

    type(func_set_t), dimension(:), intent(in) :: l_matprop_sets
    type(string_array_t), intent(out) :: list_matprop_names

    integer :: loc_id, name_id
    type(string_t), dimension(:), allocatable :: l_set_names

    do loc_id = 1, size(l_matprop_sets)
      call l_matprop_sets(loc_id)%get_names(l_set_names)
      do name_id = 1, size(l_set_names)
        call list_matprop_names%add(l_set_names(name_id))
      enddo
    enddo
  end subroutine build_list_of_all_unique_matprop_names

  subroutine build_global_matprop_func(material_ids,l_matprop_sets,name,global_matprop_func)
    use scal_func_mod, only: scal_func_type

    integer, intent(in) :: material_ids(:)
    type(func_set_t), intent(in) :: l_matprop_sets(:)
    character(*), intent(in) :: name
    type(scalfuncs_of_matprop_t), intent(out) :: global_matprop_func

    integer :: loc_id, N
    class(scal_func_type), allocatable :: l_matprop

    N = assert_eq(size(material_ids),size(l_matprop_sets))

    do loc_id = 1, N
      ! Loop over the matprop_sets and see whether the matprop with the selected name
      ! is present or not
      if (l_matprop_sets(loc_id)%has(name)) then
        ! The selected matprop is defined in the material => add it to global_matprop_func
        call l_matprop_sets(loc_id)%get_func(name,l_matprop)
        call global_matprop_func%add_id_matprop(material_ids(loc_id),l_matprop)
      else
        !Do nothing, the matprop is not defined in the material
      endif
    enddo
  end subroutine build_global_matprop_func
end module matprop_local2global_mod
