module string_manipulations
  use assertions, only: assert
  implicit none
  
  private
  public :: rstrip
  public :: starts_with, ends_with
  public :: char2bool, char2int, char2real
  public :: int2char
  public :: separate_first_word
  public :: split, split2chars
  
  character, parameter :: blank_char =  ' '
  
contains
  function rstrip(word, r)
    character(*), intent(in) :: word, r
    character(:), allocatable :: rstrip

    if (ends_with(word, r)) then
      rstrip = word(:len(word)-len(r))
    else
      rstrip = word
    endif
  end function

  logical function starts_with(word, start)
    character(*), intent(in) :: word, start

    if (len(start) > len(word)) then
      starts_with = .false.
    else
      starts_with = word(:len(start)) == start
    endif
  end function

  logical function ends_with(word, end)
    character(*), intent(in) :: word, end

    ends_with = starts_with(reverse(word), reverse(end))
  end function

  function reverse(word)
    character(*), intent(in) :: word
    character(len(word)) :: reverse

    integer :: i

    do i = 1, len(word)
      reverse(i:i) = word(len(word)-i+1 : len(word)-i+1)
    enddo
  end function

  function split(string,split_chars) result(words)
    use string_mod, only: string_t

    character(*), intent(in) :: string
    character(*), optional, intent(in) :: split_chars
    type(string_t), allocatable :: words(:)

    character(:), allocatable :: words_as_char(:)
    integer :: i, no_words

    call split2chars(string,words_as_char,split_chars)
    no_words = size(words_as_char)
    allocate(words(no_words))
    do i = 1, no_words
      call words(i)%set(trim(words_as_char(i)))
    enddo
  end function split

  subroutine split2chars(string,words,split_chars)
    character(*), intent(in) :: string
    character(:), dimension(:), allocatable, intent(out) :: words
    character(*), optional, intent(in) :: split_chars
    
    character(:), allocatable :: local_split_chars
    integer, allocatable, dimension(:) :: positions
    integer :: i
    integer :: low, up
    integer :: no_words
    
    if (present(split_chars)) then
      allocate(local_split_chars,source=split_chars)
    else
      allocate(local_split_chars,source=blank_char)
    endif
    
    call find_substring_positions(string,local_split_chars,positions)
    no_words = size(positions) + 1
    allocate(character(len(string)) :: words(no_words))
    do i = 1, no_words
      if (i == 1) then
        low = 1
      else
        low = positions(i-1) + len(local_split_chars)
      endif
      if (i == no_words) then
        up = len(string)
      else
        up = positions(i) - 1
      endif
      words(i) = string(low:up)
    enddo
    
    words = adjustl(words)
  end subroutine split2chars
  
  pure subroutine separate_first_word(string,first_word,remainder,splitter)
    character(*), intent(in) :: string
    character(:), allocatable, intent(out) :: first_word, remainder
    character(*), optional, intent(in) :: splitter
    
    integer :: pos
    character(:), allocatable :: my_splitter
    
    if (present(splitter)) then
      my_splitter = splitter
    else
      my_splitter = blank_char
    endif
    pos = index(string, my_splitter)
    if (pos == 0) pos = len(string) + 1
    first_word = trim(adjustl(string(:pos-1)))
    remainder  = trim(adjustl(string(pos+len(my_splitter):)))
  end subroutine
  
  subroutine find_substring_positions(string,substring,positions)
    use binary_tree, only: int_set
    
    character(*), intent(in) :: string
    character(*), intent(in) :: substring
    integer, allocatable, dimension(:), intent(out) :: positions
    
    type(int_set) :: set_of_positions
    integer :: search_idx, p, pos
    
    if (len(substring) /= 0) then
      search_idx = 1
      do
        p = index(string(search_idx:),substring)
        if (p == 0) exit
        pos = search_idx + p - 1
        call set_of_positions%insert(pos)
        search_idx = pos + len(substring)
      enddo
    endif
    
    call set_of_positions%sorted_values(positions)
  end subroutine find_substring_positions
  
  real(dp) function char2real(c) result(r)
    use, intrinsic :: iso_fortran_env, only: ERROR_UNIT
    use f90_kind, only: dp

    character(*), intent(in) :: c

    integer :: iostat
    character(200) :: iomsg

    read(c,*,iostat=iostat,iomsg=iomsg) r
    call assert(iostat==0,iomsg)
  end function

  impure elemental integer function char2int(string) result(val)
    character(*), intent(in) :: string
    
    character(:), allocatable :: fmt
    integer :: iostat
    character(200) :: iomsg
    
    fmt = '(1i' // int2char(len(string)) // ')'
    read(string,fmt,iostat=iostat,iomsg=iomsg) val
    call assert(iostat==0,iomsg)
  end function char2int
  
  logical function char2bool(string) result(val)
    character(*), intent(in) :: string

    character(:), allocatable :: fmt
    integer :: iostat
    character(200) :: iomsg

    fmt = '(1L' // int2char(len(string)) // ')'
    read(string, fmt, iostat=iostat, iomsg=iomsg) val
    call assert(iostat==0, 'char2bool: cannot parse string...' // new_line('a') // 'string=' // string // new_line('a') // iomsg)
  end function char2bool

  pure function int2char(I) result(C)
    integer, intent(in) :: I
    character(:), allocatable :: C
    
    character(1000) :: big_char
    
    write(big_char,'(1i100)') I
    allocate(C,source=trim(adjustl(big_char)))
  end function int2char
end module string_manipulations

