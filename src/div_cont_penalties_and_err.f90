module div_cont_penalties_and_err
  use assertions, only: assert
  use dof_handler, only: dof_handler_type, set_type_and_preallocate
  use exceptions, only: error
  use f90_kind, only: dp
  use face_integrals_mod, only: continuity_error
  use functionals, only: divergence_error
  use io_basics, only: nreals_fmt
  use local_linsys_mod, only: local_linsys_t
  use mesh_objects, only: elem_list, elem_ptr, elem_id, face_list, face_ptr
  use mesh_partitioning, only: my_first_last_elems
  use mesh_type, only: fem_mesh
  use mass_mtx, only: get_mass_matrix
  use numerical_solutions, only: get_solvec, get_dof_handler
  use petsc_ksp_mod, only: petsc_ksp_t
  use petsc_mod, only: I_am_master_process, no_petsc_procs
  use petsc_mat_mod, only: petsc_mat_type, subset_nonzero_pattern_opt
  use petsc_solver_mod, only: set_up, solve_system
  use petsc_vec_mod, only: petsc_vec_type
  use phys_time_mod, only: old_times, nold_times
  use pressure, only: gradient_oper
  use run_data, only: m_p_strategy, check_mat_symm, print_mtx_sparsity_info, mesh_dimen, &
      add_m_penalties, m_penalties_in, m_continuity_penal_is_time_independent, &
      div_m_pen_pointwise, div_m_pen_safety_factor, cont_m_pen_safety_factor
  use solvec_mod, only: solvec_t
  implicit none
  
  private
  public :: init_M_corr_linsys, solve_M_corr_linsys
  public :: assemble_m_pen_linsys
  public :: manage_div_and_cont_errs_file, output_div_and_continuity_errors
  
  integer :: masters_div_cont_errs_unit
  
  type(petsc_mat_type), protected :: M_corr_mat
  type(petsc_vec_type), protected :: M_corr_vec
  logical, protected :: M_corr_linsys_initialized = .false.
  type(petsc_ksp_t), protected :: M_corr_ksp
  type(petsc_mat_type), protected :: mass_matrix
  
contains
  subroutine manage_div_and_cont_errs_file(what_to_do)
    character(*), intent(in) :: what_to_do
    
    integer :: iostat
    character(200) :: iomsg
    
    if (I_am_master_process()) then
      select case(what_to_do)
        case('open_new')
          open(newunit=masters_div_cont_errs_unit,file='div_and_continuity_errors', &
             status='replace',pad='no',position='rewind',action='write',form='formatted', &
             iostat=iostat,iomsg=iomsg)
          call assert(iostat==0,iomsg)
            write(masters_div_cont_errs_unit,'(3(a,17x))',iostat=iostat,iomsg=iomsg) '# phys_time','div_error','continuity_error'
            call assert(iostat==0,iomsg)
        case('close')
          close(masters_div_cont_errs_unit,iostat=iostat,iomsg=iomsg)
          call assert(iostat==0,iomsg)
        case default
          call error("Don't know what to do with the div_and_cont_errs_file: "//trim(what_to_do))
        end select
    endif
  end subroutine manage_div_and_cont_errs_file
  
  subroutine output_div_and_continuity_errors(mesh)
    type(fem_mesh), intent(in) :: mesh
    
    real(dp) :: err_div, err_cont
    integer :: iostat
    character(200) :: iomsg
    
    err_div = divergence_error(mesh)
    err_cont = continuity_error(mesh)
    
    if (I_am_master_process()) then
      write(masters_div_cont_errs_unit,nreals_fmt(3),iostat=iostat,iomsg=iomsg) old_times(1), err_div, err_cont
      call assert(iostat == 0,iomsg)
    endif
  end subroutine output_div_and_continuity_errors
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine solve_M_corr_linsys(mesh,dt,pc_poisson_sol)
    ! Solve the system (see [1]):
    ! (mass_matrix + div_penalty + continuity_penalty) * M = b + b_cont_pen
    ! 
    ! where the b vector is:
    ! o mass_matrix * M                                     , for coupled solver (see p. 674)
    ! o mass_matrix * M - div_operator^T * pc_poisson_sol   , for pressure-correction (see p. 691)
    ! 
    ! [1] Fehn N. et al., "Robust and efficient dG methods for under-resolved turbulent incompressible flows",
    !     Journal of Comp. Phys. 372 (2018), pp. 667-693
    type(fem_mesh), intent(in) :: mesh
    real(dp), intent(in) :: dt
    type(petsc_vec_type), intent(in), optional :: pc_poisson_sol
    
    type(petsc_vec_type) :: M_src_petsc, grad_pc_poisson_sol
    type(solvec_t), pointer :: M
    real(dp) :: norm_M_rhs, final_rel_norm_res
    
    if (present(pc_poisson_sol)) call assert(m_p_strategy == 'pressure-correction')
    if (.not. present(pc_poisson_sol)) call assert(m_p_strategy == 'fieldsplit')
    
    M => get_solvec('mass_flux')
    
    ! 1) update solver M_corr_mat matrix, if necessary
    if (mod(nold_times,add_m_penalties)==0 .or. (.not. M_corr_linsys_initialized)) then
      call assemble_m_pen_linsys(mesh,dt,M,M_corr_linsys_initialized,M_corr_mat,M_corr_vec)
      call M_corr_mat%axpy(1.0_dp,mass_matrix,subset_nonzero_pattern_opt)
      if (check_mat_symm) call assert(M_corr_mat%is_symm(),'mass_flux correction matrix is not symmetric')
      if (print_mtx_sparsity_info) then
        write(*,'(a)',advance='no') 'mass_flux correction matrix: '
        call M_corr_mat%print_sparsity_info()
      endif
      call M_corr_ksp%replace_operator('Amat', M_corr_mat)
    endif
    ! 2) Build rhs
    call M_corr_vec%duplicate(M_src_petsc)
    call M_corr_vec%copy(M_src_petsc)
    call mass_matrix%mult_add(M%petsc_coeffs,M_src_petsc,M_src_petsc)
    if (present(pc_poisson_sol)) then
      call M_src_petsc%duplicate(grad_pc_poisson_sol)
      call gradient_oper%mult(pc_poisson_sol,grad_pc_poisson_sol)
      call M_src_petsc%subtract_vec(grad_pc_poisson_sol)
    endif
    norm_M_rhs = M_src_petsc%norm_2()
    if (I_am_master_process()) write(*,'(a,es8.1)') 'mass_flux correction: norm of rhs.: ', norm_M_rhs
    ! 3) Solve the system
    call solve_system(M_corr_ksp, M%petsc_coeffs, M_src_petsc)
    final_rel_norm_res = M_corr_mat%rel_norm_residual(M%petsc_coeffs,M_src_petsc)
    if (I_am_master_process()) write(*,"(a,es8.1)") 'rel. norm residual after solve: ', final_rel_norm_res
    
    call M%update_ghost_values()
  end subroutine solve_M_corr_linsys
  
  subroutine init_M_corr_linsys(mesh)
    type(fem_mesh), intent(in) :: mesh
    
    type(dof_handler_type), pointer :: M_dof_handler
    
    M_dof_handler => get_dof_handler('mass_flux')
    
    call get_mass_matrix(mesh,M_dof_handler,mass_matrix)
    
    call set_up(M_corr_ksp, 'mcorr')
    call M_corr_ksp%set_operators(mass_matrix, mass_matrix)
  end subroutine
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine assemble_m_pen_linsys(mesh,dt,M,M_pen_linsys_initialized,M_pen_mtx,M_pen_rhs)
    ! Definitions of penalty matrices and penalty coefficients have been taken from [1].
    ! Continuity penalty was extended to include boundary terms (because in [1] they use a definition
    ! of jump which vanishes at the boundaries. This is not valid in our case.)
    !
    ! [1] Fehn N. et al., "Robust and efficient dG methods for under-resolved turbulent incompressible flows",
    !     Journal of Comp. Phys. 372 (2018), pp. 667-693
    use pde_mod, only: pde_t
    use continuity_penal_mod, only: continuity_penal_t
    
    type(fem_mesh), intent(in) :: mesh
    real(dp), intent(in) :: dt
    type(solvec_t), intent(in) :: M
    logical, intent(inout) :: M_pen_linsys_initialized
    type(petsc_mat_type), intent(inout) :: M_pen_mtx
    type(petsc_vec_type), intent(inout) :: M_pen_rhs
    
    type(pde_t) :: pde
    type(continuity_penal_t) :: continuity_penal
    
    if (.not. M_pen_linsys_initialized) then
      call M_pen_mtx%create()
      call set_type_and_preallocate(M_pen_mtx, mesh, M%handler, M%handler, coupling_depth=1, symm=.true., force_aij = .false.)
      call M_pen_mtx%init_compatible_vecs(left_vec=M_pen_rhs)
      M_pen_linsys_initialized = .true.
    else
      call M_pen_mtx%zero_entries()
      call M_pen_rhs%zero_entries()
    endif
    
    call pde%add_term(continuity_penal)
    call pde%add_to_glob_linsys(M, mesh, M_pen_mtx, M_pen_rhs)

    call M_pen_mtx%finalize_assembly()
    call M_pen_rhs%finalize_assembly()
  end subroutine assemble_m_pen_linsys
end module
