module array_opers_mod
  use blas95, only: gemm
  use f90_kind, only: dp
  use assertions, only: assert_eq, BOUNDS_CHECK
  use, intrinsic :: iso_fortran_env, only: real32, real64
  implicit none

  private
  public :: copy_lower_triangular_part, copy_lower2upper_triangle
  public :: gemv_classic, gemm_classic
  public :: add_Aik_Bjk_wk
  public :: add_tensprod, add_weighted_tensprod, subtract_tensprod
  public :: frobnorm_symm_mtx, sum_entrywise_prod_symm_mtx

contains
  subroutine copy_lower_triangular_part(matrix,copy)
    real(dp), intent(in) :: matrix(:,:)
    real(dp), intent(out) :: copy(:,:)

    integer :: i, j, n

    n = assert_eq(size(matrix,1),size(matrix,2),size(copy,1),size(copy,2),BOUNDS_CHECK)
    do j = 1, n
      do i = j, n
        copy(i,j) = matrix(i,j)
      enddo
    enddo
  end subroutine copy_lower_triangular_part

  subroutine copy_lower2upper_triangle(mat)
    real(dp), intent(inout) :: mat(:,:)

    integer :: i, j

    do j = 1, assert_eq(shape(mat))
      do i = 1, j-1
        mat(i,j) = mat(j,i)
      enddo
    enddo
  end subroutine copy_lower2upper_triangle

  subroutine gemm_classic(TRANSA, TRANSB, M, N, K, ALPHA, A, LDA, B, LDB, BETA, C, LDC)
    !      C = alpha*op( A )*op( B ) + beta*C
    ! 
    ! The classic BLAS interface can be useful when we know that the arrays have the
    ! right memory lay-out, even if they do not have the right shape/rank in Fortran.
    ! 
    ! NOTE: This might only work with unit-strided (i.e., contiguous) arrays!
    ! 
    external :: sgemm, dgemm

    logical, intent(in) :: TRANSA, TRANSB
    integer, intent(in) :: M, N, K
    real(dp) :: alpha, beta
    integer, intent(in) :: LDA, LDB, LDC
    real(dp) :: A(lda,*), B(ldb,*) ! probably `intent(in)`, unless it can be aliased with `C`
    real(dp) :: C(ldc,*) ! intent(OUT / INOUT), depending on 'beta'

    if (dp == real32) then
      call sgemm(merge('T','N',TRANSA), merge('T','N',TRANSB), M, N, K, ALPHA, A, LDA, B, LDB, BETA, C, LDC)
    elseif (dp == real64) then
      call dgemm(merge('T','N',TRANSA), merge('T','N',TRANSB), M, N, K, ALPHA, A, LDA, B, LDB, BETA, C, LDC)
    else
      error stop "BLAS gemm: bad `dp` value"
    end if
  end subroutine

  subroutine gemv_classic(TRANS, M, N, ALPHA, A, LDA, X, INCX, BETA, Y, INCY)
    !     y := alpha*op( A )*x + beta*y
    ! 
    external :: sgemv, dgemv

    logical, intent(in) :: TRANS
    integer, intent(in) :: M, N
    real(dp), intent(in) :: alpha, beta
    integer, intent(in) :: LDA
    real(dp), intent(in) :: A(lda,*) ! not intent(in) when `alpha == 0`
    real(dp), intent(in) :: X(*), Y(*) ! intents depend on alpha, beta (possibly aliasing?)
    integer, intent(in) :: INCX, INCY ! increment for the elements of `X` / `Y`

    if (dp == real32) then
      call sgemv(merge('T', 'N', TRANS), M, N, ALPHA, A, LDA, X, INCX, BETA, Y, INCY)
    elseif (dp == real64) then
      call dgemv(merge('T', 'N', TRANS), M, N, ALPHA, A, LDA, X, INCX, BETA, Y, INCY)
    else
      error stop "BLAS gemv: bad `dp` value"
    end if
  end subroutine

  subroutine add_Aik_Bjk_wk(C, A, B, w, implementation)
    !     C_ij += A_ik * B_jk * w_k    (summation over `k`)
    ! 
    ! Element matrix assembly with numerical quadrature can always be written this way.
    ! 
    real(dp), contiguous, intent(inout) :: C(:,:)
    real(dp), contiguous, intent(in) :: A(:,:), B(:,:), w(:)
    character(*), intent(in) :: implementation

    integer :: i, ni, j, nj, k, nk

    ni = assert_eq(size(A,1), size(C,1), BOUNDS_CHECK)
    nj = assert_eq(size(B,1), size(C,2), BOUNDS_CHECK)
    nk = assert_eq(size(A,2), size(B,2), size(w), BOUNDS_CHECK)

    if (implementation == 'loop') then
      do k = 1, nk
        do j = 1, nj
          do i = 1, ni
            C(i,j) = C(i,j) + A(i,k) * B(j,k) * w(k)
          enddo
        enddo
      enddo

    else
      block
        ! This is not optimal when `A` is larger than `B`:
        real(dp) :: A_x_w(size(A,1),size(A,2))
        do k = 1, nk
          A_x_w(:,k) = A(:,k) * w(k)
        enddo
        call add_Aik_Bjk(C, A_x_w, B, implementation)
      end block
    endif
  end subroutine

  subroutine add_Aik_Bjk(C, A, B, implementation)
    !     C_ij += A_ik * B_jk     (summation over `k`)
    !     C    += A * B^T
    ! 
    real(dp), contiguous, intent(inout) :: C(:,:)
    real(dp), contiguous, intent(in) :: A(:,:), B(:,:)
    character(*), intent(in) :: implementation

    integer :: i, ni, j, nj, k, nk

    ni = assert_eq(size(A,1), size(C,1), BOUNDS_CHECK)
    nj = assert_eq(size(B,1), size(C,2), BOUNDS_CHECK)
    nk = assert_eq(size(A,2), size(B,2), BOUNDS_CHECK)

    select case(implementation)
      case('loop')
        do k = 1, nk
          do j = 1, nj
            do i = 1, ni
              C(i,j) = C(i,j) + A(i,k) * B(j,k)
            enddo
          enddo
        enddo

      case('matmul_transpose')
        C = C + matmul(A, transpose(B))

      case('BLAS95')
        call gemm(A, B, C, transb='T', beta=1.0_dp)

      case('BLAS')
        call gemm_classic(.false., .true., ni, nj, nk, 1.0_dp, A, ni, B, nj, 1.0_dp, C, ni)

      case default
        error stop "unknown: implementation"
    end select
  end subroutine

  subroutine add_tensprod(mtx,vec1,vec2)
    ! Add tensor product of 'vec1' and 'vec2' to 'mtx'.
    real(dp), intent(inout) :: mtx(:,:)
    real(dp), intent(in) :: vec1(:), vec2(:)

    call add_weighted_tensprod(mtx,vec1,vec2,1.0_dp)
  end subroutine add_tensprod

  subroutine subtract_tensprod(mtx,vec1,vec2)
    ! Subtract tensor product of 'vec1' and 'vec2' from 'mtx'.
    real(dp), intent(inout) :: mtx(:,:)
    real(dp), intent(in) :: vec1(:), vec2(:)

    call add_weighted_tensprod(mtx,vec1,vec2,-1.0_dp)
  end subroutine subtract_tensprod

  subroutine add_weighted_tensprod(mtx,vec1,vec2,alpha)
    ! Add weighted tensor product of 'vec1' and 'vec2' to 'mtx'.
    real(dp), intent(inout) :: mtx(:,:)
    real(dp), intent(in) :: vec1(:), vec2(:)
    real(dp), intent(in) :: alpha

    integer :: i, j
    integer :: n1, n2

    n1 = assert_eq(size(mtx,1),size(vec1),BOUNDS_CHECK)
    n2 = assert_eq(size(mtx,2),size(vec2),BOUNDS_CHECK)
    do j = 1, n2
      do i = 1, n1
        mtx(i,j) = mtx(i,j) + alpha * vec1(i) * vec2(j)
      enddo
    enddo
  end subroutine add_weighted_tensprod

  real(dp) function frobnorm_symm_mtx(A)
    ! Given a symmetric matrix 'A', returns the Frobenius norm:
    !                    |A|_F := sqrt(A_ij^2)
    !     frobnorm_symm_mtx(A) == sqrt(sum(A**2))
    real(dp), intent(in) :: A(:,:)

    frobnorm_symm_mtx = sqrt(sum_entrywise_prod_symm_mtx(A,A))
  end function frobnorm_symm_mtx

  real(dp) function sum_entrywise_prod_symm_mtx(A,B)
    ! Given two symmetric matrices 'A' and 'B',
    ! returns the sum of the squares of the entries of the entrywise product:
    !     sum(A * B) = A_ij B_ij.
    ! The strictly upper triangular parts of 'A' and 'B' are not referenced.
    real(dp), intent(in) :: A(:,:), B(:,:)

    integer :: i, j, n
    real(dp) :: square_lower, square_diag

    square_diag = 0.0_dp
    square_lower = 0.0_dp
    n = assert_eq(size(A,1),size(A,2),size(B,1),size(B,2),BOUNDS_CHECK)
    do j = 1, n
      square_diag = square_diag + A(j,j) * B(j,j)
      do i = j + 1, n
        square_lower = square_lower + A(i,j) * B(i,j)
      enddo
    enddo
    sum_entrywise_prod_symm_mtx = 2 * square_lower + square_diag
  end function sum_entrywise_prod_symm_mtx
end module array_opers_mod
