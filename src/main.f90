program dgflows
  use assertions, only: assert
  use boundaries, only: init_boundaries_from_file, init_total_bnd_areas
  use characteristic_len_mod, only: output_avg_mesh_size
  use continuity_penal_mod, only: preset_norm_u
  use div_cont_penalties_and_err, only: output_div_and_continuity_errors, manage_div_and_cont_errs_file
  use exceptions, only: error
  use f90_kind, only: dp
  use face_integrals_mod, only: init_face_integrals, merge_integrals, write_bnd_areas
  use fem, only: gen_quadrature
  use functions_lib, only: string2func
  use gmsh_interface, only: output_elem_partition_to_gmsh, write_to_gmsh_file, &
                            write_gmsh_mesh, write_active_elem_no_to_gmsh_file
  use initial_condition, only: set_ics => initialize
  use les_eddy_viscosity_mod, only: read_les_momentum_model_from_file, write_LES_models_to_gmsh
  use les_heat_flux_mod, only: read_les_enthalpy_model_from_file, write_heat_flux_model_to_gmsh
  use local_elem, only: init_local_elements => initialize
  use mansol_mod, only: compare_solution_with_mansol, read_mansol_set
  use matprop_mod, only: init_matprop => initialize
  use mesh_partitioning, only: init_partition_and_renumber_mesh, io_elem_no_permutations
  use mesh_refinement, only: mark_and_refine_elems_near_bnd
  use mesh_type, only: fem_mesh, gen_elem_lists, mesh_state, retrieve_mesh_state
  use multiphysics_mod, only: init_fission_power_for_CFD_calc, output_CFD_quantities_for_RT_calc, &
                              init_and_write_CFD_mesh_state
  use numerical_solutions, only: progress_time_and_all_coeffs, &
                                 init_numsol => initialize, &
                                 io_all_coeffs, print_max_ddt, &
                                 get_solvec, postprocess, &
                                 solve_for_energy, solve_for_mtum_and_press, we_solve_for, all_qnty_names, &
                                 write_numsols_and_matprops_to_gmsh_files, print_total_ndofs
  use petsc_mod, only: init_petsc, stop_petsc, init_petsc_logstages, switch_logstage, &
      I_am_master_process, no_petsc_procs, set_petsc_options_from_input_list
  use phys_time_mod, only: write_phys_times, old_times, phys_time, nold_times, BDF_weights_are_constant_from_now_on
  use pressure, only: init_div_operator
  use pressure_corr, only: init_pressure_correction
  use rans_mod, only: initialize_rans_model => initialize, write_nu_t_to_gmsh
  use rans_y_plus_mod, only: write_yplus
  use read_gmsh_mesh_mod, only: read_gmsh_mesh, verify_bc_mat_ids
  use run_data
  use scalfunc_from_datafile_mod, only: init_scalfunc_from_datafile, datafile_func_t
  use string_manipulations, only: int2char
  use sources_mod, only: adaptive_force, adapt_vol_force_to_flowthrough, init_source_terms => initialize
  use system_commands, only: init_system_commands, finalize_system_commands
  use timer_typedef, only: timer_type
  use time_step_mod, only: init_solvers, time_step, process_quantities, print_max_cfl, main_calc
  use vol_averages_mod, only: init_avg, init_group_avg, write_averages
  use wall_dist_mod, only: init_wall_dist
  implicit none
  
  type(fem_mesh) :: mesh
  type(mesh_state) :: RT_mesh_state
  integer :: i, t_step
  type(timer_type) :: initializations_timer, time_stepping_timer, postprocc_timer
  logical :: solve_for_engy
  integer :: unit
  integer :: iostat
  character(200) :: iomsg
  character(:), allocatable :: dt_fmt
  
  call init_petsc()
  call initializations_timer%start()
    if (i_am_master_process()) call print_meta_info()
    call read_input()

    call set_petsc_options_from_input_list(petsc_options)

    call init_system_commands()
    
    if (datafile_for_scalfunc /= '') call init_scalfunc_from_datafile()
    if (m_continuity_penal_preset_norm_u_func /= '') call string2func(m_continuity_penal_preset_norm_u_func, preset_norm_u)

    call init_local_elements()
    
    call init_matprop()
    
    call init_boundaries_from_file()
    
    call read_gmsh_mesh(mesh)
    call gen_elem_lists(mesh)
    call verify_bc_mat_ids(mesh)

    if (multiphysics_mode) call retrieve_mesh_state(mesh,RT_mesh_state)
    
    do i = 1, size(bnd_refinement_list)
      call mark_and_refine_elems_near_bnd(mesh,only_fluid_mesh=.true.,criterion=bnd_refinement_list(i))
    enddo
    
    if (multiphysics_mode .and. prec_conv_on_CFD_mesh) call init_and_write_CFD_mesh_state(mesh)
    
    call init_partition_and_renumber_mesh(mesh,no_petsc_procs())
    call init_whether_there_is_an_interface(mesh)

    call init_face_integrals(mesh)
    
    call init_numsol(mesh)
    
    call init_petsc_logstages([character(100) :: &
        'initializations', &
        'time_step_misc', &
        'first_steps', &
        all_qnty_names(), &
        'finalizations' &
    ])
    
    if (I_am_master_process()) call write_gmsh_mesh(mesh,'fort.11')
    
    call gen_quadrature(mesh)
    
    call output_avg_mesh_size(mesh)
    
    call init_total_bnd_areas(mesh)
    call write_bnd_areas(mesh)
    
    call solve_for_energy(solve_for_engy)
    if (multiphysics_mode .and. solve_for_engy) call init_fission_power_for_CFD_calc(CFD_mesh=mesh,RT_mesh_state=RT_mesh_state)
    
    call init_source_terms(mesh)
    if (use_rans_model) call initialize_rans_model()
    
    if (solve_for_mtum_and_press()) then
      call init_div_operator(mesh)
      if (m_p_strategy == 'pressure-correction') then
        call init_pressure_correction(mesh)
      elseif (m_p_strategy == 'fieldsplit') then
      else
        call error('Unknown m_p_strategy = '//m_p_strategy)
      endif
    endif
    
    call set_ics(mesh)
    
    if (wall_dist_specs_file /= '') call init_wall_dist(mesh)
    
    call init_avg(mesh,old_times(1))
    call init_group_avg(mesh,old_times(1))
    
    if (les_specs_file /= '') then
      open(newunit=unit, file=les_specs_file, &
           status='old', access='sequential', form='formatted', action='read', position='rewind', &
           iostat=iostat, iomsg=iomsg)
      call assert(iostat==0,iomsg)
      call read_les_momentum_model_from_file(unit,mesh)
      if (solve_for_engy) call read_les_enthalpy_model_from_file(unit,mesh)
      close(unit,iostat=iostat,iomsg=iomsg)
      call assert(iostat==0,iomsg)
    endif
    
    if (I_am_master_process()) call print_total_ndofs()
    
    if (solve_for_mtum_and_press() .and. print_div_and_continuity_errs>0) then
      call manage_div_and_cont_errs_file('open_new')
      call output_div_and_continuity_errors(mesh)
    endif
    
    if (solve_for_mtum_and_press() .and. calc_cfl > 0) call print_max_cfl(mesh)

    if (mansol_specs_file /= '') call read_mansol_set()

    call init_solvers()

    if (datafile_for_scalfunc /= '' .and. datafile_for_scalfunc_plot) then
      call write_to_gmsh_file(datafile_func_t(), mesh, 'datafile_func', only_fluid=.false., time=0)
    endif
  call initializations_timer%pauze()
  
  
  
  call time_stepping_timer%start()
  dt_fmt = 'f0.' // int2char(max(0, 1 + ceiling(-log10(dt))))
  do t_step = 1, no_time_steps
    if (I_am_master_process()) then
      print *
      write(*, "('**********************************************************')")
      write(*, "('TIME STEP ',i0,': t^{n+1}='," // dt_fmt // ")") nold_times, phys_time(1)
      write(*, "('**********************************************************')")
    endif
    
    call time_step(mesh)
    
    call switch_logstage('time_step_misc')
    if (calc_max_ddt > 0) then
      if (mod(nold_times,calc_max_ddt) == 0) call print_max_ddt(mesh)
    endif
    call progress_time_and_all_coeffs()
    call process_quantities(mesh)
    if (solve_for_mtum_and_press() .and. adaptive_force > 0) then
      if (mod(nold_times,adaptive_force) == 0) call adapt_vol_force_to_flowthrough(mesh)
    endif
    
    if (BDF_weights_are_constant_from_now_on()) main_calc = .true.

    if (I_am_master_process()) then
      write(*,'(a,es10.3)') 'estimated comp. time left: ', time_stepping_timer%time() &
                           * real(no_time_steps-t_step)/real(t_step)
    endif
  enddo
  call time_stepping_timer%pauze()
  
  
  call postprocc_timer%start()
  call switch_logstage('finalizations')
    call postprocess(mesh)

    if (mansol_specs_file /= '') call compare_solution_with_mansol(mesh)
    
    if (output_solvecs_to /= '') call io_all_coeffs(output_solvecs_to,from_file=.false.)
    if (output_solvecs_to /= '' .and. I_am_master_process()) call write_phys_times(output_solvecs_to // '_times')
    
    
    if (multiphysics_mode) call output_CFD_quantities_for_RT_calc(CFD_mesh=mesh,RT_mesh_state=RT_mesh_state)
    
    if (output_fields_to_gmsh) call write_numsols_and_matprops_to_gmsh_files(mesh)
    if (les_specs_file /= '') then
      if (output_fields_to_gmsh) call write_LES_models_to_gmsh(mesh)
      if (solve_for_engy) call write_heat_flux_model_to_gmsh(mesh)
    endif
    if (use_rans_model) then
      if (output_fields_to_gmsh) call write_nu_t_to_gmsh(mesh)
      if (wall_dist_specs_file /= '') call write_yplus(mesh)
    endif
    
    call write_averages(mesh)
    call merge_integrals()
    call manage_div_and_cont_errs_file('close')
    
    if (output_fields_to_gmsh .and. gmsh_output_includes_all_fields) then
      call output_elem_partition_to_gmsh(mesh,'elem_partition')
      call write_active_elem_no_to_gmsh_file(mesh,only_fluid=.false.,file_path='elem_numbers')
    endif
    if (output_solvecs_to /= '') then
      if (I_am_master_process()) call io_elem_no_permutations(file_path=output_solvecs_to//'_permutation',from_file=.false.)
    endif
    
    call finalize_system_commands()
  call postprocc_timer%pauze()
  
  if (i_am_master_process()) then
    block
      use math_const, only: SMALL
      use fieldsplit_mod, only: FS_ass_solve_timer
      use petsc_solver_mod, only: solve_timer
      use pressure_corr, only: p_solve_timer, pc_timer
      use qnty_solver_mod, only: asssolve__total_ass_ksp_solve_ghost

      real(dp) :: init, main, post, total, &
          pc, p_solve, solve, &
          as_total, as_ass, as_ksp, as_solve, as_ghost, &
          fs_total, fs_ass, fs_solve

      init = initializations_timer%time()
      main = time_stepping_timer%time()
      post = postprocc_timer%time()
      total = init + main + post
      pc = pc_timer%time()
      p_solve = p_solve_timer%time()
      solve = solve_timer%time()
      as_total = asssolve__total_ass_ksp_solve_ghost(1)%time()
      as_ass   = asssolve__total_ass_ksp_solve_ghost(2)%time()
      as_ksp   = asssolve__total_ass_ksp_solve_ghost(3)%time()
      as_solve = asssolve__total_ass_ksp_solve_ghost(4)%time()
      as_ghost = asssolve__total_ass_ksp_solve_ghost(5)%time()
      fs_total = FS_ass_solve_timer(1)%time()
      fs_ass = FS_ass_solve_timer(2)%time()
      fs_solve = FS_ass_solve_timer(3)%time()

      write(*,'(a)') 'time spent on ...: '
      write(*,'(a,f0.1," (",i0,"%)")')   '... initializations: ', init, nint(100 * init / total)
      write(*,'(a,f0.1," (",i0,"%)")')   '... time stepping:   ', main, nint(100 * main / total)
      write(*,'(a,f0.1," (",i0,"%)")')   '... post-processing: ', post, nint(100 * post / total)
      write(*,'()')
      if (m_p_strategy == 'pressure-correction') then
        write(*,'(a,f0.1," (",i0,"%)")') '... PC total:   ', pc,      nint(100 * pc / total)
        write(*,'(a,f0.1," (",i0,"%)")') '... p solves:   ', p_solve, nint(100 * p_solve / total)
      elseif (m_p_strategy == 'fieldsplit') then
        write(*,'(a,f0.1," (",i0,"%)")') '... Fieldsplit: ', fs_total, nint(100 * fs_total / total)
        if (fs_total > SMALL) write(*,'(a,i0,a,i0,a)')       '      ... of which assemble and solve is ', &
            nint(100 * fs_ass / fs_total), '% and ', nint(100 * fs_solve / fs_total), '%'
      endif

      write(*,'(a,f0.1," (",i0,"%)")')   '... assemble_and_solve: ', as_total, nint(100 * as_total / total)
      if (as_total > SMALL) write(*,'(a,4(i0,", "),a)') '      ... of which (assemble, setup KSP, solve, updating ghost, ) is &
          &(', nint(100*as_ass/as_total), nint(100*as_ksp/as_total), nint(100*as_solve/as_total), nint(100*as_ghost/as_total), ')%'

      write(*,'(a,f0.1," (",i0,"%)")')   '... all PETSc solves:   ', solve, nint(100 * solve / total)
      write(*,"('total runtime: ', f0.1, ' = ', es10.3)") total, total
    end block
  endif
  
  call stop_petsc()


contains
  subroutine init_whether_there_is_an_interface(mesh)
    use face_integrals_mod, only: list_faces
    use mesh_type, only: there_is_an_interface
    use mesh_objects, only: face_ptr

    type(fem_mesh), intent(in) :: mesh

    type(face_ptr), allocatable :: faces(:)

    call list_faces(mesh, only_fluid=.false., filter=is_interface, faces=faces)
    there_is_an_interface = size(faces) > 0
  end subroutine

  pure logical function is_interface(face)
    use code_const, only: INTERFACE_SOLID_FLUID
    use mesh_objects, only: face_list

    type(face_list), intent(in) :: face

    is_interface = face%ftype == INTERFACE_SOLID_FLUID
  end function

  subroutine print_meta_info()
    use assertions, only: assert, assert_eq
    use iso_fortran_env, only: compiler_options, compiler_version
    use exceptions, only: warning
    
    character(10**4) :: command
    integer :: length
    integer :: status
    logical :: too_long

    write(*,'(a)') 'Git commit: ' // GIT_HASH
    write(*,'(a)') 'Git branch: ' // GIT_BRANCH
    write(*,'(a)') 'compiled on ' // COMPILE_TIME
    
    write(*,'(a)') 'compiler: ' // compiler_version()
    write(*,'(a)') 'compiler options for this module: ...'
    write(*,'(a)') compiler_options()

    call get_command(command,length,status)
    too_long = assert_eq(status == -1,length > len(command))
    if (too_long) call warning('The invoking command was too long to read in its entirety.')
    call assert(status==0,'bad status in get_command')
    write(*,'(a)') 'Program was invoked with the following command:'
    write(*,'(a)') command(:length)
  end subroutine print_meta_info
end program dgflows

