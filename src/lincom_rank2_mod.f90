module lincom_rank2_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use dof_handler, only: dof_handler_type
  use f90_kind, only: dp
  use lincom_rank1_mod, only: lincom_rank1_t
  use local_elem, only: no_dg_functions
  use mesh_objects, only: elem_list, face_list
  use pnt_set_mod, only: pnt_set_t
  use run_data, only: mesh_dimen
implicit none
  
  private
  public :: lincom_rank2_t, operator(.dot.)
  public :: lincom_for_div
  
  type :: lincom_rank2_t
    private
    integer, public :: elem
    real(dp), allocatable, public :: stencil(:,:,:,:,:)
  contains
  procedure, non_overridable :: init
    procedure, non_overridable :: set_as_gder_at_pnts
    procedure, non_overridable :: mult
    procedure, non_overridable :: add_weighted_tensor_prod, &
                                  add_own_transpose, &
                                  axpy_own_trace_x_I
    generic :: operator(-) => substract
    procedure, private :: substract
    procedure, non_overridable :: trace => trace_of_rank2_lincom

    procedure, non_overridable :: lhs_no_dirs, rhs_no_dirs, no_points => rank2_lincom_np
  end type lincom_rank2_t
  
  interface operator(.dot.)
    module procedure known_vec_dot_rank2_lincom
  end interface operator(.dot.)
  
contains
  function lincom_for_div(elem,handler) result(div)
    type(elem_list), intent(in) :: elem
    type(dof_handler_type), intent(in) :: handler
    type(lincom_rank1_t) :: div
    
    type(lincom_rank2_t) :: grad
    
    integer :: active_elem_no

    active_elem_no = elem%get_active_elem_no(handler%only_fluid_mesh)
    call grad%set_as_gder_at_pnts(active_elem_no,elem%quad_points,handler)
    div = grad%trace()
  end function lincom_for_div
  
  subroutine set_as_gder_at_pnts(this,elem_no,pnt_set,handler)
    class(lincom_rank2_t), intent(out) :: this
    integer, intent(in) :: elem_no
    type(pnt_set_t), intent(in) :: pnt_set
    type(dof_handler_type), intent(in) :: handler
    
    integer :: d
    integer :: no_dg_func
    integer :: p, dir
    
    no_dg_func = handler%no_nod_elem(elem_no)
    call this%init(mesh_dimen,handler%no_dirs,elem_no,handler,pnt_set%np())
    do p = 1, pnt_set%np()
      do dir = 1, handler%no_dirs
        do d = 1, mesh_dimen
          this%stencil(:,dir,d,dir,p) = pnt_set%dg_transp_gders(:no_dg_func,d,p)
        enddo
      enddo
    enddo
  end subroutine set_as_gder_at_pnts
  
  subroutine init(this,lhs_no_dirs,rhs_no_dirs,elem_no,handler,no_points)
    class(lincom_rank2_t), intent(out) :: this
    integer :: lhs_no_dirs, rhs_no_dirs
    integer, intent(in) :: elem_no
    type(dof_handler_type), intent(in) :: handler
    integer, intent(in) :: no_points
    
    integer :: no_nod
    
    if (handler%no_grps /= 1) error stop "don't know to which group I belong"
    
    this%elem = elem_no
    no_nod = handler%no_nod_elem(elem_no)
    allocate(this%stencil(no_nod,handler%no_dirs,lhs_no_dirs,rhs_no_dirs,no_points))
    this%stencil = 0.0_dp
  end subroutine init
  
  subroutine mult(this,scal_values)
    class(lincom_rank2_t), intent(inout) :: this
    real(dp), intent(in) :: scal_values(:)
    
    integer :: p, np
    
    np = assert_eq(size(scal_values),size(this%stencil,4),BOUNDS_CHECK)
    
    do p = 1, np
      this%stencil(:,:,:,:,p) = this%stencil(:,:,:,:,p) * scal_values(p)
    enddo
  end subroutine mult
  
  subroutine add_weighted_tensor_prod(this,vec_values,lincom_vec,weight)
    class(lincom_rank2_t), intent(inout) :: this
    real(dp), intent(in) :: vec_values(:,:)
    type(lincom_rank1_t), intent(in) :: lincom_vec
    real(dp), intent(in) :: weight
    
    integer :: np, p
    integer :: lhs_no_dirs, i
    integer :: rhs_no_dirs, j
    
    lhs_no_dirs = assert_eq(size(vec_values,1),this%lhs_no_dirs(),BOUNDS_CHECK)
    np = assert_eq(size(vec_values,2),this%no_points(),lincom_vec%np(),BOUNDS_CHECK)
    rhs_no_dirs = assert_eq(this%rhs_no_dirs(),lincom_vec%no_dirs(),BOUNDS_CHECK)
    
    do p = 1, np
      do j = 1, rhs_no_dirs
        do i = 1, lhs_no_dirs
          this%stencil(:,:,i,j,p) = this%stencil(:,:,i,j,p) &
              + weight * vec_values(i,p) * lincom_vec%stencil(:,:,j,p)
        enddo
      enddo
    enddo
  end subroutine add_weighted_tensor_prod
  
  subroutine add_own_transpose(this)
    class(lincom_rank2_t), intent(inout) :: this
    
    integer :: no_dirs
    integer :: p
    integer :: i, j
    real(dp), allocatable :: new_stencil(:,:,:,:,:)
    
    no_dirs = assert_eq(this%lhs_no_dirs(),this%rhs_no_dirs(),BOUNDS_CHECK)
    
    allocate(new_stencil,mold=this%stencil)
    do p = 1, this%no_points()
      do j = 1, no_dirs
        do i = 1, no_dirs
          new_stencil(:,:,i,j,p) = this%stencil(:,:,i,j,p) + this%stencil(:,:,j,i,p)
        enddo
      enddo
    enddo
    call move_alloc(new_stencil,this%stencil)
  end subroutine add_own_transpose
  
  subroutine axpy_own_trace_x_I(this,alpha)
    class(lincom_rank2_t), intent(inout) :: this
    real(dp), intent(in) :: alpha
    
    integer :: no_dirs
    integer :: p
    integer :: i
    real(dp) :: trace(size(this%stencil,1),size(this%stencil,2))
    
    no_dirs = assert_eq(this%lhs_no_dirs(),this%rhs_no_dirs(),BOUNDS_CHECK)
    
    do p = 1, this%no_points()
      trace = 0.0_dp
      do i = 1, no_dirs
        trace = trace + this%stencil(:,:,i,i,p)
      enddo
      do i = 1, no_dirs
        this%stencil(:,:,i,i,p) = this%stencil(:,:,i,i,p) + alpha * trace
      enddo
    enddo
  end subroutine axpy_own_trace_x_I
  
  function substract(this,other) result(diff)
    class(lincom_rank2_t), intent(in) :: this, other
    type(lincom_rank2_t) :: diff
    
    diff = weighted_sum(1.0_dp,this,-1.0_dp,other)
  end function substract
  
  pure integer function lhs_no_dirs(this)
    class(lincom_rank2_t), intent(in) :: this
    
    lhs_no_dirs = size(this%stencil,3)
  end function lhs_no_dirs
  
  pure integer function rhs_no_dirs(this)
    class(lincom_rank2_t), intent(in) :: this
    
    rhs_no_dirs = size(this%stencil,4)
  end function rhs_no_dirs
  
  pure integer function rank2_lincom_np(this) result(no_points)
    class(lincom_rank2_t), intent(in) :: this
    
    no_points = size(this%stencil,5)
  end function
  
  function trace_of_rank2_lincom(this) result(trace)
    class(lincom_rank2_t), intent(in)  :: this
    type(lincom_rank1_t) :: trace
    
    integer :: p, np, i, no_dirs
    real(dp) :: trace_help(size(this%stencil,1),size(this%stencil,2))
    
    no_dirs = assert_eq(this%lhs_no_dirs(),this%rhs_no_dirs(),BOUNDS_CHECK)
    
    np = this%no_points()
    trace%elem = this%elem
    allocate(trace%stencil(size(this%stencil,1),size(this%stencil,2),no_dirs,np))
    trace%stencil = 0.0_dp
    do p = 1, np
      trace_help= 0.0_dp
      do i = 1, no_dirs
        trace_help = trace_help + this%stencil(:,:,i,i,p)
      enddo
      do i = 1, no_dirs
        trace%stencil(:,:,i,p) = trace%stencil(:,:,i,p) + trace_help
      enddo
    enddo
  end function trace_of_rank2_lincom
  
  function known_vec_dot_rank2_lincom(vec_values,lc_rank2) result(contraction)
    real(dp), intent(in) :: vec_values(:,:)
    class(lincom_rank2_t), intent(in)  :: lc_rank2
    type(lincom_rank1_t) :: contraction
    
    integer :: no_dirs, no_vec_dirs, np
    integer :: p, i, j
    
    no_vec_dirs = assert_eq(size(vec_values,1),lc_rank2%lhs_no_dirs(),BOUNDS_CHECK)
    np = assert_eq(size(vec_values,2),lc_rank2%no_points(),BOUNDS_CHECK)
    no_dirs = lc_rank2%rhs_no_dirs()
    contraction%elem = lc_rank2%elem
    
    allocate(contraction%stencil(size(lc_rank2%stencil,1),size(lc_rank2%stencil,2),no_dirs,np))
    contraction%stencil = 0.0_dp
    do p = 1, np
      do j = 1, no_dirs
        do i = 1, no_vec_dirs
          contraction%stencil(:,:,j,p) = contraction%stencil(:,:,j,p) + lc_rank2%stencil(:,:,i,j,p)*vec_values(i,p)
        enddo
      enddo
    enddo
  end function known_vec_dot_rank2_lincom
  
  function weighted_sum(w1,lc1,w2,lc2)
    class(lincom_rank2_t), intent(in) :: lc1, lc2
    real(dp), intent(in) :: w1, w2
    type(lincom_rank2_t) :: weighted_sum
    
    call assert(all(shape(lc1%stencil) == shape(lc2%stencil)),BOUNDS_CHECK)
    weighted_sum%elem = assert_eq(lc1%elem,lc2%elem,BOUNDS_CHECK)
    allocate(weighted_sum%stencil, source = w1 * lc1%stencil + w2 * lc2%stencil)
  end function weighted_sum
end module lincom_rank2_mod

