module pde_term_mod
  implicit none

  private
  public :: pde_term_t

  type, abstract :: pde_term_t
  contains
    procedure :: process_elem => ignore_elem
    procedure :: process_face => ignore_face
    procedure(ngbr_coupling_depth_i), deferred :: ngbr_coupling_depth
  end type

  abstract interface
    pure integer function ngbr_coupling_depth_i(this)
      import :: pde_term_t
      implicit none
      class(pde_term_t), intent(in) :: this
    end function
  end interface

contains
  subroutine ignore_elem(this,elem,qnty,local_linsys)
    ! Ignore elems by default. Overwrite in subclasses to implement elem terms.
    !
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: elem_list
    use solvec_mod, only: solvec_t

    class(pde_term_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys
  end subroutine

  subroutine ignore_face(this,face,Ein_is_E1,qnty,local_linsys)
    ! Ignore faces by default. Overwrite in subclasses to implement face terms.
    !
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: face_list
    use solvec_mod, only: solvec_t

    class(pde_term_t), intent(in) :: this
    type(face_list), intent(in) :: face
    logical, intent(in) :: Ein_is_E1
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys
  end subroutine
end module
