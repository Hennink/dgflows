module petsc_mat_mod
#include "petsc/finclude/petscmat.h"
  use petscmat, only: tMat
  use petscmat, only: &
      MAT_UNUSED_NONZERO_LOCATION_ERR, &
      MAT_ROW_ORIENTED,&
      MAT_SYMMETRIC, MAT_STRUCTURALLY_SYMMETRIC, &
      MAT_NEW_DIAGONALS, & ! new diagonals will be allowed (for block diagonal format only)
      MAT_IGNORE_OFF_PROC_ENTRIES, &
      MAT_USE_HASH_TABLE, & ! for repeated assembly of a matrix
      MAT_KEEP_NONZERO_PATTERN, &
      MAT_IGNORE_ZERO_ENTRIES, &
      MAT_USE_INODES, & ! (inodes = identical nodes), relevant for Aij matrices
      MAT_HERMITIAN, MAT_SYMMETRY_ETERNAL, &
      MAT_NEW_NONZERO_LOCATION_ERR, &
      MAT_IGNORE_LOWER_TRIANGULAR, & ! https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MATSEQSBAIJ.html
      ! MAT_ERROR_LOWER_TRIANGULAR ! ???
      ! MAT_GETROW_UPPERTRIANGULAR, ! ???
      MAT_SPD, &
      MAT_NO_OFF_PROC_ZERO_ROWS, &
      MAT_NO_OFF_PROC_ENTRIES, &
      MAT_NEW_NONZERO_LOCATIONS, MAT_NEW_NONZERO_ALLOCATION_ERR, MAT_SUBSET_OFF_PROC_ENTRIES, MAT_STRUCTURE_ONLY 
      ! MAT_SORTED_FULL not there for some reason (old PETSc version?)
  use petscmat, only: SAME_NONZERO_PATTERN, &
                      DIFFERENT_NONZERO_PATTERN, &
                      SUBSET_NONZERO_PATTERN
  use petscmat, only: MAT_DO_NOT_COPY_VALUES, &
                      MAT_COPY_VALUES, &
                      MAT_SHARE_NONZERO_PATTERN
  use petscmat, only: MAT_INITIAL_MATRIX, &
                      MAT_REUSE_MATRIX, &
                      MAT_IGNORE_MATRIX, &
                      MAT_INPLACE_MATRIX
  use petscsys, only: PETSC_NULL_INTEGER, PETSC_DETERMINE
  
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use exceptions, only: error
  use f90_kind, only: dp
  use petsc_basic_types_mod, only: fort2petsc, petsc2fort
  use petsc_matnullspace_mod, only: petsc_matnullspace_t
  use petsc_mod, only: petsc_initialized, petsc_mpi_comm, idx_fort2petsc, no_petsc_procs, petsc_proc_id
  use petsc_vec_mod, only: petsc_vec_type
  implicit none
  
  private
  public :: petsc_mat_type, destroy_matrix
  public :: mat_type_t
  public :: Pt_A_P, R_A_Rt
  public :: get_matnest_ISs
  
  type :: petsc_mat_type
    private
    type(tMat), public :: petsc_mat
    logical, public :: has_nullspace = .false.
    
    ! Whether it's blocked could also be determined by calling PETSC functions for
    ! mattype and block size, but that's probably not efficient in a tight loop.
    logical, public :: assemble_elemmat_as_block = .false.

    logical :: needs_destruction = .false. ! need not be PUBLIC, because `MatDestroy` is only necessary when creating a
                                            ! separate Mat (i.e., not as part of a KSP)
  contains
    procedure, non_overridable :: create
    procedure, non_overridable :: set_type
    procedure, non_overridable :: set_essential_options
    procedure, non_overridable :: set_sizes
    procedure, non_overridable :: get_size
    procedure, non_overridable :: set_from_options
    procedure, non_overridable :: view
    procedure, non_overridable :: XAIJ_set_preallocation
    procedure, non_overridable, private :: set_block_size
    procedure, non_overridable :: set_block_sizes
    procedure, non_overridable :: block_sizes
    procedure, non_overridable, private :: get_type
    procedure, non_overridable :: create_nest
    procedure, non_overridable :: nest_set_submat
    procedure, non_overridable :: zero_entries
    generic, public :: add => add_value, add_values
    procedure, non_overridable, private :: add_value, add_values
    generic :: set_values_blocked => set_values_blocked__one, set_values_blocked__many
    procedure, non_overridable, private :: set_values_blocked__one, set_values_blocked__many
    procedure, non_overridable :: finalize_assembly
    procedure, non_overridable :: axpy
    procedure, non_overridable :: mult_with_scal
    procedure, non_overridable :: mult
    procedure, non_overridable :: mult_transpose
    procedure, non_overridable :: mult_transpose_add
    procedure, non_overridable :: mult_add
    procedure, non_overridable :: mult_with_mat
    procedure, non_overridable :: mat_transpose_mult
    procedure, non_overridable :: get_transpose
    procedure, non_overridable :: duplicate
    procedure, non_overridable :: copy
    procedure, non_overridable :: init_compatible_vecs
    procedure, non_overridable :: set_option
    procedure, non_overridable :: set_nullspace_vec
    procedure, non_overridable, private :: set_nullspace
    procedure, non_overridable :: print_sparsity_info
    procedure, non_overridable, private :: get_local_info
    procedure, non_overridable :: is_symm
    procedure, non_overridable :: rel_norm_residual
    procedure, non_overridable :: convert, convert_inplace
    final :: destroy_matrix
  end type petsc_mat_type
  
  ! https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatOption.html
  ! https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatSetOption.html
  type :: mat_opt_t
    MatOption :: op
  end type mat_opt_t
  type(mat_opt_t), public, parameter ::                                         &
      ROW_ORIENTED                = mat_opt_t(MAT_ROW_ORIENTED),                &
      USE_HASH_TABLE              = mat_opt_t(MAT_USE_HASH_TABLE),              &
      SPD                         = mat_opt_t(MAT_SPD),                         &
      SYMMETRIC                   = mat_opt_t(MAT_SYMMETRIC),                   &
      SYMMETRY_ETERNAL            = mat_opt_t(MAT_SYMMETRY_ETERNAL),            &
      NEW_NONZERO_LOCATION_ERR    = mat_opt_t(MAT_NEW_NONZERO_LOCATION_ERR),    &
      NEW_NONZERO_ALLOCATION_ERR  = mat_opt_t(MAT_NEW_NONZERO_ALLOCATION_ERR),  &
      USE_INODES                  = mat_opt_t(MAT_USE_INODES),                  &
      NO_OFF_PROC_ENTRIES         = mat_opt_t(MAT_NO_OFF_PROC_ENTRIES),         &
      SUBSET_OFF_PROC_ENTRIES     = mat_opt_t(MAT_SUBSET_OFF_PROC_ENTRIES)

  ! https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatStructure.html
  type, public :: mat_struct_opt_type
    MatStructure :: petsc_str
  end type mat_struct_opt_type
  type(mat_struct_opt_type), public, parameter ::                                     &
      same_nonzero_pattern_opt      = mat_struct_opt_type(SAME_NONZERO_PATTERN),      &
      different_nonzero_pattern_opt = mat_struct_opt_type(DIFFERENT_NONZERO_PATTERN), &
      subset_nonzero_pattern_opt    = mat_struct_opt_type(SUBSET_NONZERO_PATTERN)

  ! https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatDuplicateOption.html
  type, public :: mat_duplicate_opt_t
    MatDuplicateOption :: op
  end type
  type(mat_duplicate_opt_t), public, parameter ::                             &
      DO_NOT_COPY_VALUES    = mat_duplicate_opt_t(MAT_DO_NOT_COPY_VALUES),    &
      COPY_VALUES           = mat_duplicate_opt_t(MAT_COPY_VALUES),           &
      SHARE_NONZERO_PATTERN = mat_duplicate_opt_t(MAT_SHARE_NONZERO_PATTERN)
  
  ! https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatType.html#MatType
  type mat_type_t
    MatType :: t
  end type
  type(mat_type_t), public, parameter ::  &
      AIJ   = mat_type_t(MATMPIAIJ),      &
      BAIJ  = mat_type_t(MATMPIBAIJ),     &
      SBAIJ = mat_type_t(MATMPISBAIJ)
  
  ! https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatReuse.html
  type :: mat_reuse_t
    MatReuse :: petsc
  end type
  type(mat_reuse_t), public, parameter ::                 &
      INITIAL_MATRIX  = mat_reuse_t(MAT_INITIAL_MATRIX),  &
      REUSE_MATRIX    = mat_reuse_t(MAT_REUSE_MATRIX),    &
      IGNORE_MATRIX   = mat_reuse_t(MAT_IGNORE_MATRIX),   &
      INPLACE_MATRIX  = mat_reuse_t(MAT_INPLACE_MATRIX)

  interface ! PETSc procedures without explicit interfaces:
    subroutine MatConvert(mat, newtype, reuse, M, ierr)
      use petscmat
      implicit none
      Mat :: mat ! in/out depends on `reuse`
      MatType, intent(in) :: newtype
      MatReuse, intent(in) :: reuse
      Mat :: M ! in/out depends on `reuse`
      PetscErrorCode, intent(out) :: ierr
    end subroutine
  end interface
contains
  subroutine set_essential_options(this)
    ! Set options that have to do with how DGFlows assembles the matrix.
    ! Probably should never be overwritten by command line options.
    ! 
    class(petsc_mat_type), intent(inout) :: this

    call this%set_option(ROW_ORIENTED,.true.)
    call this%set_option(NO_OFF_PROC_ENTRIES,.true.) ! All rows are assembled by the process to which they belong.

    ! Any add or insertion should produce an error if it would otherwise
    !   (i)   generate a new entry in the nonzero structure, or
    !   (ii)  generate a new entry that has not been preallocated.
    ! (These options are currently only supported for AIJ and BAIJ formats.)
    call this%set_option(NEW_NONZERO_LOCATION_ERR,.true.)
    call this%set_option(NEW_NONZERO_ALLOCATION_ERR,.true.)
    
    ! We know that the first assembly will set a superset of the off-process entries required for all subsequent assemblies:
    call this%set_option(SUBSET_OFF_PROC_ENTRIES,.true.)
  end subroutine

  subroutine create(this)
    use petscmat, only: MatCreate

    class(petsc_mat_type), intent(out) :: this
    
    PetscErrorCode :: ierr

    call MatCreate(petsc_mpi_comm, this%petsc_mat, ierr)
    call assert(ierr == 0, 'ierr /= 0 in MatCreate')

    this%needs_destruction = .true.
  end subroutine

  subroutine set_type(this, mattype)
    use petscmat, only: MatSetType

    class(petsc_mat_type), intent(inout) :: this
    type(mat_type_t), intent(in) :: mattype

    PetscErrorCode :: ierr

    call MatSetType(this%petsc_mat, mattype%t, ierr)
    call assert(ierr == 0, 'ierr /= 0 in MatSetType')
  end subroutine

  subroutine set_sizes(this, my_nrows, my_ncols)
    ! You cannot change the sizes once they have been set.
    ! The sizes must be set before MatSetUp() or MatXXXSetPreallocation() is called.
    ! 
    use petscmat, only: MatSetSizes

    class(petsc_mat_type), intent(inout) :: this
    integer, intent(in) :: my_nrows, my_ncols

    PetscErrorCode :: ierr

    call MatSetSizes(this%petsc_mat, fort2petsc(my_nrows), fort2petsc(my_ncols), PETSC_DETERMINE, PETSC_DETERMINE, ierr)
    call assert(ierr == 0, 'ierr /= 0 in MatSetSizes')
  end subroutine

  function get_size(this) result(petsc_size)
    ! Returns the global number of rows and columns in a matrix.
    ! 
    use petscmat, only: MatGetSize

    class(petsc_mat_type), intent(inout) :: this
    integer :: petsc_size(2)
    
    PetscInt :: petsc_m, petsc_n
    PetscErrorCode :: ierr

    call MatGetSize(this%petsc_mat, petsc_m, petsc_n, ierr)
    call assert(ierr == 0, 'ierr /= 0 in MatGetSize')

    petsc_size = petsc2fort([petsc_m, petsc_n])
  end function

  subroutine set_block_size(this, bs)
    ! This must be called before MatSetUp() or MatXXXSetPreallocation() (or
    ! will default to 1) and the block size cannot be changed later.
    ! 
    ! For MATMPIAIJ and MATSEQAIJ matrix formats, this function can be called
    ! at a later stage, provided that the specified block size is compatible
    ! with the matrix local sizes. 
    use petscmat, only: MatSetBlockSize

    class(petsc_mat_type), intent(inout) :: this
    integer, intent(in) :: bs

    PetscErrorCode :: ierr

    call MatSetBlockSize(this%petsc_mat, fort2petsc(bs), ierr)
    call assert(ierr == 0, 'ierr /= 0 in MatSetBlockSize')
  end subroutine
  
  subroutine set_block_sizes(this, rbs, cbs)
    ! Sets row (rbs) and column (cbs) sizes.
    ! 
    use petscmat, only: MatSetBlockSizes

    class(petsc_mat_type), intent(inout) :: this
    integer, intent(in) :: rbs, cbs

    PetscErrorCode :: ierr

    call MatSetBlockSizes(this%petsc_mat, fort2petsc(rbs), fort2petsc(cbs), ierr)
    call assert(ierr == 0, 'ierr /= 0 in MatSetBlockSizes')
  end subroutine
  
  function block_sizes(this) result(bs_row_col)
    use petscmat, only: MatGetBlockSizes

    class(petsc_mat_type), intent(in) :: this
    integer :: bs_row_col(2)

    PetscErrorCode :: ierr

    call MatGetBlockSizes(this%petsc_mat, bs_row_col(1), bs_row_col(2), ierr)
    call assert(ierr == 0, 'ierr /= 0 in MatGetBlockSizes')
  end function

  function get_type(this) result(petsc_type)
    interface
      subroutine MatGetType(mat, type, ierr)
        use petscmat
        implicit none
        Mat, intent(in) :: mat
        MatType, intent(out) :: type
        PetscErrorCode :: ierr
      end subroutine
    end interface
    class(petsc_mat_type), intent(in) :: this
    MatType :: petsc_type

    PetscErrorCode :: ierr

    call MatGetType(this%petsc_mat, petsc_type, ierr)
    call assert(ierr == 0, 'ierr /= 0 in MatGetType')
  end function

  subroutine set_from_options(this)
    use petscmat, only: MatSetFromOptions

    class(petsc_mat_type), intent(inout) :: this

    PetscErrorCode :: ierr

    call MatSetFromOptions(this%petsc_mat, ierr)
    call assert(ierr == 0, 'ierr /= 0 in MatSetFromOptions')
  end subroutine

  subroutine view(this)
    ! Prints all entries.
    ! Must call MatAssemblyBegin/End() before viewing the matrix.
    ! 
    use petscmat, only: MatView, PETSC_VIEWER_STDOUT_WORLD

    class(petsc_mat_type), intent(in) :: this

    PetscErrorCode :: ierr

    call MatView(this%petsc_mat, PETSC_VIEWER_STDOUT_WORLD, ierr) ! 	PETSC_VIEWER_STDOUT_WORLD  is for parallel matrices
    call assert(ierr == 0, 'ierr /= 0 in MatView')
  end subroutine

  subroutine XAIJ_set_preallocation(this, bs, dnnz, onnz, dnnzu, onnzu)
    ! For generic matrix type.
    ! 
    ! bs        - block size
    ! dnnz      - number of nonzero column blocks per block row of diagonal part of parallel matrix
    ! onnz      - number of nonzero column blocks per block row of off-diagonal part of parallel matrix
    ! dnnzu     - number of nonzero column blocks per block row of upper-triangular part of diagonal part of parallel matrix
    ! onnzu     - number of nonzero column blocks per block row of upper-triangular part of off-diagonal part of parallel matrix
    ! 

    interface
      subroutine MatXAIJSetPreallocation(A, bs, dnnz, onnz, dnnzu, onnzu, ierr)
        use petscmat
        implicit none
        Mat, intent(inout) :: A
        PetscInt, intent(in) :: bs
        PetscInt, intent(in) :: dnnz(*), onnz(*), dnnzu(*), onnzu(*)
        PetscErrorCode, intent(out) :: ierr
      end subroutine
    end interface
    class(petsc_mat_type), intent(inout) :: this
    integer, intent(in) :: bs
    integer, dimension(:), intent(in) :: dnnz, onnz, dnnzu, onnzu

    PetscErrorCode :: ierr

    call MatXAIJSetPreallocation(this%petsc_mat, bs, dnnz, onnz, dnnzu, onnzu, ierr)
    call assert(ierr==0,'ierr /= 0 in MatXAIJSetPreallocation')
  end subroutine


  subroutine create_nest(this, submats, mask, nr, nc)
    ! Creates a new matrix containing several nested submatrices, each stored separately
    ! * submats  row-aligned array of nr*nc submatrices
    ! * nr       number of nested row blocks
    ! * nc       number of nested column blocks
    !
    ! Empty submatrices could be passed to PETSc using NULL.
    ! 
    use petscmat, only: PETSC_NULL_MAT
    
    class(petsc_mat_type), intent(out) :: this
    type(petsc_mat_type), intent(in) :: submats(:) ! use raw PETSc type to support passing PETSC_NULL_MAT
    logical, intent(in) :: mask(:)
    integer, intent(in) :: nr, nc

    call assert(size(mask) == size(submats), 'inconsistent size mask/submats')
    call create_nest__raw(this, merge(submats%petsc_mat, PETSC_NULL_MAT, mask), nr, nc)
  end subroutine

  subroutine create_nest__raw(this,submats,nr,nc)
    use petscis, only: PETSC_NULL_IS

    interface
      subroutine MatCreateNest(comm, nr, is_row, nc, is_col, a, B, ierr)
        use petscmat
        implicit none
        MPI_Comm, intent(in) :: comm
        PetscInt, intent(in) :: nr, nc
        IS, intent(in) :: is_row(*), is_col(*)
        Mat, intent(in) :: a(*)
        Mat, intent(inout) :: B
        PetscErrorCode, intent(out) :: ierr
      end subroutine
    end interface
    class(petsc_mat_type), intent(out) :: this
    type(tMat), intent(in) :: submats(:) ! use raw PETSc type to support passing PETSC_NULL_MAT
    integer, intent(in) :: nr, nc
    
    PetscErrorCode :: ierr
    integer :: nmats
    
    call assert(petsc_initialized)
    nmats = assert_eq(nr*nc, size(submats))
    
    ! Pass PETSC_NULL_IS to make the index sets for each nested row/column block contiguous:
    call MatCreateNest(petsc_mpi_comm,            &
                       nr, [PETSC_NULL_IS],       &
                       nc, [PETSC_NULL_IS],       &
                       submats, this%petsc_mat,   &
                       ierr)
    call assert(ierr==0,'ierr /= 0 in MatCreateNest')
    this%needs_destruction = .true.
  end subroutine

  subroutine nest_set_submat(this, idxm, jdxm, sub)
    use petscmat, only: MatNestSetSubMat

    class(petsc_mat_type), intent(inout) :: this
    integer, intent(in) :: idxm, jdxm
    type(petsc_mat_type), intent(in) :: sub

    PetscErrorCode :: ierr
    call MatNestSetSubMat(this%petsc_mat, fort2petsc(idxm), fort2petsc(jdxm), sub%petsc_mat, ierr)
    call assert(ierr == 0, 'ierr /= 0 in MatNestSetSubMat')
  end subroutine

  subroutine zero_entries(this)
    ! Zeros all entries of a matrix. For sparse matrices this routine retains
    ! the old nonzero structure.
    !
    ! This should be called after the preallocation phase. If the matrix was
    ! not preallocated then a default, likely poor preallocation will be set
    ! in the matrix.
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatZeroEntries.html
    use petscmat, only: MatZeroEntries

    class(petsc_mat_type), intent(inout) :: this

    PetscErrorCode :: ierr

    call MatZeroEntries(this%petsc_mat,ierr)
    call assert(ierr==0,'ierr /= 0 in MatZeroEntries')
  end subroutine zero_entries

  subroutine add_value(this,row,col,value)
    use petscmat, only: MatSetValue, insert_mode => ADD_VALUES
    
    class(petsc_mat_type), intent(inout) :: this
    integer, intent(in) :: row, col
    real(dp), intent(in) :: value
    
    integer :: petsc_row, petsc_col
    PetscErrorCode :: ierr
    
    call idx_fort2petsc(row,petsc_row)
    call idx_fort2petsc(col,petsc_col)
    call MatSetValue(this%petsc_mat,petsc_row,petsc_col,value,insert_mode,ierr)
    call assert(ierr==0,'ierr /= 0 in MatSetValue')
  end subroutine add_value
  
  subroutine add_values(this,rows,cols,tvalues)
    ! This hinges on the fact that MAT_ROW_ORIENTED option in petsc_mat has 
    ! been set to PETSC_FALSE.
    ! 
    !     " Efficiency Alert: The routine MatSetValuesBlocked() may offer 
    !       much better efficiency for users of block sparse formats 
    !       (MATSEQBAIJ and MATMPIBAIJ). "
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatSetValues.html#MatSetValues
    use petscmat, only: MatSetValues, insert_mode => ADD_VALUES
    
    class(petsc_mat_type), intent(inout) :: this
    real(dp), contiguous, intent(in) :: tvalues(:,:)
    integer, intent(in) :: rows(:), cols(:)
    
    integer :: petsc_rows(size(rows)), petsc_cols(size(cols))
    PetscErrorCode :: ierr
    
    call assert(size(tvalues,2) == size(rows),'PETSc Mat: add_values: mismatch in row len',BOUNDS_CHECK)
    call assert(size(tvalues,1) == size(cols),'PETSc Mat: add_values: mismatch in col len',BOUNDS_CHECK)

    call idx_fort2petsc(rows,petsc_rows)
    call idx_fort2petsc(cols,petsc_cols)
    call MatSetValues(this%petsc_mat,size(petsc_rows),petsc_rows,size(petsc_cols),petsc_cols,tvalues,insert_mode,ierr)
    call assert(ierr==0,'ierr /= 0 in MatSetValues')
  end subroutine
  
  subroutine set_values_blocked__one(this, idxm, idxn, values)
    use petscmat, only: MatSetValuesBlocked, insert_mode => ADD_VALUES
    
    class(petsc_mat_type), intent(inout) :: this
    integer, intent(in) :: idxm, idxn
    real(dp), contiguous, intent(in) :: values(:)
    
    PetscInt :: petsc_idxm, petsc_idxn
    PetscErrorCode :: ierr

    call idx_fort2petsc(idxm, petsc_idxm)
    call idx_fort2petsc(idxn, petsc_idxn)
    call MatSetValuesBlocked(this%petsc_mat, 1, petsc_idxm, 1, petsc_idxn, values, insert_mode, ierr)
    call assert(ierr == 0, 'ierr /= 0 in MatSetValuesBlocked (one block)')
  end subroutine
  
  subroutine set_values_blocked__many(this, idxm, idxn, values)
    use petscmat, only: MatSetValuesBlocked, insert_mode => ADD_VALUES
    
    class(petsc_mat_type), intent(inout) :: this
    integer, intent(in) :: idxm(:), idxn(:)
    real(dp), contiguous, intent(in) :: values(:)
    
    PetscInt :: petsc_idxm(size(idxm)), petsc_idxn(size(idxn))
    PetscErrorCode :: ierr

    call assert(mod(size(values), size(idxm)*size(idxn)) == 0, 'MatSetValuesBlocked: weird implied bs', BOUNDS_CHECK)

    call idx_fort2petsc(idxm, petsc_idxm)
    call idx_fort2petsc(idxn, petsc_idxn)
    call MatSetValuesBlocked(this%petsc_mat, size(idxm), petsc_idxm, size(idxn), petsc_idxn, values, insert_mode, ierr)
    call assert(ierr == 0, 'ierr /= 0 in MatSetValuesBlocked (multiple blocks)')
  end subroutine
  
  subroutine finalize_assembly(this)
    use petscmat, only: MatAssemblyBegin, MatAssemblyEnd, MAT_FINAL_ASSEMBLY
    
    class(petsc_mat_type), intent(inout) :: this
    
    PetscErrorCode :: ierr
    
    call MatAssemblyBegin(this%petsc_mat,MAT_FINAL_ASSEMBLY,ierr)
    call assert(ierr==0,'ierr /= 0 in MatAssemblyBegin')
    call MatAssemblyEnd(this%petsc_mat,MAT_FINAL_ASSEMBLY,ierr)
    call assert(ierr==0,'ierr /= 0 in MatAssemblyEnd')
  end subroutine finalize_assembly
  
  subroutine axpy(this,alpha,x,mat_struct_opt)
    ! Famous 'AXPY' operation: Y <-- Y + alpha * X
    use petscmat, only: MatAXPY

    class(petsc_mat_type), intent(inout) :: this
    real(dp), intent(in) :: alpha
    type(petsc_mat_type), intent(in) :: x
    type(mat_struct_opt_type), intent(in) :: mat_struct_opt
    
    PetscErrorCode :: ierr
    
    call MatAXPY(this%petsc_mat,alpha,x%petsc_mat,mat_struct_opt%petsc_str,ierr)
    call assert(ierr==0,'ierr /= 0 in MatAXPY')
  end subroutine axpy
  
  subroutine mult_with_scal(this,alpha)
    use petscmat, only: MatScale
    
    class(petsc_mat_type), intent(inout) :: this
    real(dp), intent(in) :: alpha
    
    PetscErrorCode :: ierr
    
    call MatScale(this%petsc_mat,alpha,ierr)
    call assert(ierr==0,'ierr /= 0 in MatScale')
  end subroutine mult_with_scal
  
  subroutine mult(this,vec,mat_dot_vec)
    use petscmat, only: MatMult

    class(petsc_mat_type), intent(in) :: this
    type(petsc_vec_type), intent(in) :: vec
    type(petsc_vec_type), intent(inout) :: mat_dot_vec
    
    PetscErrorCode :: ierr
    
    call MatMult(this%petsc_mat,vec%petsc_vec,mat_dot_vec%petsc_vec,ierr)
    call assert(ierr==0,'ierr /= 0 in MatMult')
  end subroutine mult
  
  subroutine mult_transpose(this,vec,TransMat_dot_vec)
    ! TransMat_dot_vec = transpose(this) * vec
    use petscmat, only: MatMultTranspose

    class(petsc_mat_type), intent(in) :: this
    type(petsc_vec_type), intent(in) :: vec
    type(petsc_vec_type), intent(inout) :: TransMat_dot_vec
    
    PetscErrorCode :: ierr
    
    call MatMultTranspose(this%petsc_mat,vec%petsc_vec,TransMat_dot_vec%petsc_vec,ierr)
    call assert(ierr==0,'ierr /= 0 in MatMultTranspose')
  end subroutine mult_transpose
  
  subroutine mult_add(this,v1,v2,v3)
    ! Computes v3 = v2 + A * v1.
    !
    ! The vectors v1 and v3 cannot be the same, i.e., one cannot call
    ! MatMultAdd(A,v1,v2,v1).
    !
    ! Neighbor-wise collective on Mat and Vec
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatMultAdd.html
    use petscmat, only: MatMultAdd

    class(petsc_mat_type), intent(in) :: this
    type(petsc_vec_type), intent(in) :: v1
    type(petsc_vec_type), intent(inout) :: v2, v3

    PetscErrorCode :: ierr

    call MatMultAdd(this%petsc_mat,v1%petsc_vec,v2%petsc_vec,v3%petsc_vec,ierr)
    call assert(ierr==0,'ierr /= 0 in MatMultAdd')
  end subroutine

  subroutine mult_transpose_add(this, v1, v2, v3)
    ! Computes v3 = v2 + A' * v1.
    ! 
    ! The vectors v1 and v3 cannot be the same.
    ! 
    use petscmat, only: MatMultTransposeAdd
    class(petsc_mat_type), intent(in) :: this
    type(petsc_vec_type), intent(in) :: v1
    type(petsc_vec_type), intent(inout) :: v2, v3

    PetscErrorCode :: ierr

    call MatMultTransposeAdd(this%petsc_mat, v1%petsc_vec, v2%petsc_vec, v3%petsc_vec, ierr)
    call assert(ierr==0,'ierr /= 0 in MatMultTransposeAdd')
  end subroutine

  subroutine mult_with_mat(this,right_mat,mat_dot_mat)
    ! mat_dot_mat = this * right_mat
    use petscmat, only: MatMatMult, PETSC_DEFAULT_REAL, MAT_INITIAL_MATRIX
    
    class(petsc_mat_type), intent(in) :: this
    class(petsc_mat_type), intent(in) :: right_mat
    type(petsc_mat_type), intent(out) :: mat_dot_mat
    
    PetscErrorCode :: ierr
    
    call MatMatMult(this%petsc_mat,right_mat%petsc_mat,MAT_INITIAL_MATRIX, &
                    PETSC_DEFAULT_REAL,mat_dot_mat%petsc_mat,ierr)
    call assert(ierr==0,'ierr /= 0 in MatMatMult')
    mat_dot_mat%needs_destruction = .true.
  end subroutine mult_with_mat
  
  subroutine mat_transpose_mult(this,B,C)
    ! Performs Matrix-Matrix Multiplication C = this * B^T.
    !
    ! Neighbor-wise Collective on Mat
    ! WARNING: MatMatTransposeMult is implemented only for SeqAIJ matrices
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatMatTransposeMult.html#MatMatTransposeMult
    use petscmat, only: MatMatTransposeMult, PETSC_DEFAULT_REAL, MAT_INITIAL_MATRIX
    
    class(petsc_mat_type), intent(in) :: this, B
    class(petsc_mat_type), intent(out) :: C
    
    PetscErrorCode :: ierr
    type(petsc_mat_type) :: B_T
    
    if (no_petsc_procs() == 1) then
      ! SeqAIJ matrix
      call MatMatTransposeMult(this%petsc_mat,B%petsc_mat,MAT_INITIAL_MATRIX,PETSC_DEFAULT_REAL,C%petsc_mat,ierr)
      call assert(ierr == 0,'ierr /= 0 in MatMatTransposeMult')
    else
      ! Mpi_AIJ matrix => no PETSc function can be called directly
      call B%get_transpose(B_T)
      call this%mult_with_mat(B_T,C)
    endif
    C%needs_destruction = .true.
  end subroutine mat_transpose_mult

  subroutine get_transpose(this,transposed)
    use petscmat, only: MatTranspose, MAT_INITIAL_MATRIX
    
    class(petsc_mat_type), intent(in) :: this
    type(petsc_mat_type), intent(out) :: transposed
    
    PetscErrorCode :: ierr
    
    call MatTranspose(this%petsc_mat,MAT_INITIAL_MATRIX,transposed%petsc_mat,ierr)
    call assert(ierr==0,'ierr /= 0 in MatTranspose')
    transposed%needs_destruction = .true.
  end subroutine get_transpose
  
  subroutine duplicate(this,duplicate_opt,new_mat)
    ! Duplicates 'this' including the non-zero structure.
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatDuplicate.html#MatDuplicate
    use petscmat, only: MatDuplicate

    class(petsc_mat_type), intent(in) :: this
    type(mat_duplicate_opt_t), intent(in) :: duplicate_opt
    class(petsc_mat_type), intent(out) :: new_mat

    PetscErrorCode :: ierr

    call MatDuplicate(this%petsc_mat,duplicate_opt%op,new_mat%petsc_mat,ierr)
    call assert(ierr==0,'ierr /= 0 in MatDuplicate')
    new_mat%needs_destruction = .true.
  end subroutine duplicate

  subroutine init_compatible_vecs(this,right_vec,left_vec)
    ! Get vector(s) compatible with the matrix, i.e., with the same parallel layout.
    !
    use petscmat, only: PETSC_NULL_VEC

    interface
      subroutine MatCreateVecs(mat, right, left, ierr)
        use petscmat
        implicit none
        Mat, intent(in) :: mat
        Vec :: right, left
        PetscErrorCode, intent(out) :: ierr
      end subroutine
    end interface
    class(petsc_mat_type), intent(in) :: this
    type(petsc_vec_type), optional, intent(out) :: right_vec, left_vec

    PetscErrorCode :: ierr
    
    if (present(right_vec) .and. present(left_vec)) then
      call MatCreateVecs(this%petsc_Mat, right_vec%petsc_vec, left_vec%petsc_vec, ierr)
    elseif (present(right_vec)) then
      call MatCreateVecs(this%petsc_Mat, right_vec%petsc_vec, PETSC_NULL_VEC,     ierr)
    elseif (present(left_vec)) then
      call MatCreateVecs(this%petsc_Mat, PETSC_NULL_VEC,      left_vec%petsc_vec, ierr)
    endif
    call assert(ierr==0,"ierr /= 0 in MatCreateVecs")

    if (present(right_vec)) then
      right_vec%initialized = .true.
      right_vec%assemble_elemvec_as_block = this%assemble_elemmat_as_block
      call right_vec%zero_entries()
    endif
    if (present(left_vec)) then
      left_vec%assemble_elemvec_as_block = this%assemble_elemmat_as_block
      left_vec%initialized = .true.
      call left_vec%zero_entries()
    endif
  end subroutine
  

  subroutine copy(this,other_mat,mat_struct_opt)
    ! copies the matrix entries of 'this' to another existing matrix (after
    ! first zeroing the second matrix).
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatCopy.html#MatCopy
    use petscmat, only: MatCopy

    class(petsc_mat_type), intent(in) :: this
    class(petsc_mat_type), intent(inout) :: other_mat
    type(mat_struct_opt_type), intent(in) :: mat_struct_opt

    PetscErrorCode :: ierr

    call MatCopy(this%petsc_mat,other_mat%petsc_mat,mat_struct_opt%petsc_str,ierr)
    call assert(ierr==0,'ierr /= 0 in MatCopy')
  end subroutine copy

  subroutine set_option(this,op,flg)
    ! Sets a parameter option for 'this'. Some options may be specific to
    ! certain storage formats.
    !
    ! Except for MAT_UNUSED_NONZERO_LOCATION_ERR and MAT_ROW_ORIENTED all
    ! processes that share 'this' must pass the same value in flg!
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatSetOption.html#MatSetOption
    use petscmat, only: MatSetOption

    class(petsc_mat_type), intent(inout) :: this
    type(mat_opt_t), intent(in) :: op
    logical, intent(in) :: flg

    PetscErrorCode :: ierr

    call MatSetOption(this%petsc_mat,op%op,fort2petsc(flg),ierr)
    call assert(ierr==0,'ierr /= 0 in MatSetOption')
  end subroutine

  subroutine set_nullspace_vec(this,vec)
    class(petsc_mat_type), intent(inout) :: this
    type(petsc_vec_type), intent(in) :: vec
    
    type(petsc_matnullspace_t) :: matnullspace
    
    call matnullspace%create(vec)
    call this%set_nullspace(matnullspace)
  end subroutine set_nullspace_vec
  
  subroutine set_nullspace(this,matnullspace)
    ! Attaches a null space, which is used by the linear solvers.
    !
    ! If 'this' is known to be symmetric (e.g., an SBAIJ matrix, or you called
    ! MatSetOption(mat,MAT_SYMMETRIC,PETSC_TRUE)), this routine also
    ! automatically calls MatSetTransposeNullSpace().
    !
    ! Logically Collective on 'this' and 'matnullspace'.
    ! www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatSetNullSpace.html#MatSetNullSpace
    use petscmat, only: MatNullSpaceTest, MatSetNullSpace
    
    class(petsc_mat_type), intent(inout) :: this
    type(petsc_matnullspace_t), intent(in) :: matnullspace
    
    PetscErrorCode :: ierr
    PetscBool :: petsc_isnull
    
    call assert(.not.this%has_nullspace,'Mat already has a null space')
    
    call MatNullSpaceTest(matnullspace%petsc_MatNullSpace,this%petsc_mat,petsc_isnull,ierr)
    call assert(ierr == 0,'ierr /= 0 in MatNullSpaceTest')
    call assert(petsc2fort(petsc_isnull),'invalid nullspace')
    
    call MatSetNullSpace(this%petsc_mat,matnullspace%petsc_MatNullSpace,ierr)
    call assert(ierr == 0,'ierr /= 0 in MatSetNullSpace')
    this%has_nullspace = .true.
  end subroutine set_nullspace

  subroutine print_sparsity_info(this)
    class(petsc_mat_type), intent(in) :: this
    
    integer :: nnz
    
    call this%get_local_info(nz_allocated=nnz)
    write(*,'(a,i0,a,i0)') '# entries: ', nnz, ' on proc ', petsc_proc_id()
  end subroutine print_sparsity_info
  
  subroutine get_local_info(this, &
                            block_size, &
                            nz_allocated, &
                            nz_used, &
                            nz_unneeded, &
                            memory, &
                            assemblies, &
                            mallocs, &
                            fill_ratio_given, &
                            fill_ratio_needed, &
                            factor_mallocs)
    use petscmat, only: MatGetInfo, &
        MAT_INFO_SIZE, MAT_LOCAL, &
        MAT_INFO_BLOCK_SIZE, &
        MAT_INFO_NZ_ALLOCATED, &
        MAT_INFO_NZ_USED, &
        MAT_INFO_NZ_UNNEEDED, &
        MAT_INFO_MEMORY, &
        MAT_INFO_ASSEMBLIES, &
        MAT_INFO_MALLOCS, &
        MAT_INFO_FILL_RATIO_GIVEN, &
        MAT_INFO_FILL_RATIO_NEEDED, &
        MAT_INFO_FACTOR_MALLOCS
    
    class(petsc_mat_type), intent(in) :: this
    integer, optional, intent(out) :: block_size
    integer, optional, intent(out) :: nz_allocated
    integer, optional, intent(out) :: nz_used
    integer, optional, intent(out) :: nz_unneeded
    real(dp), optional, intent(out) :: memory
    integer, optional, intent(out) :: assemblies
    integer, optional, intent(out) :: mallocs
    real(dp), optional, intent(out) :: fill_ratio_given
    real(dp), optional, intent(out) :: fill_ratio_needed
    integer, optional, intent(out) :: factor_mallocs
    
    double precision, dimension(MAT_INFO_SIZE) :: info
    PetscErrorCode :: ierr
    
    call MatGetInfo(this%petsc_mat,MAT_LOCAL,info,ierr)
    call assert(ierr==0,'get_mat_info: ierr =/ 0')
    if (present(block_size))        block_size        = nint(info(MAT_INFO_BLOCK_SIZE))
    if (present(nz_allocated))      nz_allocated      = nint(info(MAT_INFO_NZ_ALLOCATED))
    if (present(nz_used))           nz_used           = nint(info(MAT_INFO_NZ_USED))
    if (present(nz_unneeded))       nz_unneeded       = nint(info(MAT_INFO_NZ_UNNEEDED))
    if (present(memory))            memory            = real(info(MAT_INFO_MEMORY),dp)
    if (present(assemblies))        assemblies        = nint(info(MAT_INFO_ASSEMBLIES))
    if (present(mallocs))           mallocs           = nint(info(MAT_INFO_MALLOCS))
    if (present(fill_ratio_given))  fill_ratio_given  = real(info(MAT_INFO_FILL_RATIO_GIVEN),dp)
    if (present(fill_ratio_needed)) fill_ratio_needed = real(info(MAT_INFO_FILL_RATIO_NEEDED),dp)
    if (present(factor_mallocs))    factor_mallocs    = nint(info(MAT_INFO_FACTOR_MALLOCS))
  end subroutine get_local_info
  
  logical function is_symm(this) result(symm)
    ! Returns whether 'this' is symmetric. Tests both my nonzeros and those 
    ! of connected processes.
    ! Collective on 'this', and memory-intensive, because PETSc copies submatrices.
    !
    ! https://www.mcs.anl.gov/petsc/petsc-current/src/mat/impls/aij/mpi/mpiaij.c.html#MatIsTranspose_MPIAIJ
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatIsSymmetric.html
    use petscmat, only: MatIsSymmetric
    
    class(petsc_mat_type), intent(in) :: this
    
    PetscReal :: petsc_tol
    PetscBool :: petsc_symm
    PetscErrorCode :: ierr
    
    petsc_tol = sqrt(epsilon(petsc_tol))
    call MatIsSymmetric(this%petsc_mat,petsc_tol,petsc_symm,ierr)
    call assert(ierr==0,'ierr /= 0 in MatIsSymmetric')
    symm = petsc2fort(petsc_symm)
  end function is_symm
  
  real(dp) function rel_norm_residual(this,sol_vec,rhs_vec)
    class(petsc_mat_type), intent(in) :: this
    type(petsc_vec_type), intent(in) :: sol_vec, rhs_vec

    type(petsc_vec_type) :: residual

    call rhs_vec%duplicate(residual)
    call this%mult(sol_vec,residual)
    call residual%axpy(-1.0_dp,rhs_vec)
    rel_norm_residual = residual%norm_2() / max(rhs_vec%norm_2(),sqrt(epsilon(1.0_dp)))
  end function rel_norm_residual

  subroutine convert(this,new_type,reuse,new_mat)
    ! https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatConvert.html
    ! 
    class(petsc_mat_type), intent(in) :: this
    type(mat_type_t), intent(in) :: new_type
    type(mat_reuse_t), intent(in) :: reuse
    type(petsc_mat_type), intent(out) :: new_mat

    PetscErrorCode :: ierr

    call assert(reuse%petsc /= INPLACE_MATRIX%petsc, "use `convert_inplace` instead of `convert` for in-place type conversion.")

    call MatConvert(this%petsc_mat,new_type%t,reuse%petsc,new_mat%petsc_mat,ierr)
    call assert(ierr==0,'ierr /= 0 in MatConvert')
  end subroutine
  
  subroutine convert_inplace(this, new_type)
    ! No explicit interface:
    ! use petscmat, only: MatConvert
    
    class(petsc_mat_type), intent(inout) :: this
    type(mat_type_t), intent(in) :: new_type

    PetscErrorCode :: ierr

    call MatConvert(this%petsc_mat, new_type%t, MAT_INPLACE_MATRIX, this%petsc_mat, ierr)
    call assert(ierr==0,'convert_inplace: ierr /= 0 in MatConvert')
  end subroutine

  impure elemental subroutine destroy_matrix(this)
    use petscmat, only: MatDestroy
    
    type(petsc_mat_type), intent(inout) :: this
    
    PetscErrorCode :: ierr
    
    if (this%needs_destruction) then
      call MatDestroy(this%petsc_mat,ierr)
      call assert(ierr==0,'ierr /= 0 in MatDestroy')
    endif
  end subroutine destroy_matrix
  
  
  subroutine get_matnest_ISs(matnest,ISs)
    use petscis

    use petsc_is_mod, only: petsc_is_t
    
    interface
      subroutine MatNestGetISs(A, rows, cols, ierr)
        use petscmat
        implicit none
        Mat, intent(in) :: A
        IS :: rows(*), cols(*)
        PetscErrorCode, intent(out) :: ierr
      end subroutine
    end interface
    class(petsc_mat_type), intent(in) :: matnest
    class(petsc_is_t), intent(out) :: ISs(:)
    
    integer :: i
    type(tIS) :: temp_ISs(0:size(ISs)-1) ! no reason for 0-based indexing in this local variable
    PetscErrorCode :: ierr
    
    call MatNestGetISs(matnest%petsc_mat, temp_ISs, [PETSC_NULL_IS], ierr)
    call assert(ierr == 0,'ierr /= 0 in MatNestGetISs')
    
    do i = 0, size(ISs)-1
      call ISDuplicate(temp_ISs(i),ISs(i+1)%petsc_is,ierr)
      call assert(ierr == 0,'ierr /= 0 in ISDuplicate')
      call IScopy(temp_ISs(i),ISs(i+1)%petsc_is,ierr)
      call assert(ierr == 0,'ierr /= 0 in IScopy')
      ISs(i+1)%initialized = .true.
    enddo
  end subroutine get_matnest_ISs
  
  subroutine Pt_A_P(A, P, C)
    ! C = P^T * A * P 
    ! 
    ! For matrix types without special implementation the function fallbacks to
    ! MatMatMult() followed by MatTransposeMatMult().
    ! 
    use petscmat, only: MatPtAP, PETSC_DEFAULT_REAL, MAT_INITIAL_MATRIX

    class(petsc_mat_type), intent(in) :: A, P
    type(petsc_mat_type), intent(out) :: C
    
    PetscErrorCode :: ierr

    call MatPtAP(A%petsc_mat, P%petsc_mat, MAT_INITIAL_MATRIX, PETSC_DEFAULT_REAL, C%petsc_mat, ierr)
    call assert(ierr == 0, 'ierr /= 0 in MatPtAP')
  end subroutine

  subroutine R_A_Rt(A, R, C)
    ! C = R * A * R^T
    ! 
    ! Only implemented for pairs of AIJ matrices and classes that inherit from AIJ.
    ! Due to PETSc sparse matrix block row distribution among processes, parallel
    ! MatRARt is implemented via explicit transpose of R, which could be very
    ! expensive. Using MatPtAP() could be cheaper.
    ! 
    ! https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatRARt.html#MatRARt
    use petscmat, only: MatRARt, PETSC_DEFAULT_REAL, MAT_INITIAL_MATRIX

    class(petsc_mat_type), intent(in) :: A, R
    type(petsc_mat_type), intent(out) :: C
    
    PetscErrorCode :: ierr
    
    call MatRARt(A%petsc_mat, R%petsc_mat, MAT_INITIAL_MATRIX, PETSC_DEFAULT_REAL, C%petsc_mat, ierr)
    call assert(ierr == 0,'ierr /= 0 in MatPtAP')
    
    C%needs_destruction = .true.
  end subroutine
end module petsc_mat_mod
