module mesh_refinement
  ! THE ENTIRE MODULE NEEDS TO BE UPDATED ACCORDING TO THE MOST 
  ! RECENT DEVELOPMENTS IN THE RADIATION-TRANSPORT CODE.
  use assertions, only: assert
  use code_const, only: BOUNDARY, INTERNAL
  use exceptions, only: error
  use f90_kind, only: dp
  use local_elem, only: no_vertices
  use mesh_objects, only: elem_list, elem_id, elem_ptr, face_list, face_ptr, &
                          vertex_coordinate, vertex_list, vertex_ptr, &
                          eltype_of_faces_of_elem, get_1st_order_eltype, &
                          get_no_elem_edges, no_faces_of_elem, get_no_children, get_no_interior_faces, same_vtx
  use mesh_type, only: fem_mesh, gen_elem_lists, insert_vertex, mesh_state, get_level_from_id
  use petsc_mod, only: I_am_master_process
  implicit none
  
  private
  public :: mark_and_refine_elems_near_bnd
  public :: refine_spatial_mesh_from_state
  
contains
  subroutine mark_and_refine_elems_near_bnd(mesh,only_fluid_mesh,criterion)
    use boundaries, only: all_specified_bnd_ids, is_periodic
    use fortran_parser_interface_mod, only: add_eqparser, evaluate
    use matprop_mod, only: all_specified_mat_ids
    use string_manipulations, only: char2int, int2char, separate_first_word, split
    use string_mod, only: as_chars

    class(fem_mesh), intent(inout) :: mesh
    logical, intent(in) :: only_fluid_mesh
    character(*), intent(in) :: criterion
    
    integer :: nghb_depth
    integer, allocatable :: bnd_ids(:), all_bnd_ids(:), mat_ids(:), all_mat_ids(:)
    integer :: i
    logical :: refine
    integer, allocatable :: eqparsers(:)
    character(:), allocatable :: label, content, eqs(:), depth_str, ids_str
    
    integer :: elem_no, no_active_elem
    type(elem_id) :: id
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_list), pointer :: elem
    
    call assert(.not. is_periodic(),'refinement of periodic meshes is not yet supported')

    call separate_first_word(criterion,label,content,':')
    select case(label)
      case('com')
        eqs = as_chars(split(content,' and '))
        allocate(eqparsers(size(eqs)))
        do i = 1, size(eqparsers)
          call add_eqparser(eqs(i),['x','y','z'],eqparsers(i))
        enddo
      
      case('near_bnd')
        all_bnd_ids = all_specified_bnd_ids()
        call separate_first_word(content,depth_str,ids_str,';')
        nghb_depth = char2int(depth_str)
        if (ids_str == '') then
          bnd_ids = all_bnd_ids
        else
          bnd_ids = char2int(as_chars(split(ids_str,',')))
          do i = 1, size(bnd_ids)
            call assert(findloc(all_bnd_ids,bnd_ids(i),dim=1) > 0,"refinement_list: unknown id " // int2char(bnd_ids(i)))
          enddo
        endif
        
      case('mat_id')
        all_mat_ids = all_specified_mat_ids()
        if (content == '') then
          mat_ids = all_mat_ids
        else
          mat_ids = char2int(as_chars(split(content,',')))
          do i = 1, size(mat_ids)
            call assert(findloc(all_mat_ids,mat_ids(i),dim=1) > 0,"refinement_list: unknown id " // int2char(mat_ids(i)))
          enddo
        endif
      
      case default
        call error('do not understand criterion.; label=' // label)
    end select
    
    
    if (allocated(mesh%list_of_elems_tobe_refined)) deallocate(mesh%list_of_elems_tobe_refined)
    allocate(mesh%list_of_elems_tobe_refined(mesh%no_active_elem))
    mesh%list_of_elems_tobe_refined = .false.
    
    call mesh%get_active_elem_list_ptr(only_fluid_mesh,active_elem_list)
    no_active_elem = mesh%get_no_active_elem(only_fluid_mesh)
    do elem_no = 1, no_active_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      select case(label)
        case('com');        refine = all([(  evaluate(eqparsers(i),elem%avg_vtx()) > 0.0_dp, i = 1, size(eqparsers)  )])
        case('near_bnd');   refine = elem%is_near_bnd(only_fluid_mesh,nghb_depth,bnd_ids)
        case('mat_id');     refine = any([(  elem%mat_id == mat_ids(i), i = 1, size(mat_ids)  )])
        case default;       call error('do not understand criterion.; label=' // label)
      end select
      mesh%list_of_elems_tobe_refined(elem%active_elem_no) = refine
    enddo
    call mark_and_refine_elements(mesh)
  end subroutine mark_and_refine_elems_near_bnd
  
  
  subroutine refine_spatial_mesh_from_state(mesh,mesh_summary)
    ! Refine mesh AT LEAST up to the elements in mesh_summary
    type(fem_mesh), intent(inout) :: mesh
    type(mesh_state), intent(in) :: mesh_summary
    
    integer :: elem_no, closest_mother_no
    logical :: refinement_added
    type(elem_id) :: id
    type(elem_list), pointer :: closest_mother
    
    ! do refinement
    if (allocated(mesh%list_of_elems_tobe_refined)) deallocate(mesh%list_of_elems_tobe_refined)
    allocate(mesh%list_of_elems_tobe_refined(mesh%no_active_elem))
    
    refinement_added = .true. ! Init statement to get the while loop started
    do while (refinement_added)
      mesh%list_of_elems_tobe_refined = .false.
      refinement_added = .false.
      
      ! Loop over list and see if these elements already exist or not
      do elem_no = 1, size(mesh_summary%active_elem_list)
        id = mesh_summary%active_elem_list(elem_no)
        if (.not. mesh%elem_exists(id)) then
          ! we seek the closest mother and mark
          closest_mother => mesh%get_closest_mother_ptr(id)
          call assert(get_level_from_id(id) > closest_mother%level,'get_level_from_id(id) <= closest_mother%level')
          closest_mother_no = closest_mother%active_elem_no
          mesh%list_of_elems_tobe_refined(closest_mother_no) = .true.
          refinement_added = .true.
        endif
      enddo
      
      if (refinement_added) then
        call mark_and_refine_elements(mesh)
        if (allocated(mesh%list_of_elems_tobe_refined)) deallocate(mesh%list_of_elems_tobe_refined)
        allocate(mesh%list_of_elems_tobe_refined(mesh%no_active_elem))
        mesh%list_of_elems_tobe_refined = .false.
      endif
    enddo
  end subroutine refine_spatial_mesh_from_state
  
  
  
  
  subroutine mark_and_refine_elements(mesh)
    ! If the mesh contains any element to be ignored, they will be skipped in the refinement process
    use run_data, only: max_mesh_irregularity
    
    type(fem_mesh), intent(inout) :: mesh
    
    logical, parameter :: ONLY_FLUID = .false.
    integer :: elem_no, nghb_no, i
    type(elem_id) :: id
    type(elem_list), pointer :: elem, nghb
    type(elem_ptr), allocatable :: active_neighbors(:)
    type(face_ptr), allocatable :: active_subfaces(:)
    type(face_list), pointer :: sub_face
    logical :: refinement_added
    
    ! Store the old active element list for later use
    mesh%old_no_active_elem = mesh%no_active_elem
    if (allocated(mesh%old_active_elem_list)) deallocate(mesh%old_active_elem_list)
    allocate(mesh%old_active_elem_list(mesh%old_no_active_elem))
    mesh%old_active_elem_list = mesh%active_elem_list
    
    ! Loop over the elements until we are sure that the irregularity rule is satisfied 
    ! after the refinement process (during the process an additional level might be present)
    if (I_am_master_process()) write(*,"('Checking refinement conformity...')",advance='no')
    refinement_added = .true.
    do
      if (.not. refinement_added) exit
      refinement_added = .false.
      do elem_no = 1, mesh%no_active_elem
        ! check only for elements that will be refined
        if (mesh%list_of_elems_tobe_refined(elem_no)) then
          id = mesh%active_elem_list(elem_no)
          elem => mesh%get_elem_ptr(id)
          call elem%active_subfaces(active_subfaces,active_neighbors)
          do i = 1, size(active_subfaces)
            sub_face => active_subfaces(i)%point
            if (.not. sub_face%is_boundary(ONLY_FLUID)) then
              nghb => active_neighbors(i)%point
              nghb_no = nghb%active_elem_no
              ! We only check cases where elem%level > nghb_level and where the nghb is not refined. 
              ! Other cases either make the level difference smaller or keep it equal.
              if (elem%level + 1 > nghb%level + max_mesh_irregularity  .and.  .not. mesh%list_of_elems_tobe_refined(nghb_no)) then
                mesh%list_of_elems_tobe_refined(nghb_no) = .true.
                refinement_added = .true.
              endif
            endif
          enddo
        endif
      enddo
    enddo
    if (I_am_master_process()) write(*,"('done.')")
    
    ! Now refine the elements by calling h_refine
    if (I_am_master_process()) write(*,"('Refining mesh...')",advance='no')
    do elem_no = 1, mesh%no_active_elem
      if (mesh%list_of_elems_tobe_refined(elem_no)) then
        id = mesh%active_elem_list(elem_no)
        elem => mesh%get_elem_ptr(id)
        call h_refine_elem(mesh,elem)
      endif
    enddo
    call gen_elem_lists(mesh)
    if (I_am_master_process()) write(*,"('done.')")
  end subroutine mark_and_refine_elements
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine h_refine_elem(mesh,mother)
    type(fem_mesh), intent(inout) :: mesh
    type(elem_list), pointer, intent(inout) :: mother
    
    integer, parameter :: MAX_NO_VERT_PER_ELEM = 27
    
    type(elem_list), pointer :: mother_neighbor, child, host_elem
    type(face_list), pointer :: mother_face, child_face, face_list_head
    type(vertex_coordinate) :: vertex_position
    type(vertex_list), pointer :: vertex_found
    logical :: casei, caseii
    integer :: face, neighbor
    integer :: edge_index, child_face_index, child_index
    integer :: no_children, no_face_children, no_neighbors
    integer :: no_interior_faces, no_elem_faces, no_elem_edges
    integer :: no_elem_vertices, no_elem_corner_vertices
    integer :: interior_face, mother_neighbor_loc_face, child_level
    integer :: vertex, mother_vertex, neighbor_vertex
    integer :: face_to_child(MAX_NO_VERT_PER_ELEM)
    type(elem_ptr), pointer :: elem_ptrs(:), neighbor_ptrs(:), child_ptrs(:)
    type(vertex_ptr), pointer :: vertex_ptrs(:)
    type(face_ptr), pointer :: child_face_ptrs(:)
    
    ! get the element type and number of children
    child_level = mother%level + 1
    no_children = get_no_children(mother%eltype)
    no_interior_faces = get_no_interior_faces(mother%eltype)
    no_elem_faces = no_faces_of_elem(mother%eltype)
    no_elem_vertices = no_vertices(mother%eltype)
    
    ! make children, inherit properties from mother and set respective links
    allocate(mother%children(no_children))
    do child_index = 1, no_children
      allocate(mother%children(child_index)%point)
      allocate(mother%children(child_index)%point%faces(no_elem_faces))
      allocate(mother%children(child_index)%point%vertices(no_elem_vertices))
      child => mother%children(child_index)%point
      mother%children(child_index)%point => child
      child%mother => mother
      child%eltype = mother%eltype
      child%level = child_level
      call set_child_id(mother%id,child_index,child_level,child%id)
      child%mat_id = mother%mat_id
      child%active = .true.
      nullify(child)
    enddo
    mother%active = .false.
    mesh%tot_no_elem = mesh%tot_no_elem + no_children - 1
    
    ! put children in array of pointers for later use
    allocate(child_ptrs(no_children))
    do child_index = 1, no_children
      child_ptrs(child_index)%point => mother%children(child_index)%point
    enddo
    
    ! create internal faces of mother and set some properties
    allocate(mother%interior_faces(no_interior_faces))
    do interior_face = 1, no_interior_faces
      allocate(mother%interior_faces(interior_face)%point)
      mother%interior_faces(interior_face)%point%level = child_level
      mother%interior_faces(interior_face)%point%active = .true.
      mother%interior_faces(interior_face)%point%ftype = INTERNAL
      mother%interior_faces(interior_face)%point%bc_id = 0
      allocate(mother%interior_faces(interior_face)%point%elem_neighbors(2))
    enddo
    
    ! setup the new pointers that are local to the mother (no interaction with mother-neighbors)
    select case (mother%eltype)
      case(23,26) ! triangle element
        ! set face pointers
        child_ptrs(1)%point%faces(2)%point => mother%interior_faces(1)%point
        child_ptrs(4)%point%faces(2)%point => mother%interior_faces(1)%point
        
        child_ptrs(2)%point%faces(3)%point => mother%interior_faces(2)%point
        child_ptrs(4)%point%faces(3)%point => mother%interior_faces(2)%point
        
        child_ptrs(3)%point%faces(1)%point => mother%interior_faces(3)%point
        child_ptrs(4)%point%faces(1)%point => mother%interior_faces(3)%point
        
        ! set face to neighbor pointers
        mother%interior_faces(1)%point%elem_neighbors(1)%point => mother%children(1)%point
        mother%interior_faces(1)%point%neighbor_loc_face_no(1) = 2
        mother%interior_faces(1)%point%elem_neighbors(2)%point => mother%children(4)%point
        mother%interior_faces(1)%point%neighbor_loc_face_no(2) = 2
        
        mother%interior_faces(2)%point%elem_neighbors(1)%point => mother%children(2)%point
        mother%interior_faces(2)%point%neighbor_loc_face_no(1) = 3
        mother%interior_faces(2)%point%elem_neighbors(2)%point => mother%children(4)%point
        mother%interior_faces(2)%point%neighbor_loc_face_no(2) = 3
        
        mother%interior_faces(3)%point%elem_neighbors(1)%point => mother%children(3)%point
        mother%interior_faces(3)%point%neighbor_loc_face_no(1) = 1
        mother%interior_faces(3)%point%elem_neighbors(2)%point => mother%children(4)%point
        mother%interior_faces(3)%point%neighbor_loc_face_no(2) = 1
        
        ! set vertex pointers
        child_ptrs(1)%point%vertices(1)%point => mother%vertices(1)%point
        child_ptrs(2)%point%vertices(2)%point => mother%vertices(2)%point
        child_ptrs(3)%point%vertices(3)%point => mother%vertices(3)%point
        
        ! in case of quadratic triangle the corners of child 4 already exist
        ! and 3 new vertices on the edges of child 4 can be created
        if (mother%eltype == 26) then ! should be at the face-part below
          call error('Not supported mother%eltype==26')
        endif
        
        
      case(24,28,29) ! quad element
        ! set face pointers
        child_ptrs(1)%point%faces(2)%point => mother%interior_faces(1)%point
        child_ptrs(2)%point%faces(4)%point => mother%interior_faces(1)%point
        
        child_ptrs(2)%point%faces(3)%point => mother%interior_faces(2)%point
        child_ptrs(3)%point%faces(1)%point => mother%interior_faces(2)%point
        
        child_ptrs(3)%point%faces(4)%point => mother%interior_faces(3)%point
        child_ptrs(4)%point%faces(2)%point => mother%interior_faces(3)%point
        
        child_ptrs(1)%point%faces(3)%point => mother%interior_faces(4)%point
        child_ptrs(4)%point%faces(1)%point => mother%interior_faces(4)%point
        
        ! set face to neighbor pointers
        mother%interior_faces(1)%point%elem_neighbors(1)%point => mother%children(1)%point
        mother%interior_faces(1)%point%neighbor_loc_face_no(1) = 2
        mother%interior_faces(1)%point%elem_neighbors(2)%point => mother%children(2)%point
        mother%interior_faces(1)%point%neighbor_loc_face_no(2) = 4
        
        mother%interior_faces(2)%point%elem_neighbors(1)%point => mother%children(2)%point
        mother%interior_faces(2)%point%neighbor_loc_face_no(1) = 3
        mother%interior_faces(2)%point%elem_neighbors(2)%point => mother%children(3)%point
        mother%interior_faces(2)%point%neighbor_loc_face_no(2) = 1
        
        mother%interior_faces(3)%point%elem_neighbors(1)%point => mother%children(3)%point
        mother%interior_faces(3)%point%neighbor_loc_face_no(1) = 4
        mother%interior_faces(3)%point%elem_neighbors(2)%point => mother%children(4)%point
        mother%interior_faces(3)%point%neighbor_loc_face_no(2) = 2
        
        mother%interior_faces(4)%point%elem_neighbors(1)%point => mother%children(1)%point
        mother%interior_faces(4)%point%neighbor_loc_face_no(1) = 3
        mother%interior_faces(4)%point%elem_neighbors(2)%point => mother%children(4)%point
        mother%interior_faces(4)%point%neighbor_loc_face_no(2) = 1
        
        ! set vertex pointers
        child_ptrs(1)%point%vertices(1)%point => mother%vertices(1)%point
        child_ptrs(2)%point%vertices(2)%point => mother%vertices(2)%point
        child_ptrs(3)%point%vertices(3)%point => mother%vertices(3)%point
        child_ptrs(4)%point%vertices(4)%point => mother%vertices(4)%point
        
        ! new point at center of quad
        vertex = mesh%no_vertices + 1
        vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + mother%vertices(2)%point%coordinate%xyz +&
                               mother%vertices(3)%point%coordinate%xyz + mother%vertices(4)%point%coordinate%xyz)/4.0_dp
        call insert_vertex(vertex,vertex_position,mesh%vertex_list_head,mesh%vertex_list_tail)
        mesh%no_vertices = mesh%no_vertices + 1
        child_ptrs(1)%point%vertices(3)%point => mesh%vertex_list_tail
        child_ptrs(2)%point%vertices(4)%point => mesh%vertex_list_tail
        child_ptrs(3)%point%vertices(1)%point => mesh%vertex_list_tail
        child_ptrs(4)%point%vertices(2)%point => mesh%vertex_list_tail
        
        ! in case of quadratic quad the corners of child 4 already exist
        if (mother%eltype == 28) call error('Not supported mother%eltype==28')
        
        
      case(34,310) ! tet element
        ! set face pointers
        child_ptrs(1)%point%faces(1)%point => mother%interior_faces(1)%point
        child_ptrs(7)%point%faces(3)%point => mother%interior_faces(1)%point
        
        child_ptrs(2)%point%faces(2)%point => mother%interior_faces(2)%point
        child_ptrs(6)%point%faces(2)%point => mother%interior_faces(2)%point
        
        child_ptrs(3)%point%faces(3)%point => mother%interior_faces(3)%point
        child_ptrs(5)%point%faces(3)%point => mother%interior_faces(3)%point
        
        child_ptrs(4)%point%faces(4)%point => mother%interior_faces(4)%point
        child_ptrs(8)%point%faces(4)%point => mother%interior_faces(4)%point
        
        child_ptrs(5)%point%faces(2)%point => mother%interior_faces(5)%point
        child_ptrs(6)%point%faces(3)%point => mother%interior_faces(5)%point
        
        child_ptrs(7)%point%faces(1)%point => mother%interior_faces(6)%point
        child_ptrs(8)%point%faces(1)%point => mother%interior_faces(6)%point
        
        child_ptrs(5)%point%faces(1)%point => mother%interior_faces(7)%point
        child_ptrs(7)%point%faces(4)%point => mother%interior_faces(7)%point
        
        child_ptrs(6)%point%faces(4)%point => mother%interior_faces(8)%point
        child_ptrs(8)%point%faces(2)%point => mother%interior_faces(8)%point
        
        ! set face to neighbor pointers
        mother%interior_faces(1)%point%elem_neighbors(1)%point => mother%children(1)%point
        mother%interior_faces(1)%point%neighbor_loc_face_no(1) = 1
        mother%interior_faces(1)%point%elem_neighbors(2)%point => mother%children(7)%point
        mother%interior_faces(1)%point%neighbor_loc_face_no(2) = 3
        
        mother%interior_faces(2)%point%elem_neighbors(1)%point => mother%children(2)%point
        mother%interior_faces(2)%point%neighbor_loc_face_no(1) = 2
        mother%interior_faces(2)%point%elem_neighbors(2)%point => mother%children(6)%point
        mother%interior_faces(2)%point%neighbor_loc_face_no(2) = 2
        
        mother%interior_faces(3)%point%elem_neighbors(1)%point => mother%children(3)%point
        mother%interior_faces(3)%point%neighbor_loc_face_no(1) = 3
        mother%interior_faces(3)%point%elem_neighbors(2)%point => mother%children(5)%point
        mother%interior_faces(3)%point%neighbor_loc_face_no(2) = 3
        
        mother%interior_faces(4)%point%elem_neighbors(1)%point => mother%children(4)%point
        mother%interior_faces(4)%point%neighbor_loc_face_no(1) = 4
        mother%interior_faces(4)%point%elem_neighbors(2)%point => mother%children(8)%point
        mother%interior_faces(4)%point%neighbor_loc_face_no(2) = 4
        
        mother%interior_faces(5)%point%elem_neighbors(1)%point => mother%children(5)%point
        mother%interior_faces(5)%point%neighbor_loc_face_no(1) = 2
        mother%interior_faces(5)%point%elem_neighbors(2)%point => mother%children(6)%point
        mother%interior_faces(5)%point%neighbor_loc_face_no(2) = 3
        
        mother%interior_faces(6)%point%elem_neighbors(1)%point => mother%children(7)%point
        mother%interior_faces(6)%point%neighbor_loc_face_no(1) = 1
        mother%interior_faces(6)%point%elem_neighbors(2)%point => mother%children(8)%point
        mother%interior_faces(6)%point%neighbor_loc_face_no(2) = 1
        
        mother%interior_faces(7)%point%elem_neighbors(1)%point => mother%children(5)%point
        mother%interior_faces(7)%point%neighbor_loc_face_no(1) = 1
        mother%interior_faces(7)%point%elem_neighbors(2)%point => mother%children(7)%point
        mother%interior_faces(7)%point%neighbor_loc_face_no(2) = 4
        
        mother%interior_faces(8)%point%elem_neighbors(1)%point => mother%children(6)%point
        mother%interior_faces(8)%point%neighbor_loc_face_no(1) = 4
        mother%interior_faces(8)%point%elem_neighbors(2)%point => mother%children(8)%point
        mother%interior_faces(8)%point%neighbor_loc_face_no(2) = 2
        
        ! set vertex pointers
        child_ptrs(1)%point%vertices(1)%point => mother%vertices(1)%point
        child_ptrs(2)%point%vertices(2)%point => mother%vertices(2)%point
        child_ptrs(3)%point%vertices(3)%point => mother%vertices(3)%point
        child_ptrs(4)%point%vertices(4)%point => mother%vertices(4)%point
        
        if (mother%eltype == 310) call error('Not supported mother%eltype==310')
        
      case(38,320) ! hex element
        ! set face pointers
        child_ptrs(1)%point%faces(4)%point => mother%interior_faces(1)%point
        child_ptrs(2)%point%faces(6)%point => mother%interior_faces(1)%point
        
        child_ptrs(2)%point%faces(5)%point => mother%interior_faces(2)%point
        child_ptrs(3)%point%faces(3)%point => mother%interior_faces(2)%point
        
        child_ptrs(3)%point%faces(6)%point => mother%interior_faces(3)%point
        child_ptrs(4)%point%faces(4)%point => mother%interior_faces(3)%point
        
        child_ptrs(1)%point%faces(5)%point => mother%interior_faces(4)%point
        child_ptrs(4)%point%faces(3)%point => mother%interior_faces(4)%point
        
        child_ptrs(5)%point%faces(4)%point => mother%interior_faces(5)%point
        child_ptrs(6)%point%faces(6)%point => mother%interior_faces(5)%point
        
        child_ptrs(6)%point%faces(5)%point => mother%interior_faces(6)%point
        child_ptrs(7)%point%faces(3)%point => mother%interior_faces(6)%point
        
        child_ptrs(7)%point%faces(6)%point => mother%interior_faces(7)%point
        child_ptrs(8)%point%faces(4)%point => mother%interior_faces(7)%point
        
        child_ptrs(5)%point%faces(5)%point => mother%interior_faces(8)%point
        child_ptrs(8)%point%faces(3)%point => mother%interior_faces(8)%point
        
        child_ptrs(1)%point%faces(2)%point => mother%interior_faces(9)%point
        child_ptrs(5)%point%faces(1)%point => mother%interior_faces(9)%point
        
        child_ptrs(2)%point%faces(2)%point => mother%interior_faces(10)%point
        child_ptrs(6)%point%faces(1)%point => mother%interior_faces(10)%point
        
        child_ptrs(3)%point%faces(2)%point => mother%interior_faces(11)%point
        child_ptrs(7)%point%faces(1)%point => mother%interior_faces(11)%point
        
        child_ptrs(4)%point%faces(2)%point => mother%interior_faces(12)%point
        child_ptrs(8)%point%faces(1)%point => mother%interior_faces(12)%point
        
        ! set face to neighbor pointers
        ! lower level (4x)
        mother%interior_faces(1)%point%elem_neighbors(1)%point => mother%children(1)%point
        mother%interior_faces(1)%point%neighbor_loc_face_no(1) = 4
        mother%interior_faces(1)%point%elem_neighbors(2)%point => mother%children(2)%point
        mother%interior_faces(1)%point%neighbor_loc_face_no(2) = 6
        
        mother%interior_faces(2)%point%elem_neighbors(1)%point => mother%children(2)%point
        mother%interior_faces(2)%point%neighbor_loc_face_no(1) = 5
        mother%interior_faces(2)%point%elem_neighbors(2)%point => mother%children(3)%point
        mother%interior_faces(2)%point%neighbor_loc_face_no(2) = 3
        
        mother%interior_faces(3)%point%elem_neighbors(1)%point => mother%children(3)%point
        mother%interior_faces(3)%point%neighbor_loc_face_no(1) = 6
        mother%interior_faces(3)%point%elem_neighbors(2)%point => mother%children(4)%point
        mother%interior_faces(3)%point%neighbor_loc_face_no(2) = 4
        
        mother%interior_faces(4)%point%elem_neighbors(1)%point => mother%children(1)%point
        mother%interior_faces(4)%point%neighbor_loc_face_no(1) = 5
        mother%interior_faces(4)%point%elem_neighbors(2)%point => mother%children(4)%point
        mother%interior_faces(4)%point%neighbor_loc_face_no(2) = 3
        
        ! upper level (4x)
        mother%interior_faces(5)%point%elem_neighbors(1)%point => mother%children(5)%point
        mother%interior_faces(5)%point%neighbor_loc_face_no(1) = 4
        mother%interior_faces(5)%point%elem_neighbors(2)%point => mother%children(6)%point
        mother%interior_faces(5)%point%neighbor_loc_face_no(2) = 6
        
        mother%interior_faces(6)%point%elem_neighbors(1)%point => mother%children(6)%point
        mother%interior_faces(6)%point%neighbor_loc_face_no(1) = 5
        mother%interior_faces(6)%point%elem_neighbors(2)%point => mother%children(7)%point
        mother%interior_faces(6)%point%neighbor_loc_face_no(2) = 3
        
        mother%interior_faces(7)%point%elem_neighbors(1)%point => mother%children(7)%point
        mother%interior_faces(7)%point%neighbor_loc_face_no(1) = 6
        mother%interior_faces(7)%point%elem_neighbors(2)%point => mother%children(8)%point
        mother%interior_faces(7)%point%neighbor_loc_face_no(2) = 4
        
        mother%interior_faces(8)%point%elem_neighbors(1)%point => mother%children(5)%point
        mother%interior_faces(8)%point%neighbor_loc_face_no(1) = 5
        mother%interior_faces(8)%point%elem_neighbors(2)%point => mother%children(8)%point
        mother%interior_faces(8)%point%neighbor_loc_face_no(2) = 3
        
        ! mid level (4x)
        mother%interior_faces(9)%point%elem_neighbors(1)%point => mother%children(1)%point
        mother%interior_faces(9)%point%neighbor_loc_face_no(1) = 2
        mother%interior_faces(9)%point%elem_neighbors(2)%point => mother%children(5)%point
        mother%interior_faces(9)%point%neighbor_loc_face_no(2) = 1
        
        mother%interior_faces(10)%point%elem_neighbors(1)%point => mother%children(2)%point
        mother%interior_faces(10)%point%neighbor_loc_face_no(1) = 2
        mother%interior_faces(10)%point%elem_neighbors(2)%point => mother%children(6)%point
        mother%interior_faces(10)%point%neighbor_loc_face_no(2) = 1
        
        mother%interior_faces(11)%point%elem_neighbors(1)%point => mother%children(3)%point
        mother%interior_faces(11)%point%neighbor_loc_face_no(1) = 2
        mother%interior_faces(11)%point%elem_neighbors(2)%point => mother%children(7)%point
        mother%interior_faces(11)%point%neighbor_loc_face_no(2) = 1
        
        mother%interior_faces(12)%point%elem_neighbors(1)%point => mother%children(4)%point
        mother%interior_faces(12)%point%neighbor_loc_face_no(1) = 2
        mother%interior_faces(12)%point%elem_neighbors(2)%point => mother%children(8)%point
        mother%interior_faces(12)%point%neighbor_loc_face_no(2) = 1
        
        ! set vertex pointers
        child_ptrs(1)%point%vertices(1)%point => mother%vertices(1)%point
        child_ptrs(2)%point%vertices(2)%point => mother%vertices(2)%point
        child_ptrs(3)%point%vertices(3)%point => mother%vertices(3)%point
        child_ptrs(4)%point%vertices(4)%point => mother%vertices(4)%point
        child_ptrs(5)%point%vertices(5)%point => mother%vertices(5)%point
        child_ptrs(6)%point%vertices(6)%point => mother%vertices(6)%point
        child_ptrs(7)%point%vertices(7)%point => mother%vertices(7)%point
        child_ptrs(8)%point%vertices(8)%point => mother%vertices(8)%point
        
        ! new point at center of hex
        vertex = mesh%no_vertices + 1
        vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + mother%vertices(2)%point%coordinate%xyz +&
          mother%vertices(3)%point%coordinate%xyz + mother%vertices(4)%point%coordinate%xyz +&
          mother%vertices(5)%point%coordinate%xyz + mother%vertices(6)%point%coordinate%xyz +&
          mother%vertices(7)%point%coordinate%xyz + mother%vertices(8)%point%coordinate%xyz)/8.0_dp
        call insert_vertex(vertex,vertex_position,mesh%vertex_list_head,mesh%vertex_list_tail)
        mesh%no_vertices = mesh%no_vertices + 1
        child_ptrs(1)%point%vertices(7)%point => mesh%vertex_list_tail
        child_ptrs(2)%point%vertices(8)%point => mesh%vertex_list_tail
        child_ptrs(3)%point%vertices(5)%point => mesh%vertex_list_tail
        child_ptrs(4)%point%vertices(6)%point => mesh%vertex_list_tail
        child_ptrs(5)%point%vertices(3)%point => mesh%vertex_list_tail
        child_ptrs(6)%point%vertices(4)%point => mesh%vertex_list_tail
        child_ptrs(7)%point%vertices(1)%point => mesh%vertex_list_tail
        child_ptrs(8)%point%vertices(2)%point => mesh%vertex_list_tail
        
        if (mother%eltype == 320) call error('Not supported mother%eltype==320')
        
        
      case default
        call error('Not supported mother%eltype')
    end select
    
    ! Get the neigbor connections set-up
    ! Prodedure is as follows:
    ! For each face of the mother see if it is active
    !
    ! (i): if active -> the neighbor (1 only) is less refined than the children to be created
    ! setting up connections is easy in this case. The face also still needs to be
    ! refined in this case with vertices to be created. Note that this case also
    ! includes the case where we're on the boundary. Except here we have no
    ! neighbors to take care of.
    !
    ! (ii): if not active -> the neighbor is equally or more refined than the children to be created
    ! here we have to deal with multiple neighbors. Depending on the refinement of
    ! the mother's neigbors there may be an arbitrarily high number of neigbors present.
    ! Here we use the fact that the mesh is hierarchic: if we are on the same level
    ! as the children who the neighbors are then we can see from the tree who are
    ! also neighbors of each specific child.
    
    ! Fill some logicals to make decisions when needed
    do face = 1, no_elem_faces
      mother_face => mother%faces(face)%point
      casei = mother_face%active
      caseii = .not. mother_face%active
      
      ! Generate the children faces if they do not exist yet or find the pointers when they do
      if (casei) then
        ! deactivate the mother face
        mother_face%active = .false.
        ! generate the children faces, set pointers, inherit properties
        no_face_children = get_no_children(eltype_of_faces_of_elem(mother%eltype))
        allocate(mother_face%children(no_face_children))
        do child_face_index = 1, no_face_children
          allocate(mother_face%children(child_face_index)%point)
          child_face => mother_face%children(child_face_index)%point
          mother_face%children(child_face_index)%point => child_face
          child_face%mother => mother_face
          child_face%active = .true.
          child_face%level = child_level
          child_face%ftype = mother_face%ftype
          child_face%bc_id = mother_face%bc_id
          if (child_face%ftype == BOUNDARY) then
            allocate(child_face%elem_neighbors(1))
          else
            allocate(child_face%elem_neighbors(2))
          endif
          nullify(child_face)
        enddo
        
        ! put children in array of pointers for later use
        allocate(child_face_ptrs(no_face_children))
        do child_face_index = 1, no_face_children
          child_face_ptrs(child_face_index)%point => mother_face%children(child_face_index)%point
        enddo
        
        ! setup the face_to_child array that is used later on to get 
        ! a more or less uniform implementation of things
        select case (mother%eltype)
          case(23,26) ! triangle
            select case (face)
              case(1)
                face_to_child(1) = 1
                face_to_child(2) = 2
              case(2)
                face_to_child(1) = 2
                face_to_child(2) = 3
              case(3)
                face_to_child(1) = 1
                face_to_child(2) = 3
              case default
                call error('Wrong face number')
            end select
          
          case(24,28) ! quad
            select case (face)
              case(1)
                face_to_child(1) = 1
                face_to_child(2) = 2
              case(2)
                face_to_child(1) = 2
                face_to_child(2) = 3
              case(3)
                face_to_child(1) = 3
                face_to_child(2) = 4
              case(4)
                face_to_child(1) = 1
                face_to_child(2) = 4
              case default
                call error('Wrong face number')
            end select
          
          case(34,310) ! tet
            select case (face)
              case(1)
                face_to_child(1) = 2
                face_to_child(2) = 3
                face_to_child(3) = 4
                face_to_child(4) = 6
              case(2)
                face_to_child(1) = 1
                face_to_child(2) = 3
                face_to_child(3) = 4
                face_to_child(4) = 7
              case(3)
                face_to_child(1) = 1
                face_to_child(2) = 2
                face_to_child(3) = 4
                face_to_child(4) = 8
              case(4)
                face_to_child(1) = 1
                face_to_child(2) = 2
                face_to_child(3) = 3
                face_to_child(4) = 5
              case default
                call error('Wrong face number')
            end select
          
          case(38,320) ! hex
            select case (face)
              case(1)
                face_to_child(1) = 1
                face_to_child(2) = 2
                face_to_child(3) = 3
                face_to_child(4) = 4
              case(2)
                face_to_child(1) = 5
                face_to_child(2) = 6
                face_to_child(3) = 7
                face_to_child(4) = 8
              case(3)
                face_to_child(1) = 1
                face_to_child(2) = 2
                face_to_child(3) = 5
                face_to_child(4) = 6
              case(4)
                face_to_child(1) = 2
                face_to_child(2) = 3
                face_to_child(3) = 6
                face_to_child(4) = 7
              case(5)
                face_to_child(1) = 3
                face_to_child(2) = 4
                face_to_child(3) = 7
                face_to_child(4) = 8
              case(6)
                face_to_child(1) = 1
                face_to_child(2) = 4
                face_to_child(3) = 5
                face_to_child(4) = 8
              case default
                call error('Wrong face number')
            end select
            
          case default
            call error('Not supported mother%eltype')
        end select
        
        
      else ! case ii
        ! get the children faces (only one level below mother_face) and put in face_ptr_array
        ! and get the associated neighbor elements and put these in elem_ptrs
        no_face_children = get_no_children(eltype_of_faces_of_elem(mother%eltype))
        allocate(child_face_ptrs(no_face_children))
        allocate(elem_ptrs(no_face_children))
        do child_face_index = 1, no_face_children
          child_face_ptrs(child_face_index)%point => mother_face%children(child_face_index)%point
          if (associated(mother_face%children(child_face_index)%point%elem_neighbors(1)%point,mother)) then
            elem_ptrs(child_face_index)%point => mother_face%children(child_face_index)%point%elem_neighbors(2)%point
          else
            elem_ptrs(child_face_index)%point => mother_face%children(child_face_index)%point%elem_neighbors(1)%point
          endif
        enddo
        
        ! find out the correspondence of the children faces to the element children:
        ! compare the corner vertices of the mother with the vertices of the neighbors
        face_to_child = 0
        no_elem_corner_vertices=no_vertices(get_1st_order_eltype(mother%eltype))
        do mother_vertex = 1, no_elem_corner_vertices
          do child_face_index = 1, no_face_children
            do neighbor_vertex = 1, no_elem_corner_vertices
              !We compare the vertex coordinates instead of the pointers as for tets refinement 
              !it appears difficult to uniquely find all neighboring vertices when doing the neighbour search
              if (same_vtx(mother%vertices(mother_vertex)%point, &
                  elem_ptrs(child_face_index)%point%vertices(neighbor_vertex)%point)) then
                face_to_child(child_face_index) = mother_vertex
              endif
            enddo
          enddo
        enddo
        
        if (mother%eltype == 34) face_to_child(4) = get_center_tet_in_tet_face(face)
      endif
      
      ! set the new children element to face pointers
      ! here the face of the children is the same as of mother
      do child_face_index = 1, no_face_children
        child_ptrs(face_to_child(child_face_index))%point%faces(face)%point => child_face_ptrs(child_face_index)%point
      enddo
      
      ! Some relevant information about the mother's neighbor
      if (.not. mother_face%ftype == BOUNDARY) then
        if (associated(mother_face%elem_neighbors(1)%point,mother)) then
          mother_neighbor => mother_face%elem_neighbors(2)%point
          mother_neighbor_loc_face =  mother_face%neighbor_loc_face_no(2)
        else
          mother_neighbor => mother_face%elem_neighbors(1)%point
          mother_neighbor_loc_face =  mother_face%neighbor_loc_face_no(1)
        endif
      endif
      
      ! For caseii setup the connectivity
      if (caseii) then
        do child_face_index = 1, no_face_children
          face_list_head => child_face_ptrs(child_face_index)%point
          child => child_ptrs(face_to_child(child_face_index))%point
          call set_face_neighbors(face_list_head,mother,child,face)
        enddo
      endif
      
      ! For casei if there's a neighbor set the pointers from children faces to the neighbors
      if (casei) then
        do child_face_index = 1, no_face_children
          child_face_ptrs(child_face_index)%point%elem_neighbors(1)%point => child_ptrs(face_to_child(child_face_index))%point
          child_face_ptrs(child_face_index)%point%neighbor_loc_face_no(1) = face
        enddo
        if (.not. mother_face%ftype == BOUNDARY) then
          do child_face_index = 1, no_face_children
            child_face_ptrs(child_face_index)%point%elem_neighbors(2)%point => mother_neighbor
            child_face_ptrs(child_face_index)%point%neighbor_loc_face_no(2) = mother_neighbor_loc_face
          enddo
        endif
      endif
      
      ! determine the new vertex coordinates to be added on the face
      ! for casei they have to be added to the list, for caseii they need to be found
      ! by searching all elements that are neighbors
      ! this may need change for 2nd order elements
      if (mother%eltype == 23 .or. mother%eltype == 24 .or. mother%eltype == 38) then ! linear elements
        call new_face_vertex_1st_order(mother,face,vertex_position)
        if (casei) then
          vertex = mesh%no_vertices + 1
          call insert_vertex(vertex,vertex_position,mesh%vertex_list_head,mesh%vertex_list_tail)
          vertex_found => mesh%vertex_list_tail
          mesh%no_vertices = mesh%no_vertices + 1
        else
          host_elem => elem_ptrs(1)%point ! any neighbor will do for 1st order elements
          call find_vertex_in_elem(vertex_position,host_elem,vertex_found)
        endif
        
        select case (mother%eltype)
          case(23) ! linear triangle
            select case (face)
              case(1)
                child_ptrs(1)%point%vertices(2)%point => vertex_found
                child_ptrs(2)%point%vertices(1)%point => vertex_found
                child_ptrs(4)%point%vertices(3)%point => vertex_found
              case(2)
                child_ptrs(2)%point%vertices(3)%point => vertex_found
                child_ptrs(3)%point%vertices(2)%point => vertex_found
                child_ptrs(4)%point%vertices(1)%point => vertex_found
              case(3)
                child_ptrs(1)%point%vertices(3)%point => vertex_found
                child_ptrs(3)%point%vertices(1)%point => vertex_found
                child_ptrs(4)%point%vertices(2)%point => vertex_found
              case default
                call error('Wrong face number')
            end select
            
          case(24) ! linear quad
            select case (face)
              case(1)
                child_ptrs(1)%point%vertices(2)%point => vertex_found
                child_ptrs(2)%point%vertices(1)%point => vertex_found
              case(2)
                child_ptrs(2)%point%vertices(3)%point => vertex_found
                child_ptrs(3)%point%vertices(2)%point => vertex_found
              case(3)
                child_ptrs(3)%point%vertices(4)%point => vertex_found
                child_ptrs(4)%point%vertices(3)%point => vertex_found
              case(4)
                child_ptrs(1)%point%vertices(4)%point => vertex_found
                child_ptrs(4)%point%vertices(1)%point => vertex_found
              case default
                call error('Wrong face number')
            end select
            
          case(38) ! linear hex
            select case (face)
              case(1)
                child_ptrs(1)%point%vertices(3)%point => vertex_found
                child_ptrs(2)%point%vertices(4)%point => vertex_found
                child_ptrs(3)%point%vertices(1)%point => vertex_found
                child_ptrs(4)%point%vertices(2)%point => vertex_found
              case(2)
                child_ptrs(5)%point%vertices(7)%point => vertex_found
                child_ptrs(6)%point%vertices(8)%point => vertex_found
                child_ptrs(7)%point%vertices(5)%point => vertex_found
                child_ptrs(8)%point%vertices(6)%point => vertex_found
              case(3)
                child_ptrs(1)%point%vertices(6)%point => vertex_found
                child_ptrs(2)%point%vertices(5)%point => vertex_found
                child_ptrs(5)%point%vertices(2)%point => vertex_found
                child_ptrs(6)%point%vertices(1)%point => vertex_found
              case(4)
                child_ptrs(2)%point%vertices(7)%point => vertex_found
                child_ptrs(3)%point%vertices(6)%point => vertex_found
                child_ptrs(6)%point%vertices(3)%point => vertex_found
                child_ptrs(7)%point%vertices(2)%point => vertex_found
              case(5)
                child_ptrs(3)%point%vertices(8)%point => vertex_found
                child_ptrs(4)%point%vertices(7)%point => vertex_found
                child_ptrs(7)%point%vertices(4)%point => vertex_found
                child_ptrs(8)%point%vertices(3)%point => vertex_found
              case(6)
                child_ptrs(1)%point%vertices(8)%point => vertex_found
                child_ptrs(4)%point%vertices(5)%point => vertex_found
                child_ptrs(5)%point%vertices(4)%point => vertex_found
                child_ptrs(8)%point%vertices(1)%point => vertex_found
              case default
                call error('Wrong face number')
            end select
            
          case default
            call error('Not supported mother%eltype')
        end select
      endif
    enddo
    
    if (mother%eltype == 34 .or. mother%eltype == 38) then ! 3D elements
      ! Determine the new vertex coordinates to be added on the edges.
      ! This is done by looking at edge-elem connections.
      !
      ! To implement the search we choose the simplest approach which is
      ! computationally expensive but is the easiest to code:
      !
      ! i)  get a list of elements at child_level that are neighbors of mother or
      !     neighbors of the neighbors
      ! ii) search through this list for vertices (outer loop is neighbor loop: cheapest on average)
      
      ! Need to correct these numbers...
      allocate(neighbor_ptrs(64*216)) ! estimated maximum (on the high side)
      allocate(vertex_ptrs(12))       ! maximum that occurs for hex
      
      ! Get the neighbors
      call get_elem_neighbors_at_childlevel(mesh%tot_no_elem,mother,child_level,no_neighbors,neighbor_ptrs)
      no_elem_edges = get_no_elem_edges(mother%eltype)
      do neighbor = 1, no_neighbors
        host_elem => neighbor_ptrs(neighbor)%point
        if (associated(host_elem)) then
          do edge_index = 1, no_elem_edges
            call new_edge_vertex_1st_order(mother,edge_index,vertex_position)
            call find_vertex_in_elem(vertex_position,host_elem,vertex_found)
            if (associated(vertex_found)) vertex_ptrs(edge_index)%point => vertex_found
          enddo
        endif
      enddo
      
      ! check whether the vertices have been found or not
      do edge_index = 1, no_elem_edges
        if (.not. associated(vertex_ptrs(edge_index)%point)) then
          call new_edge_vertex_1st_order(mother,edge_index,vertex_position)
          vertex = mesh%no_vertices + 1
          call insert_vertex(vertex,vertex_position,mesh%vertex_list_head,mesh%vertex_list_tail)
          vertex_ptrs(edge_index)%point => mesh%vertex_list_tail
          mesh%no_vertices = mesh%no_vertices + 1
        endif
      enddo
      
      select case (mother%eltype)
        case(34) ! linear tet
          child_ptrs(1)%point%vertices(2)%point => vertex_ptrs(1)%point
          child_ptrs(2)%point%vertices(1)%point => vertex_ptrs(1)%point
          child_ptrs(5)%point%vertices(3)%point => vertex_ptrs(1)%point
          child_ptrs(6)%point%vertices(1)%point => vertex_ptrs(1)%point
          child_ptrs(7)%point%vertices(2)%point => vertex_ptrs(1)%point
          child_ptrs(8)%point%vertices(4)%point => vertex_ptrs(1)%point
          
          child_ptrs(2)%point%vertices(3)%point => vertex_ptrs(2)%point
          child_ptrs(3)%point%vertices(2)%point => vertex_ptrs(2)%point
          child_ptrs(5)%point%vertices(1)%point => vertex_ptrs(2)%point
          child_ptrs(6)%point%vertices(4)%point => vertex_ptrs(2)%point
          
          child_ptrs(1)%point%vertices(3)%point => vertex_ptrs(3)%point
          child_ptrs(3)%point%vertices(1)%point => vertex_ptrs(3)%point
          child_ptrs(5)%point%vertices(2)%point => vertex_ptrs(3)%point
          child_ptrs(7)%point%vertices(1)%point => vertex_ptrs(3)%point
          
          child_ptrs(1)%point%vertices(4)%point => vertex_ptrs(4)%point
          child_ptrs(4)%point%vertices(1)%point => vertex_ptrs(4)%point
          child_ptrs(7)%point%vertices(4)%point => vertex_ptrs(4)%point
          child_ptrs(8)%point%vertices(2)%point => vertex_ptrs(4)%point
          
          child_ptrs(2)%point%vertices(4)%point => vertex_ptrs(5)%point
          child_ptrs(4)%point%vertices(2)%point => vertex_ptrs(5)%point
          child_ptrs(6)%point%vertices(3)%point => vertex_ptrs(5)%point
          child_ptrs(8)%point%vertices(1)%point => vertex_ptrs(5)%point
          
          child_ptrs(3)%point%vertices(4)%point => vertex_ptrs(6)%point
          child_ptrs(4)%point%vertices(3)%point => vertex_ptrs(6)%point
          child_ptrs(5)%point%vertices(4)%point => vertex_ptrs(6)%point
          child_ptrs(6)%point%vertices(2)%point => vertex_ptrs(6)%point
          child_ptrs(7)%point%vertices(3)%point => vertex_ptrs(6)%point
          child_ptrs(8)%point%vertices(3)%point => vertex_ptrs(6)%point
          
        case(38) ! linear hex
          child_ptrs(1)%point%vertices(2)%point => vertex_ptrs(1)%point
          child_ptrs(2)%point%vertices(1)%point => vertex_ptrs(1)%point
          
          child_ptrs(2)%point%vertices(3)%point => vertex_ptrs(2)%point
          child_ptrs(3)%point%vertices(2)%point => vertex_ptrs(2)%point
          
          child_ptrs(3)%point%vertices(4)%point => vertex_ptrs(3)%point
          child_ptrs(4)%point%vertices(3)%point => vertex_ptrs(3)%point
          
          child_ptrs(1)%point%vertices(4)%point => vertex_ptrs(4)%point
          child_ptrs(4)%point%vertices(1)%point => vertex_ptrs(4)%point
          
          child_ptrs(5)%point%vertices(6)%point => vertex_ptrs(5)%point
          child_ptrs(6)%point%vertices(5)%point => vertex_ptrs(5)%point
          
          child_ptrs(6)%point%vertices(7)%point => vertex_ptrs(6)%point
          child_ptrs(7)%point%vertices(6)%point => vertex_ptrs(6)%point
          
          child_ptrs(7)%point%vertices(8)%point => vertex_ptrs(7)%point
          child_ptrs(8)%point%vertices(7)%point => vertex_ptrs(7)%point
          
          child_ptrs(5)%point%vertices(8)%point => vertex_ptrs(8)%point
          child_ptrs(8)%point%vertices(5)%point => vertex_ptrs(8)%point
          
          child_ptrs(1)%point%vertices(5)%point => vertex_ptrs(9)%point
          child_ptrs(5)%point%vertices(1)%point => vertex_ptrs(9)%point
          
          child_ptrs(2)%point%vertices(6)%point => vertex_ptrs(10)%point
          child_ptrs(6)%point%vertices(2)%point => vertex_ptrs(10)%point
          
          child_ptrs(3)%point%vertices(7)%point => vertex_ptrs(11)%point
          child_ptrs(7)%point%vertices(3)%point => vertex_ptrs(11)%point
          
          child_ptrs(4)%point%vertices(8)%point => vertex_ptrs(12)%point
          child_ptrs(8)%point%vertices(4)%point => vertex_ptrs(12)%point
      end select
    endif
    
    ! cleanup pointers
    nullify(mother)
    nullify(mother_neighbor)
    nullify(child)
    nullify(mother_face)
    nullify(child_face)
    nullify(face_list_head)
  end subroutine h_refine_elem
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  recursive subroutine set_face_neighbors(face_head,mother,child,loc_face)
    ! Traverse the tree of active faces and set the face to element pointers and
    type(face_list), intent(inout) :: face_head
    type(elem_list), pointer, intent(in) :: mother, child
    integer, intent(in) :: loc_face
    
    integer :: child_face_index, no_face_children
    
    ! for any active element set the pointers
    ! find out which neighbor is the mother (1 or 2) then replace that entry by the child
    if (face_head%active) then
      if (associated(face_head%elem_neighbors(1)%point,mother)) then
        face_head%elem_neighbors(1)%point => child
        face_head%neighbor_loc_face_no(1) = loc_face
      else
        face_head%elem_neighbors(2)%point => child
        face_head%neighbor_loc_face_no(2) = loc_face
      endif
    else 
      ! not active so process the children
      no_face_children = size(face_head%children)
      do child_face_index = 1, no_face_children
        call set_face_neighbors(face_head%children(child_face_index)%point,mother,child,loc_face)
      enddo
    endif
  end subroutine set_face_neighbors
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine new_face_vertex_1st_order(mother,face,vertex_position)
    ! return the coordinates of the new vertex on face of element mother
    ! we do this simply by averaging the relevant corner vertex coordinates
    type(elem_list), intent(in) :: mother
    integer, intent(in) :: face
    type(vertex_coordinate), intent(out) :: vertex_position
    
    select case (mother%eltype)
      case(23) ! triangle
        select case(face)
          case(1)
            vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + &
                                   mother%vertices(2)%point%coordinate%xyz) / 2.0_dp
          case(2)
            vertex_position%xyz = (mother%vertices(2)%point%coordinate%xyz + &
                                   mother%vertices(3)%point%coordinate%xyz) / 2.0_dp
          case(3)
            vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + &
                                   mother%vertices(3)%point%coordinate%xyz) / 2.0_dp
          case default
            call error('Wrong face number')
        end select
        
      case(24) ! quad
        select case(face)
          case(1)
            vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + &
                                   mother%vertices(2)%point%coordinate%xyz) / 2.0_dp
          case(2)
            vertex_position%xyz = (mother%vertices(2)%point%coordinate%xyz + &
                                   mother%vertices(3)%point%coordinate%xyz) / 2.0_dp
          case(3)
            vertex_position%xyz = (mother%vertices(3)%point%coordinate%xyz + &
                                   mother%vertices(4)%point%coordinate%xyz) / 2.0_dp
          case(4)
            vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + &
                                   mother%vertices(4)%point%coordinate%xyz) / 2.0_dp
          case default
            call error('Wrong face number')
        end select
        
      case(38) ! hex
        select case(face)
          case(1)
            vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + &
                                   mother%vertices(2)%point%coordinate%xyz + &
                                   mother%vertices(3)%point%coordinate%xyz + &
                                   mother%vertices(4)%point%coordinate%xyz) / 4.0_dp
          case(2)
            vertex_position%xyz = (mother%vertices(5)%point%coordinate%xyz + &
                                   mother%vertices(6)%point%coordinate%xyz + &
                                   mother%vertices(7)%point%coordinate%xyz + &
                                   mother%vertices(8)%point%coordinate%xyz) / 4.0_dp
          case(3)
            vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + &
                                   mother%vertices(2)%point%coordinate%xyz + &
                                   mother%vertices(5)%point%coordinate%xyz + &
                                   mother%vertices(6)%point%coordinate%xyz) / 4.0_dp
          case(4)
            vertex_position%xyz = (mother%vertices(2)%point%coordinate%xyz + &
                                   mother%vertices(3)%point%coordinate%xyz + &
                                   mother%vertices(6)%point%coordinate%xyz + &
                                   mother%vertices(7)%point%coordinate%xyz) / 4.0_dp
          case(5)
            vertex_position%xyz = (mother%vertices(3)%point%coordinate%xyz + &
                                   mother%vertices(4)%point%coordinate%xyz + &
                                   mother%vertices(7)%point%coordinate%xyz + &
                                   mother%vertices(8)%point%coordinate%xyz) / 4.0_dp
          case(6)
            vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + &
                                   mother%vertices(4)%point%coordinate%xyz + &
                                   mother%vertices(5)%point%coordinate%xyz + &
                                   mother%vertices(8)%point%coordinate%xyz) / 4.0_dp
          case default
            call error('Wrong face number')
        end select
        
      case default
        call error('Unsupported mother%eltype')
    end select
  end subroutine new_face_vertex_1st_order
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine new_edge_vertex_1st_order(mother,edge,vertex_position)
    ! return the coordinates of the new vertex on edge of element mother
    ! we do this simply by averaging the relevant corner vertex coordinates
    type(elem_list), intent(in) :: mother
    integer, intent(in) :: edge
    type(vertex_coordinate), intent(out) :: vertex_position
    
    select case (mother%eltype)
      case(34) ! tet
        select case(edge)
          case(1)
            vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + &
                                   mother%vertices(2)%point%coordinate%xyz) / 2.0_dp
          case(2)
            vertex_position%xyz = (mother%vertices(2)%point%coordinate%xyz + &
                                   mother%vertices(3)%point%coordinate%xyz) / 2.0_dp
          case(3)
            vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + &
                                   mother%vertices(3)%point%coordinate%xyz) / 2.0_dp
          case(4)
            vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + &
                                   mother%vertices(4)%point%coordinate%xyz) / 2.0_dp
          case(5)
            vertex_position%xyz = (mother%vertices(2)%point%coordinate%xyz + &
                                   mother%vertices(4)%point%coordinate%xyz) / 2.0_dp
          case(6)
            vertex_position%xyz = (mother%vertices(3)%point%coordinate%xyz + &
                                   mother%vertices(4)%point%coordinate%xyz) / 2.0_dp
          case default
            call error('Wrong edge number')
        end select
        
      case(38) ! hex
        select case(edge)
          case(1)
            vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + &
                                   mother%vertices(2)%point%coordinate%xyz) / 2.0_dp
          case(2)
            vertex_position%xyz = (mother%vertices(2)%point%coordinate%xyz + &
                                   mother%vertices(3)%point%coordinate%xyz) / 2.0_dp
          case(3)
            vertex_position%xyz = (mother%vertices(3)%point%coordinate%xyz + &
                                   mother%vertices(4)%point%coordinate%xyz) / 2.0_dp
          case(4)
            vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + &
                                   mother%vertices(4)%point%coordinate%xyz) / 2.0_dp
          case(5)
            vertex_position%xyz = (mother%vertices(5)%point%coordinate%xyz + &
                                   mother%vertices(6)%point%coordinate%xyz) / 2.0_dp
          case(6)
            vertex_position%xyz = (mother%vertices(6)%point%coordinate%xyz + &
                                   mother%vertices(7)%point%coordinate%xyz) / 2.0_dp
          case(7)
            vertex_position%xyz = (mother%vertices(7)%point%coordinate%xyz + &
                                   mother%vertices(8)%point%coordinate%xyz) / 2.0_dp
          case(8)
            vertex_position%xyz = (mother%vertices(5)%point%coordinate%xyz + &
                                   mother%vertices(8)%point%coordinate%xyz) / 2.0_dp
          case(9)
            vertex_position%xyz = (mother%vertices(1)%point%coordinate%xyz + &
                                   mother%vertices(5)%point%coordinate%xyz) / 2.0_dp
          case(10)
            vertex_position%xyz = (mother%vertices(2)%point%coordinate%xyz + &
                                   mother%vertices(6)%point%coordinate%xyz) / 2.0_dp
          case(11)
            vertex_position%xyz = (mother%vertices(3)%point%coordinate%xyz + &
                                   mother%vertices(7)%point%coordinate%xyz) / 2.0_dp
          case(12)
            vertex_position%xyz = (mother%vertices(4)%point%coordinate%xyz + &
                                   mother%vertices(8)%point%coordinate%xyz) / 2.0_dp
          case default
            call error('Wrong edge number')
        end select
        
      case default
        call error('Unsupported mother%eltype')
    end select
  end subroutine new_edge_vertex_1st_order
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine find_vertex_in_elem(vertex_position,host_elem,vertex_found)
    ! Find the vertex with coordinates vertex_position in host_elem and return the
    ! pointer to the elem: vertex_found
    type(vertex_coordinate), intent(in) :: vertex_position
    type(elem_list), intent(in) :: host_elem
    type(vertex_list), pointer, intent(out) :: vertex_found
    
    integer :: no_vertices, vertex
    real(dp) :: x, y, z, x_test, y_test, z_test, error
    
    no_vertices = size(host_elem%vertices)
    x = vertex_position%xyz(1)
    y = vertex_position%xyz(2)
    z = vertex_position%xyz(3)
    
    do vertex = 1, no_vertices
      x_test = host_elem%vertices(vertex)%point%coordinate%xyz(1)
      y_test = host_elem%vertices(vertex)%point%coordinate%xyz(2)
      z_test = host_elem%vertices(vertex)%point%coordinate%xyz(3)
      error = (x-x_test)**2.0_dp + (y-y_test)**2.0_dp + (z-z_test)**2.0_dp
      if (error < 1.e-10_dp) then
        vertex_found => host_elem%vertices(vertex)%point
        return
      endif
    enddo
    vertex_found => null()
  end subroutine find_vertex_in_elem
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine get_elem_neighbors_at_childlevel(no_mesh_elems,mother,child_level,no_neighbors,neighbor_ptrs)
    ! Make a list of neighbors of mother and their subsequent neighbors.
    ! These may in fact contain mother itself (excluded immediately) or an element
    ! that is coarser. This element is best removed before further processing (not
    ! implemented). The elements are then refined until the correct level elements are found
    integer, intent(in) :: no_mesh_elems
    type(elem_list), pointer, intent(in) :: mother
    integer, intent(in) :: child_level
    integer, intent(out) :: no_neighbors
    type(elem_ptr), pointer, intent(inout) :: neighbor_ptrs(:) ! assumed allocated in calling subroutine
    
    integer, parameter :: MAX_CON_LEVEL = 3
    integer :: face, next_mother_neighbor
    integer :: con_level, start_of_level, end_of_level
    integer :: no_mother_neighbors, neighbor_index, max_no_neighbors
    type(elem_list), pointer :: elem
    type(elem_ptr), pointer :: mother_neighbor_ptrs(:)
    
    max_no_neighbors = 0
    do con_level = 1, MAX_CON_LEVEL
      max_no_neighbors = max_no_neighbors + 6**con_level
    enddo
    allocate(mother_neighbor_ptrs(max_no_neighbors))
    
    ! get the neighbors of mother, and their neighbors etc, up to certain level
    no_mother_neighbors = 0
    next_mother_neighbor = 1
    
    ! Get the direct neighbors of mother
    elem => mother
    do face = 1, size(elem%faces)
      if (.not. elem%faces(face)%point%ftype == BOUNDARY) then
        if (associated(elem%faces(face)%point%elem_neighbors(1)%point,elem)) then
          mother_neighbor_ptrs(next_mother_neighbor)%point => elem%faces(face)%point%elem_neighbors(2)%point
        else
          mother_neighbor_ptrs(next_mother_neighbor)%point => elem%faces(face)%point%elem_neighbors(1)%point
        endif
        no_mother_neighbors = no_mother_neighbors + 1
        next_mother_neighbor = next_mother_neighbor + 1
      endif
    enddo
    
    ! Avoid the next part if there is only a single element (no neighbors will be found)
    if (no_mesh_elems > 1) then
      ! Get the neighbors at various levels
      start_of_level = 1
      end_of_level = no_mother_neighbors
      do con_level = 2, MAX_CON_LEVEL
        do neighbor_index = start_of_level, end_of_level
          elem =>  mother_neighbor_ptrs(neighbor_index)%point
          do face = 1, size(elem%faces)
            if (.not. elem%faces(face)%point%ftype == BOUNDARY) then
              if (associated(elem%faces(face)%point%elem_neighbors(1)%point,elem)) then
                if (.not. associated(elem%faces(face)%point%elem_neighbors(2)%point,mother)) then
                  mother_neighbor_ptrs(next_mother_neighbor)%point => elem%faces(face)%point%elem_neighbors(2)%point
                  no_mother_neighbors = no_mother_neighbors + 1
                  next_mother_neighbor = next_mother_neighbor + 1
                endif
              else
                if (.not. associated(elem%faces(face)%point%elem_neighbors(1)%point,mother)) then
                  mother_neighbor_ptrs(next_mother_neighbor)%point => elem%faces(face)%point%elem_neighbors(1)%point
                  no_mother_neighbors = no_mother_neighbors + 1
                  next_mother_neighbor = next_mother_neighbor + 1
                endif
              endif
            endif
          enddo
        enddo
        
        ! new bounds of the handled level
        start_of_level = end_of_level + 1
        end_of_level = no_mother_neighbors
      enddo
    endif
    
    ! Now investigate the children of the mother_neighbors:
    ! - if they are active they will not contain the required vertex as their level is too low
    ! - if they are inactive its children (at the correct level) are added to the list
    no_neighbors = 0
    do neighbor_index = 1, no_mother_neighbors
      call get_children_at_childlevel(mother_neighbor_ptrs(neighbor_index)%point,child_level,no_neighbors,neighbor_ptrs)
    enddo
    
    ! Here we nullify the neighbors that are actually the children themselves
    ! Not doing this will lead to a crash when searching for a vertex later
    ! as these are the ones we are actually trying to find ...
    do neighbor_index = 1, no_neighbors
      if (associated(neighbor_ptrs(neighbor_index)%point%mother,mother)) then
        nullify(neighbor_ptrs(neighbor_index)%point)
      endif
    enddo
  end subroutine get_elem_neighbors_at_childlevel
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  recursive subroutine get_children_at_childlevel(elem_head,child_level,no_neighbors,neighbor_ptrs)
    type(elem_list), pointer, intent(in) :: elem_head
    integer, intent(in) :: child_level
    integer, intent(inout) :: no_neighbors
    type(elem_ptr), pointer, intent(inout) :: neighbor_ptrs(:)
    
    integer :: child, no_children
    
    if (elem_head%level <= child_level) then
      if (elem_head%level == child_level) then
        ! put in neighbor list
        neighbor_ptrs(no_neighbors+1)%point => elem_head
        no_neighbors = no_neighbors + 1
      else
        ! traverse down whenever still possible
        if (.not. elem_head%active) then
          no_children = size(elem_head%children)
          do child = 1, no_children
            call get_children_at_childlevel(elem_head%children(child)%point,child_level,no_neighbors,neighbor_ptrs)
          enddo
        endif
      endif
    else
      call error('element level > child_level')
    endif
  end subroutine get_children_at_childlevel
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine set_child_id(mother_id,child_index,child_level,child_id)
    type(elem_id), intent(in) :: mother_id
    integer, intent(in) :: child_index, child_level
    type(elem_id), intent(out) :: child_id
    
    call assert(child_level <= 20,'maximum level reached')
    
    child_id = mother_id
    child_id%path(child_level:child_level) = char(child_index + 48)
  end subroutine set_child_id
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  integer function get_center_tet_in_tet_face(face) result(center_tet_in_tet_face)
    integer, intent(in) :: face
    
    select case(face)
      case(1)
        center_tet_in_tet_face = 6
      case(2)
        center_tet_in_tet_face = 7
      case(3)
        center_tet_in_tet_face = 8
      case(4)
        center_tet_in_tet_face = 5
      case default
        call error('Wrong face number')
    end select
  end function get_center_tet_in_tet_face
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  integer function get_no_interior_edge_neighbors(eltype) result(no_nghbs_of_an_interior_edge)
    ! Returns the number of neighbors connecting to an interior edge created on refinement
    integer, intent(in) :: eltype
    
    select case(eltype)
      case(34,310)
        no_nghbs_of_an_interior_edge = 4
      case(38,320,327)
        no_nghbs_of_an_interior_edge = 4
      case default
        call error('Unsupported eltype')
    end select
  end function get_no_interior_edge_neighbors
end module mesh_refinement
