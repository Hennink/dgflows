module functionals
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use petsc_mod, only: i_am_master_process, MASTER_ID
  use mpi_wrappers_mod, only: allreduce_wrap, reduce_wrap
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only: vec_func_type
  use mesh_objects, only: elem_id, elem_list, elem_ptr
  use mesh_partitioning, only: my_first_last_elems
  use mesh_type, only: fem_mesh
  use, intrinsic :: ieee_arithmetic, only: ieee_value, IEEE_SIGNALING_NAN
  implicit none
  
  private
  public :: my_integral
  public :: continuous_rel_l2_diff
  public :: divergence_error
  
  interface continuous_rel_l2_diff
    module procedure continuous_rel_l2_diff__scal
    module procedure continuous_rel_l2_diff__vec
  end interface continuous_rel_l2_diff
  
contains
  real(dp) function my_integral(mesh,only_fluid,integrand,time) result(my_int)
    type(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid
    class(scal_func_type),  intent(in) :: integrand
    integer, intent(in) :: time

    type(elem_list), pointer :: elem
    type(elem_id) :: id
    type(elem_id), pointer, dimension(:) :: active_elem_list
    integer :: elem_no
    integer :: first_elem, last_elem
    real(dp), allocatable :: vals_at_qps(:)

    call mesh%get_active_elem_list_ptr(only_fluid,active_elem_list)
    call my_first_last_elems(only_fluid,first_elem,last_elem)
    
    my_int = 0.0_dp
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      allocate(vals_at_qps(elem%nqp))
      call integrand%get_values(elem,elem%quad_points,time,vals_at_qps)
      my_int = my_int + dot_product(vals_at_qps,elem%quot)
      deallocate(vals_at_qps)
    enddo
  end function

  real(dp) function continuous_rel_l2_diff__scal(mesh,only_fluid,new,time_new,old,time_old,bcast)
    use vec_func_scal_compon_mod, only: as_vecfunc
    
    type(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid
    class(scal_func_type), intent(in) :: new
    integer, intent(in) :: time_new
    class(scal_func_type), intent(in) :: old
    integer, intent(in) :: time_old
    logical, intent(in) :: bcast
    
    continuous_rel_l2_diff__scal = continuous_rel_l2_diff__vec( &
        mesh, only_fluid,                                       &
        as_vecfunc(new), time_new,                              &
        as_vecfunc(old), time_old,                              &
        bcast                                                   &
    )
  end function continuous_rel_l2_diff__scal
  
  real(dp) function continuous_rel_l2_diff__vec(mesh,only_fluid,new,time_new,old,time_old,bcast)
    ! Returns  |new-old|_2 / |new|_2,  where |.|_2 is the continuous L2-norm,
    ! based on the inner product <v,w> := \int (v_i w_i) .
    ! 
    ! If `bcast`, then this returns a value on all processes.
    use exceptions, only: warning
    use kahan_sum_mod, only: kahan_sum_t

    type(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid
    class(vec_func_type), intent(in) :: new
    integer, intent(in) :: time_new
    class(vec_func_type), intent(in) :: old
    integer, intent(in) :: time_old
    logical, intent(in) :: bcast
    
    integer :: dir, no_dirs, p
    type(elem_list), pointer :: elem
    type(elem_id) :: id
    type(elem_id), pointer, dimension(:) :: active_elem_list
    integer :: elem_no
    integer :: first_elem, last_elem
    real(dp), allocatable :: old_vals(:,:), new_vals(:,:)
    type(kahan_sum_t) :: my_num, my_denom
    real(dp) :: my_num_denom(2), num_denom(2), num, denom
    real(kind(num_denom)), parameter :: SMALL = sqrt(epsilon(num_denom))
    
    no_dirs = assert_eq(new%no_dirs(),old%no_dirs())
    
    call mesh%get_active_elem_list_ptr(only_fluid,active_elem_list)
    call my_first_last_elems(only_fluid,first_elem,last_elem)
    
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      allocate(new_vals(no_dirs,elem%nqp))
      allocate(old_vals(no_dirs,elem%nqp))
      call new%values_at_qps(elem,time_new,new_vals)
      call old%values_at_qps(elem,time_old,old_vals)
      do p = 1, elem%nqp
        do dir = 1, no_dirs
          call my_num%add  (elem%quot(p) * (new_vals(dir,p)-old_vals(dir,p))**2)
          call my_denom%add(elem%quot(p) * new_vals(dir,p)**2)
        enddo
      enddo
      deallocate(new_vals, old_vals)
    enddo
    
    my_num_denom = [my_num%total(), my_denom%total()]
    if (bcast) then
      call allreduce_wrap(my_num_denom,'sum',num_denom)
    else
      call reduce_wrap(my_num_denom,'sum',MASTER_ID,num_denom)
    endif
    if (bcast .or. I_am_master_process()) then
      if (all(num_denom < SMALL) .and. i_am_master_process()) call warning("relative L2 diff: both num. and denom. are small.")
      num = num_denom(1)
      denom = max(SMALL,num_denom(2))
      continuous_rel_l2_diff__vec = real(sqrt(num / denom),dp)
    else
      continuous_rel_l2_diff__vec = ieee_value(1.0_dp,IEEE_SIGNALING_NAN)
    endif
  end function continuous_rel_l2_diff__vec
  
  real(dp) function divergence_error(mesh)
    ! Defined as (see [1], p.685)
    !     E_d = ( L \int |div(u)|) / (\int ||u||),
    ! where L is a characteristic length.
    ! 
    ! Returns NaN on non-master processes.
    ! 
    ! [1] Fehn N. et al., "Robust and efficient dG methods for under-resolved turbulent incompressible flows",
    !     Journal of Comp. Phys. 372 (2018), pp. 667-693
    use numerical_solutions, only: velocity
    use run_data, only: charac_length, mesh_dimen
    
    type(fem_mesh), intent(in) :: mesh
    
    type(elem_list), pointer :: elem
    type(elem_id) :: id
    type(elem_id), pointer, dimension(:) :: active_elem_list
    integer :: elem_no
    integer :: first_elem, last_elem
    integer :: dimen
    real(dp) :: my_num_denom(2), num_denom(2)
    real(dp), allocatable :: u_vals(:,:)
    integer, parameter :: TIME = 2
    real(dp), parameter :: SMALL = epsilon(1.0_dp)
    
    dimen = assert_eq(mesh_dimen, velocity%no_dirs(), BOUNDS_CHECK)

    call mesh%get_active_elem_list_ptr(.true.,active_elem_list)
    call my_first_last_elems(.true.,first_elem,last_elem)
    
    my_num_denom = 0.0_dp
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      allocate(u_vals(dimen,elem%nqp))
      call velocity%values_at_qps(elem,TIME,u_vals)
      my_num_denom(1) = my_num_denom(1) + dot_product(abs(velocity%div_at_qps(elem,TIME)),elem%quot)
      my_num_denom(2) = my_num_denom(2) + dot_product(norm2(u_vals,1),elem%quot)
      deallocate(u_vals)
    enddo
    
    call reduce_wrap(my_num_denom,'sum',MASTER_ID,num_denom)
    if (.not. I_am_master_process()) then
      divergence_error = ieee_value(1.0_dp,IEEE_SIGNALING_NAN)
    elseif (num_denom(1) > SMALL .and. num_denom(2) > SMALL) then
      divergence_error = charac_length * num_denom(1) / num_denom(2)
    else
      divergence_error = 0.0_dp
    endif
  end function divergence_error
end module functionals

