module gsip
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use pde_term_mod, only: pde_term_t
  use scal_func_mod, only: scal_func_type
  implicit none
  
  private
  public :: gsip_t
  public :: process_internal_face
  
  type, extends(pde_term_t), abstract :: gsip_t
    ! Generalized Symmetric Interior Penalty,
    ! discretizes a term of the form
    !     - \nabla \cdot (D tau(u)) ,
    ! where
    !   * 'u' is the unknown;
    !   * 'D' is the diffusion parameter;
    !   * 'tau(.)' is the stress tensor (without diffusion coefficient!), e.g.:
    !     - Fick's law corresponds to
    !           tau(v) = grad(v) ;
    !     - Newton viscous stress in the NSE (if 'u' is velocity):
    !           tau(v) = grad(v) + grad(v)^T - (2/3) trace(grad(v)) I .
    class(scal_func_type), allocatable :: diff_param
    logical :: add_mass_flux_term = .false.
    logical :: explicit
    integer :: time_diff_param = 1
    integer :: time_density = 1
    real(dp) :: penalty_mult_factor = 1.0_dp
    
  contains
    procedure :: process_elem
    procedure, non_overridable :: process_face
    procedure, non_overridable :: ngbr_coupling_depth
    procedure, non_overridable :: process_bnd_face
    procedure :: process_internal_face

    procedure(stress_tensor_at_pnt_set_i), deferred :: stress_tensor_at_pnt_set
    procedure(stress_tensor_at_pnt_set_i), deferred :: nonFick_part_of_stress
    procedure(get_bc_in_gsip_context_i), deferred, nopass :: get_bc_in_gsip_context
    procedure, non_overridable :: density_adjusted_grad, n_dot_density_adjusted_grad
  end type gsip_t
  
  abstract interface
    subroutine stress_tensor_at_pnt_set_i(this,elem,pnt_set,handler,stress)
      use dof_handler, only: dof_handler_type
      use f90_kind, only: dp
      use mesh_objects, only: elem_list
      use pnt_set_mod, only: pnt_set_t
      use lincom_rank2_mod, only: lincom_rank2_t
      import :: gsip_t
      implicit none
      
      class(gsip_t), intent(in) :: this
      type(elem_list), intent(in) :: elem
      type(pnt_set_t), intent(in) :: pnt_set
      type(dof_handler_type), intent(in) :: handler
      type(lincom_rank2_t), intent(out) :: stress
    end subroutine
    
    subroutine get_bc_in_gsip_context_i(bc_id,qnty,bc)
      use boundary_condition_types, only: bc_type
      use solvec_mod, only: solvec_t
      implicit none
      
      integer, intent(in) :: bc_id
      type(solvec_t), intent(in) :: qnty
      class(bc_type), allocatable, intent(out) :: bc
    end subroutine get_bc_in_gsip_context_i
  end interface
  
contains
  pure integer function ngbr_coupling_depth(this)
    class(gsip_t), intent(in) :: this

    ngbr_coupling_depth = merge(-1, 1, this%explicit)
  end function

  subroutine process_elem(this,elem,qnty,local_linsys)
    use array_opers_mod, only: gemm_classic, add_weighted_tensprod
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: elem_list
    use run_data, only: mesh_dimen
    use solvec_mod, only: solvec_t
    
    class(gsip_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys
    
    logical, parameter :: use_blas = .true.
    real(dp) :: quot_x_D(elem%nqp)
    integer :: elem_no, nnod, p, dir
    real(dp), allocatable :: tdiag(:,:), adj_gders(:,:,:)
    
    elem_no = elem%get_active_elem_no(qnty%handler%only_fluid_mesh)
    call assert(qnty%handler%order(elem_no) >= 1,"(G)SIP is only for higher order functions.")
    
    ! volumetric diffusion: \int D tau(u_i) \colon grad(v_i)
    quot_x_D = elem%quot * this%diff_param%values_at_elem_qps(elem,this%time_diff_param)
    nnod = qnty%handler%no_nod_elem(elem_no)
    call this%density_adjusted_grad(elem,elem%quad_points,qnty%handler,adj_gders)

    allocate(tdiag(nnod,nnod))
    if (use_blas) then
      do p = 1, elem%nqp
        adj_gders(:,:,p) = adj_gders(:,:,p) * quot_x_D(p)
      enddo
      associate(B => elem%quad_points%dg_transp_gders)
        call gemm_classic(.false., .true., nnod, nnod, mesh_dimen * elem%nqp, 1.0_dp, &
              adj_gders, size(adj_gders,1), B, size(B,1), 0.0_dp, tdiag, size(tdiag,1))
      end associate

    else
      tdiag = 0
      associate(gders => elem%quad_points%dg_transp_gders(:nnod,:,:))
        do p = 1, elem%nqp
          do dir = 1, mesh_dimen
            call add_weighted_tensprod(tdiag, adj_gders(:,dir,p), gders(:,dir,p), quot_x_D(p))
          enddo
        enddo
      end associate
    endif

    call local_linsys%add_transposed_elemmat_all_dirs(elem_no, tdiag)
  end subroutine process_elem
  
  subroutine process_face(this,face,Ein_is_E1,qnty,local_linsys)
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: face_list
    use solvec_mod, only: solvec_t

    class(gsip_t), intent(in) :: this
    type(face_list), intent(in) :: face
    logical, intent(in) :: Ein_is_E1
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys

    logical :: is_bnd
    
    is_bnd = face%is_boundary(qnty%handler%only_fluid_mesh)
    if (is_bnd) then
      call this%process_bnd_face(face,qnty,local_linsys)
    else
      call this%process_internal_face(face,Ein_is_E1,qnty,local_linsys)
    endif
  end subroutine
  
  subroutine process_bnd_face(this,face,qnty,local_linsys)
    use boundary_condition_types
    use exceptions, only: error
    use func_operations_mod, only: operator(/)
    use lincom_rank1_mod, only: lincom_for_n_dot_rank1, &
                                lincom_rank1_t
    use lincom_rank2_mod, only: lincom_rank2_t, operator(.dot.)
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: elem_list, face_list, side_of_internal_nghb_of_bnd_face
    use penalty_param_mod, only: gsip_penalty_parameter
    use solvec_mod, only: solvec_t
    use vec_func_mod, only: vec_func_type
    
    class(gsip_t), intent(in) :: this
    type(face_list), intent(in) :: face
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys
    
    integer :: side_Ein
    type(elem_list), pointer :: Ein
    class(bc_type), allocatable :: bc
    real(dp) :: slip_coeff_x_quot(face%nqp)
    real(dp) :: inhom_at_qps(qnty%handler%no_dirs,face%nqp), robin_coeff_x_quot(face%nqp)
    real(dp) :: quot_x_penalty(face%nqp), n_sgn
    type(lincom_rank1_t) :: func
    type(lincom_rank1_t) :: n_dot_stress, n_dot_nonFick_stress
    type(lincom_rank1_t) :: n_dot_func
    type(lincom_rank1_t) :: n_dot_stress_dot_n, func_min_norm_comp
    real(dp) :: quot_x_D_Ein(face%nqp)
    type(lincom_rank2_t) :: stress, nonFick_stress
    
    call assert(face%is_boundary(qnty%handler%only_fluid_mesh))
    side_Ein = side_of_internal_nghb_of_bnd_face(face,qnty%handler%only_fluid_mesh)
    Ein => face%elem_neighbors(side_Ein)%point
    n_sgn = merge(1.0_dp,-1.0_dp,side_Ein == 1)
    
    call func%set_as_func(face,side_Ein,qnty%handler)
    
    call this%stress_tensor_at_pnt_set(Ein,face%quad_points(side_Ein),qnty%handler,stress)
    n_dot_stress = face%normal .dot. stress

    quot_x_D_Ein = face%quot * this%diff_param%values_at_qps(face,side_Ein,this%time_diff_param)
    
    call this%get_bc_in_gsip_context(face%bc_id,qnty,bc)
    select type(bc)
      type is (dirichlet_bc_type)
        call bc%inhom_part%values_at_qps(face,side_Ein,1,inhom_at_qps)
        
        ! Dirichlet consistency: lhs:  - \int_F  [v_i] n \cdot {D tau(u_i)}
        !                        rhs:  - \int_F  n \cdot D tau(v_i) (g_D)_i
        call local_linsys%add_int_dotprod_to_lhs(func,n_dot_stress,quot_x_D_Ein,-n_sgn,this%explicit)
        call local_linsys%add_int_dotprod_to_rhs(n_dot_stress,inhom_at_qps,quot_x_D_Ein,-n_sgn)
        
        ! symmetry: - \int_F  n \cdot {D tau(v_i)} [u_i]
        call local_linsys%add_int_dotprod_to_lhs(n_dot_stress,func,quot_x_D_Ein,-n_sgn,this%explicit)
        
        ! penalty: lhs:  \int_F  "penalty" [v_i] [u_i]
        !          rhs:  \int_F  "penalty" v_i (g_D)_i
        quot_x_penalty = face%quot * gsip_penalty_parameter(face,qnty%handler,this%diff_param,this%time_diff_param)
        call local_linsys%add_int_dotprod_to_lhs(func,func,quot_x_penalty,this%penalty_mult_factor,this%explicit)
        call local_linsys%add_int_dotprod_to_rhs(func,inhom_at_qps,quot_x_penalty,this%penalty_mult_factor)
        
      class is (neumann_bc_type)
        call error('NOT IMPLEMENTED')
      
      type is (generalized_neumann_bc_t)
        ! \int_F  v_i F_i
        call bc%inhom_part%values_at_qps(face,side_Ein,1,inhom_at_qps)
        call local_linsys%add_int_dotprod_to_rhs(func,inhom_at_qps,face%quot)
      
      type is (adjusted_traction_bc_t)
        ! - \int_F  n \cdot D nonFickStress(u_i) v_i
        ! nonFickStress(w) := tau(w) - FickStress(w)
        call this%nonFick_part_of_stress(Ein,face%quad_points(side_Ein),qnty%handler,nonFick_stress)
        n_dot_nonFick_stress = face%normal .dot. nonFick_stress
        call local_linsys%add_int_dotprod_to_lhs(func,n_dot_nonFick_stress,quot_x_D_Ein,-n_sgn,this%explicit)
        ! \int_F  v_i f_i
        call bc%inhom_part%values_at_qps(face,side_Ein,1,inhom_at_qps)
        call local_linsys%add_int_dotprod_to_rhs(func,inhom_at_qps,face%quot)
        
      type is (generalized_robin_bc_t)
        ! BC of the form
        !     "robin_coeff" u  +  n \cdot D tau(u)  =  g_R
        ! \int_F  "robin_coeff" u_i v_i
        robin_coeff_x_quot = bc%coeff%values_at_qps(face,side_Ein,1) * face%quot
        call local_linsys%add_int_dotprod_to_lhs(func,func,robin_coeff_x_quot,1.0_dp,this%explicit)
        ! \int_F  v_i (g_R)_i
        call bc%inhom_part%values_at_qps(face,side_Ein,1,inhom_at_qps)
        call local_linsys%add_int_dotprod_to_rhs(func,inhom_at_qps,face%quot)
        
      type is (slip_bc_type)
        ! penalty: \int_F  "penalty" (u \cdot n) (v \cdot n)
        quot_x_penalty = face%quot * gsip_penalty_parameter(face,qnty%handler,this%diff_param,this%time_diff_param)
        quot_x_penalty = this%penalty_mult_factor * quot_x_penalty
        n_dot_func = lincom_for_n_dot_rank1(face,side_Ein,func)
        call local_linsys%add_int_dotprod_to_lhs(n_dot_func,n_dot_func,quot_x_penalty,n_sgn**2,this%explicit)
        ! consistency: - \int_F  (n \cdot D tau(u) \cdot n) (v \cdot n)
        n_dot_stress_dot_n = lincom_for_n_dot_rank1(face,side_Ein,n_dot_stress)
        call local_linsys%add_int_dotprod_to_lhs(n_dot_func,n_dot_stress_dot_n,quot_x_D_Ein,-n_sgn**3,this%explicit)
        ! symmetry: - \int_F  (u \cdot n) (n \cdot D tau(v) \cdot n)
        call local_linsys%add_int_dotprod_to_lhs(n_dot_stress_dot_n,n_dot_func,quot_x_D_Ein,-n_sgn**3,this%explicit)
        ! slip: \int_F  "slip_coeff" MinNormal(u) \cdot MinNormal(v)
        ! where MinNormal(w) = w - (w \cdot n) n
        call func_min_norm_comp%set_as_func_minus_normal_component(face,side_Ein,func)
        slip_coeff_x_quot = bc%slip_coeff%values_at_qps(face,side_Ein,1) * face%quot
        call local_linsys%add_int_dotprod_to_lhs(func_min_norm_comp,func_min_norm_comp,slip_coeff_x_quot,1.0_dp,this%explicit)
        
      class default
        error stop 'gsip%process_bnd_face: unknown bc.'
    end select
  end subroutine process_bnd_face
  
  subroutine process_internal_face(this,face,Ein_is_E1,qnty,local_linsys)
    use code_const, only: INTERFACE_SOLID_FLUID
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: face_list
    use penalty_param_mod, only: gsip_penalty_parameter
    use solvec_mod, only: solvec_t
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t
    
    class(gsip_t), intent(in) :: this
    type(face_list), intent(in) :: face
    logical, intent(in) :: Ein_is_E1
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys
    
    integer :: side_Ein, side_Eout
    type(unidir_lincom_rank0_t) :: func_Ein, func_Eout, &
                                   n_dot_gder_Ein, n_dot_gder_Eout
    real(dp) :: n_sgn
    real(dp) :: quot_x_penalty(face%nqp)
    real(dp) :: quot_x_D_Ein(face%nqp), quot_x_D_Eout(face%nqp)
    
    call assert(.not. face%is_boundary(qnty%handler%only_fluid_mesh))

    if (Ein_is_E1) then
      side_Ein = 1; side_Eout = 2
      n_sgn = 1.0_dp
    else
      side_Ein = 2; side_Eout = 1
      n_sgn = -1.0_dp
    endif
    
    call func_Ein%set_as_func(face,side_Ein,qnty%handler)
    call func_Eout%set_as_func(face,side_Eout,qnty%handler)
    call this%n_dot_density_adjusted_grad(face,side_Ein,qnty%handler,n_dot_gder_Ein)
    call this%n_dot_density_adjusted_grad(face,side_Eout,qnty%handler,n_dot_gder_Eout)
    quot_x_D_Ein = face%quot * this%diff_param%values_at_qps(face,side_Ein,this%time_diff_param)
    quot_x_D_Eout = face%quot * this%diff_param%values_at_qps(face,side_Eout,this%time_diff_param)
    
    ! consistency: - \int_F  [v_i] n \cdot {D grad(u_i)}
    call local_linsys%add_inprod_to_lhs_all_dirs(func_Ein,n_dot_gder_Ein,quot_x_D_Ein,this%explicit,-0.5_dp*n_sgn)
    call local_linsys%add_inprod_to_lhs_all_dirs(func_Ein,n_dot_gder_Eout,quot_x_D_Eout,this%explicit,-0.5_dp*n_sgn)
    
    if (face%ftype == INTERFACE_SOLID_FLUID) then
      call assert(any(qnty%name == ['vol_enthalpy ','spec_enthalpy']))
    else
      ! symmetry: - \int_F  n \cdot {D grad(v_i)} [u_i]
      call local_linsys%add_inprod_to_lhs_all_dirs(n_dot_gder_Ein,func_Ein,quot_x_D_Ein,this%explicit,-0.5_dp*n_sgn)
      call local_linsys%add_inprod_to_lhs_all_dirs(n_dot_gder_Ein,func_Eout,quot_x_D_Ein,this%explicit,0.5_dp*n_sgn)
      
      ! penalty: \int_F  "penalty" [v_i] [u_i]
      quot_x_penalty = face%quot * gsip_penalty_parameter(face,qnty%handler,this%diff_param,this%time_diff_param)
      call local_linsys%add_inprod_to_lhs_all_dirs(func_Ein,func_Ein,quot_x_penalty,this%explicit,this%penalty_mult_factor)
      call local_linsys%add_inprod_to_lhs_all_dirs(func_Ein,func_Eout,quot_x_penalty,this%explicit,-this%penalty_mult_factor)
    endif
  end subroutine process_internal_face
  
  
  subroutine density_adjusted_grad(this,elem,pnt_set,handler,adj_grad)
    ! Computes
    !     adj_grad_{mip} := d(phi_m)/d(r_i) - phi_m d_i,
    ! where, at point p,
    !     phi_m   is the m'th basis function;
    !     r       is the position vector;
    !     d_i := (1/rho) * d(rho)/d(r_i) .
    use array_opers_mod, only: subtract_tensprod
    use dof_handler, only: dof_handler_type
    use mesh_objects, only: elem_list
    use numerical_solutions, only: matprop_set
    use pnt_set_mod, only: pnt_set_t
    
    class(gsip_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    type(dof_handler_type), intent(in) :: handler
    real(dp), allocatable, intent(out) :: adj_grad(:,:,:)
    
    integer :: elem_no, nnod
    class(scal_func_type), pointer :: rho_func
    real(dp), allocatable :: rel_grad_rho(:,:)
    integer :: p, np
    
    np = pnt_set%np()
    elem_no = elem%get_active_elem_no(handler%only_fluid_mesh)
    nnod = handler%no_nod_elem(elem_no)
    allocate(rel_grad_rho(elem%dimen(),np))
    
    allocate(adj_grad,source = pnt_set%dg_transp_gders(:nnod,:,:))
    if (this%add_mass_flux_term) then
      rho_func => matprop_set%func_ptr('density')
      call rho_func%rel_grad(elem,pnt_set,this%time_density,rel_grad_rho)
      do p = 1, np
        call subtract_tensprod(adj_grad(:,:,p),pnt_set%dg_funcs(:nnod,p),rel_grad_rho(:,p))
      enddo
    endif
  end subroutine density_adjusted_grad
  
  subroutine n_dot_density_adjusted_grad(this,face,side,handler,n_dot_adj_grad)
    use dof_handler, only: dof_handler_type
    use mesh_objects, only: elem_list, face_list
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t
    
    class(gsip_t), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side
    type(dof_handler_type), intent(in) :: handler
    type(unidir_lincom_rank0_t), intent(out) :: n_dot_adj_grad
    
    type(elem_list), pointer :: elem
    integer :: elem_no, nnod
    real(dp), allocatable :: adj_grad(:,:,:)
    integer :: p
    
    call assert(side == 1 .or. side == 2)
    
    elem => face%elem_neighbors(side)%point
    elem_no = elem%get_active_elem_no(handler%only_fluid_mesh)
    nnod = handler%no_nod_elem(elem_no)
    call this%density_adjusted_grad(elem,face%quad_points(side),handler,adj_grad)
    n_dot_adj_grad%elem = elem_no
    allocate(n_dot_adj_grad%stencil(nnod,face%nqp))
    do p = 1, face%nqp
      n_dot_adj_grad%stencil(:,p) = matmul(adj_grad(:,:,p),face%normal(:,p))
    enddo
  end subroutine n_dot_density_adjusted_grad
end module gsip
