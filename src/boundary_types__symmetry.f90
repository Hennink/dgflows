submodule(boundary_types) boundary_types__symmetry
  implicit none
  
  type, extends(wall_bnd_t) :: symmetry_type
  contains
    procedure :: read_specs_from_file
    procedure :: get_bc
  end type symmetry_type
  
contains
  module procedure alloc_symmetry
    allocate(symmetry_type :: bnd)
  end procedure alloc_symmetry
  
  subroutine read_specs_from_file(this,unit_number)
    
    class(symmetry_type), intent(out) :: this
    integer, intent(in) :: unit_number
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types, only: bc_type, hom_neumann_bc_type, slip_bc_type
    use functions_lib, only: zero_scalfunc
    
    class(symmetry_type), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    
    type(slip_bc_type), allocatable :: slip_bc
    
    select case(qnty_name)
      case('mass_flux','velocity')
        allocate(slip_bc)
        allocate(slip_bc%slip_coeff, source=zero_scalfunc())
        call move_alloc(slip_bc,bc)
      case('vol_enthalpy','density','wall_dist','vol_turb1','vol_turb2')
        allocate(bc,source=hom_neumann_bc_type())
      case default
        write(*,'(a)') 'ERROR: no bc for '//qnty_name//' in symmetry'
        error stop
    end select
  end subroutine get_bc
end submodule boundary_types__symmetry

