module scalfunc_gcors_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use mesh_objects, only: elem_list
  use pnt_set_mod, only: pnt_set_t
  use scal_func_mod, only: scal_func_type
  implicit none
  
  private
  public :: scalfunc_gcors_t
  
  type, abstract, extends(scal_func_type) :: scalfunc_gcors_t
  contains
    procedure, non_overridable :: get_values, get_gders
    procedure(value_at_gcor_i), deferred :: value_at_gcor
    procedure :: gder_at_gcor => fd_approx_gder
  end type scalfunc_gcors_t
  
  abstract interface
    subroutine value_at_gcor_i(this,gcor,time,value)
      import :: scalfunc_gcors_t, dp
      implicit none
      
      class(scalfunc_gcors_t), intent(in) :: this
      real(dp), intent(in) :: gcor(:)
      integer, intent(in) :: time
      real(dp), intent(out) :: value
    end subroutine
  end interface
  
contains
  subroutine get_values(this,elem,pnt_set,time,values)
    class(scalfunc_gcors_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    integer :: p, np
    
    np = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)

    do p = 1, np
      call this%value_at_gcor(pnt_set%gcors(:,p),time,values(p))
    enddo
  end subroutine get_values
  
  subroutine get_gders(this,elem,pnt_set,time,gders)
    class(scalfunc_gcors_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:)
    
    integer :: p, np
    
    call assert(size(gders,1) == elem%dimen(), BOUNDS_CHECK)
    np = assert_eq(size(gders,2),pnt_set%np(),BOUNDS_CHECK)

    do p = 1, np
      call this%gder_at_gcor(pnt_set%gcors(:,p),time,gders(:,p))
    enddo
  end subroutine get_gders

  subroutine fd_approx_gder(this,gcor,time,gder)
    use math_const, only: SMALL

    class(scalfunc_gcors_t), intent(in) :: this
    real(dp), intent(in) :: gcor(:)
    integer, intent(in) :: time
    real(dp), intent(out) :: gder(:)

    integer :: d, dimen
    real(dp) :: h
    real(dp), allocatable :: x_lo(:), x_hi(:)
    real(dp) :: val_lo, val_hi

    dimen = assert_eq(size(gcor),size(gder), BOUNDS_CHECK)

    allocate(x_lo(dimen), x_hi(dimen))
    do d = 1, size(gcor)
      h = max(SMALL,SMALL * abs(gcor(d)))
      x_lo = gcor
      x_hi = gcor
      x_lo(d) = x_lo(d) - h
      x_hi(d) = x_hi(d) + h
      call this%value_at_gcor(x_lo,time,val_lo)
      call this%value_at_gcor(x_hi,time,val_hi)
      gder(d) = (val_hi - val_lo) / (2 * h)
    enddo
  end subroutine
end module scalfunc_gcors_mod
