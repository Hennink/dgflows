module scalfunc_from_datafile_mod
  use assertions
  use f90_kind, only: dp
  use scalfunc_gcors_mod, only: scalfunc_gcors_t
  use spline_mod, only: cubic_spline_t
  implicit none

  private
  public :: init_scalfunc_from_datafile
  public :: datafile_func_t

  logical :: initialized = .false.
  integer, parameter :: DIR = 2
  type(cubic_spline_t) :: spline

  type, extends(scalfunc_gcors_t) :: datafile_func_t
  contains
    procedure, non_overridable :: value_at_gcor
    ! `gder_at_gcor` would be easy to implement, but not needed now
  end type

contains
  subroutine init_scalfunc_from_datafile()
    ! File format:
    ! 
    !   1000 ! # points
    !   0.0   1.234e-3  ! first coord. and value
    !   1.3   5.6       ! second coord. and value
    !   ...
    ! 
    use io_basics
    use mpi_wrappers_mod
    use petsc_mod, only: MASTER_ID, I_am_master_process
    use run_data, only: datafile_for_scalfunc
    
    integer :: unit
    integer :: p, np
    real(dp), allocatable :: x(:), y(:)
    real(dp) :: deriv_start, deriv_end
    character(:), allocatable :: line
    logical :: eof

    call assert(.not. initialized, "scalfunc_from_datafile_mod: already initialized")
    call assert(datafile_for_scalfunc /= '', 'cannot initalize when `datafile_for_scalfunc` is not set')

    ! Read the data:
    if (i_am_master_process()) then
      write(*,'(a)') 'Reading data file for scalfunc: file=' // datafile_for_scalfunc
      call open_input_file(datafile_for_scalfunc, unit)
      read(unit,*) np
      allocate(x(np), y(np))
      do p = 1, np
        read(unit,*) x(p), y(p)
      enddo

      ! Assert that the rest of the file is empty:
      do
        call read_line_from_input_file(unit, line, eof)
        if (eof) exit
        call assert(line == '', "More data than I expected in "// datafile_for_scalfunc // ": line=" // line)
      enddo
      close(unit)
    endif
    
    ! Broadcast to non-master processes:
    call bcast_wrap(np, MASTER_ID)
    if (.not. i_am_master_process()) allocate(x(np), y(np))
    call bcast_wrap(x, MASTER_ID)
    call bcast_wrap(y, MASTER_ID)

    ! Build the spline on all processes:
    deriv_start = (y(2) - y(1)) / (x(2) - x(1)) ! second-order estimates would be much better
    deriv_end = (y(np) - y(np-1)) / (x(np) - x(np-1))
    call spline%from_table(x, y, deriv_start, deriv_end)
    spline%saturate_ends = .true. ! for out-of-bounds quad sets

    initialized = .true.
  end subroutine

  subroutine value_at_gcor(this, gcor, time, value)
    class(datafile_func_t), intent(in) :: this
    real(dp), intent(in) :: gcor(:)
    integer, intent(in) :: time
    real(dp), intent(out) :: value

    real(dp) :: walldist

    call assert(initialized, "scalfunc_from_datafile_mod: not initialized... did you set `datafile_for_scalfunc`?", BOUNDS_CHECK)
    
    walldist = 1 - abs(gcor(DIR) - 1) ! hack to scale to Pecnick's averaged values with symm BC at y=1
    value = spline%value(walldist)
  end subroutine
end module
