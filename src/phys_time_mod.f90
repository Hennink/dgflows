module phys_time_mod
  use assertions, only: assert, assert_small
  use exceptions, only: error
  use f90_kind, only: dp
  use io_basics, only: nints_fmt, nreals_fmt
  implicit none
  
  private
  public :: init_phys_times, initialized_phys_times, write_phys_times
  public :: add_step_with_dt
  public :: phys_time, old_times, nold_times
  public :: EX_weights, BDF_weights, ith_BDF_weight
  public :: BDF_weights_are_constant_from_now_on, constant_ith_BDF_weight

  logical, protected :: initialized_phys_times = .false.
  integer, protected :: nold_times
  real(dp), protected :: old_times(10)

contains
  subroutine init_phys_times(filepath)
    use, intrinsic :: ieee_arithmetic, only: ieee_value, IEEE_SIGNALING_NAN

    character(*), intent(in) :: filepath

    integer :: unit
    integer :: iostat
    character(200) :: iomsg
    
    if (filepath =='') then
      nold_times = 1
      old_times(1) = 0.0_dp
      old_times(2:) = ieee_value(1.0_dp,IEEE_SIGNALING_NAN)
    
    else
      open(newunit=unit, file=filepath, &
           status='old', access='sequential', action='read', position='rewind', &
           iostat=iostat, iomsg=iomsg)
      call assert(iostat==0,iomsg)
      read(unit,nints_fmt(1),iostat=iostat,iomsg=iomsg) nold_times
      call assert(iostat==0,iomsg)
      read(unit,nreals_fmt(nstored()),iostat=iostat,iomsg=iomsg) old_times(:nstored())
      call assert(iostat==0,iomsg)
      close(unit,iostat=iostat,iomsg=iomsg)
      call assert(iostat==0,iomsg)
    endif

    initialized_phys_times = .true.
  end subroutine init_phys_times

  subroutine write_phys_times(filepath)
    character(*), intent(in) :: filepath

    integer :: unit
    integer :: iostat
    character(200) :: iomsg
    
    open(newunit=unit, file=filepath, &
         status='replace', access='sequential', action='write', position='rewind', &
         iostat=iostat, iomsg=iomsg)
    call assert(iostat==0,iomsg)
    write(unit,nints_fmt(1),iostat=iostat,iomsg=iomsg) nold_times
    call assert(iostat==0,iomsg)
    write(unit,nreals_fmt(nstored()),iostat=iostat,iomsg=iomsg) old_times(:nstored())
    call assert(iostat==0,iomsg)
    close(unit,iostat=iostat,iomsg=iomsg)
    call assert(iostat==0,iomsg)
  end subroutine write_phys_times

  real(dp) function phys_time(time_idx)
    ! Returns the physical time that is indicated by time_idx. This should be 
    ! the same convention for indicating times as in the solvec and in the 
    ! <*_func%get_values> methods.
    use run_data, only: dt
    use string_manipulations, only: int2char

    integer, intent(in) :: time_idx
    
    if (time_idx == 1) then
      ! time==1 corresponds to the 'new' time in the solvec and the scal/vec funcs...
      phys_time = old_times(1) + dt
    elseif (2 <= time_idx .and. time_idx-1 <= nstored()) then
      phys_time = old_times(time_idx-1)
    else
      call error('phys_time: How do I interpret time_idx=' // int2char(time_idx))
    endif
  end function phys_time

  subroutine add_step_with_dt(dt)
    real(dp), intent(in) :: dt

    call assert(nold_times < huge(nold_times),'nold_times overflow')
    
    nold_times = nold_times + 1
    old_times(2:nstored()) = old_times(:nstored()-1)
    old_times(1) = old_times(2) + dt
  end subroutine add_step_with_dt

  function BDF_weights(order,dt) result(weights)
    ! Returns the finite difference weights for the time derivative at time 1
    ! To compute the weight it is best to use the Lagrange form of the polynomial:
    !
    ! P(x) = SUM_1^n P_j(x) with P_j(x) = y_j II_1^n (x - xk)/(xj - xk)
    ! Differentiation once and evaluation at new time gives the coefficients.
    integer, intent(in) :: order
    real(dp), intent(in) :: dt
    real(dp) :: weights(order+1)
    
    real(dp) :: t(order+1)
    
    call assert(order > 0,'temporal order <= 0')
    call assert(order <= nstored(),'selected temporal order > nstored')
    
    t = [dt + old_times(1), old_times(:order)]
    select case(order)
      case(1)
        weights(2) = 1.0_dp / (t(2) - t(1))
      case(2)
        weights(2) = (t(1) - t(3)) / ((t(2) - t(1))*(t(2) - t(3)))
        weights(3) = (t(1) - t(2)) / ((t(3) - t(1))*(t(3) - t(2)))
      case(3)
        weights(2) = ((t(1) - t(3))*(t(1) - t(4))) / ((t(2) - t(1))*(t(2) - t(3))*(t(2) - t(4)))
        weights(3) = ((t(1) - t(2))*(t(1) - t(4))) / ((t(3) - t(1))*(t(3) - t(2))*(t(3) - t(4)))
        weights(4) = ((t(1) - t(2))*(t(1) - t(3))) / ((t(4) - t(1))*(t(4) - t(2))*(t(4) - t(3)))
      case default
        call error('No time scheme implemented for this order.')
    end select
    weights(1) = - sum(weights(2:))
  end function

  real(dp) function ith_BDF_weight(order,dt,i)
    integer, intent(in) :: order
    real(dp), intent(in) :: dt
    integer, intent(in) :: i
    
    real(dp) :: weights(order+1)
    
    weights = BDF_weights(order,dt)
    ith_BDF_weight = weights(i)
  end function
  
  real(dp) function constant_ith_BDF_weight(order,dt,i)
    integer, intent(in) :: order
    real(dp), intent(in) :: dt
    integer, intent(in) :: i

    real(dp) :: weights(order+1)
    
    call assert(BDF_weights_are_constant_from_now_on())

    weights = constant_BDF_weights_x_dt(order)
    constant_ith_BDF_weight = weights(i) / dt
    
  contains
    function constant_BDF_weights_x_dt(order) result(weights)
      integer, intent(in) :: order
      real(dp) :: weights(order+1)
      
      select case(order)
        case(1);        weights = [1, -1]
        case(2);        weights = [3/2._dp, -2._dp, 1/2._dp]
        case(3);        weights = [11/6._dp, -3._dp, 3/2._dp, -1/3._dp]
        case default;   call error('No constant BDF coeffs. implemented for this order.')
      end select
      call assert_small(sum(weights), "BDF weights should sum to zero.")
    end function
  end function

  logical function BDF_weights_are_constant_from_now_on()
    ! This may return a false negative.
    ! Constant weights allow for certain simplifications.
    ! 
    use math_const, only: SMALL
    use run_data, only: dt

    integer, parameter :: max_nprev = 4 + 1 ! never more than fourth-order time stepping
    real(dp), allocatable :: dts(:)
    
    if (nstored() < max_nprev) then
      BDF_weights_are_constant_from_now_on = .false.
    else
      dts = old_times(:max_nprev-1) - old_times(2:max_nprev)
      BDF_weights_are_constant_from_now_on = all(abs(dts - dt) < SMALL)
    endif
  end function
  
  function EX_weights(order,dt) result(weights)
    ! Extrapolation based on a Taylor expansion
    integer, intent(in) :: order
    real(dp), intent(in) :: dt
    real(dp) :: weights(order)
    
    real(dp) :: t(order+1)
    
    call assert(order > 0,'temporal order <= 0')
    call assert(order <= nstored(),'selected temporal order > nstored')
    
    t = [dt + old_times(1), old_times(:order)]
    select case(order)
      case(1)
      case(2)
        weights(2) = (t(2) - t(1)) / (t(2) - t(3))
      case(3)
        weights(2) = ((t(2) - t(1))*(t(1) - t(4))) / ((t(2) - t(3))*(t(3) - t(4)))
        weights(3) = ((t(1) - t(2))*(t(1) - t(3))) / ((t(2) - t(4))*(t(3) - t(4)))
      case default
        call error('No extrapolation weights implemented for this order')
    end select
    weights(1) = 1.0_dp - sum(weights(2:))
  end function EX_weights

  pure integer function nstored()
    nstored = min(nold_times,size(old_times))
  end function nstored
end module phys_time_mod
