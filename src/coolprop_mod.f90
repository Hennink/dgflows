module coolprop_mod
  ! List of DLL interface definitions: [1]
  ! [1] http://www.coolprop.org/_static/doxygen/html/_cool_prop_lib_8h.html#a4564f1d68573e96636ef87371c0f06a0
  use, intrinsic :: iso_c_binding, only: c_char, c_double, c_int, c_null_char
  use assertions, only: assert, assert_normal
  use exceptions, only: error, warning
  use f90_kind, only: dp
  implicit none
  
  private
  public :: set_reference_state
  public :: cp_value, cp_deriv
  
  interface
    function PropsSI(output, name1, prop1, name2, prop2, fluidname) bind(C,name='PropsSI')
      use, intrinsic :: iso_c_binding, only: c_char, c_double
      implicit none
      real(c_double) :: PropsSI
      character(kind=c_char), intent(in) :: output
      character(kind=c_char), intent(in) :: name1
      real(c_double), value :: prop1
      character(kind=c_char), intent(in) :: name2
      real(c_double), value :: prop2
      character(kind=c_char), intent(in) :: fluidname
    end function PropsSI

    integer(c_int) function set_reference_stateD_i(ref,T,rhomolar,hmolar0,smolar0) bind(C,name='set_reference_stateD')
      use, intrinsic :: iso_c_binding, only: c_char, c_double, c_int
      implicit none
      character(kind=c_char), intent(in) :: ref(*)
      real(c_double), value :: T, rhomolar, hmolar0, smolar0
    end function set_reference_stateD_i
  end interface

contains
  subroutine set_reference_state(ref,T,rhomolar,hmolar0,smolar0)
    ! Set the reference state based on a thermodynamic state point specified by temperature and molar density.
    ! Parameters
    !     FluidName:    The name of the fluid
    !     T:            Temperature at reference state [K]
    !     rhomolar:     Molar density at reference state [mol/m^3]
    !     hmolar0:      Molar enthalpy at reference state [J/mol]
    !     smolar0:      Molar entropy at reference state [J/mol/K]
    ! 
    ! Code to operate on an AbstractState instance: [1].
    ! High-level code: [2].
    ! Help with interface on GitHub (closed, resolved): [3]
    ! [1]  https://github.com/CoolProp/CoolProp/blob/master/src/Backends/Helmholtz/HelmholtzEOSMixtureBackend.cpp#L3922-L3941
    ! [2]  https://github.com/CoolProp/CoolProp/blob/master/src/CoolProp.cpp#L942-L955
    ! [3]  https://github.com/CoolProp/CoolProp/issues/1776
    character(*), intent(in) :: ref
    real(dp), intent(in) :: T, rhomolar, hmolar0, smolar0

    logical :: first_call = .true.
    character(:,kind=c_char), allocatable :: c_ref
    integer(c_int) :: export_code

    call assert(first_call,'do not set the CP reference state more than once')
    first_call = .false.

    c_ref = dgflow2cp_name(ref) // c_null_char
    export_code = set_reference_stateD_i(                &
        c_ref,real(T,c_double),real(rhomolar,c_double),  & 
        real(hmolar0,c_double),real(smolar0,c_double)    &
    )
    call assert(export_code == 1_c_int,'export_code == 1 indicates success')
  end subroutine set_reference_state

  recursive function cp_value(qnty,name1,prop1,name2,prop2,fluidname) result(value)
    character(*), intent(in) :: qnty
    character(*), intent(in) :: name1, name2
    real(dp), intent(in) :: prop1, prop2
    character(*), intent(in) :: fluidname
    real(dp) :: value
    
    real(dp) :: mu, rho, c_p, k

    select case(qnty)
      case('kinematic_viscosity')
        mu  = cp_value('dynamic_viscosity',name1,prop1,name2,prop2,fluidname)
        rho = cp_value('density',name1,prop1,name2,prop2,fluidname)
        value = mu / rho

      case('thermal_diffusivity')
        k = cp_value('thermal_conductivity',name1,prop1,name2,prop2,fluidname)
        rho = cp_value('density',name1,prop1,name2,prop2,fluidname)
        c_p = cp_value('spec_heat_capacity',name1,prop1,name2,prop2,fluidname)
        value = k / (rho * c_p)

      case('k_over_cp')
        k = cp_value('thermal_conductivity',name1,prop1,name2,prop2,fluidname)
        c_p = cp_value('spec_heat_capacity',name1,prop1,name2,prop2,fluidname)
        value = k / c_p

      case default
        value = PropsSI_wrapper(cp_qnty_str(qnty), &
                                name1, prop1, &
                                name2, prop2, &
                                fluidname)
    end select

    call assert_normal(value)
  end function cp_value
  
  recursive function cp_deriv(qnty,independ_var,fixed_var,name1,prop1,name2,prop2,fluidname) result(deriv)
    use, intrinsic :: ieee_arithmetic, only: ieee_is_normal
    
    character(*), intent(in) :: qnty, independ_var, fixed_var
    character(*), intent(in) :: name1, name2
    real(dp), intent(in) :: prop1, prop2
    character(*), intent(in) :: fluidname
    real(dp) :: deriv
    
    real(dp) :: rho, h, cp, k, alpha
    real(dp) :: drho_dq, dh_dq, dcp_dq, dk_dq
    real(dp) :: drho_dT, dh_dT
    real(dp) :: d2rho_dTdq, d2h_dTdq

    if (qnty == 'vol_heat_capacity') then
      rho = cp_value('density',name1,prop1,name2,prop2,fluidname)
      h   = cp_value('spec_enthalpy',name1,prop1,name2,prop2,fluidname)
      drho_dT = cp_deriv('density','temperature','pressure',name1,prop1,name2,prop2,fluidname)
      dh_dT   = cp_deriv('spec_enthalpy','temperature','pressure',name1,prop1,name2,prop2,fluidname)
      drho_dq = cp_deriv('density',independ_var,fixed_var,name1,prop1,name2,prop2,fluidname)
      dh_dq   = cp_deriv('spec_enthalpy',independ_var,fixed_var,name1,prop1,name2,prop2,fluidname)
      d2rho_dTdq = cp_deriv2('density','temperature','pressure',independ_var,fixed_var,name1,prop1,name2,prop2,fluidname)
      d2h_dTdq   = cp_deriv2('spec_enthalpy','temperature','pressure',independ_var,fixed_var,name1,prop1,name2,prop2,fluidname)
      deriv = rho * d2h_dTdq + drho_dq * dh_dT  +  drho_dT * dh_dq + d2rho_dTdq * h
    elseif (qnty == 'thermal_diffusivity') then
      k   = cp_value('thermal_conductivity',name1,prop1,name2,prop2,fluidname)
      rho = cp_value('density',name1,prop1,name2,prop2,fluidname)
      cp  = cp_value('spec_heat_capacity',name1,prop1,name2,prop2,fluidname)
      dk_dq   = cp_deriv('thermal_conductivity',independ_var,fixed_var,name1,prop1,name2,prop2,fluidname)
      drho_dq = cp_deriv('density',independ_var,fixed_var,name1,prop1,name2,prop2,fluidname)
      dcp_dq  = cp_deriv('spec_heat_capacity',independ_var,fixed_var,name1,prop1,name2,prop2,fluidname)
      alpha = k / (rho * cp)

      ! CoolProp doesn't provide derivatives of the transport quantity k.
      ! https://github.com/CoolProp/CoolProp/issues/1478
      !
      if (ieee_is_normal(dk_dq)) then
        deriv = alpha * (dk_dq/k - drho_dq/rho - dcp_dq/cp)
        call assert_normal(deriv)
      else
        ! As a crude fix, a very large return value will work for the splines.
        deriv = huge(1.0_dp)
      endif
    else
      deriv = PropsSI_wrapper(cp_deriv_str(qnty,independ_var,fixed_var), &
                              name1, prop1, &
                              name2, prop2, &
                              fluidname)
    endif
    call assert_normal(deriv)
  end function cp_deriv
  
  real(dp) function cp_deriv2(qnty,x1,fixed1,x2,fixed2,name1,prop1,name2,prop2,fluidname)
    character(*), intent(in) :: qnty, x1, fixed1, x2, fixed2
    character(*), intent(in) :: name1, name2
    real(dp), intent(in) :: prop1, prop2
    character(*), intent(in) :: fluidname

    cp_deriv2 = PropsSI_wrapper(cp_deriv2_str(qnty,x1,fixed1,x2,fixed2), &
                                      name1, prop1, &
                                      name2, prop2, &
                                      fluidname)
  end function cp_deriv2

  function PropsSI_wrapper(desired_C_str,name1,prop1,name2,prop2,fluidname) result(cp_output)
    character(len=*,kind=c_char), intent(in) :: desired_C_str
    character(*), intent(in) :: name1, name2
    real(dp), intent(in) :: prop1, prop2
    character(*), intent(in) :: fluidname
    real(dp) :: cp_output
    
    real(c_double) :: cp_SI_value
    
    cp_SI_value = PropsSI(desired_C_str, &
                          cp_qnty_str(name1), real(prop1,c_double), &
                          cp_qnty_str(name2), real(prop2,c_double), &
                          cp_qnty_str(fluidname))
    cp_output = real(cp_SI_value,dp)
  end function PropsSI_wrapper
  
  impure elemental function cp_qnty_str(qnty_name)
    ! Done accoring to example in [0], where they always use character(len=32)
    ! 
    ! [0] http://www.coolprop.org/coolprop/wrappers/FORTRAN/index.html
    character(*), intent(in) :: qnty_name
    character(len=32,kind=c_char) :: cp_qnty_str
    
    cp_qnty_str = dgflow2cp_name(qnty_name) // c_null_char
  end function cp_qnty_str
  
  impure elemental function cp_deriv_str(qnty,independ_var,fixed_var)
    ! http://www.coolprop.org/coolprop/HighLevelAPI.html#partial-derivatives
    character(*), intent(in) :: qnty, independ_var, fixed_var
    character(len=32,kind=c_char) :: cp_deriv_str
    
    cp_deriv_str = cp_deriv_format(dgflow2cp_name(qnty),         &
                                   dgflow2cp_name(independ_var), &
                                   dgflow2cp_name(fixed_var))    &
                   // c_null_char
  end function cp_deriv_str
  
  impure elemental function cp_deriv2_str(qnty,x1,fixed1,x2,fixed2)
    ! http://www.coolprop.org/coolprop/HighLevelAPI.html#partial-derivatives
    character(*), intent(in) :: qnty, x1, fixed1, x2, fixed2
    character(len=32,kind=c_char) :: cp_deriv2_str

    character(:), allocatable :: cp_qnty, cp_x1, cp_x2, cp_fixed1, cp_fixed2
    character(:), allocatable :: first_deriv, second_deriv

    cp_qnty = dgflow2cp_name(qnty)
    cp_x1 = dgflow2cp_name(x1)
    cp_x2 = dgflow2cp_name(x2)
    cp_fixed1 = dgflow2cp_name(fixed1)
    cp_fixed2 = dgflow2cp_name(fixed2)
    first_deriv = cp_deriv_format(cp_qnty,cp_x1,cp_fixed1)
    second_deriv = cp_deriv_format('(' // first_deriv // ')',cp_x2,cp_fixed2)
    cp_deriv2_str = second_deriv // c_null_char
  end function cp_deriv2_str

  pure function cp_deriv_format(qnty,independ_var,fixed_var)
    character(*), intent(in) :: qnty, independ_var, fixed_var
    character(:), allocatable :: cp_deriv_format

    cp_deriv_format = 'd(' // qnty // ')/d(' // independ_var // ')|' // fixed_var
  end function cp_deriv_format

  function dgflow2cp_name(dgflow_name) result(cp_name)
    character(*), intent(in) :: dgflow_name
    character(:), allocatable :: cp_name
    
    select case(dgflow_name)
      ! The full list can be found in [1].
      !
      ! [1]  http://www.coolprop.org/coolprop/HighLevelAPI.html#parameter-table
      ! [2]  http://www.coolprop.org/coolprop/HighLevelAPI.html#parameter-table
      ! [3]  https://github.com/CoolProp/CoolProp/issues/1478
      case('density');              cp_name = 'D'
      case('molar_density');        cp_name = 'Dmolar'
      case('dynamic_viscosity');    cp_name = 'viscosity'
      case('prandtl');              cp_name = 'Prandtl'
      case('pressure');             cp_name = 'P'
      case('spec_energy');          cp_name = 'Umass'
      case('spec_enthalpy');        cp_name = 'Hmass'
      case('spec_heat_capacity');   cp_name = 'd(Hmass)/d(T)|P' ! works with higher-order derivatives (unlike 'Cpmass') [3]
      case('thermal_conductivity'); cp_name = 'conductivity'
      case('temperature');          cp_name = 'T'
      
      case('deriv_rho_h'); cp_name = 'd(D)/d(Hmass)|P'

      ! See [2] for EOS for various fluids.
      case('carbon_dioxide');      cp_name = 'CarbonDioxide'
      case('water','light_water'); cp_name = 'Water'
      
      case default
        call error('dgflow2cp_name: unknown dgflow_name=' // dgflow_name)
    end select
  end function dgflow2cp_name
end module coolprop_mod
