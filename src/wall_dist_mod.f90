module wall_dist_mod
  use f90_kind, only: dp
  use numerical_solutions, only: num_scalfunc_set
  use scal_func_mod, only: scal_func_type
  implicit none
  
  private
  public :: initialized_wall_dist, init_wall_dist
  public :: wall_dist_func
  
  class(scal_func_type), allocatable, protected :: wall_dist_func
  logical , protected :: initialized_wall_dist = .false.
  
  integer :: max_p_param = 10
  logical :: normalization = .true.
  integer :: max_Picard_iter = 30
  real(dp) :: damping_coeff = 0.1_dp
  character(1000) :: exact_wall_dist = ''
  
contains
  subroutine init_wall_dist(mesh)
    use assertions, only: assert
    use exceptions, only: error
    use functions_lib, only: string2func
    use gmsh_interface, only: write_to_gmsh_file
    use io_basics, only: read_line_from_input_file
    use mesh_type, only: fem_mesh
    use run_data, only: wall_dist_specs_file
    use string_manipulations, only: separate_first_word
    
    type(fem_mesh), intent(in) :: mesh
    
    character(:), allocatable :: how2calc,wall_dist_specs
    integer :: unit
    integer :: iostat
    character(200) :: iomsg
    namelist /wall_dist_params/ max_p_param, normalization, max_Picard_iter, damping_coeff, exact_wall_dist
    
    call assert(wall_dist_specs_file /= '','No wall distance specs file to read.')
    
    open(newunit=unit, file=wall_dist_specs_file, &
         status='old', access='sequential', form='formatted', action='read', position='rewind', &
         iostat=iostat, iomsg=iomsg)
    call assert(iostat==0,iomsg)
    
    call read_line_from_input_file(unit,how2calc)
    select case(how2calc)
      case('given')
        call read_line_from_input_file(unit,wall_dist_specs)
        call string2func(wall_dist_specs,wall_dist_func)
        call write_to_gmsh_file(wall_dist_func,mesh,'wall_dist',only_fluid=.true.,time=1)
        
      case('calculated')
        read(unit,wall_dist_params,iostat=iostat,iomsg=iomsg)
        call assert(iostat==0,iomsg)
        call assert(max_p_param >= 2, "wall distance p_param < 2")
        call calc_wall_dist(mesh)
        
      case('from_old_calculation')
        call num_scalfunc_set%get_func('wall_dist',wall_dist_func)
        
      case default
        call error("cannot interpret wall dist: how2calc=" // how2calc)
    end select
    initialized_wall_dist = .true.
  end subroutine init_wall_dist
  
  
  subroutine calc_wall_dist(mesh)
    use functionals, only: continuous_rel_l2_diff
    use functions_lib, only: string2func
    use mesh_type, only: fem_mesh
    use numerical_solutions, only: symm_of_qnty_linsys, &
                                   update_old_coeff, &
                                   get_solvec
    use petsc_mod, only: I_am_master_process
    use solvec_mod, only: solvec_t
    use timer_typedef, only: timer_type
    use wall_dist_solver_mod, only: wall_dist_solver_t, p_param
    
    type(fem_mesh), intent(in) :: mesh
    
    type(wall_dist_solver_t), target :: wall_dist_solver
    type(timer_type) :: timer_global, timer_iter
    integer :: iter
    type(solvec_t), pointer :: wall_dist
    class(scal_func_type), allocatable :: exact_wall_dist_func
    real(dp) :: best_damping_coeff
    real(dp), parameter :: MAX_ERROR = 1.0e-4_dp
    logical :: converged
    real(dp) :: error
    
    call timer_global%start()
    
    wall_dist => get_solvec('wall_dist')
    call num_scalfunc_set%get_func('wall_dist',wall_dist_func)
    
    wall_dist_solver%qnty = 'wall_dist'
    call wall_dist_solver%set_solver_specs(expect_symm=symm_of_qnty_linsys('wall_dist'))
    
    if (I_am_master_process()) then
      print *
      print *, '====================================================='
      print *, ' Wall dist calculation (normalisation set to ',normalization,')'
      print *, '====================================================='
      print *
    endif
    
    do p_param = 2, max_p_param
      if (I_am_master_process()) then
        print *
        print *, '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
        print *, 'p-Poisson parameter = ', p_param
      endif
      if (p_param < 6) then
        best_damping_coeff = max(0.4_dp,damping_coeff)
      else
        best_damping_coeff = damping_coeff
      endif
      do iter = 1, max_Picard_iter
        call timer_iter%start()
          if (I_am_master_process()) then
            print *
            print *, '*********************'
            write (*,'(a,i4)') '  Picard iter:', iter
            print *, '*********************'
          endif
          call wall_dist_solver%assemble_and_solve(mesh)

          error = continuous_rel_l2_diff(mesh,.true.,new=wall_dist_func,time_new=1,old=wall_dist_func,time_old=2,bcast=.true.)
          if (I_am_master_process()) then
            print *
            write(*,'(a,es10.3)') 'rel. L2-error wrt previous iter = ', error
            write(*,'(a,es10.3)') 'comp. time this iter = ', timer_iter%time()
          endif
          converged = error < MAX_ERROR
        call timer_iter%reset()
        
        call wall_dist%damp_new_coeff(best_damping_coeff)
        call update_old_coeff('wall_dist')
        if (converged) exit
      enddo
      
      if (.not. converged .and. I_am_master_process()) then
        print *
        write (*,'(a,i4,a)') '****** WARNING ****** : no converge within', max_Picard_iter, ' Picard iterations'
      endif
      if (I_am_master_process()) print *, '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    enddo
    
    if (normalization) call normalize_wall_dist(mesh)
    
    if (exact_wall_dist /= '') then
      call string2func(exact_wall_dist,exact_wall_dist_func)
      error = continuous_rel_l2_diff(mesh,.true.,new=exact_wall_dist_func,time_new=1,old=wall_dist_func,time_old=1,bcast=.false.)
      if (I_am_master_process()) write(*,'(a,es10.3)') new_line('a') // ' rel. L2-error wrt exact distance = ', error
    endif
    
    if (I_am_master_process()) then
      print *
      print *, '=============================================================='
      write (*,'(a,es10.3)') '   End wall dist calculation. Total comp. time = ', timer_global%time()
      print *, '=============================================================='
    endif
  end subroutine calc_wall_dist
  
  
  subroutine normalize_wall_dist(mesh)
    use galerkin_projection, only: galerkin_proj
    use mesh_type, only: fem_mesh
    use numerical_solutions, only: get_solvec
    use scalfunc_from_proc_mod, only: as_scalfunc
    use solvec_mod, only: solvec_t
    
    type(fem_mesh), intent(in) :: mesh
    
    type(solvec_t), pointer :: wall_dist
    integer, parameter :: TIME = 1
    
    wall_dist => get_solvec('wall_dist')
    call galerkin_proj(mesh,wall_dist%handler,as_scalfunc(normalize_wall_dist_on_elem),TIME,wall_dist%petsc_coeffs)
    call wall_dist%update_ghost_values()
  end subroutine normalize_wall_dist
  
  subroutine normalize_wall_dist_on_elem(elem,pnt_set,time,values)
    use mesh_objects, only: elem_list
    use pnt_set_mod, only: pnt_set_t
    
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    real(dp), dimension(size(values)) :: dist
    real(dp), dimension(elem%dimen(),size(values)) :: dist_grad
    real(dp) :: factor
    
    call wall_dist_func%get_values(elem,pnt_set,time,dist)
    call wall_dist_func%get_gders(elem,pnt_set,time,dist_grad)
    
    factor = real(max_p_param,dp)/real(max_p_param-1,dp)
    values = (factor*dist + norm2(dist_grad,1)**max_p_param)**(1.0_dp/factor) - norm2(dist_grad,1)**(max_p_param-1)
  end subroutine normalize_wall_dist_on_elem
end module wall_dist_mod

