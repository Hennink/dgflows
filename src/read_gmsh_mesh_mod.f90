module read_gmsh_mesh_mod
  use assertions,  only: assert, assert_eq, assert_normal
  use exceptions, only: error
  use f90_kind, only: dp
  use string_manipulations, only: int2char
  use run_data, only: mesh_periodic_equiv_tol
  use support, only: iseq
  implicit none
  
  private
  public :: read_gmsh_mesh
  public :: verify_bc_mat_ids
  
contains
  subroutine link_gmsh_msh_to_unit(unit_no)
    use petsc_mod, only: I_am_master_process, petsc_mpi_barrier
    use run_data, only: input_mesh, mesh_dimen
    use string_manipulations, only: int2char, rstrip, separate_first_word
    use system_commands, only: exec_cmd
    
    integer, intent(out) :: unit_no
    
    character(:), allocatable :: file_type, specs, msh_file
    character(1000) :: iomsg
    integer :: ios
    
    call separate_first_word(input_mesh,file_type,specs,':')
    if (input_mesh == '') then
      msh_file = 'fort.8' ! For historical reasons, default to reading a Gmsh msh file from 'fort.8'.
    elseif (file_type == 'gmsh_msh') then
      msh_file = specs
    elseif (file_type == 'gmsh_geo') then
      msh_file = rstrip(specs, '.geo') // '.msh'
      call gmsh(specs, msh_file)
    else
      call error('cannot inperpret input_mesh=' // trim(input_mesh) // &
                 " ... I don't understand file_type=" // file_type)
    endif

    open(file=msh_file,newunit=unit_no, &
         status='old',position='rewind',action='read', &
         iomsg=iomsg,iostat=ios)
    call assert(ios == 0,'ios /= 0 when opening msh_file; iomsg=' // trim(iomsg))

  contains
    subroutine gmsh(geo_file, msh_file)
      character(*), intent(in) :: geo_file, msh_file

      character(:), allocatable :: gmsh_cmd, needs_update
      logical :: exist

      if (I_am_master_process()) then
        inquire(file=geo_file, exist=exist)
        call assert(exist, 'Input file for Gmsh does not exist: file=' // geo_file)
        gmsh_cmd = 'gmsh ' // geo_file // ' -' // int2char(mesh_dimen) // ' -format msh -o ' // msh_file
        needs_update = '[ ! -f ' // msh_file // ' ] || [ ' // geo_file // ' -nt ' // msh_file // ' ]'
        call exec_cmd('if ' // needs_update // ' ; then ' // gmsh_cmd // ' ; fi', wait=.true.)
      endif
      call petsc_mpi_barrier()
    end subroutine
  end subroutine
  
  subroutine read_gmsh_mesh(mesh)
    use boundaries, only: periodicity_specs
    use code_const, only: BOUNDARY, INTERNAL, INTERFACE_SOLID_FLUID, INTERFACE_WITH_IGNORED, UNINITIALIZED_INT
    use local_elem, only: loc_idx_of_face_vertices_in_elem, &
                          no_vertices
    use mesh_type, only: fem_mesh, insert_vertex, periodicity_shift
    use mesh_objects, only: elem_list, &
                            eltype_of_faces_of_elem, &
                            no_faces_of_elem, &
                            vertex_coordinate, &
                            vertex_list, vertex_ptr
    use petsc_mod, only: I_am_master_process

    type(fem_mesh), intent(out) :: mesh
    
    ! Some limits when reading data from gmsh file
    integer, parameter :: MAX_NO_VERT_PER_ELEM = 27
    integer, parameter :: MAX_NO_FACES_PER_ELEM = 6
    integer, parameter :: MAX_NO_VERT_PER_FACE = 9
    ! integer, parameter :: MAX_NO_ELEM_PER_EDGE = 10
    
    ! Values of bc_id of special faces. These need to be hard-coded for now.
    integer, parameter :: INTERFACE_SOLID_FLUID_ID = 500
    integer, parameter :: INTERFACE_WITH_IGNORED_ELEM_ID = 400
    
    ! Local variables
    integer :: lunit
    logical :: vertex_found
    logical :: nghb_found
    logical :: bnd_elem_found
    logical :: found_in_this_vertex
    integer :: vertex_prim_no, vertex_sec_no
    integer :: vertex1
    integer :: vertex2
    integer :: face_vertex
    integer :: nghb_face_vertex
    integer :: no_elem_faces
    integer :: no_nghb_faces
    integer :: no_facevertices
    integer :: no_unique_faces
    integer :: free_face
    integer :: no_tot_elem_gmsh
    integer :: dummy1,dummy2,dummy3,dummy4,dummy5
    integer :: dummy6,dummy7,dummy8,dummy9,dummy10
    integer :: dummy11,dummy12,dummy13,dummy14,dummy15
    integer :: dummy16,dummy17,dummy18,dummy19,dummy20
    integer :: dummy21,dummy22,dummy23,dummy24,dummy25
    integer :: dummy26,dummy27
    integer :: i
    integer :: elem_no
    integer :: elem_sec_no
    integer :: bnd_elem_sec_no
    integer :: bnd_elem_counter
    integer :: bnd_elem_counter2
    integer :: elem_counter
    integer :: elem_counter2
    integer :: test_bnd_elem
    integer :: test_bnd_elem2
    integer :: test_elem
    integer :: test_elem2
    integer :: bnd_elem      ! loop counter
    integer :: nghb_elem     ! loop counter
    integer :: face
    integer :: global_face
    integer :: nghb_face     ! loop counter
    integer :: elem_dl       ! loop counter
    integer :: no_tags       ! used for skipping info when reading gmsh file
    integer :: vertex        ! vertex number
    integer :: dim_elem      ! dimension corresponding to an element
    integer :: no_phys_names ! how many physical names to be read
    integer :: no_vert_elem  ! how many vertices on an element
    integer :: no_vert_bnd_elem ! how many vertices on an element
    integer :: no_elem       ! number of internal elements
    integer :: no_bnd_elem   ! number of boundary elements
    integer :: loc_vertex    ! number of boundary elements
    integer :: max_no_elem_per_vertex
    integer :: max_no_bnd_elem_per_vertex
    integer :: face_no, face_sec_no
    real(dp), dimension(3) :: shift_vector
    integer, allocatable, dimension(:) :: eltype_gmsh
    integer, allocatable, dimension(:) :: eltype_dl
    integer, allocatable, dimension(:) :: eltype_dl_bnd
    integer, allocatable, dimension(:) :: elem_id_gmsh
    integer, allocatable, dimension(:) :: vol_id
    integer, allocatable, dimension(:,:) :: vertex_connect_gmsh
    integer, allocatable, dimension(:,:) :: vertex_connect_dl
    integer, allocatable, dimension(:,:) :: vertex_connect_dl_bnd
    integer, allocatable, dimension(:) :: bnd_info
    integer, allocatable, dimension(:) :: face_vertices
    integer, allocatable, dimension(:) :: nghb_facevertices
    integer, allocatable, dimension(:) :: dummy_int_array
    integer, allocatable, dimension(:) :: no_elem_per_vertex
    integer, allocatable, dimension(:) :: no_bnd_elem_per_vertex
    integer, allocatable, dimension(:,:) :: vertex_to_elem_array
    integer, allocatable, dimension(:,:) :: vertex_to_bnd_elem_array
    integer, allocatable, dimension(:) :: temp
    integer, allocatable, dimension(:,:,:) :: face_to_face_array
    logical, allocatable, dimension(:) :: global_face_is_periodic
    integer, allocatable, dimension(:) :: global_face_periodic_dir
    integer, allocatable, dimension(:) :: global_face_periodic_bc_id
    integer :: face_periodic_dir
    character(260) :: char_array
    type(vertex_list) :: vertex_prim
    type(vertex_coordinate) :: vertex_position
    type(vertex_ptr), pointer :: vertex_ptr_array(:)
    logical :: mesh_is_periodic
    integer, allocatable, dimension(:) :: primary_periodic_face_ids, secondary_periodic_face_ids
    integer, allocatable, dimension(:) :: periodic_dirs
    logical :: face_is_primary, face_is_secondary
    integer :: no_periodic_directions, per_dir_no
    
    if (I_am_master_process()) print *,'Reading gmsh file'
    
    call periodicity_specs(is_periodic=mesh_is_periodic, &
        prim_id=primary_periodic_face_ids,sec_id=secondary_periodic_face_ids, &
        dir=periodic_dirs)

    no_periodic_directions = assert_eq(size(primary_periodic_face_ids),size(secondary_periodic_face_ids)) 
    
    allocate(face_vertices(MAX_NO_VERT_PER_FACE))
    allocate(nghb_facevertices(MAX_NO_VERT_PER_FACE))
    
    call link_gmsh_msh_to_unit(lunit)
    
    ! Skip MeshFormat header
    read(lunit,*)
    read(lunit,*)
    read(lunit,*)
    
    ! Check PhysicalNames header
    read(lunit,200,end=100,err=100) char_array
    100 continue
    if (index(char_array,'$PhysicalNames')/=0) then
      read(lunit,*) no_phys_names
      do i=1,no_phys_names
        read(lunit,*)
      enddo
      read(lunit,*) ! this skips the line $EndPhysicalNames
      read(lunit,*) ! this skips the line $Nodes
    endif
    
    ! Read the vertices of the mesh
    read(lunit,*) mesh%no_vertices
    do vertex=1,mesh%no_vertices
      read(lunit,*) dummy1,vertex_position
      call insert_vertex(vertex,vertex_position,mesh%vertex_list_head,mesh%vertex_list_tail)
    enddo
    read(lunit,*)
    
    ! Read the elements of the mesh and
    ! put the element type in eltype_gmsh(:)
    ! put the element id in vertex_array_gmsh(:)
    allocate(dummy_int_array(20))
    read(lunit,*)
    read(lunit,*) no_tot_elem_gmsh
    allocate(eltype_gmsh(no_tot_elem_gmsh))
    allocate(elem_id_gmsh(no_tot_elem_gmsh))
    allocate(vertex_connect_gmsh(MAX_NO_VERT_PER_ELEM,no_tot_elem_gmsh))
    do elem_no=1,no_tot_elem_gmsh
      read(lunit,200,end=101,err=101) char_array
    101 continue
    200 format(80a)
      read(char_array,*) dummy1,eltype_gmsh(elem_no),no_tags
      if (eltype_gmsh(elem_no)==1) then     ! 2-node line
        no_vert_elem = 2
      elseif (eltype_gmsh(elem_no)==8) then ! 3-node quadratic line
        no_vert_elem = 3
      elseif (eltype_gmsh(elem_no)==2) then ! 3-node linear triangle
        no_vert_elem = 3
      elseif (eltype_gmsh(elem_no)==9) then ! 6-node quadratic triangle
        no_vert_elem = 6
      elseif (eltype_gmsh(elem_no)==4) then ! 4-node linear tetrahedron
        no_vert_elem = 4
      elseif (eltype_gmsh(elem_no)==11) then ! 10-node quadratic tetrahedron
        no_vert_elem = 10
      elseif (eltype_gmsh(elem_no)==3) then ! 4-node linear quad
        no_vert_elem = 4
      elseif (eltype_gmsh(elem_no)==16) then ! 8-node quadratic quad
        no_vert_elem = 8
      elseif (eltype_gmsh(elem_no)==10) then ! 9-node quadratic quad
        no_vert_elem = 9
      elseif (eltype_gmsh(elem_no)==5) then ! 8-node linear hex
        no_vert_elem = 8
      elseif (eltype_gmsh(elem_no)==17) then ! 20-node quadratic hex
        no_vert_elem = 20
      elseif (eltype_gmsh(elem_no)==12) then ! 27-node quadratic hex
        no_vert_elem = 27
      elseif (eltype_gmsh(elem_no)==15) then ! 1-node point
        no_vert_elem = 1
      else
        print *,'element type',eltype_gmsh(elem_no),'not supported'
        STOP
      endif
      read(char_array,*) dummy1,dummy2,dummy3,elem_id_gmsh(elem_no),(dummy_int_array(i),i=1,no_tags-1),&
                         (vertex_connect_gmsh(i,elem_no),i=1,no_vert_elem)
    enddo
    read(lunit,*)
    
    close(lunit)
    
    ! Determine the dimension of the geometry by checking the various element types
    mesh%dimen=1
    do elem_no=1,no_tot_elem_gmsh
      if (eltype_gmsh(elem_no)==1)  dim_elem=1
      if (eltype_gmsh(elem_no)==2)  dim_elem=2
      if (eltype_gmsh(elem_no)==3)  dim_elem=2
      if (eltype_gmsh(elem_no)==4)  dim_elem=3
      if (eltype_gmsh(elem_no)==5)  dim_elem=3
      if (eltype_gmsh(elem_no)==8)  dim_elem=1
      if (eltype_gmsh(elem_no)==9)  dim_elem=2
      if (eltype_gmsh(elem_no)==10) dim_elem=2
      if (eltype_gmsh(elem_no)==11) dim_elem=3
      if (eltype_gmsh(elem_no)==12) dim_elem=3
      if (eltype_gmsh(elem_no)==15) dim_elem=0
      if (eltype_gmsh(elem_no)==16) dim_elem=2
      if (eltype_gmsh(elem_no)==17) dim_elem=3
      if (dim_elem > mesh%dimen) mesh%dimen=dim_elem
    enddo
    
    if (I_am_master_process()) print *,'dim =',mesh%dimen
    
    ! Based on the known dimensionality of the problem select the internal elements
    elem_dl = 0
    allocate(eltype_dl(no_tot_elem_gmsh)) ! These are a bit too big ...
    allocate(vol_id(no_tot_elem_gmsh))
    allocate(vertex_connect_dl(MAX_NO_VERT_PER_ELEM,no_tot_elem_gmsh))
    do elem_no=1,no_tot_elem_gmsh
      if (mesh%dimen==1) then
        if ( (eltype_gmsh(elem_no)==1).or.(eltype_gmsh(elem_no)==8) ) then
          elem_dl = elem_dl + 1
          if (eltype_gmsh(elem_no)==1) eltype_dl(elem_dl) = 12
          if (eltype_gmsh(elem_no)==8) eltype_dl(elem_dl) = 13
          vol_id(elem_dl) = elem_id_gmsh(elem_no)
          vertex_connect_dl(:,elem_dl) = vertex_connect_gmsh(:,elem_no)
        endif
      endif
      
      if (mesh%dimen==2) then
        if ( (eltype_gmsh(elem_no)==2).or.(eltype_gmsh(elem_no)==3).or.(eltype_gmsh(elem_no)==9).or.&
                                       (eltype_gmsh(elem_no)==10).or.(eltype_gmsh(elem_no)==16) ) then
          elem_dl = elem_dl + 1
          if (eltype_gmsh(elem_no)==2)  eltype_dl(elem_dl) = 23
          if (eltype_gmsh(elem_no)==3)  eltype_dl(elem_dl) = 24
          if (eltype_gmsh(elem_no)==9)  eltype_dl(elem_dl) = 26
          if (eltype_gmsh(elem_no)==16) eltype_dl(elem_dl) = 28
          if (eltype_gmsh(elem_no)==10) eltype_dl(elem_dl) = 29
          vol_id(elem_dl) = elem_id_gmsh(elem_no)
          vertex_connect_dl(:,elem_dl) = vertex_connect_gmsh(:,elem_no)
        endif
      endif
      
      if (mesh%dimen==3) then
        if ( (eltype_gmsh(elem_no)==4).or.(eltype_gmsh(elem_no)==5).or.(eltype_gmsh(elem_no)==11).or.&
                                       (eltype_gmsh(elem_no)==12).or.(eltype_gmsh(elem_no)==17) ) then
          elem_dl = elem_dl + 1
          if (eltype_gmsh(elem_no)==4)  eltype_dl(elem_dl) = 34
          if (eltype_gmsh(elem_no)==5)  eltype_dl(elem_dl) = 38
          if (eltype_gmsh(elem_no)==11) eltype_dl(elem_dl) = 310
          if (eltype_gmsh(elem_no)==17) eltype_dl(elem_dl) = 320
          if (eltype_gmsh(elem_no)==12) eltype_dl(elem_dl) = 327
          vol_id(elem_dl) = elem_id_gmsh(elem_no)
          vertex_connect_dl(:,elem_dl) = vertex_connect_gmsh(:,elem_no)
        endif
      endif
    enddo
    no_elem = elem_dl
    mesh%tot_no_elem= no_elem
    
    ! Here re-order the node numbers if necessary.
    ! Lines, Quadrilaterals and Linear Hexahedral elements are identical.
    ! Triangles are not identical but are obtained from rotation.
    ! All Tetrahedral and above-linear Hex elements however need to be renumbered.
    do elem_no=1,no_elem
      if (eltype_dl(elem_no)==34) then
        dummy1 = vertex_connect_dl(1,elem_no)
        dummy2 = vertex_connect_dl(2,elem_no)
        dummy3 = vertex_connect_dl(3,elem_no)
        dummy4 = vertex_connect_dl(4,elem_no)
        
        vertex_connect_dl(1,elem_no) = dummy2
        vertex_connect_dl(2,elem_no) = dummy3
        vertex_connect_dl(3,elem_no) = dummy4
        vertex_connect_dl(4,elem_no) = dummy1
      endif
      
      if (eltype_dl(elem_no)==310) then
        dummy1  = vertex_connect_dl(1,elem_no)
        dummy2  = vertex_connect_dl(2,elem_no)
        dummy3  = vertex_connect_dl(3,elem_no)
        dummy4  = vertex_connect_dl(4,elem_no)
        dummy5  = vertex_connect_dl(5,elem_no)
        dummy6  = vertex_connect_dl(6,elem_no)
        dummy7  = vertex_connect_dl(7,elem_no)
        dummy8  = vertex_connect_dl(8,elem_no)
        dummy9  = vertex_connect_dl(9,elem_no)
        dummy10 = vertex_connect_dl(10,elem_no)
        
        vertex_connect_dl(1,elem_no)  = dummy2
        vertex_connect_dl(2,elem_no)  = dummy3
        vertex_connect_dl(3,elem_no)  = dummy4
        vertex_connect_dl(4,elem_no)  = dummy1
        vertex_connect_dl(5,elem_no)  = dummy6
        vertex_connect_dl(6,elem_no)  = dummy9
        vertex_connect_dl(7,elem_no)  = dummy10
        vertex_connect_dl(8,elem_no)  = dummy5
        vertex_connect_dl(9,elem_no)  = dummy7
        vertex_connect_dl(10,elem_no) = dummy8
      endif
      
      if (eltype_dl(elem_no)==320) then
        dummy9  = vertex_connect_dl(9,elem_no)
        dummy10 = vertex_connect_dl(10,elem_no)
        dummy11 = vertex_connect_dl(11,elem_no)
        dummy12 = vertex_connect_dl(12,elem_no)
        dummy13 = vertex_connect_dl(13,elem_no)
        dummy14 = vertex_connect_dl(14,elem_no)
        dummy15 = vertex_connect_dl(15,elem_no)
        dummy16 = vertex_connect_dl(16,elem_no)
        dummy17 = vertex_connect_dl(17,elem_no)
        dummy18 = vertex_connect_dl(18,elem_no)
        dummy19 = vertex_connect_dl(19,elem_no)
        dummy20 = vertex_connect_dl(20,elem_no)
        
        vertex_connect_dl(9,elem_no)  = dummy9
        vertex_connect_dl(10,elem_no) = dummy12
        vertex_connect_dl(11,elem_no) = dummy14
        vertex_connect_dl(12,elem_no) = dummy10
        vertex_connect_dl(13,elem_no) = dummy17
        vertex_connect_dl(14,elem_no) = dummy19
        vertex_connect_dl(15,elem_no) = dummy20
        vertex_connect_dl(16,elem_no) = dummy18
        vertex_connect_dl(17,elem_no) = dummy11
        vertex_connect_dl(18,elem_no) = dummy13
        vertex_connect_dl(19,elem_no) = dummy15
        vertex_connect_dl(20,elem_no) = dummy16
      endif
      
      if (eltype_dl(elem_no)==327) then
        dummy9  = vertex_connect_dl(9,elem_no)
        dummy10 = vertex_connect_dl(10,elem_no)
        dummy11 = vertex_connect_dl(11,elem_no)
        dummy12 = vertex_connect_dl(12,elem_no)
        dummy13 = vertex_connect_dl(13,elem_no)
        dummy14 = vertex_connect_dl(14,elem_no)
        dummy15 = vertex_connect_dl(15,elem_no)
        dummy16 = vertex_connect_dl(16,elem_no)
        dummy17 = vertex_connect_dl(17,elem_no)
        dummy18 = vertex_connect_dl(18,elem_no)
        dummy19 = vertex_connect_dl(19,elem_no)
        dummy20 = vertex_connect_dl(20,elem_no)
        dummy21 = vertex_connect_dl(21,elem_no)
        dummy22 = vertex_connect_dl(22,elem_no)
        dummy23 = vertex_connect_dl(23,elem_no)
        dummy24 = vertex_connect_dl(24,elem_no)
        dummy25 = vertex_connect_dl(25,elem_no)
        dummy26 = vertex_connect_dl(26,elem_no)
        dummy27 = vertex_connect_dl(27,elem_no)
        
        vertex_connect_dl(9,elem_no)  = dummy9
        vertex_connect_dl(10,elem_no) = dummy12
        vertex_connect_dl(11,elem_no) = dummy14
        vertex_connect_dl(12,elem_no) = dummy10
        vertex_connect_dl(13,elem_no) = dummy17
        vertex_connect_dl(14,elem_no) = dummy19
        vertex_connect_dl(15,elem_no) = dummy20
        vertex_connect_dl(16,elem_no) = dummy18
        vertex_connect_dl(17,elem_no) = dummy11
        vertex_connect_dl(18,elem_no) = dummy13
        vertex_connect_dl(19,elem_no) = dummy15
        vertex_connect_dl(20,elem_no) = dummy16
        vertex_connect_dl(21,elem_no) = dummy21
        vertex_connect_dl(22,elem_no) = dummy26
        vertex_connect_dl(23,elem_no) = dummy22
        vertex_connect_dl(24,elem_no) = dummy25
        vertex_connect_dl(25,elem_no) = dummy23
        vertex_connect_dl(26,elem_no) = dummy24
        vertex_connect_dl(27,elem_no) = dummy27
      endif
    enddo
    
    ! Setup some primary information of the elements
    ! the head is a non-existent element with no_elem children (the elements read from file)
    allocate(mesh%elem_list_head)
    mesh%elem_list_head%eltype = 0  ! not relevant, not used
    mesh%elem_list_head%level = -1  ! level of head is -1
    mesh%elem_list_head%id%branch = 0 ! not relevant?
    mesh%elem_list_head%mat_id = 0 ! not relevant?
    mesh%elem_list_head%active = .false.
    
    allocate(mesh%elem_list_head%children(no_elem))
    do elem_no=1,no_elem
      allocate(mesh%elem_list_head%children(elem_no)%point)
      mesh%elem_list_ptr => mesh%elem_list_head%children(elem_no)%point
      mesh%elem_list_ptr%eltype = eltype_dl(elem_no)                        ! set eltype
      mesh%elem_list_ptr%level = 0                                          ! set level to zero for element
      mesh%elem_list_ptr%id%branch = elem_no                                ! set branch of id
      mesh%elem_list_ptr%mat_id = vol_id(elem_no)                           ! id for material specification
      mesh%elem_list_ptr%active = .true.                                    ! active element
      mesh%elem_list_ptr%mother => mesh%elem_list_head                      ! ptr from child to mother
      mesh%elem_list_head%children(elem_no)%point => mesh%elem_list_ptr     ! ptr from mother to child
      nullify(mesh%elem_list_ptr)
    enddo
    
    ! Set up the pointers from the elements to the vertices
    ! To quickly access the vertex data structure we will not seek through the list
    ! until we find the right vertex but we first setup a array of this type
    ! (vertex_ptr_array). After this setup stage the correct pointer is then
    ! immediately found.
    allocate(vertex_ptr_array(mesh%no_vertices))
    mesh%vertex_list_ptr => mesh%vertex_list_head
    do vertex=1,mesh%no_vertices
      vertex_ptr_array(vertex)%point => mesh%vertex_list_ptr
      if (associated(mesh%vertex_list_ptr%next)) mesh%vertex_list_ptr => mesh%vertex_list_ptr%next
    enddo
    nullify(mesh%vertex_list_ptr)
    
    ! Now set the pointers in each element to the vertices
    do elem_no=1,no_elem
      mesh%elem_list_ptr => mesh%elem_list_head%children(elem_no)%point
      no_vert_elem = no_vertices(eltype_dl(elem_no))
      allocate(mesh%elem_list_ptr%vertices(no_vert_elem))
      do i=1,no_vert_elem
        mesh%elem_list_ptr%vertices(i)%point => vertex_ptr_array(vertex_connect_dl(i,elem_no))%point
      enddo
      nullify(mesh%vertex_list_ptr)
    enddo
    
    ! Select boundary elements from the complete element list
    elem_dl = 0
    allocate(bnd_info(no_tot_elem_gmsh))
    allocate(eltype_dl_bnd(no_tot_elem_gmsh))
    allocate(vertex_connect_dl_bnd(MAX_NO_VERT_PER_ELEM,no_tot_elem_gmsh))
    do elem_no=1,no_tot_elem_gmsh
      if (mesh%dimen==1) then
    !   I do not include these because I do not have an element type for it
    !   Better to do this in the near future to have uniformity
      endif
      
      if (mesh%dimen==2) then
        if ((eltype_gmsh(elem_no)==1).or.(eltype_gmsh(elem_no)==8)) then
          elem_dl = elem_dl + 1
          if (eltype_gmsh(elem_no)==1) eltype_dl_bnd(elem_dl) = 12
          if (eltype_gmsh(elem_no)==8) eltype_dl_bnd(elem_dl) = 13
          vertex_connect_dl_bnd(:,elem_dl) = vertex_connect_gmsh(:,elem_no)
          bnd_info(elem_dl) = elem_id_gmsh(elem_no)
        endif
      endif
      
      if (mesh%dimen==3) then
        if ((eltype_gmsh(elem_no)==2).or.(eltype_gmsh(elem_no)==3).or.(eltype_gmsh(elem_no)==9).or.&
            (eltype_gmsh(elem_no)==10).or.(eltype_gmsh(elem_no)==16) ) then
          elem_dl = elem_dl + 1
          if (eltype_gmsh(elem_no)==2)  eltype_dl_bnd(elem_dl) = 23
          if (eltype_gmsh(elem_no)==3)  eltype_dl_bnd(elem_dl) = 24
          if (eltype_gmsh(elem_no)==9)  eltype_dl_bnd(elem_dl) = 26
          if (eltype_gmsh(elem_no)==16) eltype_dl_bnd(elem_dl) = 28
          if (eltype_gmsh(elem_no)==10) eltype_dl_bnd(elem_dl) = 29
          vertex_connect_dl_bnd(:,elem_dl) = vertex_connect_gmsh(:,elem_no)
          bnd_info(elem_dl) = elem_id_gmsh(elem_no)
        endif
      endif
    enddo
    no_bnd_elem = elem_dl
    if (no_bnd_elem==0) then
      print *,'Warning: no boundary elements were selected'
    endif
    if (I_am_master_process()) print *,'no_bnd_elem',no_bnd_elem
    
    ! Get the number of internal elements that a node is associated with
    allocate(no_elem_per_vertex(mesh%no_vertices))
    no_elem_per_vertex = 0
    do elem_no=1,no_elem
      no_vert_elem = no_vertices(eltype_dl(elem_no))
      do loc_vertex=1,no_vert_elem
        vertex = vertex_connect_dl(loc_vertex,elem_no)
        no_elem_per_vertex(vertex) = no_elem_per_vertex(vertex) + 1
      enddo
    enddo
    max_no_elem_per_vertex = maxval(no_elem_per_vertex(1:mesh%no_vertices))
    allocate(vertex_to_elem_array(max_no_elem_per_vertex,mesh%no_vertices))
    
    ! Get the indices of internal elements that a node is associated with
    allocate(temp(mesh%no_vertices))
    temp = 1
    vertex_to_elem_array = 0
    do elem_no=1,no_elem
      no_vert_elem = no_vertices(eltype_dl(elem_no))
      do loc_vertex=1,no_vert_elem
        vertex = vertex_connect_dl(loc_vertex,elem_no)
        vertex_to_elem_array(temp(vertex),vertex) = elem_no
        temp(vertex) = temp(vertex) + 1
      enddo
    enddo
    deallocate(temp)
    
    ! Get the number of boundary elements that a node is associated with
    allocate(no_bnd_elem_per_vertex(mesh%no_vertices))
    no_bnd_elem_per_vertex = 0
    do bnd_elem=1,no_bnd_elem
      no_vert_bnd_elem = no_vertices(eltype_dl_bnd(bnd_elem))
      do loc_vertex=1,no_vert_bnd_elem
        vertex = vertex_connect_dl_bnd(loc_vertex,bnd_elem)
        no_bnd_elem_per_vertex(vertex) = no_bnd_elem_per_vertex(vertex) + 1
      enddo
    enddo
    max_no_bnd_elem_per_vertex = maxval(no_bnd_elem_per_vertex(1:mesh%no_vertices))
    allocate(vertex_to_bnd_elem_array(max_no_bnd_elem_per_vertex,mesh%no_vertices))
    
    ! Get the indices of boundary elements that a node is associated with
    allocate(temp(mesh%no_vertices))
    temp = 1
    vertex_to_bnd_elem_array = 0
    do bnd_elem=1,no_bnd_elem
      no_vert_bnd_elem = no_vertices(eltype_dl_bnd(bnd_elem))
      do loc_vertex=1,no_vert_bnd_elem
        vertex = vertex_connect_dl_bnd(loc_vertex,bnd_elem)
        vertex_to_bnd_elem_array(temp(vertex),vertex) = bnd_elem
        temp(vertex) = temp(vertex) + 1
      enddo
    enddo
    deallocate(temp)
    
    ! Setup face to face indexing (which face of which element faces a specific other face)
    ! Every face is either an internal face or has a boundary tag
    ! Any face also has a global number to store the average unit normal
    !
    ! Array for the following information 1=elem / 2=face   / 3=global face
    ! On the boundary it contains         0      / bnd_elem / global face
    !
    ! Procedure:
    !  Loop over elements and faces
    !  Find the neighbors of the face through the node connections
    !  If there is only 0 neighbors -> on the boundary then
    !  search the boundary elements for the right one.
    
    allocate(face_to_face_array(no_elem,MAX_NO_FACES_PER_ELEM,3))
    face_to_face_array = 0
    face_to_face_array(:,:,1) = -1 ! initialize to negative to recognize unhandled elements
    
    allocate(global_face_is_periodic(no_elem*8)) ! This is more than enough storage
    global_face_is_periodic = .false.
    allocate(global_face_periodic_dir(no_elem*8))
    global_face_periodic_dir = 0
    allocate(global_face_periodic_bc_id(no_elem*8))
    global_face_periodic_bc_id = 0
    
    free_face = 1
    do elem_no=1,no_elem
      no_elem_faces=no_faces_of_elem(mesh%elem_list_head%children(elem_no)%point%eltype)
      do face=1,no_elem_faces
        if (face_to_face_array(elem_no,face,1) == -1) then ! not handled yet
          
          no_facevertices = no_vertices(eltype_of_faces_of_elem(eltype_dl(elem_no))) ! Is this needed?
          call loc_idx_of_face_vertices_in_elem(eltype_dl(elem_no),face,face_vertices)
          
          ! find neighboring element by choosing from an element from the node_to_elem_array
          ! of the 1st node and test it against the lists from another node
          nghb_found = .false.
          vertex1 = vertex_connect_dl(face_vertices(1),elem_no)
    elem_loop: do elem_counter=1,no_elem_per_vertex(vertex1)
            test_elem = vertex_to_elem_array(elem_counter,vertex1)
            if (test_elem /= elem_no) then
              nghb_found = .true.
              
              ! here the loop to see if it is not the right element by testing the other face vertices
              do face_vertex=2,no_facevertices
                found_in_this_vertex = .false.
                vertex2 = vertex_connect_dl(face_vertices(face_vertex),elem_no)
                do elem_counter2=1,no_elem_per_vertex(vertex2)
                  test_elem2 = vertex_to_elem_array(elem_counter2,vertex2)
                  if (test_elem2 == test_elem) found_in_this_vertex = .true.
                enddo
                if (.not.found_in_this_vertex) then
                  nghb_found = .false.
                  cycle elem_loop
                endif
              enddo
              
              if (nghb_found) then
                nghb_elem = test_elem
                exit elem_loop
              endif
            endif
          enddo elem_loop
          
          ! At this point we have either found the neighbour (nghb_found=.true.) and we
          ! look for the right face or we need to look for the boundary element instead
          if (nghb_found) then
            
            ! find face number of the neighboring element
            no_nghb_faces=no_faces_of_elem(mesh%elem_list_head%children(nghb_elem)%point%eltype)
    nghb_face_loop: do nghb_face=1,no_nghb_faces
              call loc_idx_of_face_vertices_in_elem(eltype_dl(nghb_elem),nghb_face,nghb_facevertices)
              
              do face_vertex=1,no_facevertices
                vertex_found = .false.
                do nghb_face_vertex=1,no_facevertices
                  if (vertex_connect_dl(face_vertices(face_vertex),elem_no) == &
                      vertex_connect_dl(nghb_facevertices(nghb_face_vertex),nghb_elem)) then
                    vertex_found = .true.
                    exit
                  endif
                enddo
                if (.not.vertex_found) cycle nghb_face_loop
              enddo
              
              if (vertex_found) then
                exit
              endif
            enddo nghb_face_loop
            
            ! put information in arrays
            face_to_face_array(elem_no,face,1) = nghb_elem
            face_to_face_array(elem_no,face,2) = nghb_face
            face_to_face_array(elem_no,face,3) = free_face
            face_to_face_array(nghb_elem,nghb_face,1) = elem_no
            face_to_face_array(nghb_elem,nghb_face,2) = face
            face_to_face_array(nghb_elem,nghb_face,3) = free_face

            free_face = free_face+1
          else
            
            ! find boundary element by choosing from an element from the node_to_elem_array
            ! of the 1st node and test it against the lists from another node
            nghb_found = .false. ! does this statement do anything? REMOVE??????
            vertex1 = vertex_connect_dl(face_vertices(1),elem_no)
    bnd_elem_loop: do bnd_elem_counter=1,no_bnd_elem_per_vertex(vertex1)
              test_bnd_elem = vertex_to_bnd_elem_array(bnd_elem_counter,vertex1)
              bnd_elem_found = .true.
              ! here loop to see if it is not the right element by testing the other facevertices
              do face_vertex=2,no_facevertices
                found_in_this_vertex = .false.
                vertex2 = vertex_connect_dl(face_vertices(face_vertex),elem_no)
                do bnd_elem_counter2=1,no_bnd_elem_per_vertex(vertex2)
                  test_bnd_elem2 = vertex_to_bnd_elem_array(bnd_elem_counter2,vertex2)
                  if (test_bnd_elem2 == test_bnd_elem) found_in_this_vertex = .true.
                enddo
                if (.not.found_in_this_vertex) then
                  bnd_elem_found = .false.
                  cycle bnd_elem_loop
                endif
              enddo
              
              if (bnd_elem_found) then
                bnd_elem = test_bnd_elem
                exit bnd_elem_loop
              endif
            enddo bnd_elem_loop
            
            ! Regular boundary element
            if (.not. mesh_is_periodic) then
              face_to_face_array(elem_no,face,1) = 0
              face_to_face_array(elem_no,face,2) = bnd_elem
              face_to_face_array(elem_no,face,3) = free_face
              free_face = free_face+1
            endif
            
            ! Primary periodic or regular boundary element
            if (mesh_is_periodic) then

              ! get if the face is a primary or secondary
              face_is_primary = .false.
              face_is_secondary = .false.
              face_periodic_dir = 0
              do i=1,no_periodic_directions
                if (bnd_info(bnd_elem) == primary_periodic_face_ids(i)) then
                  face_is_primary = .true.
                  face_periodic_dir = i
                endif
                if (bnd_info(bnd_elem) == secondary_periodic_face_ids(i)) then
                  face_is_secondary = .true.
                  face_periodic_dir = i
                endif
              enddo

              if (.not.face_is_secondary) then
                if (.not.face_is_primary) then
                  face_to_face_array(elem_no,face,1) = 0
                  face_to_face_array(elem_no,face,2) = bnd_elem
                  face_to_face_array(elem_no,face,3) = free_face
                  free_face = free_face+1
                else
                  face_to_face_array(elem_no,face,1) = 1 ! avoid the 0 that indicates the boundary
                  face_to_face_array(elem_no,face,2) = bnd_elem
                  face_to_face_array(elem_no,face,3) = free_face
                  global_face_is_periodic(free_face) = .true.
                  global_face_periodic_dir(free_face) = face_periodic_dir
                  global_face_periodic_bc_id(free_face) = bnd_info(bnd_elem)
                  free_face = free_face+1
                endif
              endif
            endif
          endif
        endif
      enddo
    enddo
    no_unique_faces = free_face-1
    if (I_am_master_process()) print *,'no_unique_faces', no_unique_faces
    
    ! Loop bnd elements. If they are primary periodic then seek the opposite periodic face

    if (mesh_is_periodic) then
      if (.not.allocated(periodicity_shift)) allocate(periodicity_shift(mesh%dimen,no_periodic_directions))     
      do bnd_elem=1,no_bnd_elem

        ! get if the face is a primary or secondary
        face_is_primary = .false.
        face_is_secondary = .false.
        do i=1,no_periodic_directions
          if (bnd_info(bnd_elem) == primary_periodic_face_ids(i)) then
            face_is_primary = .true.
            per_dir_no = i
          endif
          if (bnd_info(bnd_elem) == secondary_periodic_face_ids(i)) then
            face_is_secondary = .true.
            per_dir_no = i
          endif
        enddo

        ! check if it is a primary periodic
        if (face_is_primary) then
          no_vert_bnd_elem = no_vertices(eltype_dl_bnd(bnd_elem))
          
          vertex_prim_no = vertex_connect_dl_bnd(1,bnd_elem)
          vertex_prim = vertex_ptr_array(vertex_prim_no)%point
          
          ! Get the opposite node in the periodic mesh
          call get_opposite_node_periodic_mesh(periodic_dirs(per_dir_no), secondary_periodic_face_ids(per_dir_no), &
              vertex_prim, vertex_ptr_array, bnd_info, &
              eltype_dl_bnd, vertex_connect_dl_bnd, no_bnd_elem, vertex_sec_no, &
              shift_vector)         
          
          periodicity_shift(:,per_dir_no) = shift_vector(:mesh%dimen) 
 
          ! Get the opposite bnd_elem
          call get_opposite_bnd_elem(bnd_elem,vertex_sec_no,shift_vector,no_bnd_elem_per_vertex,vertex_to_bnd_elem_array, &
                                     eltype_dl_bnd,vertex_connect_dl_bnd,vertex_ptr_array,bnd_elem_sec_no)
          
          ! We know that bnd_elem is on the primary side and bnd_elem_sec_no is on the opposite side
          ! Get the corresponding internal elements and the face
          call get_elem_and_face_no(bnd_elem,eltype_dl,eltype_dl_bnd,no_elem_per_vertex,vertex_to_elem_array, &
                                    vertex_connect_dl,vertex_connect_dl_bnd,elem_no,face_no)
          call get_elem_and_face_no(bnd_elem_sec_no,eltype_dl,eltype_dl_bnd,no_elem_per_vertex,vertex_to_elem_array, &
                                    vertex_connect_dl,vertex_connect_dl_bnd,elem_sec_no,face_sec_no)
          
          ! Now adapt the face_to_face_array with these data
          free_face = face_to_face_array(elem_no,face_no,3)
          face_to_face_array(elem_no,    face_no,    1) = elem_sec_no
          face_to_face_array(elem_no,    face_no,    2) = face_sec_no
          face_to_face_array(elem_sec_no,face_sec_no,1) = elem_no
          face_to_face_array(elem_sec_no,face_sec_no,2) = face_no
          face_to_face_array(elem_sec_no,face_sec_no,3) = free_face
        endif
      enddo
    endif
    
    ! Setup some primary information of the faces
    ! the head is a non-existent element with no_unique_faces children
    allocate(mesh%face_list_head)
    mesh%face_list_head%level = -1  ! level of head is -1
    mesh%face_list_head%active = .false.
    mesh%face_list_head%ftype = UNINITIALIZED_INT
    call assert(no_unique_faces /= 0,'read_gmsh_mesh: no_unique_faces == 0')
    allocate(mesh%face_list_head%children(no_unique_faces))
    do face = 1, no_unique_faces
      allocate(mesh%face_list_head%children(face)%point)
      mesh%face_list_ptr => mesh%face_list_head%children(face)%point ! find the child ptr
      mesh%face_list_head%children(face)%point => mesh%face_list_ptr ! ptr from mother to child
      mesh%face_list_ptr%mother => mesh%face_list_head               ! ptr from child to mother
      mesh%face_list_ptr%level = 0                                   ! set level to zero for face
      mesh%face_list_ptr%active = .true.                             ! active face
      nullify(mesh%face_list_ptr)
    enddo
 
    ! Set the faces as periodic when they are
    ! Also set the bc_id to what was stored in global_face_periodic_bc_id
    do global_face = 1, no_unique_faces
      mesh%face_list_ptr => mesh%face_list_head%children(global_face)%point
      mesh%face_list_ptr%periodic_id = global_face_periodic_dir(global_face)
      if (global_face_is_periodic(global_face)) then
        mesh%face_list_ptr%is_periodic = .true.
        mesh%face_list_ptr%bc_id = global_face_periodic_bc_id(global_face)
      endif
    enddo
    
    ! Transfer the face_to_face_array information to the
    ! element -> face pointers and the face -> element pointers
    do elem_no = 1, no_elem
      no_elem_faces=no_faces_of_elem(mesh%elem_list_head%children(elem_no)%point%eltype)
      mesh%elem_list_ptr => mesh%elem_list_head%children(elem_no)%point
      allocate(mesh%elem_list_ptr%faces(no_elem_faces))
      call assert(allocated(mesh%face_list_head%children),'read_gmsh_mesh: mesh%face_list_head%children not allocated')
      do face = 1, no_elem_faces
        global_face = face_to_face_array(elem_no,face,3)
        call assert(global_face > 0,'global_face cannot be a negative index')
        mesh%face_list_ptr => mesh%face_list_head%children(global_face)%point
        mesh%elem_list_ptr%faces(face)%point => mesh%face_list_ptr  ! point to global face
        
        ! face is on the boundary
        if (face_to_face_array(elem_no,face,1) == 0) then
          mesh%face_list_ptr%ftype = BOUNDARY
          mesh%face_list_ptr%bc_id = bnd_info(face_to_face_array(elem_no,face,2))
          allocate(mesh%face_list_ptr%elem_neighbors(1))
          mesh%face_list_ptr%elem_neighbors(1)%point => mesh%elem_list_ptr ! point to neighbor
          mesh%face_list_ptr%neighbor_loc_face_no(1) = face                ! and its local face
          
        ! face is NOT on the boundary
        else
          mesh%face_list_ptr%ftype = INTERNAL
          if (.not. allocated(mesh%face_list_ptr%elem_neighbors)) allocate(mesh%face_list_ptr%elem_neighbors(2))
          if (.not. associated(mesh%face_list_ptr%elem_neighbors(1)%point)) then
            allocate(mesh%face_list_ptr%elem_neighbors(1)%point)
            mesh%face_list_ptr%elem_neighbors(1)%point => mesh%elem_list_ptr
            mesh%face_list_ptr%neighbor_loc_face_no(1) = face
          else
            allocate(mesh%face_list_ptr%elem_neighbors(2)%point)
            mesh%face_list_ptr%elem_neighbors(2)%point => mesh%elem_list_ptr
            mesh%face_list_ptr%neighbor_loc_face_no(2) = face
          endif
          
          ! Determine if the face is an interface with an element to be ignored,
          ! or if it is an interface between solid and fluid.
          ! In this cases, it must be treated as a boundary face, with an ID.
          if (associated(mesh%face_list_ptr%elem_neighbors(1)%point) .and. &
              associated(mesh%face_list_ptr%elem_neighbors(2)%point)) then
            associate(nghb1 => mesh%face_list_ptr%elem_neighbors(1)%point,&
                      nghb2 => mesh%face_list_ptr%elem_neighbors(2)%point  )
              if (nghb1%is_to_be_ignored() .neqv. nghb2%is_to_be_ignored()) then
                mesh%face_list_ptr%ftype = INTERFACE_WITH_IGNORED
                mesh%face_list_ptr%bc_id = INTERFACE_WITH_IGNORED_ELEM_ID
              elseif (nghb1%is_fluid() .neqv. nghb2%is_fluid()) then
                mesh%face_list_ptr%ftype = INTERFACE_SOLID_FLUID
                mesh%face_list_ptr%bc_id = INTERFACE_SOLID_FLUID_ID
              endif
            end associate
          endif
        endif
        nullify(mesh%face_list_ptr)
      enddo
      nullify(mesh%elem_list_ptr)
    enddo
    if (I_am_master_process()) print *,'Done reading gmsh file'
  end subroutine read_gmsh_mesh
  
  subroutine get_opposite_node_periodic_mesh(periodic_dir,opposite_periodic_face_id,&
      vertex_prim,vertex_ptr_array,bnd_info,eltype_dl_bnd,vertex_connect_dl_bnd,no_bnd_elem,vertex_sec_no,shift_vector)
    use mesh_objects, only: vertex_list, vertex_ptr
    use mesh_type, only: fem_mesh
    use local_elem, only: no_vertices
    
    integer, intent(in) :: periodic_dir
    integer, intent(in) :: opposite_periodic_face_id
    type(vertex_list) :: vertex_prim
    type(vertex_ptr), pointer :: vertex_ptr_array(:)
    integer, allocatable, dimension(:) :: bnd_info
    integer, allocatable, dimension(:) :: eltype_dl_bnd
    integer, allocatable, dimension(:,:) :: vertex_connect_dl_bnd
    integer, intent(in) :: no_bnd_elem
    integer, intent(out) :: vertex_sec_no
    real(dp), intent(out) :: shift_vector(:)
    
    integer :: nonperiodic_dirs(2)
    integer :: bnd_elem_sec_no
    integer :: loc_vertex
    integer :: no_vert_bnd_elem_sec
    real(dp) :: xyz_prim(3), xyz_sec(3), nonper_prim(2), nonper_sec(2)
    type(vertex_list) :: vertex_sec
    
    call assert(size(shift_vector) == 3,'get_opposite_node_periodic_mesh: expected size-3 shift_vector')
    
    select case(periodic_dir)
      case(1);        nonperiodic_dirs = [2,3]
      case(2);        nonperiodic_dirs = [1,3]
      case(3);        nonperiodic_dirs = [1,2]
      case default;   error stop 'ERROR: bad periodic_dir'
    end select
    
    xyz_prim = vertex_prim%coordinate%xyz
    nonper_prim = xyz_prim(nonperiodic_dirs)

    do bnd_elem_sec_no = 1, no_bnd_elem
      if (bnd_info(bnd_elem_sec_no) /= opposite_periodic_face_id) cycle

      no_vert_bnd_elem_sec = no_vertices(eltype_dl_bnd(bnd_elem_sec_no))
      do loc_vertex = 1, no_vert_bnd_elem_sec
        vertex_sec_no = vertex_connect_dl_bnd(loc_vertex,bnd_elem_sec_no)
        vertex_sec = vertex_ptr_array(vertex_sec_no)%point
        xyz_sec = vertex_sec%coordinate%xyz
        nonper_sec = xyz_sec(nonperiodic_dirs)
        if (all(iseq(nonper_prim,nonper_sec,tolerance=mesh_periodic_equiv_tol))) then
          shift_vector = xyz_sec - xyz_prim
          return
        endif
      enddo
    enddo

    call error('get_opposite_node_periodic_mesh: did not find shift_vector')
  end subroutine get_opposite_node_periodic_mesh
  
  subroutine get_opposite_bnd_elem(bnd_elem_no,vertex_sec_no,shift_vector,no_bnd_elem_per_vertex,vertex_to_bnd_elem_array, &
                                   eltype_dl_bnd,vertex_connect_dl_bnd,vertex_ptr_array,opposite_bnd_elem_no)
    use local_elem, only: no_vertices
    use mesh_objects, only: vertex_list, vertex_ptr
    use mesh_type, only: fem_mesh
    
    integer, intent(in) :: bnd_elem_no
    integer, intent(in) :: vertex_sec_no
    real(dp), intent(in) :: shift_vector(:)
    integer, intent(in)  :: no_bnd_elem_per_vertex(:)
    integer, intent(in) :: vertex_to_bnd_elem_array(:,:)
    integer, intent(in) :: eltype_dl_bnd(:)
    integer, intent(in) :: vertex_connect_dl_bnd(:,:)
    type(vertex_ptr), intent(in) :: vertex_ptr_array(:)
    integer, intent(out) :: opposite_bnd_elem_no
    
    logical :: vertex_found
    integer :: face_vertex, face_vertex2
    integer :: vertex_sec_test_no
    integer :: bnd_elem_counter
    integer :: test_bnd_elem_no
    integer :: vertex_prim_no
    integer :: no_vert_bnd_elem
    real(dp) :: xyz_sec(3), xyz_prim(3), xyz_sec_test(3)
    type(vertex_list) :: vertex_prim ,vertex_sec_test
    
    call assert(size(shift_vector) == 3,'shift_vector should have size 3')
    call assert_normal(shift_vector,'something is wrong with the shift vector')
    
    bnd_elem_loop: do bnd_elem_counter = 1, no_bnd_elem_per_vertex(vertex_sec_no)
      test_bnd_elem_no = vertex_to_bnd_elem_array(bnd_elem_counter,vertex_sec_no)
      no_vert_bnd_elem = no_vertices(eltype_dl_bnd(bnd_elem_no))

      do face_vertex = 1, no_vert_bnd_elem ! loop over primary vertices
        vertex_prim_no = vertex_connect_dl_bnd(face_vertex,bnd_elem_no)
        vertex_prim = vertex_ptr_array(vertex_prim_no)%point
        xyz_prim = vertex_prim%coordinate%xyz
        xyz_sec = xyz_prim + shift_vector
        
        ! test all vertices of test_bnd_elem
        vertex_found = .false.
        do face_vertex2 = 1, no_vert_bnd_elem
          vertex_sec_test_no = vertex_connect_dl_bnd(face_vertex2,test_bnd_elem_no)
          vertex_sec_test = vertex_ptr_array(vertex_sec_test_no)%point
          xyz_sec_test = vertex_sec_test%coordinate%xyz
          vertex_found = all(iseq(xyz_sec,xyz_sec_test,tolerance=mesh_periodic_equiv_tol))
          if (vertex_found) exit
        enddo
        
        if (.not. vertex_found) cycle bnd_elem_loop
      enddo

      opposite_bnd_elem_no = test_bnd_elem_no
      return
    enddo bnd_elem_loop

    call error('get_opposite_bnd_elem: did not find elem')
  end subroutine get_opposite_bnd_elem
  
  subroutine get_elem_and_face_no(bnd_elem_no,eltype_dl,eltype_dl_bnd,no_elem_per_vertex,vertex_to_elem_array, &
                                  vertex_connect_dl,vertex_connect_dl_bnd,elem_no,face_no)
    ! Procedure:
    ! 1. Get a vertex of the bnd_elem
    ! 2. Find all elements connected to this vertex
    ! 3. Loop over the elements and their faces and compare the vertices to those of the bnd_elem
    use mesh_objects, only: eltype_of_faces_of_elem, no_faces_of_elem
    use local_elem, only: loc_idx_of_face_vertices_in_elem, no_vertices
    
    integer, intent(in) :: bnd_elem_no
    integer, intent(in) :: eltype_dl(:), eltype_dl_bnd(:)
    integer, intent(in) :: no_elem_per_vertex(:)
    integer, intent(in) :: vertex_to_elem_array(:,:)
    integer, intent(in) :: vertex_connect_dl(:,:), vertex_connect_dl_bnd(:,:)
    integer, intent(out) :: elem_no, face_no
    
    logical :: vertex_found
    logical :: all_vertices_found
    integer :: no_face_vertices
    integer :: bnd_vertex1
    integer :: bnd_vertex
    integer :: face_vertex_no
    integer :: vertex
    integer :: no_elem_faces
    integer :: no_vert_bnd_elem
    integer :: bnd_vertex_no
    integer :: elem_counter
    integer, allocatable, dimension(:) :: face_vertices

    call assert(1 <= bnd_elem_no .and. bnd_elem_no <= size(eltype_dl_bnd),  &
        "get_elem_and_face_no: bad index of 'eltype_dl_bnd': bnd_elem_no=" // int2char(bnd_elem_no) )
    
    no_vert_bnd_elem = no_vertices(eltype_dl_bnd(bnd_elem_no))
    
    ! first vertex of the bound_vertices element
    bnd_vertex1 = vertex_connect_dl_bnd(1,bnd_elem_no)
    
    elem_loop: do elem_counter = 1, no_elem_per_vertex(bnd_vertex1)
      elem_no = vertex_to_elem_array(elem_counter,bnd_vertex1)
      
      no_elem_faces = no_faces_of_elem(eltype_dl(elem_no))
      Y: do face_no = 1, no_elem_faces
        no_face_vertices = no_faces_of_elem(eltype_of_faces_of_elem(eltype_dl(elem_no)))
        if (allocated(face_vertices)) deallocate(face_vertices)
        allocate(face_vertices(no_face_vertices))
        call loc_idx_of_face_vertices_in_elem(eltype_dl(elem_no),face_no,face_vertices)
        
        all_vertices_found = .true.
        do face_vertex_no = 1, no_face_vertices
          vertex = vertex_connect_dl(face_vertices(face_vertex_no),elem_no)
          vertex_found = .false.
          do bnd_vertex_no = 1, no_vert_bnd_elem
            bnd_vertex = vertex_connect_dl_bnd(bnd_vertex_no,bnd_elem_no)
            if (bnd_vertex == vertex) vertex_found = .true.
          enddo
          ! try another face
          if (.not.vertex_found) then
            all_vertices_found = .false.
            cycle Y
          endif
        enddo
        if (all_vertices_found) then
          return
        else
          STOP 'get_elem_and_face_no: elem/face not found'
        endif
      enddo Y
    enddo elem_loop
  end subroutine get_elem_and_face_no
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine verify_bc_mat_ids(mesh)
    use boundaries, only: all_specified_bnd_ids, periodicity_specs
    use matprop_mod, only: all_specified_mat_ids
    use mesh_type, only: fem_mesh
    use run_data, only: ignore_elems_with_mat_id
    
    type(fem_mesh), intent(in) :: mesh
    
    integer, allocatable :: mat_ids(:), spec_mat_ids(:)
    integer, allocatable :: bnd_ids(:), spec_bnd_ids(:), temp(:)
    logical :: is_periodic
    integer, allocatable, dimension(:) :: prim_id, sec_id
    integer, allocatable, dimension(:) :: periodic_dir
    logical :: matching_mat_ids, matching_bnd_ids
    character(1000) :: msg
    integer :: i
     
    call mesh%get_all_ids(mat_ids,bnd_ids)
    spec_mat_ids = all_specified_mat_ids()
    spec_bnd_ids = all_specified_bnd_ids()
    
    ! Remove from the list of specified bnd_ids those related to periodic faces, which
    ! are not treated as boundary faces by the code
    call periodicity_specs(is_periodic,prim_id,sec_id,periodic_dir)
    if (is_periodic) then
      do i = 1, size(prim_id)
        temp = pack(spec_bnd_ids,spec_bnd_ids /= prim_id(i) .and. spec_bnd_ids /= sec_id(i))
        call move_alloc(temp,spec_bnd_ids)
      enddo
    endif
    ! Remove from the list of mesh mat_ids those to be ignored during calculations
    do i = 1, size(ignore_elems_with_mat_id)
      temp = pack(mat_ids, mat_ids /= ignore_elems_with_mat_id(i))
      call move_alloc(temp,mat_ids)
    enddo
    
    matching_mat_ids = contain_same_numbers(mat_ids,spec_mat_ids)
    matching_bnd_ids = contain_same_numbers(bnd_ids,spec_bnd_ids)
    if (.not. matching_bnd_ids) then
      write(msg,*) '[',bnd_ids,'] /= [',spec_bnd_ids,']'
      call error("mismatch in bnd ID's: " // new_line('a') // trim(msg))
    endif
    if (.not. matching_mat_ids) then
      write(msg,*) '[',mat_ids,'] /= [',spec_mat_ids,']'
      call error("mismatch in mat ID's: " // new_line('a') // trim(msg))
    endif
  end subroutine verify_bc_mat_ids
  
  logical function contain_same_numbers(list_1,list_2)
    use binary_tree, only: int_set
    
    integer, intent(in) :: list_1(:), list_2(:)
    
    type(int_set) :: set_1, set_2
    integer, allocatable :: sorted_1(:), sorted_2(:)
    
    call set_1%insert(list_1)
    call set_2%insert(list_2)
    call set_1%sorted_values(sorted_1)
    call set_2%sorted_values(sorted_2)
    if (size(sorted_1) /= size(sorted_2)) then
      contain_same_numbers = .false.
    else
      contain_same_numbers = all(sorted_1 == sorted_2)
    endif
  end function contain_same_numbers
end module read_gmsh_mesh_mod
