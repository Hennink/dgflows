module petsc_is_mod
#include "petsc/finclude/petscis.h"
  use petscis
  use assertions, only: assert, BOUNDS_CHECK
  implicit none
  
  private
  public :: petsc_is_t
  public :: petsc_Loc2Glob_map_t
  
  type :: petsc_is_t
    private
    type(tIS), public :: petsc_is
    logical, public :: initialized = .false.
  contains
    private
    final :: destroy_is
  end type petsc_is_t
  
  type :: petsc_Loc2Glob_map_t
    ! mapping from an arbitrary local ordering from 0 to n-1 to a global PETSc 
    ! ordering used by a vector or matrix.
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/IS/ISLocalToGlobalMapping.html#ISLocalToGlobalMapping
    private
    type(ISLocalToGlobalMapping), public :: mapping
    logical, public :: destroy_upon_finalization = .false.
  contains
    generic :: apply_Glob2toLoc_mapping => apply_Glob2toLoc_mapping__scalar, apply_Glob2toLoc_mapping__rank1
    procedure, non_overridable, private :: apply_Glob2toLoc_mapping__scalar, apply_Glob2toLoc_mapping__rank1
    final :: destroy
  end type petsc_Loc2Glob_map_t
  
contains
  impure elemental subroutine destroy_is(this)
    type(petsc_is_t), intent(inout) :: this
    
    PetscErrorCode :: ierr
    
    if (this%initialized) then
      call ISDestroy(this%petsc_is,ierr)
      call assert(ierr == 0,'ierr /= 0 in ISDestroy')
    endif
  end subroutine destroy_is
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine apply_Glob2toLoc_mapping__scalar(this,G_idx,L_idx)
    class(petsc_Loc2Glob_map_t), intent(in) :: this
    integer, intent(in) :: G_idx
    integer, intent(out) :: L_idx
    
    integer :: G_idx_array(1), L_idx_array(1)
    
    G_idx_array(1) = G_idx
    call this%apply_Glob2toLoc_mapping__rank1(G_idx_array,L_idx_array)
    L_idx = L_idx_array(1)
  end subroutine
  
  subroutine apply_Glob2toLoc_mapping__rank1(this,G_idxs,L_idxs)
    ! Provides the local numbering for a list of integers specified with a global numbering. 
    ! Not collective.
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/IS/ISGlobalToLocalMappingApply.html
    use petscis, only: mapping_mode => IS_GTOLM_MASK
    use petscsys, only: PETSC_NULL_INTEGER
    use assertions, only: assert_eq
    use petsc_mod, only: idx_fort2petsc, idx_petsc2fort
    
    class(petsc_Loc2Glob_map_t), intent(in) :: this
    integer, intent(in) :: G_idxs(:)
    integer, intent(out) :: L_idxs(:)
    
    PetscErrorCode :: ierr
    integer :: petsc_G_idxs(size(G_idxs))
    integer :: petsc_L_idxs(size(L_idxs))
    integer :: no_idxs
    
    no_idxs = assert_eq(size(G_idxs),size(L_idxs),BOUNDS_CHECK)

    call idx_fort2petsc(G_idxs,petsc_G_idxs)
    call ISGlobalToLocalMappingApply(this%mapping,mapping_mode,no_idxs,petsc_G_idxs,PETSC_NULL_INTEGER(1),petsc_L_idxs,ierr)
    call assert(ierr == 0,'ierr /= 0 in ISGlobalToLocalMappingApply')
    call assert(petsc_L_idxs /= -1,'One or more provided global indexes do not map into a local index')
    call idx_petsc2fort(petsc_L_idxs,L_idxs)
  end subroutine
  
  subroutine destroy(this)
    ! Destroys a mapping between a local (0 to n) ordering and a global parallel ordering
    !
    ! Not Collective 
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/IS/ISLocalToGlobalMappingDestroy.html
    type(petsc_Loc2Glob_map_t), intent(inout) :: this
    
    PetscErrorCode :: ierr
    
    if (this%destroy_upon_finalization) then
      call ISLocalToGlobalMappingDestroy(this%mapping,ierr)
      call assert(ierr == 0,'ierr /= 0 in ISLocalToGlobalMappingDestroy')
    endif
  end subroutine destroy
end module petsc_is_mod
