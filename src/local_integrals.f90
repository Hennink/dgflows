module local_integrals
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use local_elem, only: get_quadrature, no_dg_functions
  use mesh_objects, only: elem_list, face_list, side_of_internal_nghb_of_bnd_face
  implicit none
  
  private
  public :: int_fi_x_func_on_elem
  public :: int_fi_fj_on_elem
  public :: int_grad_fi_x_fj_on_elem
  public :: int_fEin_x_vecfunc_dot_n_on_bnd_face
  public :: integrals_fi_x_fj_x_n_on_face
  
  interface int_fi_x_func_on_elem
    module procedure :: int_fi_x_scalfunc_on_elem
    module procedure :: int_fi_x_vecfunc_on_elem
  end interface int_fi_x_func_on_elem
  
contains
  ! ============== ALL ELEM INTEGRALS: ================
  
  subroutine int_fi_x_scalfunc_on_elem(elem,order,scal_func,time,elem_int_fi_x_scalfunc)
    use scal_func_mod, only: scal_func_type

    type(elem_list), intent(in) :: elem
    integer, intent(in) :: order
    class(scal_func_type), intent(in) :: scal_func
    integer, intent(in) :: time
    real(dp), allocatable, dimension(:), intent(out) :: elem_int_fi_x_scalfunc
    
    integer :: nnod
    real(dp) :: scalfunc_x_qout(elem%nqp)
    
    nnod = no_dg_functions(elem%eltype,order)
    scalfunc_x_qout = scal_func%values_at_elem_qps(elem,time) * elem%quot
    allocate(elem_int_fi_x_scalfunc(nnod))
    elem_int_fi_x_scalfunc = matmul(elem%quad_points%dg_funcs(:nnod,:),scalfunc_x_qout)
  end subroutine int_fi_x_scalfunc_on_elem
  
  subroutine int_fi_x_vecfunc_on_elem(elem,order,vec_func,time,elem_int_fi_x_vecfunc)
    use vec_func_mod, only: vec_func_type
    
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: order
    class(vec_func_type), intent(in) :: vec_func
    integer, intent(in) :: time
    real(dp), allocatable, dimension(:,:), intent(out) :: elem_int_fi_x_vecfunc
    
    integer :: i, nnod
    real(dp), allocatable :: vec_func_at_qps(:,:)
    integer :: dir, ndirs
    integer :: qp
    
    ndirs = vec_func%no_dirs()
    nnod = no_dg_functions(elem%eltype, order)
    
    allocate(vec_func_at_qps(ndirs,elem%nqp))
    call vec_func%values_at_qps(elem,time,vec_func_at_qps)
    
    allocate(elem_int_fi_x_vecfunc(nnod,ndirs), source=0.0_dp)
    do qp = 1, elem%nqp
      do dir = 1, ndirs
        do i = 1, nnod
          elem_int_fi_x_vecfunc(i,dir) = elem_int_fi_x_vecfunc(i,dir) + &
              elem%quad_points%dg_funcs(i,qp) * vec_func_at_qps(dir,qp) * elem%quot(qp)
        enddo
      enddo
    enddo
  end subroutine int_fi_x_vecfunc_on_elem
  
  subroutine int_fi_fj_on_elem(elem,order,elem_int_fi_fj)
    ! Computes the mass matrix for all functions up to 'order'.
    !
    ! This does not use the faster precalculated quarature. set in 'elem',
    ! because that does not store all higher-order functions.
    use array_opers_mod, only: add_Aik_Bjk_wk
    use local_global_mappings, only: global_dg_functions
    
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: order
    real(dp), allocatable, dimension(:,:), intent(out) :: elem_int_fi_fj
    
    real(dp), allocatable :: func(:,:), det_jac(:), lcors(:,:), local_weights(:), global_weights(:)
    integer :: qp
    
    call get_quadrature(elem%eltype,2*order,lcors,local_weights)
    call global_dg_functions(elem,order,lcors,func=func,det_jac=det_jac)
    allocate(global_weights,source = local_weights * abs(det_jac))
    allocate(elem_int_fi_fj(size(func,1),size(func,1)), source=0.0_dp)
    call add_Aik_Bjk_wk(elem_int_fi_fj, func, func, global_weights, 'loop')
  end subroutine
  
  pure subroutine int_grad_fi_x_fj_on_elem(elem,order_i,order_j,elem_int_grad_fi_x_fj)
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: order_i, order_j
    real(dp), allocatable, dimension(:,:,:), intent(out) :: elem_int_grad_fi_x_fj
    
    integer :: no_nodes_i, no_nodes_j
    integer :: qp
    integer :: i, j
    integer :: d, dimen
    
    no_nodes_i = no_dg_functions(elem%eltype, order_i)
    no_nodes_j = no_dg_functions(elem%eltype, order_j)
    dimen = elem%dimen()
    
    allocate(elem_int_grad_fi_x_fj(no_nodes_i,no_nodes_j,dimen))
    elem_int_grad_fi_x_fj = 0.0_dp
    do qp = 1, elem%nqp
      do d = 1, dimen
        do j = 1, no_nodes_j
          do i = 1, no_nodes_i
            elem_int_grad_fi_x_fj(i,j,d) = elem_int_grad_fi_x_fj(i,j,d) &
                + elem%quad_points%dg_transp_gders(i,d,qp) * elem%quad_points%dg_funcs(j,qp) * elem%quot(qp)
          enddo
        enddo
      enddo
    enddo
  end subroutine int_grad_fi_x_fj_on_elem
  
  
  ! ============== ALL FACE INTEGRALS: ================
  ! ==(used only in 'pressure' and 'pressure__init')===
  
  subroutine int_fEin_x_vecfunc_dot_n_on_bnd_face(bnd_face,order,vec_func,time,integral)
    use run_data, only: mesh_dimen
    use vec_func_mod, only: vec_func_type
    
    type(face_list), intent(in) :: bnd_face
    integer, intent(in) :: order
    class(vec_func_type), intent(in) :: vec_func
    integer, intent(in) :: time
    real(dp), allocatable, dimension(:), intent(out) :: integral
    
    logical, parameter :: only_fluid = .true. !the integral is only used for pressure calculations!
    integer :: side_Ein
    type(elem_list), pointer :: Ein
    integer :: nnod, i, dimen, d, p, nqp
    real(dp) :: vecfunc_at_qps(mesh_dimen,bnd_face%nqp)
    
    side_Ein = side_of_internal_nghb_of_bnd_face(bnd_face, only_fluid)
    call assert(side_Ein == 1 .or. side_Ein == 2, 'int_fEin_x_vecfunc_dot_n_on_bnd_face: weird `side_Ein`', BOUNDS_CHECK)
    Ein => bnd_face%elem_neighbors(side_Ein)%point
    nnod = no_dg_functions(Ein%eltype,order)
    dimen = assert_eq(mesh_dimen, vec_func%no_dirs())
    
    call vec_func%get_values(Ein, bnd_face%quad_points(side_Ein), time, vecfunc_at_qps)
    allocate(integral(nnod), source=0.0_dp)
    associate(fEin => bnd_face%quad_points(side_Ein)%dg_funcs)
      nqp = assert_eq(bnd_face%nqp, size(fEin,2), size(bnd_face%normal,2), size(bnd_face%quot), BOUNDS_CHECK)
      do p = 1, nqp
        do d = 1, dimen
          do i = 1, nnod
            integral(i) = integral(i) &
                + fEin(i,p) * vecfunc_at_qps(d,p) * bnd_face%normal(d,p) * bnd_face%quot(p)
          enddo
        enddo
      enddo
    end associate
    if (side_Ein == 2) integral = - integral
  end subroutine
  
  subroutine integrals_fi_x_fj_x_n_on_face(Ein_is_E1,boundary_face,face,fi_x_fj_x_n_Ein_Ein,fi_x_fj_x_n_Ein_Eout)
    logical, intent(in) :: Ein_is_E1
    logical, intent(in) :: boundary_face
    type(face_list), target, intent(in) :: face
    real(dp), allocatable, dimension(:,:,:), intent(out) :: fi_x_fj_x_n_Ein_Ein
    real(dp), allocatable, dimension(:,:,:), intent(out) :: fi_x_fj_x_n_Ein_Eout
    
    integer :: side_Ein, side_Eout
    integer :: no_nod_Ein, no_nod_Eout
    real(dp), pointer :: func_Ein(:,:) => null(), func_Eout(:,:) => null()
    real(dp), dimension(:,:), allocatable :: normal
    integer :: qp
    integer :: i,j,dim
    integer :: dimen
    
    if (Ein_is_E1) then
      side_Ein = 1
      side_Eout = 2
    else
      side_Ein = 2
      side_Eout = 1
    endif
    func_Ein => face%quad_points(side_Ein)%dg_funcs
    if (.not. boundary_face) func_Eout => face%quad_points(side_Eout)%dg_funcs
    if (Ein_is_E1) then
      allocate(normal,source=face%normal)
    else
      allocate(normal,source=-face%normal)
    endif
    dimen = size(normal,1)
    no_nod_Ein = size(func_Ein,1)
    if (.not. boundary_face) no_nod_Eout = size(func_Eout,1)
    
    allocate(fi_x_fj_x_n_Ein_Ein(no_nod_Ein,no_nod_Ein,dimen),source=0.0_dp)
    if (.not. boundary_face) allocate(fi_x_fj_x_n_Ein_Eout(no_nod_Ein,no_nod_Eout,dimen),source=0.0_dp)
    do qp = 1, face%nqp
      do dim = 1, dimen
        do j = 1, no_nod_Ein
          do i = 1, no_nod_Ein
            fi_x_fj_x_n_Ein_Ein(i,j,dim) = fi_x_fj_x_n_Ein_Ein(i,j,dim) +&
                                           func_Ein(i,qp)*func_Ein(j,qp)*normal(dim,qp)*face%quot(qp)
          enddo
        enddo
        if (.not. boundary_face) then
          do j = 1, no_nod_Eout
            do i = 1, no_nod_Ein
              fi_x_fj_x_n_Ein_Eout(i,j,dim) = fi_x_fj_x_n_Ein_Eout(i,j,dim) +&
                                              func_Ein(i,qp)*func_Eout(j,qp)*normal(dim,qp)*face%quot(qp)
            enddo
          enddo
        endif
      enddo
    enddo
  end subroutine integrals_fi_x_fj_x_n_on_face
end module local_integrals

