submodule(boundary_types) boundary_types__noslip_fixedT_wall
  use func_set_mod, only: func_set_t
  implicit none
  
  type, extends(wall_bnd_t) :: noslip_fixedT_wall_t
    type(func_set_t) :: fixed_values
  contains
    procedure :: read_specs_from_file
    procedure :: get_bc
  end type noslip_fixedT_wall_t
  
contains
  module procedure noslip_fixedT_wall
    allocate(noslip_fixedT_wall_t :: bnd)
  end procedure noslip_fixedT_wall
  
  subroutine read_specs_from_file(this,unit_number)
    use read_funcs_mod, only: read_block_as_func_specs
    use matprop_mod, only: expand_func_set_with_matprop
    
    class(noslip_fixedT_wall_t), intent(out) :: this
    integer, intent(in) :: unit_number
    
    call read_block_as_func_specs(unit_number,this%fixed_values)
    call expand_func_set_with_matprop(this%fixed_values)
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use assertions, only: assert
    use boundary_condition_types, only: bc_type, dirichlet_bc_type, hom_neumann_bc_type
    use functions_lib, only: zero_vecfunc
    use rans_mod, only: rans_model
    
    class(noslip_fixedT_wall_t), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    
    type(dirichlet_bc_type), allocatable :: dirichlet_bc
    
    select case(qnty_name)
      case('vol_turb1','vol_turb2')
        if (rans_model%is_low_Re()) then
          allocate(dirichlet_bc)
          allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(1))
          call move_alloc(dirichlet_bc,bc)
        else
          call assert(.not. this%fixed_values%has(qnty_name))
          allocate(bc,source=hom_neumann_bc_type())
        endif
      case('wall_dist')
        allocate(dirichlet_bc)
        allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(1))
        call move_alloc(dirichlet_bc,bc)
      case default
        allocate(dirichlet_bc)
        call this%fixed_values%get_func(qnty_name,dirichlet_bc%inhom_part)
        call move_alloc(dirichlet_bc,bc)
    end select
  end subroutine get_bc
end submodule boundary_types__noslip_fixedT_wall

