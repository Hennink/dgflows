submodule(matprop_mod) matprop__lib_boussinesq
  use f90_kind, only: dp
  implicit none
  
  ! The properties have been set starting from the ones of air, in order to:
  ! - have a Prandtl number of 0.71 (used in all reference papers).
  ! - have a Rayleigh number equal to 10^3,10^4, etc (but it depends on gravity vector and temperature)
  ! - have a high thermal diffusivity(~10^-1) , to have reasonably fast transients
  real(dp), parameter :: density = 1.0_dp
  real(dp), parameter :: prandtl = 0.71_dp
  real(dp), parameter :: dynamic_viscosity = 18.15e-2_dp
  real(dp), parameter :: thermal_expansion_coeff = 4.64_dp
  real(dp), parameter :: spec_heat_capacity = 100.7_dp
  
  real(dp), parameter :: kinematic_viscosity = dynamic_viscosity/density
  real(dp), parameter :: thermal_diffusivity = kinematic_viscosity/prandtl
  real(dp), parameter :: thermal_conductivity = thermal_diffusivity*density*spec_heat_capacity
  real(dp), parameter :: vol_heat_capacity = density * spec_heat_capacity ! neglect thermal expansion
  
contains
  module subroutine get_boussinesq_matprop(matprop_set)
    use func_set_expansion_mod, only: func_set_expansion_t
    use functions_lib, only: const_func
    
    type(func_set_expansion_t), intent(out) :: matprop_set

    call matprop_set%add_known_function('thermal_expansion_coeff',const_func(thermal_expansion_coeff))
    call matprop_set%add_known_function('density',const_func(density))
    call matprop_set%add_known_function('thermal_conductivity',const_func(thermal_conductivity))
    call matprop_set%add_known_function('thermal_diffusivity',const_func(thermal_diffusivity))
    call matprop_set%add_known_function('kinematic_viscosity',const_func(kinematic_viscosity))
    call matprop_set%add_known_function('spec_heat_capacity',const_func(spec_heat_capacity))
    call matprop_set%add_known_function('vol_heat_capacity',const_func(vol_heat_capacity))
    call matprop_set%add_known_function('k_over_cp',const_func(thermal_conductivity/spec_heat_capacity))

    call matprop_set%add_func('temperature','spec_enthalpy',T2spec_h)
    call matprop_set%add_func('spec_enthalpy','temperature',spec_h2T)

    call matprop_set%finalize()
  end subroutine get_boussinesq_matprop
  
  pure real(dp) function T2spec_h(T) result(spec_h)
    use f90_kind, only: dp
    
    real(dp), intent(in) :: T
    
    spec_h = spec_heat_capacity * T
  end function T2spec_h

  pure real(dp) function spec_h2T(h) result(T)
    use f90_kind, only: dp

    real(dp), intent(in) :: h

    T = h / spec_heat_capacity
  end function spec_h2T
end submodule matprop__lib_boussinesq

