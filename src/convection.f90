module convection
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use exceptions, only: error
  use f90_kind, only: dp
  use pde_term_mod, only: pde_term_t
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only : vec_func_type
  implicit none
  
  private
  public :: conv_t
  
  type, extends(pde_term_t) :: conv_t
    private
    integer, public :: time_conv_field = 1 ! time at which to evaluate convecting field
    integer, public :: time_density = 1 ! time at which to evaluate density
    logical, public :: explicit
    logical, public :: LF_pointwise = .true.
    logical, public :: add_vol_div_corr
    logical, public :: add_face_jump_corr
    character(100), public :: convecting_qnty = 'velocity'
    logical :: corrected_convection = .false.
    class(scal_func_type), allocatable :: cc_coeff

  contains
    procedure, non_overridable :: ngbr_coupling_depth
    procedure, non_overridable :: process_elem, process_face
    procedure, non_overridable :: set_cc_coeff
    procedure, non_overridable, private :: n_dot_conv_field_at_bnd, &
                                           Lax_Friedrichs_flux, conservative_upwind_flux, &
                                           get_conv_field
  end type conv_t
  
contains
  pure integer function ngbr_coupling_depth(this)
    class(conv_t), intent(in) :: this

    ngbr_coupling_depth = merge(-1, 1, this%explicit)
  end function

  subroutine process_elem(this,elem,qnty,local_linsys)
    ! Volumetric convective term. Non-fluid elements are ignored.
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: elem_list
    use numerical_solutions, only: mass_flux, matprop_set
    use run_data, only: mesh_dimen
    use solvec_mod, only: solvec_t
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t
    
    class(conv_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys
    
    class(vec_func_type), allocatable :: beta
    class(scal_func_type), allocatable :: density
    real(dp) :: beta_at_qps(mesh_dimen,elem%nqp), quot_x_div_M(elem%nqp)
    type(unidir_lincom_rank0_t) :: beta_dot_gder, func
    
    if (.not. elem%is_fluid()) return
    
    call this%get_conv_field(beta)
    call beta%values_at_qps(elem,this%time_conv_field,beta_at_qps)
    call beta_dot_gder%set_as_vec_dot_gder(beta_at_qps,elem,qnty%handler)
    call func%set_as_func(elem,qnty%handler)
    
    ! - \int_E beta \cdot grad(v) \cdot Q
    call local_linsys%add_inprod_to_lhs_all_dirs(beta_dot_gder,func,elem%quot,this%explicit,-1.0_dp)
    
    if (this%add_vol_div_corr) then
      quot_x_div_M = elem%quot * mass_flux%div_at_qps(elem,this%time_conv_field)
      if (this%convecting_qnty == 'velocity') then
        ! - 0.5 * \int_E div(mass_flux) v_i (Q_i/rho)
        call matprop_set%get_func('density',density)
        quot_x_div_M = quot_x_div_M / density%values_at_elem_qps(elem,this%time_density)
      elseif (this%convecting_qnty == 'mass_flux') then
        ! - 0.5 * \int_E div(mass_flux) v_i q_i
      else
        call error('weird convecting_qnty='//trim(this%convecting_qnty ))
      endif
      call local_linsys%add_inprod_to_lhs_all_dirs(func,func,quot_x_div_M,this%explicit,-0.5_dp)
    endif
  end subroutine process_elem
  
  
  subroutine process_face(this,face,Ein_is_E1,qnty,local_linsys)
    ! Boundary convective terms. Non-fluid elements are ignored.
    use boundaries, only: get_bnd
    use boundary_condition_types
    use boundary_types, only: bnd_type
    use code_const, only: LAX_FRIEDRICHS, UPWIND_CONSERVATIVE
    use lincom_rank1_mod, only: lincom_rank1_t
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: face_list
    use numerical_solutions, only: conv_num_flux_chosen_for_qnty, mass_flux, matprop_set
    use run_data, only: mesh_dimen
    use solvec_mod, only: solvec_t
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t
    
    class(conv_t), intent(in) :: this
    type(face_list), intent(in) :: face
    logical, intent(in) :: Ein_is_E1
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys
    
    integer :: side_Ein, side_Eout, qp
    type(unidir_lincom_rank0_t) :: func_Ein, func_Eout
    type(lincom_rank1_t) :: alldir_func
    real(dp), allocatable, dimension(:,:) :: normal
    real(dp), dimension(face%nqp) :: beta_dot_n
    real(dp) :: quot_Ein_Ein(face%nqp), quot_Ein_Eout(face%nqp)
    real(dp) :: inflow_weights(face%nqp), outflow_weights(face%nqp)
    class(bnd_type), allocatable :: bnd
    class(bc_type), allocatable :: qnty_bc
    real(dp) :: qnty_dirichlet_at_qps(qnty%no_dirs(),face%nqp)
    class(scal_func_type), allocatable :: density
    real(dp) :: jump_M(mesh_dimen,face%nqp)
    
    if (Ein_is_E1) then
      side_Ein = 1; side_Eout = 2
      allocate(normal,source=face%normal)
    else
      side_Ein = 2; side_Eout = 1
      allocate(normal,source=-face%normal)
    endif
    associate(elem => face%elem_neighbors(side_Ein)%point)
      if (.not. elem%is_fluid()) return
    end associate
    
    if (face%is_boundary(only_fluid_mesh=.true.)) then
      ! Even if Q is defined on the whole domain, only bc's for U are of interest.
      call get_bnd(face%bc_id,bnd)
      call func_Ein%set_as_func(face,side_Ein,qnty%handler)
      ! We do point-by-point upwinding.
      beta_dot_n = this%n_dot_conv_field_at_bnd(face)
      ! All points with outflow:
      ! \int_F max(0,beta \cdot n)  v \cdot Q^+
      outflow_weights = face%quot * max(0.0_dp,beta_dot_n)
      call local_linsys%add_inprod_to_lhs_all_dirs(func_Ein,func_Ein,outflow_weights,this%explicit,1.0_dp)
      ! All points with inflow:
      if (any(beta_dot_n < 0.0_dp)) then
        inflow_weights = face%quot * min(0.0_dp,beta_dot_n)
        call bnd%get_bc(qnty%name,qnty_bc)
        select type(qnty_bc)
          type is (dirichlet_bc_type)
            call qnty_bc%inhom_part%values_at_qps(face,side_Ein,1,qnty_dirichlet_at_qps)
            ! - \int_F min(0,beta \cdot n)  v \cdot g_D
            call alldir_func%set_as_func(face,side_Ein,qnty%handler)
            call local_linsys%add_int_dotprod_to_rhs(alldir_func,qnty_dirichlet_at_qps,inflow_weights,-1.0_dp)
          type is (hom_neumann_bc_type)
            call local_linsys%add_inprod_to_lhs_all_dirs(func_Ein,func_Ein,inflow_weights,this%explicit,1.0_dp)
          type is (adjusted_traction_bc_t)
            call local_linsys%add_inprod_to_lhs_all_dirs(func_Ein,func_Ein,inflow_weights,this%explicit,1.0_dp)
          type is (generalized_neumann_bc_t)
            call local_linsys%add_inprod_to_lhs_all_dirs(func_Ein,func_Ein,inflow_weights,this%explicit,1.0_dp)
          class default
            call error('cannot handle bc. at inflow bnd; qnty='//qnty%name)
        end select
      endif
    else
      ! On internal faces, we compute:
      ! written as a sum over all elements T, with boundary dT, normal n_T, and inside '+',
      !     \int_T \int_{dT}  (v_i)^+ H_i,
      !
      ! where we choose an appropriate flux function for H_i.
      select case(conv_num_flux_chosen_for_qnty(qnty%name))
        case(LAX_FRIEDRICHS)
          call this%Lax_Friedrichs_flux(face,side_Ein,side_Eout,normal,qnty,quot_Ein_Ein,quot_Ein_Eout)
        case(UPWIND_CONSERVATIVE)
          call this%conservative_upwind_flux(face,side_Ein,side_Eout,normal,quot_Ein_Ein,quot_Ein_Eout)
        case default
          call error('convection: unknown numerical flux chosen for qnty.')
      end select
      call func_Ein%set_as_func(face,side_Ein,qnty%handler)
      call func_Eout%set_as_func(face,side_Eout,qnty%handler)
      call local_linsys%add_inprod_to_lhs_all_dirs(func_Ein,func_Ein,quot_Ein_Ein,this%explicit,1.0_dp)
      call local_linsys%add_inprod_to_lhs_all_dirs(func_Ein,func_Eout,quot_Ein_Eout,this%explicit,1.0_dp)
      
      
      if (this%add_face_jump_corr) then
        call mass_flux%jump_at_qps(face,.true.,this%time_conv_field,jump_M)
        if (.not. Ein_is_E1) jump_M = - jump_M
        do qp = 1, face%nqp
          quot_Ein_Ein(qp) = 0.5_dp * face%quot(qp) * dot_product(jump_M(:,qp),normal(:,qp))
        enddo
        if (this%convecting_qnty == 'velocity') then
          ! + 0.5 * \int_F ([M] \cdot n) {v_i (Q_i/rho)}
          call matprop_set%get_func('density',density)
          quot_Ein_Ein = quot_Ein_Ein / density%values_at_qps(face,side_Ein,this%time_density)
        elseif (this%convecting_qnty == 'mass_flux') then
          ! + 0.5 * \int_F ([M] \cdot n) {v_i q_i}
        else
          call error('weird convecting_qnty='//trim(this%convecting_qnty ))
        endif
        call local_linsys%add_inprod_to_lhs_all_dirs(func_Ein,func_Ein,quot_Ein_Ein,this%explicit,0.5_dp)
      endif
    endif
  end subroutine process_face
  
  function n_dot_conv_field_at_bnd(this,bnd_face) result(n_dot_beta)
    use boundaries, only: get_bnd
    use boundary_condition_types
    use boundary_types, only: bnd_type
    use mesh_objects, only: face_list, side_of_internal_nghb_of_bnd_face
    use numerical_solutions, only: matprop_set

    class(conv_t), intent(in) :: this
    type(face_list), intent(in) :: bnd_face
    real(dp) :: n_dot_beta(bnd_face%nqp)

    integer :: fluid_side
    real(dp) :: n_dot_u(bnd_face%nqp)
    class(bnd_type), allocatable :: bnd
    class(bc_type), allocatable :: rho_bc
    class(scal_func_type), allocatable :: rho

    call assert(bnd_face%is_boundary(only_fluid_mesh=.true.))

    call get_bnd(bnd_face%bc_id,bnd)
    fluid_side = side_of_internal_nghb_of_bnd_face(bnd_face,only_fluid_mesh=.true.)
    n_dot_u = bnd%n_dot_velocity_at_qps(bnd_face,fluid_side,this%time_conv_field)
    if (this%convecting_qnty == 'velocity') then
      ! At boundary faces, the corrective conv. is absorbed into the SIP form.
      n_dot_beta = n_dot_u
    elseif (this%convecting_qnty == 'mass_flux') then
      if (all(n_dot_u == 0.0_dp)) then
        n_dot_beta = 0.0_dp
      else
        call bnd%get_bc('density',rho_bc)
        select type(rho_bc)
          type is (dirichlet_bc_type)
            call assert(rho_bc%inhom_part%no_dirs() == 1)
            call rho_bc%inhom_part%select_dir(1,rho)
          type is (hom_neumann_bc_type)
            call matprop_set%get_func('density',rho)
          class default
            call error('BC for density not supported')
        end select
        n_dot_beta = n_dot_u * rho%values_at_qps(bnd_face,fluid_side,this%time_density)
      endif
    else
      call error('weird: this%convecting_qnty='//this%convecting_qnty)
    endif
  end function n_dot_conv_field_at_bnd
  
  subroutine Lax_Friedrichs_flux(this,face,side_Ein,side_Eout,normal,qnty,quot_Ein_Ein,quot_Ein_Eout)
    ! Lax-Friedrichs flux:
    !     H_i := (\alpha/2) (Q_i^+ - Q_i^-)  +  n_T \cdot {{beta Q_i}} .
    !
    ! where
    !           / 2 max_{neighbors} |beta \cdot n|,  for Q == U or Q == M,
    ! alpha := {
    !           \   max_{neighbors} |beta \cdot n|,  else.
    use mesh_objects, only: face_list
    use run_data, only: mesh_dimen
    use solvec_mod, only: solvec_t
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t
    
    class(conv_t), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side_Ein, side_Eout
    real(dp), intent(in) :: normal(:,:)
    type(solvec_t), intent(in) :: qnty
    real(dp), intent(out) :: quot_Ein_Ein(:), quot_Ein_Eout(:)
    
    class(vec_func_type), allocatable :: beta
    real(dp), dimension(mesh_dimen,face%nqp) :: beta_plus_at_qps, beta_min_at_qps
    real(dp), dimension(face%nqp) :: beta_plus_dot_n, beta_min_dot_n
    real(dp), dimension(face%nqp) :: alpha_at_qps
    integer :: qp, nqp
    
    call assert(.not. face%is_boundary(only_fluid_mesh=.true.))
    nqp = assert_eq(face%nqp,size(quot_Ein_Ein),size(quot_Ein_Eout),BOUNDS_CHECK)
    
    call this%get_conv_field(beta)
    call beta%values_at_qps(face,side_Ein,this%time_conv_field,beta_plus_at_qps)
    call beta%values_at_qps(face,side_Eout,this%time_conv_field,beta_min_at_qps)
    do qp = 1, nqp
      beta_plus_dot_n(qp) = dot_product(beta_plus_at_qps(:,qp),normal(:,qp))
      beta_min_dot_n(qp) = dot_product(beta_min_at_qps(:,qp),normal(:,qp))
    enddo
    if (this%LF_pointwise) then
      alpha_at_qps = max(abs(beta_min_dot_n),abs(beta_plus_dot_n))
    else
      alpha_at_qps = max(maxval(abs(beta_min_dot_n)),maxval(abs(beta_plus_dot_n)))
    endif
    if (qnty%name == 'velocity' .or. qnty%name == 'mass_flux') alpha_at_qps = 2.0_dp * alpha_at_qps
    quot_Ein_Ein  = 0.5_dp * face%quot * (alpha_at_qps + beta_plus_dot_n)
    quot_Ein_Eout = - 0.5_dp * face%quot * (alpha_at_qps - beta_min_dot_n)
  end subroutine Lax_Friedrichs_flux
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine conservative_upwind_flux(this,face,side_Ein,side_Eout,normal,quot_Ein_Ein,quot_Ein_Eout)
    ! An upwind conservative flux:
    !     H_i := (n_T \cdot M_C) ({Q_i/rho} + alpha [Q_i/rho]) .
    ! 
    ! It can be rewritten as:
    !           / (n_T \cdot M_C) (Q_i/rho)^-,  for (n_T \cdot M_C) < 0
    !   H_i := {
    !           \ (n_T \cdot M_C) (Q_i/rho)^+,  for (n_T \cdot M_C) >= 0
    use mesh_objects, only: face_list
    use numerical_solutions, only: matprop_set
    use solvec_mod, only: solvec_t
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t
    
    class(conv_t), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: side_Ein, side_Eout
    real(dp), intent(in) :: normal(:,:)
    real(dp), intent(out) :: quot_Ein_Ein(:), quot_Ein_Eout(:)
    
    class(scal_func_type), pointer :: rho
    real(dp), dimension(face%nqp) :: M_C_dot_n
    
    call assert(.not. this%corrected_convection,'NOT IMPLEMENTED')
    call assert(.not. face%is_boundary(only_fluid_mesh=.true.))
    
    M_C_dot_n = numflux_in_continuity_eq(face,side_Ein==1,normal,this%time_conv_field)
    quot_Ein_Ein = face%quot * max(0.0_dp,M_C_dot_n)
    quot_Ein_Eout = face%quot * min(0.0_dp,M_C_dot_n)
    if (this%convecting_qnty == 'velocity') then
      rho => matprop_set%func_ptr('density')
      quot_Ein_Ein = quot_Ein_Ein / rho%values_at_qps(face,side_Ein,this%time_density)
      quot_Ein_Eout = quot_Ein_Eout / rho%values_at_qps(face,side_Eout,this%time_density)
    elseif (this%convecting_qnty == 'mass_flux') then
    else
      call error('weird convecting_qnty='//trim(this%convecting_qnty ))
    endif
  end subroutine conservative_upwind_flux
  
  function numflux_in_continuity_eq(face,Ein_is_E1,normal,time) result(M_C_dot_n)
    ! Returns the numerical flux chosen in the continuity equation, given by
    !                 M_C = {M} + gamma_stab [p] n
    !     =>  n \cdot M_C = n \cdot {M} + gamma_stab [p]
    !
    ! where
    !                  /  h_F / kinem_visc ,   for equal order;
    !   gamma_stab := {
    !                  \                 0 ,   for mixed order.
    use mesh_objects, only: face_list
    use numerical_solutions, only: num_scalfunc_set, mass_flux, matprop_set, pressure_needs_penalization
    use penalty_param_mod, only: maxval_of_sides_at_qps
    use run_data, only: mesh_dimen, p_stab_safety_factor

    type(face_list), intent(in) :: face
    logical, intent(in) :: Ein_is_E1
    real(dp), intent(in) :: normal(:,:)
    integer, intent(in) :: time
    real(dp) :: M_C_dot_n(face%nqp)
    
    class(scal_func_type), pointer :: p_func, nu_func
    real(dp) :: avg_M(mesh_dimen,face%nqp)
    real(dp) :: jump_P(face%nqp), nu(face%nqp)
    integer :: qp
    
    call assert(.not. face%is_boundary(only_fluid_mesh=.true.),"numflux_in_continuity_eq: expected internal face")

    call mass_flux%avg_at_qps(face,.true.,time,avg_M)
    do qp = 1, face%nqp
      M_C_dot_n(qp) = dot_product(avg_M(:,qp),normal(:,qp))
    enddo
    
    if (pressure_needs_penalization) then
      ! the average mass flux must be corrected by the jump in the pressure
      p_func => num_scalfunc_set%func_ptr('pressure')
      nu_func => matprop_set%func_ptr('kinematic_viscosity')
      call p_func%jump_at_qps(face,.true.,time,jump_P)
      if (.not. Ein_is_E1) jump_P = - jump_P
      nu = maxval_of_sides_at_qps(nu_func,face,only_fluid_mesh=.true.,time=2)
      M_C_dot_n = M_C_dot_n + p_stab_safety_factor * (face%diameter / nu) * jump_P
    endif
  end function numflux_in_continuity_eq
  
  subroutine get_conv_field(this,conv_field)
    use func_operations_mod
    use numerical_solutions, only: matprop_set, mass_flux, velocity
    use rel_grad_of_scalfunc_mod, only: rel_grad_as_vecfunc
    
    class(conv_t), intent(in) :: this
    class(vec_func_type), allocatable, intent(out) :: conv_field
    
    class(scal_func_type), pointer :: rho
    class(vec_func_type), allocatable :: aux
    
    select case(this%convecting_qnty)
      case('mass_flux');  conv_field = mass_flux
      case('velocity');   conv_field = velocity
      case default;       call error('unknown this%convecting_qnty=' // this%convecting_qnty)
    end select
    
    if (this%corrected_convection) then
      rho => matprop_set%func_ptr('density')
      call assert(this%convecting_qnty == 'velocity','No CC when this%convecting_qnty=' // this%convecting_qnty)
      call move_alloc(conv_field,aux)
      conv_field = aux + this%cc_coeff * rel_grad_as_vecfunc(rho)
    endif
  end subroutine get_conv_field
  
  subroutine set_cc_coeff(this,cc_coeff)
    class(conv_t), intent(inout) :: this
    class(scal_func_type), intent(in) :: cc_coeff
    
    allocate(this%cc_coeff,source=cc_coeff)
    this%corrected_convection = .true.
  end subroutine set_cc_coeff
end module convection
