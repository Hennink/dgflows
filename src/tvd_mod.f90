module tvd_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use mesh_type, only: fem_mesh
  use scal_func_mod, only: scal_func_type
  use solvec_mod, only: solvec_t
  implicit none
  
  private
  public :: TVD_limiting_of_solution
  
  type, extends(scal_func_type) :: limited_scal_func_t
    private
    real(dp), allocatable :: avg(:)
    real(dp), allocatable :: lim_grad(:,:)
    logical :: only_fluid_mesh
  contains
    procedure, non_overridable :: get_values => eval_limited_scal_func_on_elem
  end type limited_scal_func_t
  
contains
  subroutine TVD_limiting_of_solution(mesh,solvec)
    use galerkin_projection, only: galerkin_proj
    
    type(fem_mesh), intent(in) :: mesh
    type(solvec_t), intent(inout) :: solvec
    
    type(limited_scal_func_t) :: lim_num_func
    
    ! The limiting procedure is valid only for linear approximations
    ! TODO implement projection onto linear space
    call assert(all(solvec%handler%order(:) <= 1),'TVD limiting of high-order solutions not implemented yet.')
    
    ! The limiting procedure is applied only to scalar quantities
    ! TODO generalize limiting procedure for vector quantities
    call assert(solvec%no_dirs() <= 1,'TVD limiting of vector quantities not implemented yet.')
    
    
    ! Calculate the solution avg and limited gradient on each element
    call get_solution_avg_and_lim_grad(mesh,solvec,lim_num_func%avg,lim_num_func%lim_grad)
    
    ! Update solvec coefficients with a Galerkin projection of the limited solution, of the form:
    ! lim_sol = avg + lim_grad*(r - r_c)
    ! r := global coord of a point in the element
    ! r_c := global coord of the element centroid
    lim_num_func%only_fluid_mesh = solvec%handler%only_fluid_mesh
    call galerkin_proj(mesh,solvec%handler,lim_num_func,time=1,discrete_proj=solvec%petsc_coeffs)
    call solvec%update_ghost_values()
  end subroutine TVD_limiting_of_solution
  
  subroutine get_solution_avg_and_lim_grad(mesh,solvec,avg,lim_grad)
    use local_global_mappings, only: gcors2pnt_set
    use mesh_objects, only: face_ptr, face_list, elem_id, elem_list, elem_ptr
    use mesh_partitioning, only: my_first_last_elems
    use pnt_set_mod, only: pnt_set_t
    use qnty_handler_mod, only: max_sol_order
    use run_data, only: mesh_dimen
    use solvec_as_scalfunc_mod, only: solvec_as_scalfunc
    use support, only: minmod, cartesian_unit_vector
    
    type(fem_mesh), intent(in) :: mesh
    type(solvec_t), intent(in) :: solvec
    real(dp), allocatable, intent(out) :: avg(:)
    real(dp), allocatable, intent(out) :: lim_grad(:,:)
    
    integer :: first_elem, last_elem
    integer :: no_effective_nghb
    type(elem_id) :: id
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_list), pointer :: elem, nghb
    type(elem_ptr), allocatable :: active_neighbors(:)
    type(face_ptr), allocatable :: active_subfaces(:)
    type(face_list), pointer :: face
    
    class(scal_func_type), allocatable :: num_func
    real(dp), allocatable :: est_grad_elem_nghb(:,:)
    real(dp), dimension(mesh%dimen) :: gcor_elem_c, gcor_nghb_c, vec
    real(dp) :: grad_in_c(mesh_dimen,1), cos_theta, nghb_avg
    type(pnt_set_t) :: pntset_centroid
    
    integer :: elem_no, face_no
    integer :: dim
    integer, parameter :: TIME = 1
    
    call mesh%get_active_elem_list_ptr(solvec%handler%only_fluid_mesh,active_elem_list)
    call my_first_last_elems(solvec%handler%only_fluid_mesh,first_elem,last_elem)
    
    
    !Evaluate and store the solution average value on each elem in partition
    call solvec_as_scalfunc(solvec,num_func)
    allocate(avg(first_elem:last_elem))
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      call num_func%eval_avg_value(elem,TIME,avg(elem_no))
    enddo
    
    
    !Evaluate and store the limited solution gradient on each elem in partition
    allocate(lim_grad(mesh%dimen,first_elem:last_elem))
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      
      !if the elem average is negative, it must be substituted by a small
      !number and the slope must be put to zero
      if (avg(elem_no) <= 0.0_dp) then
        avg(elem_no) = small_value_for_solution(solvec%name,elem)
        lim_grad(:,elem_no) = 0.0_dp
      
      else
        !Evaluate the solution gradient in the elem centroid.
        gcor_elem_c = elem%avg_vtx()
        associate(gcor => reshape(gcor_elem_c,[mesh%dimen,1]))
          call gcors2pnt_set(elem,max_sol_order(elem),gcor,pntset_centroid)
          call num_func%get_gders(elem,pntset_centroid,time,grad_in_c)
        end associate
        lim_grad(:,elem_no) = grad_in_c(:,1)
        
        !Estimate the solution gradient on neighbors.
        call elem%active_subfaces(active_subfaces,active_neighbors)
        allocate(est_grad_elem_nghb(mesh%dimen,size(active_subfaces)))
        !The variable is over-sized, because if face is an interface then it will have
        !some neighbors, but they must not be taken into account. 
        !Hence, we need another variable counting the effective no of neighbors.
        no_effective_nghb = 0
        do face_no = 1, size(active_subfaces)
          face => active_subfaces(face_no)%point
          if (.not. face%is_boundary(solvec%handler%only_fluid_mesh)) then
            no_effective_nghb = no_effective_nghb + 1
            nghb => active_neighbors(face_no)%point
            
            ! The neighbor might not be in the same partition, so the average might not
            ! be stored in avg(:) => let's compute it independently. This is possible 
            ! because we can always access the first-neighbors values, even if they are
            ! off-process.
            call num_func%eval_avg_value(nghb,time,nghb_avg)
            
            gcor_nghb_c = nghb%avg_vtx()
            vec = gcor_nghb_c - gcor_elem_c
            do dim = 1, mesh%dimen
              ! A neighbor must be taken into account in the estimation of the gradient along a 
              ! certain direction only if its orientation is relevant, i.e. only if the angle between
              ! the vector connecting the 2 centroids and the cartesian axis of interest satisfies:
              ! (0<=theta<= pi/3) .or. (2pi/3<=theta<= pi) => abs(cos(theta)) >= 0.5.
              cos_theta = dot_product(vec/norm2(vec),cartesian_unit_vector(mesh%dimen,dim))
              if (abs(cos_theta) >= 0.5_dp) then
                est_grad_elem_nghb(dim,no_effective_nghb) = (nghb_avg-avg(elem_no))*vec(dim)/(norm2(vec)**2)
              else
                est_grad_elem_nghb(dim,no_effective_nghb) = lim_grad(dim,elem_no) ! neutral choice for the minmod
              endif
            enddo
          endif
        enddo
        
        ! Limit the solution gradient
        do dim = 1, mesh%dimen
          lim_grad(dim,elem_no) = minmod([lim_grad(dim,elem_no),est_grad_elem_nghb(dim,1:no_effective_nghb)])
        enddo
        deallocate(est_grad_elem_nghb)
      endif
    enddo
  end subroutine get_solution_avg_and_lim_grad
  
  real(dp) function small_value_for_solution(sol_name,elem) result(small_value)
    use exceptions, only: error
    use mesh_objects, only: elem_list
    use numerical_solutions, only: matprop_set
    use run_data, only: rans_model_description
    
    character(*), intent(in) :: sol_name
    type(elem_list), intent(in) :: elem
    
    class(scal_func_type), pointer :: rho, nu
    integer, parameter :: TIME = 2
    real(dp) :: avg_rho, avg_nu
    real(dp) :: small_value_nu_t
    real(dp), parameter :: small_value_k = 1.0e-6_dp
    real(dp), parameter :: C_mu = 0.09_dp
    
    rho => matprop_set%func_ptr('density')
    call rho%eval_avg_value(elem,TIME,avg_rho)
    
    !usually the eddy viscosity is limited by a small fraction 
    !of the molecular one. Then, we use this value if needed
    nu => matprop_set%func_ptr('kinematic_viscosity')
    call nu%eval_avg_value(elem,TIME,avg_nu)
    small_value_nu_t = 1.0e-4_dp*avg_nu
    
    select case (sol_name)
      case('vol_turb1')
        small_value = merge(small_value_k*avg_rho,small_value_nu_t,rans_model_description/='SA')
      case('vol_turb2')
        select case (rans_model_description)
          case ('k-eps')
            small_value = avg_rho*C_mu*(small_value_k**2)/small_value_nu_t
          case('k-omega')
            small_value = avg_rho*small_value_k/small_value_nu_t
          case default
            call error("unknown RANS model")
        end select
      case default
        small_value = 0.0_dp
      end select
  end function small_value_for_solution
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine eval_limited_scal_func_on_elem(this,elem,pnt_set,time,values)
    use mesh_objects, only: elem_list
    use pnt_set_mod, only: pnt_set_t
    
    class(limited_scal_func_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
      
    integer :: p, no_points
    real(dp), dimension(elem%dimen()) :: gcor_elem_c, vec
    integer :: active_elem_no
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)

    active_elem_no = elem%get_active_elem_no(this%only_fluid_mesh)
    gcor_elem_c = elem%avg_vtx()
    do p = 1, no_points
      vec = pnt_set%gcors(:,p) - gcor_elem_c
      values(p) = this%avg(active_elem_no) + dot_product(this%lim_grad(:,active_elem_no),vec)
    enddo
  end subroutine eval_limited_scal_func_on_elem
end module tvd_mod
