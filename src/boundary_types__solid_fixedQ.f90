submodule(boundary_types) boundary_types__solid_fixedQ
  implicit none
  
  type, extends(wall_bnd_t) :: solid_fixedQ_t
    character(:), allocatable :: heat_flux_input_line
  contains
    procedure :: read_specs_from_file
    procedure :: get_bc
  end type solid_fixedQ_t
  
contains
  module procedure solid_fixedQ
    allocate(solid_fixedQ_t :: bnd)
  end procedure solid_fixedQ
  
  subroutine read_specs_from_file(this,unit_number)
    use io_basics, only: read_line_from_input_file
    
    class(solid_fixedQ_t), intent(out) :: this
    integer, intent(in) :: unit_number
    
    call read_line_from_input_file(unit_number,this%heat_flux_input_line)
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types, only: bc_type, inhom_neumann_bc_type
    use boundary_types_support_mod, only: assemble_heat_flux_neumann_bc
    
    class(solid_fixedQ_t), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    
    type(inhom_neumann_bc_type), allocatable :: inhom_neumann_bc
    
    select case(qnty_name)
      case('spec_enthalpy','vol_enthalpy','temperature')
        call assemble_heat_flux_neumann_bc(this%heat_flux_input_line,this%total_area,qnty_name,inhom_neumann_bc)
        call move_alloc(inhom_neumann_bc,bc)
      case default
        write(*,'(a)') 'ERROR: no bc for '//qnty_name//' in solid_fixedQ'
        error stop
    end select
  end subroutine get_bc
end submodule boundary_types__solid_fixedQ

