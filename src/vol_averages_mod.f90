module vol_averages_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use exceptions, only: error, warning
  use avg_over_elem_groups_mod, only: elemgroup_averager_t
  use dof_handler, only: dof_handler_type
  use f90_kind, only: dp
  use mesh_type, only: fem_mesh
  use numerical_solutions, only: matprop_set, num_scalfunc_set, velocity, solve_for_mtum_and_press, solve_for_energy
  use petsc_mod, only: i_am_master_process
  use run_data, only: average_m, average_restress, avg_over_groups_with_idvec, constant_density, mesh_dimen
  use scal_func_mod, only: scal_func_type
  use solvec_mod, only: solvec_t
  use vec_func_mod, only: vec_func_type
  implicit none
  
  private
  public :: init_avg, init_group_avg
  public :: update_averages
  public :: write_averages
  
  type(solvec_t) :: avg_m
  type(solvec_t) :: avg_prods_u
  
  real(dp) :: latest_time
  
  type(elemgroup_averager_t) :: averager
  character(100) :: GROUP_AVG_NAMES(67) = [ character(100) ::                               &
      ! Zeroth-order momenttum-related quantities:
      'rho',                                                                                &
      'mu',                                                                                 &
      'nu',                                                                                 &
      ! Zeroth-order energy-related quantites:
      'k',                                                                                  &
      'k/cp',                                                                               &
      'T',                                                                                  &
      'rho*T',                                                                              &
      'h',                                                                                  &
      'rho*h',                                                                              &
      'T*T',                                                                                &
      'rho*T*T',                                                                            &
      ! First-order momentum quantities:
      'u1', 'u2', 'u3',                                                                     &
      'rho*u1', 'rho*u2', 'rho*u3',                                                         &
      ! Second-order momentum quantities:
      'u1*u1', 'u2*u1', 'u3*u1',    'u2*u2', 'u3*u2',    'u3*u3',                           &
      'rho*u1*u1', 'rho*u2*u1', 'rho*u3*u1',    'rho*u2*u2', 'rho*u3*u2',    'rho*u3*u3',   &
      'd1u1', 'd1u2', 'd1u3',  'd2u1', 'd2u2', 'd2u3',  'd3u1', 'd3u2', 'd3u3', & ! redundant with du, but easier to post-process
      'rho*d1u1', 'rho*d1u2', 'rho*d1u3',    'rho*d2u1', 'rho*d2u2', 'rho*d2u3',    'rho*d3u1', 'rho*d3u2', 'rho*d3u3', &
      'mu*d1u1', 'mu*d1u2', 'mu*d1u3',    'mu*d2u1', 'mu*d2u2', 'mu*d2u3',    'mu*d3u1', 'mu*d3u2', 'mu*d3u3', & ! viscous stress
      'tau:du', &  ! for dissipation
      ! First-order energy-related quantites:
      'rho*T*u1', 'rho*T*u2', 'rho*T*u3',                                                   &
      ! RANS/LES transport properties:
      'nu_eff',                                                                             &
      'nu_eff/nu_mol',                                                                      &
      'nu_sfs_les_smag', 'nu_sfs_les_wale', 'nu_sfs_les_qr',                                            &
      '(k/cp)_eff',                                                                         &
      '(k/cp)_eff/(k/cp)_mol'                                                               &
  ]

contains
  subroutine init_avg(mesh,start_time)
    use dof_handler, only: init_dof_handler
    use numerical_solutions, only: get_dof_handler
    
    type(fem_mesh), intent(in) :: mesh
    real(dp), intent(in) :: start_time
    
    integer :: nprods
    type(dof_handler_type), pointer :: m_handler
    
    if (.not. solve_for_mtum_and_press()) return
    
    if (average_restress) call assert(average_m,'must average m to average Re stress')
    
    latest_time = start_time
    m_handler => get_dof_handler('mass_flux')
    
    if (average_m) then
      call avg_m%init_to_zero('m_avg',mesh,ncoeffs=1,no_dirs=mesh%dimen,orders=m_handler%order,only_fluid_mesh=.true.)
    endif
    
    if (average_restress) then
      nprods = no_uniq_prods()
      call avg_prods_u%init_to_zero('avg_prods_u',mesh,ncoeffs=nprods,no_dirs=1,orders=2*m_handler%order,only_fluid_mesh=.true.)
    endif
  end subroutine init_avg
  
  pure integer function no_uniq_prods()
    integer :: d
    
    no_uniq_prods = 0
    do d = 1, mesh_dimen
      no_uniq_prods = no_uniq_prods + d
    enddo
  end function no_uniq_prods
  
  function index_lin2mat(linear_idx) result(mat_idx)
    integer, intent(in) :: linear_idx
    integer :: mat_idx(2)
    
    integer :: i, j, counted
    
    counted = 0
    do i = 1, mesh_dimen
      do j = i, mesh_dimen
        counted = counted + 1
        if (counted == linear_idx) then
          mat_idx = [i,j]
          return
        endif
      enddo
    enddo
    call error('cannot find matrix entry belonging to linear_idx')
  end function index_lin2mat
  
  subroutine write_averages(mesh)
    use gmsh_interface, only: write_to_gmsh_file
    use petsc_mod, only: I_am_master_process
    use solvec_as_scalfunc_mod, only: num_scal_func_t
    use solvec_as_vecfunc_mod, only: num_vec_func_t, solvec_as_vecfunc
    use string_manipulations, only: int2char
    
    type(fem_mesh), intent(in) :: mesh
    
    integer :: idx, idx2(2)
    class(num_vec_func_t), allocatable :: avg_m_vecfunc
    class(scal_func_type), allocatable :: density
    type(num_scal_func_t) :: avg_cross
    type(num_vec_func_t) :: avg_m_func
    character(100) :: names(no_uniq_prods())
    
    if (.not. solve_for_mtum_and_press()) return
    
    if (average_restress) call assert(average_m,'must average m to average Re stress')
    
    if (average_m) then
      avg_m_vecfunc = solvec_as_vecfunc(avg_m)
      call write_to_gmsh_file(avg_m_vecfunc,mesh,'num-avg-mass_flux',only_fluid=.true.,time=1)
    endif
    
    if (average_restress) then
      if (I_am_master_process()) write(*,'(a)',advance='no') 'writing data field to re_stress_*.pos... '
      call matprop_set%get_func('density', density)
      avg_m_func = solvec_as_vecfunc(avg_m)
      call avg_cross%init(avg_prods_u)
      do idx = 1, no_uniq_prods()
        idx2 = index_lin2mat(idx)
        names(idx) = 're_stress_' // int2char(idx2(1)) // int2char(idx2(2))
      enddo
      call write_to_gmsh_file(mesh,names,.true.,get_re_stress_vals)
      if (I_am_master_process()) write (*,'(a)') 'done'
    endif
    
    if (avg_over_groups_with_idvec /= '') call averager%write_plottable()
  
  contains
    subroutine get_re_stress_vals(elem,pnt_set,re_stress)
      use mesh_objects, only: elem_list
      use pnt_set_mod, only: pnt_set_t
      
      type(elem_list), intent(in) :: elem
      type(pnt_set_t), intent(in) :: pnt_set
      real(dp), intent(out) :: re_stress(:,:)
      
      integer :: idx, idx2(2)
      integer :: np
      real(dp), allocatable :: avg_x(:), rho(:), vals_re(:), avg_m_vals(:,:)
      
      np = pnt_set%np()
      allocate(avg_x(np), rho(np), vals_re(np))
      allocate(avg_m_vals(mesh_dimen,np))

      call avg_m_func%get_values(elem,pnt_set,time=1,values=avg_m_vals)
      call density%get_values(elem,pnt_set,time=1,values=rho)
      do idx = 1, no_uniq_prods()
        idx2 = index_lin2mat(idx)
        call avg_cross%get_values(elem,pnt_set,time=idx,values=avg_x)
        re_stress(:,idx) = avg_x - avg_m_vals(idx2(1),:) * avg_m_vals(idx2(2),:) / rho**2
      enddo
    end subroutine get_re_stress_vals
  end subroutine write_averages
  
  subroutine update_averages(mesh,new_time)
    use numerical_solutions, only: get_solvec
    
    type(fem_mesh), intent(in) :: mesh
    real(dp), intent(in) :: new_time
    
    type(solvec_t), pointer :: new_m
    
    if (.not. solve_for_mtum_and_press()) return
    call assert(new_time > latest_time,'update_averages: new_time must be greater than lastest_time')
    if (i_am_master_process()) write(*,'(a)') "Updating volumetric averages ..."

    if (average_m) then
      new_m => get_solvec('mass_flux')
      call update_avg_val(avg_m%local_F90_coeffs(:,1),new_m%local_F90_coeffs(:,1),new_time)
    endif
    
    if (average_restress) then
      call proj_of_uu(mesh,velocity,new_time)
    endif
    
    if (avg_over_groups_with_idvec /= '') call averager%update_avg(mesh)
    
    latest_time = new_time
  end subroutine update_averages
  
  elemental subroutine update_avg_val(old_avg,new_val,new_time)
    real(dp), intent(inout) :: old_avg
    real(dp), intent(in) :: new_val
    real(dp), intent(in) :: new_time
    
    old_avg =   old_avg * latest_time / new_time  &
              + new_val * (new_time - latest_time) / new_time
  end subroutine update_avg_val
  
  subroutine proj_of_uu(mesh,u_vecfunc,new_time)
    use dof_handler, only: dof
    use mesh_objects, only: elem_id, elem_list
    use mesh_partitioning, only: my_first_last_elems
    
    type(fem_mesh), intent(in) :: mesh
    class(vec_func_type), intent(in) :: u_vecfunc
    real(dp), intent(in) :: new_time
  
    integer :: elem_no, first_elem, last_elem
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    real(dp), allocatable :: local_disc_proj(:,:)
    real(dp), pointer :: local_coeffs(:,:)
    
    call my_first_last_elems(.true.,first_elem,last_elem)
    call mesh%get_active_elem_list_ptr(.true.,active_elem_list)
    do elem_no = first_elem,last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      call local_proj_of_uu(elem,u_vecfunc,local_disc_proj)
      local_coeffs => avg_prods_u%ptr_to_coeffs(elem_no,dir=1)
      call update_avg_val(local_coeffs,local_disc_proj,new_time)
    enddo
  end subroutine proj_of_uu
  
  subroutine local_proj_of_uu(elem,vec_func,local_disc_proj)
    use mesh_objects, only: elem_list
    
    type(elem_list), intent(in) :: elem
    class(vec_func_type), intent(in) :: vec_func
    real(dp), allocatable, intent(out) :: local_disc_proj(:,:)
    
    call int_fi_ui_uj_on_elem(elem,vec_func,local_disc_proj)
    call elem%solve_for_massmat(local_disc_proj)
  end subroutine local_proj_of_uu
  
  subroutine int_fi_ui_uj_on_elem(elem,vec_func,fi_x_crossterms)
    use local_elem, only: get_quadrature, local_dg_functions
    use mesh_objects, only: elem_list
    use pnt_set_mod, only: pnt_set_t
    
    type(elem_list), intent(in) :: elem
    class(vec_func_type), intent(in) :: vec_func
    real(dp), allocatable, intent(out) :: fi_x_crossterms(:,:)
    
    integer :: elem_no
    integer :: func_order, quad_order, nnod
    type(pnt_set_t) :: quad_points
    real(dp), allocatable :: quot(:)
    real(dp), allocatable :: vec_func_at_qps(:,:)
    real(dp) :: cross
    integer :: idx, idx2(2), nterms
    integer :: qp, nqp
    
    elem_no = elem%get_active_elem_no(only_fluid_mesh=.true.)
    func_order = avg_prods_u%handler%order(elem_no)
    quad_order = 2 * func_order
    
    call get_quadrature(elem%eltype,quad_order,quad_points%lcors,quot)
    call local_dg_functions(elem%eltype,func_order,quad_points%lcors,func=quad_points%dg_funcs)
    nqp = assert_eq(size(quot),quad_points%np(),BOUNDS_CHECK)
    allocate(vec_func_at_qps(vec_func%no_dirs(),nqp))
    call vec_func%get_values(elem,quad_points,time=1,values=vec_func_at_qps)
    
    nnod = quad_points%no_dg_funcs()
    nterms = no_uniq_prods()
    allocate(fi_x_crossterms(nnod,nterms), source=0.0_dp)
    do qp = 1, nqp
      do idx = 1, nterms
        idx2 = index_lin2mat(idx)
        cross = vec_func_at_qps(idx2(1),qp) * vec_func_at_qps(idx2(2),qp)
        fi_x_crossterms(:,idx) = fi_x_crossterms(:,idx) + quad_points%dg_funcs(:,qp) * cross * quot(qp)
      enddo
    enddo
  end subroutine int_fi_ui_uj_on_elem
  
  
  subroutine init_group_avg(mesh,start_time)
    use eqv_elem_groups_mod, only: elem_groups_t
    
    type(fem_mesh), intent(in) :: mesh
    real(dp), intent(in) :: start_time
    
    type(elem_groups_t) :: elem_groups

    if (.not. solve_for_mtum_and_press()) return
    if (avg_over_groups_with_idvec == '') return
    
    call elem_groups%init(mesh,avg_over_groups_with_idvec)
    call elem_groups%write_groups_to_gmsh('eqv_elems__avg',mesh)
    call averager%init(mesh,elem_groups,start_time,get_interesting_instant_vals,GROUP_AVG_NAMES)
  end subroutine init_group_avg
  
  subroutine get_interesting_instant_vals(elem,pnt_set,time,vals)
    use, intrinsic :: ieee_arithmetic, only: ieee_value, IEEE_QUIET_NAN
    use h_solver_mod, only: eff_k_over_cp
    use m_solver_mod, only: eff_kinematic_viscosity, include_m_convection
    use mesh_objects, only: elem_list
    use pnt_set_mod, only: pnt_set_t
    use les_const_eddy_visc_mod, only: const_smag_sfs_nu, const_wale_sfs_nu, const_qr_sfs_nu, can_calculate_smag
    use run_data, only: les_specs_file, use_rans_model, &
                        rm_transp_stress_term, div_free_velocity
    use support, only: axpy_own_trace_x_I
    
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: vals(:,:)
    
    class(scal_func_type), pointer :: rho_func, mu_func, T_func, h_func, k_func, kcp_func
    class(scal_func_type), allocatable :: nu_eff_func, kcp_eff_func
    real(dp), dimension(size(vals,2)) :: rho, mu, nu,   &
                                         nu_eff,        &
                                         nu_sfs_les_smag, nu_sfs_les_wale, nu_sfs_les_qr, &
                                         k, kcp, h, T,  &
                                         kcp_eff
    real(dp) :: u(mesh_dimen,size(vals,2)),  du(mesh_dimen,mesh_dimen,size(vals,2)), &
                                            tau(mesh_dimen,mesh_dimen,size(vals,2))
    integer :: p, np, i, nnames
    logical :: solve_for_engy
    character(:), allocatable :: engy_qnty
    real(dp) :: NONEXISTENT
    
    np = assert_eq(size(vals,2),pnt_set%np(),BOUNDS_CHECK)
    nnames = assert_eq(size(GROUP_AVG_NAMES),size(vals,1),'get_interesting_instant_vals: mismatch in nnames',BOUNDS_CHECK)
    
    NONEXISTENT = ieee_value(1.0_dp,IEEE_QUIET_NAN)
    call solve_for_energy(solve_for_engy,engy_qnty)
    
    ! Get the instantaneous values for the basic quantities:
    rho_func => matprop_set%func_ptr('density')
    mu_func  => matprop_set%func_ptr('dynamic_viscosity')
    call rho_func%get_values(elem,pnt_set,time,rho)
    call mu_func%get_values(elem,pnt_set,time,mu)
    call velocity%get_values(elem,pnt_set,time,u)
    call velocity%get_gders(elem,pnt_set,time,du)
    do p = 1, np
      nu(p) = mu(p) / rho(p)
      tau(:,:,p) = mu(p) * du(:,:,p)
      if (.not. rm_transp_stress_term) tau(:,:,p) = tau(:,:,p) + transpose(tau(:,:,p))
      if (.not. div_free_velocity) call axpy_own_trace_x_I(tau(:,:,p),-1/3.0_dp)
    enddo

    if (use_rans_model .or. les_specs_file /= '') then
      if (i_am_master_process() .and. .not. include_m_convection) &
              call warning('vol. averaging: advection is turned off, so eff_nu may just be nu')
      call eff_kinematic_viscosity(nu_eff_func)
      call nu_eff_func%get_values(elem,pnt_set,time,nu_eff)
    else
      nu_eff = NONEXISTENT
    endif

    if (can_calculate_smag()) then
      call const_smag_sfs_nu(elem, pnt_set, time, nu_sfs_les_smag)
    else
      nu_sfs_les_smag = NONEXISTENT
    endif
    call const_wale_sfs_nu(elem, pnt_set, time, nu_sfs_les_wale)
    call const_qr_sfs_nu(elem, pnt_set, time, nu_sfs_les_qr)

    if (solve_for_engy) then
      k_func => matprop_set%func_ptr('thermal_conductivity')
      kcp_func => matprop_set%func_ptr('k_over_cp')
      T_func => matprop_set%func_ptr('temperature')
      h_func => matprop_set%func_ptr('spec_enthalpy')
      call k_func%get_values(elem,pnt_set,time,k)
      call kcp_func%get_values(elem,pnt_set,time,kcp)
      call T_func%get_values(elem,pnt_set,time,T)
      call h_func%get_values(elem,pnt_set,time,h)
    else
      k = NONEXISTENT
      kcp = NONEXISTENT
      T = NONEXISTENT
      h = NONEXISTENT
    endif

    if (solve_for_engy .and. (use_rans_model .or. les_specs_file /= '')) then
      call eff_k_over_cp(kcp_eff_func)
      call kcp_eff_func%get_values(elem,pnt_set,time,kcp_eff)
    else
      kcp_eff = NONEXISTENT
    endif
    
    ! Go over averaged quantities:
    do p = 1, np
!DEC$ UNROLL
      do i = 1, nnames
        select case(GROUP_AVG_NAMES(i))
          case('rho');                    vals(i,p) = rho(p)
          case('mu');                     vals(i,p) = mu(p)
          case('nu');                     vals(i,p) = nu(p)
          case('k');                      vals(i,p) = k(p)
          case('k/cp');                   vals(i,p) = kcp(p)
          case('T');                      vals(i,p) = T(p)
          case('h');                      vals(i,p) = h(p)
    
          case('rho*T');                  vals(i,p) = rho(p) * T(p)
          case('rho*h');                  vals(i,p) = rho(p) * h(p)
          case('T*T');                    vals(i,p) = T(p) * T(p)
          case('rho*T*T');                vals(i,p) = rho(p) * T(p) * T(p)
          
          case('u1');                    vals(i,p) = u(1,p)
          case('u2');                    vals(i,p) = u(2,p)
          case('u3');                    vals(i,p) = u(3,p)
          
          case('rho*u1');                vals(i,p) = rho(p) * u(1,p)
          case('rho*u2');                vals(i,p) = rho(p) * u(2,p)
          case('rho*u3');                vals(i,p) = rho(p) * u(3,p)
          
          case('u1*u1');                  vals(i,p) = u(1,p) * u(1,p)
          case('u2*u1');                  vals(i,p) = u(2,p) * u(1,p)
          case('u3*u1');                  vals(i,p) = u(3,p) * u(1,p)
          case('u2*u2');                  vals(i,p) = u(2,p) * u(2,p)
          case('u3*u2');                  vals(i,p) = u(3,p) * u(2,p)
          case('u3*u3');                  vals(i,p) = u(3,p) * u(3,p)
          
          case('rho*u1*u1');              vals(i,p) = rho(p) * u(1,p) * u(1,p)
          case('rho*u2*u1');              vals(i,p) = rho(p) * u(2,p) * u(1,p)
          case('rho*u3*u1');              vals(i,p) = rho(p) * u(3,p) * u(1,p)
          case('rho*u2*u2');              vals(i,p) = rho(p) * u(2,p) * u(2,p)
          case('rho*u3*u2');              vals(i,p) = rho(p) * u(3,p) * u(2,p)
          case('rho*u3*u3');              vals(i,p) = rho(p) * u(3,p) * u(3,p)
          
          case('d1u1');                vals(i,p) = du(1,1,p)
          case('d1u2');                vals(i,p) = du(1,2,p)
          case('d1u3');                vals(i,p) = du(1,3,p)
          case('d2u1');                vals(i,p) = du(2,1,p)
          case('d2u2');                vals(i,p) = du(2,2,p)
          case('d2u3');                vals(i,p) = du(2,3,p)
          case('d3u1');                vals(i,p) = du(3,1,p)
          case('d3u2');                vals(i,p) = du(3,2,p)
          case('d3u3');                vals(i,p) = du(3,3,p)
          
          case('rho*d1u1');                vals(i,p) = rho(p) * du(1,1,p)
          case('rho*d1u2');                vals(i,p) = rho(p) * du(1,2,p)
          case('rho*d1u3');                vals(i,p) = rho(p) * du(1,3,p)
          case('rho*d2u1');                vals(i,p) = rho(p) * du(2,1,p)
          case('rho*d2u2');                vals(i,p) = rho(p) * du(2,2,p)
          case('rho*d2u3');                vals(i,p) = rho(p) * du(2,3,p)
          case('rho*d3u1');                vals(i,p) = rho(p) * du(3,1,p)
          case('rho*d3u2');                vals(i,p) = rho(p) * du(3,2,p)
          case('rho*d3u3');                vals(i,p) = rho(p) * du(3,3,p)
          
          case('mu*d1u1');                vals(i,p) = mu(p) * du(1,1,p)
          case('mu*d1u2');                vals(i,p) = mu(p) * du(1,2,p)
          case('mu*d1u3');                vals(i,p) = mu(p) * du(1,3,p)
          case('mu*d2u1');                vals(i,p) = mu(p) * du(2,1,p)
          case('mu*d2u2');                vals(i,p) = mu(p) * du(2,2,p)
          case('mu*d2u3');                vals(i,p) = mu(p) * du(2,3,p)
          case('mu*d3u1');                vals(i,p) = mu(p) * du(3,1,p)
          case('mu*d3u2');                vals(i,p) = mu(p) * du(3,2,p)
          case('mu*d3u3');                vals(i,p) = mu(p) * du(3,3,p)
          
          case('tau:du');                 vals(i,p) = sum(tau(:,:,p) * transpose(du(:,:,p)))
          
          case('rho*T*u1');              vals(i,p) = rho(p) * T(p) * u(1,p)
          case('rho*T*u2');              vals(i,p) = rho(p) * T(p) * u(2,p)
          case('rho*T*u3');              vals(i,p) = rho(p) * T(p) * u(3,p)
          
          case('nu_eff');             vals(i,p) = nu_eff(p)
          case('nu_eff/nu_mol');      vals(i,p) = nu_eff(p) / nu(p)
          case('nu_sfs_les_smag');    vals(i,p) = nu_sfs_les_smag(p)
          case('nu_sfs_les_wale');    vals(i,p) = nu_sfs_les_wale(p)
          case('nu_sfs_les_qr');      vals(i,p) = nu_sfs_les_qr(p)
          
          case('(k/cp)_eff');             vals(i,p) = kcp_eff(p)
          case('(k/cp)_eff/(k/cp)_mol');  vals(i,p) = kcp_eff(p) / kcp(p)
          
          case default;                   call error('get_interesting_instant_vals: unknown name')
        end select
      enddo
    enddo
  end subroutine get_interesting_instant_vals
end module vol_averages_mod

