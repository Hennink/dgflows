module mesh_type
  use assertions, only: assert
  use f90_kind, only: dp
  use mesh_objects, only: elem_id, elem_list, elem_ptr, face_list, face_ptr, vertex_list
  implicit none
  
  private
  public :: fem_mesh
  public :: periodicity_shift, mesh_state
  public :: gen_elem_lists
  public :: get_level_from_id
  public :: insert_vertex
  public :: test_if_ids_are_in_same_tree
  public :: retrieve_mesh_state, write_mesh_state
  
  public :: ngbrs_removed_from_bnd
  
  real(dp), allocatable :: periodicity_shift(:,:)
  logical, public :: there_is_an_interface

  type fem_mesh
    ! The mesh contains all important things: dim, the elements, faces, vertices
    integer :: dimen
    integer :: no_vertices
    integer :: tot_no_elem
    integer :: no_active_elem
    integer :: no_active_fluid_elem
    integer :: old_no_active_elem

    logical, allocatable :: list_of_elems_tobe_refined(:) !TODO: turn this to integer
    type(elem_id), allocatable :: tot_elem_list(:)
    type(elem_id), allocatable :: active_elem_list(:)
    type(elem_id), allocatable :: active_fluid_elem_list(:)
    type(elem_id), allocatable :: old_active_elem_list(:)

    type(elem_list), pointer :: elem_list_head => null()
    type(elem_list), pointer :: elem_list_ptr => null()

    type(face_list), pointer :: face_list_head => null()
    type(face_list), pointer :: face_list_ptr => null()

    type(vertex_list), pointer :: vertex_list_head => null()
    type(vertex_list), pointer :: vertex_list_tail => null()
    type(vertex_list), pointer :: vertex_list_ptr => null()
  
  contains
    private
    procedure, public :: get_elem_ptr => get_elem_ptr_in_mesh
    procedure, public :: elem_exists => elem_exists_in_mesh
    procedure, public :: get_closest_mother_ptr => get_closest_mother_ptr_in_mesh
    procedure, public :: get_no_active_elem => get_no_active_elem_of_mesh
    procedure, public :: get_active_elems, &
                         get_active_elem_list_ptr => get_active_elem_list_ptr_of_mesh
    procedure, public :: get_all_ids => get_all_ids_in_mesh
    procedure, public :: has_been_refined => mesh_has_been_refined
  end type fem_mesh
  
  type mesh_state
    integer :: tot_no_elem
    integer :: no_active_elem
    integer :: no_active_fluid_elem
    type(elem_id), allocatable :: tot_elem_list(:)
    type(elem_id), allocatable :: active_elem_list(:)
    type(elem_id), allocatable :: active_fluid_elem_list(:)
  end type mesh_state
  
contains
  function get_elem_ptr_in_mesh(this,id) result(elem_ptr)
    ! TODO: "ichar" is both system- and compiler-dependent, making 
    ! this code both hard to read and highly error-prone.
    class(fem_mesh), intent(in) :: this
    type(elem_id), intent(in) :: id
    type(elem_list), pointer :: elem_ptr
    
    integer :: level
    integer :: child_index
    
    call assert(id%branch <= size(this%elem_list_head%children),'elem%branch does not exist!')
    
    ! first go to the correct branch:
    elem_ptr => this%elem_list_head%children(id%branch)%point
    
    ! now follow the tree down to the correct element:
    do level = 1, len(id%path)
      child_index = ichar(id%path(level:level)) - 48
      if (child_index /= 0) then
        call assert(allocated(elem_ptr%children),'elem%children not allocated!')
        call assert(child_index <= size(elem_ptr%children),'elem%child does not exist!')
        elem_ptr => elem_ptr%children(child_index)%point
      else
        return
      endif
    enddo
  end function get_elem_ptr_in_mesh
  
  logical function elem_exists_in_mesh(this,id) result(elem_exists)
    ! TODO: "ichar" is both system- and compiler-dependent, making 
    ! this code both hard to read and highly error-prone.
    class(fem_mesh), intent(in) :: this
    type(elem_id), intent(in) :: id
    
    integer :: level
    integer :: child_index
    type(elem_list), pointer :: elem_ptr
    
    call assert(id%branch <= size(this%elem_list_head%children),'elem%branch does not exist!')
    
    elem_ptr => this%elem_list_head%children(id%branch)%point
    
    do level = 1, len(id%path)
      child_index = ichar(id%path(level:level)) - 48
      if (child_index /= 0) then
        if (allocated(elem_ptr%children) .and. child_index <= size(elem_ptr%children)) then
          elem_ptr => elem_ptr%children(child_index)%point
        else
          elem_exists = .false.
          return
        endif
      else
        elem_exists = .true.
        return
      endif
    enddo
  end function elem_exists_in_mesh
  
  function get_closest_mother_ptr_in_mesh(this,id) result(mother_ptr)
    ! Find the element pointer that exists in mesh and has a lower level than 'id'
    class(fem_mesh), intent(in) :: this
    type(elem_id), intent(in) :: id
    type(elem_list), pointer :: mother_ptr

    integer :: level
    integer :: patch_level
    integer :: child_index
    
    patch_level = get_level_from_id(id)
    
    ! first go to the correct branch
    mother_ptr => this%elem_list_head%children(id%branch)%point
    
    ! follow the tree down to the correct element
    do level = 1, patch_level-1
      if (mother_ptr%active) then
        return
      else
        child_index = ichar(id%path(level:level)) - 48
        mother_ptr => mother_ptr%children(child_index)%point
      endif
    enddo
  end function get_closest_mother_ptr_in_mesh
  
  pure integer function get_no_active_elem_of_mesh(this,only_fluid_mesh) result(no_active_elem)
    class(fem_mesh), intent(in) :: this
    logical, intent(in) :: only_fluid_mesh
    
    if (only_fluid_mesh) then
      no_active_elem = this%no_active_fluid_elem
    else
      no_active_elem = this%no_active_elem
    endif
  end function get_no_active_elem_of_mesh
  
  subroutine ngbrs_removed_from_bnd(mesh,only_fluid,ngbrs_removed)
    ! Find out how many neighbors are between each element and the boundary. 
    ! If `ngbrs_removed(i) = d`, then there are `d` neighbors between the 
    ! i'th element and the boundary.
    ! 
    ! This processes the whole mesh, not just my elements.
    class(fem_mesh), target, intent(in) :: mesh
    logical, intent(in) :: only_fluid
    integer, allocatable, intent(out) :: ngbrs_removed(:)
    
    type(elem_ptr), allocatable :: elems(:)
    type(elem_list), pointer :: elem, ngbr
    integer :: elem_no, ngbr_no
    type(elem_ptr), allocatable :: active_neighbors(:)
    type(face_ptr), allocatable :: active_subfaces(:)
    integer :: i
    integer :: layer
    
    call mesh%get_active_elems(only_fluid,elems)
    allocate(ngbrs_removed(size(elems)))
    ngbrs_removed = -1
    
    ! Find all boundary elements:
    do elem_no = 1, size(elems)
      elem => elems(elem_no)%point
      if (elem%is_at_bnd(only_fluid)) ngbrs_removed(elem_no) = 0
    enddo
    
    ! Process the elements in layers, from the boundary inward:
    do layer = 1, huge(1)-1
      if (all(ngbrs_removed >= 0)) exit
      
      do elem_no = 1, size(elems)
        if (ngbrs_removed(elem_no) >= 0) cycle
        
        elem => elems(elem_no)%point
        call elem%active_subfaces(active_subfaces,active_neighbors)
        do i = 1, size(active_neighbors)
          ngbr => active_neighbors(i)%point
          ngbr_no = ngbr%get_active_elem_no(only_fluid)
          if (ngbrs_removed(ngbr_no) == layer - 1) ngbrs_removed(elem_no) = layer
        enddo
      enddo
    enddo
  end subroutine ngbrs_removed_from_bnd
  
  subroutine get_active_elems(this,only_fluid,elems)
    ! Builds a list of elem pointers. 
    ! Costs 32 bit per elem, so it's a slightly inefficient use of memory when iterating.
    class(fem_mesh), target, intent(in) :: this
    logical, intent(in) :: only_fluid
    type(elem_ptr), allocatable, intent(out) :: elems(:)

    type(elem_id), pointer :: active_elem_list(:)
    type(elem_id) :: id
    integer :: i

    call this%get_active_elem_list_ptr(only_fluid,active_elem_list)
    allocate(elems(size(active_elem_list)))
    do i = 1, size(active_elem_list)
      id = active_elem_list(i)
      elems(i)%point => this%get_elem_ptr(id)
    enddo
  end subroutine get_active_elems
  
  subroutine get_active_elem_list_ptr_of_mesh(this,only_fluid_mesh,active_elem_list)
    class(fem_mesh), target, intent(in) :: this
    logical, intent(in) :: only_fluid_mesh
    type(elem_id), pointer, intent(out) :: active_elem_list(:)
    
    if (only_fluid_mesh) then
      active_elem_list => this%active_fluid_elem_list
    else
      active_elem_list => this%active_elem_list
    endif
  end subroutine get_active_elem_list_ptr_of_mesh
  
  subroutine get_all_ids_in_mesh(this,elem_ids,bc_ids)
    use binary_tree, only: int_set
    use code_const, only: INTERFACE_SOLID_FLUID
    
    class(fem_mesh), intent(in) :: this
    integer, allocatable, dimension(:), optional, intent(out) :: elem_ids
    integer, allocatable, dimension(:), optional, intent(out) :: bc_ids
    
    type(int_set) :: elem_ids_set
    type(int_set) :: bc_ids_set
    
    type(elem_id) :: id
    integer :: elem_no
    type(elem_list), pointer :: elem
    type(elem_ptr), allocatable, dimension(:) :: active_neighbors
    type(face_ptr), allocatable, dimension(:) :: active_subfaces
    integer :: i
    type(face_list), pointer :: sub_face
    logical, parameter :: ONLY_FLUID = .false.
    
    do elem_no = 1, this%no_active_elem
      id = this%active_elem_list(elem_no)
      elem => this%get_elem_ptr(id)
      call elem_ids_set%insert(elem%mat_id)
      call elem%active_subfaces(active_subfaces,active_neighbors)
      do i = 1, size(active_subfaces)
        sub_face => active_subfaces(i)%point
        if (sub_face%is_boundary(ONLY_FLUID) .or. sub_face%ftype == INTERFACE_SOLID_FLUID) then
          call bc_ids_set%insert(sub_face%bc_id)
        endif
      enddo
    enddo
    
    if (present(elem_ids)) call elem_ids_set%sorted_values(elem_ids)
    if (present(bc_ids)) call bc_ids_set%sorted_values(bc_ids)
  end subroutine get_all_ids_in_mesh
  
  logical function mesh_has_been_refined(this)
    class(fem_mesh), intent(in) :: this
    
    integer :: elem_no
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    
    do elem_no = 1, this%no_active_elem
      id = this%active_elem_list(elem_no)
      elem => this%get_elem_ptr(id)
      if (elem%level > 0) then
        mesh_has_been_refined = .true.
        return
      endif
    enddo
    mesh_has_been_refined = .false.
  end function mesh_has_been_refined
  
  pure subroutine gen_elem_lists(mesh)
    type(fem_mesh), intent(inout) :: mesh
    
    integer :: idx, idx_act, idx_act_fluid
    type(elem_id), allocatable :: aux_act_elem_list(:), aux_fluid_act_elem_list(:)
    
    if (allocated(mesh%tot_elem_list)) deallocate(mesh%tot_elem_list)
    allocate(mesh%tot_elem_list(mesh%tot_no_elem))
    allocate(aux_act_elem_list(mesh%tot_no_elem)) !oversized
    allocate(aux_fluid_act_elem_list(mesh%tot_no_elem)) !oversized
    idx = 1
    idx_act = 1
    idx_act_fluid = 1
    mesh%elem_list_ptr => mesh%elem_list_head
    call select_elems(mesh%elem_list_ptr,mesh%tot_elem_list,aux_act_elem_list,aux_fluid_act_elem_list,idx,idx_act,idx_act_fluid)
    
    !allocate the active_elem_list with the right size
    mesh%no_active_elem = idx_act - 1
    if (allocated(mesh%active_elem_list)) deallocate(mesh%active_elem_list)
    allocate(mesh%active_elem_list, source=aux_act_elem_list(1:mesh%no_active_elem))
    !allocate the active_fluid_elem_list with the right size
    mesh%no_active_fluid_elem = idx_act_fluid - 1
    if (allocated(mesh%active_fluid_elem_list)) deallocate(mesh%active_fluid_elem_list)
    allocate(mesh%active_fluid_elem_list, source=aux_fluid_act_elem_list(1:mesh%no_active_fluid_elem))
  end subroutine gen_elem_lists
  
  pure recursive subroutine select_elems(elem_head,array,array_active,array_active_fluid,idx,idx_act,idx_act_fluid)
    type(elem_list), intent(inout) :: elem_head
    type(elem_id), dimension(:), intent(inout) :: array(:), array_active(:), array_active_fluid(:)
    integer, intent(inout) :: idx, idx_act, idx_act_fluid
    
    integer :: child, no_children
    
    ! for any active element store the id in the array
    if (elem_head%active) then
      elem_head%tot_elem_no = idx
      array(idx) = elem_head%id
      idx = idx + 1
      
      if (.not. elem_head%is_to_be_ignored()) then
        elem_head%active_elem_no = idx_act
        array_active(idx_act) = elem_head%id
        idx_act = idx_act + 1
      
        ! if the element belongs to the fluid region, store the id in the fluid array
        if (elem_head%is_fluid()) then
          elem_head%active_fluid_elem_no = idx_act_fluid
          array_active_fluid(idx_act_fluid) = elem_head%id
          idx_act_fluid = idx_act_fluid + 1
        endif
      endif
    else
      ! not active so process the children:
      no_children = size(elem_head%children)
      do child = 1, no_children
        call select_elems(elem_head%children(child)%point,array,array_active,array_active_fluid,idx,idx_act,idx_act_fluid)
      enddo
    endif
  end subroutine select_elems
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  pure subroutine test_if_ids_are_in_same_tree(id1,id2,same_tree,id1_is)
    use code_const, only: EQUAL, FINER, COARSER
    
    type(elem_id), intent(in) :: id1, id2
    logical, intent(out) :: same_tree
    integer, intent(out) :: id1_is
    integer :: level, level1, level2, level_min
    
    ! Init
    same_tree = .false.
    id1_is = -1 ! should trigger an error if ever used
    
    ! Check if ids are in same tree
    ! Branches need to be equal to consider same_tree
    if (id1%branch == id2%branch) then
      level1 = get_level_from_id(id1)
      level2 = get_level_from_id(id2)
      level_min = min(level1,level2)
      
      ! Check the paths to see if they are equal up to the minimum overlap.
      ! If so, they are same_tree.
      same_tree = .true.
      do level = 1, level_min
        if (id1%path(level:level) /= id2%path(level:level)) same_tree = .false.
      enddo
      
      ! If in same_tree; check finer etc by their respective levels
      if (same_tree) then
        if (level1 == level2) then
          id1_is = EQUAL
        elseif (level1 > level2) then
          id1_is = FINER
        else
          id1_is = COARSER
        endif
      endif
    endif
  end subroutine test_if_ids_are_in_same_tree
  
  pure integer function get_level_from_id(id) result(level)
    ! Returns the level of an element that has a particular id.
    ! The routine simply counts how many non-zeros there are in the path.
    ! It could be slightly improved by stopping when a zero is encountered.
    type(elem_id), intent(in) :: id
    
    integer :: i
    
    level = 0
    do i = 1, len(id%path)
      if (id%path(i:i) /= '0') level = level + 1
    enddo
  end function get_level_from_id
  
  pure subroutine insert_vertex(vertex_no,xyz,head,tail)
    use mesh_objects, only: vertex_coordinate, vertex_list

    integer,intent(in) :: vertex_no
    type(vertex_coordinate),intent(in) :: xyz
    type(vertex_list), pointer, intent(inout) :: head
    type(vertex_list), pointer, intent(inout) :: tail

    if (.not. associated(head)) then ! list non-existent
      allocate(head)
      tail => head
    else ! list exists and head and tail exist
      allocate(tail%next)
      tail => tail%next
    endif

    nullify(tail%next)
    tail%vertex_no = vertex_no
    tail%coordinate = xyz
  end subroutine insert_vertex

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  subroutine retrieve_mesh_state(mesh,mesh_summary)
    type(fem_mesh), intent(in) :: mesh
    type(mesh_state), intent(out) :: mesh_summary
    
    mesh_summary%tot_no_elem = mesh%tot_no_elem
    mesh_summary%no_active_elem = mesh%no_active_elem
    mesh_summary%no_active_fluid_elem = mesh%no_active_fluid_elem
    allocate(mesh_summary%tot_elem_list,source=mesh%tot_elem_list)
    allocate(mesh_summary%active_elem_list,source=mesh%active_elem_list)
    allocate(mesh_summary%active_fluid_elem_list,source=mesh%active_fluid_elem_list)
  end subroutine retrieve_mesh_state
  
  subroutine write_mesh_state(mesh,unit_no,for_RT_code)
    type(fem_mesh), intent(in) :: mesh
    integer, intent(in) :: unit_no
    logical, intent(in) :: for_RT_code
    
    integer :: elem_no
    type(elem_id) :: id
    
    open(unit_no)
      if (.not. for_RT_code) write(unit_no,*) mesh%tot_no_elem
      write(unit_no,*) mesh%no_active_elem
      if (.not. for_RT_code) write(unit_no,*) mesh%no_active_fluid_elem
      
      if (.not. for_RT_code) then
        do elem_no = 1, mesh%tot_no_elem
          id = mesh%tot_elem_list(elem_no)
          write(unit_no,*) id%branch, id%path
        enddo
      endif
      
      do elem_no = 1, mesh%no_active_elem
        id = mesh%active_elem_list(elem_no)
        write(unit_no,*) id%branch, id%path
      enddo
      
      if (.not. for_RT_code) then
        do elem_no = 1, mesh%no_active_fluid_elem
          id = mesh%active_fluid_elem_list(elem_no)
          write(unit_no,*) id%branch, id%path
        enddo
      endif
    close(unit_no)
  end subroutine write_mesh_state
  
  subroutine read_mesh_state(mesh_summary,unit_no)
    type(mesh_state), intent(inout) :: mesh_summary
    integer, intent(in) :: unit_no
    
    integer :: elem_no
    type(elem_id) :: id
    
    open(unit_no)
      read(unit_no,*) mesh_summary%tot_no_elem
      read(unit_no,*) mesh_summary%no_active_elem
      read(unit_no,*) mesh_summary%no_active_fluid_elem
      
      allocate(mesh_summary%tot_elem_list(mesh_summary%tot_no_elem))
      do elem_no = 1, mesh_summary%tot_no_elem
        read(unit_no,*) id%branch, id%path
        mesh_summary%tot_elem_list(elem_no) = id
      enddo
      
      allocate(mesh_summary%active_elem_list(mesh_summary%no_active_elem))
      do elem_no = 1, mesh_summary%no_active_elem
        read(unit_no,*) id%branch, id%path
        mesh_summary%active_elem_list(elem_no) = id
      enddo
      
      allocate(mesh_summary%active_fluid_elem_list(mesh_summary%no_active_fluid_elem))
      do elem_no = 1, mesh_summary%no_active_fluid_elem
        read(unit_no,*) id%branch, id%path
        mesh_summary%active_fluid_elem_list(elem_no) = id
      enddo
    close(unit_no)
  end subroutine read_mesh_state
end module mesh_type

