module density_weighted_time_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use pde_term_mod, only: pde_term_t
  use mesh_objects, only: elem_list
  use numerical_solutions, only: temporal_order_for_qnty, matprop_set, num_scalfunc_set
  use scal_func_mod, only: scal_func_type
  use solvec_mod, only: solvec_t
  implicit none
  
  private
  public :: expl_time
  public :: newest_ddt_rho_at_qps
  
  type, extends(pde_term_t) :: density_weighted_expl_time_t
    ! Discretizes the explicit part of a time derivative that includes old 
    ! density values (e.g., 'd/dt (rho * h)' or 'd/dt rho').
    ! 
    ! expl_weights = [1st explicit wght, 2nd explicit wgt, ...]
    private
    character(:), allocatable :: qnty
    real(dp), allocatable :: expl_weights(:)
  contains
    procedure, non_overridable :: process_elem
    procedure, non_overridable :: ngbr_coupling_depth
  end type 
  
contains
  pure integer function ngbr_coupling_depth(this)
    class(density_weighted_expl_time_t), intent(in) :: this

    ngbr_coupling_depth = -1
  end function

  type(density_weighted_expl_time_t) function expl_time(qnty,expl_weights)
    character(*), intent(in) :: qnty
    real(dp), intent(in) :: expl_weights(:)
    
    expl_time%qnty = qnty
    expl_time%expl_weights = expl_weights
  end function expl_time
  
  subroutine process_elem(this,elem,qnty,local_linsys)
    use exceptions, only: error
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: elem_list
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t
    
    class(density_weighted_expl_time_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys
    
    integer :: t, nt
    real(dp), allocatable :: old_vals(:,:), weighted_old_vals(:)
    type(unidir_lincom_rank0_t) :: func
    class(scal_func_type), pointer :: rho_func, h_func

    nt = size(this%expl_weights)
    allocate(old_vals(elem%nqp,nt))
    allocate(weighted_old_vals(elem%nqp))

    rho_func => matprop_set%func_ptr('density')
    h_func => num_scalfunc_set%func_ptr('spec_enthalpy')
    do t = 1, nt
      select case(this%qnty)
        case('rho');            old_vals(:,t) = rho_func%values_at_elem_qps(elem, t+1)
        case('rho_x_spec_h');   old_vals(:,t) = rho_func%values_at_elem_qps(elem, t+1) * h_func%values_at_elem_qps(elem, t+1)
        case default;          call error('unexpected density-weighted qnty. qnty=' // this%qnty)
      end select
    enddo
    weighted_old_vals = matmul(old_vals, -this%expl_weights)
    call func%set_as_func(elem,qnty%handler)
    call local_linsys%add_inprod_to_rhs_all_dirs(func,weighted_old_vals,elem%quot)
  end subroutine
  
  function newest_ddt_rho_at_qps(elem,dt) result(ddt_rho)
    ! Finite difference estimation of d/dt rho at the newest time step, based 
    ! on all stored values of h.
    use phys_time_mod, only: BDF_weights
    
    type(elem_list), intent(in) :: elem
    real(dp), intent(in) :: dt
    real(dp) :: ddt_rho(elem%nqp)

    class(scal_func_type), pointer :: rho_func
    integer :: t, order
    real(dp), allocatable :: weights(:)
    
    rho_func => matprop_set%func_ptr('density')
    order = temporal_order_for_qnty('spec_enthalpy')
    weights = BDF_weights(order,dt)
    ddt_rho = 0.0_dp
    do t = 1, order + 1
      ddt_rho = ddt_rho + weights(t) * rho_func%values_at_elem_qps(elem,t)
    enddo
  end function newest_ddt_rho_at_qps
end module density_weighted_time_mod
