module pprint_mod
  use f90_kind, only: dp
  use, intrinsic :: iso_fortran_env, only : unit => output_unit
  implicit none

  private
  public :: pprint

  interface pprint
    module procedure pprint_named_vector, pprint_named_matrix
    module procedure pprint_vector, pprint_matrix
  end interface pprint

contains
  subroutine pprint_named_vector(name,vec)
    character(*), intent(in) :: name
    real(dp), intent(in) :: vec(:)

    call pprint_named_matrix(name,reshape(vec,[size(vec),1]))
  end subroutine pprint_named_vector

  subroutine pprint_vector(vec)
    real(dp), intent(in) :: vec(:)

    call pprint_matrix(reshape(vec,[size(vec),1]))
  end subroutine pprint_vector

  subroutine pprint_named_matrix(name,mtx)
    character(*), intent(in) :: name
    real(dp), intent(in) :: mtx(:,:)

    character(:), allocatable :: title
    integer :: i

    allocate(title,source='=== ' // name // ':' // ' ===')
    write(unit,'(A)') title
    call pprint_matrix(mtx)
    do i = 1, len(title)
      write(unit,"('_')",advance='no')
    enddo
    write(unit,'()')
  end subroutine pprint_named_matrix

  subroutine pprint_matrix(mtx)
    real(dp), intent(in) :: mtx(:,:)

    integer :: i, j

    do i = 1, size(mtx,1)
      do j = 1, size(mtx,2)
        write(unit,'(es8.1)',advance='no') mtx(i,j)
        if (j < size(mtx,2)) write(unit,'(a)',advance='no') ' '
      enddo
      write(unit,'()')
    enddo
  end subroutine pprint_matrix
end module pprint_mod
