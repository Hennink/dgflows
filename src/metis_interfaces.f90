module metis_interfaces
  use, intrinsic :: iso_c_binding, only: c_int, c_double, c_loc, c_null_ptr, c_ptr
  use exceptions, only: error
  implicit none
  
  private
  public :: part_mesh_dual
  public :: METIS_IDX_T_KIND, set_default_options, set_option
  public :: METIS_DBG_INFO, METIS_OBJTYPE_VOL
  
  ! Based some trail and error, this is the only value for METIS_IDX_T_KIND for
  ! which we get the right results.
  integer, parameter :: METIS_IDX_T_KIND = c_int, &
                        METIS_REAL_T_KIND = c_double, &
                        METIS_INT_KIND = c_int
  
  interface
    function METIS_part_mesh_dual(ne,nn,eptr,eind,vwgt,vsize,ncommon,nparts,tpwgts,options,objval,epart,npart) &
                                  bind(c,name='METIS_PartMeshDual')
      use, intrinsic :: iso_c_binding, only: c_ptr
      import :: METIS_INT_KIND
      implicit none
      
      type(c_ptr), value :: ne
      type(c_ptr), value :: nn
      type(c_ptr), value :: eptr
      type(c_ptr), value :: eind
      type(c_ptr), value :: vwgt
      type(c_ptr), value :: vsize
      type(c_ptr), value :: ncommon
      type(c_ptr), value :: nparts
      type(c_ptr), value :: tpwgts
      type(c_ptr), value :: options
      type(c_ptr), value :: objval
      type(c_ptr), value :: epart
      type(c_ptr), value :: npart
      integer(METIS_INT_KIND) :: METIS_part_mesh_dual
    end function METIS_part_mesh_dual
  end interface
  
  ! List of options, copied from metis.h:
  integer, parameter :: METIS_NOPTIONS = 40
  enum, bind(c)
    enumerator :: METIS_OPTION_PTYPE, &     ! partioning method:
                                            !   METIS_PTYPE_RB         Multilevel recursive bisectioning
                                            !   METIS_PTYPE_KWAY       Multilevel k-way partitioning
                                            
                  METIS_OPTION_OBJTYPE, &   ! type of objective:
                                            !   METIS_OBJTYPE_CUT      Edge-cut minimization
                                            !   METIS_OBJTYPE_VOL      Total communication volume minimization
                                            !   METIS_OBJTYPE_NODE     ??? (not explained in the manual)
                                            
                  METIS_OPTION_CTYPE, &     ! matching scheme to be used during coarsening:
                                            !   METIS_CTYPE_RM         Random matching
                                            !   METIS_CTYPE_SHEM       Sorted heavy-edge matching
                                            
                  METIS_OPTION_IPTYPE, &    ! the algorithm used during initial partitioning:
                                            !   METIS IPTYPE GROW      Grows a bisection using a greedy strategy
                                            !   METIS IPTYPE RANDOM    Computes a bisection at random followed by a refinement
                                            !   METIS IPTYPE EDGE      Derives a separator from an edge cut
                                            !   METIS IPTYPE NODE      Grow a bisection using a greedy node-based strategy
                                            !   METIS_IPTYPE_METISRB   ??? (not explained in the manual)
                                            
                  METIS_OPTION_RTYPE, &     ! algorithm used for refinement:
                                            !   METIS RTYPE FM         FM-based cut refinement
                                            !   METIS RTYPE GREEDY     Greedy-based cut and volume refinement
                                            !   METIS RTYPE SEP2SIDED  Two-sided node FM refinement
                                            !   METIS RTYPE SEP1SIDED  One-sided node FM refinement
                                            
                  METIS_OPTION_DBGLVL, &    ! Specifies the amount of progress/debugging information will be printed during 
                                            ! the execution of the algorithms. Default value is 0 (no debugging/progress info). 
                                            ! For non-zero values, see later (from line 128)
                                            
                  METIS_OPTION_NITER, &     ! number of iterations for the refinement algorithms at each stage of the
                                            ! uncoarsening process. Default is 10.
                                            
                  METIS_OPTION_NCUTS, &     ! specifies the number of different partitionings that it will compute. 
                                            ! The final partitioning is the one that achieves the best edgecut or 
                                            ! communication volume. Default is 1.
                                            
                  METIS_OPTION_SEED, &      ! Specifies the seed for the random number generator
                                            
                  METIS_OPTION_NO2HOP, &    ! Specifies if the coarsening will not perform any 2hop matchings when the standard 
                                            ! matching approach fails to sufficiently coarsen the graph. The 2hop matching is 
                                            ! very effective for graphs with power-law degree distributions (0 - yes, do it; 1 - no)
                                            
                  METIS_OPTION_MINCONN, &   ! Specifies whether (0 - no; 1 - yes) partitioning routines should try to minimize the
                                            ! max degree of the subdomain graph, i.e., the graph in which each partition is a node,
                                            ! and edges connect subdomains with a shared interface.
                                            
                  METIS_OPTION_CONTIG, &    ! Specifies if (0 - no; 1 - yes) the partitioning routines should try to produce 
                                            ! partitions that are contiguous. Ignored if the input graph is not connected.
                                            
                  METIS_OPTION_COMPRESS, &  ! Specifies if (0 - no; 1 - yes) the graph should be compressed by combining 
                                            ! together vertices that have identical adjacency lists.
                                            
                  METIS_OPTION_CCORDER, &   ! Specifies if (0 - no; 1 - yes) the connected components of the graph should 
                                            ! first be identified and ordered separately.
                                            
                  METIS_OPTION_PFACTOR, &   ! Explanation too long (see manual). Seems useful only for matrices
                                            
                  METIS_OPTION_NSEPS, &     ! number of different separators that it will compute at each level of nested
                                            ! dissection. The final separator that is used is the smallest one. Default is 1.
                                            
                  METIS_OPTION_UFACTOR, &   ! Specifies the maximum allowed load imbalance among the partitions. A value of
                                            ! x indicates that the allowed load imbalance is (1 + x) / 1000.
                                            
                  METIS_OPTION_NUMBERING, & ! C-style or Fortran-style numbering
                                            
                  ! Used for command-line parameter purposes:
                  METIS_OPTION_HELP, &
                  METIS_OPTION_TPWGTS, &
                  METIS_OPTION_NCOMMON, &
                  METIS_OPTION_NOOUTPUT, &
                  METIS_OPTION_BALANCE, &
                  METIS_OPTION_GTYPE, &
                  METIS_OPTION_UBVEC
  end enum
  enum, bind(c)
    ! Values for METIS_OPTION_PTYPE
    enumerator :: METIS_PTYPE_RB, METIS_PTYPE_KWAY
  end enum
  enum, bind(c)
    ! Values for METIS_OPTION_OBJTYPE
    enumerator :: METIS_OBJTYPE_CUT, METIS_OBJTYPE_VOL, METIS_OBJTYPE_NODE
  end enum
  enum, bind(c)
    ! Values for METIS_OPTION_CTYPE
    enumerator :: METIS_CTYPE_RM, METIS_CTYPE_SHEM
  end enum
  enum, bind(c)
    ! Values for METIS_OPTION_IPTYPE
    enumerator :: METIS_IPTYPE_GROW, METIS_IPTYPE_RANDOM, METIS_IPTYPE_EDGE, METIS_IPTYPE_NODE, METIS_IPTYPE_METISRB
  end enum
  enum, bind(c)
    ! Values for METIS_OPTION_RTYPE
    enumerator :: METIS_RTYPE_FM, METIS_RTYPE_GREEDY, METIS_RTYPE_SEP2SIDED, METIS_RTYPE_SEP1SIDED
  end enum
  enum, bind(c)
    ! These values for METIS_OPTION_DBGLVL can be combined with a bitwise OR.
    enumerator :: METIS_DBG_INFO       = 1,   & ! Shows various diagnostic messages
                  METIS_DBG_TIME       = 2,   & ! Perform timing analysis
                  METIS_DBG_COARSEN    = 4,   & ! Show the coarsening progress
                  METIS_DBG_REFINE     = 8,   & ! Show the refinement progress
                  METIS_DBG_IPART      = 16,  & ! Show info on initial partitioning
                  METIS_DBG_MOVEINFO   = 32,  & ! Show info on vertex moves during refinement
                  METIS_DBG_SEPINFO    = 64,  & ! Show info on vertex moves during sep refinement
                  METIS_DBG_CONNINFO   = 128, & ! Show info on minimization of subdomain connectivity
                  METIS_DBG_CONTIGINFO = 256, & ! Show info on elimination of connected components
                  METIS_DBG_MEMORY     = 2048   ! Show info related to wspace allocation
  end enum

contains
  subroutine part_mesh_dual(no_nodes, CSR_elem_list, CSR_node_list, &
                            min_common_vtx_ngbrs, no_parts, elem_partition, &
                            node_partition, elem_weights, elem_sizes, &
                            partition_w, metis_options, edgecut_or_comm_vol)
    ! elem_weights         Leave out to give all elements an equal weight.
    !
    ! elem_sizes           sizes of the elements that are used for computing the total
    !                      communication volume. This can be left out when the objective is cut
    !                      or when all elements have an equal size.
    !
    ! edgecut_or_comm_vol  Upon successful completion, this stores either the
    !                      edgecut or the total communication volume of the
    !                      dual graphs partitioning.
    use assertions, only: assert, assert_small
    use f90_kind, only: dp
    use petsc_mod, only: i_am_master_process
    
    integer, intent(in) :: no_nodes
    integer, intent(in) :: CSR_elem_list(:), CSR_node_list(:)
    integer, intent(in) :: min_common_vtx_ngbrs
    integer, intent(in) :: no_parts
    integer, intent(out) :: elem_partition(:)
    integer, optional, intent(out) :: node_partition(:)
    integer, optional, intent(in) :: elem_weights(:), elem_sizes(:)
    real(dp), optional, intent(in) :: partition_w(:)
    integer(METIS_IDX_T_KIND), target, optional, intent(in) :: metis_options(:)
    integer, optional, intent(out) :: edgecut_or_comm_vol
    
    integer(METIS_IDX_T_KIND), target :: c_no_elem, c_no_nodes
    integer(METIS_IDX_T_KIND), target :: c_CSR_elem_list(size(CSR_elem_list)), c_CSR_node_list(size(CSR_node_list))
    integer(METIS_IDX_T_KIND), target :: c_min_common_vtx_ngbrs
    integer(METIS_IDX_T_KIND), target :: c_no_parts
    integer(METIS_IDX_T_KIND), target :: c_edgecut_or_comm_vol
    integer(METIS_IDX_T_KIND), target :: c_elem_partition(size(CSR_elem_list)-1), c_node_partition(no_nodes)
    integer(METIS_IDX_T_KIND), allocatable, target :: c_elem_weights(:), c_elem_sizes(:)
    real(METIS_REAL_T_KIND), allocatable, target :: c_partition_w(:)
    
    type(c_ptr) :: ne
    type(c_ptr) :: nn
    type(c_ptr) :: eptr
    type(c_ptr) :: eind
    type(c_ptr) :: vwgt
    type(c_ptr) :: vsize
    type(c_ptr) :: ncommon
    type(c_ptr) :: nparts
    type(c_ptr) :: tpwgts
    type(c_ptr) :: options
    type(c_ptr) :: objval
    type(c_ptr) :: epart
    type(c_ptr) :: npart
    
    integer(METIS_INT_KIND) :: metis_out
    integer :: no_elem
    
    if (present(partition_w)) then
      call assert(size(partition_w) == no_parts)
      call assert_small(sum(partition_w) - 1.0_dp,'sum(partition_w) should be 1.0')
    endif
    no_elem = size(CSR_elem_list) - 1
    call assert(size(elem_partition) == no_elem)
    if (present(node_partition)) call assert(size(node_partition) == no_nodes)
    if (present(elem_weights)) call assert(size(elem_weights) == no_elem)
    if (present(elem_sizes))   call assert(size(elem_sizes) == no_elem)
    if (present(metis_options)) call assert(size(metis_options) == METIS_NOPTIONS)

    if (no_parts == 1) then
      ! Metis unexpectedly crashes when there is only one partition.
      elem_partition = 0
      if (present(node_partition)) node_partition = 0
      if (present(edgecut_or_comm_vol)) edgecut_or_comm_vol = 0
      return
    endif

    if (present(elem_weights)) allocate(c_elem_weights(size(elem_weights)))
    if (present(elem_sizes)) allocate(c_elem_sizes(size(elem_sizes)))
    if (present(partition_w)) allocate(c_partition_w(size(partition_w)))
    
    c_no_elem = no_elem
    c_no_nodes = no_nodes
    c_CSR_elem_list = CSR_elem_list
    c_CSR_node_list = CSR_node_list
    if (present(elem_weights)) c_elem_weights = elem_weights
    if (present(elem_sizes)) c_elem_sizes = elem_sizes
    c_no_parts = no_parts
    c_min_common_vtx_ngbrs = min_common_vtx_ngbrs
    if (present(partition_w)) c_partition_w = partition_w
    
    ne = c_loc(c_no_elem)
    nn = c_loc(c_no_nodes)
    eptr = c_loc(c_CSR_elem_list)
    eind = c_loc(c_CSR_node_list)
    if (present(elem_weights)) then
      vwgt = c_loc(c_elem_weights)
    else
      vwgt = c_null_ptr
    endif
    if (present(elem_sizes)) then
      vsize = c_loc(c_elem_sizes)
    else
      vsize = c_null_ptr
    endif
    nparts = c_loc(c_no_parts)
    ncommon = c_loc(c_min_common_vtx_ngbrs)
    if (present(partition_w)) then
      tpwgts = c_loc(c_partition_w)
    else
      tpwgts = c_null_ptr
    endif
    if (present(metis_options)) then
      options = c_loc(metis_options)
    else
      options = c_null_ptr
    endif
    objval = c_loc(c_edgecut_or_comm_vol)
    epart = c_loc(c_elem_partition)
    npart = c_loc(c_node_partition)
    
    metis_out = METIS_part_mesh_dual(ne,nn,eptr,eind,vwgt,vsize,ncommon,nparts,tpwgts,options,objval,epart,npart)
    call check_metis_output(metis_out)

    if (i_am_master_process()) write(*,'(a,i0)') "Metis's edgecut or comm vol = ", c_edgecut_or_comm_vol
    if (present(edgecut_or_comm_vol)) edgecut_or_comm_vol = c_edgecut_or_comm_vol
    elem_partition = c_elem_partition
    if (present(node_partition)) node_partition = c_node_partition
  end subroutine part_mesh_dual

  subroutine set_option(options,opt,val)
    integer(METIS_IDX_T_KIND), intent(inout) :: options(0:)
    character(*), intent(in) :: opt
    integer(METIS_IDX_T_KIND), intent(in) :: val

    integer(METIS_IDX_T_KIND) :: idx

    select case(opt)
      case('objective');  idx = METIS_OPTION_OBJTYPE
      case('debug');      idx = METIS_OPTION_DBGLVL
      case default;       call error('METIS: unknown opt=' // opt)
    end select
    options(idx) = val
  end subroutine set_option

  subroutine set_default_options(options)
    integer(METIS_IDX_T_KIND), allocatable, intent(out) :: options(:)

    interface
      function METIS_set_default_options(options) bind(c,name='METIS_SetDefaultOptions')
        use, intrinsic :: iso_c_binding, only: c_ptr
        import :: METIS_INT_KIND
        implicit none

        type(c_ptr), value :: options
        integer(METIS_INT_KIND) :: METIS_set_default_options
      end function METIS_set_default_options
    end interface
    integer(METIS_INT_KIND) :: metis_output
    integer(METIS_IDX_T_KIND), target :: c_options(METIS_NOPTIONS)
    type(c_ptr) :: options_ptr

    options_ptr = c_loc(c_options)
    metis_output = METIS_set_default_options(options_ptr)
    call check_metis_output(metis_output)
    options = c_options
  end subroutine set_default_options
  
  subroutine check_metis_output(metis_output)
    use string_manipulations, only: int2char

    integer(METIS_INT_KIND), intent(in) :: metis_output
    
    character(:), allocatable :: error_msg
    ! These values are copied from 'metis.h':
    integer(METIS_INT_KIND), parameter :: METIS_OK              = 1_METIS_INT_KIND, &
                                          METIS_ERROR_INPUT     = -2_METIS_INT_KIND, &
                                          METIS_ERROR_MEMORY    = -3_METIS_INT_KIND, &
                                          METIS_ERROR           = -4_METIS_INT_KIND

    select case(metis_output)
      case(METIS_OK)
      case(METIS_ERROR_INPUT);  error_msg = 'erroneous inputs and/or options'
      case(METIS_ERROR_MEMORY); error_msg = 'insufficient memory'
      case(METIS_ERROR);        error_msg = 'unknown error'
      case default;             error_msg = 'unknown output value: ' // int2char(int(metis_output))
    end select
    if (allocated(error_msg)) call error('METIS: '//error_msg)
  end subroutine check_metis_output
end module metis_interfaces
