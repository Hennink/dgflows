module pressure
  use assertions, only: assert
  use boundaries, only: get_bc
  use boundary_condition_types
  use density_weighted_time_mod, only: newest_ddt_rho_at_qps
  use dof_handler, only: dof, dof_handler_type, set_type_and_preallocate
  use f90_kind, only: dp
  use galerkin_projection, only: galerkin_proj
  use local_elem, only: mutually_hierarchical_func_sets
  use local_integrals, only: int_grad_fi_x_fj_on_elem, &
                             integrals_fi_x_fj_x_n_on_face, &
                             int_fEin_x_vecfunc_dot_n_on_bnd_face
  use mesh_objects, only: elem_list, elem_ptr, elem_id, &
                          face_list, face_ptr
  use mesh_partitioning, only: my_first_last_elems
  use mesh_type, only: fem_mesh
  use numerical_solutions, only: get_dof_handler, get_solvec
  use penalty_param_mod, only: maxval_of_sides_at_qps
  use petsc_mod, only: I_am_master_process
  use petsc_mat_mod, only: petsc_mat_type
  use petsc_vec_mod, only: petsc_vec_type
  use run_data, only: div_free_velocity
  use solvec_mod, only: solvec_t
  use vec_block_mod, only: vec_block_t
  implicit none
  
  private
  public :: init_div_operator
  public :: gradient_oper
  public :: rhs_continuity_eq
  public :: init_pressure_nullspace_vec, const_pressure_proj

  type(petsc_mat_type), protected :: gradient_oper
  type(petsc_vec_type), protected :: const_pressure_proj
  
contains
subroutine init_div_operator(mesh)
  type(fem_mesh), intent(in) :: mesh
  
  type(dof_handler_type), pointer :: P_handler, M_handler
  class(bc_type), allocatable :: bc
  integer :: first_elem, last_elem
  integer :: nnod_M, nnod_M_nghb, nnod_P
  integer :: elem_no, nghb_no, face_no
  type(elem_id) :: id
  integer :: dim
  integer :: node_v, node_u, row, col
  real(dp) :: mat_entry
  type(elem_list), pointer :: elem, neighbor
  type(elem_ptr), allocatable :: active_neighbors(:)
  type(face_ptr), allocatable :: active_subfaces(:)
  type(face_list), pointer :: sub_face
  logical :: boundary_face, add_outgoing_edge_term
  real(dp) :: face_average_factor
  real(dp), allocatable :: elem_int_grad_fi_x_fj(:,:,:)
  real(dp), allocatable :: fi_x_fj_x_n_Ein_Ein(:,:,:), fi_x_fj_x_n_Ein_Eout(:,:,:)
  
  type(petsc_mat_type) :: div_operator_petsc

  M_handler => get_dof_handler('mass_flux')
  P_handler => get_dof_handler('pressure')
  
  call div_operator_petsc%create()
  call set_type_and_preallocate(div_operator_petsc, mesh, P_handler, M_handler, coupling_depth=1, symm=.false., force_aij = .true.)
  
  call my_first_last_elems(.true.,first_elem,last_elem)
  do elem_no = first_elem, last_elem
    id = mesh%active_fluid_elem_list(elem_no)
    elem => mesh%get_elem_ptr(id)
    nnod_M = M_handler%no_nod_elem(elem_no)
    nnod_P = P_handler%no_nod_elem(elem_no)
    
    call assert(mutually_hierarchical_func_sets(elem%eltype,M_handler%order(elem_no),P_handler%order(elem_no)), &
                                  'init_div_operator: pairs of basis function sets need to be hierarchical here.')
    
    ! volumetric term of div_operator:  - \int q (nabla \cdot M)
    call int_grad_fi_x_fj_on_elem(elem,M_handler%order(elem_no),P_handler%order(elem_no),elem_int_grad_fi_x_fj)
    do dim = 1, mesh%dimen
      do node_v = 1, nnod_P
        do node_u = 1, nnod_M
          row = dof(P_handler,1,elem_no,1,node_v)
          col = dof(M_handler,1,elem_no,dim,node_u)
          mat_entry = - elem_int_grad_fi_x_fj(node_u,node_v,dim)
          call div_operator_petsc%add(row,col,mat_entry)
        enddo
      enddo
    enddo
    call elem%active_subfaces(active_subfaces,active_neighbors)
    do face_no = 1, size(active_subfaces)
      sub_face => active_subfaces(face_no)%point
      boundary_face = sub_face%is_boundary(only_fluid_mesh=.true.)
      if (boundary_face) then
        call get_bc(sub_face%bc_id,'mass_flux',bc)
      else
        neighbor => active_neighbors(face_no)%point
        nghb_no = neighbor%active_fluid_elem_no
        nnod_M_nghb = M_handler%no_nod_elem(nghb_no)
      endif
      
      ! edge terms of div_operator:  \int_F {q} ([M] \cdot n)
      call integrals_fi_x_fj_x_n_on_face(sub_face%elem_is_E1(elem),boundary_face,sub_face,fi_x_fj_x_n_Ein_Ein, &
                                                                                          fi_x_fj_x_n_Ein_Eout )
      
      if (boundary_face) then
        face_average_factor = 1.0_dp
        select type(bc)
          class is (dirichlet_bc_type);        add_outgoing_edge_term = .true.
          type is (hom_neumann_bc_type);       add_outgoing_edge_term = .false.
          type is (inhom_neumann_bc_type);     error stop "init_div_operator: I don't know what do to with a inhom. Neumann bc"
          type is (generalized_neumann_bc_t);  add_outgoing_edge_term = .false.
          type is (adjusted_traction_bc_t);    add_outgoing_edge_term = .false.
          type is (robin_bc_type);             error stop "init_div_operator: I don't know what do to with a Robin bc"
          type is (slip_bc_type);              add_outgoing_edge_term = .true.
          class default;                       error stop 'init_div_operator: unknown bc.'
        end select
      else
        ! Internal face
        face_average_factor = 0.5_dp
        add_outgoing_edge_term = .true.
      endif
      
      if (add_outgoing_edge_term) then
        do dim = 1, mesh%dimen
          do node_u = 1, nnod_M
            do node_v = 1, nnod_P
              col = dof(M_handler,1,elem_no,dim,node_u)
              row = dof(P_handler,1,elem_no,1,node_v)
              mat_entry = + face_average_factor * fi_x_fj_x_n_Ein_Ein(node_v,node_u,dim)
              call div_operator_petsc%add(row,col,mat_entry)
            enddo
          enddo
        enddo
      endif
      if (.not. boundary_face) then
        do dim = 1, mesh%dimen
          do node_u = 1, nnod_M_nghb
            do node_v = 1, nnod_P
              col = dof(M_handler,1,nghb_no,dim,node_u)
              row = dof(P_handler,1,elem_no,1,node_v)
              mat_entry = - face_average_factor * fi_x_fj_x_n_Ein_Eout(node_v,node_u,dim)
              call div_operator_petsc%add(row,col,mat_entry)
            enddo
          enddo
        enddo
      endif
    enddo
  enddo
  call div_operator_petsc%finalize_assembly()
  
  call div_operator_petsc%get_transpose(gradient_oper)
end subroutine init_div_operator


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine rhs_continuity_eq(mesh,P_handler,dt,continuity_rhs)
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t

    type(fem_mesh), intent(in) :: mesh
    type(dof_handler_type), intent(in) :: P_handler
    real(dp), intent(in) :: dt
    type(petsc_vec_type), intent(inout) :: continuity_rhs
    
    integer :: P_order
    integer :: elem_no, first_elem, last_elem
    type(elem_list), pointer :: elem
    type(elem_ptr), allocatable, dimension(:) :: active_neighbors
    type(face_ptr), allocatable, dimension(:) :: active_subfaces
    integer :: i
    type(face_list), pointer :: sub_face
    type(elem_id) :: id
    type(vec_block_t) :: vec_block
    type(unidir_lincom_rank0_t) :: p_value
    class(bc_type), allocatable :: M_bc
    real(dp), allocatable, dimension(:) :: int_fEin_x_Ubc_dot_n
    
    call my_first_last_elems(.true.,first_elem,last_elem)
    do elem_no = first_elem, last_elem
      id = mesh%active_fluid_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      P_order = P_handler%order(elem_no)
      
      ! On boundary faces build the rhs vector
      !     \int_F q (g_D \cdot n)
      call elem%active_subfaces(active_subfaces,active_neighbors)
      do i = 1, size(active_subfaces)
        sub_face => active_subfaces(i)%point
        if (.not. sub_face%is_boundary(only_fluid_mesh=.true.)) cycle
        
        call get_bc(sub_face%bc_id,'mass_flux',M_bc)
        select type(M_bc)
          type is (dirichlet_bc_type)
            call int_fEin_x_vecfunc_dot_n_on_bnd_face(sub_face,P_order,M_bc%inhom_part,1,int_fEin_x_Ubc_dot_n)
            call continuity_rhs%add(dof(P_handler,1,elem_no,1), int_fEin_x_Ubc_dot_n)
          type is (hom_neumann_bc_type)
          type is (inhom_neumann_bc_type);    error stop "pressure_correction: inhom. neumann not implemented"
          type is (generalized_neumann_bc_t)
          type is (adjusted_traction_bc_t)
          type is (robin_bc_type);            error stop "pressure_correction: robin not implemented"
          type is (slip_bc_type)
          class default;                      error stop 'pressure_correction: unknown bc_type'
        end select
      enddo
      
      if (.not. div_free_velocity) then
        ! d/dt rho^{n+1}
        call vec_block%init(elem_no,P_handler)
        call p_value%set_as_func(elem,P_handler)
        call vec_block%add_inprod_all_dirs(p_value,newest_ddt_rho_at_qps(elem,dt),elem%quot)
        call vec_block%add_to_glob_vec(continuity_rhs,P_handler)
      endif
    enddo
    call continuity_rhs%finalize_assembly()
  end subroutine rhs_continuity_eq
  
  subroutine init_pressure_nullspace_vec(mesh,P_mat,P_handler)
    use functions_lib, only: const_func
    
    type(fem_mesh), intent(in) :: mesh
    type(petsc_mat_type), intent(in) :: P_mat
    type(dof_handler_type), intent(in) :: P_handler
    
    if (I_am_master_process()) write(*,"(a)") "Setting the pressure null space to a constant..."
    call P_mat%init_compatible_vecs(right_vec=const_pressure_proj)
    call galerkin_proj(mesh,P_handler,const_func(1.0_dp),1,const_pressure_proj)
    call const_pressure_proj%normalize_by_2norm()
  end subroutine
  
  subroutine check_mass_error(mesh,dt)
    ! Computes and prints the residual: - g  +  div_operator * M
    type(fem_mesh), intent(in):: mesh
    real(dp), intent(in) :: dt
    
    type(petsc_vec_type) :: mass_error
    real(dp) :: norm_mass_error
    type(solvec_t), pointer :: M, P
    
    M => get_solvec('mass_flux')
    P => get_solvec('pressure')
    
    call gradient_oper%init_compatible_vecs(right_vec=mass_error)
    call rhs_continuity_eq(mesh,P%handler,dt,mass_error)
    call mass_error%mult_with_scal(-1.0_dp)
    call gradient_oper%mult_transpose_add(M%petsc_coeffs,mass_error,mass_error)
    norm_mass_error = mass_error%norm_2()
    if (I_am_master_process()) write(*,'(a,es10.3)') 'NORM OF MASS ERROR = ', norm_mass_error
  end subroutine check_mass_error
end module pressure
