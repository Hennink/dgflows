module curl_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only: vec_func_type
  implicit none
  
  private
  public :: curl_t, curl

  type, extends(vec_func_type) :: curl_t
    private
    class(vec_func_type), allocatable :: vecfield
  contains
    procedure, non_overridable :: get_values
    procedure, non_overridable :: no_dirs
  end type curl_t

contains
  type(curl_t) function curl(vecfield)
    use run_data, only: mesh_dimen

    class(vec_func_type), intent(in) :: vecfield

    call assert(mesh_dimen == vecfield%no_dirs())
    allocate(curl%vecfield,source=vecfield)
  end function curl
  
  pure integer function no_dirs(this)
    class(curl_t), intent(in) :: this

      no_dirs = 3
  end function no_dirs

  subroutine get_values(this,elem,pnt_set,time,values)
    use f90_kind, only: dp
    use mesh_objects, only: elem_list
    use pnt_set_mod, only: pnt_set_t
    use scal_func_mod, only: scal_func_type
    use vec_func_mod, only: vec_func_type

    class(curl_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:,:)

    real(dp), allocatable :: gders_vecfield(:,:,:)
    integer :: np

    np = assert_eq(size(values,2),pnt_set%np(),BOUNDS_CHECK)
    
    allocate(gders_vecfield(elem%dimen(),this%vecfield%no_dirs(),np))
    call this%vecfield%get_gders(elem,pnt_set,time,gders_vecfield)
    
    if (this%vecfield%no_dirs() == 3) then
      values(1,:) = gders_vecfield(2,3,:) - gders_vecfield(3,2,:)
      values(2,:) = -(gders_vecfield(1,3,:) - gders_vecfield(3,1,:))
    else
      values(1,:) = 0.0_dp
      values(2,:) = 0.0_dp
    endif
    values(3,:) = gders_vecfield(1,2,:) - gders_vecfield(2,1,:)
  end subroutine get_values
end module curl_mod
