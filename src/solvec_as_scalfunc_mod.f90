module solvec_as_scalfunc_mod
  use assertions, only: assert, BOUNDS_CHECK
  use f90_kind, only: dp
  use mesh_objects, only: elem_list
  use pnt_set_mod, only: pnt_set_t
  use scal_func_mod, only: scal_func_type
  use solvec_mod, only: solvec_t
  implicit none
  
  private
  public :: num_scal_func_t, solvec_as_scalfunc
  
  type, extends(scal_func_type) :: num_scal_func_t
    private
    type(solvec_t), pointer :: solvec => null()
  contains
    procedure, non_overridable :: init
    procedure, non_overridable :: get_values, get_gders
  end type num_scal_func_t
  
contains
  subroutine solvec_as_scalfunc(solvec,scal_func)
    use solvec_mod, only: solvec_t
    
    type(solvec_t), target, intent(in) :: solvec
    class(scal_func_type), allocatable, intent(out) :: scal_func
    
    type(num_scal_func_t) :: num_scal_func
    
    call num_scal_func%init(solvec)
    allocate(scal_func,source=num_scal_func)
  end subroutine solvec_as_scalfunc
  
  subroutine init(this,solvec)
    class(num_scal_func_t), intent(out) :: this
    type(solvec_t), target, intent(in) :: solvec
    
    call assert(solvec%no_dirs() == 1,                                                    &
        'solvec_as_scalfunc: selecting a dir in a vec-valued solvec is not implemented.   &
        &(It would require specialized procedures for evaluating the solvec.)'            &
    )
    
    this%solvec => solvec
  end subroutine init
  
  subroutine get_values(this,elem,pnt_set,time,values)
    class(num_scal_func_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    logical :: only_fluid_mesh
    integer :: elem_no
    integer :: nnod
    real(dp) :: values_all_solvecdirs(1,size(values))

    call assert(size(values) == pnt_set%np(),'scal. eval_at_pnt_set: bad np',BOUNDS_CHECK)
    
    only_fluid_mesh = this%solvec%handler%only_fluid_mesh
    elem_no = elem%get_active_elem_no(only_fluid_mesh)
    nnod = this%solvec%handler%no_nod_elem(elem_no)
    call assert(nnod <= size(pnt_set%dg_funcs,1),'scal. eval_at_pnt_set: not enough nodes stored for values',BOUNDS_CHECK)
    
    call this%solvec%dotprod(elem_no,pnt_set%dg_funcs(:nnod,:),time,values_all_solvecdirs)
    values = values_all_solvecdirs(1,:)
  end subroutine get_values
  
  subroutine get_gders(this,elem,pnt_set,time,gders)
    class(num_scal_func_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:)
    
    logical :: only_fluid_mesh
    integer :: elem_no
    integer :: nnod
    real(dp) :: gders_all_solvecdirs(size(gders,1),1,size(gders,2))
    
    call assert(size(gders,1) == size(pnt_set%dg_transp_gders,2),'scal. eval_at_pnt_set: bad size for dimen',BOUNDS_CHECK)
    call assert(size(gders,2) == pnt_set%np(),'scal. eval_at_pnt_set: bad np',BOUNDS_CHECK)
    
    only_fluid_mesh = this%solvec%handler%only_fluid_mesh
    elem_no = elem%get_active_elem_no(only_fluid_mesh)
    nnod = this%solvec%handler%no_nod_elem(elem_no)
    call assert(nnod <= size(pnt_set%dg_transp_gders,1),'scal. eval_at_pnt_set: not enough nodes stored for gders',BOUNDS_CHECK)

    call this%solvec%matmult(elem_no,pnt_set%dg_transp_gders(:nnod,:,:),time,gders_all_solvecdirs(:,1:1,:))
    gders = gders_all_solvecdirs(:,1,:)
  end subroutine get_gders
end module solvec_as_scalfunc_mod
