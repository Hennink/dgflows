module interface_continuity_mod
  use pde_term_mod, only: pde_term_t
  implicit none
  
  private
  public :: interface_continuity_t
  
  type, extends(pde_term_t) :: interface_continuity_t
    ! Ensures continuity of the temperature profile by taking a penalty term
    ! in the temperature and linearizing it in the volumetric (or specific) enthalpy.
    logical :: explicit
    integer :: time = 1
    integer :: time_density = 1
  contains
    procedure, non_overridable :: process_face
    procedure, non_overridable :: ngbr_coupling_depth
  end type interface_continuity_t
  
contains
  pure integer function ngbr_coupling_depth(this)
    use mesh_type, only: there_is_an_interface

    class(interface_continuity_t), intent(in) :: this

    ngbr_coupling_depth = merge(1, -1, there_is_an_interface .and. .not. this%explicit)
  end function

  subroutine process_face(this,face,Ein_is_E1,qnty,local_linsys)
    ! Imposition of T continuity => penalty integral from temperature SIP form:
    !     o  LHS (if we solve for H): \int_F  "penalty" * [H/(rho*cp)] [v]
    !     o  LHS (if we solve for h): \int_F  "penalty" * [h/(cp)] [v]
    !     o  RHS: - \int_F  "penalty" [v] [T] + \int_F  "penalty" [v] [h/cp]
    use assertions, only: assert
    use code_const, only: INTERFACE_SOLID_FLUID
    use f90_kind, only: dp
    use lincom_rank1_mod, only: lincom_rank1_t
    use local_linsys_mod, only: local_linsys_t
    use mesh_objects, only: face_list
    use numerical_solutions, only: num_scalfunc_set, matprop_set
    use penalty_param_mod, only: gsip_penalty_parameter
    use scal_func_mod, only: scal_func_type
    use vec_func_mod, only: vec_func_type
    use solvec_mod, only: solvec_t

    class(interface_continuity_t), intent(in) :: this
    type(face_list), intent(in) :: face
    logical, intent(in) :: Ein_is_E1
    type(solvec_t), intent(in) :: qnty
    type(local_linsys_t), intent(inout) :: local_linsys
    
    integer :: side_Ein, side_Eout
    real(dp) :: quot_x_penalty_in_k(face%nqp)
    type(lincom_rank1_t) :: values_Ein, values_Eout
    real(dp), dimension(face%nqp) :: quot_Ein, quot_Eout
    class(scal_func_type), allocatable :: k, cp, h, rho
    class(vec_func_type), allocatable :: T
    real(dp), dimension(face%nqp) :: rho_E1, rho_E2
    real(dp), dimension(face%nqp) :: cp_Ein, cp_Eout
    real(dp), dimension(face%nqp) :: h_Ein, h_Eout
    real(dp), dimension(1,face%nqp) :: jump_T, jump_h_over_cp
    
    if (.not. face%ftype == INTERFACE_SOLID_FLUID) return
    
    call assert(qnty%name == 'vol_enthalpy' .or. qnty%name=='spec_enthalpy', &
                'NOT IMPLEMENTED: interface continuity for '//qnty%name)
    
    if (Ein_is_E1) then
      side_Ein = 1; side_Eout = 2
    else
      side_Ein = 2; side_Eout = 1
    endif
    
    if (qnty%name=='spec_enthalpy') then
      call num_scalfunc_set%get_func('spec_enthalpy',h)
    else
      call matprop_set%get_func('spec_enthalpy',h)
    endif
    call matprop_set%get_func('thermal_conductivity',k)
    call matprop_set%get_func('spec_heat_capacity',cp)
    call matprop_set%get_func('temperature',T)
    
    call T%jump_at_qps(face,.false.,this%time,jump_T)
    if (Ein_is_E1) then
      call cp%values_at_qps_E1_E2(face,.false.,this%time,cp_Ein,cp_Eout)
      call h%values_at_qps_E1_E2(face,.false.,this%time,h_Ein,h_Eout)
    else
      call cp%values_at_qps_E1_E2(face,.false.,this%time,cp_Eout,cp_Ein)
      call h%values_at_qps_E1_E2(face,.false.,this%time,h_Eout,h_Ein)
      jump_T = -jump_T
    endif
    quot_x_penalty_in_k = face%quot * gsip_penalty_parameter(face,qnty%handler,k,this%time)
    quot_Ein = quot_x_penalty_in_k / cp_Ein
    quot_Eout = quot_x_penalty_in_k / cp_Eout
    
    if (qnty%name=='vol_enthalpy') then
      call matprop_set%get_func('density',rho)
      rho_E1 = rho%values_at_qps(face,side=1,time=this%time_density)
      rho_E2 = rho%values_at_qps(face,side=2,time=this%time_density)
      if (Ein_is_E1) then
        quot_Ein = quot_Ein / rho_E1
        quot_Eout = quot_Eout / rho_E2
      else
        quot_Ein = quot_Ein / rho_E2
        quot_Eout = quot_Eout / rho_E1
      endif
    endif
    
    call values_Ein%set_as_func(face,side_Ein,qnty%handler)
    call values_Eout%set_as_func(face,side_Eout,qnty%handler)
    call local_linsys%add_int_dotprod_to_lhs(values_Ein,values_Ein,quot_Ein,1.0_dp,this%explicit)
    call local_linsys%add_int_dotprod_to_lhs(values_Ein,values_Eout,quot_Eout,-1.0_dp,this%explicit)
    
    jump_h_over_cp = reshape(h_Ein/cp_Ein-h_Eout/cp_Eout, shape(jump_h_over_cp))
    call local_linsys%add_int_dotprod_to_rhs(values_Ein,jump_T,quot_x_penalty_in_k,-1.0_dp)
    call local_linsys%add_int_dotprod_to_rhs(values_Ein,jump_h_over_cp,quot_x_penalty_in_k,1.0_dp)
  end subroutine process_face
end module interface_continuity_mod
