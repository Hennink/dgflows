module dyad_axpy_mod
  use dyad_mod, only: dyad_t
  use f90_kind, only: dp
  implicit none

  private
  public :: dyad_axpy_t

  type, extends(dyad_t) :: dyad_axpy_t
    real(dp) :: alpha
  contains
    procedure, non_overridable :: func
    procedure, non_overridable :: deriv_wrt_arg1, deriv_wrt_arg2
  end type dyad_axpy_t

contains
  elemental real(dp) function func(this,arg1,arg2)
    class(dyad_axpy_t), intent(in) :: this
    real(dp), intent(in) :: arg1, arg2

    func = arg1 + this%alpha * arg2
  end function func

  elemental real(dp) function deriv_wrt_arg1(this,arg1,arg2)
    class(dyad_axpy_t), intent(in) :: this
    real(dp), intent(in) :: arg1, arg2

    deriv_wrt_arg1 = 1.0_dp
  end function deriv_wrt_arg1

  elemental real(dp) function deriv_wrt_arg2(this,arg1,arg2)
    class(dyad_axpy_t), intent(in) :: this
    real(dp), intent(in) :: arg1, arg2

    deriv_wrt_arg2 = this%alpha
  end function deriv_wrt_arg2
end module dyad_axpy_mod
