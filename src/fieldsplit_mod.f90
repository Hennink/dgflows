module fieldsplit_mod
  ! Module to solve the coupled NSE:
  !      _           _   _ _     _ _
  !     |  A_00 A_01  | | m |   | f |
  !     |             | |   | = |   | ,
  !     |_ A_10 A_11 _| |_p_|   |_g_|
  ! where
  !     A_00  =  momentum matrix
  !     A_10  =  D    =  divergence
  !     A_01  =  D^T  =  gradient
  !     A_11  =  C    =  pressure stabilization
  !     f     =  momentum rhs (no pressure gradient)
  !     g     =  pressure rhs (includes d/dt rho)
  use assertions, only: assert
  use div_cont_penalties_and_err, only: init_M_corr_linsys, solve_M_corr_linsys
  use dof_handler, only: dof_handler_type
  use f90_kind, only: dp
  use mass_mtx, only: get_inv_mass_matrix
  use math_const, only: SMALL
  use mesh_type, only: fem_mesh
  use numerical_solutions, only: pressure_needs_penalization, get_solvec, temporal_order_for_qnty, &
                                 nullspace_is_nontrivial => pressure_mtx_is_singular
  use petsc_ksp_mod, only: petsc_ksp_t
  use petsc_mod, only: I_am_master_process
  use petsc_is_mod, only: petsc_is_t
  use petsc_mat_mod, only: petsc_mat_type, destroy_matrix, get_matnest_ISs, &
      SPD, SYMMETRIC, SYMMETRY_ETERNAL, SHARE_NONZERO_PATTERN, same_nonzero_pattern_opt, subset_nonzero_pattern_opt
  use petsc_solver_mod, only: set_up_FIELDSPLIT_solver, solve_system
  use petsc_vec_mod, only: petsc_vec_type
  use pressure, only: rhs_continuity_eq, init_pressure_nullspace_vec, &
                      const_pressure_proj, A_01 => gradient_oper
  use pressure_corr, only: create_p_penalty_mat, init_SIP_p_mtx, init_LDG_p_mtx, penalty_is_constant_from_now_on
  use run_data, only: check_mat_symm, use_approx_schur_PC, use_approx_schur_KSP, add_m_penalties, m_penalties_in
  use solvec_mod, only: solvec_t
  use timer_typedef, only: timer_type
  use qnty_solver_mod, only: qnty_solver_t
  implicit none
  
  private
  public :: solve_NS_coupled_sys

  type(timer_type), public, protected :: FS_ass_solve_timer(3) ! [total, ass, solve]
  
  type(petsc_ksp_t), allocatable :: ksp
  type(petsc_is_t), allocatable :: field_ISs(:)
  type(petsc_mat_type), allocatable :: A

  type(petsc_mat_type) :: A_10, A_11
  type(petsc_mat_type), target :: min_a_over_dt_LDG ! = -(alpha/dt) * D M^-1 D^T  (approx_S_noA11)
  type(petsc_mat_type), target :: approx_S
  type(petsc_vec_type) :: nullspace_vec

  logical :: initialized_module = .false.
  logical :: penalty_has_been_precalculated = .false.
  logical :: solver_is_constant_from_now_on = .false.
  
contains
  subroutine solve_NS_coupled_sys(mesh, dt, M_solver, rel_diff_sol)
    ! Assembles only the time-dependent parts and 
    ! solves the coupled system using the PCFIELDSPLIT provided by PETSc (see [1-2]).
    !
    ! [1] https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/PC/PCFIELDSPLIT.html
    ! [2] https://www.mcs.anl.gov/petsc/petsc-current/src/snes/examples/tutorials/ex70.c.html
    type(fem_mesh), intent(in) :: mesh
    real(dp), intent(in) :: dt
    class(qnty_solver_t), target, intent(inout) :: M_solver
    real(dp), intent(out) :: rel_diff_sol
    
    type(petsc_mat_type), pointer :: A_00
    type(petsc_vec_type) :: x, b, f, g
    character(1) :: field_names(2)
    type(solvec_t), pointer :: P, M
    real(dp) :: x_old_norm, b_norm
    real(dp) :: init_rel_norm_res, final_rel_norm_res
    type(petsc_vec_type) :: sol_diff
    type(petsc_mat_type), pointer :: ptr_approx_S
    logical :: not_null_mat(4)

    call FS_ass_solve_timer(1)%start()

    if (.not. initialized_module) then
      call init_fieldsplit(mesh)
      initialized_module = .true.
    endif

    field_names = ["M","P"]
    M => get_solvec('mass_flux')
    P => get_solvec('pressure')
    
    ! Assemble A_00 and f.
    call FS_ass_solve_timer(2)%start()
    call M_solver%assemble_glob_linsys(mesh, f)
    A_00 => M_solver%lhs
    call FS_ass_solve_timer(2)%pauze()
    if (check_mat_symm .and. M_solver%expect_symm) call assert(A_00%is_symm(),'Coupled solver: mass_flux matrix is not symmetric')
    
    if (solver_is_constant_from_now_on) call assert(m_solver%lhs_is_const_from_now_on, 'expected M matrix to remain constant.')
    if (.not. solver_is_constant_from_now_on) then
      allocate(ksp, field_ISs(2), A)

      ! Construct approximated Schur:
      if (.not. pressure_needs_penalization) then
        ptr_approx_S => min_a_over_dt_LDG
      elseif (penalty_has_been_precalculated) then
        ptr_approx_S => approx_S
      else
        ! Substitute A_11 with pressure stabilization matrix:
        call create_p_penalty_mat(mesh,P%handler,A_11)
        call A_11%mult_with_scal(-1.0_dp)
        if (check_mat_symm) call assert(A_11%is_symm(),'Coupled solver: A_11 is not symmetric')
        
        call min_a_over_dt_LDG%duplicate(SHARE_NONZERO_PATTERN, approx_S)
        call min_a_over_dt_LDG%copy(approx_S, same_nonzero_pattern_opt)
        call approx_S%axpy(1.0_dp, A_11, subset_nonzero_pattern_opt)
        call approx_S%set_option(SPD,.true.)
        call approx_S%set_option(SYMMETRIC,.true.)
        call approx_S%set_option(SYMMETRY_ETERNAL,.true.)
        ptr_approx_S => approx_S

        penalty_has_been_precalculated = penalty_is_constant_from_now_on()
        if (penalty_has_been_precalculated) call destroy_matrix(min_a_over_dt_LDG)
        if (penalty_has_been_precalculated .and. i_am_master_process()) &
            write(*,'(/,a,/)') '======== PRECALCULATED PRESSURE MATRICES'
      endif
      
      ! Set up the coupled matrix:
      not_null_mat = [.true., .true., .true., pressure_needs_penalization]
      call A%create_nest([A_00,A_01,A_10,A_11], not_null_mat, nr=2, nc=2)
      if (nullspace_is_nontrivial()) call A%set_nullspace_vec(nullspace_vec)

      ! Set up the KSP:
      call get_matnest_ISs(A,field_ISs)
      call set_up_FIELDSPLIT_solver(ksp, A, A, field_names, field_ISs, ptr_approx_S)
      
      if ((penalty_has_been_precalculated .or. .not. pressure_needs_penalization) .and. m_solver%lhs_is_const_from_now_on) then
        solver_is_constant_from_now_on = .true.
        if (i_am_master_process()) write(*,'(/,a,/)') '======== PRECALCULATED FIELDSPLIT KSP AND MATRIX'
      endif
    endif

    ! Assemble the rhs:
    call A_10%init_compatible_vecs(left_vec=g)
    call rhs_continuity_eq(mesh,P%handler,dt,g)
    call b%create_nest([f,g])

    call x%create_nest([M%petsc_coeffs,P%petsc_coeffs]) !This is done to initialize the solution properly
    call x%duplicate(sol_diff)
    call x%copy(sol_diff)
    
    ! Solve the system:
    init_rel_norm_res = A%rel_norm_residual(x,b)
    x_old_norm = x%norm_2()
    b_norm = b%norm_2()
    if (I_am_master_process()) then
      print *, 'norm(solution) = ',  x_old_norm
      print *, 'norm(rhs) = ', b_norm
    endif
    call FS_ass_solve_timer(3)%start()
    call solve_system(ksp, x, b)
    call FS_ass_solve_timer(3)%pauze()
    final_rel_norm_res = A%rel_norm_residual(x,b)
    if (I_am_master_process()) &
                write(*,'(a,es8.1,a,es8.1)') 'rel. norm residual (before/after): ',init_rel_norm_res,'/',final_rel_norm_res
    
    ! Compute solution relative difference
    call sol_diff%subtract_vec(x)
    rel_diff_sol = sol_diff%norm_2() / max(x_old_norm,SMALL)
    
    ! Update our solvecs
    call x%get_copy_of_subvec(field_ISs(1),M%petsc_coeffs)
    call x%get_copy_of_subvec(field_ISs(2),P%petsc_coeffs)
    call M%update_ghost_values()
    call P%update_ghost_values()
    
    if (.not. solver_is_constant_from_now_on) deallocate(ksp, field_ISs, A)

    ! Penalize the mass-flux in a post-processing step, if necessary
    if (add_m_penalties > 0 .and. m_penalties_in == 'm-postproc') call solve_M_corr_linsys(mesh,dt)

    call FS_ass_solve_timer(1)%start()
  end subroutine solve_NS_coupled_sys
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine init_fieldsplit(mesh)
    ! Assembles time-independent parts of the lhs of the coupled system.
    use exceptions, only: warning
    use phys_time_mod, only: ith_BDF_weight
    use run_data, only: dt, replace_local_dg_by_sip

    type(fem_mesh), intent(in) :: mesh
    
    type(solvec_t), pointer :: M, P
    type(petsc_vec_type) :: null_m_vec
    type(petsc_mat_type) :: inv_mass_matrix
    real(dp) :: alpha_over_dt

    M => get_solvec('mass_flux')
    P => get_solvec('pressure')
    
    if (I_am_master_process()) print *, 'Init fieldsplit matrices...'
    
    ! Assemble A_01 (gradient)
    call A_01%get_transpose(A_10)
    
    if (nullspace_is_nontrivial()) then
      call init_pressure_nullspace_vec(mesh,A_01,P%handler)
      call A_01%init_compatible_vecs(left_vec=null_m_vec)
      call nullspace_vec%create_nest([null_m_vec,const_pressure_proj])
    endif
    
    ! Construct approx Schur complement, using the same matrices we use for pressure-correction (LDG or SIP)
    ! This is done because it resembles, for a DG discretization, the SIMPLE-based approximation described in [1]
    !
    ! [1] Klaij C.M. et al., On the design of block preconditioners for maritime engineering, 
    !     VII Int. Conf. on Computational Methods in Marine Engineering, MARINE 2017, pp. 893-904
    if (use_approx_schur_PC .or. use_approx_schur_KSP) then
      if (replace_local_dg_by_sip) then
        if (i_am_master_process()) call warning('The SIP matrix is usually unferior than LDG as an approximate Schur compliment.')
        call init_SIP_p_mtx(mesh, P, min_a_over_dt_LDG)
      else
        call get_inv_mass_matrix(mesh,M%handler,inv_mass_matrix)
        call init_LDG_p_mtx(mesh, P%handler, inv_mass_matrix, min_a_over_dt_LDG)
      endif
      alpha_over_dt = ith_BDF_weight(temporal_order_for_qnty('mass_flux'), dt, i=1)
      call min_a_over_dt_LDG%mult_with_scal(-1 / alpha_over_dt)
      
      call min_a_over_dt_LDG%set_option(SPD,.true.)
      call min_a_over_dt_LDG%set_option(SYMMETRIC,.true.)
      call min_a_over_dt_LDG%set_option(SYMMETRY_ETERNAL,.true.)
    endif
    
    if (add_m_penalties > 0 .and. m_penalties_in == 'm-postproc') call init_M_corr_linsys(mesh)
    
    if (I_am_master_process()) print *, 'done.'
  end subroutine init_fieldsplit
end module fieldsplit_mod

