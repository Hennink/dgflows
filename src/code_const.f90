module code_const
  !---------------------------------------------------------------------
  ! This module contains code parameters
  !---------------------------------------------------------------------
  use, intrinsic :: iso_fortran_env
  use f90_kind
  implicit none
  
  real(dp), parameter :: UNINITIALIZED_REAL = - (huge(1.0_dp) / 2) ! Not NAN, because we want to use `x == UNINITIALIZED_REAL`
  
  character(*), parameter :: UNINITIALIZED_CHAR = "UNINITIALIZED"
  ! `UNINITIALIZED_INT` is not `huge(1)` because of underflow/overflow, and prevent compiler warning about integer division:
  integer, parameter :: UNINITIALIZED_INT = - (huge(1)-mod(huge(1),2)) / 2

  
! Numerical flux chosen for convection
  integer, parameter :: LAX_FRIEDRICHS = 0
  integer, parameter :: UPWIND_CONSERVATIVE = 1
  
  ! Flags to distinguish the different types of faces we have to handle
  integer, parameter :: INTERNAL = 11
  integer, parameter :: BOUNDARY = 22
  integer, parameter :: INTERFACE_SOLID_FLUID = 33
  integer, parameter :: INTERFACE_WITH_IGNORED = 44
  
  ! Constants useful to compare elem ids
  integer, parameter :: EQUAL = 13
  integer, parameter :: FINER = 26
  integer, parameter :: COARSER = 39
end module code_const
