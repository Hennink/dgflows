module monad_dyad_lib_mod
  use f90_kind, only: dp
  use dyad_mod, only: dyad_t
  use monad_mod, only: monad_t
  implicit none
  
  private
  public :: power_monad_t, sqrt_monad_t, max_monad_t
  public :: add_dyad, subtract_dyad
  
  type, extends(monad_t) :: power_monad_t
    integer :: expon
  contains
    procedure, non_overridable :: value => square
  end type power_monad_t
  
  type, extends(monad_t) :: sqrt_monad_t
  contains
    procedure, non_overridable :: value => square_root
  end type sqrt_monad_t
  
  type, extends(monad_t) :: max_monad_t
    real(dp) :: minimum
  contains
    procedure, non_overridable :: value => maximize
  end type max_monad_t

  type, extends(dyad_t) :: add_dyad_t
  contains
    procedure, non_overridable :: func => add
  end type add_dyad_t
  
  type, extends(dyad_t) :: subtract_dyad_t
  contains
    procedure, non_overridable :: func => subtract
  end type subtract_dyad_t
  
  type(add_dyad_t), protected :: add_dyad
  type(subtract_dyad_t), protected :: subtract_dyad
  
contains
  elemental real(dp) function square(this,arg)
    class(power_monad_t), intent(in) :: this
    real(dp), intent(in) :: arg
    
    square = arg**this%expon
  end function square
  
  elemental real(dp) function square_root(this,arg)
    class(sqrt_monad_t), intent(in) :: this
    real(dp), intent(in) :: arg
    
    square_root = sqrt(arg)
  end function square_root
  
  elemental real(dp) function maximize(this,arg)
    class(max_monad_t), intent(in) :: this
    real(dp), intent(in) :: arg

    maximize = max(arg,this%minimum)
  end function maximize

  elemental real(dp) function add(this,arg1,arg2)
    class(add_dyad_t), intent(in) :: this
    real(dp), intent(in) :: arg1, arg2
  
    add = arg1 + arg2
  end function add
  
  elemental real(dp) function subtract(this,arg1,arg2)
    class(subtract_dyad_t), intent(in) :: this
    real(dp), intent(in) :: arg1, arg2
    
    subtract = arg1 - arg2
  end function subtract
end module monad_dyad_lib_mod
