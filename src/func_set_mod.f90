module func_set_mod
  use assertions, only: assert, assert_eq
  use exceptions, only: error
  use scal_func_mod, only: scal_func_type
  use string_mod, only: string_t, as_chars
  use vec_func_mod, only: vec_func_type
  use quantities_mod, only: nquantities, idx, idx2name
  implicit none
  
  private
  public :: func_set_t
  
  type :: slot_t
    private
    class(vec_func_type), allocatable :: vecfunc
    class(scal_func_type), allocatable :: scalfunc
  contains
#ifdef __INTEL_COMPILER
    ! ifort17 had a bug where it failed to free the allocatable components sometimes.
    ! gfortran9 had an internal error at compile time with these final procedures.
    final :: dealloc_slot
#endif
  end type slot_t
  
  type func_set_t
    private
    type(slot_t) :: slots(nquantities)
  contains
    procedure, non_overridable :: func_ptr
    generic :: get_func => get_scalfunc, get_vecfunc
    procedure, non_overridable, private :: get_scalfunc, get_vecfunc
    
    generic :: add_func => add_scalfunc, add_vecfunc
    procedure, non_overridable, private :: add_scalfunc, add_vecfunc
    
    procedure, non_overridable :: has, has_both, get_names
    
    procedure, non_overridable :: union, merge_exl_duplicates
    procedure, non_overridable :: expand_with_known_relations
    procedure, non_overridable :: delete, reduced_set
    
    generic, public :: assignment(=) => assign_func_set
    procedure, non_overridable, private, pass(variable) :: assign_func_set
    
    procedure, non_overridable :: write_to_gmsh_files
    
#ifdef __INTEL_COMPILER
    final :: reset_func_set
#endif
  end type func_set_t
  
contains
  function func_ptr(this,name)
    ! Returns a pointer to the scalfunc in the set, assuming it's there.
    class(func_set_t), target, intent(in) :: this
    character(*), intent(in) :: name
    class(scal_func_type), pointer :: func_ptr

    integer :: i

    i = idx(name)
    call assert(allocated(this%slots(i)%scalfunc),'func_ptr: is not listed as a scalar; name=' // name)
    func_ptr => this%slots(i)%scalfunc
  end function

  subroutine get_scalfunc(this,name,scalfunc)
    class(func_set_t), intent(in) :: this
    character(*), intent(in) :: name
    class(scal_func_type), allocatable, intent(out) :: scalfunc
    
    integer :: i

    i = idx(name)
    call assert(allocated(this%slots(i)%scalfunc),'get_scalfunc: is not listed as a scalar; name=' // name)
    allocate(scalfunc, source = this%slots(i)%scalfunc)
  end subroutine
  
  subroutine get_vecfunc(this,name,vecfunc)
    use vec_func_scal_compon_mod, only: as_vecfunc

    class(func_set_t), intent(in) :: this
    character(*), intent(in) :: name
    class(vec_func_type), allocatable, intent(out) :: vecfunc
    
    associate(slot => this%slots(idx(name)))
      if (allocated(slot%vecfunc)) then
        allocate(vecfunc, source = slot%vecfunc)
      elseif (allocated(slot%scalfunc)) then
        vecfunc = as_vecfunc(slot%scalfunc)
      else
        call error('get_vecfunc: not listed: name=' // name)
      endif
    end associate
  end subroutine

  subroutine add_scalfunc(this,name,scalfunc)
    class(func_set_t), intent(inout) :: this
    character(*), intent(in) :: name
    class(scal_func_type), intent(in) :: scalfunc
    
    associate(slot => this%slots(idx(name)))
      call assert(.not. allocated(slot%scalfunc) .and. .not. allocated(slot%vecfunc),"scalar already in funcset; name=" // name)
      allocate(slot%scalfunc, source = scalfunc)
    end associate
  end subroutine
  
  subroutine add_vecfunc(this,name,vecfunc)
    class(func_set_t), intent(inout) :: this
    character(*), intent(in) :: name
    class(vec_func_type), intent(in) :: vecfunc
    
    associate(slot => this%slots(idx(name)))
      call assert(.not. allocated(slot%scalfunc) .and. .not. allocated(slot%vecfunc),"vector already in funcset; name=" // name)
      if (vecfunc%no_dirs() == 1) then
        call vecfunc%select_dir(dir=1,scalfunc=slot%scalfunc)
      else
        allocate(slot%vecfunc, source = vecfunc)
      endif
    end associate
  end subroutine
  
  subroutine union(this,func_set)
    ! this <-- union(this,func_set)
    class(func_set_t), intent(inout) :: this
    class(func_set_t), intent(in) :: func_set
    
    integer :: i
    logical :: has_scal, has_vec
    
    do i = 1, nquantities
      associate(                          &
        this_slot => this%slots(i),       &
        other_slot => func_set%slots(i)   &
      )
        has_scal = allocated(other_slot%scalfunc)
        has_vec = allocated(other_slot%vecfunc)
        if (has_scal .or. has_vec) then
          call assert(.not. allocated(this_slot%scalfunc) .and. .not. allocated(this_slot%vecfunc),'already had qnty')
        endif

        if (has_scal .and. has_vec) then
          call error('both vec and scal allocated')
        elseif (has_scal) then
          allocate(this_slot%scalfunc, source = other_slot%scalfunc)
        elseif (has_vec) then
          allocate(this_slot%vecfunc, source = other_slot%vecfunc)
        endif
      end associate
    enddo
  end subroutine
  
  subroutine merge_exl_duplicates(this,func_set)
    class(func_set_t), intent(inout) :: this
    class(func_set_t), intent(in) :: func_set

    integer :: i
    logical :: has_scal, has_vec
    
    do i = 1, nquantities
      associate(                          &
        this_slot => this%slots(i),       &
        other_slot => func_set%slots(i)   &
      )
        if (allocated(this_slot%scalfunc) .or. allocated(this_slot%vecfunc)) cycle

        has_scal = allocated(other_slot%scalfunc)
        has_vec = allocated(other_slot%vecfunc)
        if (has_scal .and. has_vec) then
          call error('both vec and scal allocated')
        elseif (has_scal) then
          allocate(this_slot%scalfunc, source = other_slot%scalfunc)
        elseif (has_vec) then
          allocate(this_slot%vecfunc, source = other_slot%vecfunc)
        endif
      end associate
    enddo
  end subroutine

  subroutine expand_with_known_relations(this)
    use func_operations_mod, only: operator(*), operator(/)
    
    class(func_set_t), intent(inout) :: this
    
    class(vec_func_type), allocatable :: v
    class(scal_func_type), allocatable :: s1, s2
    
    if (.not. this%has('vol_enthalpy') & 
        .and. this%has_both('spec_enthalpy','density')) then
      call this%get_scalfunc('spec_enthalpy',s1)
      call this%get_scalfunc('density',s2)
      call this%add_scalfunc('vol_enthalpy',s1*s2)
    endif
    
    if (.not. this%has('spec_enthalpy') & 
        .and. this%has_both('vol_enthalpy','density')) then
      call this%get_scalfunc('vol_enthalpy',s1)
      call this%get_scalfunc('density',s2)
      call this%add_scalfunc('spec_enthalpy',s1/s2)
    endif
    
    
    if (.not. this%has('mass_flux') &
        .and. this%has_both('velocity','density')) then
      call this%get_vecfunc('velocity',v)
      call this%get_scalfunc('density',s1)
      call this%add_vecfunc('mass_flux',s1*v)
    endif
    
    if (.not. this%has('velocity') &
        .and. this%has_both('mass_flux','density')) then
      call this%get_vecfunc('mass_flux',v)
      call this%get_scalfunc('density',s1)
      call this%add_vecfunc('velocity',v/s1)
    endif
    
    if (.not. this%has('vol_turb1') & 
        .and. this%has_both('spec_turb1','density')) then
      call this%get_scalfunc('spec_turb1',s1)
      call this%get_scalfunc('density',s2)
      call this%add_scalfunc('vol_turb1',s1*s2)
    endif
    
    if (.not. this%has('spec_turb1') & 
        .and. this%has_both('vol_turb1','density')) then
      call this%get_scalfunc('vol_turb1',s1)
      call this%get_scalfunc('density',s2)
      call this%add_scalfunc('spec_turb1',s1/s2)
    endif
    
    if (.not. this%has('vol_turb2') & 
        .and. this%has_both('spec_turb2','density')) then
      call this%get_scalfunc('spec_turb2',s1)
      call this%get_scalfunc('density',s2)
      call this%add_scalfunc('vol_turb2',s1*s2)
    endif
    
    if (.not. this%has('spec_turb2') & 
        .and. this%has_both('vol_turb2','density')) then
      call this%get_scalfunc('vol_turb2',s1)
      call this%get_scalfunc('density',s2)
      call this%add_scalfunc('spec_turb2',s1/s2)
    endif
    
    
    if (.not. this%has('vol_force') &
        .and. this%has_both('spec_force','density')) then
      call this%get_vecfunc('spec_force',v)
      call this%get_scalfunc('density',s1)
      call this%add_vecfunc('vol_force',s1*v)
    endif
    
    if (.not. this%has('spec_force') &
        .and. this%has_both('vol_force','density')) then
      call this%get_vecfunc('vol_force',v)
      call this%get_scalfunc('density',s1)
      call this%add_vecfunc('spec_force',v/s1)
    endif
  end subroutine expand_with_known_relations
  
  subroutine delete(this,names)
    class(func_set_t), intent(inout) :: this
    type(string_t), dimension(:), intent(in) :: names
    
    integer :: n, i
    
    do n = 1, size(names)
      i = idx(names(n)%get())
      call dealloc_slot(this%slots(i))
    enddo
  end subroutine
  
  type(func_set_t) function reduced_set(this,name_to_leave_out)
    class(func_set_t), intent(in) :: this
    character(*), intent(in) :: name_to_leave_out
    
    integer :: i, leftout
    
    leftout = idx(name_to_leave_out)
    do i = 1, nquantities
      if (i == leftout) cycle

      associate(                              &
          this_slot => this%slots(i),         &
          other_slot => reduced_set%slots(i)  &
      )
        if (allocated(this_slot%scalfunc))  allocate(other_slot%scalfunc,source=this_slot%scalfunc)
        if (allocated(this_slot%vecfunc))   allocate(other_slot%vecfunc,source=this_slot%vecfunc)
      end associate
    enddo
  end function reduced_set
  
  impure elemental logical function has(this,name)
    class(func_set_t), intent(in) :: this
    character(*), intent(in) :: name
    
    associate(slot => this%slots(idx(name)))
      has = allocated(slot%scalfunc) .or. allocated(slot%vecfunc)
    end associate
  end function
  
  impure elemental logical function has_both(this,name1,name2)
    class(func_set_t), intent(in) :: this
    character(*), intent(in) :: name1, name2
    
    has_both = this%has(name1) .and. this%has(name2)
  end function
  
  subroutine get_names(this,names)
    class(func_set_t), intent(in) :: this
    type(string_t), allocatable, intent(out) :: names(:)
    
    integer :: i
    logical :: avail(nquantities)
    type(string_t), allocatable :: all_names(:)
    
    all_names = idx2name([(i, i = 1, nquantities)])

    do i = 1, nquantities
      avail(i) = allocated(this%slots(i)%scalfunc) .or. allocated(this%slots(i)%vecfunc)
    enddo
    allocate(names(count(avail)))
    names = pack(all_names,mask=avail)
  end subroutine
  
  subroutine assign_func_set(variable,expression)
    class(func_set_t), intent(out) :: variable
    class(func_set_t), intent(in) :: expression
    
    call variable%union(expression)
  end subroutine
  
  subroutine write_to_gmsh_files(this,mesh,prefix)
    use gmsh_interface, only: write_to_gmsh_file
    use mesh_type, only: fem_mesh
    
    class(func_set_t), intent(in) :: this
    type(fem_mesh), intent(in) :: mesh
    character(*), intent(in) :: prefix
    
    integer, parameter :: TIME = 1
    logical, parameter :: ONLY_FLUID = .true.
    type(string_t), allocatable :: names(:)
    character(:), allocatable :: char_names(:)
    
    call this%get_names(names)
    char_names = as_chars(names)
    call write_to_gmsh_file(mesh,prefix // char_names,ONLY_FLUID,get_vals)
  
  contains
    subroutine get_vals(elem,pnt_set,vals)
      use f90_kind, only: dp
      use mesh_objects, only: elem_list
      use pnt_set_mod, only: pnt_set_t
      
      type(elem_list), intent(in) :: elem
      type(pnt_set_t), intent(in) :: pnt_set
      real(dp), intent(out) :: vals(:,:)
      
      integer :: n, nnames
      integer :: i
      
      call assert(size(vals,1) == pnt_set%np(),'write_to_gmsh_files: mismatch # points')
      nnames = assert_eq(size(vals,2),size(names),'write_to_gmsh_files: mismatch # vals')
      call assert(elem%is_fluid(),'I thought you were only plotting func_sets in a fluid.')

      do n = 1, nnames
        i = idx(char_names(n))
        associate(slot => this%slots(i))
          call assert(.not. allocated(slot%vecfunc),'Cannot print vecfuncs')
          call slot%scalfunc%get_values(elem,pnt_set,TIME,vals(:,n))
        end associate
      enddo
    end subroutine get_vals
  end subroutine write_to_gmsh_files

  impure elemental subroutine dealloc_slot(this)
    type(slot_t), intent(inout) :: this
    
    if (allocated(this%scalfunc)) deallocate(this%scalfunc)
    if (allocated(this%vecfunc)) deallocate(this%vecfunc)
  end subroutine
  
#ifdef __INTEL_COMPILER
  impure elemental subroutine reset_func_set(this)
    type(func_set_t), intent(inout) :: this
    
    call dealloc_slot(this%slots)
  end subroutine reset_func_set
#endif
end module func_set_mod
