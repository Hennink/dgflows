submodule(boundary_types) boundary_types__noslip_htc_wall
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only: vec_func_type
  implicit none
  
  type, extends(wall_bnd_t) :: noslip_htc_wall_t
    class(vec_func_type), allocatable :: velocity
    class(scal_func_type), allocatable :: htc
    class(scal_func_type), allocatable :: T_ext
  contains
    procedure :: read_specs_from_file
    procedure :: get_bc
  end type noslip_htc_wall_t
  
contains
  module procedure noslip_htc_wall
    allocate(noslip_htc_wall_t :: bnd)
  end procedure noslip_htc_wall
  
  subroutine read_specs_from_file(this,unit_number)
    use io_basics, only: read_line_from_input_file
    use read_funcs_mod, only: read_line_as_qnty_eq_func
    
    class(noslip_htc_wall_t), intent(out) :: this
    integer, intent(in) :: unit_number
    
    call read_line_as_qnty_eq_func(unit_number,'velocity',this%velocity)
    call read_line_as_qnty_eq_func(unit_number,'htc',this%htc)
    call read_line_as_qnty_eq_func(unit_number,'T_external',this%T_ext)
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types, only: bc_type, dirichlet_bc_type, robin_bc_type
    use boundary_types_support_mod, only: assemble_htc_robin_bc
    use exceptions, only: error
    use func_operations_mod
    use functions_lib, only: zero_vecfunc
    use numerical_solutions, only: matprop_set
    use rans_mod, only: rans_model
    use scal_func_mod, only: scal_func_type
    
    class(noslip_htc_wall_t), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    
    type(dirichlet_bc_type), allocatable :: dirichlet_bc
    type(robin_bc_type), allocatable :: robin_bc
    class(scal_func_type), allocatable :: rho
    
    allocate(dirichlet_bc)
    select case(qnty_name)
      case('vol_enthalpy','spec_enthalpy','temperature')
        call assemble_htc_robin_bc(qnty_name,this%htc,this%T_ext,robin_bc)
        call move_alloc(robin_bc,bc)
      case('mass_flux','velocity')
        call matprop_set%get_func('density',rho)
        allocate(dirichlet_bc%inhom_part,source= rho * this%velocity)
        call move_alloc(dirichlet_bc,bc)
      case('vol_turb1','vol_turb2')
        if (rans_model%is_low_Re()) then
          allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(1))
          call move_alloc(dirichlet_bc,bc)
        else
          call error("NOT IMPLEMENTED: qnty_name=" // qnty_name)
        endif
      case('wall_dist')
        allocate(dirichlet_bc%inhom_part,source=zero_vecfunc(1))
        call move_alloc(dirichlet_bc,bc)
      case default
        write(*,'(a)') 'ERROR: no bc for '//qnty_name//' in noslip_htc_wall'
        error stop
    end select
  end subroutine get_bc
end submodule boundary_types__noslip_htc_wall
