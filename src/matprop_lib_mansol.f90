module matprop_lib_mansol
  ! Not particularly realistic, but used for a manufactured solution.
  !
  ! Constant heat capacity:
  !     T = h / cp + T0
  !     h = cp * (T - T0)
  ! where 'T0' is the reference temperature.
  use exceptions, only: error
  use f90_kind, only: dp
  implicit none

  private
  public :: get_mansol_matprop

  real(dp), parameter :: UNINITIALIZED = huge(1.0_dp)
  character(*), parameter :: UNINITIALIZED_CHAR = 'UNINITIALIZED'
  real(dp):: T0 = UNINITIALIZED, cp = UNINITIALIZED, &
             mu0 = UNINITIALIZED, prandtl = UNINITIALIZED, &
             rho0 = UNINITIALIZED, rho1 = UNINITIALIZED, &
             T_rho_power = UNINITIALIZED
  character(200) :: rho_props = UNINITIALIZED_CHAR, transport_props = UNINITIALIZED_CHAR
  logical :: cap_transport_props = .false.

contains
  subroutine get_mansol_matprop(unit,matprop_set)
    use assertions, only: assert
    use func_set_expansion_mod, only: func_set_expansion_t
    use functions_lib, only: const_func, mansol_scalfunc_t

    integer, intent(in) :: unit
    type(func_set_expansion_t), intent(out) :: matprop_set

    logical :: exact_transport_props, exact_rho
    integer :: iostat
    character(200) :: iomsg
    namelist /matprop_specs/  cp, T0, rho0, rho1, T_rho_power, &
                              mu0, prandtl, &
                              rho_props, transport_props, &
                              cap_transport_props, &
                              exact_transport_props, exact_rho

    ! Set defaults:
    exact_transport_props = .false.
    exact_rho = .false.
    
    ! Read the file:
    read(unit,matprop_specs,iostat=iostat,iomsg=iomsg)
    call assert(iostat==0,iomsg)
    
    ! Sanity checks:
    call assert(transport_props /= UNINITIALIZED_CHAR,"mansol matprop: transport_props was not initialized")
    call assert(rho_props /= UNINITIALIZED_CHAR,"mansol matprop: rho_props was not initialized")
    call assert(T0 /= UNINITIALIZED,'mansol matprop: T0 was not initialized')
    call assert(cp /= UNINITIALIZED,'mansol matprop: cp was not initialized')
    call assert(mu0 /= UNINITIALIZED,'mansol matprop: mu0 was not initialized')
    call assert(prandtl /= UNINITIALIZED,'mansol matprop: prandtl was not initialized')
    call assert(rho0 /= UNINITIALIZED,'mansol matprop: rho0 was not initialized')
    call assert( &
        rho_props == 'constant' .eqv. rho1 == UNINITIALIZED, &
        'mansol matprop: initialize `rho1` iff `rho_props` is not "constant"' &
    )
    call assert( &
        rho_props == 'power' .neqv. T_rho_power == UNINITIALIZED, &
        'mansol matprop: initialize `T_rho_power` iff `rho_props` is not "power"' &
    )

    ! Build the set of matprops:
    call matprop_set%add_func('temperature','spec_enthalpy',T2spec_h)
    call matprop_set%add_func('spec_enthalpy','temperature',spec_h2T)
    call matprop_set%add_known_function('spec_heat_capacity',const_func(cp))

    if (exact_rho) then
      call matprop_set%add_known_function('density',mansol_scalfunc_t(name='rho'))
      call matprop_set%add_known_function('deriv_rho_h',mansol_scalfunc_t(name='deriv_rho_h'))
    else
      if (rho_props == 'constant') then
        call matprop_set%add_known_function('density',const_func(rho0))
      else
        call matprop_set%add_func('spec_enthalpy','density',h2rho)
      endif
      select case(rho_props)
        case('constant');               call matprop_set%add_known_function('deriv_rho_h',const_func(0.0_dp))
        case('affine');                 call matprop_set%add_known_function('deriv_rho_h',const_func(- (rho1 - rho0) / cp))
        case('power','sqrt','mixing');  call matprop_set%add_func('spec_enthalpy','deriv_rho_h',h2deriv_rho_h)
        case default;     call error("cannot add 'deriv_rho_h' for matprops: unknown rho_props=" // rho_props)
      end select
      call matprop_set%add_func('spec_enthalpy','deriv_H_h',h2deriv_volH_spech)
    endif

    if (exact_transport_props) then
      call matprop_set%add_known_function('dynamic_viscosity',mansol_scalfunc_t(name='mu'))
      call matprop_set%add_known_function('kinematic_viscosity',mansol_scalfunc_t(name='nu'))
      call matprop_set%add_known_function('thermal_conductivity',mansol_scalfunc_t(name='k'))
      call matprop_set%add_known_function('thermal_diffusivity',mansol_scalfunc_t(name='alpha'))
      call matprop_set%add_known_function('k_over_cp',mansol_scalfunc_t(name='k_over_cp'))
    else
      call matprop_set%add_func('spec_enthalpy','dynamic_viscosity',h2mu)
      call matprop_set%add_func('spec_enthalpy','kinematic_viscosity',h2nu)
      call matprop_set%add_func('spec_enthalpy','thermal_conductivity',h2k)
      call matprop_set%add_func('spec_enthalpy','thermal_diffusivity',h2alpha)
      call matprop_set%add_func('spec_enthalpy','k_over_cp',h2_k_over_cp)
    endif

    call matprop_set%finalize()
  end subroutine get_mansol_matprop

  real(dp) function T2spec_h(T) result(spec_h)
    real(dp), intent(in) :: T

    spec_h = cp * (T - T0)
  end function T2spec_h

  real(dp) function spec_h2T(spec_h) result(T)
    real(dp), intent(in) :: spec_h

    T = spec_h / cp + T0
  end function spec_h2T

  real(dp) function h2rho(spec_h) result(rho)
    real(dp), intent(in) :: spec_h

    real(dp) :: T

    T = spec_h2T(spec_h)
    select case(rho_props)
      case('constant'); rho = rho0
      case('affine');   rho = T * rho0  +  (1 - T) * rho1
      case('power');    rho = T**(1/T_rho_power) * rho0 + (1 - T**(1/T_rho_power)) * rho1
      case('sqrt');     rho = rho1 - (rho1 - rho0) * sqrt(T)
      case('mixing');   rho = 1 / (T/rho0 + (1-T)/rho1)
      case default;     call error('h2rho: unknown rho_props=' // rho_props)
    end select
  end function h2rho

  real(dp) function h2k(spec_h) result(k)
    real(dp), intent(in) :: spec_h

    k = cp * h2mu(spec_h) / prandtl
  end function h2k

  real(dp) function h2mu(spec_h) result(mu)
    real(dp), intent(in) :: spec_h

    real(dp) :: T

    T = spec_h2T(spec_h)
    select case(transport_props)
      case('constant');   mu = mu0
      case('linear');     mu = mu0 + T
      case('parabola');   mu = mu0 + T * (1 - T)
      case default;       call error('h2mu: unknown transport_props=' // transport_props)
    end select

    if (cap_transport_props) mu = max(0.0_dp,mu)
  end function h2mu

  real(dp) function h2nu(spec_h) result(nu)
    real(dp), intent(in) :: spec_h

    nu = h2mu(spec_h) / h2rho(spec_h)
  end function h2nu

  real(dp) function h2_k_over_cp(spec_h) result(k_over_cp)
    real(dp), intent(in) :: spec_h

    k_over_cp = h2k(spec_h) / cp
  end function h2_k_over_cp

  real(dp) function h2alpha(spec_h) result(alpha)
    real(dp), intent(in) :: spec_h

    alpha = h2k(spec_h) / (cp * h2rho(spec_h))
  end function h2alpha

  real(dp) function h2deriv_rho_h(spec_h) result(deriv_rho_h)
    real(dp), intent(in) :: spec_h

    real(dp) :: T

    T = spec_h2T(spec_h)
    deriv_rho_h = T2deriv_rho_T(T) / cp
  end function h2deriv_rho_h

  real(dp) function T2deriv_rho_T(T) result(deriv_rho_T)
    real(dp), intent(in) :: T

    select case(rho_props)
      case('constant');   deriv_rho_T = 0.0_dp
      case('affine');     deriv_rho_T = - (rho1 - rho0)
      case('power');      deriv_rho_T = - (rho1 - rho0) * T**(1/T_rho_power - 1) / T_rho_power
      case('sqrt');       deriv_rho_T = - (rho1 - rho0) / (2 * sqrt(T))
      case('mixing');     deriv_rho_T = - rho0 * rho1 * (rho1 - rho0) / (rho0*(1-T) + rho1*T)**2
      case default;       call error('T2deriv_rho_T: unknown rho_props=' // rho_props)
    end select
  end function

  real(dp) function h2deriv_volH_spech(spec_h) result(deriv_volH_spech)
    real(dp), intent(in) :: spec_h

    deriv_volH_spech = h2rho(spec_h) + spec_h * h2deriv_rho_h(spec_h)
  end function h2deriv_volH_spech
end module matprop_lib_mansol
