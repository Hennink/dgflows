module read_funcs_mod
  use assertions, only: assert
  implicit none

  private
  public :: read_block_as_func_specs
  public :: read_line_as_qnty_eq_func

  interface read_line_as_qnty_eq_func
    module procedure read_line_as_qnty_eq_scalfunc
    module procedure read_line_as_qnty_eq_vecfunc
  end interface read_line_as_qnty_eq_func

contains
  subroutine read_line_as_qnty_eq_scalfunc(unit_number,expected_qnty,scal_func)
    use functions_lib, only: string2func
    use io_basics, only: read_line_as_label_eq_specs
    use scal_func_mod, only: scal_func_type

    integer, intent(in) :: unit_number
    character(*), intent(in) :: expected_qnty
    class(scal_func_type), allocatable, intent(out) :: scal_func

    character(:), allocatable :: func_str

    call read_line_as_label_eq_specs(unit_number,expected_qnty,func_str)
    call string2func(func_str,scal_func)
  end subroutine read_line_as_qnty_eq_scalfunc

  subroutine read_line_as_qnty_eq_vecfunc(unit_number,expected_qnty,vec_func)
    use functions_lib, only: string2func
    use io_basics, only: read_line_as_label_eq_specs
    use vec_func_mod, only: vec_func_type

    integer, intent(in) :: unit_number
    character(*), intent(in) :: expected_qnty
    class(vec_func_type), allocatable, intent(out) :: vec_func

    character(:), allocatable :: func_str

    call read_line_as_label_eq_specs(unit_number,expected_qnty,func_str)
    call string2func(func_str,vec_func)
  end subroutine read_line_as_qnty_eq_vecfunc

  subroutine read_block_as_func_specs(unit_number,func_specs)
    use func_set_mod, only: func_set_t
    use functions_lib, only: string2func
    use io_basics, only: read_line_from_input_file
    use string_manipulations, only: separate_first_word
    use vec_func_mod, only: vec_func_type

    integer, intent(in) :: unit_number
    type(func_set_t), intent(out) :: func_specs

    character(:), allocatable :: input_line
    character(:), allocatable :: qnty_name, func_str
    class(vec_func_type), allocatable :: func

    call read_line_from_input_file(unit_number,input_line)
    call assert(adjustl(input_line) == 'BEGIN qnty_specs', &
                'expected "BEGIN qnty_specs", but input_line=' // input_line)
    do
      call read_line_from_input_file(unit_number,input_line)
      if (adjustl(input_line) == 'END qnty_specs') exit
      call separate_first_word(input_line,qnty_name,func_str,'=')
      call input_name_to_DGFlow_name(qnty_name)
      call string2func(func_str,func)
      call func_specs%add_func(qnty_name,func)
    enddo
  end subroutine read_block_as_func_specs

  subroutine input_name_to_DGFlow_name(name)
    use run_data, only: use_rans_model

    character(:), allocatable, intent(inout) :: name

    character(:), allocatable :: DGFlow_name

    select case(name)
      case('velocity','mass_flux','pressure','density','ddt_rho', &
           'wall_dist', &
           'spec_enthalpy','vol_enthalpy','temperature', &
           'spec_heat_capacity','vol_heat_capacity', &
           'dynamic_viscosity','kinematic_viscosity', &
           'thermal_conductivity','thermal_diffusivity','k_over_cp')
        DGFlow_name = trim(adjustl(name))

      case('vol_turb1','vol_turb2','spec_turb1','spec_turb2')
        call assert(use_rans_model,'INPUT ERROR: turb qnty specified when not solving RANS eq.')
        DGFlow_name = trim(adjustl(name))
    end select

    call assert(allocated(DGFlow_name),'unknown qnty in input: name='//name)
    deallocate(name)
    allocate(name,source=DGFlow_name)
  end subroutine input_name_to_DGFlow_name
end module read_funcs_mod
