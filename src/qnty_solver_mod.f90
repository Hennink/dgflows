module qnty_solver_mod
  use assertions, only: assert, assert_eq
  use dof_handler, only: dof_handler_type, set_type_and_preallocate
  use exceptions, only: error
  use f90_kind, only: dp
  use mesh_partitioning, only: my_first_last_elems
  use mesh_type, only: fem_mesh
  use numerical_solutions, only: get_solvec, tvd_limit_for_qnty
  use pde_mod, only: pde_t, move_terms_with_lhs
  use petsc_mod, only: I_am_master_process
  use petsc_mat_mod
  use petsc_solver_mod, only: set_up, solve_system
  use petsc_vec_mod, only: petsc_vec_type, drop_petsc_vec
  use petsc_ksp_mod, only: petsc_ksp_t
  use phys_time_mod, only: nold_times, BDF_weights_are_constant_from_now_on
  use quantities_mod, only: name2sym
  use run_data, only: check_mat_symm, print_mtx_sparsity_info
  use solvec_mod, only: solvec_t
  use support, only: bool_i
  use timer_typedef, only: timer_type
  use tvd_mod, only: TVD_limiting_of_solution
  implicit none
  
  private
  public :: qnty_solver_t

  type(timer_type), public, protected :: asssolve__total_ass_ksp_solve_ghost(5) ! [total, ass, setup KSP, solve, update ghost]
  
  type, abstract :: qnty_solver_t
    private
    character(:), allocatable, public :: qnty

    logical, public :: expect_symm
    
    logical, public :: postprocessing_linsys_changes_lhs = .false.

    type(petsc_ksp_t), allocatable :: ksp
    type(petsc_mat_type), public :: lhs ! Stored in case it can be reused. Public for the Fieldsplit solver.
    logical, public :: lhs_is_const_from_now_on = .false.

    logical, public :: keep_separate_frozen = .false. ! Only useful when iterating within a time step
    logical :: frozen_linsys_is_precalculated = .false.
    integer :: time_idx_of_precalc_frozen = -huge(1)
    type(petsc_mat_type) :: frozen_part_lhs
    type(petsc_vec_type) :: frozen_part_rhs

    logical, public :: keep_separate_constant = .false. ! saves time, but takes more memory
    logical :: constant_linsys_is_precalculated = .false.
    type(petsc_mat_type) :: constant_part_lhs
    type(petsc_vec_type) :: constant_part_rhs
  contains
    procedure, non_overridable :: set_solver_specs
    procedure :: get_qnty_solvec
    procedure, non_overridable :: assemble_and_solve
    procedure, non_overridable :: assemble_glob_linsys
    procedure, non_overridable, private :: discard_frozen_part
    procedure(collect_pde_i), deferred :: collect_pde_varfrozconst
    procedure :: postprocess_linsys
  end type
  
  abstract interface
    subroutine collect_pde_i(this, variable_terms, frozen_terms, constant_terms)
      ! variable:   differs every time the linsys is solved, even when iterated within a time step
      !             (e.g., convection in momentum equation)
      ! frozen:     based 'frozen' coefficients that do not need updating when iterating within a time step
      !             (e.g., SIP, with diffusion parameter extrapolated from previous time steps))
      ! constant:   same for all solves at all time steps
      !             (e.g., SIP when the diffusion parameter is a constant)
      ! 
      use pde_mod, only: pde_t
      import :: qnty_solver_t
      implicit none

      class(qnty_solver_t), intent(in) :: this
      type(pde_t), intent(out) :: variable_terms, frozen_terms, constant_terms
    end subroutine
  end interface

contains
  subroutine postprocess_linsys(this,mesh,lhs,rhs)
    ! To add things to the linear system that do no need to be computed 
    ! element-wise at every time step, such as the pressure gradient on the 
    ! rhs on the momentum equations.
    class(qnty_solver_t), intent(in) :: this
    type(fem_mesh), intent(in) :: mesh
    type(petsc_mat_type), intent(inout) :: lhs
    type(petsc_vec_type), intent(inout) :: rhs
  end subroutine

  subroutine set_solver_specs(this,expect_symm)
    class(qnty_solver_t), intent(inout) :: this
    logical, intent(in) :: expect_symm
    
    this%expect_symm = expect_symm
  end subroutine
  
  function get_qnty_solvec(this) result(solvec_ptr)
    class(qnty_solver_t), intent(in) :: this
    type(solvec_t), pointer :: solvec_ptr

    solvec_ptr => get_solvec(this%qnty)
  end function

  subroutine assemble_and_solve(this, mesh)
    ! Keep the KSP object throughout the calculation, even if the operators (i.e. the matrices)
    ! change. This enables options like `-pc_factor_reuse_ordering`.
    ! 
    ! The KSP/PC/subKSP/subPC types and options do no change when the operators are replaced.
    ! 
    ! All calls to `set_operators` must use the same size matrices!
    ! 
    ! If `lhs` just points to another matrix, then we assume that it's the same matrix as before,
    ! that is, we do not update the KSP operators.
    ! 
    class(qnty_solver_t), target, intent(inout) :: this
    type(fem_mesh), intent(in) :: mesh
    
    type(petsc_vec_type) :: rhs

    type(solvec_t), pointer :: solvec
    
    call asssolve__total_ass_ksp_solve_ghost(1)%start()
    
    call asssolve__total_ass_ksp_solve_ghost(2)%start()
    call this%assemble_glob_linsys(mesh, rhs)
    call asssolve__total_ass_ksp_solve_ghost(2)%pauze()

    solvec => this%get_qnty_solvec()
    
    ! Print some info:
    if (print_mtx_sparsity_info) then
      write(*,"('info on ',a,': ')",advance='no') solvec%name
      call this%lhs%print_sparsity_info()
    endif
    if (this%expect_symm .and. check_mat_symm) call assert(this%lhs%is_symm(),solvec%name//' matrix is not symmetric')
    
    call asssolve__total_ass_ksp_solve_ghost(4)%start()
    call solve_system(this%ksp, solvec%petsc_coeffs, rhs)
    call asssolve__total_ass_ksp_solve_ghost(4)%pauze()

    call asssolve__total_ass_ksp_solve_ghost(5)%start()
    call solvec%update_ghost_values()
    call asssolve__total_ass_ksp_solve_ghost(5)%pauze()

    ! Some post-processing:
    if (tvd_limit_for_qnty(this%qnty)) call TVD_limiting_of_solution(mesh,solvec)

    call asssolve__total_ass_ksp_solve_ghost(1)%pauze()
  end subroutine
  
  subroutine assemble_glob_linsys(this, mesh, rhs)
    class(qnty_solver_t), target, intent(inout) :: this
    type(fem_mesh), intent(in) :: mesh
    type(petsc_vec_type), intent(out) :: rhs
    
    type(solvec_t), pointer :: solvec
    type(pde_t) :: variable_terms, frozen_terms, constant_terms, pde_to_add
    integer :: depth_pde, depth_var, depth_froz, depth_const
    type(mat_struct_opt_type) :: struct_opt
    type(mat_duplicate_opt_t) :: dup_opt

    call this%collect_pde_varfrozconst(variable_terms, frozen_terms, constant_terms)
    depth_var = variable_terms%ngbr_coupling_depth()
    depth_froz = frozen_terms%ngbr_coupling_depth()
    depth_const = constant_terms%ngbr_coupling_depth()
    depth_pde = max(depth_var, depth_froz, depth_const)

    call assert(depth_pde > 0, "no point in using PETSc when there is not coupling between the elems")

    solvec => this%get_qnty_solvec()

    ! On the first call, assemble the full system into a single matrix, to get the right nonzero structure:
    if (.not. allocated(this%ksp)) then
      call this%lhs%create()
      call set_type_and_preallocate(this%lhs, mesh, solvec%handler, solvec%handler, depth_pde, &
                                    symm=this%expect_symm, force_aij = .false.)

      call this%lhs%init_compatible_vecs(left_vec=rhs)
      pde_to_add = variable_terms + frozen_terms + constant_terms
      call pde_to_add%add_to_glob_linsys(solvec, mesh, this%lhs, rhs)
      call this%lhs%finalize_assembly()
      call rhs%finalize_assembly()
      call this%postprocess_linsys(mesh, this%lhs, rhs)

      allocate(this%ksp)
      call set_up(this%ksp, name2sym(this%qnty))
      call this%ksp%set_operators(this%lhs, this%lhs)

      return
    endif

    ! Optimize for the case where the LHS is the same as last time (much faster to reuse the Mat):
    if (this%lhs_is_const_from_now_on) then
      pde_to_add = variable_terms + frozen_terms
      call this%constant_part_rhs%duplicate(rhs)
      call this%constant_part_rhs%copy(rhs)
      call pde_to_add%add_to_glob_linsys(solvec, mesh, this%lhs, rhs)
      call rhs%finalize_assembly()
      call this%postprocess_linsys(mesh,this%lhs,rhs)

      return
    endif

    ! Do not store constant/frozen LHS with a tiny stencil, because the overhead
    ! from copying the matrices is bigger than just reassembling the terms.
    ! (But Vecs don't have a sparsity pattern, so copying part of the RHS is
    ! always faster than reassembling.)
    if (depth_const <= 0) call move_terms_with_lhs(constant_terms, frozen_terms)
    if (depth_froz <= 0)  call move_terms_with_lhs(frozen_terms, variable_terms)

    call this%lhs%init_compatible_vecs(left_vec=rhs)

    pde_to_add = variable_terms ! will be expanded as needed
    
    ! Initizalize with the constant part, or zero the LHS/RHS:
    call this%lhs%zero_entries()
    if ((.not. this%keep_separate_constant) .or. .not. BDF_weights_are_constant_from_now_on()) then
      pde_to_add = pde_to_add + constant_terms
    elseif (constant_terms%has_terms()) then
      if (.not. this%constant_linsys_is_precalculated) then
        ! This allocates too much space if `depth_const < depth_pde`, but PETSc should clean that up when finalizing the assembly.
        dup_opt = merge(share_nonzero_pattern, do_not_copy_values, depth_pde == depth_const)
        call this%lhs%duplicate(dup_opt, this%constant_part_lhs)
        call rhs%duplicate(this%constant_part_rhs)
        call constant_terms%add_to_glob_linsys(solvec, mesh, this%constant_part_lhs, this%constant_part_rhs)
        call this%constant_part_lhs%finalize_assembly()
        call this%constant_part_rhs%finalize_assembly()
        if (depth_const < 0) call destroy_matrix(this%constant_part_lhs)
  
        this%lhs_is_const_from_now_on = depth_froz < 0 .and. depth_var < 0 .and. .not. this%postprocessing_linsys_changes_lhs
        if (this%lhs_is_const_from_now_on .and. I_am_master_process()) &
            write(*,'(/,a,/)') '======== PRECALCULATED FULL MATRIX for ' // this%qnty
        this%constant_linsys_is_precalculated = .true.
      endif
      if (depth_const >= 0) then
        struct_opt = merge(same_nonzero_pattern_opt, subset_nonzero_pattern_opt, depth_const == depth_pde)
        call this%constant_part_lhs%copy(this%lhs, struct_opt)
      endif
      call this%constant_part_rhs%copy(rhs)

      if (this%lhs_is_const_from_now_on) call destroy_matrix(this%constant_part_lhs)
    endif

    ! Add the frozen part:
    if (.not. this%keep_separate_frozen) then
      pde_to_add = pde_to_add + frozen_terms

    elseif (frozen_terms%has_terms()) then
      if (.not. this%frozen_linsys_is_precalculated) then
        dup_opt = merge(share_nonzero_pattern, do_not_copy_values, depth_pde == depth_froz)
        call this%lhs%duplicate(dup_opt, this%frozen_part_lhs) ! may allocate more zeros than needed
        call rhs%duplicate(this%frozen_part_rhs)

        this%frozen_linsys_is_precalculated = .true.
      endif
      
      if (this%time_idx_of_precalc_frozen < nold_times) then
        call this%frozen_part_lhs%zero_entries()
        call this%frozen_part_rhs%zero_entries()
        call frozen_terms%add_to_glob_linsys(solvec, mesh, this%frozen_part_lhs, this%frozen_part_rhs)
        call this%frozen_part_lhs%finalize_assembly()
        call this%frozen_part_rhs%finalize_assembly()

        this%time_idx_of_precalc_frozen = nold_times
      endif
      
      if (depth_froz >= 0) then
        struct_opt = merge(same_nonzero_pattern_opt, subset_nonzero_pattern_opt, depth_froz == depth_pde)
        call this%lhs%axpy(1.0_dp, this%frozen_part_lhs, struct_opt)
      endif
      call rhs%add_vec(this%frozen_part_rhs)
    endif

    ! Assemble the terms for which there are no global linear systems:
    call pde_to_add%add_to_glob_linsys(solvec, mesh, this%lhs, rhs)
    call this%lhs%finalize_assembly() ! must be called before adding other matrices
    call rhs%finalize_assembly()

    ! Individual solvers may have specialized postprocessing:
    call this%postprocess_linsys(mesh,this%lhs,rhs)
  end subroutine

  subroutine discard_frozen_part(this)
    class(qnty_solver_t), intent(inout) :: this
    
    if (this%frozen_linsys_is_precalculated) then
      call destroy_matrix(this%frozen_part_lhs)
      call drop_petsc_vec(this%frozen_part_rhs)
      this%frozen_linsys_is_precalculated = .false.
    endif
  end subroutine
end module qnty_solver_mod
