submodule(boundary_types) boundary_types__solid_isolated
  implicit none
  
  type, extends(wall_bnd_t) :: solid_isolated_t
  contains
    procedure :: read_specs_from_file
    procedure :: get_bc
  end type solid_isolated_t
  
contains
  module procedure solid_isolated
    allocate(solid_isolated_t :: bnd)
  end procedure solid_isolated
  
  subroutine read_specs_from_file(this,unit_number)
    class(solid_isolated_t), intent(out) :: this
    integer, intent(in) :: unit_number
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types, only: bc_type, hom_neumann_bc_type
    
    class(solid_isolated_t), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    
    type(hom_neumann_bc_type), allocatable :: hom_neumann_bc
    
    select case(qnty_name)
      case('temperature','spec_enthalpy','vol_enthalpy')
        allocate(hom_neumann_bc)
        call move_alloc(hom_neumann_bc,bc)
      case default
        write(*,'(a)') 'ERROR: no bc for '//qnty_name//' in solid_isolated'
        error stop
    end select
  end subroutine get_bc
end submodule boundary_types__solid_isolated

