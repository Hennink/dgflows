module system_commands
  use assertions, only: assert
  use petsc_mod, only: i_am_master_process
  implicit none
  
  private
  public :: synchronous_commands_mode
  public :: init_system_commands, finalize_system_commands
  public :: exec_cmd
  public :: unix_cat_files, unix_rm_files

  ! synchronous_commands_mode determines how to deal with synchronous commands:
  !   'synchronous':          probably best performance, especially when not all processors are 
  !                           working on DGF, but can leave subprocesses hanging after DGF is finished;
  !   'force_asynchronous':   slow, but guarantees that all tasks are finished when DGF stops;
  !   'at_end':               performs all synchronous commands in parallel at the end of the program;
  !   'build_script':         requires you to call a wrap-up script after DGF exits, but can be fast.
  character(100) :: synchronous_commands_mode = 'at_end'
  integer :: unit_wrapup
  character(*), parameter :: NAME_WRAPUP = 'wrapup_dgf'

contains
  subroutine init_system_commands()
    integer :: iostat
    character(200) :: iomsg

    if (i_am_master_process()) then
      if (synchronous_commands_mode == 'build_script' .or. synchronous_commands_mode == 'at_end') then
        open(  &
            file=NAME_WRAPUP, newunit=unit_wrapup,  &
            status='replace', pad='no', position='rewind', action='write', form='formatted',  &
            iostat=iostat, iomsg=iomsg  &
        )
        write(unit_wrapup,'(a)') '#!/usr/bin/env bash'
        write(unit_wrapup,'(a)') 'echo "entered $0"'
        call assert(iostat==0,iomsg)
      endif
    endif
  end subroutine 

  subroutine finalize_system_commands()
    integer :: iostat
    character(200) :: iomsg
    character(:), allocatable :: run_rm

    if (i_am_master_process()) then
      if (synchronous_commands_mode == 'build_script' .or. synchronous_commands_mode == 'at_end') then
        write(unit_wrapup,'(a)') 'wait'
        write(unit_wrapup,'(a)') 'echo "$0: done wrapping up DGF"'
        close(unit_wrapup,iostat=iostat,iomsg=iomsg)
        call assert(iostat==0,iomsg)
      endif
      
      run_rm = '(bash ' // NAME_WRAPUP // ' && rm -f ' // NAME_WRAPUP // ')'
      if (synchronous_commands_mode == 'build_script') write(*,'(a)') 'To wrap up some loose ends from DGF, please call ' // run_rm
      if (synchronous_commands_mode == 'at_end') call exec_cmd(run_rm,wait=.true.)
    endif
  end subroutine

  subroutine unix_cat_files(file_from,file_into)
    character(*), intent(in) :: file_from, file_into
    
    character(1000) :: command
    
    write(command,"('cat ',a,' >>',a)") file_from, file_into
    call exec_cmd(trim(command),wait=.true.)
  end subroutine unix_cat_files
  
  subroutine unix_rm_files(file_list)
    character(*), intent(in) :: file_list
    
    character(1000) :: command
    
    write(command,"('rm -f ',a)") file_list
    call exec_cmd(trim(command),wait=.false.)
  end subroutine unix_rm_files
  
  subroutine exec_cmd(cmd,wait)
    ! Same as the intrinsic "execute_command_line", except that
    ! it always asserts that there are no errors.
    use assertions, only: assert
    use exceptions, only: error
    
    character(*), intent(in) :: cmd
    logical, intent(in) :: wait
    
    logical :: do_it_now, async
    character(1000) :: cmdmsg
    integer :: exitstat, cmdstat

    select case(synchronous_commands_mode)
      case('force_asynchronous')
        do_it_now = .true.
        async = .true.
      
      case('synchronous')
        do_it_now = .true.
        async = wait
      
      case('build_script','at_end')
        do_it_now = wait
        if (do_it_now) async = .true.
      
      case default
        call error('exec_cmd: unknown synchronous_commands_mode=' // synchronous_commands_mode)
    end select

    if (do_it_now) then
      call execute_command_line(cmd,wait=async,exitstat=exitstat,cmdstat=cmdstat,cmdmsg=cmdmsg)
      select case(cmdstat)
        case(0)
        case(-1);       call error("The processor does not support command execution. cmd=" // cmd)
        case(-2);       call error("wait==.true., but the processor does not support asynchronous command execution. cmd=" // cmd)
        case(1:);       call error(                                         &
                            "An error occurred while executing a command. " &
                            // new_line('a') // "cmdmsg=" // cmdmsg         &
                            // new_line('a') // "cmd=" // cmd               &
                        )
        case default;   call error("Weird (non-standard?) value for cmdstat while executing a command. cmd=" // cmd)
      end select
      if (async) call assert(exitstat == 0,"Recieved a non-zero process exit status from a command. cmd=" // cmd)
    
    else
      call assert(i_am_master_process(),'building a script with commands is only implemented for the master process')
      write(unit_wrapup,'(a)') '(' // cmd // ') &'
    endif
  end subroutine exec_cmd
end module system_commands

