module fortran_parser_interface_mod
  ! Interface to the FortranParser library.
  !
  ! GitHub repos: https://github.com/jacopo-chevallard/FortranParser
  !               https://github.com/hennink/FortranParser
  ! 
  ! This interface to the EquationParser functionality is necessary because of 
  ! the INTENT(INOUT) attribute of the passed-dummy argument in 
  ! '<EquationParser>%evaluate', which makes it highly inconvenient to bind 
  ! that object to a type.
  use FortranParser, only: EquationParser
  use assertions, only: assert
  implicit none

  private
  public :: add_eqparser
  public :: evaluate
  
  integer, parameter :: MAX_NO_PARSERS = 100
  integer :: no_parsers = 0

  type(EquationParser) :: parsers(MAX_NO_PARSERS)

contains
  subroutine add_eqparser(eq,vars,idx)
    character(*), intent(in) :: eq, vars(:)
    integer, intent(out) :: idx

    call assert(no_parsers < MAX_NO_PARSERS,'stack overflow: no room for more eq. parsers.')
    no_parsers = no_parsers + 1
    idx = no_parsers
    parsers(idx) = EquationParser(eq,vars)
  end subroutine add_eqparser

  real(dp) function evaluate(idx,vars)
    use f90_kind, only: dp

    integer, intent(in) :: idx
    real(dp), intent(in) :: vars(:)
    
    call assert(1 <= idx .and. idx <= no_parsers,'bad idx: no such eq. parser')
    evaluate = parsers(idx)%evaluate(vars)
  end function evaluate
end module fortran_parser_interface_mod
