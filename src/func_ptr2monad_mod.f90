module func_ptr2monad_mod
  use monad_mod, only: monad_t
  implicit none
  
  private
  public :: func_ptr2monad
  public :: real2real_i
  
  type, extends(monad_t) :: func2monad_t
    private
    procedure(real2real_i), pointer, nopass :: func_ptr => null()
  contains
    procedure, non_overridable :: value
  end type func2monad_t
  
  abstract interface
    real(dp) function real2real_i(arg)
      use f90_kind, only: dp
      implicit none
      real(dp), intent(in) :: arg
    end function real2real_i
  end interface
  
contains
  subroutine func_ptr2monad(func_ptr,monad)
    use monad_mod, only: monad_t
    
    procedure(real2real_i) :: func_ptr
    class(monad_t), allocatable, intent(out) :: monad
    
    type(func2monad_t) :: monad_from_func_ptr
    
    monad_from_func_ptr%func_ptr => func_ptr
    allocate(monad,source=monad_from_func_ptr)
    ! We assume that any compiled Fortran function should be fast enough to
    ! legitimize the extra calculations involved in finite difference.
    monad%use_finite_diff = .true.
  end subroutine func_ptr2monad
  
  impure elemental real(dp) function value(this,arg)
    use f90_kind, only: dp
    
    class(func2monad_t), intent(in) :: this
    real(dp), intent(in) :: arg
    
    value = this%func_ptr(arg)
  end function value
end module func_ptr2monad_mod

