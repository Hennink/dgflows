module coolprop_with_fixed_qnty_mod
  use f90_kind, only: dp
  implicit none
  
  private
  public :: cp_with_fixed_qnty_t
  
  type :: cp_with_fixed_qnty_t
    character(:), allocatable :: indep_qnty, dep_qnty, fixed_qnty
    real(dp) :: fixed_value
    character(:), allocatable :: fluidname
  
  contains
     private
     procedure, public :: get_spline
     procedure :: refine_knots
     procedure :: segs_need_refinement
     procedure :: spline_from_points
     procedure :: cp_deriv
     procedure, public :: cp_value
  end type cp_with_fixed_qnty_t
  
contains
  subroutine get_spline(this,domain,spline)
    use spline_mod, only: cubic_spline_t
    
    class(cp_with_fixed_qnty_t), intent(in) :: this
    real(dp), intent(in) :: domain(2)
    type(cubic_spline_t), intent(out) :: spline
    
    real(dp), allocatable :: points(:)
    logical :: converged
    
    allocate(points,source=domain)
    do
      call this%refine_knots(points,converged)
      if (converged) exit
    enddo
    spline = this%spline_from_points(points)
  end subroutine get_spline
  
  subroutine refine_knots(this,points,converged)
    use assertions, only: assert
    
    class(cp_with_fixed_qnty_t), intent(in) :: this
    real(dp), allocatable, intent(inout) :: points(:)
    logical, intent(out) :: converged
    
    logical :: need_refinement(size(points)-1)
    real(dp), allocatable :: new_points(:)
    integer :: no_points, new_no_points
    integer :: i
    integer :: added
    
    need_refinement = this%segs_need_refinement(points)
    converged = .not. any(need_refinement)
    no_points = size(points)
    new_no_points = no_points + count(need_refinement)
    allocate(new_points(new_no_points))
    added = 0
    do i = 1, no_points - 1
      new_points(added+1) = points(i)
      added = added + 1
      if (need_refinement(i)) then
        new_points(added+1) = (points(i) + points(i+1)) / 2.0_dp
        added = added + 1
      endif
    enddo
    new_points(added+1) = points(size(points))
    added = added + 1

    call assert(added == new_no_points)
    if (converged) call assert(all(new_points == points))
    
    call move_alloc(new_points,points)
  end subroutine refine_knots
  
  function segs_need_refinement(this,points)
    use spline_mod, only: cubic_spline_t
    use support, only: linspace
    use run_data, only: coolprop_spline_tol
    
    class(cp_with_fixed_qnty_t), intent(in) :: this
    real(dp), intent(in) :: points(:)
    logical :: segs_need_refinement(size(points)-1)
    
    type(cubic_spline_t) :: spline
    
    integer, parameter :: no_test_points = 10
    
    real(dp) :: test_points(no_test_points)
    integer :: seg, p
    real(dp) :: x, cp_value, diff
    real(dp), parameter :: SMALL = sqrt(epsilon(1.0_dp))
    
    spline = this%spline_from_points(points)
    do seg = 1, size(points) - 1
      test_points = linspace(points(seg),points(seg+1),no_test_points)
      segs_need_refinement(seg) = .false.
      do p = 1, no_test_points
        x = test_points(p)
        cp_value = this%cp_value(x)
        diff = spline%value(x) - cp_value
        if (abs(cp_value) >= SMALL) then
          segs_need_refinement(seg) = abs(diff/cp_value) > coolprop_spline_tol
        else
          segs_need_refinement(seg) = abs(diff) > SMALL
        endif
        if (segs_need_refinement(seg)) exit
      enddo
    enddo
  end function segs_need_refinement
  
  type(cubic_spline_t) function spline_from_points(this,points) result(spline)
    use f90_kind, only: dp
    use spline_mod, only: cubic_spline_t
    
    class(cp_with_fixed_qnty_t), intent(in) :: this
    real(dp), intent(in) :: points(:)
    
    real(dp) :: y(size(points))
    real(dp) :: dy_dx_left, dy_dx_right
    
    y = this%cp_value(points)
    dy_dx_left =  this%cp_deriv(points(1))
    dy_dx_right = this%cp_deriv(points(size(points)))
    call spline%from_table(points,y,dy_dx_left,dy_dx_right)
  end function spline_from_points
  
  impure elemental real(dp) function cp_value(this,x)
    use coolprop_mod, only: calc_cp_value => cp_value
    
    class(cp_with_fixed_qnty_t), intent(in) :: this
    real(dp), intent(in) :: x
    
    cp_value = calc_cp_value(this%dep_qnty, &
                             this%indep_qnty,x, &
                             this%fixed_qnty,this%fixed_value, &
                             this%fluidname)
  end function cp_value
  
  impure elemental real(dp) function cp_deriv(this,x)
    ! Use finite difference to estimate the derivative, because CoolProp does
    ! not compute all derivatives (in particular not the transport poperties).
    use math_const, only: SMALL
    
    class(cp_with_fixed_qnty_t), intent(in) :: this
    real(dp), intent(in) :: x
    
    cp_deriv = (this%cp_value(x+SMALL) - this%cp_value(x-SMALL)) / (2*SMALL)
  end function cp_deriv
end module coolprop_with_fixed_qnty_mod
