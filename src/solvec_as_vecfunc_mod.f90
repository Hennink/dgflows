module solvec_as_vecfunc_mod
  use assertions, only: assert, BOUNDS_CHECK
  use f90_kind, only: dp
  use mesh_objects, only: elem_list
  use pnt_set_mod, only: pnt_set_t
  use solvec_mod, only: solvec_t
  use vec_func_mod, only: vec_func_type
  implicit none
  
  private
  public :: num_vec_func_t, solvec_as_vecfunc
  
  type, extends(vec_func_type) :: num_vec_func_t
    private
    type(solvec_t), pointer :: solvec => null()
  contains
    procedure, non_overridable :: get_values, get_gders, no_dirs
    procedure, non_overridable :: select_dir
  end type num_vec_func_t
  
contains
  function solvec_as_vecfunc(solvec) result(num_vec_func)
    type(solvec_t), target, intent(in) :: solvec
    type(num_vec_func_t) :: num_vec_func
    
    num_vec_func%solvec => solvec
  end function solvec_as_vecfunc
  
  
  pure integer function no_dirs(this)
    class(num_vec_func_t), intent(in) :: this
    
    no_dirs = this%solvec%no_dirs()
  end function no_dirs
  
  subroutine select_dir(this,dir,scalfunc)
    use scal_func_mod, only: scal_func_type
    use solvec_as_scalfunc_mod, only: num_scal_func_t
    
    class(num_vec_func_t), intent(in) :: this
    integer, intent(in) :: dir
    class(scal_func_type), allocatable, intent(out) :: scalfunc
    
    type(num_scal_func_t) :: num_scal_func
    
    call assert(dir == 1,'solvec_as_scalfunc not implemented for multidirectional solvec')

    call num_scal_func%init(this%solvec)
    allocate(scalfunc,source=num_scal_func)
  end subroutine select_dir
  
  subroutine get_values(this,elem,pnt_set,time,values)
    class(num_vec_func_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:,:)
    
    logical :: only_fluid_mesh
    integer :: elem_no
    integer :: nnod

    call assert(size(values,1) == this%no_dirs(),'solvec_as_vecfunc_mod: eval_at_pnt_set: 1',BOUNDS_CHECK)
    call assert(size(values,2) == pnt_set%np(),'solvec_as_vecfunc_mod: eval_at_pnt_set: 2',BOUNDS_CHECK)

    only_fluid_mesh = this%solvec%handler%only_fluid_mesh
    elem_no = elem%get_active_elem_no(only_fluid_mesh)
    nnod = this%solvec%handler%no_nod_elem(elem_no)
    call assert(nnod <= size(pnt_set%dg_funcs,1),'vec. eval_at_pnt_set: not enough nodes stored for values',BOUNDS_CHECK)

    call this%solvec%dotprod(elem_no,pnt_set%dg_funcs(:nnod,:),time,values)
  end subroutine get_values
  
  subroutine get_gders(this,elem,pnt_set,time,gders)
    class(num_vec_func_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: gders(:,:,:)
    
    logical :: only_fluid_mesh
    integer :: elem_no
    integer :: nnod
    
    call assert(size(gders,1) == size(pnt_set%dg_transp_gders,2),'solvec_as_vecfunc_mod: eval_at_pnt_set: 3',BOUNDS_CHECK)
    call assert(size(gders,2) == this%no_dirs(),'solvec_as_vecfunc_mod: eval_at_pnt_set: 4',BOUNDS_CHECK)
    call assert(size(gders,3) == pnt_set%np(),'solvec_as_vecfunc_mod: eval_at_pnt_set: 5',BOUNDS_CHECK)

    only_fluid_mesh = this%solvec%handler%only_fluid_mesh
    elem_no = elem%get_active_elem_no(only_fluid_mesh)
    nnod = this%solvec%handler%no_nod_elem(elem_no)
    call assert(nnod <= size(pnt_set%dg_transp_gders,1),'vec. eval_at_pnt_set: not enough nodes stored for gders',BOUNDS_CHECK)

    call this%solvec%matmult(elem_no,pnt_set%dg_transp_gders(:nnod,:,:),time,gders)
  end subroutine get_gders
end module solvec_as_vecfunc_mod
