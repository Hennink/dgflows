module boundaries
  use assertions, only: assert, assert_eq
  use boundary_condition_types, only: bc_type
  use boundary_types, only: bnd_type
  implicit none
  
  private
  public :: absolute_pressure_is_fixed, is_periodic, periodicity_specs
  public :: all_specified_bnd_ids
  public :: init_boundaries_from_file, init_total_bnd_areas
  public :: get_bc
  public :: get_bnd
  
  logical, protected, public :: bc_are_time_independent = .false.
  
  type id_bnd_tuple
    private
    integer :: id
    class(bnd_type), allocatable :: bnd
  end type id_bnd_tuple
  
  integer, parameter :: MAX_NO_BND_IDS = 10
  integer :: no_bnd_ids = 0
  type(id_bnd_tuple), dimension(MAX_NO_BND_IDS) :: all_id_bnd_tuples
  
contains
  subroutine add_bnd_id_pair(id,bnd)
    integer, intent(in) :: id
    class(bnd_type), intent(in) :: bnd
    
    call assert(no_bnd_ids < MAX_NO_BND_IDS,'stack overflow: no space for (id,bnd) pair')
    call assert(local_idx_of_bc_id(id) == 0,"Boundary ID's must not be specified more than once.")
    
    no_bnd_ids = no_bnd_ids + 1
    associate(new_pair => all_id_bnd_tuples(no_bnd_ids))
      new_pair%id = id
      allocate(new_pair%bnd,source=bnd)
    end associate
  end subroutine add_bnd_id_pair
  
  subroutine get_bc(bc_id,qnty_name,bc)
    integer, intent(in) :: bc_id
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    
    class(bnd_type), allocatable :: bnd
    
    call get_bnd(bc_id,bnd)
    call bnd%get_bc(qnty_name,bc)
  end subroutine get_bc
  
  subroutine get_bnd(bc_id,bnd)
    use string_manipulations, only: int2char
    
    integer, intent(in) :: bc_id
    class(bnd_type), allocatable, intent(out) :: bnd
    
    integer :: local_idx
    
    local_idx = local_idx_of_bc_id(bc_id)
    call assert(local_idx > 0,'did not find bc_id=' // int2char(bc_id))
    allocate(bnd,source=all_id_bnd_tuples(local_idx)%bnd)
  end subroutine get_bnd
  
  logical function absolute_pressure_is_fixed() result(abs_press)
    integer :: i
    
    call assert(no_bnd_ids >= 1, 'absolute_pressure_is_fixed: looks like module has not been initialized yet')

    abs_press = .false.
    do i = 1, no_bnd_ids
      abs_press = abs_press .or. all_id_bnd_tuples(i)%bnd%fixes_absolute_pressure()
    enddo
  end function
  
  pure integer function local_idx_of_bc_id(id) result(idx)
    integer, intent(in) :: id
    
    do idx = 1, no_bnd_ids
      if (id == all_id_bnd_tuples(idx)%id) return
    enddo
    idx = 0
  end function local_idx_of_bc_id
  
  logical function is_periodic()
    integer, allocatable, dimension(:) :: dummy_1,dummy_2,dummy_3
    
    call periodicity_specs(is_periodic,dummy_1,dummy_2,dummy_3)
  end function is_periodic
  
  subroutine periodicity_specs(is_periodic,prim_id,sec_id,dir)
    use bnd_t_periodic_mod, only : periodic_bnd_t
    
    logical, intent(out) :: is_periodic
    integer, allocatable, intent(out) :: prim_id(:), sec_id(:)
    integer, allocatable, intent(out) :: dir(:)
    
    integer, parameter :: MAX_NPERIODIC = 3
    logical, dimension(MAX_NPERIODIC) :: prim_found, sec_found
    integer :: i, nperiodic
    
    allocate(prim_id(MAX_NPERIODIC), sec_id(MAX_NPERIODIC), dir(MAX_NPERIODIC))

    prim_found = .false.
    sec_found = .false.
    do i = 1, no_bnd_ids
      select type(bnd => all_id_bnd_tuples(i)%bnd)
        class is(periodic_bnd_t)
          call assert(bnd%couple_id <= MAX_NPERIODIC,'couple_id cannot be greater than MAX_NPERIODIC')
          if (bnd%is_primary) then
            call assert(.not. prim_found(bnd%couple_id), 'more than one periodic BC with the same couple_id')
            prim_found(bnd%couple_id) = .true.
            prim_id(bnd%couple_id) = all_id_bnd_tuples(i)%id
            dir(bnd%couple_id) = bnd%dir
          else
            call assert(.not. sec_found(bnd%couple_id), 'more than one periodic BC with the same couple_id')
            sec_found(bnd%couple_id) = .true.
            sec_id(bnd%couple_id) = all_id_bnd_tuples(i)%id
          endif
      end select
    enddo
    call assert(prim_found .eqv. sec_found,'prim_found should equal sec_found')
    nperiodic = findloc(prim_found,.true.,dim=1,back=.true.)
    is_periodic = nperiodic > 0
    if (is_periodic) call assert(prim_found(:nperiodic),"periodic IDs not contiguous")
    call reduce_len(prim_id,nperiodic)
    call reduce_len(sec_id,nperiodic)
    call reduce_len(dir,nperiodic)
  end subroutine periodicity_specs
  
  subroutine reduce_len(array,new_len)
    ! Reduces length of 'array' to 'new_len'.
    integer, allocatable, intent(inout) :: array(:)
    integer, intent(in) :: new_len

    integer, allocatable :: temp(:)

    temp = array(:new_len)
    call move_alloc(temp,array)
  end subroutine reduce_len

  function all_specified_bnd_ids() result(bnd_ids)
    integer :: bnd_ids(no_bnd_ids)
    
    bnd_ids = all_id_bnd_tuples(:no_bnd_ids)%id
  end function
  
  subroutine init_boundaries_from_file()
    use exceptions, only: error
    use io_basics, only: open_input_file, &
                         read_line_as_label_eq_specs, &
                         read_line_from_input_file
    use petsc_mod, only: i_am_master_process
    use run_data, only: file_path_bc_specs
    use string_manipulations, only: char2int, int2char, separate_first_word, starts_with, char2bool

    integer :: unit_number
    logical :: EOF
    character(:), allocatable :: input_line
    character(:), allocatable :: id_char, bnd_type_description, dummy, flag
    integer :: bc_id
    class(bnd_type), allocatable :: bnd
    
    call open_input_file(file_path_bc_specs,unit_number)
      do
        call read_line_from_input_file(unit_number,input_line,EOF)
        if (EOF) exit
        if (input_line == '') then
        elseif (starts_with(input_line,'bc_are_time_independent')) then
          call separate_first_word(input_line, dummy, flag, '=')
          call assert(dummy == 'bc_are_time_independent', 'cannot parse line=' // input_line)
          bc_are_time_independent = char2bool(flag)
          if (i_am_master_process()) write(*,'(a,L1)') 'bc_are_time_independent=', bc_are_time_independent
        else
          call separate_first_word(input_line,id_char,bnd_type_description,':')
          bc_id = char2int(id_char)
          call get_bnd_from_file(unit_number,bnd_type_description,bnd)
          call add_bnd_id_pair(bc_id,bnd)
        endif
      enddo
    close(unit_number)
  end subroutine init_boundaries_from_file
  
  subroutine get_bnd_from_file(unit_number,bnd_type_description,bnd)
    use io_basics, only: read_line_from_input_file
    
    integer, intent(in) :: unit_number
    character(*), intent(in) :: bnd_type_description
    class(bnd_type), allocatable, intent(out) :: bnd
    
    character(:), allocatable :: last_line
    
    call alloc_bnd_from_description(bnd_type_description,bnd)
    call bnd%read_specs_from_file(unit_number)
    call read_line_from_input_file(unit_number,last_line)
    call assert(last_line == '', &
        'Boundary specifications should end with a blank line, but I got:' &
        // new_line('a') // last_line)
  end subroutine get_bnd_from_file
  
  subroutine alloc_bnd_from_description(bnd_type_description,bnd)
    use bnd_t_periodic_mod, only : periodic_bnd_t
    use boundary_types
    
    character(*), intent(in) :: bnd_type_description
    class(bnd_type), allocatable, intent(out) :: bnd
    
    select case(bnd_type_description)
      case('noslip_fixedQ_wall')
        call noslip_fixedQ_wall(bnd)
      case('noslip_htc_wall')
        call noslip_htc_wall(bnd)
      case('noslip_fixedT_wall')
        call noslip_fixedT_wall(bnd)
      case('noslip_isolated_wall')
        call noslip_isolated_wall(bnd)
      case('noslip_internal_wall')
        call noslip_internal_wall(bnd)
      case('symmetry')
        call alloc_symmetry(bnd)
      case('wall_func_adiabatic')
        call alloc_wall_func_adiabatic(bnd)
      case('wall_func_fixedQ')
        call alloc_wall_func_fixedQ(bnd)
      case('wall_func_fixedT')
        call alloc_wall_func_fixedT(bnd)
      case('fixed_velocity')
        call alloc_fixed_velocity(bnd)
      case('outflow')
        call alloc_outflow(bnd)
      case('solid_fixedT')
        call solid_fixedT(bnd)
      case('solid_isolated')
        call solid_isolated(bnd)
      case('solid_fixedQ')
        call solid_fixedQ(bnd)
      case('solid_htc')
        call solid_htc(bnd)
      case('periodic')
        allocate(periodic_bnd_t :: bnd)
      
      case default
        error stop 'bnd_type_description not found'
    end select
  end subroutine alloc_bnd_from_description
  
  
  subroutine init_total_bnd_areas(mesh)
    use f90_kind, only: dp
    use mesh_objects, only: elem_id, elem_list, elem_ptr, face_list, face_ptr
    use mesh_partitioning, only: my_first_last_elems
    use mesh_type, only: fem_mesh
    use mpi_wrappers_mod, only: allreduce_wrap
  
    class(fem_mesh), intent(in) :: mesh
    
    integer :: elem_no, first_elem, last_elem
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    type(elem_ptr), allocatable, dimension(:) :: active_neighbors
    type(face_ptr), allocatable, dimension(:) :: active_subfaces
    integer :: i, loc_idx
    type(face_list), pointer :: sub_face
    real(dp) :: my_areas(no_bnd_ids), areas(no_bnd_ids)
    
    call my_first_last_elems(.false.,first_elem,last_elem)
    
    my_areas = 0.0_dp
    do elem_no = first_elem, last_elem
      id = mesh%active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      call elem%active_subfaces(active_subfaces,active_neighbors)
      do i = 1, size(active_subfaces)
        sub_face => active_subfaces(i)%point
        if (sub_face%is_boundary(only_fluid_mesh=.false.)) then
          loc_idx = local_idx_of_bc_id(sub_face%bc_id)
          my_areas(loc_idx) = my_areas(loc_idx) + sub_face%area
        endif
      enddo
    enddo
    
    call allreduce_wrap(my_areas,'sum',areas)
    do loc_idx = 1, no_bnd_ids
      all_id_bnd_tuples(loc_idx)%bnd%total_area = areas(loc_idx)
    enddo
  end subroutine init_total_bnd_areas
end module boundaries

