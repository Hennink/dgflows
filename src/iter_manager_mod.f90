module iter_manager_mod
  use assertions, only: assert
  use math_const, only: SMALL
  use f90_kind, only: dp
  implicit none

  private
  public :: iter_manager_t

  type :: iter_manager_t
    private
    character(:), allocatable :: error_name
    integer :: max_iter = huge(1)
    integer :: iter = 0
    real(dp), allocatable :: errors(:)
    real(dp) :: tol = SMALL
  contains
    procedure :: init
    procedure :: start_new_iteration
    procedure :: add_iter
    procedure :: iter_done, finished, converged
    procedure :: print_errors
    final :: warn_if_not_converged
  end type iter_manager_t

contains
  subroutine init(this,error_name,max_iter,tol)
    class(iter_manager_t), intent(out) :: this
    character(*), intent(in) :: error_name
    integer, intent(in) :: max_iter
    real(dp), optional, intent(in) :: tol

    call assert(max_iter >= 0,'negative value for max_iter')

    allocate(this%error_name,source= error_name)
    this%max_iter = max_iter
    allocate(this%errors(max_iter))
    if (present(tol)) this%tol = tol
  end subroutine init

  subroutine start_new_iteration(this)
    class(iter_manager_t), intent(inout) :: this

    this%iter = 0
  end subroutine start_new_iteration

  subroutine add_iter(this,error)
    class(iter_manager_t), intent(inout) :: this
    real(dp), intent(in) :: error

    call assert(this%iter < this%max_iter,'more than max. # iters.')

    this%iter = this%iter + 1
    this%errors(this%iter) = error
  end subroutine add_iter

  integer function iter_done(this)
    class(iter_manager_t), intent(in) :: this

    iter_done = this%iter
  end function iter_done

  logical function finished(this)
    class(iter_manager_t), intent(in) :: this

    if (this%iter == this%max_iter) then
      finished = .true.
    else
      finished = this%converged()
    endif
  end function finished

  logical function converged(this)
    class(iter_manager_t), intent(in) :: this

    if (this%iter > 0) then
      converged = this%errors(this%iter) <= this%tol
    else
      converged = .false.
    endif
  end function converged

  subroutine print_errors(this)
    class(iter_manager_t), intent(in) :: this

    integer :: i

    do i = 1, this%iter
      write(*,"(a,' = ',es8.1)") this%error_name, this%errors(i)
    enddo
  end subroutine print_errors

  subroutine warn_if_not_converged(this)
    use exceptions, only: warning
    use petsc_mod, only: I_am_master_process

    type(iter_manager_t), intent(in) :: this

    character(1000) :: msg

    if (this%iter > 0 .and. .not. this%converged() .and. I_am_master_process()) then
      write(msg,"(a,' = ',es8.1,' > ',es8.1)") this%error_name, this%errors(this%iter), this%tol
      call warning(trim(msg))
    endif
  end subroutine warn_if_not_converged
end module iter_manager_mod
