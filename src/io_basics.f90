module io_basics
  use assertions, only: assert
  use string_manipulations, only: int2char
  implicit none

  private
  public :: open_input_file
  public :: INT_FMT, REAL_FMT, nints_fmt, nreals_fmt
  public :: count_str_in_file, str_locations_in_file
  public :: prompt_yn
  public :: read_line_from_input_file
  public :: read_line_as_label_eq_specs
  public :: my_temp_proc_name, merge_temp_proc_files
  public :: blank_new_file

  character(*), parameter :: REAL_FMT = 'es26.17e3', INT_FMT = 'i20'
  character(*), parameter :: TEMPFILE_TEMPLATE = "(a,a,'.temp.dgflow')"

contains
  function my_temp_proc_name(basename)
    use petsc_mod, only: petsc_proc_id
    
    character(*), intent(in) :: basename
    character(:), allocatable :: my_temp_proc_name

    my_temp_proc_name = temp_proc_name(basename,petsc_proc_id())
  end function my_temp_proc_name

  function temp_proc_name(basename,proc)
    ! Returns the name of the temp. file to which to write in order not not
    ! conflict with other processes.
    character(*), intent(in) :: basename
    integer, intent(in) :: proc
    character(:), allocatable :: temp_proc_name

    character(100 + len(basename)) :: name

    write(name,TEMPFILE_TEMPLATE) trim(basename), int2char(proc)
    temp_proc_name = trim(name)
  end function temp_proc_name

  subroutine merge_temp_proc_files(basename,head,tail)
    ! Concatonates 
    !     the content of the 'head' file,
    !     all temporary process files associated with 'basename', and
    !     the content of the 'tail' file
    ! to 'basename'.
    ! Deletes the merged files afterward.
    use petsc_mod, only: no_petsc_procs
    use system_commands, only: exec_cmd

    character(*), intent(in) :: basename, head, tail

    character(100 + len_trim(basename)) :: all_temp_files
    character(:), allocatable :: list_all, cat, rm

    ! Pipe the files through xargs, rather than listing them in the command line,
    ! which would otherwise get cluttered in case of a large number of processes.
    write(all_temp_files,TEMPFILE_TEMPLATE) trim(basename), '{0..' // int2char(no_petsc_procs()-1) // '}'
    list_all = 'ls --sort=none ' // head // ' ' // trim(all_temp_files) // ' ' // tail
    cat = list_all // ' | xargs cat > ' // basename
    rm = list_all // ' | xargs rm -f '
    call exec_cmd(cat // ' && ' // rm,wait=.false.)
  end subroutine
  
  integer function count_str_in_file(str,unit)
    ! Counts the number of lines that start with 'str', and rewinds the file.
    ! Pass str='' to count the total number of lines.
    character(*), intent(in) :: str
    integer, intent(in) :: unit

    integer, allocatable :: locs(:)

    call str_locations_in_file(str,unit,locs)
    count_str_in_file = size(locs)
  end function count_str_in_file

  subroutine str_locations_in_file(str,unit,locs)
    ! Finds the numbers of the lines that begin with 'str', and rewinds the file.
    use, intrinsic :: iso_fortran_env, only: IOSTAT_END

    character(*), intent(in) :: str
    integer, intent(in) :: unit
    integer, allocatable, intent(out) :: locs(:)

    integer :: iostat
    character(len(str)) :: line
    integer :: i, nlocs
    character(200) :: iomsg
    integer, allocatable :: temp(:)

    rewind(unit,iostat=iostat)
    call assert(iostat == 0,'str_locations_in_file: cannot rewind')
    nlocs = 0
    allocate(locs(0))
    do i = 1, huge(i)-1
      read(unit,'(a)',pad='yes',iostat=iostat,iomsg=iomsg) line
      if (iostat == IOSTAT_END) exit
      call assert(iostat == 0,iomsg)
      if (line == str) then
        nlocs = nlocs + 1
        if (nlocs > size(locs)) then
          allocate(temp(2*nlocs))
          temp(:nlocs-1) = locs
          call move_alloc(temp,locs)
        endif
        locs(nlocs) = i
      endif
    enddo
    allocate(temp,source=locs(:nlocs))
    call move_alloc(temp,locs)
    rewind(unit,iostat=iostat)
    call assert(iostat == 0,'str_locations_in_file: cannot rewind')
  end subroutine str_locations_in_file


  logical function prompt_yn() result(yesno)
    use, intrinsic :: iso_fortran_env, only: input_unit, output_unit

    character(:), allocatable :: input

    write(output_unit,'(a)') '(y/n)'
    do
      call read_line_from_input_file(input_unit,input)
      select case(input)
        case('n','N','no','No')
          yesno = .false.
          exit
        case('y','Y','yes','Yes')
          yesno = .true.
          exit
        case default
          write(output_unit,'(a)') 'Please write "y" or "n".'
      end select
    enddo
  end function prompt_yn

  subroutine open_input_file(file_path, unit_number)
    character(*), intent(in) :: file_path
    integer, intent(out) :: unit_number
    
    integer :: ios
    character(200) :: iomsg
    
    open( file=file_path, newunit=unit_number, &
          status='old', position='rewind', action='read', pad='no', &
          iostat=ios, iomsg=iomsg)
    call assert(ios==0,"open_input_file: iomsg="//iomsg)
  end subroutine open_input_file
  
  subroutine read_line_from_input_file(unit,line,eof)
    use, intrinsic :: iso_fortran_env, only: IOSTAT_EOR, IOSTAT_END

    integer, intent(in) :: unit
    character(:), allocatable, intent(out) :: line
    logical, optional, intent(out) :: eof
    
    integer :: size
    character(256) :: buffer   
    integer :: iostat
    character(200) :: iomsg
    integer :: pos
    
    line = ''
    do
      read (unit, "(a)", advance='no', size=size, iostat=iostat, iomsg=iomsg, pad='yes') buffer
      call assert(iostat <= 0, trim(iomsg))

      line = line // buffer(:size)
      
      if (present(EOF)) EOF = iostat == IOSTAT_END
      if (iostat == IOSTAT_END) call assert(present(EOF),'reached end of file, but EOF dummy is not present; iomsg=' // trim(iomsg))
      
      select case(iostat)
        case(0)
        case(IOSTAT_EOR, IOSTAT_END); exit
        case default;                 error stop "this should never have happened: unknown negative `iostat`"
      end select
    end do

    ! Delete the comment and trailing whitespace:
    pos = scan(line,'!')
    if (pos /= 0) line = line(:pos-1)
    line = trim(line)
  end subroutine
  
  subroutine read_line_as_label_eq_specs(unit_number,expected_label,specs)
    use string_manipulations, only: separate_first_word
    
    integer, intent(in) :: unit_number
    character(*), intent(in) :: expected_label
    character(:), allocatable, intent(out) :: specs
    
    character(:), allocatable :: input_line
    character(:), allocatable :: qnty_name
    
    call read_line_from_input_file(unit_number,input_line)
    call separate_first_word(input_line,qnty_name,specs,'=')
    call assert(qnty_name == expected_label,"expected specs for '"//expected_label//"', but got this:"//new_line('a')//input_line)
  end subroutine read_line_as_label_eq_specs

  character(100) pure function nints_fmt(n)
    ! Returns a standard format specifier for reading/writing n integers.
    integer, intent(in) :: n

    nints_fmt = "(" // int2char(n) // INT_FMT // ")"
  end function nints_fmt

  character(100) pure function nreals_fmt(n)
    ! Returns a standard format specifier for reading/writing n reals.
    integer, intent(in) :: n

    nreals_fmt = "(" // int2char(n) // REAL_FMT // ")"
  end function nreals_fmt

  subroutine blank_new_file(filepath,form)
    ! Ensures that there is a blank file in 'file_path'.
    character(*), intent(in)  :: filepath, form

    integer :: unit
    integer :: iostat
    character(200) :: iomsg

    open(file=filepath, newunit=unit, &
         status='replace', pad='no', form=form, &
         iostat=iostat,iomsg=iomsg)
    call assert(iostat == 0,iomsg)
    close(unit,iostat=iostat,iomsg=iomsg)
    call assert(iostat == 0,iomsg)
  end subroutine blank_new_file
end module io_basics
