submodule(matprop_mod) matprop__lib_MSFR_salt
  use f90_kind, only: dp
  use functions_lib, only: const_func
  use run_data, only: T_ref => boussinesq_T_ref
  implicit none
  
  ! The properties have been taken from SAMOFAR D1.4, page 7, Table 1.
  real(dp), parameter :: T_melt = 854.0_dp ! (K), melting T for MSFR salt
  real(dp), parameter :: T_min = 0.7_dp * T_melt
  
  real(dp), parameter :: cp = 1578.95_dp
  real(dp), parameter :: k = 1.7_dp
  real(dp), parameter :: k_over_cp = k / cp;
  
  real(dp) :: rho_ref
  
contains
  module subroutine get_MSFR_salt_matprop(matprop_set)
    type(func_set_expansion_t), intent(out) :: matprop_set
    
    rho_ref = T2rho(T_ref)
    
    call matprop_set%add_known_function('density',const_func(rho_ref))
    call matprop_set%add_known_function('thermal_expansion_coeff',const_func(T2beta(T_ref)))
    call matprop_set%add_known_function('thermal_conductivity',const_func(k))
    call matprop_set%add_known_function('spec_heat_capacity',const_func(cp))
    call matprop_set%add_known_function('k_over_cp',const_func(k_over_cp))
    call matprop_set%add_known_function('vol_heat_capacity',const_func(cp*rho_ref))
    call matprop_set%add_known_function('thermal_diffusivity',const_func(k_over_cp/rho_ref))
    
    call matprop_set%add_func('temperature','spec_enthalpy',T2spec_h)
    call matprop_set%add_func('spec_enthalpy','temperature',spec_h2T)
    call matprop_set%add_func('vol_enthalpy','dynamic_viscosity',H2mu)
    call matprop_set%add_func('vol_enthalpy','kinematic_viscosity',H2nu)
    
    call matprop_set%finalize()
  end subroutine get_MSFR_salt_matprop
  
  real(dp) function T2rho(T) result(rho)
    real(dp), intent(in) :: T
    
    rho = 5108.0_dp - 0.8234_dp * T
  end function T2rho
  
  real(dp) function T2beta(T) result(beta)
    real(dp), intent(in) :: T
    
    beta = 0.8234_dp/T2rho(T)
  end function T2beta
  
  real(dp) function H2mu(H) result(mu)
    real(dp), intent(in) :: H
    
    real(dp) :: T
    
    T = H / (rho_ref * cp)
    call assert(T >= T_min,'T <= safety_margin*T_melt.')
    mu = 6.187e-4_dp * exp(772.2_dp / (T - 765.2_dp))
  end function H2mu
  
  real(dp) function H2nu(H) result(nu)
    real(dp), intent(in) :: H
    
    nu = H2mu(H) / rho_ref
  end function H2nu
  
  
  real(dp) function T2spec_h(T) result(spec_h)
    real(dp), intent(in) :: T
    
    call assert(T >= T_min,'T <= safety_margin*T_melt.')
    spec_h = cp * T
  end function T2spec_h

  real(dp) function spec_h2T(h) result(T)
    real(dp), intent(in) :: h
    
    T = h / cp
    call assert(T >= T_min,'T <= safety_margin*T_melt.')
  end function spec_h2T
end submodule matprop__lib_MSFR_salt

