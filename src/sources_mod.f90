module sources_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use exceptions, only: error
  use f90_kind, only: dp
  use face_integrals_mod, only: list_bc_id_faces, my_integral, unity_at_qps, n_dot_u_at_qps
  use func_operations_mod, only: operator(+), operator(-), operator(*), operator(/)
  use mesh_objects, only: elem_list, face_ptr
  use mesh_type, only: fem_mesh
  use mpi_wrappers_mod, only: allreduce_wrap
  use numerical_solutions, only: matprop_set, velocity
  use petsc_mod, only: i_am_master_process
  use pnt_set_mod, only: pnt_set_t
  use run_data, only: mesh_dimen
  use scal_func_mod, only: scal_func_type
  use vec_func_mod, only: vec_func_type
  implicit none
  
  private
  public :: add_tot_enthalpy_src_to_pde
  public :: add_tot_vol_force_to_pde
  public :: force_is_constant, enthalpy_src_is_constant
  public :: initialize
  public :: adaptive_force, adapt_vol_force_to_flowthrough
  public :: spec_force_func ! public only for RANS modules
  
  class(vec_func_type), allocatable, protected :: spec_force_func
  class(vec_func_type), allocatable :: vol_force_func
  class(scal_func_type), allocatable :: p_drop_coeff_func
  real(dp) :: ref_density = 0.0_dp
  
  class(scal_func_type), allocatable :: htc, htc_T_ext
  class(scal_func_type), allocatable :: vol_enthalpy_src_func
  
  logical, protected :: force_is_constant = .false., enthalpy_src_is_constant = .false.

  ! This type is just here to recast the 'total_force' subroutine into the type that's needed for assembly:
  type, extends(vec_func_type) :: total_force_t
  contains
    procedure, non_overridable :: get_values => total_force
    procedure, non_overridable :: no_dirs => dimen_func
  end type total_force_t


  integer :: adaptive_force = 0
  integer :: adaptive_force_face_id
  real(dp) :: adaptive_force_force
  real(dp) :: adaptive_force_target_integral_n_in_u
  real(dp) :: adaptive_force_overrelaxation = 0.1_dp
  integer :: adaptive_force_dir

contains
  subroutine initialize(mesh)
    use functions_lib, only: string2func
    use io_basics, only: open_input_file
    use run_data, only: file_path_sources_specs
    use string_manipulations, only: char2int, char2real
    
    type(fem_mesh), intent(in) :: mesh

    integer :: unit_number
    character(:), allocatable :: vol_enthalpy_src(:), vol_force(:)
    character(:), allocatable :: packed_list(:)
    character(:), allocatable :: vol_htc_ext_temperature, vol_htc, spec_force, p_drop_coeff
    namelist /sources_specs/ &
        spec_force, vol_force, ref_density, &
        vol_htc_ext_temperature, vol_htc, vol_enthalpy_src,&
        p_drop_coeff, &
        force_is_constant, enthalpy_src_is_constant
    integer :: iostat, i
    character(200) :: iomsg
    class(vec_func_type), allocatable :: f_i, new
    class(scal_func_type), allocatable :: aux_scalfunc, single_vol_enthalpy_src
    
    real(dp) :: adaptive_force_init_force
    real(dp) :: adaptive_force_target_avg_n_in_u
    type(face_ptr), allocatable :: my_faces(:)
    real(dp) :: my_area, area
    namelist /sources_specs/ &
        adaptive_force, &
        adaptive_force_face_id, &
        adaptive_force_init_force, &
        adaptive_force_target_avg_n_in_u, &
        adaptive_force_dir, &
        adaptive_force_overrelaxation
    

    allocate(character(10000) :: vol_htc_ext_temperature, vol_htc, spec_force, p_drop_coeff, vol_enthalpy_src(100), vol_force(100))
    ! Assign blanks to the char slices, not the chars themselves, to avoid re-allocation of the length:
    vol_enthalpy_src(:) = ''
    vol_force(:) = ''
    vol_htc_ext_temperature(:) = ''
    vol_htc(:) = ''
    spec_force(:) = ''
    p_drop_coeff(:) = ''
    call open_input_file(file_path_sources_specs,unit_number)
      read(unit_number,sources_specs,iostat=iostat,iomsg=iomsg)
      call assert(iostat == 0,'problem reading sources: iomsg=' // trim(iomsg))
    close(unit_number)
    
    ! Momentum sources:
    if (spec_force /= '') call string2func(spec_force,spec_force_func)
    ! Sum the functions in the 'vol_force' list:
    packed_list = pack(vol_force,vol_force /= '')
    do i = 1, size(packed_list)
      call string2func(packed_list(i),f_i)
      if (i == 1) then
        call move_alloc(f_i,new)
      else
        new = vol_force_func + f_i
      endif
      call move_alloc(new,vol_force_func)
    enddo
    
    if (p_drop_coeff /= '') call string2func(p_drop_coeff,p_drop_coeff_func)
    
    ! HTC sources:
    if (assert_eq(vol_htc /= '', vol_htc_ext_temperature /= '')) then
      call string2func(vol_htc,htc)
      call string2func(vol_htc_ext_temperature,htc_T_ext)
    endif
    
    ! Sum the functions in the 'vol_enthalpy_src' list:
    do i = 1, size(vol_enthalpy_src)
      if (vol_enthalpy_src(i) == '') cycle

      call string2func(vol_enthalpy_src(i),single_vol_enthalpy_src)
      if (allocated(vol_enthalpy_src_func)) then
        call move_alloc(vol_enthalpy_src_func,aux_scalfunc)
        allocate(vol_enthalpy_src_func,source=aux_scalfunc + single_vol_enthalpy_src)
      else
        allocate(vol_enthalpy_src_func,source=single_vol_enthalpy_src)
      endif
    enddo

    if (adaptive_force > 0) then
      adaptive_force_force = adaptive_force_init_force
      ! Calculate the desired integral of n*m from the target avg:
      call list_bc_id_faces(mesh,only_fluid=.true.,bc_id=adaptive_force_face_id,faces=my_faces)
      my_area = my_integral(my_faces,unity_at_qps)
      call allreduce_wrap(my_area,'sum',area)
      adaptive_force_target_integral_n_in_u = area * adaptive_force_target_avg_n_in_u
    endif
  end subroutine initialize
  
  subroutine adapt_vol_force_to_flowthrough(mesh)
    type(fem_mesh), intent(in) :: mesh
    
    type(face_ptr), allocatable :: my_faces(:)
    real(dp) :: my_n_in_u, n_in_u
    real(dp) :: new_estimate

    call list_bc_id_faces(mesh,only_fluid=.true.,bc_id=adaptive_force_face_id,faces=my_faces)
    my_n_in_u = my_integral(my_faces,n_dot_u_at_qps)
    call allreduce_wrap(my_n_in_u,'sum',n_in_u)
    ! Assuming a constant friction factor, the Darcy-Weisbach equation predicts pdrop ~ u**2:
    new_estimate = adaptive_force_force * (adaptive_force_target_integral_n_in_u / n_in_u)**2
    adaptive_force_force =  adaptive_force_overrelaxation * new_estimate &
                              + (1-adaptive_force_overrelaxation) * adaptive_force_force
    if (i_am_master_process()) write(*,*) 'adjusted vol force to ', adaptive_force_force
  end subroutine adapt_vol_force_to_flowthrough

  subroutine add_tot_vol_force_to_pde(pde)
    ! Add to pde all forces specified in input. Do nothing if there are no forces.
    use pde_mod, only: pde_t
    use source_pde_term_mod, only: source_t
    
    class(pde_t), intent(inout) :: pde
    
    type(source_t) :: vol_force_term
    
    if (allocated(spec_force_func) .or. allocated(vol_force_func) .or. adaptive_force > 0) then
      vol_force_term%src = total_force_t()
      call pde%add_term(vol_force_term)
    endif

    if (allocated(p_drop_coeff_func)) call add_p_drop_term_to_pde(pde)
  end subroutine add_tot_vol_force_to_pde
  
  subroutine total_force(this,elem,pnt_set,time,values)
    use run_data, only: boussinesq_force, boussinesq_T_ref

    class(total_force_t), intent(in) :: this
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:,:)

    integer :: p, np
    integer :: dimen
    class(scal_func_type), pointer :: rho_func, beta_func, T_func
    real(dp), allocatable :: rho(:), beta(:), T(:), spec_force(:,:), vol_force(:,:), rho_x_f(:,:)

    dimen = assert_eq(size(values,1),elem%dimen(),'total_force: mismatch in dimen',BOUNDS_CHECK)
    np = assert_eq(size(values,2),pnt_set%np(),'total_force: mismatch between pnt_set and size(values)',BOUNDS_CHECK)
    
    values = 0.0_dp
    
    if (allocated(spec_force_func)) then
      allocate(spec_force(dimen,np),rho_x_f(dimen,np))
      call spec_force_func%get_values(elem,pnt_set,time,spec_force)
      
      rho_func => matprop_set%func_ptr('density')
      allocate(rho(np))
      call rho_func%get_values(elem,pnt_set,time,rho)
      if (boussinesq_force) then
        beta_func => matprop_set%func_ptr('thermal_expansion_coeff')
        T_func => matprop_set%func_ptr('temperature')
        allocate(beta(np),T(np))
        call beta_func%get_values(elem,pnt_set,time,beta)
        call T_func%get_values(elem,pnt_set,time,T)
        do p = 1, np
          rho_x_f(:,p) = beta(p) * (boussinesq_T_ref - T(p)) * rho(p) * spec_force(:,p)
        enddo
      else
        do p = 1, np
          rho_x_f(:,p) = (rho(p) - ref_density) * spec_force(:,p)
        enddo
      endif
      values = values + rho_x_f
    endif

    if (allocated(vol_force_func)) then
      allocate(vol_force(dimen,np))
      call vol_force_func%get_values(elem,pnt_set,time,vol_force)
      values = values + vol_force
    endif

    if (adaptive_force > 0) values(adaptive_force_dir,:) = values(adaptive_force_dir,:) + adaptive_force_force
  end subroutine total_force

  pure integer function dimen_func(this) result(dimen)
    class(total_force_t), intent(in) :: this

    dimen = mesh_dimen
  end function dimen_func

  subroutine add_p_drop_term_to_pde(pde)
    ! Momentum source opposed to the flow: K * rho/2 * v**2
    ! The force can be treated implicitly, as a reaction term of the form:
    ! F(v) = - K * |v|/2 * (rho*v)
    use first_order_reaction_mod, only: first_order_reaction_t
    use pde_mod, only: pde_t
    use scalfunc_from_proc_mod, only: as_scalfunc
    
    type(pde_t), intent(inout) :: pde
    
    type(first_order_reaction_t) :: p_drop_term
    
    p_drop_term%rate = as_scalfunc(p_drop)
    call pde%add_term(p_drop_term)
  end subroutine add_p_drop_term_to_pde
  
  subroutine p_drop(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)

    real(dp) :: p_drop_coeff(size(values)), u(elem%dimen(),size(values))

    call assert(size(values) == pnt_set%np(),BOUNDS_CHECK)

    call velocity%get_values(elem,pnt_set,time,u)
    call p_drop_coeff_func%get_values(elem,pnt_set,time,p_drop_coeff)
    values = 0.5_dp * p_drop_coeff * norm2(u,dim=1)
  end subroutine p_drop

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine add_tot_enthalpy_src_to_pde(pde,scaling)
    use functions_lib, only: const_func
    use multiphysics_mod, only: get_fission_src_func
    use pde_mod, only: pde_t
    use run_data, only: multiphysics_mode
    use source_pde_term_mod, only: source_t
    
    class(pde_t), intent(inout) :: pde
    character(*), intent(in) :: scaling
    
    class(scal_func_type), allocatable :: S, fission_src, aux_scalfunc
    class(scal_func_type), allocatable :: rho, cp
    type(source_t) :: vol_src_term
    
    if (.not.(allocated(vol_enthalpy_src_func) .or. allocated(htc) .or. multiphysics_mode)) return
    
    allocate(S,source=const_func(0.0_dp))
    
    if (allocated(htc)) call add_htc_terms_to_pde(scaling,pde,S)
    
    if (allocated(vol_enthalpy_src_func)) then
      call move_alloc(S,aux_scalfunc)
      allocate(S,source=aux_scalfunc + vol_enthalpy_src_func)
    endif
    
    if (multiphysics_mode) then
      call get_fission_src_func(fission_src)
      call move_alloc(S,aux_scalfunc)
      allocate(S,source=aux_scalfunc + fission_src)
    endif
    
    select case(scaling)
      case('volumetric')
        call move_alloc(S,vol_src_term%src_all_dirs)
      case('temperature')
        ! Always use the matprop density, even when there is a predictor in the
        ! fluid. In practice this comes down to a frozen coefficient method.
        call matprop_set%get_func('spec_heat_capacity',rho)
        call matprop_set%get_func('spec_heat_capacity',cp)
        allocate(vol_src_term%src_all_dirs,source = S / (rho * cp))
      case default
        error stop "unknown scaling in 'add_tot_enthalpy_src_to_pde'"
    end select
    call pde%add_term(vol_src_term)
  end subroutine add_tot_enthalpy_src_to_pde
  
  subroutine add_htc_terms_to_pde(scaling,pde,rhs_func)
    ! Energy source: gamma*(T_ext-T)
    ! Must be split into a LHS part (gamma*T) and a RHS part (gamma*T_ext).
    use first_order_reaction_mod, only: first_order_reaction_t
    use pde_mod, only: pde_t
    
    character(*), intent(in) :: scaling
    type(pde_t), intent(inout) :: pde
    class(scal_func_type), allocatable, intent(inout) :: rhs_func

    type(first_order_reaction_t) :: lhs_part
    class(scal_func_type), allocatable :: to_add, temp
    class(scal_func_type), pointer :: T, h, rho, cp
    
    ! Always use the matprop density, even when there is a predictor in the
    ! fluid. In practice this comes down to a frozen coefficient method.
    h => matprop_set%func_ptr('spec_enthalpy')
    rho => matprop_set%func_ptr('density')
    cp => matprop_set%func_ptr('spec_heat_capacity')
    T => matprop_set%func_ptr('temperature')
    allocate(lhs_part%rate,source = htc / (rho * cp))
    call pde%add_term(lhs_part)
    
    select case(scaling)
      case('volumetric')
        ! We must linearize the src in terms of vol_enthalpy, according to:
        !   T^(n+1) - T^n ~ [H^(n+1) - H^n]/(rho*cp)^n 
        ! This leads to:
        !   o LHS part: gamma/(rho*cp)^n * H
        !   o RHS part: gamma*(T_ext + h^n/cp^n - T^n)
        to_add = htc * (htc_T_ext + h/cp - T)
      case('temperature')
        ! No need for linearization.
        !   o LHS part: gamma/(rho*cp) * T
        !   o RHS part: gamma/(rho*cp) * T_ext
        to_add = htc * htc_T_ext
        ! The division by rho*cp is performed in 'add_tot_enthalpy_src_to_pde'
      case default
        error stop "unknown scaling in 'add_htc_terms_to_pde'"
    end select
    call move_alloc(rhs_func,temp)
    allocate(rhs_func,source = temp + to_add)
  end subroutine add_htc_terms_to_pde
end module sources_mod
