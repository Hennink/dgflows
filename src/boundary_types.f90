module boundary_types
  use f90_kind, only: dp
  use support, only: bool_i, false, true
  implicit none
  
  public
  
  type, abstract :: bnd_type
    real(dp) :: total_area

  contains
    procedure(bool_i), nopass, deferred :: fixes_absolute_pressure
    procedure(bool_i), nopass, deferred :: has_inflow
    procedure(n_dot_velocity_at_qps_i), deferred :: n_dot_velocity_at_qps
    procedure(get_bc_i), deferred :: get_bc
    procedure(read_specs_from_file_interface), deferred :: read_specs_from_file
  end type bnd_type
  
  type, extends(bnd_type), abstract :: wall_bnd_t
  contains
    procedure, non_overridable, nopass :: fixes_absolute_pressure => false
    procedure, non_overridable, nopass :: has_inflow => false
    procedure :: n_dot_velocity_at_qps => zero_n_dot_velocity
  end type wall_bnd_t
  
  type, extends(bnd_type), abstract :: nonwall_bnd_t
  end type nonwall_bnd_t
  
  abstract interface
    function n_dot_velocity_at_qps_i(this,face,fluid_side,time)
      use f90_kind, only: dp
      use mesh_objects, only: face_list
      import :: bnd_type
      implicit none
      class(bnd_type), intent(in) :: this
      type(face_list), intent(in) :: face
      integer, intent(in) :: fluid_side
      integer, intent(in) :: time
      real(dp) :: n_dot_velocity_at_qps_i(face%nqp)
    end function n_dot_velocity_at_qps_i

    subroutine get_bc_i(this,qnty_name,bc)
      use boundary_condition_types, only: bc_type
      import :: bnd_type
      implicit none
      class(bnd_type), intent(in) :: this
      character(*), intent(in) :: qnty_name
      class(bc_type), allocatable, intent(out) :: bc
    end subroutine get_bc_i
    
    subroutine read_specs_from_file_interface(this,unit_number)
      import :: bnd_type
      implicit none
      class(bnd_type), intent(out) :: this
      integer, intent(in) :: unit_number
    end subroutine read_specs_from_file_interface
  end interface
  
  
  interface
    module subroutine noslip_fixedQ_wall(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine noslip_fixedQ_wall
    
    module subroutine noslip_htc_wall(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine noslip_htc_wall
    
    module subroutine noslip_fixedT_wall(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine noslip_fixedT_wall
    
    module subroutine noslip_isolated_wall(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine noslip_isolated_wall
    
    module subroutine noslip_internal_wall(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine noslip_internal_wall
    
    module subroutine alloc_symmetry(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine alloc_symmetry
    
    module subroutine alloc_wall_func_adiabatic(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine alloc_wall_func_adiabatic
    
    module subroutine alloc_wall_func_fixedQ(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine alloc_wall_func_fixedQ
    
    module subroutine alloc_wall_func_fixedT(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine alloc_wall_func_fixedT
    
    module subroutine alloc_fixed_velocity(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine alloc_fixed_velocity
    
    module subroutine alloc_outflow(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine alloc_outflow
    
    module subroutine solid_fixedT(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine solid_fixedT
    
    module subroutine solid_isolated(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine solid_isolated
    
    module subroutine solid_fixedQ(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine solid_fixedQ
    
    module subroutine solid_htc(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine solid_htc

    module subroutine alloc_periodic(bnd)
      implicit none
      class(bnd_type), allocatable, intent(out) :: bnd
    end subroutine alloc_periodic
  end interface
    
contains
  function zero_n_dot_velocity(this,face,fluid_side,time)
    use f90_kind, only: dp
    use mesh_objects, only: face_list

    class(wall_bnd_t), intent(in) :: this
    type(face_list), intent(in) :: face
    integer, intent(in) :: fluid_side
    integer, intent(in) :: time
    real(dp) :: zero_n_dot_velocity(face%nqp)
    
    zero_n_dot_velocity = 0.0_dp
  end function zero_n_dot_velocity
end module boundary_types
