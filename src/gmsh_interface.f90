module gmsh_interface
  use assertions, only: assert, assert_eq
  use f90_kind, only: dp
  use io_basics, only: INT_FMT, REAL_FMT, nints_fmt, my_temp_proc_name, merge_temp_proc_files
  use mesh_objects, only: elem_id, elem_list, vertex_coordinate
  use mesh_partitioning, only: my_first_last_elems
  use mesh_type, only: fem_mesh
  use petsc_mod, only: I_am_master_process, petsc_mpi_barrier
  use system_commands, only: unix_cat_files, unix_rm_files
  implicit none

  private
  public :: output_elem_partition_to_gmsh
  public :: write_active_elem_no_to_gmsh_file
  public :: write_gmsh_mesh
  public :: write_gmsh_elementwise_sol, write_to_gmsh_file

  interface write_to_gmsh_file
    module procedure write_scalfunc_to_gmsh_file
    module procedure write_vec_func_to_gmsh_file
    module procedure write_values_to_gmsh_file
  end interface write_to_gmsh_file

contains
  subroutine write_gmsh_mesh(mesh,file)
    use local_elem, only: no_dg_functions

    character(*), intent(in) :: file
    type(fem_mesh), intent(inout) :: mesh
    
    integer :: unit
    integer :: vertex, help_elem
    integer, allocatable :: permutation(:)
    integer :: iostat
    character(200) :: iomsg
    
    write(*,"('Generating mesh file...')",advance='no')
    
    open(                                         &
        file=file, newunit=unit,                  &
        action='write',pad='no',status='replace', &
        iostat=iostat,iomsg=iomsg                 &
    )
      call assert(iostat == 0,iomsg)

      write(unit,'(a)') '$MeshFormat'
      write(unit,'(a)') '2.1 0 8'
      write(unit,'(a)') '$EndMeshFormat'
      
      write(unit,'(a)') '$Nodes'
      write(unit,*) mesh%no_vertices
      mesh%vertex_list_ptr => mesh%vertex_list_head
      do vertex = 1, mesh%no_vertices
        write(unit,'(i20,3' // REAL_FMT // ')') vertex,mesh%vertex_list_ptr%coordinate
        if (associated(mesh%vertex_list_ptr%next)) mesh%vertex_list_ptr => mesh%vertex_list_ptr%next
      enddo
      nullify(mesh%vertex_list_ptr)
      write(unit,'(a)') '$EndNodes'
      
      write(unit,'(a)') '$Elements'
      write(unit,'(i20)') mesh%tot_no_elem
      mesh%elem_list_ptr => mesh%elem_list_head
      allocate(permutation(no_dg_functions(38,8)))
      help_elem = 1
      call write_elem(unit,mesh%elem_list_ptr,help_elem,permutation)
      nullify(mesh%elem_list_ptr)
      nullify(mesh%vertex_list_ptr)
      write(unit,'(a)') '$EndElements'
    close(unit,iostat=iostat,iomsg=iomsg)
    call assert(iostat == 0,iomsg)
    
    write(*,"('done.')")
  end subroutine
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  pure integer function get_gmsh_eltype(eltype) result(gmsh_eltype)
    integer, intent(in) :: eltype
    
    select case(eltype)
      case(12)
        gmsh_eltype = 1
      case(13)
        gmsh_eltype = 8
      case(23)
        gmsh_eltype = 2
      case(26)
        gmsh_eltype = 9
      case(24)
        gmsh_eltype = 3
      case(28)
        gmsh_eltype = 16
      case(29)
        gmsh_eltype = 10
      case(34)
        gmsh_eltype = 4
      case(38)
        gmsh_eltype = 5
      case(310)
        gmsh_eltype = 11
      case(320)
        gmsh_eltype = 17
      case(327)
        gmsh_eltype = 12
      case default
        error stop "get_gmsh_eltype: unknown eltype"
    end select
  end function get_gmsh_eltype
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  recursive subroutine write_elem(unit_var,elem_head,elem,permutation)
    integer, intent(in) :: unit_var
    type(elem_list), pointer, intent(in) :: elem_head
    integer, intent(inout) :: elem
    integer, intent(inout) :: permutation(:)
    
    integer :: child, no_children, vertex
    
    ! for any active element write out element or seek children to handle
    if (elem_head%active) then
      call get_vertex_permutation(elem_head%eltype,.true.,permutation)
      write(unit_var,'(i20,i20,a3,i20,i20,i20,27i20)') elem, get_gmsh_eltype(elem_head%eltype), '3', &
                                                       elem_head%mat_id,elem_head%mat_id,elem, &
                                                      (elem_head%vertices(permutation(vertex))%point%vertex_no, &
                                                       vertex = 1, size(elem_head%vertices))
      elem = elem + 1
    else 
      ! not active so process the children
      no_children = size(elem_head%children)
      do child = 1, no_children
        call write_elem(unit_var,elem_head%children(child)%point,elem,permutation)
      enddo
    endif
  end subroutine write_elem
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  pure subroutine get_vertex_permutation(eltype,inverse,permutation)
    integer, intent(in) :: eltype
    logical, intent(in) :: inverse
    integer, intent(out) :: permutation(:)
    
    integer :: i
    
    ! default to no permutation:
    permutation =  [(i, i = 1, size(permutation))]
    
    if (eltype==34) then
      if (.not.inverse) then
        permutation(1) = 2
        permutation(2) = 3
        permutation(3) = 4
        permutation(4) = 1
      else
        permutation(1) = 4
        permutation(2) = 1
        permutation(3) = 2
        permutation(4) = 3
      endif
    endif
  end subroutine get_vertex_permutation
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine write_active_elem_no_to_gmsh_file(mesh,only_fluid,file_path)
    use mesh_partitioning, only: my_first_last_elems
    
    type(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid
    character(*), intent(in) :: file_path
    
    integer :: first_elem, last_elem, i
    real(dp), allocatable :: my_active_elem_numbers(:)
    
    call my_first_last_elems(only_fluid,first_elem,last_elem)
    my_active_elem_numbers = [(i, i = first_elem, last_elem)]
    call write_gmsh_elementwise_sol(file_path,my_active_elem_numbers,mesh,only_fluid,file_path//'.pos')
  end subroutine write_active_elem_no_to_gmsh_file
  
  subroutine output_elem_partition_to_gmsh(mesh,file_path)
    use mesh_partitioning, only: partition_of_elem, my_first_last_elems
    
    type(fem_mesh), intent(in) :: mesh
    character(*), intent(in) :: file_path
    
    logical :: ONLY_FLUID = .false.
    integer :: elem_no, first_elem, last_elem
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    real(dp), allocatable :: my_partitions(:)
    
    call my_first_last_elems(ONLY_FLUID,first_elem,last_elem)
    allocate(my_partitions(first_elem:last_elem))
    do elem_no = first_elem, last_elem
      id = mesh%active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      my_partitions(elem_no) = partition_of_elem(elem)
    enddo
    call write_gmsh_elementwise_sol(file_path,my_partitions,mesh,ONLY_FLUID,file_path//'.pos')
  end subroutine output_elem_partition_to_gmsh
  
  subroutine write_vec_func_to_gmsh_file(vec_func,mesh,file,only_fluid,time)
    use exceptions, only: error
    use scal_func_mod, only: scal_func_type
    use string_manipulations, only: int2char
    use vec_func_mod, only: vec_func_type
    use run_data, only: mesh_dimen
    
    class(vec_func_type), intent(in)  :: vec_func
    type(fem_mesh), intent(in) :: mesh
    character(*), intent(in) :: file
    logical, intent(in) :: only_fluid
    integer, intent(in) :: time
    
    integer :: nnames
    character(len(file)+10) :: names(10)
    integer :: dir, ndirs

    if (I_am_master_process()) write(*,'(a)',advance='no') 'writing vec field to ' // file // '.pos... '
    
    ndirs = vec_func%no_dirs()
    names(:ndirs) = [(file // '_' // int2char(dir), dir = 1, ndirs)]
    names(ndirs+1) = file // '_abs'
    nnames = ndirs + 1
    if (mesh_dimen == 2) then
      names(nnames+1) = file // '_curl'
      nnames = nnames + 1
    elseif (mesh_dimen == 3) then
      names(nnames+1:nnames+3) = [(file // '_curl' // '_' // int2char(dir), dir = 1, ndirs)]
      nnames = nnames + 3
    else
      call error("write_vec_func_to_gmsh_file: weird mesh_dimen")
    endif
    call write_values_to_gmsh_file(mesh,names(:nnames),only_fluid,get_vecfunc_vals)
    if (I_am_master_process()) write (*,'(a)') 'done'
  
  contains
    subroutine get_vecfunc_vals(elem,pnt_set,vals)
      use mesh_objects, only: elem_list
      use pnt_set_mod, only: pnt_set_t
      
      type(elem_list), intent(in) :: elem
      type(pnt_set_t), intent(in) :: pnt_set
      real(dp), intent(out) :: vals(:,:)
      
      real(dp), allocatable :: vec(:,:), gders(:,:,:)
      integer :: np
      
      np = assert_eq(size(vals,1),pnt_set%np())

      allocate(vec(ndirs,np))
      allocate(gders(mesh_dimen,ndirs,np))
      call vec_func%get_values(elem,pnt_set,time,vec)
      call vec_func%get_gders(elem,pnt_set,time,gders)
      vals(:,:ndirs)  = transpose(vec)
      vals(:,ndirs+1) = norm2(vec,dim=1)
      if (mesh_dimen == 2) then
        vals(:,ndirs+2) = gders(1,2,:) - gders(2,1,:)
      elseif (mesh_dimen == 3) then
        vals(:,ndirs+2) = gders(2,3,:) - gders(3,2,:)
        vals(:,ndirs+3) = gders(3,1,:) - gders(1,3,:)
        vals(:,ndirs+4) = gders(1,2,:) - gders(2,1,:)
      else
        call error("write_vec_func_to_gmsh_file: weird mesh_dimen")
      endif
    end subroutine get_vecfunc_vals
  end subroutine write_vec_func_to_gmsh_file
  
  subroutine write_scalfunc_to_gmsh_file(scalfunc,mesh,file,only_fluid,time)
    use scal_func_mod, only: scal_func_type
    
    class(scal_func_type), intent(in)  :: scalfunc
    type(fem_mesh), intent(in) :: mesh
    character(*), intent(in) :: file
    logical, intent(in) :: only_fluid
    integer, intent(in) :: time
    
    if (I_am_master_process()) write(*,'(a)',advance='no') 'writing scal field to ' // file // '.pos... '
    call write_values_to_gmsh_file(mesh,[file],only_fluid,get_scalfunc_vals)
    if (I_am_master_process()) write (*,'(a)') 'done'
  contains
    subroutine get_scalfunc_vals(elem,pnt_set,vals)
      use mesh_objects, only: elem_list
      use pnt_set_mod, only: pnt_set_t
      
      type(elem_list), intent(in) :: elem
      type(pnt_set_t), intent(in) :: pnt_set
      real(dp), intent(out) :: vals(:,:)
      
      call scalfunc%get_values(elem,pnt_set,time,vals(:,1))
    end subroutine get_scalfunc_vals
  end subroutine write_scalfunc_to_gmsh_file
  
  subroutine write_values_to_gmsh_file(mesh,names,only_fluid,get_vals)
    use local_elem, only: no_vertices, no_dg_functions
    use local_global_mappings, only: gcors2pnt_set
    use pnt_set_mod, only: pnt_set_t
    use qnty_handler_mod, only: max_sol_order
    use run_data, only: average_restress
    
    interface
      subroutine get_vals_i(elem,pnt_set,vals)
        ! `vals(p,q)` is the value of quantity `q` at point `p`.
        use f90_kind, only: dp
        use mesh_objects, only: elem_list
        use pnt_set_mod, only: pnt_set_t
        implicit none
        type(elem_list), intent(in) :: elem
        type(pnt_set_t), intent(in) :: pnt_set
        real(dp), intent(out) :: vals(:,:)
      end subroutine get_vals_i
    end interface
    
    type(fem_mesh), intent(in) :: mesh
    character(*), intent(in) :: names(:)
    logical, intent(in) :: only_fluid
    procedure(get_vals_i) :: get_vals
    
    integer :: no_vert, first_elem, last_elem
    integer :: loc_vertex, elem_no
    type(elem_id) :: id
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_list), pointer :: elem
    real(dp), allocatable :: gcor_vertices(:,:), values(:,:)
    integer :: func_order
    type(pnt_set_t) :: pnt_set
    integer, allocatable :: permutation(:)
    character(5000) :: filenames(size(names))
    integer :: units(size(names))
    integer :: i, ios, start_unit, end_unit
    character(200) :: iomsg
    character(:), allocatable :: start_file, end_file
    
    allocate(permutation(no_dg_functions(38,8)))
    
    call mesh%get_active_elem_list_ptr(only_fluid,active_elem_list)
    call my_first_last_elems(only_fluid,first_elem,last_elem)
    
    do i = 1, size(names)
      filenames(i) = trim(names(i)) // '.pos'
      open(newunit=units(i),file=my_temp_proc_name(filenames(i)), &
           iostat=ios,status='replace',action='write',position='rewind',iomsg=iomsg)
      call assert(ios == 0, 'in opening file, iomsg=' // trim(iomsg))
    enddo
    
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      no_vert = no_vertices(elem%eltype)
      
      call elem%glob_coord_vertices(gcor_vertices)
      ! A hack to make sure there are enough nodes when this is called for the Reynolds stresses.
      func_order = merge(2,1,average_restress) * max_sol_order(elem)
      call gcors2pnt_set(elem,func_order,gcor_vertices,pnt_set)
      allocate(values(no_vert,size(names)))
      call get_vals(elem,pnt_set,values)
      
      ! TODO: Permuting still needs coding on gmsh output
      call get_vertex_permutation(elem%eltype,.true.,permutation)
      
      do i = 1, size(names)
        write (units(i),'(i20,i20,27es25.16e3)') elem%tot_elem_no, no_vert, &
                      (values(permutation(loc_vertex),i),loc_vertex=1,no_vert)
      enddo
      deallocate(values)
    enddo
    
    do i = 1, size(names)
      close(units(i),status='keep',iostat=ios,iomsg=iomsg)
      call assert(ios == 0,'in closing file, iomsg=' // trim(iomsg))
    enddo
    
    call petsc_mpi_barrier()
    if (I_am_master_process()) then
      do i = 1, size(names)
        start_file = trim(filenames(i)) // '_start.temp'
        end_file = trim(filenames(i)) // '_end.temp'

        open(file=start_file, newunit=start_unit,&
             status='replace', position='rewind', action='write', pad='no', &
             iostat=ios,iomsg=iomsg)
        call assert(ios==0,trim(iomsg))
          write(start_unit,'(a)') '$MeshFormat'
          write(start_unit,'(a)') '2.1 0 8'
          write(start_unit,'(a)') '$EndMeshFormat'
          write(start_unit,'(a)') '$ElementNodeData'
          write(start_unit,'(a)') '1'
          write(start_unit,'(a)',advance='no') '"' // trim(names(i))
          write(start_unit,'(a)') '"'
          write(start_unit,'(a)') '1'
          write(start_unit,'(a)') '1.0'
          write(start_unit,'(a)') '3'
          write(start_unit,'(a)') '0'
          write(start_unit,'(a)') '1'
          write(start_unit,'(i20)') mesh%get_no_active_elem(only_fluid)
        close(start_unit,status='keep',iostat=ios,iomsg=iomsg)
        call assert(ios==0,trim(iomsg))
            
        open(file=end_file, newunit=end_unit,&
             status='replace', position='rewind', action='write', pad='no', &
             iostat=ios,iomsg=iomsg)
        call assert(ios==0,trim(iomsg))
          write (end_unit,'(a)') '$EndElementNodeData'
        close(end_unit,status='keep',iostat=ios,iomsg=iomsg)
        call assert(ios==0,trim(iomsg))
        
        call merge_temp_proc_files(trim(filenames(i)),head=start_file,tail=end_file)
      enddo
    endif
  end subroutine write_values_to_gmsh_file
  
  subroutine write_gmsh_elementwise_sol(name,my_solution,mesh,only_fluid,filename)
    character(*), intent(in) :: name
    real(dp), intent(in) :: my_solution(:)
    type(fem_mesh), intent(in) :: mesh
    logical, intent(in) :: only_fluid
    character(*), intent(in) :: filename
    
    integer :: unit
    integer :: elem_no, my_nelem, first_elem, last_elem
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_id) :: id
    type(elem_list), pointer :: elem
    character(:), allocatable :: start_file, end_file
    integer :: start_unit, end_unit
    character(200) :: iomsg
    integer :: iostat
    
    call my_first_last_elems(only_fluid,first_elem,last_elem)
    my_nelem = last_elem - first_elem + 1
    call assert(size(my_solution) == my_nelem,'write_gmsh_elementwise_sol: Expected size(my_solution) == my_nelem')
    call mesh%get_active_elem_list_ptr(only_fluid,active_elem_list)
    
    open(file=my_temp_proc_name(filename), newunit=unit,&
         status='replace', position='rewind', action='write', pad='no', &
         iostat=iostat,iomsg=iomsg)
    call assert(iostat==0,trim(iomsg))
      do elem_no = first_elem, last_elem
        id = active_elem_list(elem_no)
        elem => mesh%get_elem_ptr(id)
        write(unit,'(' // INT_FMT // ',' // REAL_FMT // ')') elem%tot_elem_no, my_solution(elem_no-first_elem+1)
      enddo
    close(unit,iostat=iostat,iomsg=iomsg)
    call assert(iostat==0,trim(iomsg))
    
    call petsc_mpi_barrier()
    if (I_am_master_process()) then
      start_file = filename // '_start.temp'
      end_file = filename // '_end.temp'
      
      open(file=start_file, newunit=start_unit,&
           status='replace', position='rewind', action='write', pad='no', &
           iostat=iostat,iomsg=iomsg)
      call assert(iostat==0,trim(iomsg))
        write(start_unit,'(a)') '$MeshFormat'
        write(start_unit,'(a)') '2.1 0 8'
        write(start_unit,'(a)') '$EndMeshFormat'
        write(start_unit,'(a)') '$ElementData'
        write(start_unit,'(a)') '1'
        write(start_unit,'(a,i20,i20,a)') '"' // name
        write(start_unit,'(a)') '1'
        write(start_unit,'(a)') '1.0'
        write(start_unit,'(a)') '3'
        write(start_unit,'(a)') '0'
        write(start_unit,'(a)') '1'
        write(start_unit,nints_fmt(1)) size(active_elem_list)
      close(start_unit,iostat=iostat,iomsg=iomsg)
      call assert(iostat==0,trim(iomsg))
        
      open(file=end_file, newunit=end_unit,&
           status='replace', position='rewind', action='write', pad='no', &
           iostat=iostat,iomsg=iomsg)
      call assert(iostat==0,trim(iomsg))
        write(end_unit,'(a)') '$EndElementData'
      close(end_unit,iostat=iostat,iomsg=iomsg)
      call assert(iostat==0,trim(iomsg))

      call merge_temp_proc_files(trim(filename),head=start_file,tail=end_file)
    endif
  end subroutine write_gmsh_elementwise_sol
end module gmsh_interface

