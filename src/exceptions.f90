module exceptions
  use, intrinsic :: iso_fortran_env, only: ERROR_UNIT, OUTPUT_UNIT
  implicit none
  
  private
  public :: warning
  public :: error
  
contains
  subroutine warning(msg)
    character(*), intent(in) :: msg

    call write_to_stderr('WARNING:  ' // msg)
  end subroutine warning
  
  subroutine error(msg)
    character(*), optional, intent(in) :: msg

    flush(OUTPUT_UNIT)
    
#ifdef __INTEL_COMPILER
    block
      use ifcore, only: tracebackqq
      ! An ifort compiler extension to provides traceback information.
      ! https://software.intel.com/en-us/fortran-compiler-developer-guide-and-reference-tracebackqq
      call tracebackqq(string='ENCOUNTERED AN ERROR',user_exit_code=-1)
    end block
#endif

    if (present(msg)) call write_to_stderr('ERROR:  ' // msg)
    flush(ERROR_UNIT)
    error stop
  end subroutine error

  subroutine write_to_stderr(msg)
    character(*), intent(in) :: msg

    integer :: iostat
    character(1000) :: iomsg

    write(ERROR_UNIT,'(a)',iostat=iostat,iomsg=iomsg) msg
    if (iostat /= 0) then
      write(*,"('CANNOT WRITE TO ERROR_UNIT. iomsg=',a)") trim(iomsg)
      error stop
    endif
  end subroutine write_to_stderr
end module exceptions
