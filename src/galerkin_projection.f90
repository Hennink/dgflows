module galerkin_projection
  use assertions, only: assert
  use dof_handler, only: dof_handler_type, get_dof
  use f90_kind, only: dp
  use mesh_objects, only: elem_id, elem_list
  use mesh_type, only: fem_mesh
  use petsc_vec_mod, only: petsc_vec_type
  use vec_func_mod, only: vec_func_type
  implicit none
  
  private
  public :: project_onto_different_orders
  public :: galerkin_proj
  
  interface galerkin_proj
    module procedure galerkin_proj_of_scal_func
    module procedure galerkin_proj_of_vec_func
  end interface galerkin_proj
  
contains
  subroutine project_onto_different_orders(mesh,old,new_p,new)
    ! 'new_p' is the full vector, for all processes, not just for my elems.
    ! 
    ! Do not alias 'new' and 'old'.
    use solvec_as_vecfunc_mod, only: num_vec_func_t, solvec_as_vecfunc
    use solvec_mod, only: solvec_t

    type(fem_mesh), intent(in) :: mesh
    class(solvec_t), intent(in) :: old
    integer, intent(in) :: new_p(:)
    class(solvec_t), intent(out) :: new

    logical :: only_fluid
    type(num_vec_func_t) :: old_func
    integer :: time

    only_fluid = old%handler%only_fluid_mesh
    call assert(size(new_p) == mesh%get_no_active_elem(only_fluid),'project_onto_different_orders: mismatching sizes')

    call new%init_to_zero(          &
        name=old%name,              &
        mesh=mesh,                  &
        ncoeffs=old%ncoeffs(),      &
        no_dirs=old%no_dirs(),      &
        orders=new_p,               &
        only_fluid_mesh=only_fluid  &
    )

    old_func = solvec_as_vecfunc(old)
    do time = old%ncoeffs(), 1, -1
      call galerkin_proj_of_vec_func(mesh,new%handler,old_func,time,new%petsc_coeffs)
      call new%update_ghost_values_to_f90idx(time)
    enddo
  end subroutine

  subroutine galerkin_proj_of_scal_func(mesh,handler,continuous_func,time,discrete_proj)
    use scal_func_mod, only: scal_func_type
    use vec_func_scal_compon_mod, only: as_vecfunc
    
    type(fem_mesh), intent(in) :: mesh
    type(dof_handler_type), intent(in) :: handler
    class(scal_func_type), intent(in) :: continuous_func
    integer, intent(in) :: time
    type(petsc_vec_type), intent(inout) :: discrete_proj
    
    call assert(handler%no_dirs == 1)
    
    call galerkin_proj_of_vec_func(mesh,handler,as_vecfunc(continuous_func),time,discrete_proj)
  end subroutine galerkin_proj_of_scal_func
  
  subroutine galerkin_proj_of_vec_func(mesh,handler,continuous_func,time,discrete_proj)
    use mesh_partitioning, only: my_first_last_elems
    
    type(fem_mesh), intent(in) :: mesh
    type(dof_handler_type), intent(in) :: handler
    class(vec_func_type), intent(in) :: continuous_func
    integer, intent(in) :: time
    type(petsc_vec_type), intent(inout) :: discrete_proj
    
    integer :: elem_no
    type(elem_id) :: id
    type(elem_id), pointer :: active_elem_list(:)
    type(elem_list), pointer :: elem
    
    real(dp), allocatable :: local_discrete_proj(:,:)
    integer, allocatable :: rows(:)
    integer :: order, nnod
    integer :: dir
    integer :: first_elem, last_elem
    
    call assert(continuous_func%no_dirs() == handler%no_dirs,'galerkin_proj_of_vec_func: bad # dirs')
    
    call my_first_last_elems(handler%only_fluid_mesh,first_elem,last_elem)
    call mesh%get_active_elem_list_ptr(handler%only_fluid_mesh,active_elem_list)
    
    do elem_no = first_elem, last_elem
      id = active_elem_list(elem_no)
      elem => mesh%get_elem_ptr(id)
      order = handler%order(elem_no)
      
      call local_galerkin_proj(elem,order,continuous_func,time,local_discrete_proj)
      nnod = size(local_discrete_proj,1)
      
      do dir = 1, size(local_discrete_proj,2)
        call get_dof(handler,1,elem_no,dir,rows)
        call discrete_proj%insert(rows,local_discrete_proj(:,dir))
      enddo
    enddo
    call discrete_proj%finalize_assembly()
  end subroutine galerkin_proj_of_vec_func
  
  
  subroutine local_galerkin_proj(elem,order,continuous_func,time,local_discrete_proj)
    use local_integrals, only: int_fi_x_func_on_elem
    
    type(elem_list), intent(in) :: elem
    integer, intent(in) :: order
    class(vec_func_type), intent(in) :: continuous_func
    integer, intent(in) :: time
    real(dp), allocatable, intent(out) :: local_discrete_proj(:,:)
    
    call int_fi_x_func_on_elem(elem,order,continuous_func,time,local_discrete_proj)
    call elem%solve_for_massmat(local_discrete_proj)
  end subroutine local_galerkin_proj
end module galerkin_projection

