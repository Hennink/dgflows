submodule(boundary_types) boundary_types__solid_fixedT
  use func_set_mod, only: func_set_t
  implicit none
  
  type, extends(wall_bnd_t) :: solid_fixedT_t
    type(func_set_t) :: fixed_values
  contains
    procedure, non_overridable :: read_specs_from_file
    procedure, non_overridable :: get_bc
  end type solid_fixedT_t
  
contains
  module procedure solid_fixedT
    allocate(solid_fixedT_t :: bnd)
  end procedure solid_fixedT
  
  subroutine read_specs_from_file(this,unit_number)
    use matprop_mod, only: expand_func_set_with_matprop
    use read_funcs_mod, only: read_block_as_func_specs
    
    class(solid_fixedT_t), intent(out) :: this
    integer, intent(in) :: unit_number
    
    call read_block_as_func_specs(unit_number,this%fixed_values)
    call expand_func_set_with_matprop(this%fixed_values)
  end subroutine read_specs_from_file
  
  subroutine get_bc(this,qnty_name,bc)
    use boundary_condition_types, only: bc_type, dirichlet_bc_type
    
    class(solid_fixedT_t), intent(in) :: this
    character(*), intent(in) :: qnty_name
    class(bc_type), allocatable, intent(out) :: bc
    
    type(dirichlet_bc_type), allocatable :: dirichlet_bc
    
    select case(qnty_name)
      case('vol_enthalpy','temperature','spec_enthalpy')
        allocate(dirichlet_bc)
        call this%fixed_values%get_func(qnty_name,dirichlet_bc%inhom_part)
        call move_alloc(dirichlet_bc,bc)
      case default
        write(*,'(a)') 'ERROR: no bc for '//qnty_name//' in solid_fixedT'
        error stop
    end select
  end subroutine get_bc
end submodule boundary_types__solid_fixedT

