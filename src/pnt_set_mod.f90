module pnt_set_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  implicit none

  private
  public :: pnt_set_t
  
  type :: pnt_set_t
    ! Given the encompassing element, the tag uniquely identifies the point set.
    ! A negative value indicates that the point set is untagged.
    integer :: tag = -1 
    real(dp), allocatable :: lcors(:,:), gcors(:,:)
    real(dp), allocatable :: dg_funcs(:,:), dg_transp_gders(:,:,:)
  contains
    procedure, non_overridable :: set_tag
    procedure, non_overridable :: np, no_dg_funcs
    procedure, non_overridable :: is_tagged, is_tagged_by
  end type pnt_set_t

contains
  logical function is_tagged_by(this,tag)
    class(pnt_set_t), intent(in) :: this
    integer, intent(in) :: tag

    if (this%tag >= 0 .and. tag >= 0) then
      is_tagged_by = this%tag == tag
    else
      is_tagged_by = .false.
    endif
  end function is_tagged_by

  logical function is_tagged(this)
    class(pnt_set_t), intent(in) :: this
    
    is_tagged = this%tag >= 0
  end function is_tagged

  subroutine set_tag(this,description)
    ! Rather than setting tags directly, doing it with a character description
    ! makes it far less likely that we set the same tag in different places.
    use exceptions, only: error
    use string_manipulations, only: char2int

    class(pnt_set_t), intent(inout) :: this
    character(*), intent(in) :: description

    character(*), parameter :: STD_FQ = 'standard_face_quad'

    if (description == 'standard_vol_quad') then
      this%tag = 1
    elseif (description == 'group_avg_quad') then
      this%tag = 2
    elseif (description(:len(STD_FQ)) == STD_FQ) then
      this%tag = 10 + char2int(description(len(STD_FQ)+1:))
    else
      call error("uniq_tag: unknown description=" // description)
    endif
  end subroutine set_tag

  integer function np(this)
    class(pnt_set_t), intent(in) :: this

    integer :: sizes(4)
    logical :: allocs(size(sizes))

    allocs(1) = allocated(this%lcors)
    if (allocs(1)) sizes(1) = size(this%lcors,2)

    allocs(2) = allocated(this%gcors)
    if (allocs(2)) sizes(2) = size(this%gcors,2)

    allocs(3) = allocated(this%dg_funcs)
    if (allocs(3)) sizes(3) = size(this%dg_funcs,2)

    allocs(4) = allocated(this%dg_transp_gders)
    if (allocs(4)) sizes(4) = size(this%dg_transp_gders,3)

    np = assert_eq(pack(sizes,mask=allocs),BOUNDS_CHECK)
  end function np

  integer function no_dg_funcs(this)
    class(pnt_set_t), intent(in) :: this

    integer :: sizes(2)
    logical :: allocs(size(sizes))

    allocs(1) = allocated(this%dg_funcs)
    if (allocs(1)) sizes(1) = size(this%dg_funcs,1)

    allocs(2) = allocated(this%dg_transp_gders)
    if (allocs(2)) sizes(2) = size(this%dg_transp_gders,1)

    no_dg_funcs = assert_eq(pack(sizes,mask=allocs),BOUNDS_CHECK)
  end function no_dg_funcs
end module pnt_set_mod

