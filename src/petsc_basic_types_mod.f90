module petsc_basic_types_mod
#include "petsc/finclude/petscsys.h"
  use petscsys, only: PETSC_FALSE, PETSC_TRUE

  use f90_kind, only: dp
  implicit none

  private
  public :: fort2petsc
  public :: petsc2fort

  interface fort2petsc
    module procedure fort2petsc_bool
    module procedure fort2petsc_float
    module procedure fort2petsc_int
  end interface fort2petsc

  interface petsc2fort
    module procedure petsc2fort_bool
    module procedure petsc2fort_float
    module procedure petsc2fort_int
  end interface petsc2fort

contains
  pure function fort2petsc_bool(fort_logical) result(petsc_bool)
    logical, intent(in) :: fort_logical
    PetscBool :: petsc_bool

    if (fort_logical) then
      petsc_bool = PETSC_TRUE
    else
      petsc_bool = PETSC_FALSE
    endif
  end function fort2petsc_bool

  elemental function fort2petsc_int(fort_int) result(petsc_int)
    integer, intent(in) :: fort_int
    PetscInt :: petsc_int

    petsc_int = fort_int
  end function fort2petsc_int

  pure function fort2petsc_float(fort_real) result(petsc_scal)
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Sys/PetscScalar.html
    real(dp), intent(in) :: fort_real
    PetscReal :: petsc_scal

    petsc_scal = fort_real
  end function fort2petsc_float


  logical function petsc2fort_bool(petsc_bool) result(fort_logical)
    PetscBool, intent(in) :: petsc_bool

    if (petsc_bool .eqv. PETSC_TRUE) then
      fort_logical = .true.
    elseif (petsc_bool .eqv. PETSC_FALSE) then
      fort_logical = .false.
    else
      error stop 'ERROR in petsc2fort_bool: weird PetscBool value'
    endif
  end function petsc2fort_bool

  real(dp) function petsc2fort_float(petsc_scal) result(fort_real)
    PetscReal, intent(in) :: petsc_scal

    fort_real = petsc_scal
  end function petsc2fort_float

  elemental integer function petsc2fort_int(petsc_int) result(fort_int)
    PetscInt, intent(in) :: petsc_int

    fort_int = petsc_int
  end function petsc2fort_int
end module petsc_basic_types_mod
