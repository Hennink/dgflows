submodule(rans_mod) rans_mod__SA
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use curl_mod, only: curl_t, curl
  use mesh_objects, only: elem_list
  use numerical_solutions, only: matprop_set, num_scalfunc_set, velocity
  use pnt_set_mod, only: pnt_set_t
  use support, only: true
  use wall_dist_mod, only: wall_dist_func
  implicit none
  
  ! Von-Karman constant
  real(dp), parameter :: Karman = 0.41_dp
  
  ! Model constants
  real(dp), parameter :: sigma= 2.0_dp / 3.0_dp
  real(dp), parameter :: C_b1 = 0.1355_dp
  real(dp), parameter :: C_b2 = 0.622_dp
  real(dp), parameter :: C_nu1 = 7.1_dp
  real(dp), parameter :: C_nu2 = 0.7_dp
  real(dp), parameter :: C_nu3 = 0.9_dp
  real(dp), parameter :: C_w1 = C_b1 / (Karman**2) + (1.0_dp + C_b2) / sigma
  real(dp), parameter :: C_w2 = 0.3_dp
  real(dp), parameter :: C_w3 = 2.0_dp
  
  type, extends(rans_model_t) :: SA_model_t
  contains
    procedure, nopass :: is_low_Re => true
  end type SA_model_t
  
  type(curl_t) :: vorticity_func
  class(scal_func_type), allocatable :: nu_molec
  class(scal_func_type), allocatable :: num_nu_tilde, num_rho_nu_tilde
  logical :: add_buoyancy_term

contains
  module procedure get_SA_model
    call num_scalfunc_set%get_func('spec_turb1',num_nu_tilde)
    call num_scalfunc_set%get_func('vol_turb1',num_rho_nu_tilde)
    call matprop_set%get_func('kinematic_viscosity',nu_molec)
    vorticity_func = curl(velocity)
    
    allocate(SA_model_t :: rans_model)
    rans_model%kinem_visc%proc => kinem_visc_turb
    rans_model%diff_coeff_1st_eq%proc => diff_coeff_nu_tilde
    rans_model%source_1st_eq%proc => source_nu_tilde
    rans_model%react_coeff_1st_eq%proc => react_coeff_nu_tilde
    rans_model%no_eq = 1
  end procedure get_SA_model
  
  subroutine kinem_visc_turb(elem,pnt_set,time,values)
    ! The turbulent kinematic viscosity is bounded from below by a small
    ! fraction of the molecular viscosity
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    real(dp), dimension(size(values)) :: nu_tilde, f_nu1, nu
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call num_nu_tilde%get_values(elem,pnt_set,time,nu_tilde)    
    call eval_f_nu1(elem,pnt_set,time,f_nu1)
    
    do p = 1, no_points
      values(p) = max(f_nu1(p) * max(0.0_dp,nu_tilde(p)),1.0e-4_dp*nu(p))
    enddo
  end subroutine kinem_visc_turb
  
  subroutine diff_coeff_nu_tilde(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    real(dp), dimension(size(values)) :: nu_tilde, nu
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call num_nu_tilde%get_values(elem,pnt_set,time,nu_tilde)

    do p = 1, no_points
      if (nu_tilde(p) >= 0.0_dp) then
       values(p) = (nu(p) + nu_tilde(p) ) / sigma
      else
        values(p) = (nu(p) + nu_tilde(p) + (nu_tilde(p)**2)/(2.0_dp*nu(p))) / sigma
      endif
    enddo
  end subroutine diff_coeff_nu_tilde
  
  subroutine source_nu_tilde(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    real(dp), dimension(size(values)) :: nu_tilde, d_vals, r_vals, f_w, g_n, S_vals, rho
    real(dp), dimension(elem%dimen(),size(values)) :: grad_nu_tilde
    real(dp) :: coeff1, coeff2, coeff3, addition_term
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call num_nu_tilde%get_values(elem,pnt_set,time,nu_tilde)
    call num_nu_tilde%get_gders(elem,pnt_set,time,grad_nu_tilde)
    call density%get_values(elem,pnt_set,time,rho)
    call wall_dist_func%get_values(elem,pnt_set,time,d_vals)
    call eval_r(elem,pnt_set,time,r_vals)
    call eval_S(elem,pnt_set,time,S_vals)
    call eval_f_w(elem,pnt_set,time,f_w)
    call eval_gn(elem,pnt_set,time,g_n)
    
    do p = 1, no_points
      addition_term = (C_b2/sigma)*dot_product(grad_nu_tilde(:,p),grad_nu_tilde(:,p))
      if (nu_tilde(p) >= 0.0_dp) then
        coeff1 = (1.0_dp/(d_vals(p)**2))*(C_b1/(r_vals(p)* Karman**2) - C_w1*f_w(p))
        ! extra loop for positivity of source coeff:
        if (coeff1 >= 0.0_dp) then
          ! positive source coefficient
          values(p) = rho(p)* (coeff1 * nu_tilde(p)**2 + addition_term)
        else
          ! negative source coefficient
          ! in this situation add -2a*nu_tilde^n to LHS
          values(p) = rho(p) * (- coeff1 * nu_tilde(p)**2 + addition_term) 
        endif
      else
        coeff2 = C_b1 * S_vals(p) * g_n(p)
        coeff3 = C_w1 * (1.0_dp/d_vals(p))**2
        values(p) = rho(p) * max(coeff2*nu_tilde(p) + coeff3*nu_tilde(p)**2 + addition_term,0.0_dp)
      endif
    enddo
  end subroutine source_nu_tilde
  
  subroutine react_coeff_nu_tilde(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    real(dp), dimension(size(values)) :: d_vals, r_vals, f_w, nu_tilde
    real(dp) :: coeff1
    integer :: no_points, p
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call num_nu_tilde%get_values(elem,pnt_set,time,nu_tilde)
    call wall_dist_func%get_values(elem,pnt_set,time,d_vals)
    call eval_r(elem,pnt_set,time,r_vals)
    call eval_f_w(elem,pnt_set,time,f_w)
    
    do p = 1, no_points
      if (nu_tilde(p) >= 0.0_dp) then
        coeff1 = (1.0_dp/(d_vals(p)**2))*(C_b1/(r_vals(p)* Karman**2) - C_w1*f_w(p))
        values(p) = 2.0_dp * max(-coeff1, 0.0_dp) * nu_tilde(p)
      else
        values(p) = 0.0_dp
      endif
    enddo
  end subroutine react_coeff_nu_tilde
    
  subroutine eval_f_nu1(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    real(dp), dimension(size(values)) :: nu_tilde, nu, Chi
    
    call assert(size(values)==pnt_set%np(),BOUNDS_CHECK)
    
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call num_nu_tilde%get_values(elem,pnt_set,time,nu_tilde)

    Chi = nu_tilde / nu
    values = (Chi**3) / (Chi**3 + C_nu1**3)
  end subroutine eval_f_nu1
  
  subroutine eval_f_nu2(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    real(dp), dimension(size(values)) :: nu_tilde, nu, f_nu1, Chi
    
    call assert(size(values)==pnt_set%np(),BOUNDS_CHECK)
    
    call nu_molec%get_values(elem,pnt_set,time,nu)
    call eval_f_nu1(elem,pnt_set,time,f_nu1)
    call num_nu_tilde%get_values(elem,pnt_set,time,nu_tilde)

    Chi = nu_tilde / nu
    values = 1.0_dp - Chi / (1.0_dp + f_nu1 * Chi)
  end subroutine eval_f_nu2
  
  subroutine eval_f_w(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    real(dp), dimension(size(values)) :: g_vals, r_vals
    
    call assert(size(values)==pnt_set%np(),BOUNDS_CHECK)
    
    call eval_r(elem,pnt_set,time,r_vals)
    
    g_vals = r_vals + C_w2*(r_vals**6 - r_vals)
    values = g_vals * ((1.0_dp + C_w3**6) / (g_vals**6 + C_w3**6))**(1.0_dp/6.0_dp)
  end subroutine eval_f_w
  
  subroutine eval_r(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    real(dp), dimension(size(values)) :: nu_tilde, r_star, d, S_tilde
    real(dp) :: r_max = 10._dp
    integer :: p, no_points
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call eval_S_tilde(elem,pnt_set,time,S_tilde)
    call num_nu_tilde%get_values(elem,pnt_set,time,nu_tilde)
    call wall_dist_func%get_values(elem,pnt_set,time,d)
    
    do p = 1, no_points
      r_star(p) = nu_tilde(p) / (S_tilde(p)*(Karman*d(p))**2)
      if (r_star(p) < 0.0_dp) then
        values(p) = r_max
      else
        values(p) = min(r_star(p),r_max)
      endif
    enddo
  end subroutine eval_r
  
  subroutine eval_S_tilde(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    real(dp), dimension(size(values)) :: nu_tilde, d_vals, f_nu2, S, S_bar
    integer ::  p, no_points
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call wall_dist_func%get_values(elem,pnt_set,time,d_vals)
    call num_nu_tilde%get_values(elem,pnt_set,time,nu_tilde)
    call eval_f_nu2(elem,pnt_set,time,f_nu2)
    call eval_S(elem,pnt_set,time,S)
    
    S_bar = nu_tilde * f_nu2 / ((Karman * d_vals)**2)
    do p = 1, no_points
      if (S_bar(p) >= -C_nu2 * S(p)) then
        values(p) = S(p) + S_bar(p)
      else
        values(p) = S(p) + (S(p) * (C_nu2**2 * S(p) + C_nu3 * S_bar(p))) / ((C_nu3 - 2.0_dp*C_nu2)*S(p) - S_bar(p))
      endif
    enddo
  end subroutine eval_S_tilde
  
  subroutine eval_S(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)    
    real(dp) :: curl_vec(3, size(values))
    integer :: p, no_points
    
    no_points = assert_eq(size(values),pnt_set%np(),BOUNDS_CHECK)
    
    call vorticity_func%get_values(elem,pnt_set,time,curl_vec)
    do p = 1, no_points
      values(p) = sum(abs(curl_vec(:,p)))
    enddo
  end subroutine eval_S
  
  subroutine eval_gn(elem,pnt_set,time,values)
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    real(dp), dimension(size(values)) :: nu_tilde, nu, Chi
    
    call assert(size(values)==pnt_set%np(),BOUNDS_CHECK)
    
    call num_nu_tilde%get_values(elem,pnt_set,time,nu_tilde)
    call nu_molec%get_values(elem,pnt_set,time,nu)
    
    Chi = nu_tilde / nu
    values = 1.0_dp - (1000.0_dp*Chi**2/(1.0_dp + Chi**2))
  end subroutine eval_gn
end submodule rans_mod__SA
