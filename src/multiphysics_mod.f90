module multiphysics_mod
  use array_opers_mod, only: add_weighted_tensprod
  use assertions, only: assert, assert_eq
  use code_const, only: EQUAL, FINER, COARSER
  use dof_handler, only: dof_handler_type, init_dof_handler, get_dof, init_dof_handler_from_mesh_state
  use exceptions, only: error
  use f90_kind, only: dp
  use galerkin_projection, only: galerkin_proj
  use io_basics, only: INT_FMT, REAL_FMT
  use lapack_interface_mod, only: solve_SPD_linsys_keep_matrix
  use local_elem, only: get_quadrature
  use local_global_mappings, only: global_dg_functions, elem_lcor2gcor, elem_global_to_local_coord
  use local_integrals, only: int_fi_fj_on_elem
  use math_const, only: FROM_CM3_TO_M3, FROM_CM2_TO_M2
  use mesh_objects, only: elem_id, elem_list
  use mesh_type, only: fem_mesh, mesh_state, test_if_ids_are_in_same_tree, write_mesh_state, retrieve_mesh_state
  use mpi_wrappers_mod, only: bcast_wrap
  use numerical_solutions, only: get_dof_handler, maxorder_for, velocity, &
                                 solve_for_energy, matprop_set, get_solvec, predict_new_coeff
  use petsc_mod, only: I_am_master_process, MASTER_ID
  use petsc_vec_mod, only: petsc_vec_type
  use qnty_handler_mod, only: findloc_qnty
  use rans_mod, only: rans_model
  use run_data, only: use_rans_model, avg_T_in_ignored_elems, prec_conv_on_CFD_mesh, extrapolate_T_for_RT_calc
  use scal_func_mod, only: scal_func_type
  use solvec_as_scalfunc_mod, only: solvec_as_scalfunc
  use solvec_mod, only: solvec_t
  use vec_func_mod, only: vec_func_type
  implicit none
  
  private
  public :: init_fission_power_for_CFD_calc
  public :: get_fission_src_func
  public :: output_CFD_quantities_for_RT_calc
  public :: init_and_write_CFD_mesh_state
  
  type(solvec_t), protected :: fiss_solvec
  
  type(mesh_state) :: CFD_mesh_state
  
contains
  subroutine get_fission_src_func(fission_src)
    class(scal_func_type), allocatable, intent(out) :: fission_src
    
    call solvec_as_scalfunc(fiss_solvec,fission_src)
  end subroutine get_fission_src_func
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine init_fission_power_for_CFD_calc(CFD_mesh,RT_mesh_state)
    type(fem_mesh), intent(in) :: CFD_mesh
    type(mesh_state), intent(in) :: RT_mesh_state
    
    integer, parameter :: UNIT_NO_RT = 302
    integer :: RT_fiss_order, RT_fiss_length, ios
    real(dp), allocatable :: RT_fiss_Fcoeff(:), CFD_fiss_Fcoeff(:)
    type(dof_handler_type) :: RT_handler
    character(200) :: msg
    
    if (I_am_master_process()) write(*,'(a)',advance='no') 'Projecting RT fields useful for CFD code... '
    
    ! 1) create a solvec for the fission source on the CFD_mesh
    call fiss_solvec%init_to_zero('fission_src', CFD_mesh, ncoeffs=1, no_dirs=1, &
                                  orders=spread(maxorder_for('vol_enthalpy'),1,CFD_mesh%no_active_elem), &
                                  only_fluid_mesh=.false.)
    
    if (I_am_master_process()) then
      ! 2) init handler and read RT_fission source coeffs from file
      open(UNIT_NO_RT,iostat=ios,form='unformatted',status='old',iomsg=msg); call assert(ios == 0,trim(msg))
        
        read(UNIT_NO_RT,iostat=ios,iomsg=msg) RT_fiss_order; call assert(ios == 0,trim(msg))
        call assert(RT_fiss_order == maxorder_for('vol_enthalpy'),'Fission src order /= enthalpy order')
        
        call init_dof_handler_from_mesh_state(CFD_mesh,RT_mesh_state,1,1,RT_fiss_order,'total_mesh',RT_handler)
        
        read(UNIT_NO_RT,iostat=ios,iomsg=msg) RT_fiss_length; call assert(ios == 0,trim(msg))
        call assert(RT_fiss_length == RT_handler%no_dof_1grp,'in fission src file I/O, incompatible lenghts')
        
        allocate(RT_fiss_Fcoeff(RT_fiss_length),source=0.0_dp)
        read(UNIT_NO_RT,iostat=ios,iomsg=msg) RT_fiss_Fcoeff; call assert(ios == 0,trim(msg))
        
      close(UNIT_NO_RT,status='keep',iostat=ios,iomsg=msg); call assert(ios == 0,trim(msg))
    
      ! 3) project the fission src onto the CFD mesh
      call proj_RT_qnty_on_mesh_used_by_CFD(CFD_mesh,RT_mesh_state,RT_handler,RT_fiss_Fcoeff,fiss_solvec%handler,CFD_fiss_Fcoeff)
    endif
    
    ! 4) Broadcast projected fission source coeffs
    if (.not. I_am_master_process()) allocate(CFD_fiss_Fcoeff(fiss_solvec%handler%no_dof_1grp))
    call bcast_wrap(CFD_fiss_Fcoeff,MASTER_ID)
    
    ! 5) Fix units and finalize fiss_solvec
    if (CFD_mesh%dimen == 2) then
      CFD_fiss_Fcoeff = CFD_fiss_Fcoeff / FROM_CM2_TO_M2
    elseif (CFD_mesh%dimen == 3) then
      CFD_fiss_Fcoeff = CFD_fiss_Fcoeff / FROM_CM3_TO_M3
    else
      call error('init_RT_quantities_for_CFD_calc: unsupported mesh dimension.')
    endif
    call fiss_solvec%petsc_coeffs%copy_from_global_fort_vec(CFD_fiss_Fcoeff)
    call fiss_solvec%update_ghost_values()
    
    if (I_am_master_process()) write(*,'(a)') 'done'
  end subroutine init_fission_power_for_CFD_calc
  
  
  subroutine proj_RT_qnty_on_mesh_used_by_CFD(mesh_CFD,mesh_state_RT,handler_RT,solution_RT,handler_CFD,solution_CFD)
    type(fem_mesh), intent(in) :: mesh_CFD
    type(mesh_state), intent(in) :: mesh_state_RT
    type(dof_handler_type), intent(in) :: handler_RT
    real(dp), intent(in) :: solution_RT(:)
    type(dof_handler_type), intent(in) :: handler_CFD
    real(dp), allocatable, intent(out) :: solution_CFD(:)
    
    type(elem_id) :: id_RT, test_id_RT, id_CFD, test_id_CFD
    type(elem_list), pointer :: elem_CFD, elem_RT
    integer :: elem_no_CFD, test_elem_no_CFD, elem_no_RT, test_elem_no_RT
    integer, allocatable :: dofs_RT(:,:), dofs_CFD(:,:)
    integer :: no_dirs, dir, nnod, active_elem_no_CFD
    integer :: order, qp, nqp, id_CFD_is
    logical :: same_tree_space, only_fluid, skip_elem
    real(dp) :: gcor(mesh_CFD%dimen), sol_RT_qp
    real(dp), allocatable :: lcors_CFD(:,:), lcors_RT(:,:), fun_dg_CFD(:,:), fun_dg_RT(:,:)
    real(dp), allocatable :: mass_matrix(:,:), det_jac(:), quot(:), rhs(:)
    
    call assert(size(solution_RT)==handler_RT%no_dof_1grp,'size(solution_RT) /= handler_RT%no_dof_1grp')
    no_dirs = assert_eq(handler_CFD%no_dirs,handler_RT%no_dirs)
    allocate(solution_CFD(handler_CFD%no_dof_1grp),source=0.0_dp)
    only_fluid = handler_CFD%only_fluid_mesh
    
    ! We have to loop over the total CFD and RT meshes, skipping the elements to be ignored.
    ! start a pointer to the first spatial element in the RT mesh
    elem_no_RT = 1
    do elem_no_CFD = 1, mesh_CFD%tot_no_elem
      id_CFD = mesh_CFD%tot_elem_list(elem_no_CFD)
      elem_CFD => mesh_CFD%get_elem_ptr(id_CFD)
      
      ! loop as many times as the CFD element fits into the old without changing elem_no_RT
      indef_space_loop : do
        id_RT = mesh_state_RT%tot_elem_list(elem_no_RT)
        elem_RT => mesh_CFD%get_elem_ptr(id_RT)
        
        call test_if_ids_are_in_same_tree(id_CFD,id_RT,same_tree_space,id_CFD_is)
        if (same_tree_space) then
          skip_elem = assert_eq(elem_CFD%is_to_be_ignored(),elem_RT%is_to_be_ignored()) .or. &
                      (.not. assert_eq(elem_CFD%is_fluid(),elem_RT%is_fluid()) .and. only_fluid)
          if (.not. skip_elem) then
            active_elem_no_CFD = elem_CFD%get_active_elem_no(only_fluid)
            order = assert_eq(handler_CFD%order(active_elem_no_CFD),handler_RT%order(elem_no_RT)) !unequal order is now allowed
            call get_dof(handler_CFD,1,active_elem_no_CFD,dofs_CFD)
            call get_dof(handler_RT,1,elem_no_RT,dofs_RT)
            nnod = assert_eq(size(dofs_CFD,1),size(dofs_RT,1))
            
            if (id_CFD_is == EQUAL) then
              do dir = 1, no_dirs
                solution_CFD(dofs_CFD(:,dir)) = solution_CFD(dofs_CFD(:,dir)) + solution_RT(dofs_RT(:,dir))
              enddo
            endif
            
            if (id_CFD_is == FINER) then
              ! Compute quadrature on CFD mesh and dg functions on both meshes
              call get_quadrature(elem_CFD%eltype,2*order,lcors_CFD,quot)
              call global_dg_functions(elem_CFD,order,lcors_CFD,func=fun_dg_CFD,det_jac=det_jac)
              quot = quot * abs(det_jac)
              nqp = assert_eq(size(fun_dg_CFD,2),size(lcors_CFD,2),size(quot))
              allocate(lcors_RT,mold=lcors_CFD)
              do qp = 1, nqp
                call elem_lcor2gcor(elem_CFD,lcors_CFD(:,qp),gcor)
                call elem_global_to_local_coord(elem_RT,size(gcor),gcor,lcors_RT(:,qp))
              enddo
              call global_dg_functions(elem_RT,order,lcors_RT,func=fun_dg_RT)
              nnod = assert_eq(size(dofs_CFD,1),size(fun_dg_RT,1),size(fun_dg_CFD,1))
              
              ! assemble mass matrix
              allocate(mass_matrix(nnod,nnod),source=0.0_dp)
              do qp = 1, nqp
                call add_weighted_tensprod(mass_matrix,fun_dg_CFD(:,qp),fun_dg_CFD(:,qp),quot(qp))
              enddo
              ! assemble rhs and solve the linear system
              allocate(rhs(nnod))
              do dir = 1, no_dirs
                rhs = 0.0_dp
                do qp = 1, nqp
                  sol_RT_qp = dot_product(solution_RT(dofs_RT(:,dir)),fun_dg_RT(:,qp))
                  rhs = rhs + fun_dg_CFD(:,qp)*quot(qp)*sol_RT_qp
                enddo
                call solve_SPD_linsys_keep_matrix(mass_matrix,rhs)
                solution_CFD(dofs_CFD(:,dir)) = solution_CFD(dofs_CFD(:,dir)) + rhs
              enddo
              deallocate(lcors_RT,mass_matrix,rhs)
            endif
            
            if (id_CFD_is == COARSER) then
              call error('RT mesh more refined than CFD one not supported yet.')
            endif
          endif
        else
          call error('Lost spatial overlap between CFD and RT meshes!')
        endif ! same_space condition
        
        ! Is the next elem_no_RT overlapping => cycle loop
        if (elem_no_RT < mesh_state_RT%tot_no_elem) then
          test_elem_no_RT = elem_no_RT + 1
          test_id_RT = mesh_state_RT%tot_elem_list(test_elem_no_RT)
          call test_if_ids_are_in_same_tree(id_CFD,test_id_RT,same_tree_space,id_CFD_is)
          if (same_tree_space) then
            elem_no_RT = elem_no_RT + 1
            cycle indef_space_loop
          endif
        endif
        
        ! Is the next elem_no_CFD overlapping => exit loop
        if (elem_no_CFD < mesh_CFD%tot_no_elem) then
          test_elem_no_CFD = elem_no_CFD + 1
          test_id_CFD = mesh_CFD%tot_elem_list(test_elem_no_CFD)
          call test_if_ids_are_in_same_tree(test_id_CFD,id_RT,same_tree_space,id_CFD_is)
          if (same_tree_space) then
            exit indef_space_loop
          endif
        endif
        
        ! Both must be increased => exit loop and move elem_no_RT
        elem_no_RT = elem_no_RT + 1
        exit indef_space_loop
        
      enddo indef_space_loop
    enddo ! elem_no_CFD loop
  end subroutine proj_RT_qnty_on_mesh_used_by_CFD
  
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine init_and_write_CFD_mesh_state(CFD_mesh)
    type(fem_mesh), intent(in) :: CFD_mesh
    
    integer, parameter :: UNIT_NO_MESH_STATE = 304
    
    call retrieve_mesh_state(CFD_mesh,CFD_mesh_state)
    if (I_am_master_process()) call write_mesh_state(CFD_mesh,UNIT_NO_MESH_STATE,for_RT_code=.true.)
  end subroutine init_and_write_CFD_mesh_state
  
  subroutine output_CFD_quantities_for_RT_calc(CFD_mesh,RT_mesh_state)
    ! We output: velocity and nu_t coefficients and the avg T on each RT element.
    ! For each quantity, we need to:
    !  1) get the function and the dof_handler
    !  2) derive the vectors of coefficients, with a Galerkin projection (none of them is a qnty we solve for)
    ! Then, only the master process will 
    !  3) (U and nu_t) initialize corresponding RT handler and project the coefficients on the RT mesh
    !  3) (T) initialize array and, for each RT element, calculate the avg T on corresponding portion of CFD mesh
    !  4) write everything to the output files for the RT code
    ! If prec_conv_on_CFD_mesh, then the velocity is projected using a mesh_state corresponding to the CFD mesh,
    ! as the RT code is going to use that mesh.
    type(fem_mesh), intent(in) :: CFD_mesh
    type(mesh_state), intent(in) :: RT_mesh_state
    
    character(*), parameter :: FILE_NO_FLOW = 'fort.303',   &
                               FILE_T_AVG = 'elem_T_average'
    integer :: unit_no_flow
    integer, parameter :: TIME_IDX = 2
    integer :: ios, unit_no_T, T_time_index
    character(200) :: msg
    type(dof_handler_type), pointer :: U_CFD_handler, T_CFD_handler
    type(dof_handler_type) :: nu_t_CFD_handler
    type(dof_handler_type) :: U_RT_handler, nu_t_RT_handler
    integer :: U_order, nu_t_order
    real(dp), allocatable :: U_RT_Fcoeff(:), U_CFD_Fcoeff(:), &
                             nu_t_RT_Fcoeff(:), nu_t_CFD_Fcoeff(:), &
                             T_CFD_Fcoeff(:), T_avg_RT(:)
    type(petsc_vec_type) :: U_CFD_Pcoeff, nu_t_CFD_Pcoeff, T_CFD_Pcoeff, &
                            seq_U_CFD_Pcoeff, seq_nu_t_CFD_Pcoeff, seq_T_CFD_Pcoeff
    integer :: elem_no
    logical :: solve_for_engy
    class(scal_func_type), pointer :: temperature
    character(:), allocatable :: engy_qnty
    type(solvec_t), pointer :: T_solvec
    
    if (I_am_master_process()) write(*,'(a)',advance='no') 'Projecting CFD fields useful for RT code... '
    
    call solve_for_energy(solve_for_engy,engy_qnty)
    if (solve_for_engy) then
      T_CFD_handler => get_dof_handler(engy_qnty)
      if (engy_qnty == 'temperature') then
        T_solvec => get_solvec(engy_qnty)
        if (extrapolate_T_for_RT_calc) call T_solvec%extrapolate_new_from_old_coeff()
        call T_solvec%petsc_coeffs%to_seq_vec_on_0th_process(seq_T_CFD_Pcoeff)
      else
        temperature => matprop_set%func_ptr('temperature')
        call T_CFD_Pcoeff%init(no_local_rows=T_CFD_handler%no_dof_in_partition())
        T_time_index = merge(0,TIME_IDX,extrapolate_T_for_RT_calc)
        call galerkin_proj(CFD_mesh,T_CFD_handler,temperature,T_time_index,discrete_proj=T_CFD_Pcoeff)
        call T_CFD_Pcoeff%to_seq_vec_on_0th_process(seq_T_CFD_Pcoeff)
      endif
      
      if (I_am_master_process()) then
        allocate(T_CFD_Fcoeff(T_CFD_handler%no_dof_1grp))
        call seq_T_CFD_Pcoeff%get_local_values(T_CFD_Fcoeff)
        call calculate_T_avg_on_RT_mesh(RT_mesh_state,CFD_mesh,T_CFD_handler,T_CFD_Fcoeff,T_avg_RT)
        
        open(newunit=unit_no_T,file=FILE_T_AVG,status='replace', &
             action='write',position='rewind',iostat=ios,iomsg=msg); call assert(ios == 0,trim(msg))
          
          do elem_no = 1, RT_mesh_state%tot_no_elem
            write(unit_no_T,"("//INT_FMT//","//REAL_FMT//")",iostat=ios,iomsg=msg) elem_no, T_avg_RT(elem_no)
            call assert(ios == 0,trim(msg))
          enddo
        
        close(unit_no_T,status='keep',iostat=ios,iomsg=msg); call assert(ios == 0,trim(msg))
      endif
    endif
    
    if (findloc_qnty('mass_flux') /= 0) then
      U_CFD_handler => get_dof_handler('mass_flux')
      call U_CFD_Pcoeff%init(no_local_rows=U_CFD_handler%no_dof_in_partition())
      call galerkin_proj(CFD_mesh,U_CFD_handler,velocity,TIME_IDX,discrete_proj=U_CFD_Pcoeff)
      call U_CFD_Pcoeff%to_seq_vec_on_0th_process(seq_U_CFD_Pcoeff)
      if (use_rans_model) then
        nu_t_order = maxorder_for('mass_flux') ! Use the highest available polynomial order
        call init_dof_handler(nu_t_CFD_handler,CFD_mesh,1,1,nu_t_order,.true.)
        call nu_t_CFD_Pcoeff%init(no_local_rows=nu_t_CFD_handler%no_dof_in_partition())
        call galerkin_proj(CFD_mesh,nu_t_CFD_handler,rans_model%kinem_visc,TIME_IDX,discrete_proj=nu_t_CFD_Pcoeff)
        call nu_t_CFD_Pcoeff%to_seq_vec_on_0th_process(seq_nu_t_CFD_Pcoeff)
      endif
      
      if (I_am_master_process()) then
        U_order = maxorder_for('mass_flux')
        allocate(U_CFD_Fcoeff(U_CFD_handler%no_dof_1grp))
        call seq_U_CFD_Pcoeff%get_local_values(U_CFD_Fcoeff)
        if (prec_conv_on_CFD_mesh) then
          ! Use the CFD mesh state instead of the RT one
          call init_dof_handler_from_mesh_state(CFD_mesh,CFD_mesh_state,1,U_CFD_handler%no_dirs,U_order,'total_mesh',U_RT_handler)
          call proj_CFD_qnty_on_mesh_used_by_RT(CFD_mesh_state,CFD_mesh,U_CFD_handler,U_CFD_Fcoeff,U_RT_handler,U_RT_Fcoeff)
        else
          call init_dof_handler_from_mesh_state(CFD_mesh,RT_mesh_state,1,U_CFD_handler%no_dirs,U_order,'total_mesh',U_RT_handler)
          call proj_CFD_qnty_on_mesh_used_by_RT(RT_mesh_state,CFD_mesh,U_CFD_handler,U_CFD_Fcoeff,U_RT_handler,U_RT_Fcoeff)
        endif
        if (use_rans_model) then
          call init_dof_handler_from_mesh_state(CFD_mesh,RT_mesh_state,1,1,nu_t_order,'total_mesh',nu_t_RT_handler)
          allocate(nu_t_CFD_Fcoeff(nu_t_CFD_handler%no_dof_1grp))
          call seq_nu_t_CFD_Pcoeff%get_local_values(nu_t_CFD_Fcoeff)
          call proj_CFD_qnty_on_mesh_used_by_RT(  &
              RT_mesh_state, CFD_mesh,            &
              nu_t_CFD_handler, nu_t_CFD_Fcoeff,  &
              nu_t_RT_handler, nu_t_RT_Fcoeff     &
          )
        endif
        
        open(                                                                                             &
            file=FILE_NO_FLOW, newunit=unit_no_flow,                                                      &
            status='replace', access='sequential', form='unformatted',action='write', position='rewind',  &
            iostat=ios ,iomsg=msg                                                                         &
        )
        call assert(ios == 0,trim(msg))
        
          write(unit_no_flow,iostat=ios,iomsg=msg) U_order; call assert(ios == 0,trim(msg))
          write(unit_no_flow,iostat=ios,iomsg=msg) size(U_RT_Fcoeff); call assert(ios == 0,trim(msg))
          write(unit_no_flow,iostat=ios,iomsg=msg) U_RT_Fcoeff; call assert(ios == 0,trim(msg))
          
          write(unit_no_flow,iostat=ios,iomsg=msg) use_rans_model; call assert(ios == 0,trim(msg))
          
          if (use_rans_model) then
            write(unit_no_flow,iostat=ios,iomsg=msg) nu_t_order; call assert(ios == 0,trim(msg))
            write(unit_no_flow,iostat=ios,iomsg=msg) size(nu_t_RT_Fcoeff); call assert(ios == 0,trim(msg))
            write(unit_no_flow,iostat=ios,iomsg=msg) nu_t_RT_Fcoeff; call assert(ios == 0,trim(msg))
          endif
          
        close(unit_no_flow,status='keep',iostat=ios,iomsg=msg); call assert(ios == 0,trim(msg))
      endif
    endif
    
    if (I_am_master_process()) write(*,'(a)') 'done'
  end subroutine output_CFD_quantities_for_RT_calc
  
  subroutine proj_CFD_qnty_on_mesh_used_by_RT(mesh_state_RT,mesh_CFD,handler_CFD,solution_CFD,handler_RT,solution_RT)
    type(mesh_state), intent(in) :: mesh_state_RT
    type(fem_mesh), intent(in) :: mesh_CFD
    type(dof_handler_type), intent(in) :: handler_CFD
    real(dp), intent(in) :: solution_CFD(:)
    type(dof_handler_type), intent(in) :: handler_RT
    real(dp), allocatable, intent(out) :: solution_RT(:)
    
    type(elem_id) :: id_RT, test_id_RT, id_CFD, test_id_CFD
    type(elem_list), pointer :: elem_CFD, elem_RT
    integer :: elem_no_CFD, test_elem_no_CFD, elem_no_RT, test_elem_no_RT
    integer, allocatable :: dofs_RT(:,:), dofs_CFD(:,:)
    integer :: no_dirs, dir, nnod, active_elem_no_CFD
    integer :: order, qp, nqp, id_RT_is
    logical :: same_tree_space, only_fluid, skip_elem
    real(dp) :: gcor(mesh_CFD%dimen), sol_CFD_qp
    real(dp), allocatable :: lcors_CFD(:,:), lcors_RT(:,:), fun_dg_CFD(:,:), fun_dg_RT(:,:)
    real(dp), allocatable :: mass_matrix(:,:), det_jac(:), quot(:), rhs(:)
    
    call assert(size(solution_CFD)==handler_CFD%no_dof_1grp,'size(solution_CFD) /= handler_CFD%no_dof_1grp')
    no_dirs = assert_eq(handler_RT%no_dirs,handler_CFD%no_dirs)
    allocate(solution_RT(handler_RT%no_dof_1grp),source=0.0_dp)
    only_fluid = handler_CFD%only_fluid_mesh
    
    ! We have to loop over the total CFD and RT meshes, skipping the elements to be ignored.
    ! start a pointer to the first spatial element in the CFD mesh
    elem_no_CFD = 1
    do elem_no_RT = 1, mesh_state_RT%tot_no_elem
      id_RT = mesh_state_RT%tot_elem_list(elem_no_RT)
      elem_RT => mesh_CFD%get_elem_ptr(id_RT)
      
      ! loop as many times as the RT element fits into the old without changing elem_no_CFD
      indef_space_loop : do
        id_CFD = mesh_CFD%tot_elem_list(elem_no_CFD)
        elem_CFD => mesh_CFD%get_elem_ptr(id_CFD)
        
        call test_if_ids_are_in_same_tree(id_RT,id_CFD,same_tree_space,id_RT_is)
        if (same_tree_space) then
          skip_elem = assert_eq(elem_CFD%is_to_be_ignored(),elem_RT%is_to_be_ignored()) .or. &
                      (.not. assert_eq(elem_CFD%is_fluid(),elem_RT%is_fluid()) .and. only_fluid)
          if (.not. skip_elem) then
            active_elem_no_CFD = elem_CFD%get_active_elem_no(only_fluid)
            order = assert_eq(handler_RT%order(elem_no_RT),handler_CFD%order(active_elem_no_CFD)) !unequal order is now allowed
            call get_dof(handler_RT,1,elem_no_RT,dofs_RT)
            call get_dof(handler_CFD,1,active_elem_no_CFD,dofs_CFD)
            nnod = assert_eq(size(dofs_CFD,1),size(dofs_RT,1))
            
            if (id_RT_is == EQUAL) then
              do dir = 1, no_dirs
                solution_RT(dofs_RT(:,dir)) = solution_RT(dofs_RT(:,dir)) + solution_CFD(dofs_CFD(:,dir))
              enddo
            endif
            
            if (id_RT_is == FINER) then
              call error('RT mesh more refined than CFD one not supported yet.')
            endif
            
            if (id_RT_is == COARSER) then
              ! Compute quadrature on CFD mesh and dg functions on both meshes
              call get_quadrature(elem_CFD%eltype,2*order,lcors_CFD,quot)
              call global_dg_functions(elem_CFD,order,lcors_CFD,func=fun_dg_CFD,det_jac=det_jac)
              quot = quot * abs(det_jac)
              nqp = assert_eq(size(fun_dg_CFD,2),size(lcors_CFD,2),size(quot))
              allocate(lcors_RT,mold=lcors_CFD)
              do qp = 1, nqp
                call elem_lcor2gcor(elem_CFD,lcors_CFD(:,qp),gcor)
                call elem_global_to_local_coord(elem_RT,size(gcor),gcor,lcors_RT(:,qp))
              enddo
              call global_dg_functions(elem_RT,order,lcors_RT,func=fun_dg_RT)
              
              ! Compute mass matrix on RT mesh
              call int_fi_fj_on_elem(elem_RT,order,mass_matrix)
              nnod = assert_eq(size(dofs_CFD,1),size(fun_dg_RT,1),size(fun_dg_CFD,1),size(mass_matrix,1),size(mass_matrix,2))
              ! assemble rhs and solve the linear system
              allocate(rhs(nnod))
              do dir = 1, no_dirs
                rhs = 0.0_dp
                do qp = 1, nqp
                  sol_CFD_qp = dot_product(solution_CFD(dofs_CFD(:,dir)),fun_dg_CFD(:,qp))
                  rhs = rhs + fun_dg_RT(:,qp)*quot(qp)*sol_CFD_qp
                enddo
                call solve_SPD_linsys_keep_matrix(mass_matrix,rhs)
                solution_RT(dofs_RT(:,dir)) = solution_RT(dofs_RT(:,dir)) + rhs
              enddo
              deallocate(lcors_RT,rhs)
            endif
          endif
        else
          call error('Lost spatial overlap between CFD and RT meshes!')
        endif ! same_space condition
        
        ! Is the next elem_no_CFD overlapping => cycle loop
        if (elem_no_CFD < mesh_CFD%tot_no_elem) then
          test_elem_no_CFD = elem_no_CFD + 1
          test_id_CFD = mesh_CFD%tot_elem_list(test_elem_no_CFD)
          call test_if_ids_are_in_same_tree(id_RT,test_id_CFD,same_tree_space,id_RT_is)
          if (same_tree_space) then
            elem_no_CFD = elem_no_CFD + 1
            cycle indef_space_loop
          endif
        endif
        
        ! Is the next elem_no_RT overlapping => exit loop
        if (elem_no_RT < mesh_state_RT%tot_no_elem) then
          test_elem_no_RT = elem_no_RT + 1
          test_id_RT = mesh_state_RT%tot_elem_list(test_elem_no_RT)
          call test_if_ids_are_in_same_tree(test_id_RT,id_CFD,same_tree_space,id_RT_is)
          if (same_tree_space) then
            exit indef_space_loop
          endif
        endif
        
        ! Both must be increased => exit loop and move elem_no_CFD
        elem_no_CFD = elem_no_CFD + 1
        exit indef_space_loop
        
      enddo indef_space_loop
    enddo ! elem_no_RT loop
  end subroutine proj_CFD_qnty_on_mesh_used_by_RT
  
  subroutine calculate_T_avg_on_RT_mesh(mesh_state_RT,mesh_CFD,T_CFD_handler,T_CFD_Fcoeff,T_avg_RT)
    ! Similar to the CFD --> projection procedure, but we don't need to compute the T coeffs on the RT mesh,
    ! we just have to compute an average T value for each RT element.
    ! To do this, we just have to compute the integral of the temperature in the correspondent portion of RT mesh.
    type(mesh_state), intent(in) :: mesh_state_RT
    type(fem_mesh), intent(in) :: mesh_CFD
    type(dof_handler_type), intent(in) :: T_CFD_handler
    real(dp), intent(in) :: T_CFD_Fcoeff(:)
    real(dp), allocatable, intent(out) :: T_avg_RT(:)
    
    type(elem_id) :: id_RT, test_id_RT
    type(elem_id) :: id_CFD, test_id_CFD
    type(elem_list), pointer :: elem_CFD, elem_RT
    integer :: elem_no_CFD, test_elem_no_CFD
    integer :: elem_no_RT, test_elem_no_RT
    logical :: same_tree_space
    integer :: order, qp, nqp, id_RT_is
    integer, allocatable :: dofs_CFD(:)
    real(dp), allocatable :: lcors_CFD(:,:), fun_dg_CFD(:,:), det_jac(:), quot(:)
    real(dp), allocatable :: T_int_RT(:), elem_vol_RT(:)
    
    call assert(size(T_CFD_Fcoeff)==T_CFD_handler%no_dof_1grp,'size(T_CFD_Fcoeff) /= T_CFD_handler%no_dof_1grp')
    allocate(T_int_RT(mesh_state_RT%tot_no_elem),source=0.0_dp)
    allocate(elem_vol_RT(mesh_state_RT%tot_no_elem),source=0.0_dp)
    
    ! We have to loop over the total CFD and RT meshes, skipping the elements to be ignored.
    ! start a pointer to the first spatial element in the CFD mesh
    elem_no_CFD = 1
    do elem_no_RT = 1, mesh_state_RT%tot_no_elem
      id_RT = mesh_state_RT%tot_elem_list(elem_no_RT)
      elem_RT => mesh_CFD%get_elem_ptr(id_RT)
      
      ! loop as many times as the RT element fits into the old without changing elem_no_CFD
      indef_space_loop : do
        id_CFD = mesh_CFD%tot_elem_list(elem_no_CFD)
        elem_CFD => mesh_CFD%get_elem_ptr(id_CFD)
        
        call test_if_ids_are_in_same_tree(id_RT,id_CFD,same_tree_space,id_RT_is)
        if (same_tree_space) then
          if (.not. assert_eq(elem_RT%is_to_be_ignored(),elem_CFD%is_to_be_ignored())) then
            order = T_CFD_handler%order(elem_CFD%active_elem_no)
            allocate(dofs_CFD,source=T_CFD_handler%flat_dof(1,elem_CFD%active_elem_no))
            
            if (id_RT_is == EQUAL .or. id_RT_is == COARSER) then
              ! Compute quadrature on CFD mesh
              call get_quadrature(elem_CFD%eltype,2*order,lcors_CFD,quot)
              call global_dg_functions(elem_CFD,order,lcors_CFD,func=fun_dg_CFD,det_jac=det_jac)
              call assert(size(dofs_CFD,1)==size(fun_dg_CFD,1),'size(dofs_CFD,1)/=size(fun_dg_CFD,1)')
              quot = quot * abs(det_jac)
              nqp = assert_eq(size(fun_dg_CFD,2),size(lcors_CFD,2),size(quot))
              do qp = 1, nqp
                T_int_RT(elem_no_RT) = T_int_RT(elem_no_RT) + quot(qp)*dot_product(T_CFD_Fcoeff(dofs_CFD),fun_dg_CFD(:,qp))
              enddo
              elem_vol_RT(elem_no_RT) = elem_vol_RT(elem_no_RT) + sum(quot)
            endif
            
            if (id_RT_is == FINER) then
              call error('RT mesh more refined than CFD one not supported yet.')
            endif
            
            deallocate(dofs_CFD)
          endif
        else
          call error('Lost spatial overlap between CFD and RT meshes!')
        endif ! same_space condition
        
        ! Is the next elem_no_CFD overlapping => cycle loop
        if (elem_no_CFD < mesh_CFD%tot_no_elem) then
          test_elem_no_CFD = elem_no_CFD + 1
          test_id_CFD = mesh_CFD%tot_elem_list(test_elem_no_CFD)
          call test_if_ids_are_in_same_tree(id_RT,test_id_CFD,same_tree_space,id_RT_is)
          if (same_tree_space) then
            elem_no_CFD = elem_no_CFD + 1
            cycle indef_space_loop
          endif
        endif
        
        ! Is the next elem_no_RT overlapping => exit loop
        if (elem_no_RT < mesh_state_RT%tot_no_elem) then
          test_elem_no_RT = elem_no_RT + 1
          test_id_RT = mesh_state_RT%tot_elem_list(test_elem_no_RT)
          call test_if_ids_are_in_same_tree(test_id_RT,id_CFD,same_tree_space,id_RT_is)
          if (same_tree_space) then
            exit indef_space_loop
          endif
        endif
        
        ! Both must be increased => exit loop and move elem_no_CFD
        elem_no_CFD = elem_no_CFD + 1
        exit indef_space_loop
        
      enddo indef_space_loop
    enddo ! elem_no_RT loop
    
    allocate(T_avg_RT(mesh_state_RT%tot_no_elem))
    do elem_no_RT = 1, mesh_state_RT%tot_no_elem
      id_RT = mesh_state_RT%tot_elem_list(elem_no_RT)
      elem_RT => mesh_CFD%get_elem_ptr(id_RT)
      if (elem_RT%is_to_be_ignored()) then
        T_avg_RT(elem_no_RT) = avg_T_in_ignored_elems
      else
        T_avg_RT(elem_no_RT) = T_int_RT(elem_no_RT) / elem_vol_RT(elem_no_RT)
      endif
    enddo
  end subroutine calculate_T_avg_on_RT_mesh
end module multiphysics_mod
