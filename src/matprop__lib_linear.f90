submodule(matprop_mod) matprop__lib_linear
  ! linearization of properties, to be specified by the user
  use f90_kind, only: dp
  use code_const, only: UNINITIALIZED_REAL, UNINITIALIZED_CHAR
  implicit none

  real(dp) :: T_0 = UNINITIALIZED_REAL, h_0 = UNINITIALIZED_REAL, rho_0 = UNINITIALIZED_REAL, & ! reference values
      cp = UNINITIALIZED_REAL, & ! specific heat capacity
      beta = UNINITIALIZED_REAL, & ! thermal expansion coefficient
      mu = UNINITIALIZED_REAL, & ! dynamic viscosity
      k = UNINITIALIZED_REAL ! thermal conductivity
  
contains
  module subroutine get_linear_matprop(unit_number,matprop_set)
    use func_link_mod, only: func_link_t
    use func_set_expansion_mod, only: func_set_expansion_t
    use functions_lib, only: const_func
    use string_manipulations, only: char2bool
    
    integer ,intent(in) :: unit_number
    type(func_set_expansion_t), intent(out) :: matprop_set
    
    character(1000) :: constant_density
    real(dp) :: prandtl
    namelist /reference_values/ T_0, h_0, rho_0, cp, beta, mu, k, &
                                constant_density, prandtl
    integer :: ios
    character(200) :: iomsg
    type(func_link_t) :: link
    logical :: const_rho

    constant_density = UNINITIALIZED_CHAR
    prandtl = UNINITIALIZED_REAL
    read(unit_number, reference_values, pad='yes', iostat=ios, iomsg=iomsg)
    call assert(ios == 0,'could not read matprop reference point; iomsg=' // trim(iomsg))
    
    call assert([T_0, h_0, rho_0, cp, mu] /= UNINITIALIZED_REAL, 'matprops: specify T_0, h_0, rho_0, cp, mu')
    call assert(prandtl == UNINITIALIZED_REAL .neqv. k == UNINITIALIZED_REAL, 'matprops: specify either prandtl or k, not both')
    if (k == UNINITIALIZED_REAL) k = mu * cp / prandtl
    call assert(constant_density /= UNINITIALIZED_CHAR, 'matrops: specify `constant_density`')
    const_rho = char2bool(trim(constant_density))
    call assert(const_rho .eqv. beta == UNINITIALIZED_REAL, 'matprops: set `beta` iff `constant_density` is false')

    call link%init('temperature','spec_enthalpy',T2spec_h)
    call matprop_set%add_func_link(link)
    call link%init('spec_enthalpy','temperature',spec_h2T)
    call matprop_set%add_func_link(link)
    if (const_rho) then
      call matprop_set%add_known_function('density',const_func(rho_0))
      call matprop_set%add_known_function('deriv_rho_h',const_func(0.0_dp))
      call matprop_set%add_known_function('kinematic_viscosity',const_func(mu / rho_0))
      call matprop_set%add_known_function('thermal_diffusivity',const_func(k / (rho_0 * cp)))
      ! call matprop_set%add_known_function('thermal_expansion_coeff',const_func(0.0_dp))
    else
      call link%init('spec_enthalpy','density',spec_h2rho)
      call matprop_set%add_func_link(link)
      call link%init('spec_enthalpy','deriv_rho_h',spec_h2deriv_rho_h)
      call matprop_set%add_func_link(link)
      call link%init('spec_enthalpy','kinematic_viscosity',spec_h2nu)
      call matprop_set%add_func_link(link)
      call link%init('spec_enthalpy','thermal_diffusivity',spec_h2thermdiff)
      call matprop_set%add_func_link(link)
      call matprop_set%add_known_function('thermal_expansion_coeff',const_func(beta))
    endif
    call matprop_set%add_known_function('thermal_conductivity',const_func(k))
    call matprop_set%add_known_function('dynamic_viscosity',const_func(mu))
    call matprop_set%add_known_function('spec_heat_capacity',const_func(cp))
    call matprop_set%add_known_function('k_over_cp',const_func(k/cp))

    call matprop_set%finalize()
  end subroutine get_linear_matprop
  
  real(dp) function T2spec_h(T) result(spec_enthalpy)
    real(dp), intent(in) :: T
    
    ! call assert(T < T_max)

    spec_enthalpy = cp * (T-T_0) + h_0
  end function T2spec_h
  
  pure real(dp) function spec_h2T(spec_h) result(temperature)
    real(dp), intent(in) :: spec_h

    temperature = (spec_h - h_0) / cp + T_0
  end function spec_h2T

  pure real(dp) function spec_h2rho(spec_h) result(density)
    ! based on:
    !   rho - rho_0   =  (d/dT rho) (T - T_0)
    !   beta          =  - (d/dT rho) / rho
    real(dp), intent(in) :: spec_h

    real(dp) :: relative_expansion

    relative_expansion = beta * (spec_h2T(spec_h) - T_0)
    density = rho_0 / (1.0_dp + relative_expansion)
  end function spec_h2rho

  pure real(dp) function spec_h2deriv_rho_h(spec_h) result(deriv_rho_h)
    ! d/dh rho  =  (d/dT rho) * (d/dh T)  =  (d/dT rho) / (d/dT h)
    ! beta      =  - (d/dT rho) / rho
    real(dp), intent(in) :: spec_h

    deriv_rho_h = - spec_h2rho(spec_h) * beta / cp
  end function spec_h2deriv_rho_h

  real(dp) function spec_h2nu(spec_h) result(kinematic_viscosity)
    real(dp), intent(in) :: spec_h

    kinematic_viscosity = mu / spec_h2rho(spec_h)
  end function spec_h2nu

  real(dp) function spec_h2thermdiff(spec_h) result(thermal_diffusivity)
    real(dp), intent(in) :: spec_h

    thermal_diffusivity = k / (spec_h2rho(spec_h) * cp)
  end function spec_h2thermdiff
end submodule matprop__lib_linear

