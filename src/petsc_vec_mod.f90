module petsc_vec_mod
#include "petsc/finclude/petscvec.h"
  use petscvec, only: tVec
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use petsc_basic_types_mod, only: fort2petsc, petsc2fort
  use petsc_mod, only: idx_fort2petsc
  implicit none
  
  private
  public :: petsc_vec_type, drop_petsc_vec
  
  type :: petsc_vec_type
    ! The "petsc_vec" object is only made public for it to be used in other 
    ! PETSc-related modules. It should not be accessed elsewhere.
    type(tVec), public :: petsc_vec
    logical, public :: initialized = .false.
    logical, public :: assemble_elemvec_as_block = .false.
    
  contains
    procedure, non_overridable :: init
    procedure, non_overridable :: create_nest
    procedure, non_overridable :: duplicate
    procedure, non_overridable :: copy
    procedure, non_overridable :: set
    procedure, non_overridable :: zero_entries
    procedure, non_overridable :: perturb_randomly
    procedure, non_overridable, private :: set_random
    generic :: add => add_value, add_values
    procedure, non_overridable, private :: add_value
    procedure, non_overridable, private :: add_values
    generic :: add_values_blocked => add_values_blocked__one, add_values_blocked__many
    procedure, non_overridable, private :: add_values_blocked__one, add_values_blocked__many
    generic :: insert => insert_value, insert_values
    procedure, non_overridable, private :: insert_value
    procedure, non_overridable, private :: insert_values
    procedure, non_overridable :: insert_values_local
    procedure, non_overridable, private :: set_value
    procedure, non_overridable, private :: set_values
    procedure, non_overridable :: copy_from_local_fort_vec
    procedure, non_overridable :: copy_from_global_fort_vec
    procedure, non_overridable :: finalize_assembly
    procedure, non_overridable, private :: fortran_local_bounds
    procedure, non_overridable :: local_size
    procedure, non_overridable, private :: global_size
    procedure, non_overridable, private :: is_sequential
    procedure, non_overridable :: sum => vec_sum, norm_2, &
                                  max => vec_max, min => vec_min
    procedure, non_overridable :: normalize_by_2norm
    procedure, non_overridable :: get_Loc2Glob_mapping
    procedure, non_overridable :: get_local_values
    procedure, non_overridable :: get_values_including_ghosts
    procedure, non_overridable :: get_copy_of_subvec
    procedure, non_overridable :: update_ghost_values
    procedure, non_overridable :: to_seq_vec_on_0th_process
    procedure, non_overridable :: add_vec, subtract_vec
    procedure, non_overridable :: axpy
    procedure, non_overridable, private :: aypx, waxpy, axpby
    procedure, non_overridable :: mult_with_scal
    procedure, non_overridable, private :: pointwise_mult
    procedure, non_overridable :: pointwise_divide
    final :: drop_petsc_vec
  end type petsc_vec_type
  
contains
  subroutine init(this,no_local_rows,no_global_rows,ghost_idxs)
    use petscsys, only: PETSC_DECIDE
    use petscvec, only: VecCreate, VecSetType, VecSetSizes, VecMPISetGhost
    use petsc_mod, only: petsc_mpi_comm, petsc_initialized, no_petsc_procs
    
    class(petsc_vec_type), intent(out) :: this
    integer, optional, intent(in) :: no_local_rows
    integer, optional, intent(in) :: no_global_rows
    integer, optional, intent(in) :: ghost_idxs(:)
    
    integer, allocatable :: petsc_ghost_idxs(:)
    PetscErrorCode :: ierr
    
    call assert(petsc_initialized)
    call assert(present(no_local_rows) .or. present(no_global_rows), &
                'To init a PETSc Vec you must specify its local or global size')
    
    call VecCreate(petsc_mpi_comm,this%petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecCreate')
    this%initialized = .true.
    
    if (no_petsc_procs() == 1) then
      call VecSetType(this%petsc_vec,VECSEQ,ierr)
    else
      call VecSetType(this%petsc_vec,VECMPI,ierr)
    endif
    call assert(ierr == 0,'ierr /= 0 in VecSetType')
    
    if (present(no_local_rows)) then
      call VecSetSizes(this%petsc_vec,no_local_rows,PETSC_DECIDE,ierr)
    elseif (present(no_global_rows)) then
      call VecSetSizes(this%petsc_vec,PETSC_DECIDE,no_global_rows,ierr)
    endif
    call assert(ierr == 0,'ierr /= 0 in VecSetSizes')
    
    if (no_petsc_procs() > 1 .and. present(ghost_idxs)) then
      allocate(petsc_ghost_idxs(size(ghost_idxs)))
      call idx_fort2petsc(ghost_idxs,petsc_ghost_idxs)
      call VecMPISetGhost(this%petsc_vec,size(petsc_ghost_idxs),petsc_ghost_idxs,ierr)
      call assert(ierr == 0,'ierr /= 0 in VecMPISetGhost')
    endif
    
    call this%zero_entries()
  end subroutine init
  
  
  subroutine create_nest(this,subvecs)
    ! Creates a new vector containing several nested subvectors, each stored separately
    !
    use petscis, only: PETSC_NULL_IS
    use petsc_mod, only: petsc_initialized, petsc_mpi_comm
    use petsc_basic_types_mod, only: fort2petsc

    interface
      subroutine VecCreateNest(comm, nb, is, x, Y, ierr)
        use petscvec
        implicit none
        MPI_Comm, intent(in) :: comm
        PetscInt, intent(in) :: nb
        IS, intent(in) :: is(*)
        Vec, intent(in) :: X(*)
        Vec, intent(inout) :: Y
        PetscErrorCode, intent(out) :: ierr
      end subroutine
    end interface
    
    class(petsc_vec_type), intent(out) :: this
    class(petsc_vec_type), intent(in) :: subvecs(:)
    
    integer :: nb, i
    PetscErrorCode :: ierr
    
    call assert(petsc_initialized)
    
    nb = size(subvecs)
    ! Pass PETSC_NULL_IS to pack subvectors contiguously:
    call VecCreateNest(petsc_mpi_comm, nb, [PETSC_NULL_IS], [(subvecs(i)%petsc_vec, i = 1, nb)], this%petsc_vec,ierr)
    call assert(ierr==0,'ierr /= 0 in VecCreateNest')
    this%initialized = .true.
  end subroutine create_nest
  
  subroutine duplicate(this,new_vec)
    ! Creates a new vector of the same type as an existing vector.
    ! DOES NOT COPY the vector entries, but rather allocates storage
    ! for the new vector.
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecDuplicate.html
    use petscvec, only: VecDuplicate

    class(petsc_vec_type), intent(in) :: this
    type(petsc_vec_type), intent(out) :: new_vec
    
    PetscErrorCode :: ierr
    
    call VecDuplicate(this%petsc_vec,new_vec%petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecDuplicate')
    new_vec%initialized = .true.
  end subroutine duplicate
  
  subroutine copy(this,y)
    ! Copies a vector. 'y' <-- 'this'
    ! For default parallel PETSc vectors, both 'this' and 'y' must be
    ! distributed in the same manner; local copies are done.
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecCopy.html#VecCopy
    use petscvec, only: VecCopy

    class(petsc_vec_type), intent(in) :: this
    type(petsc_vec_type), intent(inout) :: y

    PetscErrorCode :: ierr

    call VecCopy(this%petsc_vec,y%petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecCopy')
  end subroutine copy

  subroutine set(this,alpha)
    ! Sets all components of a vector to a single scalar value.
    ! (Logically Collective on 'this')
    !
    ! You CANNOT call this after you have called VecSetValues() but before
    ! you call VecAssemblyBegin/End().
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecSet.html
    use petscvec, only: VecSet

    class(petsc_vec_type), intent(inout) :: this
    real(dp), intent(in) :: alpha

    PetscErrorCode :: ierr

    call VecSet(this%petsc_vec,fort2petsc(alpha),ierr)
    call assert(ierr == 0,'ierr /= 0 in VecSet')
  end subroutine set

  subroutine zero_entries(this)
    ! puts a 0.0 in each element of 'this'
    !
    ! The VecZeroEntries PETSc function does not need to exist since the exact
    ! functionality can be obtained with the VecSet function.
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecZeroEntries.html
    use petscvec, only: VecZeroEntries

    class(petsc_vec_type), intent(inout) :: this

    PetscErrorCode :: ierr

    call VecZeroEntries(this%petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecZeroEntries')
  end subroutine zero_entries
  
  
  subroutine perturb_randomly(this,alpha)
    ! Replaces each entry t of 'this' as
    !       r <-- Uniform(-alpha,alpha)
    !       t <-- (1+r) * t,
    class(petsc_vec_type), intent(inout) :: this
    real(dp), intent(in) :: alpha

    type(petsc_vec_type) :: work

    call this%duplicate(work)
    call work%set_random()
    call work%mult_with_scal(2*alpha)
    call work%pointwise_mult(work,this)
    call this%aypx(1-alpha,work)
  end subroutine perturb_randomly

  subroutine set_random(this)
    ! Sets all components to random numbers from Uniform(0,1).
    !
    ! Logically Collective on Vec
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecSetRandom.html#VecSetRandom
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Sys/PetscRandomCreate.html#PetscRandomCreate
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Sys/PetscRandomSetType.html#PetscRandomSetType
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Sys/PetscRandomDestroy.html#PetscRandomDestroy
    use petscvec, only: PetscRandomCreate, PetscRandomSetType, VecSetRandom, PetscRandomDestroy
    use petscsys, only: tPetscRandom

    use petsc_mod, only: petsc_mpi_comm
    use, intrinsic :: iso_c_binding, only: c_char, c_null_char

    class(petsc_vec_type), intent(inout) :: this

    PetscErrorCode :: ierr
    type(tPetscRandom) :: rctx
    ! We do not appear to have access to the usual defined constants of type
    ! 'PetscRandomType' from Fortran, so we hard-code the string:
    character(len=*,kind=c_char), parameter :: petsc_random_type = "rand"//c_null_char

    ! Create a context for generating random numbers, and initialize the generator:
    call PetscRandomCreate(petsc_mpi_comm,rctx,ierr)
    call assert(ierr == 0,'ierr /= 0 in PetscRandomCreate')

    ! Build a context for generating particular type of random numbers:
    call PetscRandomSetType(rctx,petsc_random_type,ierr)
    call assert(ierr == 0,'ierr /= 0 in PetscRandomSetType')

    ! Passing NULL for rctx should trigger Vec to create an internal random
    ! number context, but we don't know how to do that from Fortran.
    call VecSetRandom(this%petsc_vec,rctx,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecSetRandom')

    call PetscRandomDestroy(rctx,ierr)
    call assert(ierr == 0,'ierr /= 0 in PetscRandomDestroy')
  end subroutine set_random

  subroutine add_value(this,row,value)
    use petscvec, only: insert_mode => ADD_VALUES
    
    class(petsc_vec_type), intent(inout) :: this
    real(dp), intent(in) :: value
    integer, intent(in) :: row
    
    call this%set_value(row,value,insert_mode)
  end subroutine add_value
  
  subroutine add_values(this,rows,values)
    use petscvec, only: insert_mode => ADD_VALUES
    
    class(petsc_vec_type), intent(inout) :: this
    real(dp), intent(in) :: values(:)
    integer, intent(in) :: rows(:)
    
    call assert(size(values) == size(rows),'values and rows have incompatible sizes',BOUNDS_CHECK)
    
    call this%set_values(rows,values,insert_mode)
  end subroutine add_values
  
  subroutine add_values_blocked__one(this, ix, values)
    use petscvec, only: VecSetValuesBlocked, insert_mode => ADD_VALUES

    class(petsc_vec_type), intent(inout) :: this
    integer, intent(in) :: ix
    real(dp), intent(in) :: values(:)
    
    PetscInt :: petsc_ix
    PetscErrorCode :: ierr

    call idx_fort2petsc(ix, petsc_ix)
    call VecSetValuesBlocked(this%petsc_vec, 1, petsc_ix, values, insert_mode, ierr)
    call assert(ierr == 0,'ierr /= 0 in VecSetValuesBlocked')
  end subroutine

  subroutine add_values_blocked__many(this, ix, values)
    use petscvec, only: VecSetValuesBlocked, insert_mode => ADD_VALUES

    class(petsc_vec_type), intent(inout) :: this
    integer, intent(in) :: ix(:)
    real(dp), intent(in) :: values(:)
    
    PetscInt :: petsc_ix(size(ix))
    PetscErrorCode :: ierr

    call idx_fort2petsc(ix, petsc_ix)
    call VecSetValuesBlocked(this%petsc_vec, size(ix), petsc_ix, values, insert_mode, ierr)
    call assert(ierr == 0,'ierr /= 0 in VecSetValuesBlocked')
  end subroutine

  subroutine insert_value(this,row,value)
    use petscvec, only: insert_mode => INSERT_VALUES
    
    class(petsc_vec_type), intent(inout) :: this
    real(dp), intent(in) :: value
    integer, intent(in) :: row
    
    call this%set_value(row,value,insert_mode)
  end subroutine insert_value
  
  subroutine insert_values(this,rows,values)
    use petscvec, only: insert_mode => INSERT_VALUES
    
    class(petsc_vec_type), intent(inout) :: this
    integer, intent(in) :: rows(:)
    real(dp), intent(in) :: values(:)
    
    call assert(size(values) == size(rows),'values and rows have incompatible sizes',BOUNDS_CHECK)
    
    call this%set_values(rows,values,insert_mode)
  end subroutine insert_values
  
  subroutine set_value(this,row,value,insert_mode)
    interface
      subroutine VecSetValue(v, row, value, mode, ierr);
        use petscvec
        implicit none
        type(tVec), intent(inout) :: v
        PetscInt, intent(in) :: row
        PetscScalar, intent(in) :: value
        InsertMode, intent(in) :: mode
        integer, intent(out) :: ierr
      end subroutine
    end interface

    class(petsc_vec_type), intent(inout) :: this
    real(dp), intent(in) :: value
    integer, intent(in) :: row
    InsertMode, intent(in) :: insert_mode
    
    integer :: petsc_row
    PetscErrorCode :: ierr
    
    call idx_fort2petsc(row,petsc_row)
    call VecSetValue(this%petsc_vec, petsc_row, value, insert_mode, ierr)
    call assert(ierr == 0,'ierr /= 0 in VecSetValue')
  end subroutine set_value
  
  subroutine set_values(this,rows,values,insert_mode)
    use petscvec, only: VecSetValues

    class(petsc_vec_type), intent(inout) :: this
    real(dp), contiguous, intent(in) :: values(:)
    integer, intent(in) :: rows(:)
    InsertMode, intent(in) :: insert_mode
    
    integer :: petsc_rows(size(rows))
    integer :: nvals
    PetscErrorCode :: ierr

    nvals = assert_eq(size(rows),size(values),'size mismatch in petsc_vec_t set_values',BOUNDS_CHECK)
    
    call idx_fort2petsc(rows,petsc_rows)
    call VecSetValues(this%petsc_vec,nvals,petsc_rows,values,insert_mode,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecSetValues')
  end subroutine set_values
  
  subroutine insert_values_local(this,rows,values)
    ! Inserts or adds values into certain locations of a vector, using a local ordering of the nodes. 
    !
    ! Not Collective
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecSetValuesLocal.html
    use petscvec, only: VecSetValuesLocal
    use petscvec, only: insert_mode => INSERT_VALUES
    
    class(petsc_vec_type), intent(inout) :: this
    real(dp), intent(in) :: values(:)
    integer, intent(in) :: rows(:)
    
    integer :: petsc_rows(size(rows))
    integer :: nvals
    PetscErrorCode :: ierr
    
    nvals = assert_eq(size(rows),size(values),'size mismatch in petsc_vec_t insert_values_local',BOUNDS_CHECK)
    
    call idx_fort2petsc(rows,petsc_rows)
    call VecSetValuesLocal(this%petsc_vec,nvals,petsc_rows,values,insert_mode,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecSetValuesLocal')
  end subroutine insert_values_local
  
  subroutine copy_from_local_fort_vec(this,fort_vec)
    use petsc_mod, only: no_petsc_procs
    use support, only: range
    
    class(petsc_vec_type), intent(inout) :: this
    real(dp), intent(in) :: fort_vec(:)
    
    integer, allocatable :: local_indexes(:)
    
    call assert(size(fort_vec) == this%local_size(),'copy_from_local_fort_vec: incompatible vector sizes',BOUNDS_CHECK)
    
    if (no_petsc_procs() > 1) then
      call range(size(fort_vec),local_indexes)
      call this%insert_values_local(local_indexes,fort_vec)
      call this%finalize_assembly()
    else
      ! No Local2Global mapping is set for the petsc_vec ==> we cannot insert values
      ! using local indexes. However, with 1 processor, local and global indexes 
      ! are the same, so we can insert values directly using the latter.
      call this%copy_from_global_fort_vec(fort_vec)
    endif
  end subroutine copy_from_local_fort_vec
  
  subroutine copy_from_global_fort_vec(this,fort_vec)
    class(petsc_vec_type), intent(inout) :: this
    real(dp), intent(in) :: fort_vec(:)
    
    integer :: fort_low_idx, fort_high_idx
    integer, allocatable :: rows(:)
    integer :: i
    
    call assert(size(fort_vec) == this%global_size(),'copy_from_global_fort_vec: incompatible vector sizes',BOUNDS_CHECK)
    
    call this%fortran_local_bounds(fort_low_idx,fort_high_idx)
    allocate(rows(this%local_size()))
    rows = [(i, i=fort_low_idx,fort_high_idx)]
    call this%insert(rows,fort_vec(fort_low_idx:fort_high_idx))
    call this%finalize_assembly()
  end subroutine copy_from_global_fort_vec
  
  subroutine finalize_assembly(this)
    use petscvec, only: VecAssemblyBegin, VecAssemblyEnd
    
    class(petsc_vec_type), intent(inout) :: this
    
    PetscErrorCode :: ierr
    
    call VecAssemblyBegin(this%petsc_vec,ierr)
    call assert(ierr == 0,"ierr /= 0 in VecAssemblyBegin")
    call VecAssemblyEnd(this%petsc_vec,ierr)
    call assert(ierr == 0,"ierr /= 0 in VecAssemblyEnd")
  end subroutine finalize_assembly
  
  subroutine fortran_local_bounds(this,low_idx,up_idx)
    use petscvec, only: VecGetOwnershipRange

    class(petsc_vec_type), intent(in) :: this
    integer, intent(out) :: low_idx, up_idx
    
    PetscInt :: petsc_low, petsc_up
    PetscErrorCode :: ierr
    
    call VecGetOwnershipRange(this%petsc_vec,petsc_low,petsc_up,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecGetOwnershipRange')
    low_idx = petsc_low + 1
    up_idx = petsc_up
  end subroutine fortran_local_bounds
  
  integer function local_size(this)
    use petscvec, only: VecGetLocalSize

    class(petsc_vec_type), intent(in) :: this
    
    PetscErrorCode :: ierr
    PetscInt :: petsc_local_size
    
    call VecGetLocalSize(this%petsc_vec,petsc_local_size,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecGetLocalSize')
    local_size = petsc_local_size
  end function local_size
  
  integer function global_size(this)
    use petscvec, only: VecGetSize

    class(petsc_vec_type), intent(in) :: this
    
    PetscErrorCode :: ierr
    PetscInt :: petsc_global_size
    
    call VecGetSize(this%petsc_vec,petsc_global_size,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecGetSize')
    global_size = petsc_global_size
  end function global_size
  
  logical function is_sequential(this)
    ! TODO: There must be a better way of doing this.
    class(petsc_vec_type), intent(in) :: this
    
    is_sequential = this%local_size() == this%global_size()
  end function is_sequential
  
  real(dp) function vec_sum(this)
    ! Returns the sum of all the components of 'this'.
    !
    ! Collective on Vec
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecSum.html
    use petscvec, only: VecSum

    class(petsc_vec_type), intent(in) :: this

    PetscErrorCode :: ierr
    PetscScalar :: petsc_sum

    call VecSum(this%petsc_vec,petsc_sum,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecSum')
    vec_sum = petsc2fort(petsc_sum)
  end function vec_sum
  
  subroutine vec_max(this,max_value,max_position)
    use petscvec, only: VecMax
    
    class(petsc_vec_type), intent(in) :: this
    real(dp), intent(out) :: max_value
    integer, optional, intent(out) :: max_position
    
    PetscErrorCode :: ierr
    PetscInt :: petsc_max_position
    PetscReal :: petsc_max_value
    
    call VecMax(this%petsc_vec,petsc_max_position,petsc_max_value,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecMax')
    max_value = petsc2fort(petsc_max_value)
    if (present(max_position)) max_position = petsc2fort(petsc_max_position)
  end subroutine vec_max
  
  subroutine vec_min(this,min_value,min_position)
    use petscvec, only: VecMin

    class(petsc_vec_type), intent(in) :: this
    real(dp), intent(out) :: min_value
    integer, optional, intent(out) :: min_position
    
    PetscErrorCode :: ierr
    PetscInt :: petsc_min_position
    PetscReal :: petsc_min_value
    
    call VecMin(this%petsc_vec,petsc_min_position,petsc_min_value,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecMin')
    min_value = petsc2fort(petsc_min_value)
    if (present(min_position)) min_position = petsc2fort(petsc_min_position)
  end subroutine vec_min
  
  real(dp) function norm_2(this)
    use petscvec, only: VecNorm, petsc_normtype => NORM_2
    
    class(petsc_vec_type), intent(in) :: this
    
    PetscErrorCode :: ierr
    PetscReal :: petsc_norm_2
    
    call VecNorm(this%petsc_vec, petsc_normtype, petsc_norm_2, ierr)
    call assert(ierr == 0,'ierr /= 0 in VecNorm')
    norm_2 = petsc_norm_2
  end function norm_2
  
  subroutine normalize_by_2norm(this,old_2norm)
    use petscvec, only: VecNormalize

    class(petsc_vec_type), intent(in) :: this
    real(dp), optional, intent(out) :: old_2norm
    
    PetscErrorCode :: ierr
    PetscReal :: petsc_old_2norm
    
    call VecNormalize(this%petsc_vec,petsc_old_2norm,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecNormalize')
    if (present(old_2norm)) old_2norm = real(petsc_old_2norm,dp)
  end subroutine normalize_by_2norm
  
  
  subroutine get_Loc2Glob_mapping(this,Loc2Glob_mapping)
    ! Gets the local-to-global numbering set by VecSetLocalToGlobalMapping()
    ! (in case of ghosted vectors, the map is automatically set, see
    !  http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecMPISetGhost.html )
    ! 
    ! Not Collective
    use petscvec, only: VecGetLocalToGlobalMapping
    use petsc_is_mod, only: petsc_Loc2Glob_map_t

    class(petsc_vec_type), intent(in) :: this
    type(petsc_Loc2Glob_map_t), intent(out) :: Loc2Glob_mapping
    
    PetscErrorCode :: ierr
    
    call VecGetLocalToGlobalMapping(this%petsc_vec,Loc2Glob_mapping%mapping,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecGetLocalToGlobalMapping')
    ! Do not set `Loc2Glob_mapping%destroy_upon_finalization`, because `Loc2Glob_mapping` 
    ! is probably just a pointer to something that PETSc will destroy automatically 
    ! with the associated vector, and PETSc gets upset when destroying a null pointer.
  end subroutine get_Loc2Glob_mapping
  
  
  subroutine get_values_including_ghosts(this,values)
    use petscvec, only: VecGhostGetLocalForm
    use petsc_mod, only: no_petsc_procs
    
    class(petsc_vec_type), intent(in) :: this
    real(dp), intent(out) :: values(:)
    
    type(petsc_vec_type) :: local_ghost_vec
    PetscErrorCode :: ierr
    
    if (no_petsc_procs() > 1) then
      call VecGhostGetLocalForm(this%petsc_vec,local_ghost_vec%petsc_vec,ierr)
      call assert(ierr == 0,'ierr /= 0 in VecGhostGetLocalForm')
      local_ghost_vec%initialized = .true.
      
      call assert(size(values) == local_ghost_vec%local_size(),BOUNDS_CHECK)
      call local_ghost_vec%get_local_values(values)
    else
      call assert(size(values) == this%local_size(),BOUNDS_CHECK)
      call this%get_local_values(values)
    endif
  end subroutine get_values_including_ghosts
  
  subroutine get_local_values(this,values)
    use petscvec, only: VecGetArrayReadF90, VecRestoreArrayReadF90
    
    class(petsc_vec_type), intent(in) :: this
    real(dp), intent(out) :: values(:)
    
    PetscScalar, pointer :: ptr_to_petsc_vec(:)
    PetscErrorCode :: ierr
    
    call assert(size(values) == this%local_size(),BOUNDS_CHECK)
    
    call VecGetArrayReadF90(this%petsc_vec,ptr_to_petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecGetArrayReadF90')
    
      values = ptr_to_petsc_vec
    
    call VecRestoreArrayReadF90(this%petsc_vec,ptr_to_petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecRestoreArrayReadF90')
  end subroutine get_local_values
  
  subroutine get_copy_of_subvec(this,subvec_IS,vec_copy)
    use petscvec, only: VecGetSubVector, VecRestoreSubVector
    use petsc_is_mod, only: petsc_is_t
    
    class(petsc_vec_type), intent(in) :: this
    type(petsc_is_t), intent(in) :: subvec_IS
    class(petsc_vec_type), intent(inout) :: vec_copy
    
    type(petsc_vec_type) :: subvec
    PetscErrorCode :: ierr
    
    call VecGetSubVector(this%petsc_vec,subvec_IS%petsc_is,subvec%petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecGetSubVector')
      subvec%initialized = .true.
      call assert(subvec%local_size()==vec_copy%local_size(),'get_copy_of_subvec: incompatible sizes',BOUNDS_CHECK)
      call subvec%copy(vec_copy)
    call VecRestoreSubVector(this%petsc_vec,subvec_IS%petsc_is,subvec%petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecRestoreSubVector')
  end subroutine get_copy_of_subvec
  
  subroutine update_ghost_values(this)
    use petscvec, only: VecGhostUpdateBegin, VecGhostUpdateEnd, &
        insert_mode => INSERT_VALUES, scatter_mode => SCATTER_FORWARD
    use petsc_mod, only: no_petsc_procs
    
    class(petsc_vec_type), intent(inout) :: this
    
    PetscErrorCode :: ierr
    
    if (.not. this%is_sequential()) then
      call VecGhostUpdateBegin(this%petsc_vec,insert_mode,scatter_mode,ierr)
      call assert(ierr == 0,'ierr /= 0 in VecGhostUpdateBegin')
      call VecGhostUpdateEnd(this%petsc_vec,insert_mode,scatter_mode,ierr)
      call assert(ierr == 0,'ierr /= 0 in VecGhostUpdateEnd')
    else
      call assert(no_petsc_procs() == 1, &
      'Why do you update ghost values of a sequential vector during parallel calculations?')
      ! Do nothing
    endif
  end subroutine update_ghost_values
  
  subroutine to_seq_vec_on_0th_process(this,seq_vec)
    use petscvec, only: tVecScatter, &
        VecScatterCreateToZero, VecScatterBegin, VecScatterEnd, VecScatterDestroy, &
        insert_mode => INSERT_VALUES, scatter_mode => SCATTER_FORWARD
    use petsc_mod, only: no_petsc_procs
    
    class(petsc_vec_type), intent(in) :: this
    type(petsc_vec_type), intent(out) :: seq_vec
    
    type(tVecScatter) :: scatter_ctx
    PetscErrorCode :: ierr
    
    if (.not. this%is_sequential()) then
      call VecScatterCreateToZero(this%petsc_vec,scatter_ctx,seq_vec%petsc_vec,ierr)
      call assert(ierr == 0,'ierr /= 0 in VecScatterCreateToZero')
        call VecScatterBegin(scatter_ctx,this%petsc_vec,seq_vec%petsc_vec,insert_mode,scatter_mode,ierr)
        call assert(ierr == 0,'ierr /= 0 in VecScatterBegin')
        call VecScatterEnd(scatter_ctx,this%petsc_vec,seq_vec%petsc_vec,insert_mode,scatter_mode,ierr)
        call assert(ierr == 0,'ierr /= 0 in VecScatterEnd')
      call VecScatterDestroy(scatter_ctx,ierr)
      call assert(ierr == 0,'ierr /= 0 in VecScatterDestroy')
      seq_vec%initialized = .true.
    else
      call assert(no_petsc_procs() == 1, 'Why do you scatter a sequential vector during parallel calculations?')
      call this%duplicate(seq_vec)
      call this%copy(seq_vec)
    endif
  end subroutine to_seq_vec_on_0th_process
  
  subroutine add_vec(this, x)
    class(petsc_vec_type), intent(inout) :: this
    type(petsc_vec_type), intent(in) :: x

    call this%aypx(1.0_dp, x) ! The docs specifically mention that it is optimized for this value of `beta`.
  end subroutine

  subroutine subtract_vec(this, x)
    class(petsc_vec_type), intent(inout) :: this
    type(petsc_vec_type), intent(in) :: x

    call this%axpy(-1.0_dp, x)
  end subroutine

  subroutine axpy(this,alpha,x)
    ! Famous 'AXPY' operation: Y <-- Y + alpha * X
    ! x and 'this' MUST be different vectors.
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecAXPY.html
    use petscvec, only: VecAXPY

    class(petsc_vec_type), intent(inout) :: this
    real(dp), intent(in) :: alpha
    type(petsc_vec_type), intent(in) :: x
    
    PetscErrorCode :: ierr
    
    call VecAXPY(this%petsc_vec,fort2petsc(alpha),x%petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecAXPY')
  end subroutine

  subroutine axpby(y, alpha, beta, x)
    !     y <-- a * x + b * y
    ! x and y MUST be different vectors.
    ! The implementation is optimized for alpha and/or beta values of 0.0 and 1.0.
    ! 
    ! https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecAXPBY.html#VecAXPBY
    use petscvec, only: VecAXPBY

    class(petsc_vec_type), intent(inout) :: y
    real(dp), intent(in) :: alpha, beta
    type(petsc_vec_type), intent(in) :: x
    
    PetscErrorCode :: ierr

    call VecAXPBY(y%petsc_vec, fort2petsc(alpha), fort2petsc(beta), x%petsc_vec, ierr)
    call assert(ierr == 0,'ierr /= 0 in VecAXPBY')
  end subroutine
  
  subroutine aypx(this, beta, x)
    ! Computes
    !     this = x + beta * this .
    ! x and 'this' MUST be different vectors.
    ! The implementation is optimized for beta of -1.0, 0.0, and 1.0 
    !
    ! Logically Collective on Vec
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecAYPX.html#VecAYPX
    use petscvec, only: VecAYPX

    class(petsc_vec_type), intent(inout) :: this
    real(dp), intent(in) :: beta
    type(petsc_vec_type), intent(in) :: x

    PetscErrorCode :: ierr

    call VecAYPX(this%petsc_vec,fort2petsc(beta),x%petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecAYPX')
  end subroutine

  subroutine waxpy(this,alpha,x,y)
    ! Computes:  this = alpha x + y
    ! Notes: 'this' cannot be either 'x' or 'y', but 'x' and 'y' can be the same.
    !
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecWAXPY.html
    use petscvec, only: VecWAXPY

    class(petsc_vec_type), intent(inout) :: this
    real(dp), intent(in) :: alpha
    class(petsc_vec_type), intent(in) :: x, y

    PetscErrorCode :: ierr

    call VecWAXPY(this%petsc_vec,fort2petsc(alpha),x%petsc_vec,y%petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecWAXPY')
  end subroutine waxpy

  subroutine mult_with_scal(this,alpha)
    use petscvec, only: VecScale

    class(petsc_vec_type), intent(inout) :: this
    real(dp), intent(in) :: alpha
    
    PetscErrorCode :: ierr
    
    call VecScale(this%petsc_vec,alpha,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecScale')
  end subroutine mult_with_scal
  
  subroutine pointwise_mult(this,x,y)
    ! Computes the componentwise multiplication this = x * y.
    ! Any subset of the x, y, and 'this' may be the same vector.
    !
    ! Logically Collective on Vec
    ! http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Vec/VecPointwiseMult.html
    use petscvec, only: VecPointwiseMult

    class(petsc_vec_type), intent(inout) :: this, x, y

    PetscErrorCode :: ierr

    call VecPointwiseMult(this%petsc_vec,x%petsc_vec,y%petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecPointwiseMult')
  end subroutine pointwise_mult

  subroutine pointwise_divide(this,x,y)
    ! Computes the component-wise division this = x / y.
    ! Logically Collective on Vec.
    !
    ! Notes: any subset of the 'x', 'y', and 'this' may be the same vector. 
    use petscvec, only: VecPointwiseDivide

    class(petsc_vec_type), intent(inout) :: this, x, y
    
    PetscErrorCode :: ierr
    
    call VecPointwiseDivide(this%petsc_vec,x%petsc_vec,y%petsc_vec,ierr)
    call assert(ierr == 0,'ierr /= 0 in VecPointwiseDivide')
  end subroutine pointwise_divide
  
  impure elemental subroutine drop_petsc_vec(this)
    use petscvec, only: VecDestroy

    type(petsc_vec_type), intent(inout) :: this
    
    PetscErrorCode :: ierr
    
    if (this%initialized) then
      call VecDestroy(this%petsc_vec,ierr)
      call assert(ierr == 0,'ierr /= 0 in VecDestroy')
    endif
  end subroutine drop_petsc_vec
end module petsc_vec_mod
