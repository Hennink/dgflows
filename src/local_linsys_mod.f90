module local_linsys_mod
  use assertions, only: assert, assert_eq, BOUNDS_CHECK
  use f90_kind, only: dp
  use solvec_mod, only: solvec_t
  use mtx_block_mod, only: mtx_block_t
  use vec_block_mod, only: vec_block_t
  implicit none
  
  private
  public :: local_linsys_t
  
  integer, parameter :: max_no_lhs = 32
  
  type :: local_linsys_t
    private
    type(solvec_t), pointer :: solvec => null()
    type(mtx_block_t) :: lhs(max_no_lhs)
    type(vec_block_t), public :: rhs
    integer :: no_lhs = 0
  
  contains
    procedure, non_overridable :: init
    procedure, non_overridable :: add_to_glob_linsys
    procedure, non_overridable :: add_inprod_to_lhs_all_dirs, add_inprod_to_rhs_all_dirs
    procedure, non_overridable :: add_int_dotprod_to_lhs
    procedure, non_overridable :: add_int_dotprod_to_rhs
    procedure, non_overridable :: add_transposed_elemmats_for_dirs, add_transposed_elemmat_all_dirs
    procedure, non_overridable, private :: idx_of_coupled_elem
    procedure, non_overridable, private :: couple_new_elem
  end type local_linsys_t
  
contains
  subroutine init(this,solvec,row_elem)
    use solvec_mod, only: solvec_t
    
    class(local_linsys_t), intent(out) :: this
    type(solvec_t), target, intent(in) :: solvec
    integer, intent(in) :: row_elem
    
    this%solvec => solvec
    call this%rhs%init(row_elem,solvec%handler)
  end subroutine init
  
  subroutine add_to_glob_linsys(this,global_lhs,global_rhs)
    use petsc_mat_mod, only: petsc_mat_type
    use petsc_vec_mod, only: petsc_vec_type
    
    class(local_linsys_t), intent(inout) :: this
    type(petsc_mat_type), intent(inout) :: global_lhs
    type(petsc_vec_type), optional, intent(inout) :: global_rhs
    
    integer :: j
    
    do j = 1, this%no_lhs
      call this%lhs(j)%add_to_glob_mtx(global_lhs,this%solvec%handler)
    enddo
    if (present(global_rhs)) call this%rhs%add_to_glob_vec(global_rhs,this%solvec%handler)
  end subroutine add_to_glob_linsys
  
  subroutine add_inprod_to_lhs_all_dirs(this,scal1,scal2,quad_weights,explicit,alpha)
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t

    class(local_linsys_t), intent(inout) :: this
    type(unidir_lincom_rank0_t), intent(in) :: scal1, scal2
    real(dp), intent(in) :: quad_weights(:)
    logical, intent(in) :: explicit
    real(dp), intent(in) :: alpha

    integer :: dir
    integer :: idx
    real(dp), allocatable :: scal2_vals(:,:)

    if (explicit) then
      scal2_vals = scal2%eval(this%solvec)
      do dir = 1, this%solvec%no_dirs()
        call this%rhs%add_inprod_to_dir(scal1,scal2_vals(:,dir),dir,-alpha*quad_weights)
      enddo
    else
      call this%idx_of_coupled_elem(scal2%elem,idx)
      call this%lhs(idx)%add_inprod(scal1,scal2,alpha*quad_weights)
    endif
  end subroutine add_inprod_to_lhs_all_dirs

  subroutine add_transposed_elemmats_for_dirs(this, rhs_elem, tranpose_elemmats_for_dirs)
    class(local_linsys_t), intent(inout) :: this
    integer, intent(in) :: rhs_elem
    real(dp), intent(in) :: tranpose_elemmats_for_dirs(:,:,:,:)

    integer :: idx

    call this%idx_of_coupled_elem(rhs_elem,idx)
    call this%lhs(idx)%add_transposed_elemmats_for_dirs(tranpose_elemmats_for_dirs)
  end subroutine

  subroutine add_transposed_elemmat_all_dirs(this, rhs_elem, transposed_elemmat)
    class(local_linsys_t), intent(inout) :: this
    integer, intent(in) :: rhs_elem
    real(dp), intent(in) :: transposed_elemmat(:,:)

    integer :: idx

    call this%idx_of_coupled_elem(rhs_elem,idx)
    call this%lhs(idx)%add_transposed_elemmat_all_dirs(transposed_elemmat)
  end subroutine

  subroutine add_inprod_to_rhs_all_dirs(this,scal1,scal2,quad_weights,alpha)
    use unidir_lincom_rank0_mod, only: unidir_lincom_rank0_t

    class(local_linsys_t), intent(inout) :: this
    type(unidir_lincom_rank0_t), intent(in) :: scal1
    real(dp), intent(in) :: scal2(:), quad_weights(:)
    real(dp), optional, intent(in) :: alpha

    real(dp) :: alpha_x_weights(size(quad_weights))

    if (present(alpha)) then
      alpha_x_weights = alpha * quad_weights
    else
      alpha_x_weights = quad_weights
    endif
    call this%rhs%add_inprod_all_dirs(scal1,scal2,alpha_x_weights)
  end subroutine add_inprod_to_rhs_all_dirs

  subroutine add_int_dotprod_to_lhs(this,vec1,vec2,quad_weights,alpha,explicit)
    use lincom_rank1_mod, only: lincom_rank1_t
    
    class(local_linsys_t), intent(inout) :: this
    type(lincom_rank1_t), intent(in) :: vec1, vec2
    real(dp), intent(in) :: quad_weights(:)
    real(dp), intent(in) :: alpha
    logical, intent(in) :: explicit
    
    integer :: idx
    
    call assert(size(quad_weights) == vec1%np(),BOUNDS_CHECK)
    call assert(size(quad_weights) == vec2%np(),BOUNDS_CHECK)

    if (explicit) then
      call add_int_dotprod_to_rhs(this,vec1,vec2%eval(this%solvec),quad_weights,-alpha)
    else
      call this%idx_of_coupled_elem(vec2%elem,idx)
      call this%lhs(idx)%add_int_dotprod(vec1,vec2,quad_weights,alpha)
    endif
  end subroutine add_int_dotprod_to_lhs
  
  subroutine add_int_dotprod_to_rhs(this,vec1,vec2,quad_weights,alpha)
    use lincom_rank1_mod, only: lincom_rank1_t
    
    class(local_linsys_t), intent(inout) :: this
    type(lincom_rank1_t), intent(in) :: vec1
    real(dp), intent(in) :: vec2(:,:)
    real(dp), intent(in) :: quad_weights(:)
    real(dp), optional, intent(in) :: alpha
    
    real(dp) :: my_alpha

    call assert(size(quad_weights) == vec1%np(),BOUNDS_CHECK)
    
    if (present(alpha)) then
      my_alpha = alpha
    else
      my_alpha = 1.0_dp
    endif
    call this%rhs%add_int_dotprod(vec1, vec2, quad_weights, my_alpha)
  end subroutine
  
  subroutine idx_of_coupled_elem(this,coupled_elem,idx)
    class(local_linsys_t), intent(inout) :: this
    integer, intent(in) :: coupled_elem
    integer, intent(out) :: idx
    
    do idx = 1, this%no_lhs
      if (this%lhs(idx)%rhs_elem == coupled_elem) return
    enddo
    call this%couple_new_elem(coupled_elem,idx)
  end subroutine idx_of_coupled_elem
  
  subroutine couple_new_elem(this,coupled_elem,new_idx)
    class(local_linsys_t), intent(inout) :: this
    integer, intent(in) :: coupled_elem
    integer, intent(out) :: new_idx

    integer :: row_elem
    
    call assert(this%no_lhs < max_no_lhs,'local_linsys_t: stack overflow')
    
    this%no_lhs = this%no_lhs + 1
    new_idx = this%no_lhs
    row_elem = this%rhs%elem
    associate(new_lhs => this%lhs(new_idx), &
              handler => this%solvec%handler)
      call new_lhs%init(row_elem,coupled_elem,handler)
    end associate
  end subroutine couple_new_elem
end module local_linsys_mod
