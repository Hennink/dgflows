module mesh_objects
  use assertions, only: assert, assert_eq, perform_all_bc, BOUNDS_CHECK
  use code_const, only: BOUNDARY, INTERNAL, INTERFACE_SOLID_FLUID, INTERFACE_WITH_IGNORED, UNINITIALIZED_INT
  use exceptions, only: error
  use array_opers_mod, only: copy_lower_triangular_part, copy_lower2upper_triangle
  use f90_kind, only: dp
  use lapack95, only: potrs ! http://www.netlib.org/lapack95/lug95/node340.html
  use pnt_set_mod, only: pnt_set_t
  use run_data, only: assume_affine_elems, mesh_dimen
  use string_manipulations, only: int2char
  use support, only: identity_matrix, iseq
  implicit none
  
  private
  public :: elem_id, same_id
  public :: elem_ptr
  public :: elem_list
  public :: eltype_of_faces_of_elem
  public :: face_ptr
  public :: face_list
  public :: get_1st_order_eltype
  public :: get_no_elem_edges
  public :: mat_id_is_fluid_id
  public :: no_faces_of_elem
  public :: get_no_children
  public :: get_no_interior_edges
  public :: get_no_interior_faces
  public :: vertex_coordinate
  public :: vertex_list
  public :: vertex_ptr
  public :: side_of_internal_nghb_of_bnd_face
  public :: same_vtx
  
  type elem_id
    integer :: branch                            ! the coarsest element (initial mesh)
    character(20) :: path='00000000000000000000' ! the refinement path taken starting at branch
  end type elem_id
  
  type vertex_coordinate
    real(dp), dimension(3) :: xyz
  end type vertex_coordinate
  
  type elem_ptr
    ! This type facilitates an array of pointers to elements.
    type(elem_list), pointer :: point => null()
  end type elem_ptr
  
  type face_ptr
    ! This type facilitates an array of pointers to faces.
    type(face_list), pointer :: point => null()
  end type face_ptr
  
  type vertex_ptr
    ! This type facilitates an array of pointers to vertices.
    type(vertex_list), pointer :: point => null()
  end type vertex_ptr
  
  type elem_list
    ! Element structure linked list containing the
    !  - eltype: the element type
    !  - level: the refinement level of this element
    !  - elem_id: a unique identifier that can be used to traverse the tree to get to the element
    !  - active: whether this element is active (no children) or not (further refined)
    !  - mother: pointer to the mother of the element
    !  - children: pointer array of pointers to the children of the element
    integer :: eltype
    integer :: level
    type(elem_id) :: id
    integer :: tot_elem_no
    integer :: active_elem_no
    integer :: active_fluid_elem_no
    integer :: mat_id
    logical :: active
    type(elem_list), pointer :: mother => null()
    type(elem_ptr), allocatable :: children(:)
    type(face_ptr), allocatable :: faces(:)
    type(face_ptr), allocatable :: interior_faces(:)
    type(vertex_ptr), allocatable :: vertices(:)

    integer :: nqp
    type(pnt_set_t) :: quad_points
    real(dp), allocatable :: quot(:)
    real(dp) :: volume
    real(dp), allocatable :: chol_massmat(:,:)
  
  contains
    procedure, non_overridable :: active_subfaces => aux_active_subfaces
    procedure, non_overridable :: dimen => dimen_of_elem
    procedure, non_overridable :: is_near_bnd => elem_is_near_bnd
    procedure, non_overridable :: is_fluid => elem_is_fluid, &
                                  is_to_be_ignored => elem_is_to_be_ignored, &
                                  is_at_bnd => elem_is_at_bnd
    procedure, non_overridable :: glob_coord_vertices => glob_coord_vertices_of_elem
    procedure, non_overridable :: avg_vtx, minmax_xyz_vert, com
    procedure, non_overridable :: get_active_elem_no => get_active_elem_no_of_elem
    procedure, non_overridable :: global_idx_vertices => global_idx_vertices_of_elem
    procedure, non_overridable :: no_common_vtx_ngbrs => no_common_vtx_ngbrs_of_elem
    generic :: solve_for_massmat => solve_for_massmat__one_rhs, solve_for_massmat__many_rhs
    procedure, non_overridable :: massmatmult, massmat, inv_massmat
    procedure, non_overridable :: global_local_volume_ratio
    procedure, non_overridable :: solve_for_massmat__one_rhs, solve_for_massmat__many_rhs
  end type elem_list
  
  type face_list
    ! Face structure linked list containing the
    !  - level: the refinement level of this element
    !  - active: whether this face is active (no children) or not (further refined)
    !  - mother: pointer to the mother of the element
    !  - children: pointer array of pointers to the children of the element
    !  - neighbors: pointer array of pointers to the neighboring elements
    integer :: level = UNINITIALIZED_INT
    logical :: active
    integer :: ftype = UNINITIALIZED_INT
    logical :: is_periodic = .false.
    integer :: periodic_id = 0
    integer :: bc_id = 0
    type(face_list), pointer :: mother => null()
    type(face_ptr), allocatable :: children(:)
    type(elem_ptr), allocatable :: elem_neighbors(:)
    integer :: neighbor_loc_face_no(2) = UNINITIALIZED_INT

    integer :: nqp = UNINITIALIZED_INT
    real(dp), allocatable :: face_lcors(:,:)
    type(pnt_set_t), allocatable :: quad_points(:)
    real(dp), allocatable :: quot(:)
    real(dp), allocatable :: normal(:,:)
    real(dp) :: area
    real(dp) :: diameter
    
  contains
    private
    procedure, non_overridable, public :: is_boundary
    procedure, non_overridable, public :: elem_is_E1 => elem_is_neighbor1_for_face
    procedure, non_overridable, public :: avg_unit_normal => avg_unit_normal_of_face
    procedure, non_overridable, public :: vertices => face_vertices, &
                                          glob_coord_vertices => glob_coord_vertices_of_face
    procedure, non_overridable, public :: distance_from_elem_centroid => distance_of_face_from_elem_centroid
  end type face_list
  
  type vertex_list
    ! Vertex linked list containing the vertex number and coordinates
    ! It is a linked list because that is dynamic. The actual next pointer is
    ! not used except for creating the actual storage.
    integer :: vertex_no
    type(vertex_coordinate) :: coordinate
    type(vertex_list), pointer :: next => null()
  end type vertex_list
  
  interface
    module subroutine active_subfaces_of_elem(this,subfaces,neighbors)
      class(elem_list), intent(in) :: this
      type(face_ptr), allocatable, dimension(:), intent(out) :: subfaces
      type(elem_ptr), allocatable, dimension(:), intent(out) :: neighbors
    end subroutine active_subfaces_of_elem
  end interface
  
contains
  pure logical function same_id(id1,id2)
    type(elem_id), intent(in) :: id1, id2

    same_id = id1%branch == id2%branch .and. id1%path == id2%path
  end function same_id
  
  pure integer function eltype_of_faces_of_elem(eltype) result(eltype_of_face)
    integer, intent(in) :: eltype
    
    select case(eltype)
      case(12)
        eltype_of_face = 1
      case(13)
        eltype_of_face = 1
      case(23)
        eltype_of_face = 12
      case(26)
        eltype_of_face = 13
      case(24)
        eltype_of_face = 12
      case(28)
        eltype_of_face = 13
      case(29)
        eltype_of_face = 13
      case(34)
        eltype_of_face = 23
      case(38)
        eltype_of_face = 24
      case(310)
        eltype_of_face = 26
      case(320)
        eltype_of_face = 28
      case(327)
        eltype_of_face = 29
      case default
        error stop "unknown eltype"
    end select
  end function eltype_of_faces_of_elem
  
  pure integer function no_faces_of_elem(eltype) result(no_elem_faces)
    integer, intent(in) :: eltype
    
    select case(eltype)
      case(12,13)
        no_elem_faces = 2
      case(23,26)
        no_elem_faces = 3
      case(24,28,29)
        no_elem_faces = 4
      case(34,310)
        no_elem_faces = 4
      case(38,320,327)
        no_elem_faces = 6
      case default
        error stop "unknown eltype"
    end select
  end function no_faces_of_elem
  
  pure integer function get_no_interior_faces(eltype) result(no_interior_faces)
    integer, intent(in) :: eltype
    
    select case(eltype)
      case(12,13)
        no_interior_faces = 0
      case(23,26)
        no_interior_faces = 3
      case(24,28,29)
        no_interior_faces = 4
      case(34,310)
        no_interior_faces = 8
      case(38,320,327)
        no_interior_faces = 12
      case default
        error stop "unknown eltype"
    end select
  end function get_no_interior_faces
  
  pure integer function get_no_elem_edges(eltype) result(no_elem_edges)
    integer,intent(in) :: eltype
    
    select case (eltype)
      case(34,310)
        no_elem_edges = 6
      case(38,320)
        no_elem_edges = 12
      case default
        error stop "unknown eltype"
    end select
  end function get_no_elem_edges
  
  pure integer function get_no_interior_edges(eltype) result(no_interior_edges)
    integer, intent(in) :: eltype

    select case(eltype)
      case(34,310)
        no_interior_edges = 1
      case(38,320,327)
        no_interior_edges = 6
      case default
        error stop "unknown eltype"
    end select
  end function get_no_interior_edges
  
  pure integer function get_no_children(eltype) result(no_children)
    integer, intent(in) :: eltype
    
    select case(eltype)
      case(12,13)
        no_children = 2
      case(23,26,24,28,29)
        no_children = 4
      case(34,310,38,320,327)
        no_children = 8
      case default
        error stop "unknown eltype"
    end select
  end function get_no_children
  
  pure integer function get_1st_order_eltype(eltype) result(first_order_eltype)
    integer, intent(in) :: eltype
    
    select case(eltype)
      case(12,13)
        first_order_eltype = 12
      case(23,26)
        first_order_eltype = 23
      case(24,28,29)
        first_order_eltype = 24
      case(34,310)
        first_order_eltype = 34
      case(38,320,327)
        first_order_eltype = 38
      case default
        error stop "unknown eltype"
    end select
  end function get_1st_order_eltype
  
  subroutine aux_active_subfaces(this,subfaces,neighbors)
    class(elem_list), intent(in) :: this
    type(face_ptr), allocatable, dimension(:), intent(out) :: subfaces
    type(elem_ptr), allocatable, dimension(:), intent(out) :: neighbors

    call active_subfaces_of_elem(this,subfaces,neighbors)
  end subroutine aux_active_subfaces

  pure integer function dimen_of_elem(this) result(dimen)
    use local_elem, only: get_dimen
    
    class(elem_list), intent(in) :: this
    
    ! this function must be pure, so cannot use a simple `assert_eq`:
    if (perform_all_bc) then
      if (mesh_dimen /= get_dimen(this%eltype)) error stop "expected dimen of elem to be the same as dimen of mesh"
    endif
    dimen = mesh_dimen
  end function dimen_of_elem
  
  
  pure logical function elem_is_to_be_ignored(this) result(is_to_be_ignored)
    use run_data, only: ignore_elems_with_mat_id
    
    class(elem_list), intent(in) :: this
    
    is_to_be_ignored = any(ignore_elems_with_mat_id == this%mat_id)
  end function elem_is_to_be_ignored
  
  pure logical function elem_is_fluid(this) result(is_fluid)
    class(elem_list), intent(in) :: this
    
    is_fluid = mat_id_is_fluid_id(this%mat_id)
  end function elem_is_fluid
  
  elemental logical function mat_id_is_fluid_id(id)
    ! Convention for fluid material id:
    ! mat_id = from 10000 to 19999
    integer, intent(in) :: id
    
    mat_id_is_fluid_id = id / 10000 == 1
  end function mat_id_is_fluid_id
  
  logical recursive function elem_is_near_bnd(this,only_fluid_mesh,nghb_depth,bnd_ids) result(near_bnd)
    ! Is 'this' no more than 'nghb_depth' elements removed from the boundary?
    class(elem_list), intent(in) :: this
    logical, intent(in) :: only_fluid_mesh
    integer, intent(in) :: nghb_depth, bnd_ids(:)

    type(elem_ptr), allocatable :: active_neighbors(:)
    type(face_ptr), allocatable :: active_subfaces(:)
    integer :: i

    call assert(nghb_depth >= 0)

    if (this%is_at_bnd(only_fluid_mesh,bnd_ids)) then
      near_bnd = .true.
    elseif (nghb_depth == 0) then
      near_bnd = .false.
    else
      call this%active_subfaces(active_subfaces,active_neighbors)
      do i = 1, size(active_subfaces)
        if (active_subfaces(i)%point%is_boundary(only_fluid_mesh)) then
          ! We are on a boundary element, but not with the ID that we want.
          near_bnd = .false.
        else
          near_bnd = active_neighbors(i)%point%is_near_bnd(only_fluid_mesh,nghb_depth-1,bnd_ids)
        endif
        if (near_bnd) return
      enddo
    endif
  end function elem_is_near_bnd

  logical function elem_is_at_bnd(this,only_fluid_mesh,bnd_ids)
    ! Does 'this' have a boundary face?
    class(elem_list), intent(in) :: this
    logical, intent(in) :: only_fluid_mesh
    integer, optional, intent(in) :: bnd_ids(:)
    
    integer :: i
    
    do i = 1, size(this%faces)
      ! We can loop directly over this%faces and not necessarily over this%active_subfaces()
      ! because the face%ftype and face%bc_id are inherited by any children face.
      ! Therefore, the outcomes of the following logical statements is not altered.
      if (this%faces(i)%point%is_boundary(only_fluid_mesh)) then
        if (present(bnd_ids)) then
          elem_is_at_bnd = findloc(bnd_ids,this%faces(i)%point%bc_id,dim=1) > 0
        else
          elem_is_at_bnd = .true.
        endif
        if (elem_is_at_bnd) return
      endif
    enddo
    elem_is_at_bnd = .false.
  end function elem_is_at_bnd

  pure subroutine glob_coord_vertices_of_elem(this,glob_coord_vertices)
    class(elem_list), intent(in) :: this
    real(dp), allocatable, intent(out) :: glob_coord_vertices(:,:)
    
    integer :: no_vertices
    integer :: i
    
    no_vertices = size(this%vertices)
    allocate(glob_coord_vertices(this%dimen(),no_vertices))
    do i = 1, no_vertices
      glob_coord_vertices(:,i) = this%vertices(i)%point%coordinate%xyz(1:this%dimen())
    enddo
  end subroutine glob_coord_vertices_of_elem
  
  pure function avg_vtx(this)
    class(elem_list), intent(in) :: this
    real(dp) :: avg_vtx(this%dimen())
    
    integer :: v, nvert

    nvert = size(this%vertices)
    avg_vtx = 0.0_dp
    do v = 1, nvert
      avg_vtx = avg_vtx + this%vertices(v)%point%coordinate%xyz(:this%dimen())
    enddo
    avg_vtx = avg_vtx / nvert
  end function avg_vtx
  
  subroutine minmax_xyz_vert(this,min_xyz,max_xyz)
    ! Returns the minima and maxima of the Cartesian components of the vertices.
    class(elem_list), intent(in) :: this
    real(dp), intent(out) :: min_xyz(:), max_xyz(:)
    
    integer :: dimen
    integer :: v

    dimen = assert_eq(size(min_xyz),size(max_xyz),this%dimen())
    min_xyz = huge(1.0_dp)
    max_xyz = - huge(1.0_dp)
    do v = 1, size(this%vertices)
      associate(xyz => this%vertices(v)%point%coordinate%xyz)
        min_xyz = min(min_xyz,xyz(:dimen))
        max_xyz = max(max_xyz,xyz(:dimen))
      end associate
    enddo
  end subroutine minmax_xyz_vert

  function com(this)
    ! Returns the center of mass of 'this' (gcor).
    class(elem_list), intent(in) :: this
    real(dp) :: com(mesh_dimen)

    com = matmul(this%quad_points%gcors,this%quot)
  end function com

  integer function get_active_elem_no_of_elem(this,only_fluid_mesh) result(active_elem_no)
    class(elem_list), intent(in) :: this
    logical, intent(in) :: only_fluid_mesh
    
    if (only_fluid_mesh) then
      call assert(this%is_fluid(),'get_active_elem_no_of_elem: not a fluid')
      active_elem_no = this%active_fluid_elem_no
    else
      active_elem_no = this%active_elem_no
    endif
  end function get_active_elem_no_of_elem
  
  
  function global_idx_vertices_of_elem(this) result(global_indices)
    use binary_tree, only: int_set
    use local_elem, only: no_vertices
    
    class(elem_list), intent(in) :: this
    integer, allocatable :: global_indices(:)
    
    type(int_set) :: elem_vtx_set
    type(elem_ptr), allocatable :: dummy(:)
    type(face_ptr), allocatable :: active_subfaces(:)
    type(vertex_ptr), allocatable :: sub_face_vtxs(:)
    integer :: i, j
    logical, parameter :: ONLY_FLUID = .false.
    
    call this%active_subfaces(active_subfaces,dummy)
    do i = 1, size(active_subfaces)
      call active_subfaces(i)%point%vertices(ONLY_FLUID,sub_face_vtxs)
      do j = 1, size(sub_face_vtxs)
        call elem_vtx_set%insert(sub_face_vtxs(j)%point%vertex_no)
      enddo
    enddo
    call elem_vtx_set%sorted_values(global_indices)
  end function global_idx_vertices_of_elem
  
  pure integer function no_common_vtx_ngbrs_of_elem(this) result(no_common_vtx_ngbrs)
    use local_elem, only: no_vertices
    
    class(elem_list), intent(in) :: this
    
    no_common_vtx_ngbrs = no_vertices(eltype_of_faces_of_elem(this%eltype))
  end function no_common_vtx_ngbrs_of_elem
  
  subroutine solve_for_massmat__one_rhs(this,vec)
    ! Solves for the mass matrix with 'vec' as the rhs, and stores the result in 'vec'.
    ! The number of basis functions is implied by the size of 'vec'.
    class(elem_list), intent(in) :: this
    real(dp), intent(inout) :: vec(:)

    integer :: nnod
    integer :: info

    if (assume_affine_elems) then
      vec = vec / this%global_local_volume_ratio()

    else
      nnod = size(vec)
      call potrs(this%chol_massmat(:nnod,:nnod),vec,'L',info)
      call assert(info == 0,'LAPACK potrs: info /= 0')
    endif
  end subroutine solve_for_massmat__one_rhs
  
  real(dp) function global_local_volume_ratio(this)
    ! Returns the ratio of the global element volume and the volume of the corresponding local element.
    use local_elem, only: local_elem_volume
    
    class(elem_list), intent(in) :: this

    global_local_volume_ratio = this%volume / local_elem_volume(this%eltype)
  end function global_local_volume_ratio

  subroutine solve_for_massmat__many_rhs(this,vecs)
    ! Solves for the mass matrix with 'vecs' as the rhs, and stores the result in 'vecs'.
    ! The number of basis functions is implied by the size of 'vecs'.
    class(elem_list), intent(in) :: this
    real(dp), intent(inout) :: vecs(:,:)

    integer :: nnod
    integer :: info

    if (assume_affine_elems) then
      vecs = vecs / this%global_local_volume_ratio()
    else
      nnod = size(vecs,1)
      call potrs(this%chol_massmat(:nnod,:nnod),vecs,'L',info)
      call assert(info == 0,'LAPACK potrs: info /= 0')
    endif
  end subroutine solve_for_massmat__many_rhs
  
  subroutine massmatmult(this,vec,prod)
    ! Computes the product of the mass matrix and vec. The shape of the mass 
    ! matrix is inferred from the input arguments, and can be non-square.
    !     prod    =  M vec
    !     prod_i  =  M_ik * vec_k  = (L L^T)_ik * vec_k
    !             =  L_iq * L_kq * vec_k
    ! 
    ! Probably equivalent to 'TRMV' in BLAS.
    class(elem_list), intent(in) :: this
    real(dp), intent(in) :: vec(:)
    real(dp), intent(out) :: prod(:)

    integer :: minsize
    integer :: i, q, k, max_nnod

    if (assume_affine_elems) then
      minsize = min(size(vec),size(prod))
      prod(:minsize) = this%global_local_volume_ratio() * vec(:minsize)
      prod(1+minsize:size(prod)) = 0.0_dp
    
    else
      max_nnod = assert_eq(shape(this%chol_massmat))
      call assert(size(vec) <= max_nnod,'massmatmult: size(vec) too big')
      call assert(size(prod) <= max_nnod,'massmatmult: size(prod) too big')
      
      associate(L => this%chol_massmat)
        prod = 0.0_dp
        do i = 1, size(prod)
          do q = 1, i
            do k = q, size(vec)
              prod(i) = prod(i) + L(i,q) * L(k,q) * vec(k)
            enddo
          enddo
        enddo
      end associate
    endif
  end subroutine massmatmult

  function massmat(this,order)
    ! Returns the full mass matrix for basis functions up to 'order'.
    !     M     =  L L^T
    !     M_ij  =  L_im * L_jm
    ! 
    ! This is always inefficient when the elements are known to be affine.
    use local_elem, only: no_dg_functions

    class(elem_list), intent(in) :: this
    integer, intent(in) :: order
    real(dp), allocatable :: massmat(:,:)

    integer :: i, j, m, nnod, stored_nnod

    nnod = no_dg_functions(this%eltype,order)
    if (assume_affine_elems) then
      massmat = this%global_local_volume_ratio() * identity_matrix(nnod)
    
    else
      stored_nnod = assert_eq(shape(this%chol_massmat),BOUNDS_CHECK)
      call assert(nnod <= stored_nnod,'massmat: did not store this much')

      allocate(massmat(nnod,nnod),source=0.0_dp)
      associate(L => this%chol_massmat)
        do m = 1, stored_nnod
          do j = m, nnod
            do i = j, nnod
              massmat(i,j) = massmat(i,j) + L(i,m) * L(j,m)
            enddo
          enddo
        enddo
      end associate
      call copy_lower2upper_triangle(massmat)
    endif
  end function massmat

  function inv_massmat(this,order) result(inv)
    ! Returns the full inverse mass matrix for basis functions up to 'order'.
    ! 
    ! This is always inefficient when the elements are known to be affine.
    use lapack95, only: chol2inv => potri
    use local_elem, only: no_dg_functions

    class(elem_list), intent(in) :: this
    integer, intent(in) :: order
    real(dp), allocatable :: inv(:,:)

    integer :: nnod
    integer :: info

    nnod = no_dg_functions(this%eltype,order)
    if (assume_affine_elems) then
      inv = identity_matrix(nnod) / this%global_local_volume_ratio()
    
    else
      call assert(allocated(this%chol_massmat),'inv_massmat: Chol. decomp. should be initialized')
      call assert(nnod <= size(this%chol_massmat,1),'inv_massmat: did not store this much')

      allocate(inv(nnod,nnod))
      call copy_lower_triangular_part(this%chol_massmat(:nnod,:nnod),inv)
      call chol2inv(inv,uplo='L',info=info)
      call assert(info==0,'inv_massmat: info /= 0 in LAPACK')
      call copy_lower2upper_triangle(inv)
    endif
  end function inv_massmat

  logical function is_boundary(this,only_fluid_mesh)
    class(face_list), intent(in) :: this
    logical, intent(in) :: only_fluid_mesh
    
    select case(this%ftype)
      case(BOUNDARY,INTERFACE_WITH_IGNORED);  is_boundary = .true.
      case(INTERFACE_SOLID_FLUID);            is_boundary = only_fluid_mesh
      case(INTERNAL);                         is_boundary = .false.
      case default;                           call error('Unknown face%ftype: '//int2char(this%ftype))
    end select
  end function
  
  logical function elem_is_neighbor1_for_face(this,elem) result(elem_is_neighbor1)
    class(face_list), intent(in) :: this
    type(elem_list), pointer, intent(in) :: elem
    
    elem_is_neighbor1 = associated(elem,this%elem_neighbors(1)%point)
    if (.not. elem_is_neighbor1) call assert(associated(elem,this%elem_neighbors(2)%point))
    if (this%ftype == BOUNDARY) call assert(elem_is_neighbor1)
  end function elem_is_neighbor1_for_face
  
  pure function avg_unit_normal_of_face(this) result(avg_unit_normal)
    class(face_list), intent(in) :: this
    real(dp), dimension(size(this%normal,1)) :: avg_unit_normal
    
    avg_unit_normal = matmul(this%normal,this%quot)
    avg_unit_normal = avg_unit_normal/norm2(avg_unit_normal)
  end function avg_unit_normal_of_face
  
  
  subroutine face_vertices(this,only_fluid_mesh,vertices)
    use local_elem, only: loc_idx_of_face_vertices_in_elem

    class(face_list), intent(in) :: this
    logical, intent(in) :: only_fluid_mesh
    type(vertex_ptr), allocatable, intent(out) :: vertices(:)

    integer :: side_of_smallest_nghb, face_loc_idx_in_nghb
    integer, allocatable, dimension(:) :: loc_idx_vtx_in_nghb
    integer :: v, nvert

    if (this%is_boundary(only_fluid_mesh)) then
      side_of_smallest_nghb = side_of_internal_nghb_of_bnd_face(this,only_fluid_mesh)
    elseif (this%elem_neighbors(1)%point%level >= this%elem_neighbors(2)%point%level) then
      side_of_smallest_nghb = 1
    else
      side_of_smallest_nghb = 2
    endif
    face_loc_idx_in_nghb = this%neighbor_loc_face_no(side_of_smallest_nghb)
    associate(nghb => this%elem_neighbors(side_of_smallest_nghb)%point)
      call loc_idx_of_face_vertices_in_elem(nghb%eltype,face_loc_idx_in_nghb,loc_idx_vtx_in_nghb)
      nvert = size(loc_idx_vtx_in_nghb)
      allocate(vertices(nvert))
      do v = 1, nvert
        vertices(v)%point => nghb%vertices(loc_idx_vtx_in_nghb(v))%point
      enddo
    end associate
  end subroutine face_vertices
  
  subroutine glob_coord_vertices_of_face(this,only_fluid_mesh,glob_coord)
    class(face_list), intent(in) :: this
    logical, intent(in) :: only_fluid_mesh
    real(dp), allocatable, intent(out) :: glob_coord(:,:)
    
    type(vertex_ptr), allocatable :: vertices(:)
    integer :: v
    
    call this%vertices(only_fluid_mesh,vertices)
    allocate(glob_coord(mesh_dimen,size(vertices)))
    do v = 1, size(vertices)
      glob_coord(:,v) = vertices(v)%point%coordinate%xyz(:mesh_dimen)
    enddo
  end subroutine glob_coord_vertices_of_face
  
  integer function side_of_internal_nghb_of_bnd_face(face,only_fluid_mesh) result(side)
    ! Returns the side of the only neighbor to be taken into account on a boundary face, i.e. the internal one.
    ! If the face is a geometrical boundary, there is only one neighbor.
    ! However, if the face is an interface, the internal neighbor will be the unique 
    ! neighbor that is fluid or to be considered.
    ! It can be the first or the second neighbor: we cannot know this a priori.
    type(face_list), intent(in) :: face
    logical, intent(in) :: only_fluid_mesh
    
    call assert(face%is_boundary(only_fluid_mesh))
    
    if (face%ftype == BOUNDARY) then
      side = 1
    elseif(face%ftype == INTERFACE_WITH_IGNORED) then
      call assert(face%elem_neighbors(1)%point%is_to_be_ignored() .neqv. face%elem_neighbors(2)%point%is_to_be_ignored())
      side = merge(2,1,face%elem_neighbors(1)%point%is_to_be_ignored())
    elseif (face%ftype == INTERFACE_SOLID_FLUID .and. only_fluid_mesh) then
      call assert(face%elem_neighbors(1)%point%is_fluid() .neqv. face%elem_neighbors(2)%point%is_fluid())
      side = merge(1,2,face%elem_neighbors(1)%point%is_fluid())
    else
      call error('Unknown boundary face type' )
    endif
  end function side_of_internal_nghb_of_bnd_face
  
  real(dp) function distance_of_face_from_elem_centroid(this,side,only_fluid_mesh) result(distance)
    !This subroutine evaluates the distance between a face
    !and the centroid of the neighboring element specified by "side".
    !
    !Since the centroid is evaluated WITHOUT taking into account the possible 
    !curvature of the element, only a face avg_unit_normal is considered.
    !There is room for improvement, but probably is not necessary.
    class(face_list), intent(in) :: this
    integer, intent(in) :: side
    logical, intent(in) :: only_fluid_mesh
    
    real(dp), allocatable :: face_vertices(:,:)
    real(dp), dimension(mesh_dimen) :: elem_centroid, vector, normal
    
    normal = this%avg_unit_normal()
    if (side == 2) normal = - normal
    
    !Distance = inner prod between the face normal and
    !the vector connecting the centroid and one face vertex
    elem_centroid = this%elem_neighbors(side)%point%avg_vtx()
    call this%glob_coord_vertices(only_fluid_mesh,face_vertices)
    vector = face_vertices(:,1) - elem_centroid
    distance = dot_product(normal,vector)
  end function distance_of_face_from_elem_centroid
  
  logical function same_vtx(vtx1,vtx2)
    ! Determines whether two vertices are the same based on their global coordinates
    type(vertex_list), intent(in) :: vtx1, vtx2
    
    associate(xyz_1 => vtx1%coordinate%xyz, xyz_2 => vtx2%coordinate%xyz)
      same_vtx = all(iseq(xyz_1,xyz_2))
    end associate
  end function same_vtx
end module mesh_objects

