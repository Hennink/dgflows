module monad_mod
  use assertions, only: assert
  use f90_kind, only: dp
  implicit none
  
  private
  public :: monad_t
  
  type, abstract :: monad_t
    logical :: use_finite_diff = .false.
  contains
    procedure(value_i), deferred :: value
    procedure :: deriv
  end type monad_t
  
  interface 
    impure elemental real(dp) function value_i(this,arg)
      use f90_kind, only: dp
      import :: monad_t
      implicit none
      class(monad_t), intent(in) :: this
      real(dp), intent(in) :: arg
    end function value_i
  end interface
  
contains
  real(dp) function deriv(this,arg)
    use math_const, only: SMALL

    class(monad_t), intent(in) :: this
    real(dp), intent(in) :: arg
    
    real(dp) :: h
    
    call assert(this%use_finite_diff,"monad_t only uses FD when explicitly told to")

    h = max(SMALL,abs(SMALL*arg))
    deriv = (this%value(arg+h) - this%value(arg-h)) / (2*h)
  end function deriv
end module monad_mod
