module rans_y_plus_mod
  use, intrinsic :: ieee_arithmetic, only: ieee_value, IEEE_QUIET_NAN
  use exceptions, only: warning
  use f90_kind, only: dp
  use gmsh_interface, only: write_to_gmsh_file
  use local_global_mappings, only: gcors2pnt_set
  use mesh_objects, only: elem_list, elem_ptr, face_list, face_ptr, side_of_internal_nghb_of_bnd_face
  use mesh_type, only: fem_mesh
  use numerical_solutions, only: matprop_set, velocity
  use petsc_mod, only: no_petsc_procs
  use pnt_set_mod, only: pnt_set_t
  use rans_mod, only: rans_model
  use rans_wall_functions_mod, only: eval_u_k
  use run_data, only: yplus_bnd_ids, yplus_nghb_depth, div_free_velocity
  use scal_func_mod, only: scal_func_type
  use scalfunc_from_proc_mod, only: as_scalfunc
  use wall_dist_mod, only: wall_dist_func
  use support, only: axpy_own_trace_x_I
  implicit none
  
  private
  public :: write_yplus
  
contains
  subroutine write_yplus(mesh)
    type(fem_mesh), intent(in) :: mesh
    
    if (mesh%has_been_refined() .and. no_petsc_procs() > 1) then
      ! TODO: understand why it does not work
      call warning('For some reason, write_yplus does not work in parallel for refined meshes. Skip it.')
      return
    else
      call write_to_gmsh_file(as_scalfunc(y_plus),mesh,'num-yplus',only_fluid=.true.,time=1)
    endif
  end subroutine write_yplus
  
  subroutine y_plus(elem,pnt_set,time,values)
    use qnty_handler_mod, only: max_sol_order
    
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    type(pnt_set_t) :: centroid_pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    logical, parameter :: ONLY_FLUID = .true.
    class(scal_func_type), pointer :: nu_molec
    real(dp) :: y(1), nu(1), u_tau
    logical :: consider_elem
    
    call get_elem_max_u_tau(elem,ONLY_FLUID,yplus_nghb_depth,yplus_bnd_ids,time,consider_elem,u_tau)
    if (consider_elem) then
      nu_molec => matprop_set%func_ptr('kinematic_viscosity')
      call gcors2pnt_set(elem,max_sol_order(elem),reshape(elem%avg_vtx(),[elem%dimen(),1]),centroid_pnt_set)
      call wall_dist_func%get_values(elem,centroid_pnt_set,time,y)
      call nu_molec%get_values(elem,centroid_pnt_set,time,nu)
      values = u_tau * max(0.0_dp,y(1)) / nu(1)
    else
      values = ieee_value(1.0_dp,IEEE_QUIET_NAN)
    endif
  end subroutine y_plus
  
  recursive subroutine get_elem_max_u_tau(elem,only_fluid_mesh,nghb_depth,bnd_ids,time,near_bnd,max_u_tau)
    class(elem_list), intent(in) :: elem
    logical, intent(in) :: only_fluid_mesh
    integer, intent(in) :: nghb_depth, bnd_ids(:), time
    logical, intent(out) :: near_bnd
    real(dp), intent(out) :: max_u_tau
    
    class(scal_func_type), pointer :: mu_func, density
    type(elem_ptr), allocatable :: active_neighbors(:)
    type(face_ptr), allocatable :: active_subfaces(:)
    type(face_list), pointer :: face
    real(dp), allocatable :: mu(:), rho(:), u_k(:), gder_u(:,:,:)
    real(dp) :: n_dot_stress(elem%dimen()), tensor(elem%dimen(),elem%dimen()), wall_stress
    integer :: i, p, side
    
    mu_func => matprop_set%func_ptr('dynamic_viscosity')
    density => matprop_set%func_ptr('density')
    
    if (elem%is_at_bnd(only_fluid_mesh,bnd_ids)) then
      near_bnd = .true.
      call elem%active_subfaces(active_subfaces,active_neighbors)
      max_u_tau = - huge(1.0_dp)
      do i = 1, size(active_subfaces)
        face => active_subfaces(i)%point
        if (face%is_boundary(only_fluid_mesh) .and. findloc(bnd_ids,face%bc_id,dim=1) > 0) then
          side = side_of_internal_nghb_of_bnd_face(face,only_fluid_mesh)
          if (rans_model%is_low_Re()) then
            allocate(mu(face%nqp),rho(face%nqp))
            allocate(gder_u(elem%dimen(),elem%dimen(),face%nqp))
            mu = mu_func%values_at_qps(face,side,time)
            rho = density%values_at_qps(face,side,time)
            call velocity%gders_at_qps(face,side,time,gder_u)
            do p = 1, face%nqp
              ! Full stress tensor 
              tensor = mu(p) * (gder_u(:,:,p) + transpose(gder_u(:,:,p)))
              if (.not.div_free_velocity) call axpy_own_trace_x_I(tensor,-1.0_dp/3.0_dp)
              ! force in all directions, acting on boundary face
              n_dot_stress =  matmul(face%normal(:,p),tensor)
              ! tangential component of above force
              wall_stress = sqrt( norm2(n_dot_stress)**2 - dot_product(face%normal(:,p),n_dot_stress)**2)
              max_u_tau = max(max_u_tau,sqrt(wall_stress/rho(p)))
            enddo
            deallocate(mu,rho,gder_u)
          else
            allocate(u_k(face%nqp))
            call eval_u_k(face,side,time,u_k)
            max_u_tau = max(max_u_tau,maxval(u_k))
            deallocate(u_k)
          endif
          return
        endif
      enddo
    elseif (nghb_depth == 0) then
      near_bnd = .false.
    else
      call elem%active_subfaces(active_subfaces,active_neighbors)
      do i = 1, size(active_subfaces)
        face => active_subfaces(i)%point
        if (face%is_boundary(only_fluid_mesh)) then
          ! We are on a boundary element, but not with the ID that we want
           near_bnd = .false.
        else
          call get_elem_max_u_tau(active_neighbors(i)%point,only_fluid_mesh,nghb_depth-1,bnd_ids,time,near_bnd,max_u_tau)
        endif
        if (near_bnd) return
      enddo
    endif
  end subroutine get_elem_max_u_tau
end module rans_y_plus_mod

