submodule(matprop_mod) matprop__lib_metal
  use f90_kind, only: dp
  implicit none
  
  real(dp) :: density, cp, thermal_conductivity
  
contains
  module subroutine get_metal_matprop(unit_number,matprop_set)
    use func_set_expansion_mod, only: func_set_expansion_t
    use functions_lib, only: const_func
    
    integer, intent(in) :: unit_number
    type(func_set_expansion_t), intent(out) :: matprop_set
    
    integer :: ios
    character(200) :: iomsg
    namelist /reference_values/ density, cp, thermal_conductivity
    
    density = 7960.0_dp
    cp = 450.0_dp
    thermal_conductivity = 24.0_dp
    read(unit_number,reference_values,iostat=ios,iomsg=iomsg)
    call assert(ios == 0,'could not read matprop reference point; iomsg=' // trim(iomsg))
    
    call matprop_set%add_known_function('density',const_func(density))
    call matprop_set%add_known_function('thermal_conductivity',const_func(thermal_conductivity))
    call matprop_set%add_known_function('thermal_diffusivity',const_func(thermal_conductivity/(density*cp)))
    call matprop_set%add_known_function('spec_heat_capacity',const_func(cp))
    call matprop_set%add_known_function('k_over_cp',const_func(thermal_conductivity/cp))

    call matprop_set%add_func('temperature','spec_enthalpy',T2spec_h)
    call matprop_set%add_func('spec_enthalpy','temperature',spec_h2T)

    call matprop_set%finalize()
  end subroutine get_metal_matprop
  
  pure real(dp) function T2spec_h(T) result(spec_h)
    real(dp), intent(in) :: T

    spec_h = cp * T
  end function T2spec_h

  pure real(dp) function spec_h2T(spec_h) result(T)
    real(dp), intent(in) :: spec_h
    
    T = spec_h / cp
  end function spec_h2T
end submodule matprop__lib_metal

