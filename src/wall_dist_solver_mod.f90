module wall_dist_solver_mod
  use qnty_solver_mod, only: qnty_solver_t
  use scal_func_mod, only: scal_func_type
  implicit none
  
  private
  public :: wall_dist_solver_t
  public :: p_param
  
  integer :: p_param
  
  type, extends(qnty_solver_t) :: wall_dist_solver_t
  contains
    procedure, non_overridable :: get_qnty_solvec
    procedure, non_overridable :: collect_pde_varfrozconst
  end type wall_dist_solver_t
  
contains
  function get_qnty_solvec(this) result(solvec_ptr)
    use numerical_solutions, only: get_solvec
    use solvec_mod, only: solvec_t
    
    class(wall_dist_solver_t), intent(in) :: this
    type(solvec_t), pointer :: solvec_ptr
    
    solvec_ptr => get_solvec('wall_dist')
  end function get_qnty_solvec
  
  subroutine collect_pde_varfrozconst(this, variable_terms, frozen_terms, constant_terms)
    use f90_kind, only: dp
    use functions_lib, only: const_func
    use fick_sip_mod, only: sip_ficks_law_t
    use pde_mod, only: pde_t
    use scalfunc_from_proc_mod, only: as_scalfunc
    use source_pde_term_mod, only: source_t
    
    class(wall_dist_solver_t), intent(in) :: this
    type(pde_t), intent(out) :: variable_terms, frozen_terms, constant_terms
    
    type(sip_ficks_law_t) :: sip
    type(source_t) :: walldist_src_term
    
    sip%diff_param = as_scalfunc(wall_dist_diff_coeff)
    sip%explicit = .false.
    call variable_terms%add_term(sip)
    
    allocate(walldist_src_term%src_all_dirs,source=const_func(1.0_dp))
    call constant_terms%add_term(walldist_src_term)
  end subroutine
  
  subroutine wall_dist_diff_coeff(elem,pnt_set,time,values)
    use f90_kind, only: dp
    use mesh_objects, only: elem_list
    use pnt_set_mod, only: pnt_set_t
    use numerical_solutions, only: num_scalfunc_set
    
    type(elem_list), intent(in) :: elem
    type(pnt_set_t), intent(in) :: pnt_set
    integer, intent(in) :: time
    real(dp), intent(out) :: values(:)
    
    class(scal_func_type), allocatable :: wall_dist_func
    real(dp), dimension(elem%dimen(),size(values)) :: dist_grad
    
    call num_scalfunc_set%get_func('wall_dist',wall_dist_func)
    call wall_dist_func%get_gders(elem,pnt_set,time,dist_grad)
    values = norm2(dist_grad,1)**(p_param-2)
  end subroutine wall_dist_diff_coeff
end module wall_dist_solver_mod
