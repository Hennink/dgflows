module tvd_mod
  implicit none
  
contains
  subroutine limiter(v,u_limited,N,dx)
    implicit none
    double precision :: v(:),u_limited(:),dx
    integer :: N
    
    double precision :: mean,slope,mean_l,mean_r,slope_l,slope_r,slope_lim
    integer :: elem,index_l,index_r
    
    do elem=1,N
      index_l = 2*elem-1
      index_r = 2*elem
      mean  = (v(index_r) + v(index_l))/2.d0
      slope = (v(index_r) - v(index_l))/dx
      
      if (elem /= 1) then
        index_l = 2*(elem-1)-1
        index_r = 2*(elem-1)
        mean_l  = (v(index_r) + v(index_l))/2.d0
        slope_l = (mean - mean_l)/dx
      endif
      
      if (elem /= N) then
        index_l = 2*(elem+1)-1
        index_r = 2*(elem+1)
        mean_r  = (v(index_r) + v(index_l))/2.d0
        slope_r = (mean_r - mean)/dx
      endif
      
      if (elem == 1) then
        slope_lim = minmod(slope,slope_r)
      elseif (elem == N) then
        slope_lim = minmod(slope,slope_l)
      else
        slope_lim = minmod(slope,slope_l,slope_r)
      endif
      
    ! Copy limited solution to u_limited
      index_l = 2*elem-1
      index_r = 2*elem
      u_limited(index_l) = mean - slope_lim * (dx/2.d0)
      u_limited(index_r) = mean + slope_lim * (dx/2.d0)
    enddo
  end subroutine limiter
  
  
  double precision function minmod(a1,a2,a3)
    implicit none
    
    double precision :: a1,a2
    double precision, optional :: a3
    
    double precision, allocatable :: a_array(:)
    logical :: all_same_sign
    
    ! Determine whether signs are all equal or not
    all_same_sign = .true.
    if (get_sign(a2) /= get_sign(a1)) then
      all_same_sign = .false.
    endif
    if (present(a3)) then
      if (get_sign(a3) /= get_sign(a1)) then
        all_same_sign = .false.
      endif
    endif
    
    ! Evaluate minmod function
    if (all_same_sign) then
      if (present(a3)) then
        allocate(a_array(3))
        a_array(1) = a1
        a_array(2) = a2
        a_array(3) = a3
      else
        allocate(a_array(2))
        a_array(1) = a1
        a_array(2) = a2
      endif
      minmod = get_sign(a1) * minval(abs(a_array))
    else
      minmod = 0.d0
    endif
  end function minmod
  
  
  double precision function get_sign(x)
    implicit none
    
    double precision :: x
    
    if (x > 0.d0) then
      get_sign = + 1.d0
    elseif (x < 0.d0) then
      get_sign = - 1.d0
    else
      get_sign =   0.d0
    endif
  end function get_sign
end module tvd_mod
