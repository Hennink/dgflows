program tvd_convection
  ! Solve u_t + d/dx (speed * u) = 0
  ! Use implicit DG scheme with p=1
  use tvd_mod
  implicit none
  
  logical, parameter :: TVD=.true.
  integer, parameter :: N=100
  integer, parameter :: no_inners=100
  double precision, parameter :: speed=1.0d0
  double precision, parameter :: u_inlet=0.0d0
  double precision, parameter :: L=1.0d0
  double precision, parameter :: dt=(L/N)/4.0d0
  integer :: no_dof,elem,info,start_row,end_row,step,row,col,index_l,index_r,index,inner
  double precision, allocatable :: A(:,:),b(:),u(:),u_old(:),u_star(:),u_star_limited(:),delta_u_star(:)
  integer, allocatable :: ipiv(:)
  double precision :: M(2,2) ! Mass matrix
  double precision :: C(2,2) ! Conv matrix
  double precision :: A_elem(2,2) ! Elem matrix
  double precision :: b_elem(2) ! Elem rhs
  double precision :: u_old_elem(2),delta_u_star_elem(2)
  double precision :: dx,x_left,x_right
  
  dx = L/N
  no_dof = 2*N
  
  allocate(A(no_dof,no_dof))
  allocate(b(no_dof))
  allocate(u_old(no_dof))
  allocate(u(no_dof))
  allocate(u_star(no_dof))
  allocate(u_star_limited(no_dof))
  allocate(delta_u_star(no_dof))
  allocate(ipiv(no_dof))
  
  ! M(i,j) = int_0^h h_i * h_j dx
  M(1,1) = dx/3.d0
  M(1,2) = dx/6.d0
  M(2,1) = dx/6.d0
  M(2,2) = dx/3.d0
  
  ! C(i,j) = int_0^h h_i' * h_j dx
  C(1,1) = - 0.5d0
  C(1,2) = - 0.5d0
  C(2,1) = + 0.5d0
  C(2,2) = + 0.5d0
  
  ! Init
  u = 0.d0
  u_old = 0.d0
  do elem=11,20
    index_l = 2*elem-1
    index_r = 2*elem
    u_old(index_l) = 1.d0
    u_old(index_r) = 1.d0
    u(index_l) = 1.d0
    u(index_r) = 1.d0
  enddo
  
  do step=1,10
    do inner=1,no_inners
      A = 0.d0
      b = 0.d0
      
      ! Init u_star
      u_star = u
      call limiter(u_star,u_star_limited,N,dx)
      delta_u_star = u_star_limited - u_star
      
      ! Assembly of A and b
      do elem=1,N
        start_row = 2*elem - 1
        end_row   = 2*elem
        u_old_elem = u_old(start_row:end_row)
        delta_u_star_elem = delta_u_star(start_row:end_row)
        
        A_elem = 0.d0
        b_elem = 0.d0
        
        ! Time derivative
        A_elem = M/dt
        b_elem = matmul(M,u_old_elem)/dt
        
        ! Volumetric convection term
        A_elem = A_elem - speed*C
        if (TVD) then
          b_elem = b_elem + speed * matmul(C,delta_u_star_elem)
        endif
        
        ! Left flux
        if (elem==1) then
          b_elem(1) = b_elem(1) + speed*u_inlet ! inflow
        else
          row = 2*elem-1
          col = 2*(elem-1)
          A(row,col) = A(row,col) - speed
          if (TVD) then
            index = col
            b_elem(1) = b_elem(1) + speed * delta_u_star(index)
          endif
        endif
        
        ! Right flux (outflow)
        A_elem(2,2) = A_elem(2,2) + speed
        if (TVD) then
          row = 2*elem
          index = row
          b_elem(2) = b_elem(2) - speed * delta_u_star(index)
        endif
        
        ! Assemble into complete A and b
        A(start_row:end_row,start_row:end_row) = A(start_row:end_row,start_row:end_row) + A_elem
        b(start_row:end_row) = b(start_row:end_row) + b_elem
      enddo
      
      ! Solve the lin system
      call dgesv(no_dof,1,A,no_dof,ipiv,b,no_dof,info)
      u = b
      if (info/=0) then
        print *,'info =',info
        STOP 'Solver problem'
      endif
    enddo
    
    ! Prepare for next step
    u_old = u
  enddo
  
  ! Write solution
  print *
  print *,'Final solution'
  do elem=1,N
    x_left  = (elem-1)*dx + 0.0001d0
    x_right = elem*dx     - 0.0001d0
    print *,x_left,u(2*elem-1)
    print *,x_right,u(2*elem)
  enddo
end program tvd_convection