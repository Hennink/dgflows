module support
  use sharevars
  implicit none
  
contains
  real(dp) function penalty(edge,equation)
    integer, intent(in) :: edge
    character(*), intent(in) :: equation
    
    penalty = penalty_safety_factor * IP_over_length_scale(edge,equation) * diff_scale(edge,equation)
  end function penalty
    
  real(dp) function IP_over_length_scale(edge,equation) result(IP_over_L)
    integer, intent(in) :: edge
    character(*), intent(in) :: equation
    
    real(dp) :: IP_factor
    
    select case(equation)
      case('velocity')
        IP_factor = real((order_u + 1)**2,dp)
      case default
        error stop 'IP_over_length_scale: unknown equation'
    end select
    
    if (edge == 1) then
      IP_over_L = 1.0_dp/dy(edge)
    elseif (edge == no_edges) then
      IP_over_L = 1.0_dp/dy(edge-1)
    else
      IP_over_L = max(1.0_dp/dy(edge-1),1.0_dp/dy(edge))
      IP_over_L = IP_over_L / 2.0_dp
    endif
    IP_over_L = IP_over_L * IP_factor
  end function IP_over_length_scale
  
  real(dp) function diff_scale(edge,equation) result(diff)
    integer, intent(in) :: edge
    character(*), intent(in) :: equation
    
    if (edge == 1) then
      diff = nu_t_edge(edge,1)
    elseif (edge == no_edges) then
      diff = nu_t_edge(edge-1,2)
    else
      diff = max(nu_t_edge(edge,1),nu_t_edge(edge-1,2))
    endif
    
    select case(equation)
      case('velocity')
        diff = nu_lam + diff
      case default
        error stop 'diff_scale: unknown equation'
    end select
  end function diff_scale
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine calculate_vol_integral_f(equation,elem,int_f,weight_at_qps)
    character(*), intent(in) :: equation
    integer, intent(in) :: elem
    real(dp), allocatable, intent(out) :: int_f(:)
    real(dp), optional, intent(in) :: weight_at_qps(size(qps))
    
    real(dp) :: Jac, quot
    integer :: p, nod, nnod, order
    
    select case(equation)
      case('velocity')
        nnod = nnod_u
        order = order_u
      case default
        error stop 'calculate_vol_integrals: unknown equation'
    end select
    
    Jac = dy(elem) / 2.0_dp
    
    allocate(int_f(nnod))
    int_f = 0.0_dp
    do p = 1, size(qps)
      if (present(weight_at_qps)) then
        quot = weight_at_qps(p) * qws(p)
      else
        quot =  qws(p)
      endif
      do nod = 1, nnod
        ! Volumetric integral f(i) = \int_elem f_i
        int_f(nod) = int_f(nod) + shape_fun(order,nod,qps(p)) * quot * Jac
      enddo
    enddo
  end subroutine calculate_vol_integral_f
  
  subroutine calculate_vol_integral_fxf(equation,elem,int_fxf,weight_at_qps)
    character(*), intent(in) :: equation
    integer, intent(in) :: elem
    real(dp), allocatable, intent(out) :: int_fxf(:,:)
    real(dp), optional, intent(in) :: weight_at_qps(size(qps))
    
    real(dp) :: Jac, quot
    integer :: p, nod, nod2, nnod, order
    
    select case(equation)
      case('velocity')
        nnod = nnod_u
        order = order_u
      case default
        error stop 'calculate_vol_integral_fxf: unknown equation'
    end select
    
    Jac = dy(elem) / 2.0_dp
    
    allocate(int_fxf(nnod,nnod))
    int_fxf = 0.0_dp
    do p = 1, size(qps)
      if (present(weight_at_qps)) then
        quot = weight_at_qps(p) * qws(p)
      else
        quot =  qws(p)
      endif
      do nod = 1, nnod
        do nod2 = 1, nnod
          ! Volumetric integral f_x_f(i,j) = \int_elem f_i*f_j
          int_fxf(nod,nod2) = int_fxf(nod,nod2) + shape_fun(order,nod,qps(p)) * shape_fun(order,nod2,qps(p)) * quot * Jac
        enddo
      enddo
    enddo
  end subroutine calculate_vol_integral_fxf
  
  subroutine calculate_vol_integral_gradfxgradf(equation,elem,weight_at_qps,int_wxgradfxgradf)
    character(*), intent(in) :: equation
    integer, intent(in) :: elem
    real(dp), intent(in) :: weight_at_qps(size(qps))
    real(dp), allocatable, intent(out) :: int_wxgradfxgradf(:,:)
    
    real(dp) :: Jac, quot
    integer :: p, nod, nod2, nnod, order
    
    select case(equation)
      case('velocity')
        nnod = nnod_u
        order = order_u
      case default
        error stop 'calculate_vol_integral_gradfxgradf: unknown equation'
    end select
    
    Jac = dy(elem) / 2.0_dp
    
    allocate(int_wxgradfxgradf(nnod,nnod))
    int_wxgradfxgradf = 0.0_dp
    do p = 1, size(qps)
      quot = weight_at_qps(p) * qws(p)
      do nod = 1, nnod
        do nod2 = 1, nnod
          ! Volumetric integral gradf_x_gradf(i,j)= \int_elem (grad f)_i*(grad f)_j
          int_wxgradfxgradf(nod,nod2) = int_wxgradfxgradf(nod,nod2) + &
                                   grad_shape_fun(order,nod,qps(p)) * grad_shape_fun(order,nod2,qps(p)) * quot / Jac
        enddo
      enddo
    enddo
  end subroutine calculate_vol_integral_gradfxgradf
  
  subroutine calculate_face_integrals(equation,edge,int_edge_f_right,int_edge_gradf_right,int_edge_fxf_left,int_edge_fxf_right,&
                                               int_edge_fxf_mixed,int_edge_fxgradf_left,int_edge_fxgradf_right,&
                                               int_edge_fxgradf_mixed,int_edge_gradfxf_mixed)
    character(*), intent(in) :: equation
    integer, intent(in) :: edge
    real(dp), allocatable, intent(out) :: int_edge_f_right(:),int_edge_gradf_right(:),int_edge_fxf_left(:,:), &
                             int_edge_fxf_right(:,:), int_edge_fxf_mixed(:,:), int_edge_fxgradf_left(:,:), &
                             int_edge_fxgradf_right(:,:), int_edge_fxgradf_mixed(:,:), int_edge_gradfxf_mixed(:,:)
    
    integer :: elem, order, nnod, nod, nod2
    real(dp) :: Jac
    
    if (equation == 'velocity') then
      nnod = nnod_u
      order = order_u
    else
      error stop 'calculate_face_integrals: unknown equation'
    endif
    
    allocate(int_edge_gradf_right(nnod),int_edge_f_right(nnod),int_edge_fxf_left(nnod,nnod),int_edge_fxf_right(nnod,nnod), &
             int_edge_fxf_mixed(nnod,nnod), int_edge_fxgradf_left(nnod,nnod), &
             int_edge_fxgradf_right(nnod,nnod), int_edge_fxgradf_mixed(nnod,nnod), &
             int_edge_gradfxf_mixed(nnod,nnod))
    
    ! Calculate the edge integral \int_edge f_i (i on element right of edge)
    if (edge < no_edges) then
      do nod = 1, nnod
        int_edge_f_right(nod) = shape_fun(order,nod,-1.0_dp)
      enddo
    else
      int_edge_f_right = 0.0_dp
    endif
    
    ! Calculate the edge integral \int_edge grad f_i (i on element right of edge)
    if (edge < no_edges) then
      elem = edge
      Jac = dy(elem) / 2.0_dp
      do nod = 1, nnod
        int_edge_gradf_right(nod) = grad_shape_fun(order,nod,-1.0_dp) / Jac
      enddo
    else
      int_edge_gradf_right = 0.0_dp
    endif
    
    ! Calculate the edge integral \int_edge f_i*f_j (both i and j on element left of edge)
    if (edge > 1) then
      do nod = 1, nnod
        do nod2 = 1, nnod
          int_edge_fxf_left(nod,nod2) = shape_fun(order,nod,1.0_dp) * shape_fun(order,nod2,1.0_dp)
        enddo
      enddo
    else
      int_edge_fxf_left = 0.0_dp
    endif
    
    ! Calculate the edge integral \int_edge f_i*f_j (both i and j on element right of edge)
    if (edge < no_edges) then
      do nod = 1, nnod
        do nod2 = 1, nnod
          int_edge_fxf_right(nod,nod2) = shape_fun(order,nod,-1.0_dp) * shape_fun(order,nod2,-1.0_dp)
        enddo
      enddo
    else
      int_edge_fxf_right = 0.0_dp
    endif
    
    ! Calculate the edge integral \int_edge f_i*f_j (i-th entry for element left, 
    !                                                j-th entry for element right of edge)
    if (edge > 1 .and. edge < no_edges) then
      do nod = 1, nnod
        do nod2 = 1, nnod
          int_edge_fxf_mixed(nod,nod2) = shape_fun(order,nod,1.0_dp) * shape_fun(order,nod2,-1.0_dp)
        enddo
      enddo
    else
      int_edge_fxf_mixed = 0.0_dp
    endif
    
    ! Calculate the edge integral \int_edge f_i*grad f_j (both i and j on element left of edge)
    ! One sqrt originates from the gradient; the other from the area
    if (edge > 1) then
      elem = edge - 1
      Jac = dy(elem) / 2.0_dp
      do nod = 1, nnod
        do nod2 = 1, nnod
          int_edge_fxgradf_left(nod,nod2) = shape_fun(order,nod,1.0_dp) * grad_shape_fun(order,nod2,1.0_dp) / Jac
        enddo
      enddo
    else
      int_edge_fxgradf_left = 0.0_dp
    endif
    
    ! Calculate the edge integral \int_edge f_i*grad f_j (both i and j on element right of edge)
    ! One sqrt originates from the gradient; the other from the area
    if (edge < no_edges) then
      elem = edge
      Jac = dy(elem) / 2.0_dp
      do nod = 1, nnod
        do nod2 = 1, nnod
          int_edge_fxgradf_right(nod,nod2) = shape_fun(order,nod,-1.0_dp) * grad_shape_fun(order,nod2,-1.0_dp) / Jac
        enddo
      enddo
    else
      int_edge_fxgradf_right = 0.0_dp
    endif
    
    ! Calculate the edge integral \int_edge f_i*grad f_j (i on element left and j on element right of edge)
    ! One sqrt originates from the gradient; the other from the area
    if (edge > 1 .and. edge < no_edges) then
      elem = edge
      Jac = dy(elem) / 2.0_dp
      do nod = 1, nnod
        do nod2 = 1, nnod
          int_edge_fxgradf_mixed(nod,nod2) = shape_fun(order,nod,1.0_dp) * grad_shape_fun(order,nod2,-1.0_dp) / Jac
        enddo
      enddo
    else
      int_edge_fxgradf_mixed = 0.0_dp
    endif
    
    ! Calculate the edge integral \int_edge grad f_i*f_j (i on element left and j on element right of edge)
    ! One sqrt originates from the gradient; the other from the area
    if (edge > 1 .and. edge < no_edges) then
      elem = edge - 1
      Jac = dy(elem) / 2.0_dp
      do nod = 1, nnod
        do nod2 = 1, nnod
          int_edge_gradfxf_mixed(nod,nod2) = grad_shape_fun(order,nod,1.0_dp) * shape_fun(order,nod2,-1.0_dp) / Jac
        enddo
      enddo
    else
      int_edge_gradfxf_mixed = 0.0_dp
    endif
  end subroutine calculate_face_integrals
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine compute_solution_at_elem_qps(equation,elem,sol_at_qps)
    character(*), intent(in) :: equation
    integer, intent(in) :: elem
    real(dp), intent(out) :: sol_at_qps(size(qps))
    
    integer :: p
    
    do p = 1, size(qps)
      call compute_solution_at_lcor(equation,elem,qps(p),value=sol_at_qps(p))
    enddo
  end subroutine compute_solution_at_elem_qps
  
  subroutine compute_grad_solution_at_elem_qps(equation,elem,gradsol_at_qps)
    character(*), intent(in) :: equation
    integer, intent(in) :: elem
    real(dp), intent(out) :: gradsol_at_qps(size(qps))
    
    integer :: p
    
    do p = 1, size(qps)
      call compute_solution_at_lcor(equation,elem,qps(p),grad=gradsol_at_qps(p))
    enddo
  end subroutine compute_grad_solution_at_elem_qps
  
  subroutine compute_solution_at_gcors(equation,gcors,values,grads)
    character(*), intent(in) :: equation
    real(dp), intent(in) :: gcors(:)
    real(dp), allocatable, optional, intent(out) :: values(:)
    real(dp), allocatable, optional, intent(out) :: grads(:)
    
    integer :: p
    real(dp), allocatable :: lcors(:)
    integer, allocatable :: elems(:)
    
    allocate(lcors(size(gcors)),elems(size(gcors)))
    if (present(values)) allocate(values(size(gcors)))
    if (present(grads)) allocate(grads(size(gcors)))
    
    call convert_gcor2lcor(gcors,elems,lcors)
    
    do p = 1, size(gcors)
      if (present(values)) call compute_solution_at_lcor(equation,elems(p),lcors(p),value=values(p))
      if (present(grads)) call compute_solution_at_lcor(equation,elems(p),lcors(p),grad=grads(p))
    enddo
  end subroutine compute_solution_at_gcors
  
  subroutine compute_solution_at_lcor(equation,elem,lcor,value,grad)
    character(*), intent(in) :: equation
    integer, intent(in) :: elem
    real(dp), intent(in) :: lcor
    real(dp), optional, intent(out) :: value
    real(dp), optional, intent(out) :: grad
    
    integer :: index, locnode, order, nnod
    real(dp) :: Jac
    integer, pointer :: dofs(:,:) => null()
    real(dp), pointer :: coeffs(:) => null()
    
    select case(equation)
      case('velocity')
        nnod = nnod_u
        order = order_u
        dofs => dofs_u
        coeffs => u
      case default
        error stop 'compute_solution_at_lcor: unknown equation'
    end select
    
    Jac = dy(elem) / 2.0_dp

    if (present(value)) value = 0.0_dp
    if (present(grad)) grad = 0.0_dp
    do locnode = 1, nnod
      index = dofs(locnode,elem)
      if (present(value)) value = value + coeffs(index)*shape_fun(order,locnode,lcor)
      if (present(grad)) then
        grad = grad + coeffs(index)*grad_shape_fun(order,locnode,lcor) / Jac
      endif
    enddo
  end subroutine compute_solution_at_lcor
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  real(dp) function solution_max_ddt(equation) result(max_ddt)
    character(*), intent(in) :: equation
    
    integer :: nnod, order, elem, locnode, index
    real(dp), pointer :: coeffs(:) => null()
    real(dp), pointer :: coeffs_old(:) => null()
    integer, pointer :: dofs(:,:) => null()
    real(dp), allocatable :: f(:)
    real(dp) :: int_diff, diff_coeff
    
    select case(equation)
      case('velocity')
        nnod = nnod_u
        order = order_u
        dofs => dofs_u
        coeffs => u
        coeffs_old => u_old
      case default
        error stop 'solution_max_ddt: unknown equation'
    end select
    
    max_ddt = 0.0_dp
    do elem = 1, no_elem
      call calculate_vol_integral_f(equation,elem,f)
      int_diff = 0.0_dp
      do locnode = 1, nnod
        index = dofs(locnode,elem)
        diff_coeff = coeffs(index) - coeffs_old(index)
        int_diff = int_diff + diff_coeff * f(locnode)
      enddo
      max_ddt = max(max_ddt,abs(int_diff)/dy(elem))
    enddo
  end function solution_max_ddt
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine init_solution_to_value(equation,value,sol)
    character(*), intent(in) :: equation
    real(dp), intent(in) :: value
    real(dp), intent(out) :: sol(:)
    
    integer :: nnod, order, elem, info
    real(dp), pointer :: coeffs(:)
    integer, pointer :: dofs(:,:)
    real(dp), allocatable :: value_x_f(:), fxf(:,:)
    real(dp), allocatable :: rhs(:), M(:,:), ipiv(:)
    real(dp) :: weight_at_qps(size(qps))
    integer :: row_1st, col_1st, row_end, col_end
    
    select case(equation)
      case('velocity')
        nnod = nnod_u
        order = order_u
        dofs => dofs_u
        coeffs => u
      case default
        error stop 'init_solution_to_value: unknown equation'
    end select
    
    allocate(rhs(size(coeffs)),source=0.0_dp)
    allocate(ipiv(size(coeffs)),source=0.0_dp)
    allocate(M(size(coeffs),size(coeffs)),source=0.0_dp)
    
    do elem = 1, no_elem
      row_1st = nnod*(elem-1)+1
      row_end = nnod*elem
      col_1st = nnod*(elem-1)+1
      col_end = nnod*elem
      
      ! Mass matrix
      call calculate_vol_integral_fxf(equation,elem,fxf)
      M(row_1st:row_end,col_1st:col_end) = M(row_1st:row_end,col_1st:col_end) + fxf
      
      ! rhs
      weight_at_qps = value
      call calculate_vol_integral_f(equation,elem,value_x_f,weight_at_qps)
      rhs(row_1st:row_end) = rhs(row_1st:row_end) + value_x_f
    enddo
    
    ! Call solver
    call dgesv(size(coeffs),1,M,size(coeffs),ipiv,rhs,size(coeffs),info)
    if (info /= 0) error stop 'init_solution_to_value: WARNING in solver'
    sol = rhs
  end subroutine init_solution_to_value
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine convert_gcor2lcor(gcors,elems,lcors)
    real(dp), intent(in) :: gcors(:)
    real(dp), intent(out) :: lcors(:)
    integer, intent(out) :: elems(:)
    
    integer :: p, elem
    logical :: elem_found
    
    if (size(lcors) /= size(gcors)) error stop 'convert_gcor2lcor: sizes of gcors and lcors not compatible'
    if (size(elems) /= size(gcors)) error stop 'convert_gcor2lcor: sizes of gcors and elems not compatible'
    
    do p = 1, size(gcors)
      ! Check if gcor is in the domain
      if (gcors(p) < 0.0_dp-SMALL .or. gcors(p) > H/2.0_dp+SMALL) error stop 'convert_gcor2lcor: gcor is out of domain'
      
      ! First: find the element where the gcor lies
      elem = 0
      elem_found = .false.
      do
        elem = elem + 1
        if (gcors(p) >= y_coord(elem)-SMALL .and. gcors(p) <= y_coord(elem+1)+SMALL) elem_found = .true.
        if (elem_found) exit
      enddo
      if (elem < 1 .or. elem > no_elem) error stop 'convert_gcor2lcor: problem in finding the element for at least one gcor'
      elems(p) = elem
      
      ! Second: convert gcor into lcor
      lcors(p) = 2.0_dp*gcors(p)/dy(elem) - (y_coord(elem+1)+y_coord(elem))/dy(elem)
    enddo
  end subroutine convert_gcor2lcor
  
  
  subroutine convert_lcor2gcor(lcor,elem,gcor)
    real(dp), intent(in) :: lcor
    integer, intent(in) :: elem
    real(dp), intent(out) :: gcor
    
    ! Convert lcor into gcor
    gcor = 0.5_dp*(1.0_dp - lcor)*y_coord(elem) + 0.5_dp*(1.0_dp + lcor)*y_coord(elem+1)
    
    ! Check if gcor is in the domain
    if (gcor < 0.0_dp-SMALL .or. gcor > H/2.0_dp+SMALL) error stop 'convert_lcor2gcor: gcor is out of domain'
  end subroutine convert_lcor2gcor
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  function shape_fun(order,locnode,xi)
    integer, intent(in) :: order
    real(dp), intent(in) :: xi
    integer, intent(in) :: locnode
    real(dp) :: shape_fun
    
    if (order == 1) then
      if (locnode == 1) then
        shape_fun = 0.5_dp*(1.0_dp - xi)
      elseif (locnode == 2) then
        shape_fun = 0.5_dp*(1.0_dp + xi)
      else
        print *, 'ERROR: cannot eval 1st order shape function on node =', locnode
        error stop
      endif
    elseif (order == 2) then
      if (locnode == 1) then
        shape_fun = 0.5_dp*xi*(xi - 1.0_dp)
      elseif (locnode == 2) then
        shape_fun = - xi**2 + 1.0_dp
      elseif (locnode == 3) then
        shape_fun = 0.5_dp*xi*(xi + 1.0_dp)
      else
        print *, 'ERROR: cannot eval 2nd order shape function on node =', locnode
        error stop
      endif
    else
      error stop 'shape_fun: shape functions not implemented for order > 2'
    endif
  end function shape_fun
  
  
  function grad_shape_fun(order,locnode,xi)
    integer, intent(in) :: order
    real(dp), intent(in) :: xi
    integer, intent(in) :: locnode
    real(dp) :: grad_shape_fun
    
    if (order == 1) then
      if (locnode == 1) then
        grad_shape_fun = - 0.5_dp
      elseif (locnode == 2) then
        grad_shape_fun = 0.5_dp
      else
        print *, 'ERROR: cannot eval grad of 1st order shape function on node =', locnode
        error stop
      endif
    elseif (order == 2) then
      if (locnode == 1) then
        grad_shape_fun = xi - 0.5_dp
      elseif (locnode == 2) then
        grad_shape_fun = - 2.0_dp*xi
      elseif (locnode == 3) then
        grad_shape_fun = xi + 0.5_dp
      else
        print *, 'ERROR: cannot eval grad of 2nd order shape function on node =', locnode
        error stop
      endif
    else
      error stop 'grad_shape_fun: grad shape functions not implemented for order > 2'
    endif
  end function grad_shape_fun
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine legendre_set(order,points,weights)
    integer, intent(in) :: order
    real(dp), allocatable, intent(out) :: points(:), weights(:)
    
    integer :: no_qps
    
    no_qps = (order+2)/2
    allocate(points(no_qps),weights(no_qps))
    
    if (no_qps==1) then

        points(1) =   0.0_dp

        weights(1) = 2.0_dp

    else if (no_qps==2) then

        points(1) = -0.577350269189625764509148780502_dp
        points(2) =  0.577350269189625764509148780502_dp

        weights(1) = 1.0_dp
        weights(2) = 1.0_dp

    else if (no_qps==3) then

        points(1) = -0.774596669241483377035853079956_dp
        points(2) =  0.000000000000000000000000000000_dp
        points(3) =  0.774596669241483377035853079956_dp

        weights(1) = 5.0_dp / 9.0_dp
        weights(2) = 8.0_dp / 9.0_dp
        weights(3) = 5.0_dp / 9.0_dp

    else if (no_qps==4) then

        points(1) = -0.861136311594052575223946488893_dp
        points(2) = -0.339981043584856264802665759103_dp
        points(3) =  0.339981043584856264802665759103_dp
        points(4) =  0.861136311594052575223946488893_dp

        weights(1) = 0.347854845137453857373063949222_dp
        weights(2) = 0.652145154862546142626936050778_dp
        weights(3) = 0.652145154862546142626936050778_dp
        weights(4) = 0.347854845137453857373063949222_dp

    else if (no_qps==5) then

        points(1) = -0.906179845938663992797626878299_dp
        points(2) = -0.538469310105683091036314420700_dp
        points(3) =  0.000000000000000000000000000000_dp
        points(4) =  0.538469310105683091036314420700_dp
        points(5) =  0.906179845938663992797626878299_dp

        weights(1) = 0.236926885056189087514264040720_dp
        weights(2) = 0.478628670499366468041291514836_dp
        weights(3) = 0.568888888888888888888888888889_dp
        weights(4) = 0.478628670499366468041291514836_dp
        weights(5) = 0.236926885056189087514264040720_dp

    else if (no_qps==6) then

        points(1) = - 0.932469514203152027812301554494_dp
        points(2) = - 0.661209386466264513661399595020_dp
        points(3) = - 0.238619186083196908630501721681_dp
        points(4) =   0.238619186083196908630501721681_dp
        points(5) =   0.661209386466264513661399595020_dp
        points(6) =   0.932469514203152027812301554494_dp

        weights(1) = 0.171324492379170345040296142173_dp
        weights(2) = 0.360761573048138607569833513838_dp
        weights(3) = 0.467913934572691047389870343990_dp
        weights(4) = 0.467913934572691047389870343990_dp
        weights(5) = 0.360761573048138607569833513838_dp
        weights(6) = 0.171324492379170345040296142173_dp

    else if (no_qps==7) then

        points(1) = - 0.949107912342758524526189684048_dp
        points(2) = - 0.741531185599394439863864773281_dp
        points(3) = - 0.405845151377397166906606412077_dp
        points(4) =   0.0_dp
        points(5) =   0.405845151377397166906606412077_dp
        points(6) =   0.741531185599394439863864773281_dp
        points(7) =   0.949107912342758524526189684048_dp

        weights(1) = 0.129484966168869693270611432679_dp
        weights(2) = 0.279705391489276667901467771424_dp
        weights(3) = 0.381830050505118944950369775489_dp
        weights(4) = 0.417959183673469387755102040816_dp
        weights(5) = 0.381830050505118944950369775489_dp
        weights(6) = 0.279705391489276667901467771424_dp
        weights(7) = 0.129484966168869693270611432679_dp

    else if (no_qps==8) then

        points(1) = - 0.960289856497536231683560868569_dp
        points(2) = - 0.796666477413626739591553936476_dp
        points(3) = - 0.525532409916328985817739049189_dp
        points(4) = - 0.183434642495649804939476142360_dp
        points(5) =   0.183434642495649804939476142360_dp
        points(6) =   0.525532409916328985817739049189_dp
        points(7) =   0.796666477413626739591553936476_dp
        points(8) =   0.960289856497536231683560868569_dp

        weights(1) = 0.101228536290376259152531354310_dp
        weights(2) = 0.222381034453374470544355994426_dp
        weights(3) = 0.313706645877887287337962201987_dp
        weights(4) = 0.362683783378361982965150449277_dp
        weights(5) = 0.362683783378361982965150449277_dp
        weights(6) = 0.313706645877887287337962201987_dp
        weights(7) = 0.222381034453374470544355994426_dp
        weights(8) = 0.101228536290376259152531354310_dp

    else if (no_qps==9) then

        points(1) = - 0.968160239507626089835576202904_dp
        points(2) = - 0.836031107326635794299429788070_dp
        points(3) = - 0.613371432700590397308702039341_dp
        points(4) = - 0.324253423403808929038538014643_dp
        points(5) =   0.0_dp
        points(6) =   0.324253423403808929038538014643_dp
        points(7) =   0.613371432700590397308702039341_dp
        points(8) =   0.836031107326635794299429788070_dp
        points(9) =   0.968160239507626089835576202904_dp

        weights(1) = 0.812743883615744119718921581105e-01_dp
        weights(2) = 0.180648160694857404058472031243_dp
        weights(3) = 0.260610696402935462318742869419_dp
        weights(4) = 0.312347077040002840068630406584_dp
        weights(5) = 0.330239355001259763164525069287_dp
        weights(6) = 0.312347077040002840068630406584_dp
        weights(7) = 0.260610696402935462318742869419_dp
        weights(8) = 0.180648160694857404058472031243_dp
        weights(9) = 0.812743883615744119718921581105e-01_dp

    else if (no_qps==10) then

        points(1) =  - 0.973906528517171720077964012084_dp
        points(2) =  - 0.865063366688984510732096688423_dp
        points(3) =  - 0.679409568299024406234327365115_dp
        points(4) =  - 0.433395394129247190799265943166_dp
        points(5) =  - 0.148874338981631210884826001130_dp
        points(6) =    0.148874338981631210884826001130_dp
        points(7) =    0.433395394129247190799265943166_dp
        points(8) =    0.679409568299024406234327365115_dp
        points(9) =    0.865063366688984510732096688423_dp
        points(10) =   0.973906528517171720077964012084_dp

        weights(1) =  0.666713443086881375935688098933e-01_dp
        weights(2) =  0.149451349150580593145776339658_dp
        weights(3) =  0.219086362515982043995534934228_dp
        weights(4) =  0.269266719309996355091226921569_dp
        weights(5) =  0.295524224714752870173892994651_dp
        weights(6) =  0.295524224714752870173892994651_dp
        weights(7) =  0.269266719309996355091226921569_dp
        weights(8) =  0.219086362515982043995534934228_dp
        weights(9) =  0.149451349150580593145776339658_dp
        weights(10) = 0.666713443086881375935688098933e-01_dp

    else if (no_qps==11) then

        points(1) =  - 0.978228658146056992803938001123_dp
        points(2) =  - 0.887062599768095299075157769304_dp
        points(3) =  - 0.730152005574049324093416252031_dp
        points(4) =  - 0.519096129206811815925725669459_dp
        points(5) =  - 0.269543155952344972331531985401_dp
        points(6) =    0.0_dp
        points(7) =    0.269543155952344972331531985401_dp
        points(8) =    0.519096129206811815925725669459_dp
        points(9) =    0.730152005574049324093416252031_dp
        points(10) =   0.887062599768095299075157769304_dp
        points(11) =   0.978228658146056992803938001123_dp

        weights(1) =  0.556685671161736664827537204425e-01_dp
        weights(2) =  0.125580369464904624634694299224_dp
        weights(3) =  0.186290210927734251426097641432_dp
        weights(4) =  0.233193764591990479918523704843_dp
        weights(5) =  0.262804544510246662180688869891_dp
        weights(6) =  0.272925086777900630714483528336_dp
        weights(7) =  0.262804544510246662180688869891_dp
        weights(8) =  0.233193764591990479918523704843_dp
        weights(9) =  0.186290210927734251426097641432_dp
        weights(10) = 0.125580369464904624634694299224_dp
        weights(11) = 0.556685671161736664827537204425e-01_dp

    else if (no_qps==12) then

        points(1) =  - 0.981560634246719250690549090149_dp
        points(2) =  - 0.904117256370474856678465866119_dp
        points(3) =  - 0.769902674194304687036893833213_dp
        points(4) =  - 0.587317954286617447296702418941_dp
        points(5) =  - 0.367831498998180193752691536644_dp
        points(6) =  - 0.125233408511468915472441369464_dp
        points(7) =    0.125233408511468915472441369464_dp
        points(8) =    0.367831498998180193752691536644_dp
        points(9) =    0.587317954286617447296702418941_dp
        points(10) =   0.769902674194304687036893833213_dp
        points(11) =   0.904117256370474856678465866119_dp
        points(12) =   0.981560634246719250690549090149_dp

        weights(1) =  0.471753363865118271946159614850e-01_dp
        weights(2) =  0.106939325995318430960254718194_dp
        weights(3) =  0.160078328543346226334652529543_dp
        weights(4) =  0.203167426723065921749064455810_dp
        weights(5) =  0.233492536538354808760849898925_dp
        weights(6) =  0.249147045813402785000562436043_dp
        weights(7) =  0.249147045813402785000562436043_dp
        weights(8) =  0.233492536538354808760849898925_dp
        weights(9) =  0.203167426723065921749064455810_dp
        weights(10) = 0.160078328543346226334652529543_dp
        weights(11) = 0.106939325995318430960254718194_dp
        weights(12) = 0.471753363865118271946159614850e-01_dp

    else if (no_qps==13) then

        points(1) =  - 0.984183054718588149472829448807_dp
        points(2) =  - 0.917598399222977965206547836501_dp
        points(3) =  - 0.801578090733309912794206489583_dp
        points(4) =  - 0.642349339440340220643984606996_dp
        points(5) =  - 0.448492751036446852877912852128_dp
        points(6) =  - 0.230458315955134794065528121098_dp
        points(7) =    0.0_dp
        points(8) =    0.230458315955134794065528121098_dp
        points(9) =    0.448492751036446852877912852128_dp
        points(10) =   0.642349339440340220643984606996_dp
        points(11) =   0.801578090733309912794206489583_dp
        points(12) =   0.917598399222977965206547836501_dp
        points(13) =   0.984183054718588149472829448807_dp

        weights(1) =  0.404840047653158795200215922010e-01_dp
        weights(2) =  0.921214998377284479144217759538e-01_dp
        weights(3) =  0.138873510219787238463601776869_dp
        weights(4) =  0.178145980761945738280046691996_dp
        weights(5) =  0.207816047536888502312523219306_dp
        weights(6) =  0.226283180262897238412090186040_dp
        weights(7) =  0.232551553230873910194589515269_dp
        weights(8) =  0.226283180262897238412090186040_dp
        weights(9) =  0.207816047536888502312523219306_dp
        weights(10) = 0.178145980761945738280046691996_dp
        weights(11) = 0.138873510219787238463601776869_dp
        weights(12) = 0.921214998377284479144217759538e-01_dp
        weights(13) = 0.404840047653158795200215922010e-01_dp

    else if (no_qps==14) then

        points(1) =  - 0.986283808696812338841597266704_dp
        points(2) =  - 0.928434883663573517336391139378_dp
        points(3) =  - 0.827201315069764993189794742650_dp
        points(4) =  - 0.687292904811685470148019803019_dp
        points(5) =  - 0.515248636358154091965290718551_dp
        points(6) =  - 0.319112368927889760435671824168_dp
        points(7) =  - 0.108054948707343662066244650220_dp
        points(8) =    0.108054948707343662066244650220_dp
        points(9) =    0.319112368927889760435671824168_dp
        points(10) =   0.515248636358154091965290718551_dp
        points(11) =   0.687292904811685470148019803019_dp
        points(12) =   0.827201315069764993189794742650_dp
        points(13) =   0.928434883663573517336391139378_dp
        points(14) =   0.986283808696812338841597266704_dp

        weights(1) =  0.351194603317518630318328761382e-01_dp
        weights(2) =  0.801580871597602098056332770629e-01_dp
        weights(3) =  0.121518570687903184689414809072_dp
        weights(4) =  0.157203167158193534569601938624_dp
        weights(5) =  0.185538397477937813741716590125_dp
        weights(6) =  0.205198463721295603965924065661_dp
        weights(7) =  0.215263853463157790195876443316_dp
        weights(8) =  0.215263853463157790195876443316_dp
        weights(9) =  0.205198463721295603965924065661_dp
        weights(10) = 0.185538397477937813741716590125_dp
        weights(11) = 0.157203167158193534569601938624_dp
        weights(12) = 0.121518570687903184689414809072_dp
        weights(13) = 0.801580871597602098056332770629e-01_dp
        weights(14) = 0.351194603317518630318328761382e-01_dp

    else if (no_qps==15) then

        points(1) =  - 0.987992518020485428489565718587_dp
        points(2) =  - 0.937273392400705904307758947710_dp
        points(3) =  - 0.848206583410427216200648320774_dp
        points(4) =  - 0.724417731360170047416186054614_dp
        points(5) =  - 0.570972172608538847537226737254_dp
        points(6) =  - 0.394151347077563369897207370981_dp
        points(7) =  - 0.201194093997434522300628303395_dp
        points(8) =    0.0_dp
        points(9) =    0.201194093997434522300628303395_dp
        points(10) =   0.394151347077563369897207370981_dp
        points(11) =   0.570972172608538847537226737254_dp
        points(12) =   0.724417731360170047416186054614_dp
        points(13) =   0.848206583410427216200648320774_dp
        points(14) =   0.937273392400705904307758947710_dp
        points(15) =   0.987992518020485428489565718587_dp

        weights(1) =  0.307532419961172683546283935772e-01_dp
        weights(2) =  0.703660474881081247092674164507e-01_dp
        weights(3) =  0.107159220467171935011869546686_dp
        weights(4) =  0.139570677926154314447804794511_dp
        weights(5) =  0.166269205816993933553200860481_dp
        weights(6) =  0.186161000015562211026800561866_dp
        weights(7) =  0.198431485327111576456118326444_dp
        weights(8) =  0.202578241925561272880620199968_dp
        weights(9) =  0.198431485327111576456118326444_dp
        weights(10) = 0.186161000015562211026800561866_dp
        weights(11) = 0.166269205816993933553200860481_dp
        weights(12) = 0.139570677926154314447804794511_dp
        weights(13) = 0.107159220467171935011869546686_dp
        weights(14) = 0.703660474881081247092674164507e-01_dp
        weights(15) = 0.307532419961172683546283935772e-01_dp

    else if (no_qps==16) then

        points(1) =  - 0.989400934991649932596154173450_dp
        points(2) =  - 0.944575023073232576077988415535_dp
        points(3) =  - 0.865631202387831743880467897712_dp
        points(4) =  - 0.755404408355003033895101194847_dp
        points(5) =  - 0.617876244402643748446671764049_dp
        points(6) =  - 0.458016777657227386342419442984_dp
        points(7) =  - 0.281603550779258913230460501460_dp
        points(8) =  - 0.950125098376374401853193354250e-01_dp
        points(9) =    0.950125098376374401853193354250e-01_dp
        points(10) =   0.281603550779258913230460501460_dp
        points(11) =   0.458016777657227386342419442984_dp
        points(12) =   0.617876244402643748446671764049_dp
        points(13) =   0.755404408355003033895101194847_dp
        points(14) =   0.865631202387831743880467897712_dp
        points(15) =   0.944575023073232576077988415535_dp
        points(16) =   0.989400934991649932596154173450_dp

        weights(1) =  0.271524594117540948517805724560e-01_dp
        weights(2) =  0.622535239386478928628438369944e-01_dp
        weights(3) =  0.951585116824927848099251076022e-01_dp
        weights(4) =  0.124628971255533872052476282192_dp
        weights(5) =  0.149595988816576732081501730547_dp
        weights(6) =  0.169156519395002538189312079030_dp
        weights(7) =  0.182603415044923588866763667969_dp
        weights(8) =  0.189450610455068496285396723208_dp
        weights(9) =  0.189450610455068496285396723208_dp
        weights(10) = 0.182603415044923588866763667969_dp
        weights(11) = 0.169156519395002538189312079030_dp
        weights(12) = 0.149595988816576732081501730547_dp
        weights(13) = 0.124628971255533872052476282192_dp
        weights(14) = 0.951585116824927848099251076022e-01_dp
        weights(15) = 0.622535239386478928628438369944e-01_dp
        weights(16) = 0.271524594117540948517805724560e-01_dp

    else if (no_qps==17) then

        points(1) =  - 0.990575475314417335675434019941_dp
        points(2) =  - 0.950675521768767761222716957896_dp
        points(3) =  - 0.880239153726985902122955694488_dp
        points(4) =  - 0.781514003896801406925230055520_dp
        points(5) =  - 0.657671159216690765850302216643_dp
        points(6) =  - 0.512690537086476967886246568630_dp
        points(7) =  - 0.351231763453876315297185517095_dp
        points(8) =  - 0.178484181495847855850677493654_dp
        points(9) =    0.0_dp
        points(10) =   0.178484181495847855850677493654_dp
        points(11) =   0.351231763453876315297185517095_dp
        points(12) =   0.512690537086476967886246568630_dp
        points(13) =   0.657671159216690765850302216643_dp
        points(14) =   0.781514003896801406925230055520_dp
        points(15) =   0.880239153726985902122955694488_dp
        points(16) =   0.950675521768767761222716957896_dp
        points(17) =   0.990575475314417335675434019941_dp

        weights(1) =  0.241483028685479319601100262876e-01_dp
        weights(2) =  0.554595293739872011294401653582e-01_dp
        weights(3) =  0.850361483171791808835353701911e-01_dp
        weights(4) =  0.111883847193403971094788385626_dp
        weights(5) =  0.135136368468525473286319981702_dp
        weights(6) =  0.154045761076810288081431594802_dp
        weights(7) =  0.168004102156450044509970663788_dp
        weights(8) =  0.176562705366992646325270990113_dp
        weights(9) =  0.179446470356206525458265644262_dp
        weights(10) = 0.176562705366992646325270990113_dp
        weights(11) = 0.168004102156450044509970663788_dp
        weights(12) = 0.154045761076810288081431594802_dp
        weights(13) = 0.135136368468525473286319981702_dp
        weights(14) = 0.111883847193403971094788385626_dp
        weights(15) = 0.850361483171791808835353701911e-01_dp
        weights(16) = 0.554595293739872011294401653582e-01_dp
        weights(17) = 0.241483028685479319601100262876e-01_dp

    else if (no_qps==18) then

        points(1) =  - 0.991565168420930946730016004706_dp
        points(2) =  - 0.955823949571397755181195892930_dp
        points(3) =  - 0.892602466497555739206060591127_dp
        points(4) =  - 0.803704958972523115682417455015_dp
        points(5) =  - 0.691687043060353207874891081289_dp
        points(6) =  - 0.559770831073947534607871548525_dp
        points(7) =  - 0.411751161462842646035931793833_dp
        points(8) =  - 0.251886225691505509588972854878_dp
        points(9) =  - 0.847750130417353012422618529358e-01_dp
        points(10) =   0.847750130417353012422618529358e-01_dp
        points(11) =   0.251886225691505509588972854878_dp
        points(12) =   0.411751161462842646035931793833_dp
        points(13) =   0.559770831073947534607871548525_dp
        points(14) =   0.691687043060353207874891081289_dp
        points(15) =   0.803704958972523115682417455015_dp
        points(16) =   0.892602466497555739206060591127_dp
        points(17) =   0.955823949571397755181195892930_dp
        points(18) =   0.991565168420930946730016004706_dp

        weights(1) =  0.216160135264833103133427102665e-01_dp
        weights(2) =  0.497145488949697964533349462026e-01_dp
        weights(3) =  0.764257302548890565291296776166e-01_dp
        weights(4) =  0.100942044106287165562813984925_dp
        weights(5) =  0.122555206711478460184519126800_dp
        weights(6) =  0.140642914670650651204731303752_dp
        weights(7) =  0.154684675126265244925418003836_dp
        weights(8) =  0.164276483745832722986053776466_dp
        weights(9) =  0.169142382963143591840656470135_dp
        weights(10) = 0.169142382963143591840656470135_dp
        weights(11) = 0.164276483745832722986053776466_dp
        weights(12) = 0.154684675126265244925418003836_dp
        weights(13) = 0.140642914670650651204731303752_dp
        weights(14) = 0.122555206711478460184519126800_dp
        weights(15) = 0.100942044106287165562813984925_dp
        weights(16) = 0.764257302548890565291296776166e-01_dp
        weights(17) = 0.497145488949697964533349462026e-01_dp
        weights(18) = 0.216160135264833103133427102665e-01_dp

    else if (no_qps==19) then

        points(1) =  - 0.992406843843584403189017670253_dp
        points(2) =  - 0.960208152134830030852778840688_dp
        points(3) =  - 0.903155903614817901642660928532_dp
        points(4) =  - 0.822714656537142824978922486713_dp
        points(5) =  - 0.720966177335229378617095860824_dp
        points(6) =  - 0.600545304661681023469638164946_dp
        points(7) =  - 0.464570741375960945717267148104_dp
        points(8) =  - 0.316564099963629831990117328850_dp
        points(9) =  - 0.160358645640225375868096115741_dp
        points(10) =   0.0_dp
        points(11) =   0.160358645640225375868096115741_dp
        points(12) =   0.316564099963629831990117328850_dp
        points(13) =   0.464570741375960945717267148104_dp
        points(14) =   0.600545304661681023469638164946_dp
        points(15) =   0.720966177335229378617095860824_dp
        points(16) =   0.822714656537142824978922486713_dp
        points(17) =   0.903155903614817901642660928532_dp
        points(18) =   0.960208152134830030852778840688_dp
        points(19) =   0.992406843843584403189017670253_dp

        weights(1) =  0.194617882297264770363120414644e-01_dp
        weights(2) =  0.448142267656996003328381574020e-01_dp
        weights(3) =  0.690445427376412265807082580060e-01_dp
        weights(4) =  0.914900216224499994644620941238e-01_dp
        weights(5) =  0.111566645547333994716023901682_dp
        weights(6) =  0.128753962539336227675515784857_dp
        weights(7) =  0.142606702173606611775746109442_dp
        weights(8) =  0.152766042065859666778855400898_dp
        weights(9) =  0.158968843393954347649956439465_dp
        weights(10) = 0.161054449848783695979163625321_dp
        weights(11) = 0.158968843393954347649956439465_dp
        weights(12) = 0.152766042065859666778855400898_dp
        weights(13) = 0.142606702173606611775746109442_dp
        weights(14) = 0.128753962539336227675515784857_dp
        weights(15) = 0.111566645547333994716023901682_dp
        weights(16) = 0.914900216224499994644620941238e-01_dp
        weights(17) = 0.690445427376412265807082580060e-01_dp
        weights(18) = 0.448142267656996003328381574020e-01_dp
        weights(19) = 0.194617882297264770363120414644e-01_dp

    else if (no_qps==20) then

        points(1) =  - 0.993128599185094924786122388471_dp
        points(2) =  - 0.963971927277913791267666131197_dp
        points(3) =  - 0.912234428251325905867752441203_dp
        points(4) =  - 0.839116971822218823394529061702_dp
        points(5) =  - 0.746331906460150792614305070356_dp
        points(6) =  - 0.636053680726515025452836696226_dp
        points(7) =  - 0.510867001950827098004364050955_dp
        points(8) =  - 0.373706088715419560672548177025_dp
        points(9) =  - 0.227785851141645078080496195369_dp
        points(10) = - 0.765265211334973337546404093988e-01_dp
        points(11) =   0.765265211334973337546404093988e-01_dp
        points(12) =   0.227785851141645078080496195369_dp
        points(13) =   0.373706088715419560672548177025_dp
        points(14) =   0.510867001950827098004364050955_dp
        points(15) =   0.636053680726515025452836696226_dp
        points(16) =   0.746331906460150792614305070356_dp
        points(17) =   0.839116971822218823394529061702_dp
        points(18) =   0.912234428251325905867752441203_dp
        points(19) =   0.963971927277913791267666131197_dp
        points(20) =   0.993128599185094924786122388471_dp

        weights(1) =  0.176140071391521183118619623519e-01_dp
        weights(2) =  0.406014298003869413310399522749e-01_dp
        weights(3) =  0.626720483341090635695065351870e-01_dp
        weights(4) =  0.832767415767047487247581432220e-01_dp
        weights(5) =  0.101930119817240435036750135480_dp
        weights(6) =  0.118194531961518417312377377711_dp
        weights(7) =  0.131688638449176626898494499748_dp
        weights(8) =  0.142096109318382051329298325067_dp
        weights(9) =  0.149172986472603746787828737002_dp
        weights(10) = 0.152753387130725850698084331955_dp
        weights(11) = 0.152753387130725850698084331955_dp
        weights(12) = 0.149172986472603746787828737002_dp
        weights(13) = 0.142096109318382051329298325067_dp
        weights(14) = 0.131688638449176626898494499748_dp
        weights(15) = 0.118194531961518417312377377711_dp
        weights(16) = 0.101930119817240435036750135480_dp
        weights(17) = 0.832767415767047487247581432220e-01_dp
        weights(18) = 0.626720483341090635695065351870e-01_dp
        weights(19) = 0.406014298003869413310399522749e-01_dp
        weights(20) = 0.176140071391521183118619623519e-01_dp

    else if (no_qps==21) then

        points( 1) =  -0.9937521706203896_dp
        points( 2) =  -0.9672268385663063_dp
        points( 3) =  -0.9200993341504008_dp
        points( 4) =  -0.8533633645833173_dp
        points( 5) =  -0.7684399634756779_dp
        points( 6) =  -0.6671388041974123_dp
        points( 7) =  -0.5516188358872198_dp
        points( 8) =  -0.4243421202074388_dp
        points( 9) =  -0.2880213168024011_dp
        points(10) =  -0.1455618541608951_dp
        points(11) =   0.0000000000000000_dp
        points(12) =   0.1455618541608951_dp
        points(13) =   0.2880213168024011_dp
        points(14) =   0.4243421202074388_dp
        points(15) =   0.5516188358872198_dp
        points(16) =   0.6671388041974123_dp
        points(17) =   0.7684399634756779_dp
        points(18) =   0.8533633645833173_dp
        points(19) =   0.9200993341504008_dp
        points(20) =   0.9672268385663063_dp
        points(21) =   0.9937521706203896_dp 
       
        weights( 1) =   0.1601722825777420e-01_dp
        weights( 2) =   0.3695378977085242e-01_dp
        weights( 3) =   0.5713442542685715e-01_dp
        weights( 4) =   0.7610011362837928e-01_dp
        weights( 5) =   0.9344442345603393e-01_dp
        weights( 6) =   0.1087972991671484_dp
        weights( 7) =   0.1218314160537285_dp
        weights( 8) =   0.1322689386333373_dp
        weights( 9) =   0.1398873947910731_dp
        weights(10) =   0.1445244039899700_dp
        weights(11) =   0.1460811336496904_dp
        weights(12) =   0.1445244039899700_dp
        weights(13) =   0.1398873947910731_dp
        weights(14) =   0.1322689386333373_dp
        weights(15) =   0.1218314160537285_dp
        weights(16) =   0.1087972991671484_dp
        weights(17) =   0.9344442345603393e-01_dp
        weights(18) =   0.7610011362837928e-01_dp
        weights(19) =   0.5713442542685715e-01_dp
        weights(20) =   0.3695378977085242e-01_dp
        weights(21) =   0.1601722825777420e-01_dp

    else if (no_qps==22) then

        points( 1) =  -0.9942945854823994_dp
        points( 2) =  -0.9700604978354287_dp
        points( 3) =  -0.9269567721871740_dp
        points( 4) =  -0.8658125777203002_dp
        points( 5) =  -0.7878168059792081_dp
        points( 6) =  -0.6944872631866827_dp
        points( 7) =  -0.5876404035069116_dp
        points( 8) =  -0.4693558379867570_dp
        points( 9) =  -0.3419358208920842_dp
        points(10) =  -0.2078604266882213_dp
        points(11) =  -0.6973927331972223e-01_dp
        points(12) =   0.6973927331972223e-01_dp
        points(13) =   0.2078604266882213_dp
        points(14) =   0.3419358208920842_dp
        points(15) =   0.4693558379867570_dp
        points(16) =   0.5876404035069116_dp
        points(17) =   0.6944872631866827_dp
        points(18) =   0.7878168059792081_dp
        points(19) =   0.8658125777203002_dp
        points(20) =   0.9269567721871740_dp
        points(21) =   0.9700604978354287_dp
        points(22) =   0.9942945854823994_dp
     
        weights( 1) =   0.1462799529827203e-01_dp
        weights( 2) =   0.3377490158481413e-01_dp
        weights( 3) =   0.5229333515268327e-01_dp
        weights( 4) =   0.6979646842452038e-01_dp
        weights( 5) =   0.8594160621706777e-01_dp
        weights( 6) =   0.1004141444428809_dp
        weights( 7) =   0.1129322960805392_dp
        weights( 8) =   0.1232523768105124_dp
        weights( 9) =   0.1311735047870623_dp
        weights(10) =   0.1365414983460152_dp
        weights(11) =   0.1392518728556321_dp
        weights(12) =   0.1392518728556321_dp
        weights(13) =   0.1365414983460152_dp
        weights(14) =   0.1311735047870623_dp
        weights(15) =   0.1232523768105124_dp
        weights(16) =   0.1129322960805392_dp
        weights(17) =   0.1004141444428809_dp
        weights(18) =   0.8594160621706777e-01_dp
        weights(19) =   0.6979646842452038e-01_dp
        weights(20) =   0.5229333515268327e-01_dp
        weights(21) =   0.3377490158481413e-01_dp
        weights(22) =   0.1462799529827203e-01_dp

    else if (no_qps==23) then

        points( 1) =  -0.9947693349975522_dp
        points( 2) =  -0.9725424712181152_dp
        points( 3) =  -0.9329710868260161_dp
        points( 4) =  -0.8767523582704416_dp
        points( 5) =  -0.8048884016188399_dp
        points( 6) =  -0.7186613631319502_dp
        points( 7) =  -0.6196098757636461_dp
        points( 8) =  -0.5095014778460075_dp
        points( 9) =  -0.3903010380302908_dp
        points(10) =  -0.2641356809703449_dp
        points(11) =  -0.1332568242984661_dp
        points(12) =   0.0000000000000000_dp
        points(13) =   0.1332568242984661_dp
        points(14) =   0.2641356809703449_dp
        points(15) =   0.3903010380302908_dp
        points(16) =   0.5095014778460075_dp
        points(17) =   0.6196098757636461_dp
        points(18) =   0.7186613631319502_dp
        points(19) =   0.8048884016188399_dp
        points(20) =   0.8767523582704416_dp
        points(21) =   0.9329710868260161_dp
        points(22) =   0.9725424712181152_dp
        points(23) =   0.9947693349975522_dp
     
        weights( 1) =   0.1341185948714167e-01_dp
        weights( 2) =   0.3098800585697944e-01_dp
        weights( 3) =   0.4803767173108464e-01_dp
        weights( 4) =   0.6423242140852586e-01_dp
        weights( 5) =   0.7928141177671895e-01_dp
        weights( 6) =   0.9291576606003514e-01_dp
        weights( 7) =   0.1048920914645414_dp
        weights( 8) =   0.1149966402224114_dp
        weights( 9) =   0.1230490843067295_dp
        weights(10) =   0.1289057221880822_dp
        weights(11) =   0.1324620394046967_dp
        weights(12) =   0.1336545721861062_dp
        weights(13) =   0.1324620394046967_dp
        weights(14) =   0.1289057221880822_dp
        weights(15) =   0.1230490843067295_dp
        weights(16) =   0.1149966402224114_dp
        weights(17) =   0.1048920914645414_dp
        weights(18) =   0.9291576606003514e-01_dp
        weights(19) =   0.7928141177671895e-01_dp
        weights(20) =   0.6423242140852586e-01_dp
        weights(21) =   0.4803767173108464e-01_dp
        weights(22) =   0.3098800585697944e-01_dp
        weights(23) =   0.1341185948714167e-01_dp

    else if (no_qps==24) then

        points( 1) =  -0.9951872199970213_dp    
        points( 2) =  -0.9747285559713095_dp    
        points( 3) =  -0.9382745520027327_dp    
        points( 4) =  -0.8864155270044011_dp    
        points( 5) =  -0.8200019859739029_dp    
        points( 6) =  -0.7401241915785544_dp    
        points( 7) =  -0.6480936519369755_dp    
        points( 8) =  -0.5454214713888396_dp    
        points( 9) =  -0.4337935076260451_dp    
        points(10) =  -0.3150426796961634_dp    
        points(11) =  -0.1911188674736163_dp    
        points(12) =  -0.6405689286260562e-01_dp
        points(13) =   0.6405689286260562e-01_dp
        points(14) =   0.1911188674736163_dp    
        points(15) =   0.3150426796961634_dp    
        points(16) =   0.4337935076260451_dp    
        points(17) =   0.5454214713888396_dp    
        points(18) =   0.6480936519369755_dp    
        points(19) =   0.7401241915785544_dp    
        points(20) =   0.8200019859739029_dp    
        points(21) =   0.8864155270044011_dp    
        points(22) =   0.9382745520027327_dp    
        points(23) =   0.9747285559713095_dp    
        points(24) =   0.9951872199970213_dp    
     
        weights( 1) =   0.1234122979998730e-01_dp
        weights( 2) =   0.2853138862893375e-01_dp
        weights( 3) =   0.4427743881741982e-01_dp
        weights( 4) =   0.5929858491543672e-01_dp
        weights( 5) =   0.7334648141108031e-01_dp
        weights( 6) =   0.8619016153195320e-01_dp
        weights( 7) =   0.9761865210411380e-01_dp
        weights( 8) =   0.1074442701159656_dp    
        weights( 9) =   0.1155056680537256_dp    
        weights(10) =   0.1216704729278035_dp    
        weights(11) =   0.1258374563468283_dp    
        weights(12) =   0.1279381953467521_dp    
        weights(13) =   0.1279381953467521_dp    
        weights(14) =   0.1258374563468283_dp    
        weights(15) =   0.1216704729278035_dp    
        weights(16) =   0.1155056680537256_dp   
        weights(17) =   0.1074442701159656_dp    
        weights(18) =   0.9761865210411380e-01_dp
        weights(19) =   0.8619016153195320e-01_dp
        weights(20) =   0.7334648141108031e-01_dp
        weights(21) =   0.5929858491543672e-01_dp
        weights(22) =   0.4427743881741982e-01_dp
        weights(23) =   0.2853138862893375e-01_dp
        weights(24) =   0.1234122979998730e-01_dp

    else if (no_qps==25) then

        points( 1) =  -0.9955569697904981_dp    
        points( 2) =  -0.9766639214595175_dp    
        points( 3) =  -0.9429745712289743_dp    
        points( 4) =  -0.8949919978782754_dp    
        points( 5) =  -0.8334426287608340_dp    
        points( 6) =  -0.7592592630373577_dp    
        points( 7) =  -0.6735663684734684_dp    
        points( 8) =  -0.5776629302412229_dp    
        points( 9) =  -0.4730027314457150_dp    
        points(10) =  -0.3611723058093879_dp    
        points(11) =  -0.2438668837209884_dp    
        points(12) =  -0.1228646926107104_dp    
        points(13) =   0.0000000000000000_dp    
        points(14) =   0.1228646926107104_dp  
        points(15) =   0.2438668837209884_dp    
        points(16) =   0.3611723058093879_dp    
        points(17) =   0.4730027314457150_dp    
        points(18) =   0.5776629302412229_dp    
        points(19) =   0.6735663684734684_dp    
        points(20) =   0.7592592630373577_dp    
        points(21) =   0.8334426287608340_dp    
        points(22) =   0.8949919978782754_dp    
        points(23) =   0.9429745712289743_dp    
        points(24) =   0.9766639214595175_dp    
        points(25) =   0.9955569697904981_dp    
     
        weights( 1) =   0.1139379850102617e-01_dp
        weights( 2) =   0.2635498661503214e-01_dp
        weights( 3) =   0.4093915670130639e-01_dp
        weights( 4) =   0.5490469597583517e-01_dp
        weights( 5) =   0.6803833381235694e-01_dp
        weights( 6) =   0.8014070033500101e-01_dp
        weights( 7) =   0.9102826198296370e-01_dp
        weights( 8) =   0.1005359490670506_dp    
        weights( 9) =   0.1085196244742637_dp    
        weights(10) =   0.1148582591457116_dp    
        weights(11) =   0.1194557635357847_dp    
        weights(12) =   0.1222424429903101_dp    
        weights(13) =   0.1231760537267154_dp    
        weights(14) =   0.1222424429903101_dp    
        weights(15) =   0.1194557635357847_dp    
        weights(16) =   0.1148582591457116_dp    
        weights(17) =   0.1085196244742637_dp    
        weights(18) =   0.1005359490670506_dp    
        weights(19) =   0.9102826198296370e-01_dp
        weights(20) =   0.8014070033500101e-01_dp
        weights(21) =   0.6803833381235694e-01_dp
        weights(22) =   0.5490469597583517e-01_dp
        weights(23) =   0.4093915670130639e-01_dp
        weights(24) =   0.2635498661503214e-01_dp
        weights(25) =   0.1139379850102617e-01_dp

    else if (no_qps==26) then

        points( 1) =  -0.9958857011456169_dp    
        points( 2) =  -0.9783854459564710_dp    
        points( 3) =  -0.9471590666617142_dp    
        points( 4) =  -0.9026378619843071_dp    
        points( 5) =  -0.8454459427884981_dp    
        points( 6) =  -0.7763859488206789_dp    
        points( 7) =  -0.6964272604199573_dp    
        points( 8) =  -0.6066922930176181_dp    
        points( 9) =  -0.5084407148245057_dp    
        points(10) =  -0.4030517551234863_dp    
        points(11) =  -0.2920048394859569_dp    
        points(12) =  -0.1768588203568902_dp    
        points(13) =  -0.5923009342931320e-01_dp
        points(14) =   0.5923009342931320e-01_dp
        points(15) =   0.1768588203568902_dp    
        points(16) =   0.2920048394859569_dp    
        points(17) =   0.4030517551234863_dp    
        points(18) =   0.5084407148245057_dp    
        points(19) =   0.6066922930176181_dp    
        points(20) =   0.6964272604199573_dp    
        points(21) =   0.7763859488206789_dp    
        points(22) =   0.8454459427884981_dp    
        points(23) =   0.9026378619843071_dp    
        points(24) =   0.9471590666617142_dp    
        points(25) =   0.9783854459564710_dp    
        points(26) =   0.9958857011456169_dp    
     
        weights( 1) =   0.1055137261734304e-01_dp
        weights( 2) =   0.2441785109263173e-01_dp
        weights( 3) =   0.3796238329436282e-01_dp
        weights( 4) =   0.5097582529714782e-01_dp
        weights( 5) =   0.6327404632957484e-01_dp
        weights( 6) =   0.7468414976565967e-01_dp
        weights( 7) =   0.8504589431348521e-01_dp
        weights( 8) =   0.9421380035591416e-01_dp
        weights( 9) =   0.1020591610944255_dp    
        weights(10) =   0.1084718405285765_dp    
        weights(11) =   0.1133618165463197_dp    
        weights(12) =   0.1166604434852967_dp    
        weights(13) =   0.1183214152792622_dp    
        weights(14) =   0.1183214152792622_dp    
        weights(15) =   0.1166604434852967_dp    
        weights(16) =   0.1133618165463197_dp    
        weights(17) =   0.1084718405285765_dp    
        weights(18) =   0.1020591610944255_dp    
        weights(19) =   0.9421380035591416e-01_dp
        weights(20) =   0.8504589431348521e-01_dp
        weights(21) =   0.7468414976565967e-01_dp
        weights(22) =   0.6327404632957484e-01_dp
        weights(23) =   0.5097582529714782e-01_dp
        weights(24) =   0.3796238329436282e-01_dp
        weights(25) =   0.2441785109263173e-01_dp
        weights(26) =   0.1055137261734304e-01_dp

    else if (no_qps==27) then

        points( 1) =  -0.9961792628889886_dp    
        points( 2) =  -0.9799234759615012_dp    
        points( 3) =  -0.9509005578147051_dp    
        points( 4) =  -0.9094823206774911_dp    
        points( 5) =  -0.8562079080182945_dp    
        points( 6) =  -0.7917716390705082_dp    
        points( 7) =  -0.7170134737394237_dp    
        points( 8) =  -0.6329079719464952_dp    
        points( 9) =  -0.5405515645794569_dp    
        points(10) =  -0.4411482517500269_dp    
        points(11) =  -0.3359939036385089_dp    
        points(12) =  -0.2264593654395369_dp    
        points(13) =  -0.1139725856095300_dp    
        points(14) =   0.0000000000000000_dp    
        points(15) =   0.1139725856095300_dp    
        points(16) =   0.2264593654395369_dp    
        points(17) =   0.3359939036385089_dp    
        points(18) =   0.4411482517500269_dp    
        points(19) =   0.5405515645794569_dp    
        points(20) =   0.6329079719464952_dp    
        points(21) =   0.7170134737394237_dp    
        points(22) =   0.7917716390705082_dp    
        points(23) =   0.8562079080182945_dp    
        points(24) =   0.9094823206774911_dp    
        points(25) =   0.9509005578147051_dp    
        points(26) =   0.9799234759615012_dp    
        points(27) =   0.9961792628889886_dp    
     
        weights( 1) =   0.9798996051294232e-02_dp
        weights( 2) =   0.2268623159618062e-01_dp
        weights( 3) =   0.3529705375741969e-01_dp
        weights( 4) =   0.4744941252061504e-01_dp
        weights( 5) =   0.5898353685983366e-01_dp
        weights( 6) =   0.6974882376624561e-01_dp
        weights( 7) =   0.7960486777305781e-01_dp
        weights( 8) =   0.8842315854375689e-01_dp
        weights( 9) =   0.9608872737002842e-01_dp
        weights(10) =   0.1025016378177459_dp    
        weights(11) =   0.1075782857885332_dp    
        weights(12) =   0.1112524883568452_dp    
        weights(13) =   0.1134763461089651_dp    
        weights(14) =   0.1142208673789570_dp    
        weights(15) =   0.1134763461089651_dp    
        weights(16) =   0.1112524883568452_dp    
        weights(17) =   0.1075782857885332_dp    
        weights(18) =   0.1025016378177459_dp    
        weights(19) =   0.9608872737002842e-01_dp
        weights(20) =   0.8842315854375689e-01_dp
        weights(21) =   0.7960486777305781e-01_dp
        weights(22) =   0.6974882376624561e-01_dp
        weights(23) =   0.5898353685983366e-01_dp
        weights(24) =   0.4744941252061504e-01_dp
        weights(25) =   0.3529705375741969e-01_dp
        weights(26) =   0.2268623159618062e-01_dp
        weights(27) =   0.9798996051294232e-02_dp

    else if (no_qps==28) then

        points( 1) =  -0.9964424975739544_dp    
        points( 2) =  -0.9813031653708728_dp    
        points( 3) =  -0.9542592806289382_dp    
        points( 4) =  -0.9156330263921321_dp    
        points( 5) =  -0.8658925225743951_dp    
        points( 6) =  -0.8056413709171791_dp    
        points( 7) =  -0.7356108780136318_dp    
        points( 8) =  -0.6566510940388650_dp    
        points( 9) =  -0.5697204718114017_dp    
        points(10) =  -0.4758742249551183_dp    
        points(11) =  -0.3762515160890787_dp    
        points(12) =  -0.2720616276351780_dp    
        points(13) =  -0.1645692821333808_dp    
        points(14) =  -0.5507928988403427e-01_dp
        points(15) =   0.5507928988403427e-01_dp
        points(16) =   0.1645692821333808_dp    
        points(17) =   0.2720616276351780_dp    
        points(18) =   0.3762515160890787_dp    
        points(19) =   0.4758742249551183_dp    
        points(20) =   0.5697204718114017_dp    
        points(21) =   0.6566510940388650_dp    
        points(22) =   0.7356108780136318_dp    
        points(23) =   0.8056413709171791_dp    
        points(24) =   0.8658925225743951_dp    
        points(25) =   0.9156330263921321_dp    
        points(26) =   0.9542592806289382_dp    
        points(27) =   0.9813031653708728_dp    
        points(28) =   0.9964424975739544_dp    
     
        weights( 1) =   0.9124282593094672e-02_dp
        weights( 2) =   0.2113211259277118e-01_dp
        weights( 3) =   0.3290142778230441e-01_dp
        weights( 4) =   0.4427293475900429e-01_dp
        weights( 5) =   0.5510734567571667e-01_dp
        weights( 6) =   0.6527292396699959e-01_dp
        weights( 7) =   0.7464621423456877e-01_dp
        weights( 8) =   0.8311341722890127e-01_dp
        weights( 9) =   0.9057174439303289e-01_dp
        weights(10) =   0.9693065799792999e-01_dp
        weights(11) =   0.1021129675780608_dp    
        weights(12) =   0.1060557659228464_dp    
        weights(13) =   0.1087111922582942_dp    
        weights(14) =   0.1100470130164752_dp    
        weights(15) =   0.1100470130164752_dp    
        weights(16) =   0.1087111922582942_dp    
        weights(17) =   0.1060557659228464_dp    
        weights(18) =   0.1021129675780608_dp   
        weights(19) =   0.9693065799792999e-01_dp
        weights(20) =   0.9057174439303289e-01_dp
        weights(21) =   0.8311341722890127e-01_dp
        weights(22) =   0.7464621423456877e-01_dp
        weights(23) =   0.6527292396699959e-01_dp
        weights(24) =   0.5510734567571667e-01_dp
        weights(25) =   0.4427293475900429e-01_dp
        weights(26) =   0.3290142778230441e-01_dp
        weights(27) =   0.2113211259277118e-01_dp
        weights(28) =   0.9124282593094672e-02_dp

    else if (no_qps==29) then

        points( 1) =  -0.9966794422605966_dp    
        points( 2) =  -0.9825455052614132_dp    
        points( 3) =  -0.9572855957780877_dp    
        points( 4) =  -0.9211802329530588_dp    
        points( 5) =  -0.8746378049201028_dp    
        points( 6) =  -0.8181854876152524_dp    
        points( 7) =  -0.7524628517344771_dp    
        points( 8) =  -0.6782145376026865_dp    
        points( 9) =  -0.5962817971382278_dp    
        points(10) =  -0.5075929551242276_dp    
        points(11) =  -0.4131528881740087_dp    
        points(12) =  -0.3140316378676399_dp    
        points(13) =  -0.2113522861660011_dp    
        points(14) =  -0.1062782301326792_dp    
        points(15) =   0.0000000000000000_dp    
        points(16) =   0.1062782301326792_dp    
        points(17) =   0.2113522861660011_dp    
        points(18) =   0.3140316378676399_dp    
        points(19) =   0.4131528881740087_dp    
        points(20) =   0.5075929551242276_dp    
        points(21) =   0.5962817971382278_dp    
        points(22) =   0.6782145376026865_dp    
        points(23) =   0.7524628517344771_dp    
        points(24) =   0.8181854876152524_dp    
        points(25) =   0.8746378049201028_dp    
        points(26) =   0.9211802329530588_dp    
        points(27) =   0.9572855957780877_dp    
        points(28) =   0.9825455052614132_dp    
        points(29) =   0.9966794422605966_dp    
     
        weights( 1) =   0.8516903878746365e-02_dp
        weights( 2) =   0.1973208505612276e-01_dp
        weights( 3) =   0.3074049220209360e-01_dp
        weights( 4) =   0.4140206251868281e-01_dp
        weights( 5) =   0.5159482690249799e-01_dp
        weights( 6) =   0.6120309065707916e-01_dp
        weights( 7) =   0.7011793325505125e-01_dp
        weights( 8) =   0.7823832713576385e-01_dp
        weights( 9) =   0.8547225736617248e-01_dp
        weights(10) =   0.9173775713925882e-01_dp
        weights(11) =   0.9696383409440862e-01_dp
        weights(12) =   0.1010912737599150_dp    
        weights(13) =   0.1040733100777293_dp    
        weights(14) =   0.1058761550973210_dp    
        weights(15) =   0.1064793817183143_dp    
        weights(16) =   0.1058761550973210_dp    
        weights(17) =   0.1040733100777293_dp    
        weights(18) =   0.1010912737599150_dp    
        weights(19) =   0.9696383409440862e-01_dp
        weights(20) =   0.9173775713925882e-01_dp
        weights(21) =   0.8547225736617248e-01_dp
        weights(22) =   0.7823832713576385e-01_dp
        weights(23) =   0.7011793325505125e-01_dp
        weights(24) =   0.6120309065707916e-01_dp
        weights(25) =   0.5159482690249799e-01_dp
        weights(26) =   0.4140206251868281e-01_dp
        weights(27) =   0.3074049220209360e-01_dp
        weights(28) =   0.1973208505612276e-01_dp
        weights(29) =   0.8516903878746365e-02_dp

    else if (no_qps==30) then

        points( 1) =  -0.9968934840746495_dp    
        points( 2) =  -0.9836681232797472_dp    
        points( 3) =  -0.9600218649683075_dp    
        points( 4) =  -0.9262000474292743_dp    
        points( 5) =  -0.8825605357920526_dp    
        points( 6) =  -0.8295657623827684_dp    
        points( 7) =  -0.7677774321048262_dp    
        points( 8) =  -0.6978504947933158_dp    
        points( 9) =  -0.6205261829892429_dp    
        points(10) =  -0.5366241481420199_dp    
        points(11) =  -0.4470337695380892_dp    
        points(12) =  -0.3527047255308781_dp    
        points(13) =  -0.2546369261678899_dp    
        points(14) =  -0.1538699136085835_dp    
        points(15) =  -0.5147184255531770e-01_dp
        points(16) =   0.5147184255531770e-01_dp
        points(17) =   0.1538699136085835_dp    
        points(18) =   0.2546369261678899_dp    
        points(19) =   0.3527047255308781_dp    
        points(20) =   0.4470337695380892_dp    
        points(21) =   0.5366241481420199_dp    
        points(22) =   0.6205261829892429_dp    
        points(23) =   0.6978504947933158_dp    
        points(24) =   0.7677774321048262_dp    
        points(25) =   0.8295657623827684_dp    
        points(26) =   0.8825605357920526_dp    
        points(27) =   0.9262000474292743_dp    
        points(28) =   0.9600218649683075_dp    
        points(29) =   0.9836681232797472_dp    
        points(30) =   0.9968934840746495_dp    
     
        weights( 1) =   0.7968192496166648e-02_dp
        weights( 2) =   0.1846646831109099e-01_dp
        weights( 3) =   0.2878470788332330e-01_dp
        weights( 4) =   0.3879919256962704e-01_dp
        weights( 5) =   0.4840267283059405e-01_dp
        weights( 6) =   0.5749315621761905e-01_dp
        weights( 7) =   0.6597422988218052e-01_dp
        weights( 8) =   0.7375597473770516e-01_dp
        weights( 9) =   0.8075589522942023e-01_dp
        weights(10) =   0.8689978720108314e-01_dp
        weights(11) =   0.9212252223778619e-01_dp
        weights(12) =   0.9636873717464424e-01_dp
        weights(13) =   0.9959342058679524e-01_dp
        weights(14) =   0.1017623897484056_dp    
        weights(15) =   0.1028526528935587_dp    
        weights(16) =   0.1028526528935587_dp    
        weights(17) =   0.1017623897484056_dp    
        weights(18) =   0.9959342058679524e-01_dp
        weights(19) =   0.9636873717464424e-01_dp
        weights(20) =   0.9212252223778619e-01_dp
        weights(21) =   0.8689978720108314e-01_dp
        weights(22) =   0.8075589522942023e-01_dp
        weights(23) =   0.7375597473770516e-01_dp
        weights(24) =   0.6597422988218052e-01_dp
        weights(25) =   0.5749315621761905e-01_dp
        weights(26) =   0.4840267283059405e-01_dp
        weights(27) =   0.3879919256962704e-01_dp
        weights(28) =   0.2878470788332330e-01_dp
        weights(29) =   0.1846646831109099e-01_dp
        weights(30) =   0.7968192496166648e-02_dp

    else if (no_qps==31) then

        points( 1) =  -0.99708748181947707454263838179654_dp   
        points( 2) =  -0.98468590966515248400211329970113_dp
        points( 3) =  -0.96250392509294966178905249675943_dp
        points( 4) =  -0.93075699789664816495694576311725_dp
        points( 5) =  -0.88976002994827104337419200908023_dp
        points( 6) =  -0.83992032014626734008690453594388_dp
        points( 7) =  -0.78173314841662494040636002019484_dp
        points( 8) =  -0.71577678458685328390597086536649_dp
        points( 9) =  -0.64270672292426034618441820323250_dp
        points(10) =  -0.56324916140714926272094492359516_dp
        points(11) =  -0.47819378204490248044059403935649_dp
        points(12) =  -0.38838590160823294306135146128752_dp
        points(13) =  -0.29471806998170161661790389767170_dp
        points(14) =  -0.19812119933557062877241299603283_dp
        points(15) =  -0.99555312152341520325174790118941e-01_dp
        points(16) =   0.00000000000000000000000000000000_dp  
        points(17) =   0.99555312152341520325174790118941e-01_dp
        points(18) =   0.19812119933557062877241299603283_dp    
        points(19) =   0.29471806998170161661790389767170_dp    
        points(20) =   0.38838590160823294306135146128752_dp    
        points(21) =   0.47819378204490248044059403935649_dp    
        points(22) =   0.56324916140714926272094492359516_dp    
        points(23) =   0.64270672292426034618441820323250_dp    
        points(24) =   0.71577678458685328390597086536649_dp    
        points(25) =   0.78173314841662494040636002019484_dp    
        points(26) =   0.83992032014626734008690453594388_dp    
        points(27) =   0.88976002994827104337419200908023_dp    
        points(28) =   0.93075699789664816495694576311725_dp    
        points(29) =   0.96250392509294966178905249675943_dp    
        points(30) =   0.98468590966515248400211329970113_dp    
        points(31) =   0.99708748181947707454263838179654_dp    
     
        weights( 1) =   0.74708315792487746093913218970494e-02_dp
        weights( 2) =   0.17318620790310582463552990782414e-01_dp
        weights( 3) =   0.27009019184979421800608642617676e-01_dp
        weights( 4) =   0.36432273912385464024392008749009e-01_dp
        weights( 5) =   0.45493707527201102902315857856518e-01_dp
        weights( 6) =   0.54103082424916853711666259085477e-01_dp
        weights( 7) =   0.62174786561028426910343543686657e-01_dp
        weights( 8) =   0.69628583235410366167756126255124e-01_dp
        weights( 9) =   0.76390386598776616426357674901331e-01_dp
        weights(10) =   0.82392991761589263903823367431962e-01_dp
        weights(11) =   0.87576740608477876126198069695333e-01_dp
        weights(12) =   0.91890113893641478215362871607150e-01_dp
        weights(13) =   0.95290242912319512807204197487597e-01_dp
        weights(14) =   0.97743335386328725093474010978997e-01_dp
        weights(15) =   0.99225011226672307874875514428615e-01_dp
        weights(16) =   0.99720544793426451427533833734349e-01_dp
        weights(17) =   0.99225011226672307874875514428615e-01_dp
        weights(18) =   0.97743335386328725093474010978997e-01_dp
        weights(19) =   0.95290242912319512807204197487597e-01_dp
        weights(20) =   0.91890113893641478215362871607150e-01_dp
        weights(21) =   0.87576740608477876126198069695333e-01_dp
        weights(22) =   0.82392991761589263903823367431962e-01_dp
        weights(23) =   0.76390386598776616426357674901331e-01_dp
        weights(24) =   0.69628583235410366167756126255124e-01_dp
        weights(25) =   0.62174786561028426910343543686657e-01_dp
        weights(26) =   0.54103082424916853711666259085477e-01_dp
        weights(27) =   0.45493707527201102902315857856518e-01_dp
        weights(28) =   0.36432273912385464024392008749009e-01_dp
        weights(29) =   0.27009019184979421800608642617676e-01_dp
        weights(30) =   0.17318620790310582463552990782414e-01_dp
        weights(31) =   0.74708315792487746093913218970494e-02_dp

    else if (no_qps==32) then

        points(1) =  - 0.997263861849481563544981128665_dp
        points(2) =  - 0.985611511545268335400175044631_dp
        points(3) =  - 0.964762255587506430773811928118_dp
        points(4) =  - 0.934906075937739689170919134835_dp
        points(5) =  - 0.896321155766052123965307243719_dp
        points(6) =  - 0.849367613732569970133693004968_dp
        points(7) =  - 0.794483795967942406963097298970_dp
        points(8) =  - 0.732182118740289680387426665091_dp
        points(9) =  - 0.663044266930215200975115168663_dp
        points(10) = - 0.587715757240762329040745476402_dp
        points(11) = - 0.506899908932229390023747474378_dp
        points(12) = - 0.421351276130635345364119436172_dp
        points(13) = - 0.331868602282127649779916805730_dp
        points(14) = - 0.239287362252137074544603209166_dp
        points(15) = - 0.144471961582796493485186373599_dp
        points(16) = - 0.483076656877383162348125704405e-01_dp
        points(17) =   0.483076656877383162348125704405e-01_dp
        points(18) =   0.144471961582796493485186373599_dp
        points(19) =   0.239287362252137074544603209166_dp
        points(20) =   0.331868602282127649779916805730_dp
        points(21) =   0.421351276130635345364119436172_dp
        points(22) =   0.506899908932229390023747474378_dp
        points(23) =   0.587715757240762329040745476402_dp
        points(24) =   0.663044266930215200975115168663_dp
        points(25) =   0.732182118740289680387426665091_dp
        points(26) =   0.794483795967942406963097298970_dp
        points(27) =   0.849367613732569970133693004968_dp
        points(28) =   0.896321155766052123965307243719_dp
        points(29) =   0.934906075937739689170919134835_dp
        points(30) =   0.964762255587506430773811928118_dp
        points(31) =   0.985611511545268335400175044631_dp
        points(32) =   0.997263861849481563544981128665_dp

        weights(1) =  0.701861000947009660040706373885e-02_dp
        weights(2) =  0.162743947309056706051705622064e-01_dp
        weights(3) =  0.253920653092620594557525897892e-01_dp
        weights(4) =  0.342738629130214331026877322524e-01_dp
        weights(5) =  0.428358980222266806568786466061e-01_dp
        weights(6) =  0.509980592623761761961632446895e-01_dp
        weights(7) =  0.586840934785355471452836373002e-01_dp
        weights(8) =  0.658222227763618468376500637069e-01_dp
        weights(9) =  0.723457941088485062253993564785e-01_dp
        weights(10) = 0.781938957870703064717409188283e-01_dp
        weights(11) = 0.833119242269467552221990746043e-01_dp
        weights(12) = 0.876520930044038111427714627518e-01_dp
        weights(13) = 0.911738786957638847128685771116e-01_dp
        weights(14) = 0.938443990808045656391802376681e-01_dp
        weights(15) = 0.956387200792748594190820022041e-01_dp
        weights(16) = 0.965400885147278005667648300636e-01_dp
        weights(17) = 0.965400885147278005667648300636e-01_dp
        weights(18) = 0.956387200792748594190820022041e-01_dp
        weights(19) = 0.938443990808045656391802376681e-01_dp
        weights(20) = 0.911738786957638847128685771116e-01_dp
        weights(21) = 0.876520930044038111427714627518e-01_dp
        weights(22) = 0.833119242269467552221990746043e-01_dp
        weights(23) = 0.781938957870703064717409188283e-01_dp
        weights(24) = 0.723457941088485062253993564785e-01_dp
        weights(25) = 0.658222227763618468376500637069e-01_dp
        weights(26) = 0.586840934785355471452836373002e-01_dp
        weights(27) = 0.509980592623761761961632446895e-01_dp
        weights(28) = 0.428358980222266806568786466061e-01_dp
        weights(29) = 0.342738629130214331026877322524e-01_dp
        weights(30) = 0.253920653092620594557525897892e-01_dp
        weights(31) = 0.162743947309056706051705622064e-01_dp
        weights(32) = 0.701861000947009660040706373885e-02_dp

    else if (no_qps==33) then

        points( 1) =  -0.9974246942464552_dp    
        points( 2) =  -0.9864557262306425_dp
        points( 3) =  -0.9668229096899927_dp
        points( 4) =  -0.9386943726111684_dp    
        points( 5) =  -0.9023167677434336_dp    
        points( 6) =  -0.8580096526765041_dp    
        points( 7) =  -0.8061623562741665_dp    
        points( 8) =  -0.7472304964495622_dp    
        points( 9) =  -0.6817319599697428_dp    
        points(10) =  -0.6102423458363790_dp    
        points(11) =  -0.5333899047863476_dp    
        points(12) =  -0.4518500172724507_dp    
        points(13) =  -0.3663392577480734_dp    
        points(14) =  -0.2776090971524970_dp    
        points(15) =  -0.1864392988279916_dp    
        points(16) =  -0.09363106585473338_dp
        points(17) =   0.000000000000000_dp
        points(18) =   0.09363106585473338_dp
        points(19) =   0.1864392988279916_dp    
        points(20) =   0.2776090971524970_dp    
        points(21) =   0.3663392577480734_dp    
        points(22) =   0.4518500172724507_dp    
        points(23) =   0.5333899047863476_dp    
        points(24) =   0.6102423458363790_dp    
        points(25) =   0.6817319599697428_dp    
        points(26) =   0.7472304964495622_dp    
        points(27) =   0.8061623562741665_dp    
        points(28) =   0.8580096526765041_dp    
        points(29) =   0.9023167677434336_dp    
        points(30) =   0.9386943726111684_dp    
        points(31) =   0.9668229096899927_dp    
        points(32) =   0.9864557262306425_dp    
        points(33) =   0.9974246942464552_dp    
     
        weights( 1) =   0.6606227847587558e-02_dp
        weights( 2) =   0.1532170151293465e-01_dp
        weights( 3) =   0.2391554810174960e-01_dp
        weights( 4) =   0.3230035863232891e-01_dp
        weights( 5) =   0.4040154133166965e-01_dp
        weights( 6) =   0.4814774281871162e-01_dp
        weights( 7) =   0.5547084663166357e-01_dp
        weights( 8) =   0.6230648253031755e-01_dp
        weights( 9) =   0.6859457281865676e-01_dp
        weights(10) =   0.7427985484395420e-01_dp
        weights(11) =   0.7931236479488685e-01_dp
        weights(12) =   0.8364787606703869e-01_dp
        weights(13) =   0.8724828761884425e-01_dp
        weights(14) =   0.9008195866063859e-01_dp
        weights(15) =   0.9212398664331678e-01_dp
        weights(16) =   0.9335642606559612e-01_dp
        weights(17) =   0.9376844616020999e-01_dp
        weights(18) =   0.9335642606559612e-01_dp
        weights(19) =   0.9212398664331678e-01_dp
        weights(20) =   0.9008195866063859e-01_dp
        weights(21) =   0.8724828761884425e-01_dp
        weights(22) =   0.8364787606703869e-01_dp
        weights(23) =   0.7931236479488685e-01_dp
        weights(24) =   0.7427985484395420e-01_dp
        weights(25) =   0.6859457281865676e-01_dp
        weights(26) =   0.6230648253031755e-01_dp
        weights(27) =   0.5547084663166357e-01_dp
        weights(28) =   0.4814774281871162e-01_dp
        weights(29) =   0.4040154133166965e-01_dp
        weights(30) =   0.3230035863232891e-01_dp
        weights(31) =   0.2391554810174960e-01_dp
        weights(32) =   0.1532170151293465e-01_dp
        weights(33) =   0.6606227847587558e-02_dp
      
      else
        
        error stop 'Number of quad points > 33 not supported'
    endif
  end subroutine legendre_set
end module support