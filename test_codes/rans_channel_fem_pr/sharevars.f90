module sharevars
  use, intrinsic :: iso_fortran_env, only: real64
  implicit none
  
  integer, parameter :: dp = real64
  real(dp), parameter :: SMALL = sqrt(epsilon(1.0_dp))
  
  integer, parameter  :: no_elem = 220
  integer, parameter  :: no_edges = no_elem + 1
  real(dp), parameter :: H = 2.0_dp
  real(dp), parameter :: L_max = H/2.0_dp
  real(dp), parameter :: dt = 5.0e-3_dp
  integer, parameter  :: no_steps = 1000000
  
  real(dp), parameter :: penalty_safety_factor = 1.0_dp
  
  real(dp), parameter :: Kappa = 0.41_dp
  real(dp), parameter :: C_mu = 0.09_dp
  real(dp), parameter :: nu_lam = 2.5e-5_dp
  real(dp), parameter :: dpdx = -0.000115653745790_dp
  
  integer, parameter :: order_u = 2
  
  integer, parameter  :: nnod_u = order_u + 1
  integer, parameter  :: no_dofs_u = nnod_u * no_elem
  
  real(dp), dimension(no_dofs_u), target :: u, u_old
  real(dp), allocatable :: nu_t(:,:)
  real(dp), dimension(no_elem,2) :: nu_t_edge
  real(dp) :: y_coord(no_edges), dy(no_elem)
  
  integer, target :: dofs_u(nnod_u,no_elem)
  
  real(dp), allocatable :: qps(:), qws(:)
end module sharevars
