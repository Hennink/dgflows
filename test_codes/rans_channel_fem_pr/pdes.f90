module pdes
  use sharevars
  use support
  implicit none
  
contains
  subroutine calc_u()
    integer :: info, elem, edge, locnodev, locnodeu, elem_v, elem_u
    integer :: row, col, row_1st, col_1st, row_end, col_end
    real(dp) :: rhs(no_dofs_u), ipiv(no_dofs_u)
    real(dp) :: M(no_dofs_u,no_dofs_u)
    real(dp) :: weight_at_qps(size(qps))
    real(dp), allocatable :: dpdx_x_f(:)
    real(dp), allocatable :: fxf(:,:)
    real(dp), allocatable :: edge_gradf_right(:), edge_f_right(:)
    real(dp), allocatable :: edge_fxf_left(:,:)
    real(dp), allocatable :: edge_fxf_right(:,:)
    real(dp), allocatable :: edge_fxf_mixed(:,:)
    real(dp), allocatable :: Dxgradfxgradf(:,:)
    real(dp), allocatable :: edge_fxgradf_left(:,:)
    real(dp), allocatable :: edge_fxgradf_right(:,:)
    real(dp), allocatable :: edge_fxgradf_mixed(:,:)
    real(dp), allocatable :: edge_gradfxf_mixed(:,:)
    
    M = 0.0_dp
    rhs = 0.0_dp
    
    do elem = 1, no_elem
      row_1st = nnod_u*(elem-1)+1
      row_end = nnod_u*elem
      col_1st = nnod_u*(elem-1)+1
      col_end = nnod_u*elem
      
      ! Time derivative
      call calculate_vol_integral_fxf('velocity',elem,fxf)
      M(row_1st:row_end,col_1st:col_end) = M(row_1st:row_end,col_1st:col_end) + fxf / dt
      rhs(row_1st:row_end) = rhs(row_1st:row_end) + matmul(fxf,u_old(row_1st:row_end)) / dt
      
      ! Source term, dp/dx
      weight_at_qps = dpdx
      call calculate_vol_integral_f('velocity',elem,dpdx_x_f,weight_at_qps)
      rhs(row_1st:row_end) = rhs(row_1st:row_end) - dpdx_x_f
      
      ! Stress term: Volumetric term
      weight_at_qps = nu_lam + nu_t(elem,:)
      call calculate_vol_integral_gradfxgradf('velocity',elem,weight_at_qps,Dxgradfxgradf)
      M(row_1st:row_end,col_1st:col_end) = M(row_1st:row_end,col_1st:col_end) + Dxgradfxgradf
    enddo
    
    do edge = 1, no_edges
      ! Stress term:  edges terms
      call calculate_face_integrals('velocity',edge,edge_f_right,edge_gradf_right,edge_fxf_left,edge_fxf_right,edge_fxf_mixed,&
                                               edge_fxgradf_left,edge_fxgradf_right,edge_fxgradf_mixed,edge_gradfxf_mixed)
      
      ! Boundary edges terms
      if (edge == 1) then
        ! Homogeneous Dirichlet bc
        elem_v = edge
        elem_u = edge
        do locnodeu = 1, nnod_u! u-index
          col = dofs_u(locnodeu,elem_u)
          do locnodev = 1, nnod_u   ! v_index
            row = dofs_u(locnodev,elem_v)
            M(row,col) = M(row,col) + penalty(edge,'velocity')*edge_fxf_right(locnodev,locnodeu)
            M(row,col) = M(row,col) + (nu_lam + nu_t_edge(elem_u,1))*edge_fxgradf_right(locnodev,locnodeu)
            M(row,col) = M(row,col) + (nu_lam + nu_t_edge(elem_v,1))*edge_fxgradf_right(locnodeu,locnodev)
          enddo
        enddo
      elseif (edge == no_edges) then
        ! Do nothing, because symmetry bc ==> homogeneous Neumann bc
      else
        ! Internal edges terms
        ! v_i chosen left of edge
        elem_v = edge-1
        elem_u = edge-1
        do locnodeu = 1, nnod_u ! u-index
          col = dofs_u(locnodeu,elem_u)
          do locnodev = 1, nnod_u   ! v_index
            row = dofs_u(locnodev,elem_v)
            M(row,col) = M(row,col) + penalty(edge,'velocity')*edge_fxf_left(locnodev,locnodeu)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_u,2))*edge_fxgradf_left(locnodev,locnodeu)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_v,2))*edge_fxgradf_left(locnodeu,locnodev)
          enddo
        enddo
        
        elem_u = edge
        do locnodeu = 1, nnod_u ! u-index
          col = dofs_u(locnodeu,elem_u)
          do locnodev = 1, nnod_u   ! v_index
            row = dofs_u(locnodev,elem_v)
            M(row,col) = M(row,col) - penalty(edge,'velocity')*edge_fxf_mixed(locnodev,locnodeu)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_u,1))*edge_fxgradf_mixed(locnodev,locnodeu)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_v,2))*edge_gradfxf_mixed(locnodev,locnodeu)
          enddo
        enddo
        
        ! v_i chosen right of edge 
        elem_v = edge
        elem_u = edge-1
        do locnodeu = 1, nnod_u ! u-index
          col = dofs_u(locnodeu,elem_u)
          do locnodev = 1, nnod_u   ! v_index
            row = dofs_u(locnodev,elem_v)
            M(row,col) = M(row,col) - penalty(edge,'velocity')*edge_fxf_mixed(locnodeu,locnodev)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_u,2))*edge_gradfxf_mixed(locnodeu,locnodev)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_v,1))*edge_fxgradf_mixed(locnodeu,locnodev)
          enddo
        enddo
        
        elem_u = edge
        do locnodeu= 1, nnod_u ! u-index
          col = dofs_u(locnodeu,elem_u)
          do locnodev = 1, nnod_u   ! v_index
            row = dofs_u(locnodev,elem_v)
            M(row,col) = M(row,col) + penalty(edge,'velocity')*edge_fxf_right(locnodev,locnodeu)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_u,1))*edge_fxgradf_right(locnodev,locnodeu)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_v,1))*edge_fxgradf_right(locnodeu,locnodev)
          enddo
        enddo
      endif
    enddo
    
    
    ! Symmetry test of matrix
    do row = 1, no_dofs_u
      do col = 1, row-1
        if (abs(M(row,col) - M(col,row)) > SMALL) then 
          print *,'calc_u: asymmetry detected',row,col, abs(M(row,col) - M(col,row))
          error stop
        endif
      enddo
    enddo
    
    ! Call solver
    call dgesv(no_dofs_u,1,M,no_dofs_u,ipiv,rhs,no_dofs_u,info)
    if (info /= 0) error stop 'calc_u: WARNING in solver'
    u = rhs
    
    print *, 'max dU/dt = ', solution_max_ddt('velocity')
  end subroutine calc_u
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine calc_nu_t()
    integer :: elem, p
    real(dp), dimension(size(qps)) :: grad_u_at_qps
    real(dp) :: L_mix, S, y, grad_u_l, grad_u_r
    
    do elem = 1, no_elem
      ! Compute nu_t at elem quad points
      call compute_grad_solution_at_elem_qps('velocity',elem,grad_u_at_qps)
      do p = 1, size(qps)
        call convert_lcor2gcor(qps(p),elem,y)
        S = abs(grad_u_at_qps(p))
        L_mix = min(Kappa*y,C_mu*L_max)
        nu_t(elem,p) = max(S*(L_mix**2),1.e-4*nu_lam)
      enddo
      
      ! Compute nu_t at faces quad points
      call compute_solution_at_lcor('velocity',elem,-1.0_dp,grad=grad_u_l)
      call convert_lcor2gcor(-1.0_dp,elem,y)
      S = abs(grad_u_l)
      L_mix = min(Kappa*y,C_mu*L_max)
      nu_t_edge(elem,1) = max(S*(L_mix**2),1.e-4*nu_lam)
      
      call compute_solution_at_lcor('velocity',elem,+1.0_dp,grad=grad_u_r)
      call convert_lcor2gcor(+1.0_dp,elem,y)
      S = abs(grad_u_r)
      L_mix = min(Kappa*y,C_mu*L_max)
      nu_t_edge(elem,2) = max(S*(L_mix**2),1.e-4*nu_lam)
    enddo
  end subroutine calc_nu_t
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine compute_tau_and_nut_at_gcors(gcors,tau_values,nut_values)
    real(dp), intent(in) :: gcors(:)
    real(dp), allocatable, intent(out) :: tau_values(:), nut_values(:)
    
    real(dp), allocatable :: grads_u(:)
    real(dp) :: L_mix, S
    integer :: p
    
    call compute_solution_at_gcors('velocity',gcors,grads=grads_u)
    allocate(tau_values(size(gcors)),nut_values(size(gcors)))
    do p = 1, size(gcors)
      S = abs(grads_u(p))
      L_mix = min(Kappa*gcors(p),C_mu*L_max)
      nut_values(p) = max(S*(L_mix**2),1.e-4*nu_lam)
      tau_values(p) = (nu_lam + nut_values(p)) * grads_u(p)
    enddo
  end subroutine compute_tau_and_nut_at_gcors
end module pdes