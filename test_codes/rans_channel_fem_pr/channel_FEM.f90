program channel_FEM
  use pdes
  use sharevars
  use support
  implicit none
  
  integer :: elem, locnode, edge, step, quad_order, p, no_out_points
  real(dp) :: ddy, y_0, y_end
  real(dp), allocatable :: gcors(:)
  real(dp), allocatable :: u_values(:), tau_values(:), nu_t_values(:)
  real(dp) :: time_start, time_end
  integer :: unit_no, ios
  character(200) :: msg
  
  ! Set up arrays of dofs.
  ! These arrays list the ordering of sol vectors and rhs vectors
  do elem = 1, no_elem
    do locnode = 1, nnod_u
      dofs_u(locnode,elem) = nnod_u * (elem-1) + locnode
    enddo
  enddo
  
  
  ! Build grid (domain from 0 to H/2)
  ! This is a uniform distribution
  dy = (H/2.0_dp)/no_elem
  y_coord(1) = 0.0_dp
  do edge = 2, no_edges
    y_coord(edge) = y_coord(edge-1) + dy(edge-1)
  enddo
  if (abs(y_coord(no_edges) - H/2.0_dp) >= SMALL) error stop 'problem in generating the grid'
  
  ! Set up quadrature
  quad_order = 2 * order_u
  call legendre_set(quad_order,qps,qws)
  
  ! Init solutions
  call init_solution_to_value('velocity',0.2_dp,u)
  u_old = u
  allocate(nu_t(no_elem,size(qps)))
  
  
  ! Solve RANS equations
  call cpu_time(time_start)
    do step = 1, no_steps
      print *, '**************************************************'
      print *, 'time step = ', step
      print *
      
      call calc_nu_t()
      call calc_u()
      
      !Prepare for next step
      u_old = u
    enddo
  call cpu_time(time_end)
  print *
  print *
  print *, 'Execution time = ', time_end - time_start
  
  ! Output solution
  ddy = 1.0e-3_dp
  y_0 = 0.0_dp
  y_end = H/2.0_dp
  no_out_points = nint((y_end-y_0)/ddy) + 1
  allocate(gcors(no_out_points))
  gcors(1) = y_0
  do p = 2, no_out_points
    gcors(p) = gcors(p-1) + ddy
  enddo
  
  call compute_solution_at_gcors('velocity',gcors,values=u_values)
  call compute_tau_and_nut_at_gcors(gcors,tau_values,nu_t_values)
  
  open(file='fem_1D_profiles.dat',newunit=unit_no,status='replace',action='write',iostat=ios,iomsg=msg)
    if (ios /=0) error stop 'problem in opening the output file'
      
      do p = 1, no_out_points
        write(unit_no,'(4es12.5)',iostat=ios,iomsg=msg) gcors(p), u_values(p), nu_t_values(p), tau_values(p)
        if (ios /=0) error stop 'problem in writing in the output file'
      enddo
    
    close(unit_no,status='keep',iostat=ios,iomsg=msg)
    if (ios /=0) error stop 'problem in closing the output file'
end program channel_FEM