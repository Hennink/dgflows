program wall
implicit none

integer, parameter :: N=100
integer, parameter :: No_steps=100
double precision, parameter :: D_min=1.d-3
double precision, parameter :: L=2.d0
double precision, parameter :: dt=0.d-2 !1.d-2
double precision :: dx,D_left,D_right
double precision, dimension(N) :: u,u_old,u_1000, u_999
double precision, dimension(N+1) :: D_on_faces
double precision, dimension(N) :: rhs
double precision, dimension(N,N) :: Matrix
integer, dimension(N) :: ipiv
integer :: p,info,i,step

dx = L/N

u     = 0.d0
u_old = 0.d0
u_999 = 0.d0
u_1000= 0.d0

p = 3

D_on_faces = 1.d0 ! Init D to a constant

! Calc the matrix and rhs

do step=1,No_steps

  rhs = 1.d0 * dx
  Matrix = 0.d0

  do i=1,N

    ! d/dt

    Matrix(i,i) = Matrix(i,i)! + dx/dt
    rhs(i) = rhs(i)! + (dx/dt) * u_old(i)

    ! left diffusion

    D_left = D_on_faces(i)
    if (i == 1) then
      Matrix(i,i)   = Matrix(i,i)   + D_left/(dx/2.d0)
    else
      Matrix(i,i)   = Matrix(i,i)   + D_left/dx
      Matrix(i,i-1) = Matrix(i,i-1) - D_left/dx
    endif

    ! right diffusion

    D_right = D_on_faces(i+1)
    if (i == N) then
      Matrix(i,i)   = Matrix(i,i)   + D_right/(dx/2.d0)
    else
      Matrix(i,i)   = Matrix(i,i)   + D_right/dx
      Matrix(i,i+1) = Matrix(i,i+1) - D_right/dx
    endif
  enddo

  ! Solve system

  call dgesv(N,1,Matrix,N,ipiv,rhs,N,info)
  u = rhs
  if (info /= 0) STOP 'solver problem'

  ! Update D

  D_on_faces(1) = ABS(u(1) / (dx/2.d0))
  do i=2,N
    D_on_faces(i) = ABS((u(i) - u(i-1)) / dx)
  enddo
  D_on_faces(N+1) = ABS(u(N) / (dx/2.d0))

  D_on_faces = D_on_faces**(p-2) + D_min

  ! Time copy
  
  if (step /= No_steps) u_old = u
  
  if (step == No_steps/2.0) u_1000 = u
  if (step == (No_steps/2.0-1)) u_999 = u
enddo

! Plot u

do i=1,N
  print *,(i-0.5d0) * dx,u(i),u_old(i),u_1000(i),u_999(i)
enddo

! Plot D

!do i=1,N+1
!  print *,i * dx,D_on_faces(i)
!enddo

end program wall
