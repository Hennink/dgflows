module sharevars
  use, intrinsic :: iso_fortran_env, only: real64
  implicit none
  
  integer, parameter :: dp = real64
  real(dp), parameter :: SMALL = sqrt(epsilon(1.0_dp))
  
  integer, parameter :: no_tot_iter = 1
  integer, parameter :: no_turb_iter = 1
  
  integer, parameter  :: no_elem = 20
  integer, parameter  :: no_edges = no_elem + 1
  real(dp), parameter :: H = 1.0_dp
  real(dp), parameter :: dt = 5.0e-2_dp
  integer, parameter  :: no_steps = 100000
  
  real(dp), parameter :: penalty_safety_factor = 1.0_dp
  
  real(dp), parameter :: C1_eps = 1.44_dp
  real(dp), parameter :: C2_eps = 1.92_dp
  real(dp), parameter :: sigma_k = 1.0_dp
  real(dp), parameter :: sigma_eps = 1.3_dp
  real(dp), parameter :: Kappa = 0.41_dp
  real(dp), parameter :: C_mu = 0.09_dp
  real(dp), parameter :: E = 9.0_dp
  real(dp), parameter :: nu_lam = 2.5e-5_dp
  real(dp), parameter :: dpdx = -0.001332117850942_dp
  
  integer, parameter :: order_u = 2
  integer, parameter :: order_k = 1
  integer, parameter :: order_eps = 1
  
  integer, parameter  :: nnod_u = order_u + 1
  integer, parameter  :: nnod_k = order_k + 1
  integer, parameter  :: nnod_eps = order_eps + 1
  integer, parameter  :: no_dofs_u = nnod_u * no_elem
  integer, parameter  :: no_dofs_k = nnod_k * no_elem
  integer, parameter  :: no_dofs_eps = nnod_eps * no_elem
  
  real(dp), dimension(no_dofs_u), target :: u, u_old
  real(dp), dimension(no_dofs_k), target :: k, k_old
  real(dp), dimension(no_dofs_eps), target :: eps, eps_old
  real(dp), allocatable :: nu_t(:,:)
  real(dp), dimension(no_elem,2) :: nu_t_edge
  real(dp), allocatable :: Pk(:,:)
  real(dp) :: y_coord(no_edges), dy(no_elem), delta
  
  integer, target :: dofs_u(nnod_u,no_elem)
  integer, target :: dofs_k(nnod_k,no_elem)
  integer, target :: dofs_eps(nnod_eps,no_elem)
  
  real(dp), allocatable :: qps(:), qws(:)
end module sharevars
