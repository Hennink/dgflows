program channel_FEM
  use pdes
  use sharevars
  use support
  implicit none
  
  integer :: elem, locnode, edge, step, quad_order, p, no_out_points
  real(dp) :: ddy, y_0, y_end
  real(dp), allocatable :: gcors(:)
  real(dp), allocatable :: u_values(:), k_values(:), eps_values(:), tau_values(:)
  real(dp) :: time_start, time_end
  integer :: unit_no, ios, tot_iter, turb_iter
  character(200) :: msg
  
  ! Set up arrays of dofs.
  ! These arrays list the ordering of sol vectors and rhs vectors
  do elem = 1, no_elem
    do locnode = 1, nnod_u
      dofs_u(locnode,elem) = nnod_u * (elem-1) + locnode
    enddo
    do locnode = 1, nnod_k
      dofs_k(locnode,elem) = nnod_k * (elem-1) + locnode
    enddo
    do locnode = 1, nnod_eps
      dofs_eps(locnode,elem) = nnod_eps * (elem-1) + locnode
    enddo
  enddo
  
  
  ! Build grid (domain from 0 to H/2)
  ! This is a uniform distribution
  dy = (H/2.0_dp)/no_elem
  y_coord(1) = 0.0_dp
  do edge = 2, no_edges
    y_coord(edge) = y_coord(edge-1) + dy(edge-1)
  enddo
  if (abs(y_coord(no_edges) - H/2.0_dp) >= SMALL) error stop 'problem in generating the grid'
  delta = dy(1) / 2.0_dp
  
  ! Set up quadrature
  quad_order = 3 * max(order_u,order_k,order_eps)
  call legendre_set(quad_order,qps,qws)
  
  ! Init solutions
  call init_solution_to_value('velocity',1.0_dp,u)
  call init_solution_to_value('turb_k',1.0_dp,k)
  call init_solution_to_value('turb_eps',1.0_dp,eps)
  u_old = u
  k_old = k
  eps_old = eps
  allocate(nu_t(no_elem,size(qps)))
  allocate(Pk(no_elem,size(qps)))
  
  
  ! Solve RANS equations
  call cpu_time(time_start)
    do step = 1, no_steps
      print *, '**************************************************'
      print *, 'time step = ', step
      print *
      
      do tot_iter = 1, no_tot_iter
        call calc_nu_t()
        call calc_u()
        call calc_Pk()
        do turb_iter = 1, no_turb_iter
          call calc_k()
          call calc_eps()
        enddo
      enddo
      
      !Prepare for next step
      u_old = u
      k_old = k
      eps_old = eps
    enddo
  call cpu_time(time_end)
  print *
  print *
  print *, 'Execution time = ', time_end - time_start
  
  ! Output solution
  ddy = 1.0e-3_dp
  y_0 = 0.0_dp
  y_end = H/2.0_dp
  no_out_points = nint((y_end-y_0)/ddy) + 1
  allocate(gcors(no_out_points))
  gcors(1) = y_0
  do p = 2, no_out_points
    gcors(p) = gcors(p-1) + ddy
  enddo
  
  call compute_solution_at_gcors('velocity',gcors,values=u_values)
  call compute_solution_at_gcors('turb_k',gcors,values=k_values)
  call compute_solution_at_gcors('turb_eps',gcors,values=eps_values)
  call compute_tau_at_gcors(gcors,tau_values)
  
  open(file='fem_1D_profiles.dat',newunit=unit_no,status='replace',action='write',iostat=ios,iomsg=msg)
    if (ios /=0) error stop 'problem in opening the output file'
      
      do p = 1, no_out_points
        write(unit_no,'(5es12.5)',iostat=ios,iomsg=msg) gcors(p), u_values(p), k_values(p), eps_values(p), tau_values(p)
        if (ios /=0) error stop 'problem in writing in the output file'
      enddo
    
    close(unit_no,status='keep',iostat=ios,iomsg=msg)
    if (ios /=0) error stop 'problem in closing the output file'
end program channel_FEM