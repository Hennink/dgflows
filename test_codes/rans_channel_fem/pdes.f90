module pdes
  use sharevars
  use support
  implicit none
  
contains
  subroutine calc_u()
    integer :: info, elem, edge, locnodev, locnodeu, elem_v, elem_u
    integer :: row, col, row_1st, col_1st, row_end, col_end
    real(dp) :: rhs(no_dofs_u), ipiv(no_dofs_u)
    real(dp) :: M(no_dofs_u,no_dofs_u)
    real(dp) :: weight_at_qps(size(qps))
    real(dp), allocatable :: dpdx_x_f(:)
    real(dp), allocatable :: fxf(:,:)
    real(dp), allocatable :: edge_gradf_right(:), edge_f_right(:)
    real(dp), allocatable :: edge_fxf_left(:,:)
    real(dp), allocatable :: edge_fxf_right(:,:)
    real(dp), allocatable :: edge_fxf_mixed(:,:)
    real(dp), allocatable :: Dxgradfxgradf(:,:)
    real(dp), allocatable :: edge_fxgradf_left(:,:)
    real(dp), allocatable :: edge_fxgradf_right(:,:)
    real(dp), allocatable :: edge_fxgradf_mixed(:,:)
    real(dp), allocatable :: edge_gradfxf_mixed(:,:)
    real(dp) :: delta_plus, shear_stress_coeff
    real(dp) :: k_w
    
    M = 0.0_dp
    rhs = 0.0_dp
    
    do elem = 1, no_elem
      row_1st = nnod_u*(elem-1)+1
      row_end = nnod_u*elem
      col_1st = nnod_u*(elem-1)+1
      col_end = nnod_u*elem
      
      ! Time derivative
      call calculate_vol_integral_fxf('velocity',elem,fxf)
      M(row_1st:row_end,col_1st:col_end) = M(row_1st:row_end,col_1st:col_end) + fxf / dt
      rhs(row_1st:row_end) = rhs(row_1st:row_end) + matmul(fxf,u_old(row_1st:row_end)) / dt
      
      ! Source term, dp/dx
      weight_at_qps = dpdx
      call calculate_vol_integral_f('velocity',elem,dpdx_x_f,weight_at_qps)
      rhs(row_1st:row_end) = rhs(row_1st:row_end) - dpdx_x_f
      
      ! Stress term: Volumetric term
      weight_at_qps = nu_lam + nu_t(elem,:)
      call calculate_vol_integral_gradfxgradf('velocity',elem,weight_at_qps,Dxgradfxgradf)
      M(row_1st:row_end,col_1st:col_end) = M(row_1st:row_end,col_1st:col_end) + Dxgradfxgradf
    enddo
    
    do edge = 1, no_edges
      ! Stress term:  edges terms
      call calculate_face_integrals('velocity',edge,edge_f_right,edge_gradf_right,edge_fxf_left,edge_fxf_right,edge_fxf_mixed,&
                                               edge_fxgradf_left,edge_fxgradf_right,edge_fxgradf_mixed,edge_gradfxf_mixed)
      
      ! Boundary edges terms
      if (edge == 1) then
        ! Shear stress imposed via wall function
        elem_v = edge
        elem_u = edge
        call compute_solution_at_lcor('turb_k',elem_u,-1.0_dp,k_w)
        delta_plus = delta * u_k(k_w) / nu_lam
        shear_stress_coeff = u_k(k_w) / max(u_plus(delta_plus),SMALL)
        do locnodeu = 1, nnod_u! u-index
          col = dofs_u(locnodeu,elem_u)
          do locnodev = 1, nnod_u   ! v_index
            row = dofs_u(locnodev,elem_v)
            M(row,col) = M(row,col) + shear_stress_coeff*edge_fxf_right(locnodev,locnodeu)
          enddo
        enddo
      elseif (edge == no_edges) then
        ! Do nothing, because symmetry bc ==> homogeneous Neumann bc
      else
        ! Internal edges terms
        ! v_i chosen left of edge
        elem_v = edge-1
        elem_u = edge-1
        do locnodeu = 1, nnod_u ! u-index
          col = dofs_u(locnodeu,elem_u)
          do locnodev = 1, nnod_u   ! v_index
            row = dofs_u(locnodev,elem_v)
            M(row,col) = M(row,col) + penalty(edge,'velocity')*edge_fxf_left(locnodev,locnodeu)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_u,2))*edge_fxgradf_left(locnodev,locnodeu)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_v,2))*edge_fxgradf_left(locnodeu,locnodev)
          enddo
        enddo
        
        elem_u = edge
        do locnodeu = 1, nnod_u ! u-index
          col = dofs_u(locnodeu,elem_u)
          do locnodev = 1, nnod_u   ! v_index
            row = dofs_u(locnodev,elem_v)
            M(row,col) = M(row,col) - penalty(edge,'velocity')*edge_fxf_mixed(locnodev,locnodeu)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_u,1))*edge_fxgradf_mixed(locnodev,locnodeu)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_v,2))*edge_gradfxf_mixed(locnodev,locnodeu)
          enddo
        enddo
        
        ! v_i chosen right of edge 
        elem_v = edge
        elem_u = edge-1
        do locnodeu = 1, nnod_u ! u-index
          col = dofs_u(locnodeu,elem_u)
          do locnodev = 1, nnod_u   ! v_index
            row = dofs_u(locnodev,elem_v)
            M(row,col) = M(row,col) - penalty(edge,'velocity')*edge_fxf_mixed(locnodeu,locnodev)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_u,2))*edge_gradfxf_mixed(locnodeu,locnodev)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_v,1))*edge_fxgradf_mixed(locnodeu,locnodev)
          enddo
        enddo
        
        elem_u = edge
        do locnodeu= 1, nnod_u ! u-index
          col = dofs_u(locnodeu,elem_u)
          do locnodev = 1, nnod_u   ! v_index
            row = dofs_u(locnodev,elem_v)
            M(row,col) = M(row,col) + penalty(edge,'velocity')*edge_fxf_right(locnodev,locnodeu)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_u,1))*edge_fxgradf_right(locnodev,locnodeu)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_v,1))*edge_fxgradf_right(locnodeu,locnodev)
          enddo
        enddo
      endif
    enddo
    
    
    ! Symmetry test of matrix
    do row = 1, no_dofs_u
      do col = 1, row-1
        if (abs(M(row,col) - M(col,row)) > SMALL) then 
          print *,'calc_u: asymmetry detected',row,col, abs(M(row,col) - M(col,row))
          error stop
        endif
      enddo
    enddo
    
    ! Call solver
    call dgesv(no_dofs_u,1,M,no_dofs_u,ipiv,rhs,no_dofs_u,info)
    if (info /= 0) error stop 'calc_u: WARNING in solver'
    u = rhs
    
    print *, 'max dU/dt = ', solution_max_ddt('velocity')
  end subroutine calc_u
  
  real(dp) function u_k(k)
    real(dp), intent(in) :: k
    
    u_k = (C_mu**0.25_dp) * sqrt(max(k,SMALL))
  end function u_k
  
  real(dp) function u_plus(y_plus)
    real(dp), intent(in) :: y_plus
    
    real(dp), parameter :: y_plus_c = 11.06_dp
    
    if (y_plus >= y_plus_c) then
      u_plus = log(E * y_plus) / Kappa
    else
      u_plus = y_plus
    endif
  end function u_plus
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine calc_k()
    integer :: info, elem, edge, locnodev, locnodeu, elem_v, elem_u
    integer :: row, col, row_1st, col_1st, row_end, col_end
    real(dp) :: rhs(no_dofs_k), ipiv(no_dofs_k)
    real(dp) :: M(no_dofs_k,no_dofs_k)
    real(dp) :: weight_at_qps(size(qps)), eps_at_qps(size(qps)), k_at_qps(size(qps))
    real(dp), allocatable :: Pk_x_f(:), eps_x_f(:)
    real(dp), allocatable :: fxf(:,:), eps_over_k_x_fxf(:,:)
    real(dp), allocatable :: edge_gradf_right(:), edge_f_right(:)
    real(dp), allocatable :: edge_fxf_left(:,:)
    real(dp), allocatable :: edge_fxf_right(:,:)
    real(dp), allocatable :: edge_fxf_mixed(:,:)
    real(dp), allocatable :: Dxgradfxgradf(:,:)
    real(dp), allocatable :: edge_fxgradf_left(:,:)
    real(dp), allocatable :: edge_fxgradf_right(:,:)
    real(dp), allocatable :: edge_fxgradf_mixed(:,:)
    real(dp), allocatable :: edge_gradfxf_mixed(:,:)
    
    M = 0.0_dp
    rhs = 0.0_dp
    
    do elem = 1, no_elem
      row_1st = nnod_k*(elem-1)+1
      row_end = nnod_k*elem
      col_1st = nnod_k*(elem-1)+1
      col_end = nnod_k*elem
      
      ! Time derivative
      call calculate_vol_integral_fxf('turb_k',elem,fxf)
      M(row_1st:row_end,col_1st:col_end) = M(row_1st:row_end,col_1st:col_end) + fxf / dt
      rhs(row_1st:row_end) = rhs(row_1st:row_end) + matmul(fxf,k_old(row_1st:row_end)) / dt
    
      ! Production
      weight_at_qps = Pk(elem,:)
      call calculate_vol_integral_f('turb_k',elem,Pk_x_f,weight_at_qps)
      rhs(row_1st:row_end) = rhs(row_1st:row_end) + Pk_x_f
      
      ! Dissipation
      call compute_solution_at_elem_qps('turb_eps',elem,eps_at_qps)
      call compute_solution_at_elem_qps('turb_k',elem,k_at_qps)
      weight_at_qps = 2.0_dp * eps_at_qps / k_at_qps
      call calculate_vol_integral_fxf('turb_k',elem,eps_over_k_x_fxf,weight_at_qps)
      M(row_1st:row_end,col_1st:col_end) = M(row_1st:row_end,col_1st:col_end) + eps_over_k_x_fxf
      call calculate_vol_integral_f('turb_k',elem,eps_x_f,eps_at_qps)
      rhs(row_1st:row_end) = rhs(row_1st:row_end) + eps_x_f
      
      ! Diffusion: Volumetric term
      weight_at_qps = nu_lam + nu_t(elem,:) / sigma_k
      call calculate_vol_integral_gradfxgradf('turb_k',elem,weight_at_qps,Dxgradfxgradf)
      M(row_1st:row_end,col_1st:col_end) = M(row_1st:row_end,col_1st:col_end) + Dxgradfxgradf
    enddo
    
    do edge = 1, no_edges
      ! Diffusion:  edges terms
      call calculate_face_integrals('turb_k',edge,edge_f_right,edge_gradf_right,edge_fxf_left,edge_fxf_right,edge_fxf_mixed,&
                                             edge_fxgradf_left,edge_fxgradf_right,edge_fxgradf_mixed,edge_gradfxf_mixed)
      
      ! Boundary edges terms
      if (edge == 1 .or. edge == no_edges) then
        ! Do nothing, because of symmetry bc and homogeneous Neumann bc at the wall
      else
        ! Internal edges terms
        ! v_i chosen left of edge
        elem_v = edge-1
        elem_u = edge-1
        do locnodeu = 1, nnod_k ! u-index
          col = dofs_k(locnodeu,elem_u)
          do locnodev = 1, nnod_k   ! v_index
            row = dofs_k(locnodev,elem_v)
            M(row,col) = M(row,col) + penalty(edge,'turb_k')*edge_fxf_left(locnodev,locnodeu)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_u,2) / sigma_k)*edge_fxgradf_left(locnodev,locnodeu)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_v,2) / sigma_k)*edge_fxgradf_left(locnodeu,locnodev)
          enddo
        enddo
        
        elem_u = edge
        do locnodeu = 1, nnod_k ! u-index
          col = dofs_k(locnodeu,elem_u)
          do locnodev = 1, nnod_k   ! v_index
            row = dofs_k(locnodev,elem_v)
            M(row,col) = M(row,col) - penalty(edge,'turb_k')*edge_fxf_mixed(locnodev,locnodeu)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_u,1) / sigma_k)*edge_fxgradf_mixed(locnodev,locnodeu)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_v,2) / sigma_k)*edge_gradfxf_mixed(locnodev,locnodeu)
          enddo
        enddo
        
        ! v_i chosen right of edge 
        elem_v = edge
        elem_u = edge-1
        do locnodeu = 1, nnod_k ! u-index
          col = dofs_k(locnodeu,elem_u)
          do locnodev = 1, nnod_k   ! v_index
            row = dofs_k(locnodev,elem_v)
            M(row,col) = M(row,col) - penalty(edge,'turb_k')*edge_fxf_mixed(locnodeu,locnodev)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_u,2) / sigma_k)*edge_gradfxf_mixed(locnodeu,locnodev)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_v,1) / sigma_k)*edge_fxgradf_mixed(locnodeu,locnodev)
          enddo
        enddo
        
        elem_u = edge
        do locnodeu= 1, nnod_k ! u-index
          col = dofs_k(locnodeu,elem_u)
          do locnodev = 1, nnod_k   ! v_index
            row = dofs_k(locnodev,elem_v)
            M(row,col) = M(row,col) + penalty(edge,'turb_k')*edge_fxf_right(locnodev,locnodeu)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_u,1) / sigma_k)*edge_fxgradf_right(locnodev,locnodeu)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_v,1) / sigma_k)*edge_fxgradf_right(locnodeu,locnodev)
          enddo
        enddo
      endif
    enddo
    
    
    ! Symmetry test of matrix
    do row = 1, no_dofs_k
      do col = 1, row-1
        if (abs(M(row,col) - M(col,row)) > SMALL) then 
          print *,'calc_k: asymmetry detected',row,col, abs(M(row,col) - M(col,row))
          error stop
        endif
      enddo
    enddo
    
    ! Call solver
    call dgesv(no_dofs_k,1,M,no_dofs_k,ipiv,rhs,no_dofs_k,info)
    if (info /= 0) error stop 'calc_k: WARNING in solver'
    k = rhs
    
    print *, 'max dk/dt = ', solution_max_ddt('turb_k')
  end subroutine calc_k
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine calc_eps()
    integer :: info, elem, edge, locnodev, locnodeu, elem_v, elem_u
    integer :: row, col, row_1st, col_1st, row_end, col_end
    real(dp) :: rhs(no_dofs_eps), ipiv(no_dofs_eps)
    real(dp) :: M(no_dofs_eps,no_dofs_eps)
    real(dp) :: weight_at_qps(size(qps)), eps_at_qps(size(qps)), k_at_qps(size(qps))
    real(dp), allocatable :: Pk_x_f(:), eps_x_f(:)
    real(dp), allocatable :: fxf(:,:), eps_over_k_x_fxf(:,:)
    real(dp), allocatable :: edge_gradf_right(:), edge_f_right(:)
    real(dp), allocatable :: edge_fxf_left(:,:)
    real(dp), allocatable :: edge_fxf_right(:,:)
    real(dp), allocatable :: edge_fxf_mixed(:,:)
    real(dp), allocatable :: Dxgradfxgradf(:,:)
    real(dp), allocatable :: edge_fxgradf_left(:,:)
    real(dp), allocatable :: edge_fxgradf_right(:,:)
    real(dp), allocatable :: edge_fxgradf_mixed(:,:)
    real(dp), allocatable :: edge_gradfxf_mixed(:,:)
    real(dp) :: eps_w, k_w
    
    M = 0.0_dp
    rhs = 0.0_dp
    
    do elem = 1, no_elem
      row_1st = nnod_eps*(elem-1)+1
      row_end = nnod_eps*elem
      col_1st = nnod_eps*(elem-1)+1
      col_end = nnod_eps*elem
      
      ! Time derivative
      call calculate_vol_integral_fxf('turb_eps',elem,fxf)
      M(row_1st:row_end,col_1st:col_end) = M(row_1st:row_end,col_1st:col_end) + fxf / dt
      rhs(row_1st:row_end) = rhs(row_1st:row_end) + matmul(fxf,eps_old(row_1st:row_end)) / dt
    
      ! Production
      call compute_solution_at_elem_qps('turb_k',elem,k_at_qps)
      call compute_solution_at_elem_qps('turb_eps',elem,eps_at_qps)
      weight_at_qps = C1_eps * Pk(elem,:) * eps_at_qps / k_at_qps
      call calculate_vol_integral_f('turb_eps',elem,Pk_x_f,weight_at_qps)
      rhs(row_1st:row_end) = rhs(row_1st:row_end) + Pk_x_f
      
      ! Dissipation
      weight_at_qps = 2.0_dp * C2_eps * eps_at_qps / k_at_qps
      call calculate_vol_integral_fxf('turb_eps',elem,eps_over_k_x_fxf,weight_at_qps)
      M(row_1st:row_end,col_1st:col_end) = M(row_1st:row_end,col_1st:col_end) + eps_over_k_x_fxf
      weight_at_qps = C2_eps * (eps_at_qps**2) / k_at_qps
      call calculate_vol_integral_f('turb_eps',elem,eps_x_f,weight_at_qps)
      rhs(row_1st:row_end) = rhs(row_1st:row_end) + eps_x_f
      
      ! Diffusion: Volumetric term
      weight_at_qps = nu_lam + nu_t(elem,:) / sigma_eps
      call calculate_vol_integral_gradfxgradf('turb_eps',elem,weight_at_qps,Dxgradfxgradf)
      M(row_1st:row_end,col_1st:col_end) = M(row_1st:row_end,col_1st:col_end) + Dxgradfxgradf
    enddo
    
    do edge = 1, no_edges
      ! Diffusion:  edges terms
      call calculate_face_integrals('turb_eps',edge,edge_f_right,edge_gradf_right,edge_fxf_left,edge_fxf_right,edge_fxf_mixed,&
                                               edge_fxgradf_left,edge_fxgradf_right,edge_fxgradf_mixed,edge_gradfxf_mixed)
      
      ! Boundary edges terms
      if (edge == 1) then
        ! Inhomogeneous Dirichlet bc: eps_w = u_k^3/(kappa*delta)
        elem_v = edge
        elem_u = edge
        do locnodeu = 1, nnod_eps! u-index
          col = dofs_eps(locnodeu,elem_u)
          do locnodev = 1, nnod_eps   ! v_index
            row = dofs_eps(locnodev,elem_v)
            M(row,col) = M(row,col) + penalty(edge,'turb_eps')*edge_fxf_right(locnodev,locnodeu)
            M(row,col) = M(row,col) + (nu_lam + nu_t_edge(elem_u,1) / sigma_eps)*edge_fxgradf_right(locnodev,locnodeu)
            M(row,col) = M(row,col) + (nu_lam + nu_t_edge(elem_v,1) / sigma_eps)*edge_fxgradf_right(locnodeu,locnodev)
          enddo
        enddo
        call compute_solution_at_lcor('turb_k',elem_u,-1.0_dp,k_w)
        eps_w = u_k(k_w)**3 / (Kappa * delta)
        do locnodev = 1, nnod_eps   ! v_index
          row = dofs_eps(locnodev,elem_v)
          rhs(row) = rhs(row) + penalty(edge,'turb_eps')*eps_w*edge_f_right(locnodev)
          rhs(row) = rhs(row) + eps_w*(nu_lam + nu_t_edge(elem_v,1) / sigma_eps)*edge_gradf_right(locnodev)
        enddo
      elseif (edge == no_edges) then
        ! Do nothing, because symmetry bc ==> homogeneous Neumann bc
      else
        ! Internal edges terms
        ! v_i chosen left of edge
        elem_v = edge-1
        elem_u = edge-1
        do locnodeu = 1, nnod_eps ! u-index
          col = dofs_eps(locnodeu,elem_u)
          do locnodev = 1, nnod_eps   ! v_index
            row = dofs_eps(locnodev,elem_v)
            M(row,col) = M(row,col) + penalty(edge,'turb_eps')*edge_fxf_left(locnodev,locnodeu)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_u,2) / sigma_eps)*edge_fxgradf_left(locnodev,locnodeu)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_v,2) / sigma_eps)*edge_fxgradf_left(locnodeu,locnodev)
          enddo
        enddo
        
        elem_u = edge
        do locnodeu = 1, nnod_eps ! u-index
          col = dofs_eps(locnodeu,elem_u)
          do locnodev = 1, nnod_eps   ! v_index
            row = dofs_eps(locnodev,elem_v)
            M(row,col) = M(row,col) - penalty(edge,'turb_eps')*edge_fxf_mixed(locnodev,locnodeu)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_u,1) / sigma_eps)*edge_fxgradf_mixed(locnodev,locnodeu)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_v,2) / sigma_eps)*edge_gradfxf_mixed(locnodev,locnodeu)
          enddo
        enddo
        
        ! v_i chosen right of edge 
        elem_v = edge
        elem_u = edge-1
        do locnodeu = 1, nnod_eps ! u-index
          col = dofs_eps(locnodeu,elem_u)
          do locnodev = 1, nnod_eps   ! v_index
            row = dofs_eps(locnodev,elem_v)
            M(row,col) = M(row,col) - penalty(edge,'turb_eps')*edge_fxf_mixed(locnodeu,locnodev)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_u,2) / sigma_eps)*edge_gradfxf_mixed(locnodeu,locnodev)
            M(row,col) = M(row,col) - 0.5_dp*(nu_lam + nu_t_edge(elem_v,1) / sigma_eps)*edge_fxgradf_mixed(locnodeu,locnodev)
          enddo
        enddo
        
        elem_u = edge
        do locnodeu= 1, nnod_eps ! u-index
          col = dofs_eps(locnodeu,elem_u)
          do locnodev = 1, nnod_eps   ! v_index
            row = dofs_eps(locnodev,elem_v)
            M(row,col) = M(row,col) + penalty(edge,'turb_eps')*edge_fxf_right(locnodev,locnodeu)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_u,1) / sigma_eps)*edge_fxgradf_right(locnodev,locnodeu)
            M(row,col) = M(row,col) + 0.5_dp*(nu_lam + nu_t_edge(elem_v,1) / sigma_eps)*edge_fxgradf_right(locnodeu,locnodev)
          enddo
        enddo
      endif
    enddo
    
    
    ! Symmetry test of matrix
    do row = 1, no_dofs_eps
      do col = 1, row-1
        if (abs(M(row,col) - M(col,row)) > SMALL) then 
          print *,'calc_eps: asymmetry detected',row,col, abs(M(row,col) - M(col,row))
          error stop
        endif
      enddo
    enddo
    
    ! Call solver
    call dgesv(no_dofs_eps,1,M,no_dofs_eps,ipiv,rhs,no_dofs_eps,info)
    if (info /= 0) error stop 'calc_eps: WARNING in solver'
    eps = rhs
    
    print *, 'max deps/dt = ', solution_max_ddt('turb_eps')
  end subroutine calc_eps
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine calc_nu_t()
    integer :: elem
    
    real(dp), dimension(size(qps)) :: k_at_qps, eps_at_qps
     real(dp) :: k_l, k_r, eps_l, eps_r
    
    do elem = 1, no_elem
      ! Compute nu_t at elem quad points
      call compute_solution_at_elem_qps('turb_k',elem,k_at_qps)
      call compute_solution_at_elem_qps('turb_eps',elem,eps_at_qps)
      nu_t(elem,:) = max(C_mu * k_at_qps**2 / eps_at_qps,1.e-4*nu_lam)
      
      ! Compute nu_t at faces quad points
      call compute_solution_at_lcor('turb_k',elem,-1.0_dp,k_l)
      call compute_solution_at_lcor('turb_k',elem,+1.0_dp,k_r)
      call compute_solution_at_lcor('turb_eps',elem,-1.0_dp,eps_l)
      call compute_solution_at_lcor('turb_eps',elem,+1.0_dp,eps_r)
      nu_t_edge(elem,1) = max(C_mu * k_l**2 / eps_l,1.e-4*nu_lam)
      nu_t_edge(elem,2) = max(C_mu * k_r**2 / eps_r,1.e-4*nu_lam)
    enddo
  end subroutine calc_nu_t
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine calc_Pk()
    integer :: elem
    real(dp), dimension(size(qps)) :: dudy_at_qps
    
    do elem = 1, no_elem
      call compute_grad_solution_at_elem_qps('velocity',elem,dudy_at_qps)
      Pk(elem,:) = nu_t(elem,:) * dudy_at_qps**2
    enddo
    if (any(Pk<0.0_dp)) error stop 'Pk cannot be negative!'
  end subroutine calc_Pk
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  subroutine compute_tau_at_gcors(gcors,tau_values)
    real(dp), intent(in) :: gcors(:)
    real(dp), allocatable, intent(out) :: tau_values(:)
    
    real(dp), allocatable :: grads_u(:), k_values(:), eps_values(:), nut_values(:)
    integer :: p
    
    call compute_solution_at_gcors('velocity',gcors,grads=grads_u)
    call compute_solution_at_gcors('turb_k',gcors,values=k_values)
    call compute_solution_at_gcors('turb_eps',gcors,values=eps_values)
    
    allocate(nut_values,source=C_mu*(k_values**2)/eps_values)
    
    allocate(tau_values(size(gcors)))
    do p = 1, size(gcors)
      nut_values(p) = max(nut_values(p),1.e-4_dp*nu_lam)
      tau_values(p) = max((nu_lam + nut_values(p)) * grads_u(p),0.0_dp)
    enddo
  end subroutine compute_tau_at_gcors
end module pdes