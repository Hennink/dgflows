program channel
use sharevars
use pdes
implicit none

integer :: step,j

! Build grid

dy = H/N
do j=1,N
  y(j) = (j-0.5d0) * dy
enddo
!print *,'y',y

! Init

u=1.d0
k=1.d0
eps=1.d0

! Time step

do step=1,N_steps
  call calc_nu_t
  call calc_u
  call calc_Pk
  call calc_k
  call calc_eps
enddo
call calc_nu_t
call calc_u
call calc_tau

! print solution

do j=1,N
  print *, y(j),u(j),k(j),eps(j),tau(j)
enddo

end program channel
