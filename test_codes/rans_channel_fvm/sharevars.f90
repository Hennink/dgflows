module sharevars
implicit none

integer, parameter :: N=20
integer, parameter :: N_steps=1000000
double precision, parameter :: H=0.5d0
double precision, parameter :: dt=2.d-3
double precision :: u(N),k(N),eps(N),nu_t(N),Pk(N),tau(N)
double precision :: y(N),dy

end module sharevars
