module pdes
use sharevars

double precision, parameter :: C1_eps = 1.44d0
double precision, parameter :: C2_eps = 1.92d0
double precision, parameter :: sigma_k = 1.d0
double precision, parameter :: sigma_eps = 1.3d0
double precision, parameter :: Kappa = 0.41d0
double precision, parameter :: C_mu = 0.09d0
double precision, parameter :: E = 9.d0
double precision, parameter :: nu_lam = 2.5d-5
double precision, parameter :: dpdx = -0.001373124117148d0

contains

subroutine calc_u()
implicit none

integer :: j,info
double precision :: yp,nu,help,coef,yp_plus
double precision :: diag(N)
double precision :: d_upp(N)
double precision :: d_low(N)
double precision :: rhs(N)

diag  = 0.d0
d_upp = 0.d0
d_low = 0.d0
rhs   = 0.d0

! d/dt

do j=1,N
  diag(j) = dy / dt
  rhs(j) = (dy / dt) * u(j)
enddo

! dp/dx

do j=1,N
  rhs(j) = rhs(j) - dpdx * dy
enddo

! Stress term

do j=1,N
  ! top of cell
  if (j /= N) then
    nu = (nu_t(j+1) + nu_t(j))/2.d0 + nu_lam
    help = nu/(y(j+1) - y(j))
    diag(j)  = diag(j)  + help
    d_upp(j) = d_upp(j) - help
  endif

  ! bottom of cell
  if (j /= 1) then
    nu = (nu_t(j) + nu_t(j-1))/2.d0 + nu_lam
    help = nu/(y(j) - y(j-1))
    diag(j)  = diag(j)  + help
    d_low(j) = d_low(j) - help
  else
    yp = dy / 2.d0
    yp_plus = yp * u_k(k(j)) / nu_lam
    coef = u_k(k(j)) / u_plus(yp_plus)
    diag(j)  = diag(j)  + coef
  endif
enddo

call dgtsv(N,1,d_low(2:N),diag,d_upp(1:N-1),rhs,N,info)
if (info /= 0) STOP 'Problem in Tridiag solver'
u = rhs

end subroutine calc_u

function u_k(k)
  double precision, intent(in) :: k
  double precision :: u_k
  
  u_k = (C_mu**0.25d0) * sqrt(k)
end function u_k

function u_plus(y_plus)
  double precision, intent(in) :: y_plus
  double precision :: u_plus
  
  double precision, parameter :: y_plus_c = 11.06d0
  
  if (y_plus >= y_plus_c) then
    u_plus = log(E * y_plus) / Kappa
  else
    u_plus = y_plus
  endif
end function u_plus

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine calc_k()
implicit none

integer :: j,info
double precision :: help,yp,nu,yp_plus
double precision :: diag(N)
double precision :: d_upp(N)
double precision :: d_low(N)
double precision :: rhs(N)

diag = 0.d0
d_upp = 0.d0
d_low = 0.d0
rhs = 0.d0

! d/dt

do j=1,N
  diag(j) = dy/dt
  rhs(j) = (dy / dt) * k(j)
enddo

! Production

do j=1,N
  rhs(j) = rhs(j) + Pk(j) * dy
enddo

! Dissipation

do j=1,N
  if (j /= 1) then
    diag(j) = diag(j) + (eps(j) / k(j)) * dy
  else
    ! Special case for first cell
    yp = dy / 2.d0
    yp_plus = yp * u_k(k(j)) / nu_lam
    diag(j) = diag(j) + ((u_k(k(j))**3) * (u_plus(yp_plus) / yp) / k(j)) * dy
  endif
enddo

! Diffusion

do j=1,N
  ! top of cell
  if (j /= N) then
    nu = (nu_t(j+1) + nu_t(j))/(2.d0*sigma_k) + nu_lam
    help = nu/(y(j+1) - y(j))
    diag(j)  = diag(j)  + help
    d_upp(j) = d_upp(j) - help
  endif

  ! bottom of cell
  if (j /= 1) then
    nu = (nu_t(j) + nu_t(j-1))/(2.d0*sigma_k) + nu_lam
    help = nu/(y(j) - y(j-1))
    diag(j)  = diag(j)  + help
    d_low(j) = d_low(j) - help
  endif
enddo

call dgtsv(N,1,d_low(2:N),diag,d_upp(1:N-1),rhs,N,info)
if (info /= 0) STOP 'Problem in Tridiag solver'
k = rhs

end subroutine calc_k

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine calc_eps()
implicit none

integer :: j,info
double precision :: nu,help,eps_wall,yp
double precision :: diag(N)
double precision :: d_upp(N)
double precision :: d_low(N)
double precision :: rhs(N)

diag = 0.d0
d_upp = 0.d0
d_low = 0.d0
rhs = 0.d0

! d/dt

do j=1,N
  diag(j) = dy/dt
  rhs(j) = (dy / dt) * eps(j)
enddo

! Production

do j=1,N
  rhs(j) = rhs(j) + C1_eps * Pk(j) * (eps(j) / k(j)) * dy
enddo

! Dissipation

do j=1,N
  diag(j) = diag(j) + C2_eps * (eps(j) / k(j)) * dy
enddo

! Diffusion

do j=1,N
  ! top of cell
  if (j /= N) then
    nu = (nu_t(j+1) + nu_t(j))/(2.d0*sigma_eps) + nu_lam
    help = nu/(y(j+1) - y(j))
    diag(j)  = diag(j)  + help
    d_upp(j) = d_upp(j) - help
  endif

  ! bottom of cell
  if (j /= 1) then
    nu = (nu_t(j) + nu_t(j-1))/(2.d0*sigma_eps) + nu_lam
    help = nu/(y(j) - y(j-1))
    diag(j)  = diag(j)  + help
    d_low(j) = d_low(j) - help
  endif
enddo

! Overwrite value at wall-cell

diag(1)  = 1.d0
d_upp(1) = 0.d0
d_low(1) = 0.d0
yp = dy / 2.d0
eps_wall = u_k(k(1))**3 / (Kappa * yp)
rhs(1)   = eps_wall

call dgtsv(N,1,d_low(2:N),diag,d_upp(1:N-1),rhs,N,info)
if (info /= 0) STOP 'Problem in Tridiag solver'
eps = rhs

end subroutine calc_eps

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine calc_Pk()
implicit none

integer :: j
double precision :: u_top,u_low,dudy,yp,yp_plus,tau,coef

do j=2,N
  if (j /= N) then
    u_top = (u(j+1) + u(j)) / 2.d0
  else
    u_top = u(j)
  endif

  if (j /= 1) then
    u_low = (u(j) + u(j-1)) / 2.d0
  else
    u_low = u(j)
  endif

  dudy = (u_top - u_low) / dy
  Pk(j) = nu_t(j) * (dudy**2)
enddo

! special case for j=1 (wall cell)
yp = dy / 2.d0
yp_plus = yp * u_k(k(1)) / nu_lam
coef = u_k(k(1)) / u_plus(yp_plus)
tau = coef * abs(u(1))
Pk(1) = tau * abs(u(1)) / yp

end subroutine calc_Pk

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine calc_nu_t()
implicit none

integer :: j

do j=1,N
  nu_t(j) = C_mu * (k(j)**2) / eps(j)
enddo

end subroutine calc_nu_t

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine calc_tau()
implicit none

integer :: j
double precision :: u_top,u_low

tau = 0.d0
do j=2,N
  if (j /= N) then
    u_top = (u(j+1) + u(j)) / 2.d0
  else
    u_top = u(j)
  endif
  
  if (j /= 1) then
    u_low = (u(j) + u(j-1)) / 2.d0
  else
    u_low = u(j)
  endif
  
  tau(j) = (nu_t(j) + nu_lam) * (u_top - u_low) / dy
enddo

end subroutine calc_tau


end module pdes
