#!/bin/bash

function list_deps {
  file=$1
  dir=$(dirname "${file}")
  base=$(basename -- "$file")
  mod="${base%.*}"

  use_list=$( \
    < "$file" \
    grep -i '^ *use ' \
    | awk -F, '{print $1}' \
    | awk '{print $2}' \
    | sort | uniq \
  ) 

  submod=$( \
    < "$file" \
    grep -i '^ *submodule' \
    | awk -F'(' '{print $2}' \
    | awk -F')' '{print $1}' \
    | awk -F':' '{print $NF}'
  )

  deps_statement="${mod}.o: ${file}"
  if [ -n "$submod" ]; then
    deps_statement+=" ${submod}.o"
  fi
  for m in $use_list; do
    # List only those modules that are in the same directory. This assumes a 
    # one-to-one correspondence between the file names and the modules.
    if [ -f "${dir}/${m}".f90 ]; then
      deps_statement+=" ${m}.o"
    fi
  done
  echo -e $deps_statement
}

rm -f src/dependencies
for f in src/*.f90
do
  list_deps "$f" | sed 's/ / \\\n    /g'  >> src/dependencies
done


