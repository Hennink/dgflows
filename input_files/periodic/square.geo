N=20;
Point(1) = {-1, -1, 0};
l1[] = Extrude{2, 0, 0} {Point{1} ; Layers{N, 1}; };
s1[] = Extrude{0, 2, 0} {Line{l1[]} ; Layers{N, 1}; Recombine;};

Physical Surface(10001) = {s1[1]};
Physical Line(996) = {s1[0]};
Physical Line(997) = {s1[2]};
Physical Line(998) = {s1[3]};
Physical Line(999) = {l1[1]};
