X_HALFWIDTH = 0.0005;
Y_HALFWIDTH = 0.0005;
HYDRAULIC_DIAM = 4 * X_HALFWIDTH * Y_HALFWIDTH / (X_HALFWIDTH + Y_HALFWIDTH);
HEIGHT = 5 * HYDRAULIC_DIAM;

NO_ELEM_X = 3; // 20;
NO_ELEM_Y = 3; // 20;
NO_ELEM_Z = 0.5 * HEIGHT / HYDRAULIC_DIAM * Sqrt(NO_ELEM_X * NO_ELEM_Y);

VOLUME_LABEL = 10001;
INLET_SURF_LABEL = 990;
OUTLET_SURF_LABEL = 991;
WATER_SURF_LABEL = 992;
OUTSIDE = 993;
BROTHER_SIDE = 994;

Point(1) = {-X_HALFWIDTH, -Y_HALFWIDTH, 0.0};
Point(2) = {-X_HALFWIDTH, +Y_HALFWIDTH, 0.0};
Point(3) = {+X_HALFWIDTH, +Y_HALFWIDTH, 0.0};
Point(4) = {+X_HALFWIDTH, -Y_HALFWIDTH, 0.0};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {1, 2, 3, 4};
inlet_s = news;
Plane Surface(inlet_s) = {5};
Transfinite Surface{inlet_s};
Recombine Surface{inlet_s};
Transfinite Line {2,-4} = NO_ELEM_X+1 Using Bump 0.2;
Transfinite Line {1,-3} = NO_ELEM_Y+1 Using Bump 0.2;

Printf("element len in flow direction = %g",HEIGHT/NO_ELEM_Z);
out[] = Extrude{0, 0, HEIGHT} {Surface{inlet_s} ; Layers{NO_ELEM_Z, 1}; Recombine;};
outlet_s = out[0];
volume = out[1];
sides_x[] = {out[2],out[4]};
sides_y[] = {out[3],out[5]};

Physical Volume(VOLUME_LABEL) = {volume};
Physical Surface(INLET_SURF_LABEL) = {inlet_s};
Physical Surface(OUTLET_SURF_LABEL) = {outlet_s};
Physical Surface(BROTHER_SIDE) = sides_x[];
Physical Surface(OUTSIDE) = {out[3]};
Physical Surface(WATER_SURF_LABEL) = {out[5]};

