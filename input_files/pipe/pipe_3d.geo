Z_START = 3.0e-3;
HEIGHT = 1.0e-3;
WIDTH = 2.0e-3;
Z_END = 2.0 * WIDTH + 40.0e-3;
Y_MAX = 49.6e-3;
Y_SEG1 = 0.5 * (Y_MAX - WIDTH);
If (0)
    Z_START = 3.0;
    HEIGHT = 40.0;
    WIDTH = 30.0;
    Z_END = 2.0 * WIDTH + 150.0;
    Y_MAX = 49.6;
    Y_SEG1 = 0.5 * (Y_MAX - WIDTH);
EndIf

//  =====    smallest length near corner, in flow direction = 0.00015 = 1.5e-4     =====
LAT_NO_ELEMS_Y = 10;
LAT_BUMP = .3;
LAT_NO_ELEMS_X = 10;
LAT_NO_ELEMS_X += Fmod(LAT_NO_ELEMS_X,2); // Our x-extrusion works best with integer numbers of elems
PROG_ELEMS_X = .8;
LONG_NO_ELEMS_SEG1 = 40;
RATIO_SEG1 = 0.2;
LONG_NO_ELEMS_SEG2 = 100;
BUMP_SEG2 = 0.2;
LONG_NO_ELEMS_SEG3 = 50;
RATIO_SEG3 = 0.1;


VOLUME_LABEL = 10001;
INLET_SURF_LABEL = 990;
OUTLET_SURF_LABEL = 991;
WATER_SURF_LABEL = 992;
OUTSIDE = 993;
BROTHER_SIDE = 994;



all_bottom_line_loops[] = {};
all_brother_bottom_lines[] = {};

// segment 1:
Point(1) = {0.0, 0.0, Z_START};
Point(2) = {0.0, Y_SEG1, Z_START};
Point(3) = {0.0, Y_SEG1, Z_START+WIDTH};
Point(4) = {0.0, 0.0, Z_START+WIDTH};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {1, 2, 3, 4};
all_bottom_line_loops += {5};
all_brother_bottom_lines += {1,3};
inlet_bottom_line = 4;

// bend 1:
Point(5) = {0.0, Y_SEG1+WIDTH, Z_START};
Point(6) = {0.0, Y_SEG1+WIDTH, Z_START+WIDTH};
Line(6) = {2, 5};
Line(7) = {5, 6};
Line(8) = {6, 3};
Line Loop(9) = {-2, 6, 7, 8};
all_bottom_line_loops += {9};
all_brother_bottom_lines += {6,7};

// segment 2:
Point(7) = {0.0, Y_SEG1+WIDTH, Z_END};
Point(8) = {0.0, Y_SEG1, Z_END};
Line(10) = {6, 7};
Line(11) = {7, 8};
Line(12) = {8, 3};
Line Loop(13) = {-8, 10, 11, 12};
all_bottom_line_loops += {13};
all_brother_bottom_lines += {10,12};

// bend 2:
Point(9) = {0.0, Y_SEG1+WIDTH, Z_END+WIDTH};
Point(10) = {0.0, Y_SEG1, Z_END+WIDTH};
Line(14) = {7, 9};
Line(15) = {9, 10};
Line(16) = {10, 8};
Line Loop(17) = {-11, 14, 15, 16};
all_bottom_line_loops += {17};
all_brother_bottom_lines += {15,16};

// segment 3:
Point(11) = {0.0, Y_MAX, Z_END};
Point(12) = {0.0, Y_MAX, Z_END+WIDTH};
Line(18) = {7, 11};
Line(19) = {11, 12};
Line(20) = {12, 9};
Line Loop(21) = {-14, 18, 19, 20};
all_bottom_line_loops += {21};
all_brother_bottom_lines += {18,20};
outlet_bottom_line = 19;

// Collect related lines:
lateral_lines[] = {4,-2,-8,11,-14,19};
outside_bends_lines[] = {6,7,15,16};
seg1_length_lines[] = {1,-3};
seg2_length_lines[] = {10,-12};
seg3_length_lines[] = {18,-20};

// Make the bottom mesh:
Transfinite Line lateral_lines[] = LAT_NO_ELEMS_Y+1 Using Bump LAT_BUMP;
Transfinite Line outside_bends_lines[] = LAT_NO_ELEMS_Y+1 Using Bump LAT_BUMP;
Transfinite Line seg1_length_lines[] = LONG_NO_ELEMS_SEG1+1 Using Progression Exp(Log(RATIO_SEG1)/LONG_NO_ELEMS_SEG1);
Transfinite Line seg2_length_lines[] = LONG_NO_ELEMS_SEG2+1 Using Bump BUMP_SEG2;
Transfinite Line seg3_length_lines[] = LONG_NO_ELEMS_SEG3+1 Using Progression Exp(-Log(RATIO_SEG3)/LONG_NO_ELEMS_SEG3);



// Make the layers in the x-direction:
NO_PNT_X = LAT_NO_ELEMS_X - 1;
MIDPOINT = Floor(0.5 * NO_PNT_X);
MIN_WIDTH = 0.5 * (PROG_ELEMS_X - 1.0) / (PROG_ELEMS_X - PROG_ELEMS_X^(-MIDPOINT));
one[0] = 1;
x_layers[0] = MIN_WIDTH;            // Printf("(0,l) = (%g,%g)", 0,x_layers[0]);
For i In {1:MIDPOINT}
   one[i] = 1;
   x_layers[i] = x_layers[i-1] + MIN_WIDTH * PROG_ELEMS_X^(-i);
                                    // Printf("(i,l) = (%g,%g)", i,x_layers[i]);
EndFor
For j In {MIDPOINT+1:NO_PNT_X}
   one[j] = 1;
   i = NO_PNT_X - j - 1;
   If (i == -1)
      x_layers[j] = 1.0;
   Else
      x_layers[j] = 1.0 - x_layers[i];
   EndIf
EndFor


// Extrude in the x-direction.
// We do the inlet, outlet and brother bottom lines separately, to obtain the 
// surface ID's.
tmp[] = Extrude {HEIGHT,0,0} {
    Line{inlet_bottom_line};
    Layers{one[], x_layers[]};
    Recombine;
};
inlet_surf = tmp[1];
tmp[] = Extrude {HEIGHT,0,0} {
    Line{outlet_bottom_line};
    Layers{one[], x_layers[]};
    Recombine;
};
outlet_surf = tmp[1];
all_brother_surfs[] = {};
For i In {0:9}           // hard-coded hack: 10 == SizeOf(all_brother_bottom_lines)
    l = all_brother_bottom_lines[i];
    tmp[] = Extrude {HEIGHT,0,0} {
        Line{l};
        Layers{one[], x_layers[]};
        Recombine;
    };
    all_brother_surfs += {tmp[1]};
EndFor
all_water_surfs[] = {};
all_outside_surfs[] = {};
all_volumes[] = {};
For i In {0:4}           // hard-coded hack: 5 == SizeOf(all_bottom_line_loops)
    ll = all_bottom_line_loops[i];
    s = news;
    all_water_surfs += {s};
    Plane Surface(s) = {ll};
    Transfinite Surface{s};
    Recombine Surface{s};
    tmp[] = Extrude {HEIGHT,0,0} {
        Surface{s};
        Layers{one[], x_layers[]};
        Recombine;
    };
    all_outside_surfs += {tmp[0]};
    all_volumes += {tmp[1]};
EndFor


// Label the Physical Surfaces:
Physical Surface(INLET_SURF_LABEL) = {inlet_surf};
Physical Surface(OUTLET_SURF_LABEL) = {outlet_surf};
Physical Surface(WATER_SURF_LABEL) = {all_water_surfs[]};
Physical Surface(OUTSIDE) = {all_outside_surfs[]};
Physical Surface(BROTHER_SIDE) = {all_brother_surfs[]};
Physical Volume(VOLUME_LABEL) = {all_volumes[]};
