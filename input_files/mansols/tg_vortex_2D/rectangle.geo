L = 1;
Y_MIN = -L;
Y_MAX = +L;
X_MIN = -L;
X_MAX = +L;

nelem_y = 16;
nelem_x = (X_MAX - X_MIN) / (Y_MAX - Y_MIN) * nelem_y;
bump = 1.0;

FLUID_SURF_LABEL = 10001;
// A single BND label to impose the same Dirichlet BC everywhere:
BND_LABEL = 900;
// BND_LABEL_LEFT = BND_LABEL;
// BND_LABEL_BOTTOM = BND_LABEL;
// BND_LABEL_RIGHT = BND_LABEL;
// BND_LABEL_TOP = BND_LABEL;
// BND_LABEL_LEFT = 900;
// BND_LABEL_BOTTOM = 901;
// BND_LABEL_RIGHT = 902;
// BND_LABEL_TOP = 903;


Point(1) = {X_MIN, Y_MIN, 0};
Point(2) = {X_MAX, Y_MIN, 0};
Point(3) = {X_MAX, Y_MAX, 0};
Point(4) = {X_MIN, Y_MAX, 0};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(101) = {1, 2, 3, 4};

fluid_s = news;
Plane Surface(fluid_s) = {101};
Transfinite Surface{fluid_s} = {1,2,3,4};
Recombine Surface{fluid_s};

Transfinite Line {1,-3} = 1 + nelem_x;
Transfinite Line {2,-4} = 1 + nelem_y Using Bump bump;

Physical Surface(FLUID_SURF_LABEL) = {fluid_s};
Physical Line(BND_LABEL) = {1,2,3,4};
// Physical Line(BND_LABEL_LEFT) = {1};
// Physical Line(BND_LABEL_BOTTOM) = {2};
// Physical Line(BND_LABEL_RIGHT) = {3};
// Physical Line(BND_LABEL_TOP) = {4};
