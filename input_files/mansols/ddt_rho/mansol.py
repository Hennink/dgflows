#!/usr/bin/env python3

import itertools
import numpy
import sympy

# Basic definitions:
coord = sympy.symbols('x y', real=True)
t = sympy.symbols('t', positive=True)
spacetime_vars = [*coord, t]
x, y = coord
dimen = len(coord)
def div(vec):
    assert len(vec) == len(coord)
    return sympy.simplify(sum(sympy.diff(vi,xi) for (vi,xi) in zip(vec,coord)))

# Manufactured solution:
domain = [(-1,1), (-1,1)]
timespan = (0,1)
m = [
    (x+sympy.sympify('1/2'))**2 * sympy.sin(2*t),
    y*t**3,
]
rho = sympy.integrate(-div(m),t) + 2
mu = (y - sympy.sympify('1/2'))**3 + x + 5 + t**4
p = x * (1 - y**3 * t**3)

# Sanity checks on the mansol:
continuity_residual = sympy.sympify(sympy.diff(rho,t) + div(m))
if continuity_residual != 0:
    print('WARNING: Cannot verify continuity equation. Residual = {}'.format(continuity_residual))
assert sympy.integrals.integrate(p, *((xi,*bnd)  for (xi,bnd) in zip(coord,domain))) == 0, 'pressure is not zero on average'
def global_min(scalar):
    minimum = sympy.oo
    space_time_ranges = (numpy.linspace(*minmax,num=10) for minmax in (*domain,timespan))
    for test_point in itertools.product(*space_time_ranges):
        subs = dict(zip(spacetime_vars,test_point))
        minimum = min(minimum,scalar.evalf(subs=subs))
    return minimum
assert global_min(rho) > 1e-6, 'density must be strictly positive; min(rho) = {}'.format(global_min(rho))
assert global_min(mu) >= 0, 'viscosity must be non-negative; min(mu) = {}'.format(global_min(mu))

# Calculate the terms in the NSE:
u = [mi / rho for mi in m]
ddt_m = [sympy.diff(mi,t) for mi in m]
conv = [sum( sympy.diff(mj * ui,xi) for (ui,xi) in zip(u,coord) )
        for mj in m]
grad_p = [sympy.diff(p,xi) for xi in coord]

grad_u = sympy.zeros(dimen,dimen)
for i in range(dimen):
    for j in range(dimen):
        grad_u[i,j] = sympy.diff(u[i],coord[j])
S = mu * (grad_u + grad_u.T - sympy.sympify('2 / 3') * grad_u.trace() * sympy.eye(dimen))
visc = [div(S[i,:]) for i in range(dimen)]

F = [sympy.simplify(ddt_m[i] + conv[i] - visc[i] + grad_p[i]) for i in range(dimen)]

# Print some results
for i, mi in enumerate(m):
    print('m[{}] ='.format(i), mi)
print('p = ',p)
print('rho = ',rho)
print('mu = ',mu)
for i, Fi in enumerate(F):
    print('F[{}] ='.format(i), Fi)

# In a format that DG Flows can read:
specs = [
        'constant_density  = {},'.format('T' if all(sympy.diff(rho,v) == 0 for v in spacetime_vars) else 'F'),
        'div_free_velocity = {},'.format('T' if div(u) == 0 else 'F'),
        '',
        "vol_force = '" + ',    '.join('ExtParser {}'.format(fi) for fi in F) + "',",
        '',
        "velocity = " + ',    '.join('ExtParser {}'.format(ui) for ui in u),
        "pressure = ExtParser {}".format(p),
        '',
        "density  = ExtParser {}".format(rho),
        "kinematic_viscosity  = ExtParser {}".format(mu / rho),
]
for s in specs:
        print(s)

