LEN_Y = 2;
LEN_X = LEN_Y;

NO_ELEM_Y = 5;
NO_ELEM_X = LEN_X / LEN_Y * NO_ELEM_Y;
BUMP = 0.3;

FLUID_SURF_LABEL = 10001;
ALL_BND_LABEL = 900;


Point(1) = {-LEN_X/2, -LEN_Y/2, 0};
Point(2) = {+LEN_X/2, -LEN_Y/2, 0};
Point(3) = {+LEN_X/2, +LEN_Y/2, 0};
Point(4) = {-LEN_X/2, +LEN_Y/2, 0};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(101) = {1, 2, 3, 4};

fluid_s = news;
Plane Surface(fluid_s) = {101};
// Transfinite Surface{fluid_s} = {1,2,3,4};
// Recombine Surface{fluid_s};

Transfinite Line {1,-3} = 1 + NO_ELEM_X;
Transfinite Line {2,-4} = 1 + NO_ELEM_Y Using Bump BUMP;

Physical Surface(FLUID_SURF_LABEL) = {fluid_s};
Physical Line(FLUID_SURF_LABEL) = {fluid_s};
Physical Line(ALL_BND_LABEL) = {1,2,3,4};
