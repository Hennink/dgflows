X_MAX = 3;
Y_MAX = 5;
Z_MAX = 11;

NO_ELEM_X = 3;
NO_ELEM_Y = 3;
NO_ELEM_Z = 7;

VOLUME_LABEL = 10001;
SURF_LABEL = 990;

Point(1) = {0.0, 0.0, 0.0};
Point(2) = {0.0, Y_MAX, 0.0};
Point(3) = {X_MAX, Y_MAX, 0.0};
Point(4) = {X_MAX, 0.0, 0.0};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {1, 2, 3, 4};
inlet_s = news;
Plane Surface(inlet_s) = {5};
Transfinite Surface{inlet_s};
Recombine Surface{inlet_s};
Transfinite Line {2,-4} = NO_ELEM_X+1 Using Bump 0.3;
Transfinite Line {1,-3} = NO_ELEM_Y+1 Using Bump 0.3;

out[] = Extrude{0, 0, Z_MAX} {Surface{inlet_s} ; Layers{NO_ELEM_Z, 1}; Recombine;};
outlet_s = out[0];
volume = out[1];
sides_x[] = {out[2],out[4]};
sides_y[] = {out[3],out[5]};

Physical Volume(VOLUME_LABEL) = {volume};
Physical Surface(SURF_LABEL) = {inlet_s, outlet_s, sides_x[], out[3], out[5]};
