H = 0.5;
STEP_X = H;
LENGTH = 30*H;

p = 20;
q = 400;
NSEGS_X1 = (2.*q)/3. + 1;
NSEGS_X2 = q/3. + 1;
NSEGS_Y1 = p/2. +1;

WALL_BC_ID = 990;
INLET_BC_ID = 991;
OUTLET_BC_ID = 992;
SURFACE_ID = 10001;

Point(1) = {0, 0, 0};
Point(2) = {0, -H, 0};
Point(3) = {LENGTH, -H, 0};
Point(4) = {2*LENGTH, -H, 0};
Point(5) = {2*LENGTH, H, 0};
Point(6) = {LENGTH, H, 0};
Point(7) = {0, H, 0};
Point(8) = {LENGTH, 0, 0};
Point(9) = {2*LENGTH, 0, 0};

Line(1) = {1,2} ;
Line(2) = {2,3} ;
Line(3) = {3,4} ;
Line(4) = {4,9} ;
Line(5) = {5,6} ;
Line(6) = {6,7} ;
Line(7) = {7,1} ;
Line(8) = {3,8} ;
Line(9) = {8,6} ;
Line(10) = {1,8} ;
Line(11) = {9,5} ;  
Line(12) = {8,9} ;

Line Loop(1) = {10,9,6,7} ;
Line Loop(2) = {1,2,8,-10} ;
Line Loop(3) = {12, 11, 5, -9} ;
Line Loop(4) = {3,4,-12,-8} ;

Plane Surface(1) = {1} ;
Plane Surface(2) = {2} ;
Plane Surface(3) = {3} ;
Plane Surface(4) = {4} ;

Physical Line(WALL_BC_ID) = {1,2,3,5,6};
Physical Line(OUTLET_BC_ID) = {4,11};
Physical Line(INLET_BC_ID) = {7};
Physical Surface(SURFACE_ID) = {1,2,3,4};

Transfinite Line {1} = NSEGS_Y1;
Transfinite Line {2} = NSEGS_X1; 
Transfinite Line {3} = NSEGS_X2 Using Progression Exp(Log(2)/NSEGS_X2);
Transfinite Line {4} = NSEGS_Y1;
Transfinite Line {5} = NSEGS_X2 Using Progression Exp(-Log(2)/NSEGS_X2);
Transfinite Line {6} = NSEGS_X1; 
Transfinite Line {7} = NSEGS_Y1;
Transfinite Line {8} = NSEGS_Y1;
Transfinite Line {9} = NSEGS_Y1;
Transfinite Line {10} = NSEGS_X1; 
Transfinite Line {11} = NSEGS_Y1;
Transfinite Line{12} = NSEGS_X2 Using Progression Exp(Log(2)/NSEGS_X2);

Transfinite Surface{1} = {1,8,6,7};
Transfinite Surface{2} = {1,2,3,8};
Transfinite Surface{3} = {8,9,5,6};
Transfinite Surface{4} = {3,4,9,8};

Recombine Surface{1};
Recombine Surface{2};
Recombine Surface{3};
Recombine Surface{4};


