#!/bin/bash

#SBATCH --ignore-pbs
#SBATCH -N 8  --ntasks-per-node=24    # "-N" is the number of nodes. The "--ntasks-per-node" argument defaults to 1.
# #SBATCH -n 48
#SBATCH -J patel
#SBATCH -o SBATCHOut  -e SBATCHError
# #SBATCH --mail-type=ALL
# #SBATCH --mail-user=a.hennink@tudelft.nl
#SBATCH --constraint=haswell
# #SBATCH --constraint=[haswell|ivy] # All nodes the same, either Haswell or Ivy.
#SBATCH -t 2:00:00
# #SBATCH -p short

# #PBS -N patel
# #PBS -l nodes=1:ppn=12:typek
# # #PBS -l walltime=12:30:00
# #PBS -o PBSOutPut -e PBSError
# #PBS -M a.hennink@tudelft.nl
# # #PBS -q test

# set -x
set -e

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters; expected the case number."
    exit 1
fi
case=$1

# Check whether we are using PBS and change some things accordingly:
if [ -n "${PBS_O_WORKDIR+1}" ]; then  # https://unix.stackexchange.com/a/56846/276422
    cd $PBS_O_WORKDIR
    mpi_bin=mpiexec
    work_in_scratch_dir=false
else
    mpi_bin=srun
    work_in_scratch_dir=true
fi

lscpu
init_dir=$(pwd)

if [ "${work_in_scratch_dir}" = true ] ; then
    ### Copy everyting to the scratch space and run there:
    # https://userinfo.surfsara.nl/systems/cartesius/usage/batch-usage
    find ${init_dir} \
        -maxdepth 1 -type f \
        -exec cp -p -t ${TMPDIR} {} +
    cd ${TMPDIR}
    echo "Working in TMPDIR... TMPDIR=$(pwd -P)"
    touch dgflows      # to select newer files later
    # chmod +x dgflows   # not needed if the `cp` command preserves this
fi

if [ ${case} == 0 ]; then
    ln -sf first_run fort.1
    ln -sf first_ic_specs ic_specs
else
    ln -sf later_runs fort.1
    ln -sf later_ic_specs ic_specs
fi

petsc_opts=(
    # '-info' # to get PC ILU fill ratio
    # # Use this on the output:
    # # < SBATCHOut sed -r 's/^ *\[[0-9]+\] *//g' | uniq > reduced
    #
    '-ksp_cg_single_reduction'
    # 
    # '-m_ksp_gmres_restart 200'
    # '-m_ksp_gmres_cgs_refinement_type ifneeded' # for Classical Gram-Schmidt for the Hessenberg matrix in GMRES ; options: none,ifneeded,always ; default: none
    # '-m_ksp_gmres_modifiedgramschmidt' # may be better than the default Classical GS
    # 
    '-p_pc_type bjacobi -p_sub_pc_factor_levels 0'
)

# module load vtune/18.0.4
# srun -l \
#     amplxe-cl -quiet -collect hotspots \
#     -loop-mode=loop-and-function \
#     -result-dir PROFILE_RESULT \
#     -search-dir=. \
#     ./dgflows \
#     &> OUT
# mpiexec -n 7 ./dgflows &> OUT
# aprun -n 96 ./dgflows &> OUT

# ${mpi_bin} ./dgflows \
#         -p_ksp_type pipecg  -p_pc_type bjacobi  -p_sub_pc_type icc  -p_sub_pc_factor_levels 0
#         |& tee OUT_BASELINE

${mpi_bin} ./dgflows -log_view |& tee OUT

# ps -f
# wait # -n

if [ "${work_in_scratch_dir}" = true ] ; then
    ### Copy the new files back:
    find ${TMPDIR} \
        -maxdepth 1 -type f -newer dgflows  \
        -exec cp -p -t ${init_dir} {} +
    cd ${init_dir}
fi

### Move everything to a results directory:
if < OUT grep -q -i \
        -e 'Infinity' \
        -e 'Out of memory' \
        -e 'ASSERTION ERROR' \
        -e 'PETSC ERROR' \
        -e 'ERROR: ' \
        -e "Exec format error";
            #    -e 'WARNING:  ||P_corr||_2/||P||_2 =' \
then
    echo 'found some error-indicating words in the output'
    rm *.temp.dgflow *.pos*.temp
    exit 1

else
    # Move everything to a subdir:
    casedir=$case
    mkdir ${casedir}
    for f in OUT bnd_areas face_integrals* group_plottables* fort.11 *.pos face_pnt_avg div_and_continuity_errors dgf_control_params; do
        if [ -f "$f" ]; then
            mv "$f" ${casedir}/"$f"
        fi
    done
    # Keep some copies to facilitate the next calculation:
    for f in fort.1 .dgflows.elem_no_permut .dgflows.ic* solvec* PBS* SBATCH* *_specs; do
        if [ -f "$f" ]; then
            cp -p "$f" ${casedir}/"$f"  &
        fi
    done
fi

wait
