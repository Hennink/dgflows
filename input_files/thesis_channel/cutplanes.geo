nfields = 9;
nplanes = 3;

For v In {0:nfields-1}
    lo = +1.0e10;
    hi = -1.0e10;
    
    Plugin(CutPlane).View = v;
    Plugin(CutPlane).ExtractVolume = 0 ; 
    // Plugin(CutPlane).iView = 9 ;

    Plugin(CutPlane).A = 1 ; 
    Plugin(CutPlane).B = 0 ; 
    Plugin(CutPlane).C = 0 ; 
    Plugin(CutPlane).D = -0.09 ; 
    Plugin(CutPlane).Run ; 

    Plugin(CutPlane).A = 0 ; 
    Plugin(CutPlane).B = 1 ; 
    Plugin(CutPlane).C = 0 ; 
    Plugin(CutPlane).D = -0.31 ; 
    Plugin(CutPlane).Run ; 

    Plugin(CutPlane).A = 0 ; 
    Plugin(CutPlane).B = 0 ; 
    Plugin(CutPlane).C = 1 ; 
    Plugin(CutPlane).D = -0.05 ; 
    Plugin(CutPlane).Run ; 

    For p In {0:nplanes-1}
        i = nfields + v*nplanes + p;
        lo = View[i].Min < lo ? View[i].Min : lo ;
        hi = View[i].Max > hi ? View[i].Max : hi ;
    EndFor
    diff = hi - lo;
    
    For p In {0:nplanes-1}
        i = nfields + v*nplanes + p;
        View[i].RangeType = 2;  // (1: default, 2: custom, 3: per time step)
        View[i].CustomMin = lo + 0.1 * diff;
        View[i].CustomMax = hi - 0.1 * diff;

        View[i].IntervalsType = 3; // (1: iso, 2: continuous, 3: discrete, 4: numeric)
        View[i].Light = 0;
        // View[i].LightTwoSide = 1;
        
        View[i].Visible = 0;
    EndFor
EndFor

Mesh.VolumeEdges = 0;
Mesh.VolumeFaces = 0;
Mesh.SurfaceEdges = 0;
Mesh.SurfaceFaces = 0;

For v In {0:nfields-1}
    Delete View[0];
EndFor
