// ----------- BEGIN INPUT ------------
VOLUME_LABEL = 10001;
XMIN_SURF_LABEL = 990;
XMAX_SURF_LABEL = 991;
YMIN_SURF_LABEL = 992;
YMAX_SURF_LABEL = 993;
ZMIN_SURF_LABEL = 994;
ZMAX_SURF_LABEL = 995;

RE_TAU = 395.0;

Y_HALFWIDTH = 1.0;
X_LENGTH = 6;
Z_LENGTH = 3;

RATIO_ELEMLEN_XZ = 2.0;
RATIO_ELEMLEN_ZY = 1.0; // along centerline
// Mesh in y-direction, computed elsewhere:
NELEMS_Y = 46;
STRETCHING_PARAM = 2.21319035833826;
// ----------- END INPUT --------------


// Build the y mesh:
// ------ Tanh stretching: ------
    // https://caefn.com/cfd/hyperbolic-tangent-stretching-grid
    For i In {0:NELEMS_Y-1}
      y[i] = 1 - Tanh(STRETCHING_PARAM * (1 - 2 * (i+1) / NELEMS_Y)) / Tanh(STRETCHING_PARAM);
    EndFor
// ------ Based on widths: ------
    // // `widths` is in a.u. (but think of wall units), for half the elems
    // For i In {0:NELEMS_Y/2-1}
    //     widths[i] = 1;
    // EndFor
    // y[0] = widths[0];
    // For i In {1:NELEMS_Y/2}
    //     y[i] = y[i-1] + widths[i-1];
    // EndFor
    // For i In {NELEMS_Y/2+1:NELEMS_Y-1}
    //     y[i] = y[i-1] + widths[NELEMS_Y - i];
    // EndFor
// ------ As the file was initially: ------
    // r = 1.2^(N/2);
    // h0 = 0.5*(1-r)/(1-r^(NELEMS_Y/2));
    // h=0.;
    // For i In {0:(NELEMS_Y-2)/2}
    //     h += h0*r^i;
    //     y[i] = h;
    //     y[NELEMS_Y-i-2] = 1-h;
    // EndFor
    // y[NELEMS_Y/2-1] = 0.5;
    // y[NELEMS_Y-1] = 1.;

// Normalize `y`:
For i In {0:NELEMS_Y-1}
  norm_y[i] = y[i] / y[NELEMS_Y-1];
EndFor

// Find the number of elements for x and z directions:
charac_wall_len = Y_HALFWIDTH / RE_TAU;

Printf("NELEMS_Y = %g", NELEMS_Y);
min_width = 100.0;
max_width = -1.0;
For i In {0:NELEMS_Y/2}
  If (i == 0)
    norm_width = norm_y[0];
  Else
    norm_width = norm_y[i] - norm_y[i-1];
  EndIf
  width = norm_width * 2 * Y_HALFWIDTH;
  dim_y = norm_y[i] * 2 * Y_HALFWIDTH;
  min_width = (width < min_width) ? width : min_width;
  max_width = (width > max_width) ? width : max_width;
  Printf("y+[%g] = %g (width %g)", i, dim_y / charac_wall_len, width / charac_wall_len);
EndFor
Printf("min width y+ = %g", min_width / charac_wall_len);
Printf("max width y+ = %g", max_width / charac_wall_len);

elemlen_y_bulk = max_width;
elemlen_z = elemlen_y_bulk * RATIO_ELEMLEN_ZY;
elemlen_x = elemlen_z * RATIO_ELEMLEN_XZ;
nelems_x = X_LENGTH / elemlen_x;
nelems_z = Z_LENGTH / elemlen_z;
// nelems_x = 5;
// nelems_z = 3;

nelems_x = Round(nelems_x);
nelems_z = Round(nelems_z);
Printf("dz  = %g", elemlen_z);
Printf("dx  = %g", elemlen_x);
Printf("dz+ = %g", elemlen_z / charac_wall_len);
Printf("dx+ = %g", elemlen_x / charac_wall_len);


// Build the mesh:
Point(1) = {0, 0, 0};
out1[] = Extrude {X_LENGTH, 0, 0} {
  Point{1}; Layers{nelems_x};
};

// Physical Line(1) = {out1[1]};
out2[] = Extrude {0, 0, Z_LENGTH} {
  Line{out1[1]}; Layers{nelems_z}; Recombine;
};
// Plane Surface(zmin_surf) = {101};
Physical Surface(YMIN_SURF_LABEL) = {out2[1]};

For i In {0:NELEMS_Y-1}
  nelems_per_extruded_layer[i] = 1;
EndFor
out3[] = Extrude {0.0, 2*Y_HALFWIDTH, 0.0} {
  Surface{out2[1]}; Layers{ nelems_per_extruded_layer[], norm_y[] }; Recombine;
};
Physical Surface(YMAX_SURF_LABEL) = {out3[0]};
Physical Volume(VOLUME_LABEL) = {out3[1]};
Physical Surface(ZMIN_SURF_LABEL) = {out3[2]};
Physical Surface(XMAX_SURF_LABEL) = {out3[3]};
Physical Surface(ZMAX_SURF_LABEL) = {out3[4]};
Physical Surface(XMIN_SURF_LABEL) = {out3[5]};
