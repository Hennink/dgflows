VOLUME_LABEL = 10001;
XMIN_SURF_LABEL = 990;
XMAX_SURF_LABEL = 991;
YMIN_SURF_LABEL = 992;
YMAX_SURF_LABEL = 993;
ZMIN_SURF_LABEL = 994;
ZMAX_SURF_LABEL = 995;

// Dimensions of the channel:
PI = 3.141592653589793;
Y_HALFWIDTH = 1.0;

// Input:
If (0 < 1) // based on (Hiroyuki Abea, Hiroshi Kawamurab, Yuichi Matsuo, 2004):
    // Roughly based on correlations:
    X_LENGTH = 2 * Pi * Y_HALFWIDTH; // 12.6 * Y_HALFWIDTH;
    Z_LENGTH = Pi * Y_HALFWIDTH; // 6.3 * Y_HALFWIDTH;
    
    // Same resolution ratios as in the reference:
    // 'LENPLUS' refers to the mesh length in wall units
    // RE_TAU = 180.0;
    // LENPLUS_X = 9.00;
    // LENPLUS_YWALL = 0.20;
    // LENPLUS_YBULK = 5.90;
    // LENPLUS_Z = 4.50;
    
    RE_TAU = 395.0;
    LENPLUS_X = 9.88;
    LENPLUS_YWALL = 0.15;
    LENPLUS_YBULK = 4.94;
    LENPLUS_Z = 4.94;
    
    LENPLUS_YWALL = 1.0; // override with own insight
    
    ASPECT_RATIO_ZY = LENPLUS_Z / LENPLUS_YBULK;
    ASPECT_RATIO_XZ = LENPLUS_X / LENPLUS_Z;
    Y_PLUS_FIRST_ELEM = LENPLUS_YWALL;
    
ElseIf (0 < 1) // numbers used by (Kim, Moser, Moin, 1987)
     RE_TAU = 150.0;
     X_LENGTH = 2 * PI * Y_HALFWIDTH;
     Z_LENGTH = 4.0/3 * PI * Y_HALFWIDTH;
     // ASPECT_RATIO_ZY = 1.5;
     // ASPECT_RATIO_XZ = 2.0;
     ASPECT_RATIO_ZY = 10 / 7.9;   // as in Abba
     ASPECT_RATIO_XZ = 23 / 10;    // as in Abba
     // DNS_WALLDIST_LEN_RATIO = 4.4;
    Y_PLUS_FIRST_ELEM = 0.5;

Else // numbers used by (Kim, Moser, Moin, 1987)
     RE_TAU = 395.0;
     X_LENGTH = 2 * PI * Y_HALFWIDTH;
     Z_LENGTH = 1 * PI * Y_HALFWIDTH;
     ASPECT_RATIO_ZY = 6.5 / 6.5;
     ASPECT_RATIO_XZ = 10.0 / 6.5;
     DNS_WALLDIST_LEN_RATIO = 6.5;
    Y_PLUS_FIRST_ELEM = 0.5;
EndIf
PROG = 1.2; // 1.1;
LES_DNS_LEN_RATIO = 2.0; // 1.0;

POL_ORDER = 4;
nnod_dg = (1+POL_ORDER) * (2+POL_ORDER) * (3+POL_ORDER) / (1 * 2 * 3);
geom_eff_len_ratio = nnod_dg^(1/3.0); // The FD schemes are also high-order accurate, so this is optimistic.
geom_eff_len_ratio_wall = 1.0; // geom_eff_len_ratio;

// Find the appropriate element lengths in all direction:
wall_dist_bot = Y_HALFWIDTH / RE_TAU;
wall_dist_top = Y_HALFWIDTH / RE_TAU;
wall_dist = 0.5 * (wall_dist_bot + wall_dist_top);

eff_elemlen_y = LENPLUS_YBULK * wall_dist;
// eff_elemlen_y = LES_DNS_LEN_RATIO * DNS_WALLDIST_LEN_RATIO * wall_dist;
// eff_elemlen_y = 0.7 * 7.9 * wall_dist; // It's (7.9 * wall_dist) in Abba for Re of about 180.

max_elemlen_y = LES_DNS_LEN_RATIO * geom_eff_len_ratio * eff_elemlen_y;
elemlen_z = max_elemlen_y * ASPECT_RATIO_ZY;
elemlen_x = elemlen_z * ASPECT_RATIO_XZ;

// Find the elem length in the top/bottom:
first_elemlen_y_bot = geom_eff_len_ratio_wall * Y_PLUS_FIRST_ELEM * wall_dist_bot;
first_elemlen_y_top = geom_eff_len_ratio_wall * Y_PLUS_FIRST_ELEM * wall_dist_top;
nelems_y_bot = Floor(1 + Log(max_elemlen_y / first_elemlen_y_bot)  /  Log(PROG));
nelems_y_top = Floor(1 + Log(max_elemlen_y / first_elemlen_y_top)  /  Log(PROG));
width_bot = first_elemlen_y_bot * (PROG^nelems_y_bot - 1) / (PROG - 1);
width_top = first_elemlen_y_top * (PROG^nelems_y_top - 1) / (PROG - 1);
If (width_bot + width_top < 2 * Y_HALFWIDTH)
     y_bulk_bot = -Y_HALFWIDTH + width_bot;
     y_bulk_top = Y_HALFWIDTH - width_top;
     nbulk_elems_y = Ceil((y_bulk_top - y_bulk_bot) / max_elemlen_y);
Else
     Printf("Too many elems needed to reach bulk elem size.");
     // Redefine bulk lengths to make the mesh fit. 
     // Assume the top and bottom are equal.
     nelems_y_bot = Ceil(Log(1 - Y_HALFWIDTH / first_elemlen_y_bot * (1 - PROG)) / Log(PROG));
     nelems_y_top = Ceil(Log(1 - Y_HALFWIDTH / first_elemlen_y_top * (1 - PROG)) / Log(PROG));
     max_elemlen_y = first_elemlen_y_top * PROG^(nelems_y_bot-1);
     elemlen_z = max_elemlen_y * ASPECT_RATIO_ZY;
     elemlen_x = elemlen_z * ASPECT_RATIO_XZ;
EndIf

// Find the element lengths in the bulk:
nelem_x = X_LENGTH / elemlen_x;
nelem_z = Z_LENGTH / elemlen_z;

// Construct the mesh:
z_halfwidth = Z_LENGTH / 2.0;
If (width_bot + width_top < 2 * Y_HALFWIDTH)
     Point(1) = {X_LENGTH, -Y_HALFWIDTH, -z_halfwidth};
     Point(2) = {X_LENGTH, y_bulk_bot,   -z_halfwidth};
     Point(3) = {X_LENGTH, y_bulk_top,   -z_halfwidth};
     Point(4) = {X_LENGTH, +Y_HALFWIDTH, -z_halfwidth};
     Point(5) = {0.0,      +Y_HALFWIDTH, -z_halfwidth};
     Point(6) = {0.0,      y_bulk_top,   -z_halfwidth};
     Point(7) = {0.0,      y_bulk_bot,   -z_halfwidth};
     Point(8) = {0.0,      -Y_HALFWIDTH, -z_halfwidth};

     Line(1) = {1, 2};
     Line(2) = {2, 3};
     Line(3) = {3, 4};
     Line(4) = {4, 5};
     Line(5) = {5, 6};
     Line(6) = {6, 7};
     Line(7) = {7, 8};
     Line(8) = {8, 1};

     Line Loop(101) = {1, 2, 3, 4, 5, 6, 7, 8};

     zmin_surf = news;
     Plane Surface(zmin_surf) = {101};
     Transfinite Surface{zmin_surf} = {1,4,5,8};
     Recombine Surface{zmin_surf};
     
     Transfinite Line {-4,8} = 1 + nelem_x;
     Transfinite Line {-3,5} = 1 + nelems_y_top Using Progression PROG;
     Transfinite Line {1,-7} = 1 + nelems_y_bot Using Progression PROG;
     Transfinite Line {2,-6} = 1 + nbulk_elems_y;

Else
     Point(1) = {X_LENGTH, -Y_HALFWIDTH, -z_halfwidth};
     Point(2) = {X_LENGTH, 0.0,          -z_halfwidth};
     Point(3) = {X_LENGTH, +Y_HALFWIDTH, -z_halfwidth};
     Point(4) = {0.0,      +Y_HALFWIDTH, -z_halfwidth};
     Point(5) = {0.0,      0.0,          -z_halfwidth};
     Point(6) = {0.0,      -Y_HALFWIDTH, -z_halfwidth};

     Line(1) = {1, 2};
     Line(2) = {2, 3};
     Line(3) = {3, 4};
     Line(4) = {4, 5};
     Line(5) = {5, 6};
     Line(6) = {6, 1};

     Line Loop(101) = {1, 2, 3, 4, 5, 6};

     zmin_surf = news;
     Plane Surface(zmin_surf) = {101};
     Transfinite Surface{zmin_surf} = {1,3,4,6};
     Recombine Surface{zmin_surf};
     
     Transfinite Line {3,-6} = 1 + nelem_x;
     Transfinite Line {1,-2} = 1 + nelems_y_top Using Progression PROG;
     Transfinite Line {4,-5} = 1 + nelems_y_bot Using Progression PROG;
EndIf

hydraulic_diam = 4 * Y_HALFWIDTH;
Printf("hydraulic diameter = %g",hydraulic_diam);
Printf("element len in x direction = %g", X_LENGTH / nelem_x);

out[] = Extrude{0, 0, Z_LENGTH} {Surface{zmin_surf} ; Layers{nelem_z, 1}; Recombine;};
zmax_surf = out[0];
volume = out[1];

Physical Volume(VOLUME_LABEL) = {volume};
Physical Surface(ZMIN_SURF_LABEL) = {zmin_surf};
Physical Surface(ZMAX_SURF_LABEL) = {zmax_surf};
If (width_bot + width_top < 2 * Y_HALFWIDTH)
     Physical Surface(XMAX_SURF_LABEL) = {out[2],out[3],out[4]};
     Physical Surface(YMAX_SURF_LABEL) = {out[5]};
     Physical Surface(XMIN_SURF_LABEL) = {out[6],out[7],out[8]};
     Physical Surface(YMIN_SURF_LABEL) = {out[9]};
Else
     Physical Surface(XMAX_SURF_LABEL) = {out[2],out[3]};
     Physical Surface(YMAX_SURF_LABEL) = {out[4]};
     Physical Surface(XMIN_SURF_LABEL) = {out[5],out[6]};
     Physical Surface(YMIN_SURF_LABEL) = {out[7]};
EndIf

