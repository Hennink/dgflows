#!/usr/bin/env python3

import numpy as np
import dgf_utils

# Input:
channel_halfwidth = 1.0
friction_reynolds = 395.0
wall_rho = 1.0
cfl = 0.5
polorder_m = 2
geom_elem_len = 0.196503
u_tau = 1.0

# Exact relations:
wall_nu = u_tau * channel_halfwidth / friction_reynolds
wall_mu = wall_nu * wall_rho
wall_stress = wall_rho * u_tau**2
hydr_diam = 4 * channel_halfwidth
pdrop = 4 * wall_stress / hydr_diam

turnover_time = channel_halfwidth / u_tau
charac_wall_len = wall_nu / u_tau        # = channel_halfwidth / friction_reynolds
print('friction Re number, based on channel half-width =',friction_reynolds)
print('turn-over time = u_tau / delta =',turnover_time)
print('wall_stress =',pdrop)
print('pdrop =',pdrop)
print('wall_mu ',wall_mu)
print('charac_wall_len =',charac_wall_len)


# From friction factors:
print('\n=== From an estimate of the darcy friction factor with props evaluated at the wall: ===')
bulk_rho = wall_rho
bulk_nu = wall_nu
reynolds_bulk = dgf_utils.reynolds_from_pdrop({
    'hydr_diam':      hydr_diam,
    'density':        bulk_rho,
    'pdrop':          pdrop,
    'kin_viscosity':  bulk_nu,
    'rel_roughness':  0.0,
})
u_bulk = reynolds_bulk * bulk_nu / hydr_diam
darcy_friction_factor = pdrop * (2 / bulk_rho) * hydr_diam / u_bulk**2
flowtime_one_diam = hydr_diam / u_bulk
print('u_bulk = ', u_bulk)
print('reynolds_bulk = ', reynolds_bulk)


# Kolmogorov scales:
print('\n=== From Kolmogorov estimates: ===')
engy_dissipation = u_bulk**3 / channel_halfwidth        # d/dt (energy / mass)
kolmogorov_len = (bulk_nu**3 / engy_dissipation)**(1/4.0)
print('kolmogorov_len = {:0.2e} = {:0.2f} * charac_wall_len'.format(kolmogorov_len,kolmogorov_len/charac_wall_len))


# about the mesh:
print('\n=== From mesh choices: ===')
eff_elem_len = geom_elem_len / polorder_m**2
dt = cfl * eff_elem_len / u_bulk
print('eff_elem_len / kolmogorov_len = {:0.1f}'.format(eff_elem_len / kolmogorov_len))
print('cfl',cfl)
print('dt = ', dt)
print('steps per turn-over time      ',turnover_time / dt)
print('steps per diam. of flow length',flowtime_one_diam / dt)

