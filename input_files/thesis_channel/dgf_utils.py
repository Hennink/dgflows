#!/usr/bin/env python3

from functools import lru_cache
import numpy as np
import functools

def Haaland(rel_roughness,reynolds):
    '''Returns the Haaland estimate for the Darcy friction factor. This 
    approximates the Colebrook-White equation.
    https://en.wikipedia.org/wiki/Darcy_friction_factor_formulae#Haaland_equation
    '''
    rhs = -1.8 * np.log10((rel_roughness/3.7)**1.11 + 6.9/reynolds)
    return 1/rhs**2

def colebrook_white(rel_roughness,reynolds):
    '''Returns the Colebrook-White estimate for the Darcy friction factor.
    https://en.wikipedia.org/wiki/Darcy_friction_factor_formulae#Colebrook%E2%80%93White_equation
    '''
    f_d_haaland = Haaland(rel_roughness,reynolds)
    f_d = f_d_haaland
    # Fixed-point iteration:
    for _ in range(100):
        rhs = -2 * np.log10(rel_roughness/3.71 + 2.51/reynolds/np.sqrt(f_d))
        f_d = 1/rhs**2
    assert (f_d - f_d_haaland) / f_d_haaland <= 0.05, 'Haaland cannot be far off from CW.'
    return f_d

def reynolds_from_pdrop(knowns):
    """Given a set of 'knowns',
    returns the Colebrook-White estimate of the Reynolds number."""
    diam = knowns['hydr_diam']
    rho = knowns['density']
    pdrop = knowns['pdrop']
    nu = knowns['kin_viscosity']
    rel_roughness = knowns['rel_roughness']

    # Iterate with an initial guess for the Darcy friction factor:
    f_d = 0.01
    for _ in range(100):
        u_bulk = np.sqrt(pdrop / f_d * 2 / rho * diam)
        bulk_reynolds = u_bulk * diam / nu 
        f_d = colebrook_white(rel_roughness,bulk_reynolds)
    return bulk_reynolds


def props_func(temperature,pressure,fluid):
    """Returns a function that can be called to get the Coolprop value for a quantity.
    """
    import CoolProp.CoolProp as CoolProp

    return functools.partial(CoolProp.PropsSI, in2='T', in3=temperature, in4='P', in5=pressure, in6=fluid)

@lru_cache()
def read_DNS_flexi(filepath):
    """Returns a dict with data in the Ma=0.2 case in the DNS Flexi file, as sent by Sandeep.
    """
    import pandas as pd
    
    df = pd.read_excel(filepath,sheet_name='7a_Ma02Re2800dT10')
    # I don't understand the other columns ()
    data = {c:df[c] for c in ['y', 'Ux', 'Density', 'Temp', 'Press', 'H', 'RSS', 'H.1', 'TKE']}

    # Two columns are wrongly labeled 'H', when they really contain the y-values for the RSS and TKE:
    data['y_RSS'] = data.pop('H')
    data['y_TKE'] = data.pop('H.1')

    # mach = 0.2
    # T_ref = 340
    # P_ref = 8e6
    # P_bulk = 8.020e6
    # Rho_Ref = 178.65
    U_bulk = 48.958
    H = 0.0000064
    data['y'] /= H
    data['y_RSS'] /= H
    data['y_TKE'] /= H
    data['Ux'] /= U_bulk
    data['RSS'] /= U_bulk**2
    return data


