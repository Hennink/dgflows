#!/bin/bash
set -e

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters; expected the case number."
    exit 1
fi

sed \
    -e 's/ic_specs/ic_specs/' \
    -e 's/process_ic = /! process_ic = /' \
    -e 's/randomize_ic_mmtum_press = T/randomize_ic_mmtum_press = F/' \
    -e 's/PE_data_filepath/! PE_data_filepath/g' \
    -e 's/datafile_for_scalfunc_plot/! datafile_for_scalfunc_plot/g' \
    first_run > later_runs
# sed -i -r 's/gmsh_geo:(.*?).geo/gmsh_msh:\1.msh/g' later_runs

first_case=$1
last_case=$2
name=const

dep=""
# dep="--dependency=afterok:9499342"
# dep="--dependency=afterok:$(squeue | tail -n +2 | head -n 1 | awk '{ print $1 }')"
# dep="-W depend=afterok:1278958.hpc11.hpc"

for (( i=$first_case; i<=$last_case; i++ ))
do
    if hash sbatch 2>/dev/null; then
        cmd=(sbatch -J ${name}_${i}  $dep  calc.sh ${i})
        echo "${cmd[@]}"
        last_out=$("${cmd[@]}")
        dep="--dependency=afterok:${last_out##* }"
    elif hash qsub 2>/dev/null; then
        cmd=(qsub   -N ${name}_${i}  $dep  calc.sh -F ${i})
        echo "${cmd[@]}"
        last_out=$("${cmd[@]}")
        dep="-W depend=afterok:${last_out}"
    else
        ./calc.sh ${i}
    fi
done
