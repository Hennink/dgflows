X_MIN = 0;
Y_MIN = 0;
X_MAX = 1;
Y_MAX = 1;

LEN_X = X_MAX - X_MIN;
LEN_Y = Y_MAX - Y_MIN;

NO_ELEM_X = 3;
NO_ELEM_Y = NO_ELEM_X * LEN_Y / LEN_X;
BUMP = 0.3;

FLUID_SURF_LABEL = 10001;
LABEL_BOTTOM = 901;
LABEL_RIGHT = 902;
LABEL_TOP = 903;
LABEL_LEFT = 904;

Point(1) = {X_MIN, Y_MIN, 0};
Point(2) = {X_MAX, Y_MIN, 0};
Point(3) = {X_MAX, Y_MAX, 0};
Point(4) = {X_MIN, Y_MAX, 0};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(101) = {1, 2, 3, 4};

fluid_s = news;
Plane Surface(fluid_s) = {101};
// Transfinite Surface{fluid_s} = {1,2,3,4};
// Recombine Surface{fluid_s};

Transfinite Line {1,-3} = 1 + NO_ELEM_X;
Transfinite Line {2,-4} = 1 + NO_ELEM_Y Using Bump BUMP;

Physical Surface(FLUID_SURF_LABEL) = {fluid_s};
Physical Line(FLUID_SURF_LABEL) = {fluid_s};

Physical Line(LABEL_BOTTOM) = 1;
Physical Line(LABEL_RIGHT)  = 2;
Physical Line(LABEL_TOP)    = 3;
Physical Line(LABEL_LEFT)   = 4;
