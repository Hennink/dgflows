Y_HALFWIDTH = 0.0005;
X_HALFWIDTH = 1.5 * Y_HALFWIDTH;
HEIGHT = 2 * 2 * X_HALFWIDTH;

NO_ELEM_Y = 8; // 25;
NO_ELEM_X = 0.6 * NO_ELEM_Y * X_HALFWIDTH / Y_HALFWIDTH;
NO_ELEM_Z = 0.8 * HEIGHT / (2*X_HALFWIDTH) * NO_ELEM_X;

VOLUME_LABEL = 10001;
ZMIN_SURF_LABEL = 990;
ZMAX_SURF_LABEL = 991;
YMIN_SURF_LABEL = 992;
YPLUS_SURF_LABEL = 993;
XMIN_SURF_LABEL = 994;
XMAX_SURF_LABEL = 995;

Point(1) = {-X_HALFWIDTH, -Y_HALFWIDTH, 0.0};
Point(2) = {-X_HALFWIDTH, +Y_HALFWIDTH, 0.0};
Point(3) = {+X_HALFWIDTH, +Y_HALFWIDTH, 0.0};
Point(4) = {+X_HALFWIDTH, -Y_HALFWIDTH, 0.0};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {1, 2, 3, 4};
inlet_s = news;
Plane Surface(inlet_s) = {5};
Transfinite Surface{inlet_s};
Recombine Surface{inlet_s};
Transfinite Line {2,-4} = 1 + NO_ELEM_X;
Transfinite Line {1,-3} = 1 + NO_ELEM_Y Using Bump 0.2;

HYDRAULIC_DIAM = 4 * Y_HALFWIDTH;
Printf("hydraulic diameter = %g",HYDRAULIC_DIAM);
Printf("element len in flow direction = %g",HEIGHT/NO_ELEM_Z);
out[] = Extrude{0, 0, HEIGHT} {Surface{inlet_s} ; Layers{NO_ELEM_Z, 1}; Recombine;};
outlet_s = out[0];
volume = out[1];

Physical Volume(VOLUME_LABEL) = {volume};
Physical Surface(ZMIN_SURF_LABEL) = {inlet_s};
Physical Surface(ZMAX_SURF_LABEL) = {outlet_s};
Physical Surface(XMIN_SURF_LABEL) = {out[2]};
Physical Surface(XMAX_SURF_LABEL) = {out[4]};
Physical Surface(YPLUS_SURF_LABEL) = {out[3]};
Physical Surface(YMIN_SURF_LABEL) = {out[5]};

